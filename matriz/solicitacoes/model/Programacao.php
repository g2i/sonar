<?php
final class Programacao extends Record{ 

    const TABLE = 'programacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Programacao pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
    function getCliente() {
        return $this->belongsTo('Cliente','idCliente');
    }
    function getFornecedor() {
        return $this->belongsTo('Fornecedor','idFornecedor');
    }
    function getEmpresa() {
        return $this->belongsTo('Contabilidade','contabilidade');
    }
    function getPeriodicidade() {
        return $this->belongsTo('Periodicidade','periodicidade');
    }
    function getPlanoContas() {
        return $this->belongsTo('Planocontas','idPlanoContas');
    }
    function getCorrecaoMonetaria() {
        return $this->belongsTo('CorrecaoMonetaria','correcaoMonetaria');
    }
}