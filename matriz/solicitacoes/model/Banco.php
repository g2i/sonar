<?php
final class Banco extends Record{ 

    const TABLE = 'banco';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Banco pertence a Tipo_conta
    * @return Tipo_conta $Tipo_conta
    */
    function getTipo_conta() {
        return $this->belongsTo('Tipo_conta','tipoDeConta');
    }

    function getContabilidades() {
        return $this->hasNN('ContabilidadeBancos', 'banco', 'contabilidade', 'Contabilidade');
    }
    
    /**
    * Banco pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','quemCadastro');
    }
    
    /**
    * Banco pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao');
    }
}