<?php

final class Movimento extends Record {

    const TABLE = 'movimento';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure() {
        # $criteria = new Criteria();
        # return $criteria;
    }
    public function getBanco() {
        return $this->belongsTo('Banco', 'banco');
    }

    public function getMovimentoBanco() {
        return $this->belongsTo('MovimentoBanco', 'idMovimentoBanco');
    }

    public function getContabilidade() {

        return $this->belongsTo('Contabilidade', 'idContabilidade');
    }

    public function getFornecedor() {

        return $this->belongsTo('Fornecedor', 'idFornecedor');
    }
    public function getCliente() {

        return $this->belongsTo('Cliente', 'idCliente');
    }

    public function getPlanoContas() {

        return $this->belongsTo('Planocontas', 'idPlanoContas');
    }

    public function graficoMes_old($inicio,$fim,$empresa) {
        $db = new MysqlDB();
        $sql = "SELECT EXTRACT(MONTH  FROM DATA) AS mes, SUM(credito) AS credito, SUM(debito) AS debito FROM movimento WHERE DATA >='".$inicio."' AND DATA <= '".$fim."' AND idContabilidade = ".$empresa." GROUP BY EXTRACT(MONTH  FROM DATA)";
        $db->query($sql);
        return $db->getResults();
    }

    public static function graficoMes($inicio,$fim,$empresa) {
        $query = "SELECT  DATE_FORMAT(DATA,'%Y%m') AS mes, SUM(IFNULL(credito,0)) AS credito, SUM(IFNULL(debito,0)) AS debito, (SUM(IFNULL(credito,0)) - SUM(IFNULL(debito,0))) AS diferenca FROM movimento WHERE DATA >= :inicio AND DATA <= :fim AND idContabilidade = :empresa GROUP BY mes ORDER BY mes";
        $db = new MysqlDB();
        $db->query($query);
        $db->bind(':inicio', $inicio);
        $db->bind(':fim', $fim);
        $db->bind(':empresa', $empresa);
        return $db->getResults();

    }

    public static function mapa($ano =null, $grupo = null, $empresa = null, $mes, $plano) {
    $query = "SELECT ROUND(SUM(IFNULL(M.`credito`, 0)) - SUM(IFNULL(M.`debito`, 0)), 2) AS total
            FROM movimento M
            WHERE M.`status`= 1 AND EXTRACT(YEAR FROM M.`data`) = :ano AND EXTRACT(MONTH FROM M.`data`) = :mes
            AND M.idPlanoContas = :plano";
        if(empty($ano)){
            $a = date('Y');
        }else{
            $a = $ano;
        }
        if(!empty($grupo) && empty($empresa)){
            $query.=" AND M.idContabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo) ";
        }else if(!empty($empresa)){
            $query.=" AND M.idContabilidade IN(:empresa)";
        }
        $db = new MysqlDB();
        $db->query($query);
        $db->bind(':ano', $a);
        if(!empty($grupo) && empty($empresa)) {
            $db->bind(':grupo', $grupo);
        }else if(!empty($empresa)) {
            $db->bind(':empresa', $empresa);
        }
        $db->bind(':mes', $mes);
        $db->bind(':plano', $plano);
        return $db->getRow();
    }

    public static function simplificado($ano = null,$grupo = null,$empresa = null,$mes,$classif) {
    $query = "SELECT ROUND(SUM(IFNULL(M.`credito`, 0)) - SUM(IFNULL(M.`debito`, 0)), 2) AS total
            FROM movimento M
            LEFT JOIN planocontas P
            ON M.`idPlanoContas` = P.`id`
            LEFT JOIN classificacaocontas C
            ON C.id = P.`classificacao`
            WHERE M.`status`= 1 AND EXTRACT(MONTH FROM M.`data`) =:mes AND C.id = :classif AND EXTRACT(YEAR FROM M.`data`) = :ano ";
        if(empty($ano)){
            $a = date('Y');
        }else{
            $a = $ano;
        }
        if(!empty($grupo) && empty($empresa)){
            $query.=" AND M.idContabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo) ";
        }else if(!empty($empresa)){
            $query.=" AND M.idContabilidade IN(:empresa)";
        }
        $db = new MysqlDB();
        $db->query($query);
        $db->bind(':ano', $a);
        if(!empty($grupo) && empty($empresa)) {
            $db->bind(':grupo', $grupo);
        }else if(!empty($empresa)) {
            $db->bind(':empresa', $empresa);
        }
        $db->bind(':mes', $mes);
        $db->bind(':classif', $classif);
        return $db->getRow();
    }



}
