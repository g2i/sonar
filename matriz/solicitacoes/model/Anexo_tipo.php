<?php
final class Anexo_tipo extends Record{ 

    const TABLE = 'anexo_tipo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Anexo_tipo possui Anexo_financeiros
    * @return array de Anexo_financeiros
    */
    function getAnexo_financeiros($criteria=NULL) {
        return $this->hasMany('Anexo_financeiro','tipo',$criteria);
    }
}