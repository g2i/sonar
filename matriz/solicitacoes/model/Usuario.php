<?php
final class Usuario extends Record{ 

    const TABLE = 'usuario';
    const PK = 'id';

    function getStatus() {
        return $this->belongsTo('Status','status');
    }
    
}