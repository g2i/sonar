<?php
final class Anexo_financeiro extends Record{ 

    const TABLE = 'anexo_financeiro';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }

    function getAnexoTipo() {
        return $this->belongsTo('Anexo_tipo','tipo');
    }

    function getUserCreate() {
        return $this->belongsTo('Usuario','criadopor');
    }

    function getUserAlter() {
        return $this->belongsTo('Usuario','alteradopor');
    }
}