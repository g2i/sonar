<?php

final class Cliente extends Record {
    //-----CHIESA
    /*const TABLE = 'cliente';
    const PK = 'codigo';*/
//END CHIESA-----

//-----CULTURA
    /*const TABLE = 'alunos';
    const PK = 'id';*/
//END CULTURA-----

//----RESTANTE
    const TABLE = 'cliente';
    const PK = 'id';
//END RESTANTE-----


    public static function configure(){
        /*$criteria = new Criteria();
        $criteria->addCondition("status","<>",3);
        return $criteria;*/
    }

    function getContasreceber() {
        return $this->hasMany('Contasreceber', 'idCliente');
    }

    public function comboboxesCliente() {
        $db = new MysqlDB();
        $sql = "SELECT id ,nome FROM cliente WHERE status <> 3 order by nome";
        $db->query($sql);
        return $db->getResults();
    }

    function getStatus(){
        return $this->belongsto("Status","status");
    }
}
