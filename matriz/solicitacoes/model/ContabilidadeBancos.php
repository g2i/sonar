<?php
/**
 * Created by PhpStorm.
 * User: RFTD
 * Date: 15/10/2015
 * Time: 13:51
 */
final class ContabilidadeBancos extends Record {

    const TABLE = 'contabilidade_banco';
    const PK = 'id';

    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition("status","<>",3);
        return $criteria;
    }

    function getContabilidade() {
        return $this->belongsto('Contabilidade', 'contabilidade');
    }

    function getBanco() {
        return $this->belongsto('Banco', 'banco');
    }

    function getStatus(){
        return $this->belongsto("Status","status");
    }
}
