<?php
final class Rateio_contasreceber extends Record{ 

    const TABLE = 'rateio_contasreceber';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
        return $criteria;
    }
    
    /**
    * Rateio_contasreceber pertence a Contasreceber
    * @return Contasreceber $Contasreceber
    */
    function getContasreceber() {
        return $this->belongsTo('Contasreceber','contas_receber');
    }

    function getCentro_custo() {
        return $this->belongsTo('Centro_custo','centro_custo');
    }
    
    /**
    * Rateio_contasreceber pertence a Contabilidade
    * @return Contabilidade $Contabilidade
    */
    function getContabilidade() {
        return $this->belongsTo('Contabilidade','empresa');
    }
}