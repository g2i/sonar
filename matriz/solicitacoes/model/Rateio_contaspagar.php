<?php
final class Rateio_contaspagar extends Record{ 

    const TABLE = 'rateio_contaspagar';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
        return $criteria;
    }
    
    /**
    * Rateio_contaspagar pertence a Contaspagar
    * @return Contaspagar $Contaspagar
    */
    function getContaspagar() {
        return $this->belongsTo('Contaspagar','contar_pagar');
    }

    function getCentro_custo() {
        return $this->belongsTo('Centro_custo','centro_custo');
    }

    /**
    * Rateio_contaspagar pertence a Contabilidade
    * @return Contabilidade $Contabilidade
    */
    function getContabilidade() {
        return $this->belongsTo('Contabilidade','empresa');
    }
}