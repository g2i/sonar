<?php
final class Status extends Record{ 

    const TABLE = 'status';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Status possui Classificacaos
    * @return array de Classificacaos
    */
    function getClassificacaos() {
        return $this->hasMany('Classificacao','status');
    }
    
    /**
    * Status possui Contaspagares
    * @return array de Contaspagares
    */
    function getContaspagares() {
        return $this->hasMany('Contaspagar','status');
    }
    
    function getProgramacoes() {
        return $this->hasMany('Programacao', 'status');
    }
    
    /**
    * Status possui Fornecedores
    * @return array de Fornecedores
    */
    function getFornecedores() {
        return $this->hasMany('Fornecedor','status');
    }
    
    /**
    * Status possui Planocontas
    * @return array de Planocontas
    */
    function getPlanocontas() {
        return $this->hasMany('Planocontas','status');
    }

    function getParametros($criteria=NULL) {
        return $this->hasMany('Parametros','status',$criteria);
    }
}