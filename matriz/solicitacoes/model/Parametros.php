<?php
final class Parametros extends Record{ 

    const TABLE = 'parametros';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Parametros pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
}