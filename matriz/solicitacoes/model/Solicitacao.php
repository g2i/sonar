<?php
final class Solicitacao extends Record{ 

    const TABLE = 'solicitacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Solicitacao pertence a Planocontas
    * @return Planocontas $Planocontas
    */
    function getPlanocontas() {
        return $this->belongsTo('Planocontas','planoconta_id');
    }
    
    /**
    * Solicitacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','alterado_por');
    }
    function getSituacao_solicitacao() {
        return $this->belongsTo('Situacao_solicitacao','situacao_solicitacao_id');
    }
    
    /**
    * Solicitacao pertence a Fornecedor
    * @return Fornecedor $Fornecedor
    */
    function getFornecedor() {
        return $this->belongsTo('Fornecedor','idFornecedor');
    }
    
    /**
    * Solicitacao pertence a Contabilidade
    * @return Contabilidade $Contabilidade
    */
    function getContabilidade() {
        return $this->belongsTo('Contabilidade','contabilidade');
    }
    
    /**
    * Solicitacao pertence a Tipo_pagamento
    * @return Tipo_pagamento $Tipo_pagamento
    */
    function getTipo_pagamento() {
        return $this->belongsTo('Tipo_pagamento','tipopagamento_id');
    }
    
    /**
    * Solicitacao pertence a Tipo_documento
    * @return Tipo_documento $Tipo_documento
    */
    function getTipo_documento() {
        return $this->belongsTo('Tipo_documento','tipodocumento_id');
    }
    
    /**
    * Solicitacao pertence a Programacao
    * @return Programacao $Programacao
    */
    function getProgramacao() {
        return $this->belongsTo('Programacao','programacao_id');
    }
    
    /**
    * Solicitacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','autorizado_por');
    }

    function getResponsavel(){
        return $this->belongsTo('Usuario','responsavel');
    }
    /**
    * Solicitacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario22() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
}