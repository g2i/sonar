<?php

final class Contasreceber extends Record {

    const TABLE = 'contasreceber';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure() {
        # $criteria = new Criteria();
        # return $criteria;
    }

    /**
     * Contasreceber pertence a Planocontas
     * @return Planocontas $Planocontas
     */
    function getPlanocontas() {
        return $this->belongsTo('Planocontas', 'idPlanoContas');
    }

    /**
     * Contasreceber pertence a Status
     * @return Status $Status
     */
    function getStatus() {
        return $this->belongsTo('Status', 'status');
    }

    /**
     * Contasreceber pertence a Contabilidade
     * @return Contabilidade $Contabilidade
     */
    function getContabilidade() {
        return $this->belongsTo('Contabilidade', 'contabilidade');
    }
    function getDeducoes() {
        return $this->hasMany('DeducoesContasreceber', 'contasreceber_id');
    }

//    function getDeducoes($deducoesId) {
//        $db = new MysqlDB();
//        $sql = "
//                ";
//        $db->query($sql);
//        return $db->getResults();
//    }

    function getCliente() {
        return $this->belongsTo('Cliente', 'idCliente');
    }
    function getCedente() {
        return $this->belongsTo('Cedente', 'boletocedente');
    }

    public function graficoMes_old() {
        $db = new MysqlDB();
        $sql = "select EXTRACT(MONTH FROM DATA) as mes,
                (select count(id) from contasreceber where dataPagamento is null and EXTRACT(MONTH FROM DATA) = mes) as pendentes,
                (SELECT COUNT(id) FROM contasreceber WHERE dataPagamento IS not NULL AND EXTRACT(MONTH FROM DATA) = mes) AS pagas
                from contasreceber group by EXTRACT(MONTH FROM DATA)
                ";
        $db->query($sql);
        return $db->getResults();
    }

    public static function graficoMes($inicio,$fim,$empresa) {
        $query = "SELECT DATE_FORMAT(CR.`vencimento`,'%Y%m') AS mes,
                  IF(DR.`percentual` IS NULL,SUM(CR.`valor`),((SUM(CR.`valor`)*SUM(REPLACE(DR.`percentual`,',','.')))-SUM(CR.`valor`))) AS valor
                  FROM contasreceber CR LEFT JOIN deducoes_contasreceber DR ON DR.`contasreceber_id` = CR.`id`
                  WHERE CR.`vencimento`>= :inicio AND CR.`vencimento` <= :fim AND CR.contabilidade = :empresa  GROUP BY mes ORDER BY mes ASC  ";
        $db = new MysqlDB();
        $db->query($query);
        $db->bind(':inicio', $inicio);
        $db->bind(':fim', $fim);
        $db->bind(':empresa', $empresa);
        return $db->getResults();
    }

    public function graficoNovo($inicio,$fim) {
        $db = new MysqlDB();
        $sql = "SELECT DATE_FORMAT(CR.`vencimento`,'%Y%m') AS mes,
                IF(DR.`percentual` IS NULL,SUM(CR.`valor`),((SUM(CR.`valor`)*SUM(REPLACE(DR.`percentual`,',','.')))-SUM(CR.`valor`))) AS valorDeduzido
                FROM contasreceber CR
                LEFT JOIN deducoes_contasreceber DR
                ON DR.`contasreceber_id` = CR.`id`
                WHERE CR.`vencimento`>= '".$inicio."' AND CR.`vencimento` <= '".$fim."' GROUP BY mes ORDER BY mes ASC  ";
        $db->query($sql);
        return $db->getResults();
    }

    public static function fluxoCaixaResumido($grupo, $empresas){
        if(empty($empresas)){
            $query = "SELECT
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contasreceber AS c
                    WHERE c.status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) = NOW()
                    AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo)) AS dia,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contasreceber AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01')
                    AND LAST_DAY(NOW()) AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo)) AS mes,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contasreceber AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-01-01')
                    AND DATE_FORMAT(NOW() ,'%Y-12-31') AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo)) AS ano ";
        }else{
            $query = "SELECT
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contasreceber AS c
                    WHERE c.status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) = NOW()
                    AND c.contabilidade IN(:empresas)) AS dia,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contasreceber AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01')
                    AND LAST_DAY(NOW()) AND c.contabilidade IN(:empresas)) AS mes,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contasreceber AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-01-01')
                    AND DATE_FORMAT(NOW() ,'%Y-12-31') AND c.contabilidade IN(:empresas)) AS ano ";
        }

        $db = new MysqlDB();
        $db->query($query);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        $db->execute();
        return $db->getRow();
    }

    public static function fluxoCaixa($empid){
        $fluxo = array();
        $pdo = MysqlDB::Conexao();
        $stmt = $pdo->prepare("CALL getContasReceberMes(:empid)");
        $stmt->bindParam(':empid', $empid, PDO::PARAM_INT);
        try {
            $stmt->execute();
            do {
                $fluxo[] =$stmt->fetch(PDO::FETCH_OBJ);
            } while ($stmt->nextRowset());
            return $fluxo;
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public static function SumMes($inicio, $grupo, $empresas){
        $sql = "SELECT ROUND(SUM(IFNULL(valor, 0)), 2) AS valor
                FROM contasreceber AS c WHERE DATE(c.vencimento) < :inicio
                AND c.status = 1 AND c.dataPagamento IS NULL";

        if(empty($empresas)){
            $sql .= " AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo)";
        }else{
            $sql .= " AND c.contabilidade IN(:empresas)";
        }

        $db = new MysqlDB();
        $db->query($sql);
        $db->bind(':inicio', $inicio);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        try{
            $db->execute();
            $ret = $db->getRow()->valor;
            return $ret == null ? 0 : floatval($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
    }

    public static function SumDiaDia($dia, $grupo, $empresas){
        $sql = "SELECT ROUND(IFNULL(SUM(IFNULL(valor, 0)),0), 2) AS valor
                FROM contasreceber AS c WHERE c.vencimento = :dia
                AND c.status = 1 AND c.dataPagamento IS NULL";

        if(empty($empresas)){
            $sql .= " AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo)";
        }else{
            $sql .= " AND c.contabilidade IN(:empresas)";
        }

        $db = new MysqlDB();
        $db->query($sql);
        $db->bind(':dia', $dia);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        try{
            $db->execute();
            $ret = $db->getRow()->valor;
            return $ret;
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
    }

}
