<?php

final class ContabilidadeGrupos extends Record {

    const TABLE = 'contabilidade_grupo';
    const PK = 'id';

    public static function configure(){
    }

    function getContabilidade() {
        return $this->belongsto('Contabilidade', 'contabilidade');
    }

    function getGrupo() {
        return $this->belongsto('GruposContabilidade', 'grupo');
    }
}