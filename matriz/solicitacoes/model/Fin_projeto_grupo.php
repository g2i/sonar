<?php
final class Fin_projeto_grupo extends Record{ 

    const TABLE = 'fin_projeto_grupo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Fin_projeto_grupo possui Fin_projeto_empresas
    * @return array de Fin_projeto_empresas
    */
    function getFin_projeto_empresas($criteria=NULL) {
        return $this->hasMany('Fin_projeto_empresa','fin_projeto_grupo_id',$criteria);
    }
    
    /**
    * Fin_projeto_grupo pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Fin_projeto_grupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','user_created');
    }
    
    /**
    * Fin_projeto_grupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','user_modified');
    }
}