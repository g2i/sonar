<?php

final class GrupoContabilidade extends Record {

    const TABLE = 'grupo_contabilidade';
    const PK = 'id';

    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition("status","<>",3);
        return $criteria;
    }

    function getContabilidades() {
        return $this->hasNN('ContabilidadeGrupos', 'grupo', 'contabilidade', 'Contabilidade');
    }

    function getStatus(){
        return $this->belongsto("Status","status");
    }
}
