<?php
final class DeducoesContasreceber extends Record{ 

    const TABLE = 'deducoes_contasreceber';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * DeducoesContasreceber pertence a Deducoes
    * @return Deducoes $Deducoes
    */
    function getDeducoes() {
        return $this->belongsTo('Deducoes','deducoes_id');
    }
    
    /**
    * DeducoesContasreceber pertence a Contasreceber
    * @return Contasreceber $Contasreceber
    */
    function getContasreceber() {
        return $this->belongsTo('Contasreceber','contasreceber_id');
    }
}