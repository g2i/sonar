<?php
final class Empresa_custo extends Record{ 

    const TABLE = 'empresa_custo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
        return $criteria;
    }
    
    /**
    * Empresa_custo pertence a Centro_custo
    * @return Centro_custo $Centro_custo
    */
    function getCentro_custo() {
        return $this->belongsTo('Centro_custo','centro_custo');
    }
    
    /**
    * Empresa_custo pertence a Contabilidade
    * @return Contabilidade $Contabilidade
    */
    function getContabilidade() {
        return $this->belongsTo('Contabilidade','empresa');
    }

    function getStatus(){
        return $this->belongsTo('Status','status');
    }
}