<?php
final class Usuario_modulo extends Record{ 

    const TABLE = 'usuario_modulo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Usuario_modulo pertence a Modulo
    * @return Modulo $Modulo
    */
    function getModulo() {
        return $this->belongsTo('Modulo','modulo_id');
    }
    
    /**
    * Usuario_modulo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
}