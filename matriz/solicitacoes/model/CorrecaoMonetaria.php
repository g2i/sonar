<?php
final class CorrecaoMonetaria extends Record{ 

    const TABLE = 'correcaomonetaria';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * CorrecaoMonetaria pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status_id');
    }
}