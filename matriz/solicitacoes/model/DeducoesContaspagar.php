<?php
final class DeducoesContaspagar extends Record{ 

    const TABLE = 'deducoes_contaspagar';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * DeducoesContaspagar pertence a Deducoes
    * @return Deducoes $Deducoes
    */
    function getDeducoes() {
        return $this->belongsTo('Deducoes','deducoes_id');
    }
    
    /**
    * DeducoesContaspagar pertence a Contaspagar
    * @return Contaspagar $Contaspagar
    */
    function getContaspagar() {
        return $this->belongsTo('Contaspagar','contaspagar_id');
    }
}