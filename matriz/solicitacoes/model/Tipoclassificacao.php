<?php
final class Tipoclassificacao extends Record{ 

    const TABLE = 'tipo_classificacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Tipoplanocontas possui Planocontas
    * @return array de Planocontas
    */
    function getClassificacao() {
        return $this->hasMany('Classificacao','tipo');
    }
    
    /**
    * Tipoplanocontas pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
}