<pre><?php

require_once("./lib/core/Config.php");
include './config.php';

ignore_user_abort(true);
set_time_limit(0);

$data_base_geracao = date('Y-m-d');

//-----JS
$conn = new PDO('mysql:host=192.175.99.74;dbname=g2ioper_js', 'g2ioper_js', 'js6990', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
//END STA_LUCCI-----
//
//-----CULTURA
//$conn = new PDO('mysql:host=184.107.23.204;dbname=g2iens_cultura', 'g2iens_cultura', 'cultura6990', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
//END CULTURA-----
//
//-----JS
//$conn = new PDO('mysql:host=184.107.23.204;dbname=g2iens_js', 'g2iens_js', 'js6990', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
//END JS-----
//
////-----CHIESA
/*$conn = new PDO('mysql:host=184.107.70.220;dbname=chiesaon_chiesa', 'chiesaon_ibet', 'chiesa6990', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));*/
//END CHIESA-----

//----LOCALHOST-------
//$conn = new PDO('mysql:host=localhost;dbname=financeiro_unic', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
// END LOCALHOST------


$report['programacoes'] = array();

function incMes($d = false, $qtd = 1)
{
    if (!$d)
        $d = date("Y-m-d");
    $tm = strtotime($d);
    $mk = mktime(0, 0, 0, (date('m', $tm) + $qtd), date('d', $tm), date('Y', $tm));
    while ((date('m', $tm) + $qtd) < (date('m', $mk) + 0))
        $mk = mktime(0, 0, 0, date('m', $mk), (date('d', $mk) - 1), date('Y', $tm));
    return date('Y-m-d', $mk);
}


# resgata todas as programações que precisam ser geradas
$query = '
    SELECT PER.nome AS periodicidade,P.id AS idProgramacao, P.tipo,
    CASE P.tipo
    WHEN "A pagar" THEN (SELECT COUNT(*) FROM contaspagar WHERE `status` = 1 AND `dataPagamento` IS NULL AND vencimento > NOW() AND idProgramacao = P.id )
    WHEN "A receber" THEN (SELECT COUNT(*) FROM contasreceber WHERE `status` = 1 AND `dataPagamento` IS NULL AND vencimento > NOW() AND idProgramacao = P.id )
    END AS totalGerado,
    CASE P.tipo
    WHEN "A pagar" THEN (SELECT MAX(vencimento) FROM contaspagar WHERE `status` = 1 AND idProgramacao = P.id GROUP BY idProgramacao)
    WHEN "A receber" THEN (SELECT MAX(vencimento) FROM contasreceber WHERE `status` = 1 AND idProgramacao = P.id GROUP BY idProgramacao)
    END AS ultimaGerada FROM programacao P
    LEFT JOIN periodicidade PER ON PER.id = P.periodicidade
    ORDER BY totalGerado ASC,ultimaGerada DESC
';
#echo $query;
$programacoes_gerar = $conn->query($query)->fetchAll(PDO::FETCH_OBJ);

# percorre o laço de programações a serem geradas
foreach($programacoes_gerar as $GERACAO)
{
    if(empty($GERACAO->periodicidade)) continue;

    switch($GERACAO->periodicidade){
        case "Mensal":      $qtd_meses_gerar = 3; break;
        case "Bimestral":   $qtd_meses_gerar = 3; break;
        case "Trimestral":  $qtd_meses_gerar = 2; break;
        case "Semestral":   $qtd_meses_gerar = 2; break;
        case "Anual":       $qtd_meses_gerar = 2; break;
    }

    $qtd_meses_gerar -= $GERACAO->totalGerado;

    # caso nao necessite ser gerada nenhuma, pula o laço
    if($qtd_meses_gerar<=0) continue;

    # faz o laço que gera as programações
    for($i=0;$i<$qtd_meses_gerar;$i++){

        $programacao = $conn->query("select * from programacao where id = ".$GERACAO->idProgramacao)->fetchObject();

        if(isset($dt_vencimento) && strtotime($dt_vencimento)>0) {
            $data_base_geracao = strtotime($dt_vencimento);
        }elseif (isset($GERACAO->ultimaGerada) && strtotime($GERACAO->ultimaGerada)>0) {
            $data_base_geracao = strtotime($GERACAO->ultimaGerada);
        }else{
            $data_base_geracao = strtotime($data_base_geracao);
        }

        switch ($GERACAO->periodicidade) {
            case "Mensal":
                $dt_vencimento = incMes(date('Y-m-d', $data_base_geracao), 1);
                $dt_vencimento = strtotime($dt_vencimento);
                $dt_vencimento = date('Y-m-d', mktime(0, 0, 0, date('m', $dt_vencimento), $programacao->diaVencimento, date('Y', $dt_vencimento)));
                break;
            case "Bimestral":
                $dt_vencimento = incMes(date('Y-m-d', $data_base_geracao), 2);
                $dt_vencimento = strtotime($dt_vencimento);
                $dt_vencimento = date('Y-m-d', mktime(0, 0, 0, date('m', $dt_vencimento), $programacao->diaVencimento, date('Y', $dt_vencimento)));
                break;
            case "Trimestral":
                $dt_vencimento = incMes(date('Y-m-d', $data_base_geracao), 3);
                $dt_vencimento = strtotime($dt_vencimento);
                $dt_vencimento = date('Y-m-d', mktime(0, 0, 0, date('m', $dt_vencimento), $programacao->diaVencimento, date('Y', $dt_vencimento)));
                break;
            case "Semestral":
                $dt_vencimento = incMes(date('Y-m-d', $data_base_geracao), 6);
                $dt_vencimento = strtotime($dt_vencimento);
                $dt_vencimento = date('Y-m-d', mktime(0, 0, 0, date('m', $dt_vencimento), $programacao->diaVencimento, date('Y', $dt_vencimento)));
                break;
            case "Anual":
                $dt_vencimento = incMes(date('Y-m-d', $data_base_geracao), 12);
                $dt_vencimento = strtotime($dt_vencimento);
                $dt_vencimento = date('Y-m-d', mktime(0, 0, 0, $programacao->mesVencimento, $programacao->diaVencimento, date('Y', $dt_vencimento)));
                break;
        }
        if ($programacao->tipo == "A pagar") {
            $sql_conta = "insert into contaspagar
                (data,vencimento,valor,juros,multa,dataPagamento,idPlanoContas,idFornecedor,status,
                    complemento,valorBruto,desconto,tipoPagamento,tipoDocumento,idProgramacao,contabilidade)
                    values
                (CURDATE(),'" . $dt_vencimento . "','" . $programacao->valorParcela . "',0,0,NULL," . $programacao->idPlanoContas . ",
                    " . $programacao->idFornecedor . ",1,'" . $programacao->complemento . "'," . $programacao->valorParcela . ",
                    0," . $programacao->tipoPagamento . "," . $programacao->tipoDocumento . "," . $programacao->id . "," . $programacao->contabilidade . "
                ); ";


            $ress_conta = $conn->exec($sql_conta);
            $id_conta_gerada = $conn->lastInsertId();
                $querys = "SELECT *FROM rateio_programacao WHERE programacao = :prog AND status = 1";
                try{
                    $cad = $conn->prepare($querys);
                    $cad->bindParam(":prog",$programacao->id,PDO::PARAM_INT);
                    $cad->execute();
                    $rateio = $cad->fetchAll(PDO::FETCH_OBJ);
                    if($rateio){
                        foreach($rateio as $r){
                            $ins = "INSERT INTO rateio_contaspagar (contar_pagar,valor,observacao,empresa,centro_custo,status) VALUES (:contas_pagar,:valor,:observacao,:empresa,:centro_custo,:status);";
                            $cad2 = $conn->prepare($ins);
                            $cad2->bindValue(":contas_pagar",$id_conta_gerada,PDO::PARAM_INT);
                            $cad2->bindValue(":valor",$r->valor,PDO::PARAM_STR);
                            $cad2->bindValue(":observacao",$r->observacao,PDO::PARAM_STR);
                            $cad2->bindValue(":empresa",$r->empresa,PDO::PARAM_INT);
                            $cad2->bindValue(":centro_custo",$r->centro_custo,PDO::PARAM_INT);
                            $cad2->bindValue(":status",$r->status,PDO::PARAM_INT);
                            $cad2->execute();
                        }
                    }

                }catch (PDOException $e){
                    echo $e->getTraceAsString();
                }

            $sql = "insert into `deducoes_contaspagar` (deducoes_id,valor,contaspagar_id) SELECT deducoes_id,valor,'" . $id_conta_gerada . "' FROM deducoes_programacao WHERE programacao_id = " . $programacao->id . "; ";
            $ress = $conn->exec($sql);



        } else if ($programacao->tipo == "A receber") {

            $sql_conta = "insert into contasreceber (data, valor,juros,multa, dataPagamento, idPlanoContas, idCliente, status, complemento, valorBruto, desconto, tipoPagamento, tipoDocumento, idProgramacao,vencimento,contabilidade,data_cadastro) "
                . "values(CURDATE(),'" . $programacao->valorParcela . "',0,0,NULL," . $programacao->idPlanoContas . "," . $programacao->idCliente . ",
            1,'" . $programacao->complemento . "'," . $programacao->valorParcela . ",0," . $programacao->tipoPagamento . "," . $programacao->tipoDocumento . ",
            " . $programacao->id . ",'" . $dt_vencimento . "'," . $programacao->contabilidade . ",'".date('Y-m-d H:i:s')."');";

            $ress_conta = $conn->exec($sql_conta);
            $id_conta_gerada = $conn->lastInsertId();

            $querys = "SELECT *FROM rateio_programacao WHERE programacao = :prog AND status = 1";
            try{
                $cad = $conn->prepare($querys);
                $cad->bindParam(":prog",$programacao->id,PDO::PARAM_INT);
                $cad->execute();
                $rateio = $cad->fetchAll(PDO::FETCH_OBJ);
                if($rateio){
                    foreach($rateio as $r){
                        $ins = "INSERT INTO rateio_contasreceber (contas_receber,valor,observacao,empresa,centro_custo,status) VALUES (:contas_receber,:valor,:observacao,:empresa,:centro_custo,:status);";
                        $cad2 = $conn->prepare($ins);
                        $cad2->bindValue(":contas_receber",$id_conta_gerada,PDO::PARAM_STR);
                        $cad2->bindValue(":valor",$r->valor,PDO::PARAM_STR);
                        $cad2->bindValue(":observacao",$r->observacao,PDO::PARAM_STR);
                        $cad2->bindValue(":empresa",$r->empresa,PDO::PARAM_INT);
                        $cad2->bindValue(":centro_custo",$r->centro_custo,PDO::PARAM_INT);
                        $cad2->bindValue(":status",$r->status,PDO::PARAM_INT);
                        $cad2->execute();
                    }
                }

            }catch (PDOException $e){
                echo $e->getTraceAsString();
            }

            $sql = "INSERT INTO `deducoes_contasreceber` (deducoes_id,percentual,contasreceber_id) SELECT deducoes_id,percentual,'" . $id_conta_gerada . "' FROM deducoes_programacao WHERE programacao_id = " . $programacao->id . ";";
            $ress = $conn->exec($sql);

        }

        if ($ress_conta == 1) {
            $sql = "update programacao set
                geradas = (SELECT COUNT(*) AS cont FROM contasreceber WHERE `idProgramacao` = ".$programacao->id."),
                ultimaGeracao = curdate(),
                ultimoGerado = (SELECT MAX(vencimento) FROM contasreceber WHERE `idProgramacao` = ".$programacao->id.")
                where id = ".$programacao->id;

            $ress = $conn->exec($sql);

        }

    }

    $data_base_geracao = date('Y-m-d');
    unset($qtd_meses_gerar);
    unset($dt_vencimento);

}

exit;