<?php

function convertData($data) {
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

function convertDataBR4SQL($data) {
    try {
        if ($data != "") {
            list($d, $m, $y) = preg_split('/\//', $data);
            return sprintf('%04d-%02d-%02d', $y, $m, $d);
        }
    }catch (Exception $e){
        return null;
    }
}

function DataBR($data) {

    if ($data != "") {

        list($y, $m, $d) = preg_split('/-/', $data);

        return sprintf('%02d/%02d/%04d', $d, $m, $y);

    }

}
function tratar_ultimos_dias_do_mes($dia,$data){

    $DataComUltimoDiaDoMes = new DateTime( $data );
    $newDate =  new DateTime( $data );
    $ultimoDiaDoMes = $DataComUltimoDiaDoMes->format( 't' );
    if($ultimoDiaDoMes >= $dia)
        $newDate->setDate($DataComUltimoDiaDoMes->format( 'Y' ),$DataComUltimoDiaDoMes->format( 'm' ),$dia);
    else
        $newDate->setDate($DataComUltimoDiaDoMes->format( 'Y' ),$DataComUltimoDiaDoMes->format( 'm' ),$ultimoDiaDoMes);

    return $newDate->format( 'Y-m-d' );


}

function convertDataSQL4BR($data) {
    try{
        if ($data != "") {
            list($y, $m, $d) = preg_split('/-/', $data);
            return sprintf('%02d/%02d/%04d', $d, $m, $y);
        }
    }catch (Exception $e){
        return null;
    }
}

function DataTimeBr($data){
    if ($data != "") {
        return  date("d/m/Y H:i:s", strtotime($data));//exibe no formato d/m/a
    }
}

function getAmount($money)
{
    try{
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }catch (Exception $e){
        return null;
    }
}

function validaCnpj($cnpj) {
    $cnpj = trim($cnpj);
    $soma = 0;
    $multiplicador = 0;
    $multiplo = 0;


    # [^0-9]: RETIRA TUDO QUE NÃO É NUMÉRICO,  "^" ISTO NEGA A SUBSTITUIÇÃO, OU SEJA, SUBSTITUA TUDO QUE FOR DIFERENTE DE 0-9 POR "";
    $cnpj = preg_replace('/[^0-9]/', '', $cnpj);

    if (empty($cnpj) || strlen($cnpj) != 14)
        return FALSE;

    # VERIFICAÇÃO DE VALORES REPETIDOS NO CNPJ DE 0 A 9 (EX. '00000000000000')    
    for ($i = 0; $i <= 9; $i++) {
        $repetidos = str_pad('', 14, $i);

        if ($cnpj === $repetidos)
            return FALSE;
    }

    # PEGA A PRIMEIRA PARTE DO CNPJ, SEM OS DÍGITOS VERIFICADORES    
    $parte1 = substr($cnpj, 0, 12);

    # INVERTE A 1ª PARTE DO CNPJ PARA CONTINUAR A VALIDAÇÃO
    $parte1_invertida = strrev($parte1);

    # PERCORRENDO A PARTE INVERTIDA PARA OBTER O FATOR DE CALCULO DO 1º DÍGITO VERIFICADOR
    for ($i = 0; $i <= 11; $i++) {
        $multiplicador = ($i == 0) || ($i == 8) ? 2 : $multiplicador;

        $multiplo = ($parte1_invertida[$i] * $multiplicador);

        $soma += $multiplo;

        $multiplicador++;
    }

    # OBTENDO O 1º DÍGITO VERIFICADOR        
    $rest = $soma % 11;

    $dv1 = ($rest == 0 || $rest == 1) ? 0 : 11 - $rest;

    # PEGA A PRIMEIRA PARTE DO CNPJ CONCATENANDO COM O 1º DÍGITO OBTIDO 
    $parte1 .= $dv1;

    # MAIS UMA VEZ INVERTE A 1ª PARTE DO CNPJ PARA CONTINUAR A VALIDAÇÃO 
    $parte1_invertida = strrev($parte1);

    $soma = 0;

    # MAIS UMA VEZ PERCORRE A PARTE INVERTIDA PARA OBTER O FATOR DE CALCULO DO 2º DÍGITO VERIFICADOR       
    for ($i = 0; $i <= 12; $i++) {
        $multiplicador = ($i == 0) || ($i == 8) ? 2 : $multiplicador;

        $multiplo = ($parte1_invertida[$i] * $multiplicador);

        $soma += $multiplo;

        $multiplicador++;
    }

    # OBTENDO O 2º DÍGITO VERIFICADOR
    $rest = $soma % 11;

    $dv2 = ($rest == 0 || $rest == 1) ? 0 : 11 - $rest;

    # AO FINAL COMPARA SE OS DÍGITOS OBTIDOS SÃO IGUAIS AOS INFORMADOS (OU A SEGUNDA PARTE DO CNPJ)   
    return ($dv1 == $cnpj[12] && $dv2 == $cnpj[13]) ? TRUE : FALSE;
    //echo ($dv1 == $cnpj[12] && $dv2 == $cnpj[13]) ? 'TRUE' : 'FALSE';} 
}

function validaCep($cep) {
//    $cep = $_POST['cep'];
    $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);

    $dados['sucesso'] = (string) $reg->resultado;
    $dados['rua'] = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
    $dados['bairro'] = (string) $reg->bairro;
    $dados['cidade'] = (string) $reg->cidade;
    $dados['estado'] = (string) $reg->uf;

    return json_encode($dados);

}

/**
* @param $ano ano em que se quer calcular os feriados
* @return array com os feriados do ano (fixo e moveis)
 */
function getFeriados($ano){
    $dia = 86400;
    $datas = array();
    $datas['pascoa'] = easter_date($ano);
    $datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
    $datas['carnaval'] = $datas['pascoa'] - (47 * $dia);
    $datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
    $feriados = array (
        '01/01',
        '02/02', // Navegantes
        date('d/m',$datas['carnaval']),
        date('d/m',$datas['sexta_santa']),
        date('d/m',$datas['pascoa']),
        '21/04',
        '01/05',
        date('d/m',$datas['corpus_cristi']),
        '13/06',
        '26/08',
        '07/09',
        '11/10',
        '12/10',
        '02/11',
        '15/11',
        '25/12',
    );
    return $feriados;
}

function tratar_feriados($inicio,$feriados){
    foreach ($feriados as $feriado) {
        if($feriado==date('d/m',strtotime($inicio))){
            $inicio = date('Y-m-d', strtotime('+1 days', strtotime($inicio)));
        }
    }
    $axx=0;
    while ($axx==0) {
        if (date('w', strtotime($inicio)) > 0 && date('w', strtotime($inicio)) < 6) {
            $axx=1;
        }else{
            $inicio = date('Y-m-d', strtotime('+1 days', strtotime($inicio)));
        }
    }
    return $inicio;
}

?>