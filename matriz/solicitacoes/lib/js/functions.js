/**
 * Created by Desenvolvimento on 29/06/2015.
 */
function isset() {
    var a = arguments,
        l = a.length,
        i = 0,
        undef;

    if (l === 0) {
        throw new Error('Empty isset');
    }

    while (i !== l) {
        if (a[i] === undef || a[i] === null) {
            return false;
        }
        i++;
    }
    return true;
}

function empty(mixed_var) {
    var undef, key, i, len;
    var emptyValues = [undef, null, false, 0, '', '0'];

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }

    if (typeof mixed_var === 'object') {
        for (key in mixed_var) {
            // TODO: should we check for own properties only?
            //if (mixed_var.hasOwnProperty(key)) {
            return false;
            //}
        }
        return true;
    }

    return false;
}

/*
 * Autor: Jo?o Ghinozzi
 * Contato: ghinozzi@gmail.com
 * @param {string} cnpj
 * Consulta um webservice o cep passado e retorna o endere?o, preenche automaticamente os campos #rua,#bairro,#cidade,#uf e da focus no campo #numero.
 */
function preencheCep(cep) {
    /* Configura a requisição AJAX */
    $.ajax({
        url: root + '/?m=Fornecedor&p=buscaCep',
        type: 'POST', /* Tipo da requisição */
        data: 'cep=' + cep, /* dado que será enviado via POST */
        dataType: 'json', /* Tipo de transmissão */
        success: function(data) {
            if (data.sucesso == 1) {
                $('#rua').val(data.rua);
                $('#bairro').val(data.bairro);
                $('#cidade').val(data.cidade);
                $('#uf').val(data.estado);
                $('#numero').focus();
            } else {
                OpenDialog('Erro', 'O Cep não foi encontrado.Preencha manualmente os campos!');
//                    $("#cep").val('');
            }
        }
    });
    return false;
}

/*
 * Autor: Jo?o Ghinozzi
 * Contato: ghinozzi@gmail.com
 * @param {string} cnpj
 * Verifica se o cnpj passado ? valido.
 */
function validaCnpj_old(cnpj) {
    /* Configura a requisição AJAX */
    if (cnpj != '') {
        $.ajax({
            url: root + '/?m=Fornecedor&p=validaCnpj',
            type: 'POST', /* Tipo da requisição */
            data: 'cnpj=' + cnpj, /* dado que será enviado via POST */
            dataType: 'json', /* Tipo de transmissão */
            success: function(data) {
                if (data == '0') {
                    alert("CNPJ invalido");
                    //$('#cnpj').val("");
                    $('#cnpj').focus();
                }
            }
        });
        return false;
    }
}

function validarCNPJ(cnpj) {

    cnpj = cnpj.replace(/[^\d]+/g,'');

    if(cnpj == '') return false;

    if (cnpj.length != 14)
        return false;

    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;

    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;

    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;

    return true;

}

// tranforma valor em dinhero
Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function replaceAll(string, token, newtoken) {
    while (string.indexOf(token) != -1) {
        string = string.replace(token, newtoken);
    }
    return string;
}


function initComponente(document){


    $(document).find('#btn-filtro').click(function(){
        var caminho = $(document).find('form').attr('action')
        var form = $(document).find('form').serializeObject()
        $("div.table-responsive").loadPostGrid(caminho, form, null,carregar);
        return false;
    })
}

function carregar(){
    $(document).find('body').each(function(){
        initComponente(document)
    })

}

function OpenMensagem(titulo,mensagem){
    var div = document.createElement("div");
    div.setAttribute('id', 'dialogModal');
    div.setAttribute('class', 'modal fade');
    document.body.appendChild(div);

    var html = "";
    html+="<div class='modal-dialog'>";
    html+="<div class='modal-content'>"+
        "<div class='modal-header'>"+
        "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+
        "<h4 class='modal-title' id='titleAlerta'>"+titulo+"</h4>"+
        "</div>"+
        "<div class='modal-body'>"+
        "<p id='msgAlerta'>"+mensagem+"</p>"+
        "</div>"+
        "<div class='modal-footer'>"+
        "<button type='button' class='btn btn-default' onclick='fech()'>Fechar</button>"+
        "</div>"+
        "</div>"+
        "</div>";
    $("#dialogModal").modal('show');
    $("#dialogModal").html(html);
}

function fech(){
    $(".dialogModal").modal('togger');
    setTimeout(function(){
        $("#dialogModal").remove();
    }, 1000);
}

function diferencaEmDias(dataInicial, dataFinal) {
    var divisor = (1000*60*60*24);
    return (dataFinal-dataInicial)/divisor;
}

function openOnModal(url, title, size, buttons, validation){
    LoadGif();
    var dialogSize = BootstrapDialog.SIZE_NORMAL;
    var dialogButtons = [];

    if(size)
        dialogSize = size;

    if(buttons)
        dialogButtons = buttons;

    $('<div></div>').load(url, function(){
        BootstrapDialog.show({
            size: dialogSize,
            title: title,
            message: this,
            type: BootstrapDialog.TYPE_DEFAULT,
            buttons: dialogButtons,
            onshown: function(dialog){
                initeComponentes(dialog.getModalBody());
                dialog.getModal().removeAttr('tabindex');
                CloseGif();
            },
            onhide: function(dialog){
                if($.isFunction(validation))
                    return !validation.call();
                else
                    return true;
            }
        });
    });
}

function showOnModal(url, title, size, buttons, validation){
    LoadGif();
    Acrescentar(url);
    var dialogSize = BootstrapDialog.SIZE_NORMAL;
    var dialogButtons = [];

    if(size)
        dialogSize = size;

    if(buttons)
        dialogButtons = buttons;

    $('<div></div>').load(url, function(){
        BootstrapDialog.show({
            size: dialogSize,
            title: title,
            message: this,
            type: BootstrapDialog.TYPE_DEFAULT,
            buttons: dialogButtons,
            onshown: function(dialog){
                initeComponentes(dialog.getModalBody());
                dialog.getModal().removeAttr('tabindex');
                CloseGif();
            },
            onhide: function(dialog){
                if($.isFunction(validation))
                    return validation.call(dialog);
                else
                    return true;
            }
        });
    });
}

function initeComponentes(element){

    $(element).find('.selectPicker').each(function() {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            minimumResultsForSearch: 6,
            placeholder: 'Selecione:'
        });
    });

    $(element).find('.deTransferencia').each(function() {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            minimumResultsForSearch: 6,
            placeholder: 'Selecione um banco:'
        }).change(function(){
            var url = root + '/Movimento/getPara';
            var data = {
                id: $(this).val()
            }

            $.post(url, data, function(txt){
                $(element).find('.paraTransferencia').select2('destroy');
                $(element).find('.paraTransferencia').html(txt);
                $(element).find('.paraTransferencia').select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    minimumResultsForSearch: 6,
                    placeholder: 'Selecione um banco:'
                });
            });
        });
    });

    $(element).find('.paraTransferencia').each(function() {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            minimumResultsForSearch: 6,
            placeholder: 'Selecione um banco:'
        });
    });

    $(element).find('.money2').each(function(){
        $(this).priceFormat({
            prefix: '',
            centsLimit: 2,
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
    });

    $(element).find('.currency').each(function(){
        $(this).priceFormat({
            prefix: '',
            centsLimit: 2,
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
    });

    $(element).find('.date').each(function(){
        $(this).datetimepicker({
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            locale: 'pt-Br',
            format: 'L',
            extraFormats: [ 'YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss' ],
            tooltips: {
                today: 'Ir para Hoje',
                clear: 'Limpar sele\u00e7\u00e3o',
                close: 'Fechar',
                selectMonth: 'Selecione o M\u00eas',
                prevMonth: 'M\u00eas Anterior',
                nextMonth: 'Pr\u00f3ximo M\u00eas',
                selectYear: 'Selecione o Ano',
                prevYear: 'Ano Anterior',
                nextYear: 'Pr\u00f3ximo Ano',
                selectDecade: 'Selecione a D\u00e9cada',
                prevDecade: 'D\u00e9cada Anterior',
                nextDecade: 'Pr\u00f3xima D\u00e9cada',
                prevCentury: 'S\u00e9culo Anterior',
                nextCentury: 'Pr\u00f3ximo S\u00e9culo'
            }
        });
    });

    $(element).find('.dateFormat').each(function(){
        $(this).inputmask("d/m/y",{ "placeholder": "dd/mm/yyyy" });
    });

    $(element).find('.percent').each(function(){
        $(this).priceFormat({
            prefix: '',
            limit: 5,
            centsLimit: 2,
            centsSeparator: ',',
            thousandsSeparator: '',
            allowNegative: false
        });
    });

    $(element).find('select.clientes-ajax').each(function(){
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um cliente'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Cliente/findClientes/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    $(element).find('select.credor-ajax').each(function(){
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um credor'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Fornecedor/findCredor/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    $(element).find('select.plano-ajax').each(function(){
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um credor'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Planocontas/findPlanos/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    var inicio = $(element).find('.dateInicio').first();

    var fim = $(element).find('.dateFim').first();

    if(inicio && fim) {
        inicio.datetimepicker({
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            maxDate: moment().endOf('month').format('YYYY-MM-DD'),
            locale: 'pt-Br',
            format: 'L',
            tooltips: {
                today: 'Ir para Hoje',
                clear: 'Limpar sele\u00e7\u00e3o',
                close: 'Fechar',
                selectMonth: 'Selecione o M\u00eas',
                prevMonth: 'M\u00eas Anterior',
                nextMonth: 'Pr\u00f3ximo M\u00eas',
                selectYear: 'Selecione o Ano',
                prevYear: 'Ano Anterior',
                nextYear: 'Pr\u00f3ximo Ano',
                selectDecade: 'Selecione a D\u00e9cada',
                prevDecade: 'D\u00e9cada Anterior',
                nextDecade: 'Pr\u00f3xima D\u00e9cada',
                prevCentury: 'S\u00e9culo Anterior',
                nextCentury: 'Pr\u00f3ximo S\u00e9culo'
            }
        });
        fim.datetimepicker({
            useCurrent: false,
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            minDate: moment().date(1).format('YYYY-MM-DD'),
            locale: 'pt-Br',
            format: 'L',
            tooltips: {
                today: 'Ir para Hoje',
                clear: 'Limpar sele\u00e7\u00e3o',
                close: 'Fechar',
                selectMonth: 'Selecione o M\u00eas',
                prevMonth: 'M\u00eas Anterior',
                nextMonth: 'Pr\u00f3ximo M\u00eas',
                selectYear: 'Selecione o Ano',
                prevYear: 'Ano Anterior',
                nextYear: 'Pr\u00f3ximo Ano',
                selectDecade: 'Selecione a D\u00e9cada',
                prevDecade: 'D\u00e9cada Anterior',
                nextDecade: 'Pr\u00f3xima D\u00e9cada',
                prevCentury: 'S\u00e9culo Anterior',
                nextCentury: 'Pr\u00f3ximo S\u00e9culo'
            }
        });

        inicio.on("dp.change", function (e) {
            fim.data("DateTimePicker").minDate(e.date);
        });
        fim.on("dp.change", function (e) {
            inicio.data("DateTimePicker").maxDate(e.date);
        });
    }
}

const g2iNavObject = {"navegacao": [{"p": 1, "u": root}]};

function Navegar(url,param){
    var pos = g2iNavObject.navegacao.length;
    if(param=='go'){
        pos = pos+1;
        var url2 = {"p": pos, "u": url};
        g2iNavObject.navegacao.push(url2);
    }else{
        pos = pos-1;
        g2iNavObject.navegacao.pop();
    }

    $.each(g2iNavObject,function(key, data){
        $.each(data,function(id,valor){
            var modal = $('.modal');
            if(pos==1){
                modal.modal('hide');
            }else

            if(valor.p==pos) {
                if(param=='go') {
                    if(modal.find('.modal-body') != null){
                        modal.find('.modal-body').load(valor.u, function(){
                            initeComponentes(this);
                        });
                    }else {
                        modal.find('.modal-content').load(valor.u, function(){
                            initeComponentes(this);
                        });
                    }
                }
                else {
                    if (valor.u == "")
                        return;

                    if(modal.find('.modal-body') != null){
                        modal.find('.modal-body').load(valor.u, function(){
                            initeComponentes(this);
                        });
                    }else {
                        modal.find('.modal-content').load(valor.u, function(){
                            initeComponentes(this);
                        });
                    }
                }
            }
        });
    });
}

function Acrescentar(url){
    var pos = g2iNavObject.navegacao.length;
    pos = pos+1;
    var url = {"p": pos, "u": url};
    g2iNavObject.navegacao.push(url);
}

function Decrementar(url,param){
    g2iNavObject.navegacao.pop();
    Navegar(url,param);
}

function EnviarFormulario(form){
    $(form).ajaxForm({
        success: function(d){
            Navegar('','back');
        }
    });
}

function EnviarDados(form){
    $(form).ajaxForm({
        success: function(ret){
            if(!ret.sucess) {
                BootstrapDialog.warning(ret.msg);
                return;
            }

            BootstrapDialog.confirm({
                title: 'Salvo com sucesso',
                message: ret.msg + '\r\nIniciar novo cadastro ?',
                type: BootstrapDialog.TYPE_SUCCESS,
                closable: true,
                draggable: true,
                btnCancelLabel: 'Fechar',
                btnOKLabel: 'Novo Cadastro',
                btnOKClass: 'btn-success',
                callback: function(result) {
                    if(!result)
                        Navegar('', 'back');
                    else
                        ResetForm(form)
                }
            });
        }
    });
}

function ResetForm(form){
    $(form)[0].reset();
    $(form).find(":input").not(':button,:hidden').each(function(){
        $(this).val('').change();
    });
}

function FormularioCredor(form){
    $(form).ajaxForm({
        success: function(d){
            $('.modal').modal('hide');
        }
    });
}

function FormularioAll(form,controller,campo){
    $(form).ajaxForm({
        success: function(d){
            Navegar('','back');
            setTimeout(function(){
                SelectAll(d,controller,campo);
            },1000);

        }
    });
}

function SelectAll(id,controller,campo){
    $.ajax({
        type:'POST',
        url:root+'/'+controller+'/combo',
        data:"id="+id,
        success:function(txt){
            $("[role="+campo+"]").html(txt);
        }
    });
}

function EnviarContas(form,origem){
    $(form).ajaxForm({
        success: function (d) {
            if (d > 0) {
                Navegar('','back');
                d = parseFloat(d);
                $(".valor").val(d.formatMoney(2, ',', '.'));
                $("#empresa").val("");
                $(".centro_custo").val("");
            } else{
                if(origem=="pagar"){
                    BootstrapDialog.alert("Lançamentos Realizados com sucesso!");
                    location.href = root + "/Contaspagar/all/";
                }else {
                    if ($(".deducoes").val() == 1) {
                        BootstrapDialog.alert("Lan�amento Realizado com sucesso!");
                        setTimeout(function () {
                            location.href = root + "/Contasreceber/edit/id:" + $(".contas_receber").val();
                        }, 2000);
                    } else {
                        BootstrapDialog.alert("Lan�amento Realizado com sucesso!");
                        setTimeout(function () {
                            location.href = root + "/Contasreceber/all";
                        }, 2000);
                    }
                }
            }
        }
    });
}

function ChecarRateios(id,origem){
    if(origem=="pagar")
        var url =root+"/Rateio_contaspagar/checar_rateio";
    else if(origem=="programacao")
        var url =root+"/Rateio_programacao/checar_rateio";
    else
        var url =root+"/Rateio_contasreceber/checar_rateio";

    var data = {
        id: id
    }

    $.post(url, data, function(txt){
        if(txt>0){
            txt = parseFloat(txt);
            $(".valor").val(txt);
            $(".save_rat").removeAttr("disabled");

        }else{
            txt = parseFloat(txt);
            $(".valor").val(txt);
            BootstrapDialog.alert("Para este lan�amento j� foi feito o rateio!");
            $("#empresa").val('').trigger('change');
            $(".save_rat").attr("disabled","true");
        }
    });
}

function ChecarDuplicidade(id,empresa,centro,origem){
    if(origem=="pagar")
        var url =root+"/Rateio_contaspagar/checar_duplicidade";
    else if(origem=="programacao")
        var url =root+"/Rateio_programacao/checar_duplicidade";
    else
        var url = root+"/Rateio_contasreceber/checar_duplicidade";

    var data = {
        id: id,
        empresa: empresa,
        centro: centro
    }

    $.post(url, data, function(txt){
        if(txt>0){
            BootstrapDialog.alert("J� existe um rateio para esta empresa e este Centro de Custo!<br /> Favor selecionar outra empresa ou outro Centro de Custo!");
            $(".save_rat").attr("disabled","true");
        }else{
            $(".save_rat").removeAttr("disabled");
        }
    });
}

var criarLink = function(url){
    $("#modal_start").attr("href",url);
    $("#modal_start").click();
};

function SomenteNumero(e){
    var tecla=(window.event)?event.keyCode:e.which;
    if((tecla>47 && tecla<58)) return true;
    else{
        if (tecla==8 || tecla==0) return true;
        else  return false;
    }
}

$( "[data-list='collapse']" ).click(function () {
    if(empty($( this ).parent().attr('class'))){
        $(this).parent().attr('class','active');
        $(this).parent().css({"border-left":"none","border-right":"4px solid #19AA8D"});
        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360deg)","transform":" rotate(360deg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
    }else{
        $(this).parent().removeAttr('class');
        $(this).children(".fa-th-large").removeAttr('style');
        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360reg)","transform":" rotate(360reg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
        $(this).parent().css({"border-right":"none"});
    }
})

$("[role='open-adm']").click(function () {
    $("[role='adm']").css({"display":"block"});
    $(".bounceInRight").css({"opacity":"10"});
});


function initAnexos(body){

    $(body).find('.galeria').each(function(){
        var img = [];
        if($(this).attr('link-img').length != 0){
            img = $(this).attr('link-img').split(',')
        }
        $(this).fileinput({
            language: "pt-BR",
            msgSelected:'Lista de Arquivos',
            dropZoneEnabled:false,
            showRemove: false,
            showUpload: false,
            showCancel: false,
            showClose: false,
            msgPlaceholder:'Lista esta Vazia',
            uploadTitle:'Lista de Arquivos',
            dropZoneTitle:'Vizualizador de Imagens',
            maxFileCount: 1,
            overwriteInitial: false,
            showBrowse: false,
            initialPreview:img,
            initialPreviewConfig: JSON.parse($(this).attr('data-object')),
            initialPreviewAsData: true,
            fileActionSettings: {
                showDrag: true,
                showZoom: true,
                showUpload: false,
                showRemove:false
            }
        });

    });
}
$(document).ready(function () {

    $('.input-file-edit').each(function () {
        var img = [];
        if($(this).attr('link-img').length != 0){
            img = $(this).attr('link-img').split(',')
        }
        $(this).fileinput({
            language: "pt-BR",
            maxFileCount: 1,
            overwriteInitial: true,
            initialPreview: img,
            previewFileType: 'any',
            initialPreviewConfig: [JSON.parse($(this).attr('data-object'))],
            layoutTemplates: {
                main1: "{preview}\n" +
                    "<div class=\'input-group {class}\'>\n" +
                    "   <div class=\'input-group-btn\'>\n" +
                    "       {browse}\n" +
                    "       {remove}\n" +
                    "   </div>\n" +
                    "   {caption}\n" +
                    "</div>"
            },
            initialPreviewAsData: true
        }).on('filebatchselected', function (event, files) {
            $('#caminho').val('');
        }).on("filebeforedelete", function () {
            return new Promise(function (resolve, reject) {
                BootstrapDialog.show({
                    message: 'Certeza que deseja apagar este anexo?',
                    type: BootstrapDialog.TYPE_WARNING,
                    buttons: [
                        {
                            label: 'Confirmar',
                            action: function (dialogRef) {
                                resolve();
                                $('#caminho').val('');
                                dialogRef.close();
                            }
                        },
                        {
                            label: 'cancelar',
                            action: function (dialog) {
                                reject();
                                dialogRef.close();
                            }
                        }
                    ]
                });

            });
        });
    });
});