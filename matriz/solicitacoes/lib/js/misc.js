prevent_ajax_view = true;

$(document).ready(function() {
    $('button[data-dismiss="modal"]').click(function() {
        //history.go(-1);
    });

    modallink = null;

    $(document).on('hidden.bs.modal', function(e) {
        $(e.target).removeData('bs.modal');
        //$('#modal .modal-content').html('');
    });

    $('body').on('click', 'a[data-toggle=modal]', function (event) {
        event.preventDefault();
        modalharef = $(this).attr('href');
        if ($(this).attr('data-href')) {
            modalharef = $(this).attr('data-href');
            modalelement = this;
        }
        if (modalharef.indexOf("?") == -1)
            modalharef += '?'
        modalharef += '&ajax=true';
        $(this).attr('href', modalharef);
        if (modalharef.indexOf("first") != -1) {
            Acrescentar(modalharef);
        }
        if (!$(this).attr('data-target')) {
            $(this).attr('data-target', '#modal');
        }
    });

    moment.locale('pt-Br');
    initeComponentes(this);

    $.validator.addMethod("greaterThanZero", function(value, element) {
        if ($.isFunction($.fn.unmask)){
            return this.optional(element) || ((parseFloat(value) > 0) || $(element).unmask() > 0);
        }else{
            return this.optional(element) || (parseFloat(value) > 0);
        }
    }, "Valor precisa ser maior que zero");

    $.extend($.validator.methods, {
        date: function (value, element) {
            return this.optional(element) || /^\d\d?\/\d\d?\/\d\d\d?\d?$/.test(value);
        },
        number: function (value, element) {
            return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
        }
    });
    
    $.fn.extend({
        loadGrid: function (url, args, selector, callback) {
            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';
            var aux = 0;
            if (!empty(args)) {
                $.each(args, function (key, value) {
                    if (aux == 0) {
                        url += '/?' + key + '=' + value;
                    } else {
                        url += '&' + key + '=' + value;
                    }
                    aux = 1;
                });
            }
            url += ' ' + selector;
            return this.each(function () {
                if ($.isFunction(callback)) {
                    $(this).load(url, callback);
                } else {
                    $(this).load(url);

                }
            });
        },
        loadNewGrid: function (url, args, selector, callback) {
            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';
            var n = url.indexOf("?");
            if (n > -1) {
                url += '&'+args;
            }else{
                url += '/?'+args;
            }
            url += ' ' + selector;
            return this.each(function () {
                if ($.isFunction(callback)) {
                    $(this).load(url, callback);
                } else {
                    $(this).load(url);
                }
            });
        },

        //Função nova

        loadPostGrid: function (url, form, selector, callback) {
            var ths = this
            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';
            var n = url.indexOf("?");
            if (n > -1) {
                url += '&';
            } else {
                url += '/?';
            }
            url += '' + selector;

            this.each(function () {
                if ($.isFunction(callback)) {
                    $.post(url, form, function (dados) {
                        var htm = $(dados).find(selector).html();
                        ths.html(htm)
                        carregar()
                    });
                } else {
                    return $.post(url, form, function (dados) {
                        var htm = $(dados).find(selector).html();
                        ths.html(htm)
                        carregar()
                    });
                }
            });

        }

    });

    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            error.prepend($('<i class="fa fa-exclamation fa-lg"></i> '));
            if(element.parent('.input-group').length) {
                error.insertBefore(element.parent());
            } else {
                error.insertBefore(element);
            }
        }
    });

    $.extend({
        Report: function(report, args){
            var url = root + '/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=' + report;
            $.each(args, function(key, value) {
                url += '&'+ key + '=' + encodeURIComponent(value);
            });

            window.open(url, '_blank');
        },
        ReportPost: function(report, args){
            var url = root + '/Relatorios/index.php';
            var form = '<input type="hidden" name="stimulsoft_client_key" value="ViewerFx"">';
            form += '<input type="hidden" name="stimulsoft_report_key" value="'+report+'">';
            form += '<input type="submit" id="send-report">';
            $.each(args, function(key, value ) {
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });
            //$('<form action="' + url + '" method="POST" target="_blank">' + form + '</form>').submit();
            $('.area-report').append('<form action="' + url + '" method="POST" target="_blank">' + form + '</form>');
            $('#send-report').click();
            $('.area-report').html('');
        }
    });
    
   
});

//Funções tela filtros

$.fn.extend({
    serializeJSON: function (exclude) {
        exclude || (exclude = []);
        return _.reduce(this.serializeArray(), function (hash, pair) {
            pair.value && !(pair.name in exclude) && (hash[pair.name] = pair.value);
            return hash;
        }, {});
    }
});

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};