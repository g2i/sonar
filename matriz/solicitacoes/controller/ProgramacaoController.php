<?php

final class ProgramacaoController extends AppController
{
    # página inicial do módulo Programacao

    function indexContasReceber()
    {
        $this->setTitle('Programacao');
    }

    # lista de Programacaos
    # renderiza a visão /view/Programacao/all.php

    function contasReceber()
    {
        $this->setTitle('Programacao');

        $this->set('search',null);
        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $a->setOrder('nome');
        $this->set('Fornecedor', Fornecedor::getList($a));

        $k = new Criteria();
        $k->addCondition('status', '=', 1);
        $contabilidadeCombo = Contabilidade::getList($k);
        $this->set('contabilidadeCombo', $contabilidadeCombo);

        $this->set('PlanoContas', Planocontas::TipoPlano(1));

        $aa = new Criteria();
        $aa->addCondition('status', '=', 1);
        $aa->setOrder('nome');
        $this->set('Periodicidade', Periodicidade::getList($aa));
    }

    function getContasReceber(){
        try {
            $page = (int)$_POST['current'];
            $sort = !empty($_REQUEST['sort']) ? json_decode($_REQUEST['sort']) : '';

            $sqlCount = "SELECT count(*) AS count FROM programacao AS p
                         WHERE p.tipo = 'A receber'";

            if(Config::get('origem')==1) {
                $sql = "SELECT p.id, f.nome AS cliente, pl.nome AS plano, p.diaVencimento AS dia,
                    c.nome AS empresa, pe.nome AS periodo, p.valorParcela AS valor,
                    s.descricao AS status FROM programacao AS p
                    LEFT JOIN alunos AS f ON (f.id = p.idCliente)
                    LEFT JOIN planocontas AS pl ON (pl.id = p.idPlanoContas)
                    LEFT JOIN classificacaocontas cl ON (cl.id = p.classificacao)
                    LEFT JOIN contabilidade AS c ON (c.id = p.contabilidade)
                    LEFT JOIN periodicidade AS pe ON (pe.id = p.periodicidade)
                    LEFT JOIN status AS s ON (s.id = p.status)
                    WHERE p.tipo = 'A receber'  AND cl.tipo = 1";
            }else{
                $sql = "SELECT p.id, f.nome as cliente, pl.nome AS plano,
                    c.nome AS empresa, pe.nome AS periodo, p.valorParcela AS valor,
                    s.descricao AS status, p.data_documento,p.primeiro_vencimento FROM programacao AS p
                    LEFT JOIN cliente AS f ON (f.id = p.idCliente)
                    LEFT JOIN planocontas AS pl ON (pl.id = p.idPlanoContas)
                    LEFT JOIN contabilidade AS c ON (c.id = p.contabilidade)
                    LEFT JOIN periodicidade AS pe ON (pe.id = p.periodicidade)
                    LEFT JOIN status AS s ON (s.id = p.status)
                    WHERE p.tipo = 'A receber'";
            }

            if (!empty($_POST['planoContasOpt']) && $_POST['planoContasOpt'] != 0) {
                $sqlCount .= " AND p.idPlanoContas = :planoContasOpt";
                $sql .= " AND p.idPlanoContas = :planoContasOpt";
            }
            if (!empty($_POST['periodicidadeOpt']) && $_POST['periodicidadeOpt'] != 0) {
                $sqlCount .= " AND p.periodicidade = :periodicidadeOpt";
                $sql .= " AND p.periodicidade = :periodicidadeOpt";
            }
            if (!empty($_POST['cliente']) && $_POST['cliente'] != 0) {
                $sqlCount .= " AND p.idCliente = :cliente";
                $sql .= " AND p.idCliente = :cliente";
            }
            if (!empty($_POST['contabilidade']) && $_POST['contabilidade'] != 0) {
                $sqlCount .= " AND p.contabilidade = :contabilidade";
                $sql .= " AND p.contabilidade = :contabilidade";
            }

            $sortCount = count($sort);
            if($sortCount > 0){
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($_POST['planoContasOpt']) && $_POST['planoContasOpt'] != 0) {
                $db->bind(":planoContasOpt", $_POST['planoContasOpt']);
            }

            if (!empty($_POST['periodicidadeOpt']) && $_POST['periodicidadeOpt'] != 0) {
                $db->bind(":periodicidadeOpt", $_POST['periodicidadeOpt']);
            }

            if (!empty($_POST['cliente']) && $_POST['cliente'] != 0) {
                $db->bind(":cliente", $_POST['cliente']);
            }

            if (!empty($_POST['contabilidade']) && $_POST['contabilidade'] != 0) {
                $db->bind(":contabilidade", $_POST['contabilidade']);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($_POST['planoContasOpt']) && $_POST['planoContasOpt'] != 0) {
                $db->bind(":planoContasOpt", $_POST['planoContasOpt']);
            }

            if (!empty($_POST['periodicidadeOpt']) && $_POST['periodicidadeOpt'] != 0) {
                $db->bind(":periodicidadeOpt", $_POST['periodicidadeOpt']);
            }

            if (!empty($_POST['fornecedorOpt']) && $_POST['fornecedorOpt'] != 0) {
                $db->bind(":fornecedorOpt", $_POST['fornecedorOpt']);
            }

            if (!empty($_POST['contabilidade']) && $_POST['contabilidade'] != 0) {
                $db->bind(":contabilidade", $_POST['contabilidade']);
            }

            $dados = $db->getResults();

            $valorTotal = 0;
            foreach($dados AS $d){
                $valorTotal += $d->valor;
            }

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            $ret["valorTotal"] = $valorTotal;

            echo json_encode($ret);

        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    # visualiza um(a) Programacao
    # renderiza a visão /view/Programacao/view.php

    function viewContasReceber()
    {
        try {
            $this->set('Programacao', new Programacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Programacao', 'contasReceber');
        }
    }

    # formulário de cadastro de Programacao
    # renderiza a visão /view/Programacao/add.php

    function addContasReceber()
    {
        $this->setTitle('Adicionar Programacao');
        $ordNome = new Criteria();
        $ordNome->setOrder('nome');

        $this->set('Planocontas', Planocontas::TipoPlano(1));

        $this->set('Contabilidades', Contabilidade::getList($ordNome));

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        //$m->addSqlConditions('(usado = "Contas Receber" OR usado = "Ambas")');
        $this->set("tipoDocumento", Tipo_documento::getList($m));

        $mm = new Criteria();
        $mm->addCondition("status_id", "=", 1);
        //$mm->addSqlConditions('(usado = "Contas Receber" OR usado = "Ambas")');
        $this->set("tipoPagamento", Tipo_pagamento::getList($mm));

        $cliente = new Cliente();
        $cliente = $cliente->comboboxesCliente();
        $this->set('Cliente', $cliente);

        $k = new Criteria();
        $k->setOrder("nome");
        $k->addCondition("status_id", "=", 1);
        $correcaoMonetaria = CorrecaoMonetaria::getList($k);
        $this->set("correcaoMonetaria", $correcaoMonetaria);

        $l = new Criteria();
        $l->addCondition("status", "=", "1");
        $l->setOrder('nome');
        $ll = new Criteria();
        $ll->addCondition("status", "=", "1");
        $ll->setOrder('nome');
        $this->set("Deducoes", Deducoes::getList($ll));
        $this->set('Periodicidade', Periodicidade::getList($l));
    }

    # recebe os dados enviados via post do cadastro de Programacao
    # (true)redireciona ou (false) renderiza a visão /view/Programacao/add.php

    function post_addContasReceber()
    {
        $this->setTitle('Adicionar Programacao');
        $Programacao = new Programacao();
        $this->set('Programacao', $Programacao);
        try {
            if (!empty($_POST['valorParcela'])) {
                $_POST['valorParcela'] = getAmount($_POST['valorParcela']);
            }
            $_POST['status'] = 1;
            $_POST['tipo'] = "A receber";
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['idCliente'] == 0) {
                $_POST['idCliente'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (empty($_POST['correcaoMonetaria'])) {
                $_POST['correcaoMonetaria'] = 0;
            }

            $_POST['primeiro_vencimento'] = convertDataBR4SQL($_POST['primeiro_vencimento']);
            $_POST['data_documento'] = convertDataBR4SQL($_POST['data_documento']);
            $Programacao->save($_POST);
            if($_POST['rateio_automatico']==2){
                $this->go('Programacao', 'editContasReceber',array('id' => $Programacao->id));
            }else{
                $pl = new Criteria();
                $pl->addCondition('plano_contas','=',$_POST['idPlanoContas']);
                $plano_centro = Plano_centrocusto::getList($pl);
                foreach ($plano_centro as $pa) {
                    $rateio = new Rateio_programacao();
                    $rateio->programacao = $Programacao->id;
                    $rateio->valor = $Programacao->valorParcela*($pa->porcentagem/100);
                    $rateio->observacao = "Rateio automático";
                    $rateio->empresa= $pa->empresa;
                    $rateio->centro_custo= $pa->centro_custo;
                    $rateio->status= 1;
                    $rateio->data_documento = $Programacao->data_documento;
                    $rateio->plano_contas_id = $Programacao->idPlanoContas;
                    $rateio->cliente_id = $Programacao->idCliente;
                    $rateio->save();
                }
            }
            $this->go('Programacao', 'editContasReceber',array('id' => $Programacao->id));

            new Msg(__('Programacao cadastrado com sucesso'));
            if (!empty($_POST['deducao'])) {
                if ($_POST['deducao'] == 1) {
                    $this->go('Programacao', 'editContasReceber', array('id' => $Programacao->id));
                }
            } else {
                $this->go('Programacao', 'contasReceber');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # formulário de edição de Programacao
    # renderiza a visão /view/Programacao/edit.php

    function editContasReceber()
    {
        $this->setTitle('Editar Programacao');
        try {
            $ordNome = new Criteria();
            $ordNome->setOrder('nome');
            $Programscao =new Programacao((int)$this->getParam('id'));
            $this->set('Programacao', $Programscao);

            $this->set('Planocontas', Planocontas::TipoPlano(1));

            $this->set('Contabilidades', Contabilidade::getList($ordNome));

            $m = new Criteria();
            $m->addCondition("status_id", "=", 1);
            $mm = new Criteria();
            $mm->addCondition("status_id", "=", 1);
            $this->set("tipoPagamento", Tipo_pagamento::getList($mm));
            $this->set("tipoDocumento", Tipo_documento::getList($m));

            $this->set('clienteSelect', new Cliente($Programscao->idCliente));

            $l = new Criteria();
            $l->addCondition("status", "=", "1");
            $l->setOrder('nome');
            $this->set('Periodicidade', Periodicidade::getList($l));

            $c = new Criteria();
            $c->addCondition("programacao_id", "=", (int)$this->getParam('id'));
            $deducao = DeducoesProgramacao::getList($c);
            $this->set("Deducao", $deducao);

            $k = new Criteria();
            $k->setOrder("nome");
            $k->addCondition("status_id", "=", 1);
            $correcaoMonetaria = CorrecaoMonetaria::getList($k);
            $this->set("correcaoMonetaria", $correcaoMonetaria);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Programacao', 'contasReceber');
        }
    }

    # recebe os dados enviados via post da edição de Programacao
    # (true)redireciona ou (false) renderiza a visão /view/Programacao/edit.php

    function post_editContasReceber()
    {
        $this->setTitle('Editar Programacao');
        $Programacao = new Programacao((int)$_POST['id']);

        try {
            $this->set('Programacao', $Programacao);
            if (!empty($_POST['valorParcela'])) {
                $_POST['valorParcela'] = getAmount($_POST['valorParcela']);
            }
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['idCliente'] == 0) {
                $_POST['idCliente'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (empty($_POST['tipoDocumento'])) {
                $_POST['tipoDocumento'] = 4;
            }
            if (empty($_POST['tipoPagamento'])) {
                $_POST['tipoPagamento'] = 4;
            }

            $_POST['primeiro_vencimento'] = convertDataBR4SQL($_POST['primeiro_vencimento']);
            $_POST['data_documento'] = convertDataBR4SQL($_POST['data_documento']);
            if ($Programacao->save($_POST)) {
                //ATUALIZA GERADOS -------
                if ($Programacao->tipo == "A pagar") {
                    $a = new Criteria();
                    $a->addCondition('idProgramacao', '=', $Programacao->id);
                    $a->addCondition('vencimento', '>', date("Y-m-d"));

                    $contaspagar = Contaspagar::getList($a);

                    foreach ($contaspagar as $l) {
                        $aux = new Contaspagar($l->id);
                        $aux->idFornecedor = $Programacao->idFornecedor;
                        $aux->idPlanoContas = $Programacao->idPlanoContas;
                        $aux->contabilidade = $Programacao->contabilidade;
                        $aux->complemento = $Programacao->complemento;
                        $aux->valor = $Programacao->valorParcela;
                        $aux->valorBruto = $Programacao->valorParcela;
                        $aux->tipoPagamento = $Programacao->tipoPagamento;
                        $aux->tipoDocumento = $Programacao->tipoDocumento;

                        $aux->save();

                    }

                } else if ($Programacao->tipo == "A receber") {
                    $a = new Criteria();
                    $a->addCondition('idProgramacao', '=', $Programacao->id);
                    $a->addCondition('vencimento', '>', date("Y-m-d"));

                    $contasreceber = Contasreceber::getList($a);

                    foreach ($contasreceber as $l) {
                        $aux = new Contasreceber($l->id);
                        $aux->idCliente = $Programacao->idCliente;
                        $aux->idPlanoContas = $Programacao->idPlanoContas;
                        $aux->contabilidade = $Programacao->contabilidade;
                        $aux->complemento = $Programacao->complemento;
                        $aux->valor = $Programacao->valorParcela;
                        $aux->valorBruto = $Programacao->valorParcela;
                        $aux->tipoPagamento = $Programacao->tipoPagamento;
                        $aux->tipoDocumento = $Programacao->tipoDocumento;

                        $aux->save();
                    }
                    //------------------------
                }
            }
            new Msg(__('Programacao atualizado com sucesso'));
            $this->go('Programacao', 'editContasReceber', array('id' => $Programacao->id));
        } catch
        (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
            $this->go('Programacao', 'editContasReceber', array('id' => $Programacao->id));
        }
    }

    function indexContasPagar()
    {
        $this->setTitle('Programacao');
    }

    # lista de Programacaos
    # renderiza a visão /view/Programacao/all.php

    function contasPagar()
    {
        $this->setTitle('Programacao');

        $this->set('search',null);
        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $a->setOrder('nome');
        $this->set('Fornecedor', Fornecedor::getList($a));

        $k = new Criteria();
        $k->addCondition('status', '=', 1);
        $contabilidadeCombo = Contabilidade::getList($k);
        $this->set('contabilidadeCombo', $contabilidadeCombo);

        $this->set('PlanoContas', Planocontas::TipoPlano(2));

        $aa = new Criteria();
        $aa->addCondition('status', '=', 1);
        $aa->setOrder('nome');
        $this->set('Periodicidade', Periodicidade::getList($aa));
    }

    function getContasPagar(){
        try {
            $page = (int)$_POST['current'];
            $sort = !empty($_REQUEST['sort']) ? json_decode($_REQUEST['sort']) : '';

            $sqlCount = "SELECT count(*) AS count FROM programacao AS p
                         WHERE p.tipo = 'A pagar'";

            $sql = "SELECT p.id, f.nome AS credor, pl.nome AS plano, p.diaVencimento AS dia,
                    c.nome AS empresa, pe.nome AS periodo, p.valorParcela AS valor,
                    s.descricao AS status,p.data_documento,p.primeiro_vencimento FROM programacao AS p
                    LEFT JOIN fornecedor AS f ON (f.id = p.idFornecedor)
                    LEFT JOIN planocontas AS pl ON (pl.id = p.idPlanoContas)
                     LEFT JOIN classificacaocontas cl ON (cl.id = pl.classificacao)
                    LEFT JOIN contabilidade AS c ON (c.id = p.contabilidade)
                    LEFT JOIN periodicidade AS pe ON (pe.id = p.periodicidade)
                    LEFT JOIN status AS s ON (s.id = p.status)
                    WHERE p.tipo = 'A pagar' AND cl.tipo = 2 ";

            if (!empty($_POST['planoContasOpt']) && $_POST['planoContasOpt'] != 0) {
                $sqlCount .= " AND p.idPlanoContas = :planoContasOpt";
                $sql .= " AND p.idPlanoContas = :planoContasOpt";
            }
            if (!empty($_POST['periodicidadeOpt']) && $_POST['periodicidadeOpt'] != 0) {
                $sqlCount .= " AND p.periodicidade = :periodicidadeOpt";
                $sql .= " AND p.periodicidade = :periodicidadeOpt";
            }
            if (!empty($_POST['fornecedorOpt']) && $_POST['fornecedorOpt'] != 0) {
                $sqlCount .= " AND p.idFornecedor = :fornecedorOpt";
                $sql .= " AND p.idFornecedor = :fornecedorOpt";
            }
            if (!empty($_POST['contabilidade']) && $_POST['contabilidade'] != 0) {
                $sqlCount .= " AND p.contabilidade = :contabilidade";
                $sql .= " AND p.contabilidade = :contabilidade";
            }

            $sortCount = count($sort);
            if($sortCount > 0){
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($_POST['planoContasOpt']) && $_POST['planoContasOpt'] != 0) {
                $db->bind(":planoContasOpt", $_POST['planoContasOpt']);
            }

            if (!empty($_POST['periodicidadeOpt']) && $_POST['periodicidadeOpt'] != 0) {
                $db->bind(":periodicidadeOpt", $_POST['periodicidadeOpt']);
            }

            if (!empty($_POST['fornecedorOpt']) && $_POST['fornecedorOpt'] != 0) {
                $db->bind(":fornecedorOpt", $_POST['fornecedorOpt']);
            }

            if (!empty($_POST['contabilidade']) && $_POST['contabilidade'] != 0) {
                $db->bind(":contabilidade", $_POST['contabilidade']);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($_POST['planoContasOpt']) && $_POST['planoContasOpt'] != 0) {
                $db->bind(":planoContasOpt", $_POST['planoContasOpt']);
            }

            if (!empty($_POST['periodicidadeOpt']) && $_POST['periodicidadeOpt'] != 0) {
                $db->bind(":periodicidadeOpt", $_POST['periodicidadeOpt']);
            }

            if (!empty($_POST['fornecedorOpt']) && $_POST['fornecedorOpt'] != 0) {
                $db->bind(":fornecedorOpt", $_POST['fornecedorOpt']);
            }

            if (!empty($_POST['contabilidade']) && $_POST['contabilidade'] != 0) {
                $db->bind(":contabilidade", $_POST['contabilidade']);
            }

            $dados = $db->getResults();

            $valorTotal = 0;
            foreach($dados AS $d){
                $valorTotal += $d->valor;
            }

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            $ret["valorTotal"] = $valorTotal;

            echo json_encode($ret);

        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    # visualiza um(a) Programacao
    # renderiza a visão /view/Programacao/view.php

    function viewContasPagar()
    {
        try {
            $this->set('Programacao', new Programacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Programacao', 'contasPagar');
        }
    }

    # formulário de cadastro de Programacao
    # renderiza a visão /view/Programacao/add.php

    function addContasPagar()
    {
        $this->setTitle('Adicionar Programacao');
        $ordNome = new Criteria();
        $ordNome->setOrder('nome');
        $this->set('Programacao', new Programacao);


        $this->set('Planocontas', Planocontas::TipoPlano(2));

        $this->set('Contabilidades', Contabilidade::getList($ordNome));

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        //$m->addSqlConditions('(usado = "Contas Pagar" OR usado = "Ambas")');
        $this->set("tipoDocumento", Tipo_documento::getList($m));

        $mm = new Criteria();
        $mm->addCondition("status_id", "=", 1);
        //$mm->addSqlConditions('(usado = "Contas Pagar" OR usado = "Ambas")');
        $this->set("tipoPagamento", Tipo_pagamento::getList($mm));

        $this->set('forncedorSelect', new Fornecedor());

        $l = new Criteria();
        $l->addCondition("status", "=", "1");
        $l->setOrder('nome');
        $this->set('Periodicidade', Periodicidade::getList($l));
    }

    # recebe os dados enviados via post do cadastro de Programacao
    # (true)redireciona ou (false) renderiza a visão /view/Programacao/add.php

    function post_addContasPagar()
    {
        $this->setTitle('Adicionar Programacao');
        $Programacao = new Programacao();
        $this->set('Programacao', $Programacao);
        try {
            if (!empty($_POST['valorParcela'])) {
                $_POST['valorParcela'] = getAmount($_POST['valorParcela']);
            }
            $_POST['status'] = 1;
            $_POST['tipo'] = "A pagar";
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            $_POST['primeiro_vencimento'] = convertDataBR4SQL($_POST['primeiro_vencimento']);
            $_POST['data_documento'] = convertDataBR4SQL($_POST['data_documento']);
            $Programacao->save($_POST);
            if($_POST['rateio_automatico']==2){
                $this->go('Programacao', 'editContasPagar',array('id' => $Programacao->id));

            }else{
                $pl = new Criteria();
                $pl->addCondition('plano_contas','=',$_POST['idPlanoContas']);
                $plano_centro = Plano_centrocusto::getList($pl);
                foreach ($plano_centro as $pa){
                    $rateio = new Rateio_programacao();
                    $rateio->programacao = $Programacao->id;
                    $rateio->valor = $Programacao->valorParcela*($pa->porcentagem/100);
                    $rateio->observacao = "Rateio automático";
                    $rateio->empresa= $pa->empresa;
                    $rateio->centro_custo= $pa->centro_custo;
                    $rateio->status= 1;
                    $rateio->data_documento = $Programacao->data_documento;
                    $rateio->plano_contas_id = $Programacao->idPlanoContas;
                    $rateio->fornecedor_id = $Programacao->idFornecedor;
                    $rateio->save();
                }
            }
            new Msg(__('Programacao cadastrado com sucesso'));
            $this->go('Programacao', 'editContasPagar',array('id' => $Programacao->id));

            if (!empty($_POST['deducao'])) {
                if ($_POST['deducao'] == 1) {
                    $this->go('Programacao', 'editContasPagar', array('id' => $Programacao->id));
                }
            } else {
                $this->go('Programacao', 'contasPagar');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # formulário de edição de Programacao
    # renderiza a visão /view/Programacao/edit.php

    function editContasPagar()
    {
        $this->setTitle('Editar Programacao');
        try {
            $ordNome = new Criteria();
            $ordNome->setOrder('nome');

            $Programacao = new Programacao((int)$this->getParam('id'));
            $this->set('Programacao', $Programacao);

            $this->set('Planocontas', Planocontas::TipoPlano(2));

            $this->set('Contabilidades', Contabilidade::getList($ordNome));

            $m = new Criteria();
            $m->addCondition("status_id", "=", 1);
            $mm = new Criteria();
            $mm->addCondition("status_id", "=", 1);
            $this->set("tipoPagamento", Tipo_pagamento::getList($mm));
            $this->set("tipoDocumento", Tipo_documento::getList($m));

            $this->set('forncedorSelect', new Fornecedor($Programacao->idFornecedor));

            $l = new Criteria();
            $l->addCondition("status", "=", "1");
            $l->setOrder('nome');
            $this->set('Periodicidade', Periodicidade::getList($l));

            $ll = new Criteria();
            $ll->addCondition("status", "=", "1");
            $ll->setOrder('nome');
            $this->set("Deducoes", Deducoes::getList($ll));

            $this->set("Deducao", DeducoesProgramacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Programacao', 'contasPagar');
        }
    }

    # recebe os dados enviados via post da edição de Programacao
    # (true)redireciona ou (false) renderiza a visão /view/Programacao/edit.php

    function post_editContasPagar()
    {
        $this->setTitle('Editar Programacao');
        try {
            $Programacao = new Programacao((int)$_POST['id']);
            $this->set('Programacao', $Programacao);
            if (!empty($_POST['valorParcela'])) {
                $_POST['valorParcela'] = getAmount($_POST['valorParcela']);
            }
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            $_POST['primeiro_vencimento'] = convertDataBR4SQL($_POST['primeiro_vencimento']);
            $_POST['data_documento'] = convertDataBR4SQL($_POST['data_documento']);

            $Programacao->save($_POST);
            new Msg(__('Programacao atualizado com sucesso'));
            $this->go('Programacao', 'contasPagar');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Recebe o id via post e exclui um(a) Programacao
    # redireciona para Programacao/all

    function delete()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Programacao = new Programacao((int)$_POST['id']);

            //Ao excluir a programação faz uma pesquisa das contas que não foram pagas com vencimento superior a data de hoje
            // e seta seu status como excluido

            $c = new Criteria();
            $c->addCondition('idProgramacao', '=', (int)$_POST['id']);
            $c->addSqlConditions('dataPagamento IS NULL');
            $c->addCondition('vencimento', '>=', date("Y-m-d"));
            $Contas = Contaspagar::getList($c);

            foreach($Contas as $ct) {
                $ct->status = 3;
                $ct->save();
            }

            $Programacao->delete();
            $ret['result'] = true;
            $ret['msg'] = 'Programa&ccedil;&atilde;o removida com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao remover programa&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function addDeducaoReceber()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_addDeducaoReceber()
    {
        try {
            $deducao = new DeducoesProgramacao();
            if (!empty($_POST['percentual'])) {
                $_POST['percentual'] = str_replace(",", ".", $_POST['percentual']);
            }
            $deducao->save($_POST);

            $this->go('Programacao', 'editContasReceber/id:' . $deducao->programacao_id);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function editDeducaoReceber()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));

            $this->set("DeducaoProgramacao", new DeducoesProgramacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_editDeducaoReceber()
    {
        try {
            $deducao = new DeducoesProgramacao((int)$_POST['id']);
            if (!empty($_POST['percentual'])) {
                $_POST['percentual'] = str_replace(",", ".", $_POST['percentual']);
            }
            $deducao->save($_POST);

            $this->go('Programacao', 'editContasReceber', array("id" => $deducao->programacao_id));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function addDeducaoPagar()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_addDeducaoPagar()
    {
        try {
            $deducao = new DeducoesProgramacao();
            if (!empty($_POST['valor'])) {
                $_POST['valor'] = str_replace(".", "", $_POST['valor']);
                $_POST['valor'] = str_replace(",", ".", $_POST['valor']);
            }
            $deducao->save($_POST);

            $this->go('Programacao', 'editContasPagar/id:' . $deducao->programacao_id);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function editDeducaoPagar()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));

            $this->set("DeducaoProgramacao", new DeducoesProgramacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_editDeducaoPagar()
    {
        try {
            $deducao = new DeducoesProgramacao((int)$_POST['id']);
            if (!empty($_POST['valor'])) {
                $_POST['valor'] = str_replace(".", "", $_POST['valor']);
                $_POST['valor'] = str_replace(",", ".", $_POST['valor']);
            }
            $deducao->save($_POST);

            $this->go('Programacao', 'editContasPagar', array("id" => $deducao->programacao_id));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_salvarDeducaoPagar()
    {
        try {
            $deducao = new DeducoesProgramacao((int)$_POST['id']);
            if (!empty($_POST['valor'])) {
                $_POST['valor'] = str_replace(".", "", $_POST['valor']);
                $_POST['valor'] = str_replace(",", ".", $_POST['valor']);
            }
            if ($deducao->save($_POST)) {
                echo '1';
            }
            $this->go('Programacao', 'editContasPagar', array("id" => $deducao->programacao_id));

        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            echo '0';
        }
    }
}
