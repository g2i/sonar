<?php

final class CorrecaoMonetariaController extends AppController {
    # página inicial do módulo CorrecaoMonetaria

    function index() {
        $this->setTitle('CorrecaoMonetaria');
    }

    # lista de CorrecaoMonetarias
    # renderiza a visão /view/CorrecaoMonetaria/all.php

    function all() {
        $this->setTitle('Corre&ccedil;&atilde;o Monetaria');
    }

    # visualiza um(a) CorrecaoMonetaria
    # renderiza a visão /view/CorrecaoMonetaria/view.php

    function view() {
        try {
            $this->set('CorrecaoMonetaria', new CorrecaoMonetaria((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('CorrecaoMonetaria', 'all');
        }
    }

    # formulário de cadastro de CorrecaoMonetaria
    # renderiza a visão /view/CorrecaoMonetaria/add.php

    function add() {
        $this->setTitle('Adicionar CorrecaoMonetaria');
        $this->set('CorrecaoMonetaria', new CorrecaoMonetaria);
        $c = new Criteria();
        $c->addCondition("id", "<>", 3);
        $this->set('Status', Status::getList($c));
    }

    # recebe os dados enviados via post do cadastro de CorrecaoMonetaria
    # (true)redireciona ou (false) renderiza a visão /view/CorrecaoMonetaria/add.php

    function post_add() {
        $this->setTitle('Adicionar CorrecaoMonetaria');
        $CorrecaoMonetaria = new CorrecaoMonetaria();
        $this->set('CorrecaoMonetaria', $CorrecaoMonetaria);
        try {
            $CorrecaoMonetaria->save($_POST);
            new Msg(__('CorrecaoMonetaria cadastrado com sucesso'));
            $this->go('CorrecaoMonetaria', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Status', Status::getList());
    }

    # formulário de edição de CorrecaoMonetaria
    # renderiza a visão /view/CorrecaoMonetaria/edit.php

    function edit() {
        $this->setTitle('Editar CorrecaoMonetaria');
        try {
            $this->set('CorrecaoMonetaria', new CorrecaoMonetaria((int) $this->getParam('id')));
            $c = new Criteria();
            $c->addCondition("id", "<>", 3);
            $this->set('Status', Status::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('CorrecaoMonetaria', 'all');
        }
    }

    # recebe os dados enviados via post da edição de CorrecaoMonetaria
    # (true)redireciona ou (false) renderiza a visão /view/CorrecaoMonetaria/edit.php

    function post_edit() {
        $this->setTitle('Editar CorrecaoMonetaria');
        try {
            $CorrecaoMonetaria = new CorrecaoMonetaria((int) $_POST['id']);
            $this->set('CorrecaoMonetaria', $CorrecaoMonetaria);
            $CorrecaoMonetaria->save($_POST);
            new Msg(__('CorrecaoMonetaria atualizado com sucesso'));
            $this->go('CorrecaoMonetaria', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status', Status::getList());
    }

    # Recebe o id via post e exclui um(a) CorrecaoMonetaria
    # redireciona para CorrecaoMonetaria/all

    function delete() {
        $ret = array();
        $ret['result'] = false;

        try {
            $CorrecaoMonetaria = new CorrecaoMonetaria((int) $_POST['id']);
            $CorrecaoMonetaria->status_id = 3;
            $CorrecaoMonetaria->save();
            $ret['result'] = true;
            $ret['msg'] = 'Corre&ccedil;&atilde;o monet&aacute;ria apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir corre&ccedil;&atilde;o monet&aacute;ria !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function getCorrecoes(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM correcaomonetaria AS c WHERE c.status_id <> 3";
            $sql = "SELECT c.id, c.nome, s.descricao AS status
                    FROM correcaomonetaria AS c INNER JOIN status AS s ON (s.id = c.status_id)
                    WHERE c.status_id <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND c.nome LIKE :termo";
                $sql .= " AND c.nome LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.nome";
                $sort[] = array('nome' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }

}
