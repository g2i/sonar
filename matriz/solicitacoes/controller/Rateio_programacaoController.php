<?php
final class Rateio_programacaoController extends AppController{

    # página inicial do módulo Rateio_programacao
    function index(){
        $this->setTitle('Rateio_programacao');
    }

    # lista de Rateio_programacaoes
    # renderiza a visão /view/Rateio_programacao/all.php
    function all(){
        $this->setTitle('Rateio_programacaoes');
        $p = new Paginate('Rateio_programacao', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (isset($_GET['search'])) {
            $c->addCondition('id', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if($this->getParam("programacao")){
            $c->addCondition('programacao','=',$this->getParam("programacao"));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rateio_programacaoes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rateio_programacao
    # renderiza a visão /view/Rateio_programacao/view.php
    function view(){
        try {
            $this->set('Rateio_programacao', new Rateio_programacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rateio_programacao', 'all');
        }
    }

    # formulário de cadastro de Rateio_programacao
    # renderiza a visão /view/Rateio_programacao/add.php
    function add(){
        $this->setTitle('Adicionar Rateio Programação');
        $this->set('Rateio_programacao', new Rateio_programacao);
        if($this->getParam('programacao')){
            $this->set('Programacao', new Programacao($this->getParam('programacao')));
        }else {
            $this->set('Programacao',  Programacao::getList());
        }
        $this->set('Contabilidades',  Contabilidade::getList());
        $this->set('Planocontas',  Planocontas::getList());
        $this->set('Fornecedor',  Fornecedor::getList());
        $this->set('Cliente',  Cliente::getList());
    }

    # recebe os dados enviados via post do cadastro de Rateio_programacao
    # (true)redireciona ou (false) renderiza a visão /view/Rateio_programacao/add.php
    function post_add(){
        $this->setTitle('Adicionar Rateio_programacao');
        $Rateio_programacao = new Rateio_programacao();
        $this->set('Rateio_programacao', $Rateio_programacao);
        $_POST['valor'] = str_replace(".", "", $_POST['valor']);
        $_POST['valor'] = str_replace(",", ".", $_POST['valor']);
        $_POST['status'] = 1;
        try {
            $Rateio_programacao->save($_POST);
            $Contas_pagar = new Programacao((int)$_POST['programacao']);
            $a = new Criteria();
            $a->addCondition('programacao','=',$_POST['programacao']);
            $rateio = Rateio_programacao::getList($a);
            $sum =0;
            foreach ($rateio as $r) {
                $sum+=$r->valor;
            }
            $total = $Contas_pagar->valorParcela-$sum;
            if(!empty($_POST['modal'])){
                echo $total;
                exit;
            }else{
                if($total>0){
                    new Msg(__("Lançamento Realizado com sucesso! Ainda faltam R$ ".$total." continue o rateio"));
                    $this->go('Rateio_programacao', 'all',array('programacao'=>$_POST['programacao']));
                }
            }
            new Msg(__('Operação realizada com sucesso'));
            $this->go('Rateio_programacao', 'all',array('programacao'=>$_POST['programacao']));

        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Programacao',  Programacao::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
    }

    # formulário de edição de Rateio_programacao
    # renderiza a visão /view/Rateio_programacao/edit.php
    function edit(){
        $this->setTitle('Editar Rateio_programacao');
        try {
            $this->set('Rateio_programacao', new Rateio_programacao((int) $this->getParam('id')));
            $this->set('Programacao',  Programacao::getList());
            $this->set('Contabilidades',  Contabilidade::getList());
            $this->set('Planocontas',  Planocontas::getList());
            $this->set('Fornecedor',  Fornecedor::getList());
            $this->set('Cliente',  Cliente::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rateio_programacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rateio_programacao
    # (true)redireciona ou (false) renderiza a visão /view/Rateio_programacao/edit.php
    function post_edit(){
        $this->setTitle('Editar Rateio_programacao');
        try {
            $Rateio_programacao = new Rateio_programacao((int) $_POST['id']);
            $this->set('Rateio_programacao', $Rateio_programacao);
            $Rateio_programacao->save($_POST);
            new Msg(__('Rateio_programacao atualizado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('Rateio_programacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Programacao',  Programacao::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
    }

    # Confirma a exclusão ou não de um(a) Rateio_programacao
    # renderiza a /view/Rateio_programacao/delete.php
    function delete(){
        $this->setTitle('Apagar Rateio programação');
        try {
            $this->set('Rateio_programacao', new Rateio_programacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rateio_programacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rateio_programacao
    # redireciona para Rateio_programacao/all
    function post_delete(){
        try {
            $Rateio_programacao = new Rateio_programacao((int) $_POST['id']);
            $Rateio_programacao->status=3;
            $Rateio_programacao->save();
            new Msg(__('Rateio de programação apagado com sucesso'), 1);
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rateio_programacao', 'all');
    }

    function checar_rateio(){
        $Contas_receber = new Programacao((int)$_POST['id']);
        $a = new Criteria();
        $a->addCondition('programacao','=',$_POST['id']);
        $rateio = Rateio_programacao::getList($a);
        $sum =0;
        foreach ($rateio as $r) {
            $sum+=$r->valor;
        }
        $total = $Contas_receber->valorParcela-$sum;
        echo $total;
        exit;
    }

    function checar_duplicidade(){
        $a = new Criteria();
        $a->addCondition('programacao','=',$_POST['id']);
        $a->addCondition('empresa','=',$_POST['empresa']);
        $a->addCondition('centro_custo','=',$_POST['centro']);
        $rateio = Rateio_programacao::getList($a);
        echo count($rateio);
        exit;
    }

}