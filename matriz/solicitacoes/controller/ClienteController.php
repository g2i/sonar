<?php

final class ClienteController extends AppController {

    function all() {
        $this->setTitle('Clientes');
    }

    # visualiza um(a) Fornecedor
    # renderiza a visão /view/Fornecedor/view.php

    function view() {
        try {
            $this->set('Cliente', new Cliente((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fornecedor', 'all');
        }
    }

    # formulário de cadastro de Fornecedor
    # renderiza a visão /view/Fornecedor/add.php

    function add() {
        $this->setTitle('Adicionar Cliente');
        $this->set('Cliente', new Cliente);
        $this->set('Status', Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor/add.php

    function post_add() {
        $this->setTitle('Adicionar Cliente');
        $Cliente = new Cliente();
        $this->set('Cliente', $Cliente);
        try {
            $Cliente->save($_POST);
            new Msg(__('Cliente cadastrado com sucesso'));
            if(!empty($_POST['origem'])){
                echo $Cliente->id;
                exit;
            }else{
                $this->go('Cliente', 'all');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Status', Status::getList());
    }

    # formulário de edição de Fornecedor
    # renderiza a visão /view/Fornecedor/edit.php

    function edit() {
        $this->setTitle('Editar Cliente');
        try {
            $this->set('Cliente', new Cliente((int) $this->getParam('id')));
            $this->set('Status', Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Fornecedor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor/edit.php

    function post_edit() {
        $this->setTitle('Editar Cliente');
        try {
            $Cliente = new Cliente((int) $_POST['id']);
            $this->set('Cliente', $Cliente);
            $Cliente->save($_POST);
            new Msg(__('Cliente atualizado com sucesso'));
            $this->go('Cliente', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status', Status::getList());
    }

    # Recebe o id via post e exclui um(a) Fornecedor
    # redireciona para Fornecedor/all

    function delete() {
        $ret = array();
        $ret['result'] = false;

        try {
            $Cliente = new Cliente((int) $_POST['id']);
            $Cliente->status=3;
            $Cliente->save();
            $ret['result'] = true;
            $ret['msg'] = 'Cliente apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir cliente !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function findClientes(){
        try {
            $termo = $this->getParam('termo');
            $size = (int)$this->getParam('size');
            $page = (int)$this->getParam('page');

            if(!isset($termo))
                $termo = '';

            if(!isset($size) || $size < 1)
                $size = 10;

            if(!isset($page) || $page < 1)
                $page = 1;

            $db = $this::getConn();
            if(Config::get('origem')==1) {
                $sqlCount = "SELECT count(*) AS count FROM `alunos` WHERE status=1 AND `nome` LIKE :termo";
                $sql = "SELECT `id`,`nome` FROM `alunos` WHERE status=1 AND `nome` LIKE :termo LIMIT :li OFFSET :off";
            }else if(Config::get('origem')==2){
                $sqlCount = "SELECT count(*) AS count FROM `cliente` WHERE status=1 AND `nome` LIKE :termo";
                $sql = "SELECT `id`,`nome` FROM `cliente` WHERE status=1 AND `nome` LIKE :termo LIMIT :li OFFSET :off";
            }else{
                $sqlCount = "SELECT count(*) AS count FROM `cliente` WHERE status=1 AND `cliente` LIKE :termo";
                $sql = "SELECT codigo as `id`,cliente as `nome` FROM `cliente` WHERE status=1 AND `cliente` LIKE :termo LIMIT :li OFFSET :off";
            }

            $db->query($sqlCount);
            $db->bind(":termo", $termo . "%", PDO::PARAM_STR);
            $db->execute();

            $ret = array();
            $ret["total"] = $db->getResults()[0]->count;
            $ret["dados"] = array();

            $db->query($sql);
            $db->bind(":termo", $termo . "%", PDO::PARAM_STR);
            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

            $dados = $db->getResults();

            foreach ($dados as $d) {
                $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
            }

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    function getClientes(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $origem = Config::get('origem');
            switch($origem){
                case 1:
                    $sqlCount = "SELECT count(*) AS count FROM `alunos` AS c WHERE c.status <> 3";
                    $sql = "SELECT c.id, c.nome, c.fone AS telefone, c.celular, c.email, s.descricao AS status
                            FROM alunos AS c INNER JOIN status AS s ON (s.id = c.status)
                            WHERE status <> 3";
                    break;

                case 2:
                    $sqlCount = "SELECT count(*) AS count FROM `cliente` AS c WHERE c.status <> 3";
                    $sql = "SELECT c.id, c.nome, c.telefone, c.celular, c.email, s.descricao AS status
                            FROM cliente AS c INNER JOIN status AS s ON (s.id = c.status)
                            WHERE status <> 3";
                    break;

                default:
                    $sqlCount = "SELECT count(*) AS count FROM `cliente` AS c WHERE c.status <> 3";
                    $sql = "SELECT c.codigo AS id, c.cliente AS nome, c.telefone, c.celular, c.email, s.descricao AS status
                            FROM cliente AS c INNER JOIN status AS s ON (s.id = c.status)
                            WHERE c.status <> 3";
                    break;
            }

            if (!empty($search)) {
                switch($origem) {
                    case 3:
                        $sqlCount .= " AND c.cliente LIKE :termo";
                        $sql .= " AND c.cliente` LIKE :termo";
                        break;

                    default:
                        $sqlCount .= " AND c.nome LIKE :termo";
                        $sql .= " AND c.nome LIKE :termo";
                        break;
                }
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY nome";
                $sort[] = array('nome' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }
}