<?php
final class DeducoesContaspagarController extends AppController{ 

    # página inicial do módulo DeducoesContaspagar
    function index(){
        $this->setTitle('DeducoesContaspagar');
    }

    # lista de DeducoesContaspagares
    # renderiza a visão /view/DeducoesContaspagar/all.php
    function all(){
        $this->setTitle('DeducoesContaspagares');
        $p = new Paginate('DeducoesContaspagar', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (isset($_GET['search'])) {
            $c->addCondition('id', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('DeducoesContaspagares', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) DeducoesContaspagar
    # renderiza a visão /view/DeducoesContaspagar/view.php
    function view(){
        try {
            $this->set('DeducoesContaspagar', new DeducoesContaspagar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('DeducoesContaspagar', 'all');
        }
    }

    # formulário de cadastro de DeducoesContaspagar
    # renderiza a visão /view/DeducoesContaspagar/add.php
    function add(){
        $this->setTitle('Adicionar DeducoesContaspagar');
        $this->set('DeducoesContaspagar', new DeducoesContaspagar);
        $this->set('Deducoes',  Deducoes::getList());
        $this->set('Contaspagares',  Contaspagar::getList());
    }

    # recebe os dados enviados via post do cadastro de DeducoesContaspagar
    # (true)redireciona ou (false) renderiza a visão /view/DeducoesContaspagar/add.php
    function post_add(){
        $this->setTitle('Adicionar DeducoesContaspagar');
        $DeducoesContaspagar = new DeducoesContaspagar();
        $this->set('DeducoesContaspagar', $DeducoesContaspagar);
        try {
            $DeducoesContaspagar->save($_POST);
            new Msg(__('DeducoesContaspagar cadastrado com sucesso'));
            $this->go('DeducoesContaspagar', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Deducoes',  Deducoes::getList());
        $this->set('Contaspagares',  Contaspagar::getList());
    }

    # formulário de edição de DeducoesContaspagar
    # renderiza a visão /view/DeducoesContaspagar/edit.php
    function edit(){
        $this->setTitle('Editar DeducoesContaspagar');
        try {
            $this->set('DeducoesContaspagar', new DeducoesContaspagar((int) $this->getParam('id')));
            $this->set('Deducoes',  Deducoes::getList());
            $this->set('Contaspagares',  Contaspagar::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('DeducoesContaspagar', 'all');
        }
    }

    # recebe os dados enviados via post da edição de DeducoesContaspagar
    # (true)redireciona ou (false) renderiza a visão /view/DeducoesContaspagar/edit.php
    function post_edit(){
        $this->setTitle('Editar DeducoesContaspagar');
        try {
            $DeducoesContaspagar = new DeducoesContaspagar((int) $_POST['id']);
            $this->set('DeducoesContaspagar', $DeducoesContaspagar);
            $DeducoesContaspagar->save($_POST);
            new Msg(__('DeducoesContaspagar atualizado com sucesso'));
            $this->go('DeducoesContaspagar', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Deducoes',  Deducoes::getList());
        $this->set('Contaspagares',  Contaspagar::getList());
    }

    # Confirma a exclusão ou não de um(a) DeducoesContaspagar
    # renderiza a /view/DeducoesContaspagar/delete.php
    function delete(){
        $this->setTitle('Apagar DeducoesContaspagar');
        try {
            $this->set('DeducoesContaspagar', new DeducoesContaspagar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('DeducoesContaspagar', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) DeducoesContaspagar
    # redireciona para DeducoesContaspagar/all
    function post_delete(){
        try {
            $DeducoesContaspagar = new DeducoesContaspagar((int) $_POST['id']);
            $DeducoesContaspagar->delete();
            new Msg(__('DeducoesContaspagar apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('DeducoesContaspagar', 'all');
    }

}