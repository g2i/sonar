<?php

final class ContasreceberController extends AppController
{
    # página inicial do módulo Contasreceber

    function index()
    {
        $this->setTitle('Contas a receber');
    }

    # lista de Contasreceberes
    # renderiza a visão /view/Contasreceber/all.php

    function all()
    {
        $this->setTitle('Contas a receber');

        $this->set('inicio', "01/".date("m").'/'.date("Y"));
        $this->set('fim', date("t/m/Y"));
        $cS = new Criteria();
        $cS->addCondition('id', '<>', 3);
        $this->set('StatusList', Status::getList($cS));

        $b = new Criteria();
        $b->addCondition('status', '=', '1');
        $b->setOrder('nome');
        $contabilidade = Contabilidade::getList($b);

        $this->set('idPlanoContas', Planocontas::TipoPlano(1));
        $this->set('contabilidade', $contabilidade);
    }

    # visualiza um(a) Contasreceber
    # renderiza a visão /view/Contasreceber/view.php

    function view()
    {
        try {
            $this->set('Contasreceber', new Contasreceber((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Contasreceber', 'all');
        }
    }

    # formulário de cadastro de Contasreceber
    # renderiza a visão /view/Contasreceber/add.php

    function add()
    {
        $this->setTitle('Adicionar Contas a receber');

        $ordNome = new Criteria();
        $ordNome->setOrder('nome');

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        $this->set("tipoPagamento", Tipo_pagamento::getList($m));
        $nn = new Criteria();
        $nn->addCondition("status_id", "=", 1);
        $this->set("tipoDocumento", Tipo_documento::getList($nn));


        $this->setTitle('Adicionar Contas a receber');
        $this->set('Planocontas', Planocontas::TipoPlano(1));
        $this->set('Contabilidades', Contabilidade::getList($ordNome));

        $n = new Criteria();
        $n->addCondition('id', '<>', '3');
        $this->set('Status', Status::getList($n));

        $cliente = new Cliente();
        $cliente = $cliente->comboboxesCliente();
        $this->set('Cliente', $cliente);

        $l = new Criteria();
        $l->addCondition("status", "=", "1");
        $l->setOrder('nome');
        $this->set('Periodicidade', Periodicidade::getList($l));

        $k = new Criteria();
        $k->addCondition("status_id", "=", "1");
        $k->setOrder('nome');
        $this->set('correcaoMonetaria', CorrecaoMonetaria::getList($k));
    }

    # recebe os dados enviados via post do cadastro de Contasreceber
    # (true)redireciona ou (false) renderiza a visão /view/Contasreceber/add.php

    function post_add()
    {
        $this->setTitle('Adicionar Contas a receber');
        $Contasreceber = new Contasreceber();
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Status', Status::getList());
        $this->set('Contabilidades', Contabilidade::getList());

        try {
            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount( $_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (@$_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if (@$_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if (@$_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['datafiscal'])) {
                $_POST['datafiscal'] = convertDataBR4SQL($_POST['datafiscal']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }

            $_POST['data_cadastro'] = date('Y-m-d H:i:s');
            $Contasreceber->save($_POST);
            new Msg(__('Contas á receber cadastrado com sucesso'));
            if($_POST['rateio_automatico']==2){
                $this->go('Contasreceber', 'edit', array('id' => $Contasreceber->id));
            }else{
                $pl = new Criteria();
                $pl->addCondition('plano_contas','=',$_POST['idPlanoContas']);
                $plano_centro = Plano_centrocusto::getList($pl);
                foreach ($plano_centro as $pa) {
                    $rateio = new Rateio_contasreceber();
                    $rateio->contas_receber = $Contasreceber->id;
                    $rateio->valor = $Contasreceber->valor*($pa->porcentagem/100);
                    $rateio->observacao = "Rateio automático";
                    $rateio->empresa= $pa->empresa;
                    $rateio->centro_custo= $pa->centro_custo;
                    $rateio->status= 1;
                    $rateio->data_documento = $Contasreceber->data;
                    $rateio->cliente_id = $Contasreceber->idCliente;
                    $rateio->plano_contas_id = $Contasreceber->idPlanoContas;
                    $rateio->save();
                }
            }
            $this->go('Contasreceber', 'edit', array('id' => $Contasreceber->id));
            if (!empty($_POST['deducao'])) {
                if ($_POST['deducao'] == 1) {
                    $this->go('Contasreceber', 'edit', array('id' => $Contasreceber->id));
                }
            } else {
                $this->go('Contasreceber', 'all');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # recebe os dados enviados via post do cadastro de Contasreceber
    # (true)redireciona ou (false) renderiza a visão /view/Contasreceber/add.php

    function post_addModal()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Contasreceber = new Contasreceber();

            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount( $_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (@$_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if (@$_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if (@$_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['datafiscal'])) {
                $_POST['datafiscal'] = convertDataBR4SQL($_POST['datafiscal']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }

            $_POST['data_cadastro'] = date('Y-m-d H:i:s');
            $Contasreceber->save($_POST);
            $plano = new Planocontas((int)$_POST['idPlanoContas']);

            if($plano->rateio == 2){
                $ret['conta'] = $Contasreceber->id;
            }else{
                $pl = new Criteria();
                $pl->addCondition('plano_contas','=',$_POST['idPlanoContas']);
                $plano_centro = Plano_centrocusto::getList($pl);
                foreach ($plano_centro as $pa) {
                    $rateio = new Rateio_contasreceber();
                    $rateio->contas_receber = $Contasreceber->id;
                    $rateio->valor = $Contasreceber->valor*($pa->porcentagem/100);
                    $rateio->observacao = "Rateio automático";
                    $rateio->empresa= $pa->empresa;
                    $rateio->centro_custo= $pa->centro_custo;
                    $rateio->status= 1;
                    $rateio->save();
                }
            }

            $ret['result'] = true;
            $ret['msg'] = 'Conta adicionada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao adicionar Conta !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    # formulário de edição de Contasreceber
    # renderiza a visão /view/Contasreceber/edit.php

    function edit()
    {
        $this->setTitle('Editar Contas a receber');


        ## REGISTRO A SER EDITADO
        $Contasreceber = new Contasreceber((int)$this->getParam('id'));
        $this->set('Contasreceber', $Contasreceber);

        $ordNome = new Criteria();
        $ordNome->setOrder('nome');
        $this->set('Planocontas', Planocontas::TipoPlano(1));
        $this->set('Contabilidades', Contabilidade::getList($ordNome));


        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        $nn = new Criteria();
        $nn->addCondition("status_id", "=", 1);
        $this->set("tipoPagamento", Tipo_pagamento::getList($m));
        $this->set("tipoDocumento", Tipo_documento::getList($nn));

        $n = new Criteria();
        $n->addCondition('id', '<>', '3');
        $this->set('Status', Status::getList($n));


        $this->set('clienteSelect', new Cliente($Contasreceber->idCliente));

        $l = new Criteria();
        $l->addCondition("status", "=", "1");
        $l->setOrder('nome');
        $this->set('Periodicidade', Periodicidade::getList($l));

        $c = new Criteria();
        $c->addCondition("contasreceber_id", "=", (int)$this->getParam('id'));
        $deducao = DeducoesContasreceber::getList($c);
        $this->set("Deducao", $deducao);

        $k = new Criteria();
        $k->addCondition("status_id", "=", "1");
        $k->setOrder('nome');
        $this->set('correcaoMonetaria', CorrecaoMonetaria::getList($k));

        if (!empty($Contasreceber->dataPagamento)){
            $c = new Criteria();
            $c->addCondition("idConta","=", $Contasreceber->id);
            $c->addCondition("credito",">", 0);
            $Mov = Movimento::getFirst($c);
            if($Mov) {
                $Movimento = new Movimento((int)$Mov->id);
                $Banco = $Movimento->getBanco();
                $MovimentoBanco = $Movimento->getMovimentoBanco();
                $DadosMovimento['banco'] = $Banco->nome;
                $DadosMovimento['movimento'] = $MovimentoBanco->mes . "/" . $MovimentoBanco->ano;
                $DadosMovimento['data'] = implode('/', array_reverse(explode('-', $Movimento->data)));
            }else{
                $DadosMovimento['banco'] = NULL;
                $DadosMovimento['movimento'] = NULL;
                $DadosMovimento['data'] = NULL;
            }
            $this->set('DadosMovimento',$DadosMovimento);
        }

    }

    # recebe os dados enviados via post da edição de Contasreceber
    # (true)redireciona ou (false) renderiza a visão /view/Contasreceber/edit.php

    function post_edit()
    {
        $this->setTitle('Editar Contas a receber');
        try {
            $Contasreceber = new Contasreceber((int)$_POST['id']);
            $valor_antigo = $Contasreceber->valor;

            $this->set('Contasreceber', $Contasreceber);
            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (empty($_POST['idFornecedor']) || $_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['datafiscal'])) {
                $_POST['datafiscal'] = convertDataBR4SQL($_POST['datafiscal']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }

            $Contasreceber->save($_POST);
            $a = new Criteria();
            $a->addCondition("contas_receber","=",(int)$_POST['id']);
            $rateios = Rateio_contasreceber::getList($a);
            foreach ($rateios as $pa){
                $porcentagem = $pa->valor/$valor_antigo*100;
                $rateio = new Rateio_contasreceber($pa->id);
                $rateio->contas_receber = $pa->contas_receber;
                $rateio->valor =  $_POST['valor']*($porcentagem/100);
                $rateio->observacao = "Rateio automático";
                $rateio->empresa= $pa->empresa;
                $rateio->centro_custo= $pa->centro_custo;
                $rateio->status= 1;
                $rateio->save();
            }

            new Msg(__('Contas a receber atualizado com sucesso'));
            $this->go('Contasreceber', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Status', Status::getList());
        $this->set('Contabilidades', Contabilidade::getList());
    }

    # recebe os dados enviados via post da edição de Contasreceber
    # (true)redireciona ou (false) renderiza a visão /view/Contasreceber/edit.php

    function post_editModal()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Contasreceber = new Contasreceber((int)$_POST['id']);
            $valor_antigo = $Contasreceber->valor;

            $this->set('Contasreceber', $Contasreceber);
            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }
            
            if (empty($_POST['idFornecedor']) || $_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['datafiscal'])) {
                $_POST['datafiscal'] = convertDataBR4SQL($_POST['datafiscal']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }

            $Contasreceber->save($_POST);
            $a = new Criteria();
            $a->addCondition("contas_receber","=",(int)$_POST['id']);
            $rateios = Rateio_contasreceber::getList($a);
            foreach ($rateios as $pa){
                $porcentagem = $pa->valor/$valor_antigo*100;
                $rateio = new Rateio_contasreceber($pa->id);
                $rateio->contas_receber = $pa->contas_receber;
                $rateio->valor =  $_POST['valor']*($porcentagem/100);
                $rateio->observacao = "Rateio automático";
                $rateio->empresa= $pa->empresa;
                $rateio->centro_custo= $pa->centro_custo;
                $rateio->status= 1;
                $rateio->save();
            }

            $ret['result'] = true;
            $ret['msg'] = 'Conta atualizada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao atualizar Conta !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    # Confirma a exclusão um(a) Contas a receber

    function delete()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Contaspagar = new Contasreceber((int)$_POST['id']);
            $Contaspagar->status = 3;
            $Contaspagar->save();
            $ret['result'] = true;
            $ret['msg'] = 'Conta apagada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir conta !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function lista()
    {
        $this->setTitle("Contas a Receber");
        $this->set('inicio', "01/".date("m").'/'.date("Y"));
        $this->set('fim', date("t/m/Y"));

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        $this->set("tipoDocumento", Tipo_documento::getList($m));
        $mm = new Criteria();
        $mm->addCondition("status_id", "=", 1);
        $this->set("tipoPagamento", Tipo_pagamento::getList($mm));

        $movimento = new MovimentoBanco();
        $aberto = new Criteria();
        $aberto->addCondition('status', '=', 'Aberto');
        $this->set('movimento', $movimento->getList($aberto));

        $banco = Banco::getList();
        $this->set('banco', $banco);

        $c = new Criteria();
        $c->addCondition('status', '=', 1);
        $contabilidade = Contabilidade::getList($c);
        $this->set('contabilidade', $contabilidade);

        $this->set('Planocontas', Planocontas::TipoPlano(2));
    }

    function post_lista()
    {

        $id = (!empty($_POST['id'])) ? ($_POST['id']) : "";
        $dtBase = (!empty($_POST['dtBase'])) ? ($_POST['dtBase']) : "";
        $tipoPagamento = (!empty($_POST['tipoPagamento'])) ? ($_POST['tipoPagamento']) : "";
        $numeroDocumento = (!empty($_POST['numeroDocumento'])) ? ($_POST['numeroDocumento']) : "";
        $situacao = (!empty($_POST['situacao'])) ? ($_POST['situacao']) : "";
        $movimento = (!empty($_POST['movimento'])) ? ($_POST['movimento']) : "";

        $id = explode(",", $id);
        $user = Session::get('user');

        foreach ($id as $value) {
            //Calculando valor com deducoes
            $c = new Contasreceber($value);
            $a = new Criteria($value);
            $a->addCondition("contasreceber_id", "=", $value);
            $aux = DeducoesContasreceber::getList($a);

            $percentual = 0;
            foreach ($aux as $v) {
                $v->percentual = str_replace(",", ".", $v->percentual);

                $percentual += (float)$v->percentual;
            }

            $valorDeduzido = (float)$c->valor * ((float)$percentual / 100);
            $valorDeduzido = (float)$c->valor - (float)$valorDeduzido;


            //_____
            $c->dataPagamento = $dtBase;
            $c->tipoPagamento = $tipoPagamento;
            $c->numeroDocumento = $numeroDocumento;

            if ($c->save()) {

                $aux = explode('-', $dtBase);
                $ano = $aux[0];
                $mes = $aux[1];

                $m = new MovimentoBanco($movimento);

                $j = new Movimento();
                $j->situacao = $situacao;
                $j->data = $dtBase;
                $j->idPlanoContas = $c->idPlanoContas;
                $j->banco = $m->idBanco;
                $j->idCliente = $c->idCliente;
                $j->status = 1;
                $j->documento = 'Crédito/Débito';
                $j->tipo = 2;
                $j->credito = $valorDeduzido;
                $j->categoria = 1;
                $j->idConta = $c->id;
                $j->idMovimentoBanco = $movimento;
                $j->numeroDocumento = $c->numfiscal;
                $j->idContabilidade= $c->contabilidade;
                $j->save();
            }
        }
        exit();
    }

    function validaRecebimento()
    {
        $response = "0";
        $dt = $_POST['dtBase'];
        $c = new Criteria();
        $c->addCondition('status', '=', 'Aberto');
        $movimento = MovimentoBanco::getList($c);
        foreach ($movimento as $m) {
            $aux = explode("-", $dt);
            if ($m->ano == $aux[0]) {
                if ($m->mes == $aux[1]) {
                    $response = "1";
                }
            }
        }
        echo $response;
        exit;
    }

    function addDeducaoReceber()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_addDeducaoReceber()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $deducao = new DeducoesContasreceber();
            $deducao->percentual = getAmount($_POST['percentual']);
            $deducao->contasreceber_id = $_POST['contasreceber_id'];
            $deducao->deducoes_id = $_POST['deducoes_id'];
            $deducao->save();
            $ret['result'] = true;
            $ret['msg'] = 'Dedu&ccedil;&atilde;o adicionada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao adicionar dedu&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function editDeducaoReceber()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));

            $this->set("DeducaoReceber", new DeducoesContasreceber((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_editDeducaoReceber()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $deducao = new DeducoesContasreceber((int)$_POST['id']);
            $deducao->percentual = getAmount($_POST['percentual']);
            $deducao->contasreceber_id = $_POST['contasreceber_id'];
            $deducao->deducoes_id = $_POST['deducoes_id'];
            $deducao->save();
            $ret['result'] = true;
            $ret['msg'] = 'Dedu&ccedil;&atilde;o atualizada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao atualizar dedu&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function deleteDeducaoReceber()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $deducao = new DeducoesContasreceber((int)$_POST['id']);
            $deducao->delete();
            $ret['result'] = true;
            $ret['msg'] = 'Dedu&ccedil;&atilde;o apagada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir dedu&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function anexos()
    {
        # SE ESTA RECEBENDO O FORMULARIO PARA CADASTRO DO ANEXO
        if (isset($_POST['upload_anexo'])) {
            try {
                $Anexo = new Anexocontafinanceiro();
                $Anexo->save($_POST);
                print('true');
                exit;
            } catch (Exception $e) {
                echo "Erro ao enviar anexo, recarregue e tente novamente.";
            }
        }

        $c = new Criteria();
        if ($this->getParam('contasreceber_id')) {
            $c->addCondition('contasreceber_id', '=', $this->getParam('contasreceber_id'));
            $this->set('contasreceber_id', $this->getParam('contasreceber_id'));
            $this->set('contasreceber', new Contasreceber($this->getParam('contasreceber_id')));
        }
        $c->setOrder('titulo');
        $this->set('Anexos', Anexocontafinanceiro::getList());
    }

    function add_anexo()
    {
        $this->setTitle('Adicionar Anexo');
        $this->set('Arquivo', new stdClass());
        $contasreceber = new Contasreceber($this->getParam('contasreceber_id'));
        $this->set('contasreceber', $contasreceber);
    }

    function post_add_anexo()
    {
        $this->setTitle('Adicionar Anexo');
        $anexo = new Anexocontafinanceiro();
        try {
            $upload = new FileUploader($_FILES['arquivo']);
            $upload->save($anexo->id . time(), 'contasreceber/anexo/' . $_POST['contasreceber_id'] . '/');

            $anexo->caminho = "uploads/contasreceber/anexo/" . $_POST['contasreceber_id'] . '/' . $upload->name;

            $anexo->save();
            new Msg(__('Imagem cadastrada com sucesso'));
            $this->go('Contasreceber', 'all', array());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    function delete_anexo()
    {
        try {
            $Anexo = new Anexocontafinanceiro((int)$this->getParam('id'));
            $Anexo->delete();

            new Msg(__('Anexo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    function getContas(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $inicio = !empty($_GET['inicio']) ? $_GET['inicio'] : date("Y") . '-' . date("m") . '-' . "01";
            $fim = !empty($_GET['fim']) ? $_GET['fim'] : date("Y-m-t");
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($page) || $page < 1)
                $page = 1;

            $inicioAux = convertDataBR4SQL($inicio);
            if(!empty($inicioAux)){
                $inicio = convertDataBR4SQL($inicio);
            }

            $fimAux = convertDataBR4SQL($fim);
            if(!empty($fimAux)){
                $fim = convertDataBR4SQL($fim);
            }

            $sqlCount = "SELECT count(*) AS count FROM contasreceber AS c
                         WHERE DATE(c.vencimento) BETWEEN :inicial AND :final";

            if(Config::get('origem')==1) {
                $sql = "SELECT c.id, c.data, c.vencimento, f.nome AS cliente, p.nome AS plano,
                    c.numdoc AS documento, c.complemento, IFNULL(c.valor, 0) as valor, c.dataPagamento,
                    (IFNULL(c.valor, 0) - (IFNULL(c.valor, 0) * IFNULL((SELECT SUM(d.percentual) FROM deducoes_contasreceber AS
                     d WHERE d.contasreceber_id = c.id), 0)) / 100) AS deducao, s.descricao AS status,
                     (SELECT EXISTS(SELECT * FROM anexo_financeiro
                     WHERE tipo = 2 AND id_externo = c.id)) AS anexos,
                     (SELECT EXISTS(SELECT * FROM rateio_contasreceber
                     WHERE contas_receber = c.id)) AS rateios,
                    0 AS total FROM contasreceber AS c
                    LEFT JOIN alunos AS f ON (f.id = c.idCliente)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN status AS s ON (s.id = c.status)
                    WHERE DATE(c.vencimento) BETWEEN :inicial AND :final";
            }else if(Config::get('origem')==2){
                $sql = "SELECT c.id, c.data, c.vencimento, f.nome AS cliente, p.nome AS plano,
                    c.numdoc AS documento, c.complemento, IFNULL(c.valor, 0) as valor, c.dataPagamento,
                    (IFNULL(c.valor, 0) - (IFNULL(c.valor, 0) * IFNULL((SELECT SUM(d.percentual) FROM deducoes_contasreceber AS
                     d WHERE d.contasreceber_id = c.id), 0)) / 100) AS deducao, s.descricao AS status,
                    0 AS total FROM contasreceber AS c
                    LEFT JOIN cliente AS f ON (f.id = c.idCliente)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN status AS s ON (s.id = c.status)
                    WHERE DATE(c.vencimento) BETWEEN :inicial AND :final";
            }else{
                $sql = "SELECT c.id, c.data, c.vencimento, f.cliente, p.nome AS plano,
                    c.numdoc AS documento, c.complemento, IFNULL(c.valor, 0) as valor, c.dataPagamento,
                    (IFNULL(c.valor, 0) - (IFNULL(c.valor, 0) * IFNULL((SELECT SUM(d.percentual) FROM deducoes_contasreceber AS
                     d WHERE d.contasreceber_id = c.id), 0)) / 100) AS deducao, s.descricao AS status,
                    0 AS total FROM contasreceber AS c
                    LEFT JOIN cliente AS f ON (f.id = c.idCliente)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN status AS s ON (s.id = c.status)
                    WHERE DATE(c.vencimento) BETWEEN :inicial AND :final";
            }

            if (!empty($_GET['status'])) {
                $sqlCount .= " AND c.status = :status";
                $sql .= " AND c.status = :status";
            }else{
                $sqlCount .= " AND c.status <> 3";
                $sql .= " AND c.status <> 3";
            }

            if (!empty($_GET['situacao'])) {
                if ($_GET['situacao'] == '1') {
                    $sqlCount .= " AND c.dataPagamento IS NOT NULL";
                    $sql .= " AND c.dataPagamento IS NOT NULL";
                } else {
                    $sqlCount .= " AND c.dataPagamento IS NULL";
                    $sql .= " AND c.dataPagamento IS NULL";
                }
            }

            if (!empty($_GET['idPlanoContas'])) {
                $sqlCount .= " AND c.idPlanoContas = :idPlanoContas";
                $sql .= " AND c.idPlanoContas = :idPlanoContas";
            }

            if (!empty($_GET['contabilidade'])) {
                $sqlCount .= " AND c.contabilidade = :contabilidade";
                $sql .= " AND c.contabilidade = :contabilidade";
            }

            if (!empty($_GET['cliente'])) {
                $sqlCount .= " AND c.idCliente = :cliente";
                $sql .= " AND c.idCliente = :cliente";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.vencimento";
                $sort[] = array('vencimento' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":inicial", $inicio);
            $db->bind(":final", $fim);

            if (!empty($_GET['status'])) {
                $db->bind(":status", $_GET['status']);
            }

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":idPlanoContas", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['contabilidade'])) {
                $db->bind(":contabilidade", $_GET['contabilidade']);
            }

            if (!empty($_GET['cliente'])) {
                $db->bind(":cliente", $_GET['cliente']);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);
            $db->bind(":inicial", $inicio);
            $db->bind(":final", $fim);

            if (!empty($_GET['status'])) {
                $db->bind(":status", $_GET['status']);
            }

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":idPlanoContas", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['contabilidade'])) {
                $db->bind(":contabilidade", $_GET['contabilidade']);
            }

            if (!empty($_GET['cliente'])) {
                $db->bind(":cliente", $_GET['cliente']);
            }

            $dados = $db->getResults();

            $valorTotal = 0;
            foreach($dados AS $d){
                $valorTotal += $d->valor;
            }

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            $ret["valorTotal"] = $valorTotal;

            echo json_encode($ret);

        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    function getContasPagar(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $inicio = !empty($_GET['inicio']) ? $_GET['inicio'] : date("Y") . '-' . date("m") . '-' . "01";
            $fim = !empty($_GET['fim']) ? $_GET['fim'] : date("Y-m-t");
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

            $inicioAux = convertDataBR4SQL($inicio);
            if(!empty($inicioAux)){
                $inicio = convertDataBR4SQL($inicio);
            }

            $fimAux = convertDataBR4SQL($fim);
            if(!empty($fimAux)){
                $fim = convertDataBR4SQL($fim);
            }

            $sqlCount = "SELECT count(*) AS count FROM contasreceber AS c
                         WHERE DATE(c.vencimento) BETWEEN :inicial AND :final
                         AND c.dataPagamento IS NULL";

            if(Config::get('origem')==1) {
                $sql = "SELECT c.id, c.data, c.vencimento, f.nome AS cliente, p.nome AS plano,
                    c.numdoc AS documento, c.complemento, IFNULL(c.valor, 0) as valor, c.dataPagamento,
                    (IFNULL(c.valor, 0) - (IFNULL(c.valor, 0) * IFNULL((SELECT SUM(d.percentual) FROM deducoes_contasreceber AS
                     d WHERE d.contasreceber_id = c.id), 0)) / 100) AS deducao, s.descricao AS status,
                    0 AS total FROM contasreceber AS c
                    LEFT JOIN alunos AS f ON (f.id = c.idCliente)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN status AS s ON (s.id = c.status)
                    WHERE DATE(c.vencimento) BETWEEN :inicial AND :final
                    AND c.dataPagamento IS NULL AND c.status = 1";
            }else if(Config::get('origem')==2){
                $sql = "SELECT c.id, c.data, c.vencimento, f.nome AS cliente, p.nome AS plano,
                    c.numdoc AS documento, c.complemento, IFNULL(c.valor, 0) as valor, c.dataPagamento,
                    (IFNULL(c.valor, 0) - (IFNULL(c.valor, 0) * IFNULL((SELECT SUM(d.percentual) FROM deducoes_contasreceber AS
                     d WHERE d.contasreceber_id = c.id), 0)) / 100) AS deducao, s.descricao AS status,
                    0 AS total FROM contasreceber AS c
                    LEFT JOIN cliente AS f ON (f.id = c.idCliente)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN status AS s ON (s.id = c.status)
                    WHERE DATE(c.vencimento) BETWEEN :inicial AND :final
                    AND c.dataPagamento IS NULL AND c.status = 1";
            }else{
                $sql = "SELECT c.id, c.data, c.vencimento, f.cliente, p.nome AS plano,
                    c.numdoc AS documento, c.complemento, IFNULL(c.valor, 0) as valor, c.dataPagamento,
                    (IFNULL(c.valor, 0) - (IFNULL(c.valor, 0) * IFNULL((SELECT SUM(d.percentual) FROM deducoes_contasreceber AS
                     d WHERE d.contasreceber_id = c.id), 0)) / 100) AS deducao, s.descricao AS status,
                    0 AS total FROM contasreceber AS c
                    LEFT JOIN cliente AS f ON (f.id = c.idCliente)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN status AS s ON (s.id = c.status)
                    WHERE DATE(c.vencimento) BETWEEN :inicial AND :final
                    AND c.dataPagamento IS NULL AND c.status = 1";
            }

            if (!empty($_GET['idPlanoContas'])) {
                $sqlCount .= " AND c.idPlanoContas = :plano";
                $sql .= " AND c.idPlanoContas = :plano";
            }

            if (!empty($_GET['cliente'])) {
                $sqlCount .= " AND c.cliente = :cliente";
                $sql .= " AND c.cliente = :cliente";
            }

            $sql .= " ORDER BY c.vencimento";
            $sort[] = array('vencimento' => 'asc');

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":inicial", $inicio);
            $db->bind(":final", $fim);

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":plano", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['cliente'])) {
                $db->bind(":cliente", $_GET['cliente']);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);
            $db->bind(":inicial", $inicio);
            $db->bind(":final", $fim);

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":plano", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['cliente'])) {
                $db->bind(":cliente", $_GET['cliente']);
            }

            $dados = $db->getResults();

            $valorTotal = 0;
            foreach($dados AS $d){
                $valorTotal += $d->valor;
            }

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            $ret["valorTotal"] = $valorTotal;

            echo json_encode($ret);

        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    function getDeducoes(){
        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $conta = (int)$this->getParam('id');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM deducoes_contasreceber AS dc
                         WHERE dc.contasreceber_id = :conta";

            $sql = "SELECT dc.id, d.nome AS deducao, dc.percentual,
                    IF(c.dataPagamento IS NULL, TRUE , FALSE) AS pago
                    FROM deducoes_contasreceber AS dc
                    LEFT JOIN deducoes AS d ON d.id = dc.deducoes_id
                    LEFT JOIN contasreceber AS c ON c.id = dc.contasreceber_id
                    WHERE dc.contasreceber_id = :conta";

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY d.nome";
                $sort[] = array('deducao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":conta", $conta);

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);
            $db->bind(":conta", $conta);

            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);

        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }
}