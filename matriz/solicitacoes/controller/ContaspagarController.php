<?php

final class ContaspagarController extends AppController
{

    function recebido()
    {

        $this->setTitle('Receber Documento ou Boleto');
        try {
            $this->set('Contaspagar', new Contaspagar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Contaspagar', 'all');
        }

    }

    function post_recebido()
    {

        $this->setTitle('Receber Documento ou Boleto');
        try {
            $Contaspagar = new Contaspagar((int)$_POST['id']);
            $this->set('Contaspagar', $Contaspagar);
            $Contaspagar->save($_POST);
            $this->go('Contaspagar', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }

    }

    function index()
    {
        $this->setTitle('Contas a pagar');
    }

    function all()
    {
        $this->setTitle('Contas a pagar');

        $this->set('inicio', "01/" . date("m") . '/' . date("Y"));
        $this->set('fim', date("t/m/Y"));
        $this->set('status', 1);
        $this->set('situacao', NULL);

        $b = new Criteria();
        $b->addCondition('status', '=', '1');
        $b->setOrder('nome');
        $contabilidade = Contabilidade::getList($b);

        $bb = new Criteria();
        $bb->addCondition('status', '=', '1');
        $bb->setOrder('nome');

        $cS = new Criteria();
        $cS->addCondition('id', '<>', 3);

        $this->set('StatusList', Status::getList($cS));

        $this->set('idPlanoContas', Planocontas::TipoPlano(2));
        $this->set('contabilidade', $contabilidade);
    }

    function view()
    {
        try {
            $this->set('Contaspagar', new Contaspagar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Contaspagar', 'all');
        }
    }

    # formulÃ¡rio de cadastro de Contaspagar
    # renderiza a visÃ£o /view/Contaspagar/add.php

    function add()
    {
        $this->setTitle('Adicionar Contas a pagar');
        $c = new Criteria();
        $c->setOrder("nome");
        $cc = new Criteria();
        $cc->setOrder("nome");

        $solicitacaoCriteria = new Criteria();
        $solicitacaoCriteria->addCondition('id', '=', $this->getParam('id'));
        $this->set('Solicitacao', Solicitacao::getFirst($solicitacaoCriteria));

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        $this->set("tipoPagamento", Tipo_pagamento::getList($m));
        $mm = new Criteria();
        $mm->addCondition("status_id", "=", 1);
        $this->set("tipoDocumento", Tipo_documento::getList($mm));

        $this->set('Planocontas', Planocontas::TipoPlano(2));
        $this->set('Contabilidade', Contabilidade::getList($cc));

        $j = new Criteria();
        $j->addCondition('id', '<>', 3);
        $this->set('Status', Status::getList($j));
        $l = new Criteria();
        $l->setOrder("nome");
        $l->addCondition("status", "=", "1");
        $this->set('Periodicidade', Periodicidade::getList($l));

        $g = new Criteria();
        $g->addCondition("status", "=", 1);
        $this->set("Centro_custo", Centro_custo::getList($g));

        $solicitacao_id = $this->getParam('id');
        $g1 = new Criteria();
        $g1->addCondition('id', '=', $solicitacao_id);
        $meioPagamento = '';
        foreach (Solicitacao::getList($g1) as $s) {
            $meioPagamento = $s->meio_pagamento;
        }
        $this->set('meioPagamento', $meioPagamento);
    }

    # recebe os dados enviados via post do cadastro de Contaspagar
    # (true)redireciona ou (false) renderiza a visÃ£o /view/Contaspagar/add.php

    function post_add()
    {
        $this->setTitle('Adicionar Contas a pagar');

        if ($_POST['formulario'] == 'individual') {
            try {
                $Contaspagar = new Contaspagar();

                if (!empty($_POST['valor'])) {
                    $_POST['valor'] = getAmount($_POST['valor']);
                }
                if (!empty($_POST['multa'])) {
                    $_POST['multa'] = getAmount($_POST['multa']);
                }

                if (!empty($_POST['valorBruto'])) {
                    $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
                }
                if (!empty($_POST['juros'])) {
                    $_POST['juros'] = getAmount($_POST['juros']);
                }
                if (!empty($_POST['desconto'])) {
                    $_POST['desconto'] = getAmount($_POST['desconto']);
                }
                if (!empty($_POST['data'])) {
                    $_POST['data'] = convertDataBR4SQL($_POST['data']);
                }
                if (!empty($_POST['vencimento'])) {
                    $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
                }
                if (!empty($_POST['dataPagamento'])) {
                    $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
                }
                if ($_POST['idFornecedor'] == 0) {
                    $_POST['idFornecedor'] = NULL;
                }
                if ($_POST['idPlanoContas'] == 0) {
                    $_POST['idPlanoContas'] = NULL;
                }
                if ($_POST['contabilidade'] == 0) {
                    $_POST['contabilidade'] = NULL;
                }
                $Contaspagar->save($_POST);
                $plano = new Planocontas((int)$_POST['idPlanoContas']);

                if ($plano->rateio == 2) {
                    $this->go('Contaspagar', 'edit', array('id' => $Contaspagar->id));
                } else {
                    $pl = new Criteria();
                    $pl->addCondition('plano_contas', '=', $_POST['idPlanoContas']);
                    $plano_centro = Plano_centrocusto::getList($pl);
                    foreach ($plano_centro as $pa) {
                        $rateio = new Rateio_contaspagar();
                        $rateio->contar_pagar = $Contaspagar->id;
                        $rateio->valor = $Contaspagar->valor * ($pa->porcentagem / 100);
                        $rateio->observacao = "Rateio automÃ¡tico";
                        $rateio->empresa = $pa->empresa;
                        $rateio->centro_custo = $pa->centro_custo;
                        $rateio->status = 1;
                        $rateio->data_documento = $Contaspagar->data;
                        $rateio->cliente_id = $Contaspagar->idFornecedor;
                        $rateio->plano_contas_id = $Contaspagar->idPlanoContas;
                        $rateio->save();
                    }
                }
                $this->set('Contaspagar', $Contaspagar);
                new Msg(__('Contaspagar cadastrado com sucesso'));
                $this->go('Contaspagar', 'all', array('id' => $Contaspagar->id));
            } catch (Exception $e) {
                new Msg($e->getMessage(), 3);
            }
            $this->set('Planocontas', Planocontas::getList());
            $this->set('Fornecedores', Fornecedor::getList());
            $j = new Criteria();
            $j->addCondition('id', '<>', 3);
            $this->set('Status', Status::getList($j));
        } else if ($_POST['formulario'] == 'gerar') {
            Try {
                $programacao = new Programacao();
                $_POST['status'] = "1";
                $_POST['tipo'] = "A pagar";
                $programacao->save($_POST);
                new Msg("ProgramaÃ§Ã£o cadastrada com sucesso");
            } catch (Exception $e) {
                new Msg($e->getMessage(), 3);
            }
            $this->go('Solicitacao', 'all');
        }
    }

    function post_addModal()
    {
        $ret = array();
        $ret['result'] = false;
        try {
            $Solicitacao = new Solicitacao((int)$_POST['solicitacao_id']);
            $Solicitacao->situacao_solicitacao_id = 1; //aceita
            $Solicitacao->save($Solicitacao);
            $Contaspagar = new Contaspagar();

            if (!empty($_POST['solicitacao_id'])) {
                $_POST['solicitacao_id'] = $_POST['solicitacao_id'];
            }
            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }
            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }

            if ($_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            $Contaspagar->status = 4;
            $Contaspagar->save($_POST);
            $plano = new Planocontas((int)$_POST['idPlanoContas']);

            if ($plano->rateio == 2) {
                $ret['conta'] = $Contaspagar->id;
            } else {
                $pl = new Criteria();
                $pl->addCondition('plano_contas', '=', $_POST['idPlanoContas']);
                $plano_centro = Plano_centrocusto::getList($pl);
                foreach ($plano_centro as $pa) {
                    $rateio = new Rateio_contaspagar();
                    $rateio->contar_pagar = $Contaspagar->id;
                    $rateio->valor = $Contaspagar->valor * ($pa->porcentagem / 100);
                    $rateio->observacao = "Rateio automático";
                    $rateio->empresa = $pa->empresa;
                    $rateio->centro_custo = $pa->centro_custo;
                    $rateio->status = 2;
                    $rateio->save();
                }
            }

            $ret['result'] = true;
            $ret['msg'] = 'Conta adicionada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao adicionar Conta !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    # formulÃ¡rio de ediÃ§Ã£o de Contaspagar
    # renderiza a visÃ£o /view/Contaspagar/edit.php

    function edit()
    {
        $this->setTitle('Editar Contas a pagar');
        try {
            $c = new Criteria();
            $c->setOrder("nome");

            $cc = new Criteria();
            $cc->setOrder("nome");

            $m = new Criteria();
            $m->addCondition("status_id", "=", 1);

            $this->set("tipoPagamento", Tipo_pagamento::getList($m));

            $mm = new Criteria();
            $mm->addCondition("status_id", "=", 1);
            $this->set("tipoDocumento", Tipo_documento::getList($mm));

            $Contaspagar = new Contaspagar((int)$this->getParam('id'));
            $this->set('Contaspagar', $Contaspagar);
            $this->set('Contabilidade', Contabilidade::getList($c));
            $this->set('Planocontas', Planocontas::TipoPlano(2));

            $this->set('forncedorSelect', new Fornecedor($Contaspagar->idFornecedor));


            $this->set('Status', Status::getList());
            if (!empty($Contaspagar->dataPagamento)) {
                $c = new Criteria();
                $c->addCondition("idConta", "=", $Contaspagar->id);
                $c->addCondition("debito", ">", 0);
                $Mov = Movimento::getFirst($c);
                if ($Mov) {
                    $Movimento = new Movimento((int)$Mov->id);
                    $Banco = $Movimento->getBanco();
                    $MovimentoBanco = $Movimento->getMovimentoBanco();
                    $DadosMovimento['banco'] = $Banco->nome;
                    $DadosMovimento['movimento'] = $MovimentoBanco->mes . "/" . $MovimentoBanco->ano;
                    $DadosMovimento['data'] = implode('/', array_reverse(explode('-', $Movimento->data)));
                } else {
                    $DadosMovimento['banco'] = NULL;
                    $DadosMovimento['movimento'] = NULL;
                    $DadosMovimento['data'] = NULL;
                }
                $this->set('DadosMovimento', $DadosMovimento);
            }

        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Contaspagar', 'all');
        }
    }

    # recebe os dados enviados via post da ediÃ§Ã£o de Contaspagar
    # (true)redireciona ou (false) renderiza a visÃ£o /view/Contaspagar/edit.php

    function post_edit()
    {
        $this->setTitle('Editar Contas a pagar');
        try {
            $Contaspagar = new Contaspagar((int)$_POST['id']);
            $valor_antigo = $Contaspagar->valor;
            $this->set('Contaspagar', $Contaspagar);

            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }


            if ($_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            $Contaspagar->save($_POST);

            $a = new Criteria();
            $a->addCondition("contar_pagar", "=", (int)$_POST['id']);
            $rateios = Rateio_contaspagar::getList($a);
            foreach ($rateios as $pa) {
                $porcentagem = $pa->valor / $valor_antigo * 100;
                $rateio = new Rateio_contaspagar($pa->id);
                $rateio->contar_pagar = $pa->contar_pagar;
                $rateio->valor = $_POST['valor'] * ($porcentagem / 100);
                $rateio->observacao = "Rateio automático";
                $rateio->empresa = $pa->empresa;
                $rateio->centro_custo = $pa->centro_custo;
                $rateio->status = 1;
                $rateio->save();
            }
            new Msg(__('Contaspagar atualizado com sucesso'));
            if (!empty($_POST['deducao'])) {
                if ($_POST['deducao'] == 1) {
                    $this->go('Contaspagar', 'edit', array('id' => $Contaspagar->id));
                }
            } else {
                $this->go('Contaspagar', 'all');
            }
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $j = new Criteria();
        $j->addCondition('id', '<>', 3);
        $this->set('Status', Status::getList($j));
    }


    # recebe os dados enviados via post da ediÃ§Ã£o de Contaspagar
    # (true)redireciona ou (false) renderiza a visÃ£o /view/Contaspagar/edit.php

    function post_editModal()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Contaspagar = new Contaspagar((int)$_POST['id']);
            $valor_antigo = $Contaspagar->valor;
            $this->set('Contaspagar', $Contaspagar);

            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }


            if ($_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if ($_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if ($_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            $Contaspagar->save($_POST);
            $a = new Criteria();
            $a->addCondition("contar_pagar", "=", (int)$_POST['id']);
            $rateios = Rateio_contaspagar::getList($a);
            foreach ($rateios as $pa) {
                $porcentagem = $pa->valor / $valor_antigo * 100;
                $rateio = new Rateio_contaspagar($pa->id);
                $rateio->contar_pagar = $pa->contar_pagar;
                $rateio->valor = $_POST['valor'] * ($porcentagem / 100);
                $rateio->observacao = "Rateio automático";
                $rateio->empresa = $pa->empresa;
                $rateio->centro_custo = $pa->centro_custo;
                $rateio->status = 1;
                $rateio->save();
            }

            $ret['result'] = true;
            $ret['msg'] = 'Conta atualizada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao atualizar Conta !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    # Recebe o id via post e exclui um(a) Contaspagar
    # redireciona para Contaspagar/all
    function delete()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Contaspagar = new Contaspagar((int)$_POST['id']);
            $Contaspagar->status = 3;
            $Contaspagar->save();
            $ret['result'] = true;
            $ret['msg'] = 'Conta apagada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir conta !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function lista()
    {
        $this->setTitle("Contas a Pagar");

        $this->set('inicio', "01/" . date("m") . '/' . date("Y"));
        $this->set('fim', date("t/m/Y"));

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        $this->set("tipoDocumento", Tipo_documento::getList($m));
        $mm = new Criteria();
        $mm->addCondition("status_id", "=", 1);
        $this->set("tipoPagamento", Tipo_pagamento::getList($mm));

        $movimento = new MovimentoBanco();
        $aberto = new Criteria();
        $aberto->addCondition('status', '=', 'Aberto');
        $this->set('movimento', $movimento->getList($aberto));

        $banco = Banco::getList();
        $this->set('banco', $banco);

        $c = new Criteria();
        $c->addCondition('status', '=', 1);
        $contabilidade = Contabilidade::getList($c);
        $this->set('contabilidade', $contabilidade);

        $this->set('Planocontas', Planocontas::TipoPlano(2));
    }

    function post_lista()
    {

        $id = (!empty($_POST['id'])) ? ($_POST['id']) : "";
        $dtBase = (!empty($_POST['dtBase'])) ? ($_POST['dtBase']) : "";
        $tipoPagamento = (!empty($_POST['tipoPagamento'])) ? ($_POST['tipoPagamento']) : "";
        $numeroDocumento = (!empty($_POST['numeroDocumento'])) ? ($_POST['numeroDocumento']) : "";
        $situacao = (!empty($_POST['situacao'])) ? ($_POST['situacao']) : "";
        $movimento = (!empty($_POST['movimento'])) ? ($_POST['movimento']) : "";

        $id = explode(",", $id);
        $user = Session::get('user');
        foreach ($id as $value) {
            $c = new Contaspagar($value);
            $c->dataPagamento = $dtBase;
            $c->tipoPagamento = $tipoPagamento;
            $c->numeroDocumento = $numeroDocumento;
            if ($c->save()) {

                $a = new Criteria();
                $a->addCondition("contaspagar_id", "=", $value);
                $aux = DeducoesContaspagar::getList($a);
                $deducao = 0;
                foreach ($aux as $v) {
                    $deducao += (float)$v->valor;
                }

                $valorDeduzido = (float)$c->valor - $deducao;

                $aux = explode('-', $dtBase);
                $ano = $aux[0];
                $mes = $aux[1];

                $m = new MovimentoBanco($movimento);

                $j = new Movimento();
                $j->situacao = $situacao;
                $j->data = $dtBase;
                $j->idPlanoContas = $c->idPlanoContas;
                $j->banco = $m->idBanco;
                $j->idFornecedor = $c->idFornecedor;
                $j->status = 1;
                $j->documento = 'Crédito/Débito';
                $j->tipo = 1;
                $j->debito = $valorDeduzido;
                $j->categoria = 1;
                $j->idConta = $c->id;
                $j->idMovimentoBanco = $movimento;
                $j->idContabilidade = $c->contabilidade;

                $j->save();
            }
        }
        exit();
    }

    function validaRecebimento()
    {
        $response = "0";
        $dt = $_POST['dtBase'];
        $c = new Criteria();
        $c->addCondition('status', '=', 'Aberto');
        $movimento = MovimentoBanco::getList($c);
        foreach ($movimento as $m) {
            $aux = explode("-", $dt);
            if ($m->ano == $aux[0]) {
                if ($m->mes == $aux[1]) {
                    $response = "1";
                }
            }
        }
        echo $response;
        exit;
    }

    function addDeducaoPagar()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_addDeducaoPagar()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $deducao = new DeducoesContaspagar();
            $deducao->valor = getAmount($_POST['valor']);
            $deducao->contaspagar_id = $_POST['contaspagar_id'];
            $deducao->deducoes_id = $_POST['deducoes_id'];
            $deducao->save();
            $ret['result'] = true;
            $ret['msg'] = 'Dedu&ccedil;&atilde;o adicionada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao adicionar dedu&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function editDeducaoPagar()
    {
        try {
            $a = new Criteria();
            $a->setOrder("nome");
            $a->addCondition("status", "=", 1);
            $this->set("Deducao", Deducoes::getList($a));

            $this->set("DeducaoPagar", new DeducoesContaspagar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
    }

    function post_editDeducaoPagar()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $deducao = new DeducoesContaspagar((int)$_POST['id']);
            $deducao->valor = getAmount($_POST['valor']);
            $deducao->contaspagar_id = $_POST['contaspagar_id'];
            $deducao->deducoes_id = $_POST['deducoes_id'];
            $deducao->save();
            $ret['result'] = true;
            $ret['msg'] = 'Dedu&ccedil;&atilde;o atualizada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao atualizar dedu&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function deleteDeducaoPagar()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $deducao = new DeducoesContaspagar((int)$_POST['id']);
            $deducao->delete();
            $ret['result'] = true;
            $ret['msg'] = 'Dedu&ccedil;&atilde;o apagada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir dedu&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function anexos()
    {
        # SE ESTA RECEBENDO O FORMULARIO PARA CADASTRO DO ANEXO
        if (isset($_POST['upload_anexo'])) {
            try {
                $Anexo = new Anexocontafinanceiro();
                $Anexo->save($_POST);
                print('true');
                exit;
            } catch (Exception $e) {
                echo "Erro ao enviar anexo, recarregue e tente novamente.";
            }
        }

        $c = new Criteria();
        if ($this->getParam('contaspagar_id')) {
            $c->addCondition('contaspagar_id', '=', $this->getParam('contaspagar_id'));
            $this->set('contaspagar_id', $this->getParam('contaspagar_id'));
            $this->set('contaspagar', new Contaspagar($this->getParam('contaspagar_id')));
        }
        $c->setOrder('titulo');
        $this->set('Anexos', Anexocontafinanceiro::getList());
    }

    function add_anexo()
    {
        $this->setTitle('Adicionar Anexo');
        $this->set('Arquivo', new stdClass());
        $Contaspagar = new Contaspagar($this->getParam('contaspagar_id'));
        $this->set('Contaspagar', $Contaspagar);
    }

    function post_add_anexo()
    {
        $this->setTitle('Adicionar Anexo');
        $anexo = new Anexocontafinanceiro();
        try {
            $anexo->tipo = "A pagar";
            $anexo->descricao = $_POST['descricao'];
            $anexo->idConta = $_POST['contaspagar_id'];

            $upload = new FileUploader($_FILES['arquivo']);

            $upload->save($anexo->id . time(), 'Contaspagar/anexo/' . $_POST['contaspagar_id'] . '/');

            $anexo->caminho = "uploads/Contaspagar/anexo/" . $_POST['contaspagar_id'] . '/' . $upload->name;

            $anexo->save();
            new Msg(__('Imagem cadastrada com sucesso'));
            $this->go('Contaspagar', 'all', array());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    function delete_anexo()
    {
        try {
            $Anexo = new Anexocontafinanceiro((int)$this->getParam('id'));
            $Anexo->delete();

            new Msg(__('Anexo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    function getContas()
    {

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
//            $inicio = !empty($_GET['inicio']) ? $_GET['inicio'] : date("Y") . '-' . date("m") . '-' . "01";
//            $fim = !empty($_GET['fim']) ? $_GET['fim'] : date("Y-m-t");
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

//            $inicioAux = convertDataBR4SQL($inicio);
//            if(!empty($inicioAux)){
//                $inicio = convertDataBR4SQL($inicio);
//            }
//
//            $fimAux = convertDataBR4SQL($fim);
//            if(!empty($fimAux)){
//                $fim = convertDataBR4SQL($fim);
//            }

            $sqlCount = "SELECT count(*) AS count FROM contaspagar AS c
                         WHERE c.solicitacao_id = :solicitacao";

            $sql = "SELECT c.id,c.parcela, c.vencimento, f.nome AS credor, p.nome AS plano,
                    ct.nome AS empresa, t.descricao AS tipo, c.complemento, c.valor, c.data, c.dataPagamento,
                    (c.valor - IFNULL((SELECT SUM(d.valor) FROM deducoes_contaspagar AS
                     d WHERE d.contaspagar_id = c.id), 0)) AS deducao,
                    (SELECT EXISTS(SELECT * FROM anexo_financeiro
                     WHERE tipo = 1 AND id_externo = c.id)) AS anexos,
                     (SELECT EXISTS(SELECT * FROM rateio_contaspagar
                     WHERE contar_pagar = c.id)) AS rateio,
                    0 AS total, c.complemento as obs, c.recebido as recebido FROM contaspagar AS c
                    LEFT JOIN fornecedor AS f ON (f.id = c.idFornecedor)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN contabilidade AS ct ON (ct.id = c.contabilidade)
                    LEFT JOIN tipo_documento AS t ON (t.id = c.tipoDocumento)
                    WHERE c.solicitacao_id = :solicitacao";

            if (!empty($_GET['status'])) {
                $sqlCount .= " AND c.status = :status";
                $sql .= " AND c.status = :status";
            } else {
                $sqlCount .= " AND c.status <> 3";
                $sql .= " AND c.status <> 3";
            }

            if (!empty($_GET['situacao'])) {
                if ($_GET['situacao'] == '1') {
                    $sqlCount .= " AND c.dataPagamento IS NOT NULL";
                    $sql .= " AND c.dataPagamento IS NOT NULL";
                } else {
                    $sqlCount .= " AND c.dataPagamento IS NULL";
                    $sql .= " AND c.dataPagamento IS NULL";
                }
            }

            if (!empty($_GET['recebido'])) {
                $sqlCount .= " AND c.recebido = :recebido";
                $sql .= " AND c.recebido = :recebido";
            }

            if (!empty($_GET['idPlanoContas'])) {
                $sqlCount .= " AND c.idPlanoContas = :plano";
                $sql .= " AND c.idPlanoContas = :plano";
            }

            if (!empty($_GET['contabilidade'])) {
                $sqlCount .= " AND c.contabilidade = :contabilidade";
                $sql .= " AND c.contabilidade = :contabilidade";
            }

            if (!empty($_GET['fornecedor'])) {
                $sqlCount .= " AND c.idFornecedor = :fornecedor";
                $sql .= " AND c.idFornecedor = :fornecedor";
            }

            $sortCount = count($sort);
            if ($sortCount < 1) {
                $sql .= " ORDER BY c.vencimento";
                $sort[] = array('vencimento' => 'asc');
            } else {
                $sql .= " ORDER BY";
                for ($i = 0; $i < $sortCount; $i++) {
                    if ($i > 0)
                        $sql .= ", ";

                    $sql .= " " . $sort[$i][0] . " " . $sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);
            $db->bind(":solicitacao", $_GET['solicitacaoId']);
//            $db->bind(":inicial", $inicio);
//            $db->bind(":final", $fim);

            if (!empty($_GET['status'])) {
                $db->bind(":status", $_GET['status']);
            }

            if (!empty($_GET['recebido'])) {
                $db->bind(":recebido", $_GET['recebido']);
            }

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":plano", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['contabilidade'])) {
                $db->bind(":contabilidade", $_GET['contabilidade']);
            }

            if (!empty($_GET['fornecedor'])) {
                $db->bind(":fornecedor", $_GET['fornecedor']);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);
            $db->bind(":solicitacao", $_GET['solicitacaoId']);
//            $db->bind(":inicial", $inicio);
//            $db->bind(":final", $fim);

            if (!empty($_GET['status'])) {
                $db->bind(":status", $_GET['status']);
            }

            if (!empty($_GET['recebido'])) {
                $db->bind(":recebido", $_GET['recebido']);
            }

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":plano", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['contabilidade'])) {
                $db->bind(":contabilidade", $_GET['contabilidade']);
            }

            if (!empty($_GET['fornecedor'])) {
                $db->bind(":fornecedor", $_GET['fornecedor']);
            }

            $dados = $db->getResults();

            $valorTotal = 0;
            foreach ($dados AS $d) {
                $valorTotal += $d->valor;
            }

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            $ret["valorTotal"] = $valorTotal;

            echo json_encode($ret);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }

    function getContasPagar()
    {

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $inicio = !empty($_GET['inicio']) ? $_GET['inicio'] : date("Y") . '-' . date("m") . '-' . "01";
            $fim = !empty($_GET['fim']) ? $_GET['fim'] : date("Y-m-t");
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

            $inicioAux = convertDataBR4SQL($inicio);
            if (!empty($inicioAux)) {
                $inicio = convertDataBR4SQL($inicio);
            }

            $fimAux = convertDataBR4SQL($fim);
            if (!empty($fimAux)) {
                $fim = convertDataBR4SQL($fim);
            }

            $sqlCount = "SELECT count(*) AS count FROM contaspagar AS c
                         WHERE DATE(c.vencimento) BETWEEN :inicial AND :final
                         AND c.dataPagamento IS NULL";

            $sql = "SELECT c.id, c.vencimento, f.nome AS credor, p.nome AS plano,
                    ct.nome AS empresa, t.descricao AS tipo, c.complemento as obs, c.valor,
                    (c.valor - IFNULL((SELECT SUM(d.valor) FROM deducoes_contaspagar AS
                     d WHERE d.contaspagar_id = c.id), 0)) AS deducao,
                    0 AS total FROM contaspagar AS c
                    LEFT JOIN fornecedor AS f ON (f.id = c.idFornecedor)
                    LEFT JOIN planocontas AS p ON (p.id = c.idPlanoContas)
                    LEFT JOIN contabilidade AS ct ON (ct.id = c.contabilidade)
                    LEFT JOIN tipo_documento AS t ON (t.id = c.tipoDocumento)
                    WHERE DATE(c.vencimento) BETWEEN :inicial AND :final AND
                    c.dataPagamento IS NULL AND c.status = 1";

            if (!empty($_GET['idPlanoContas'])) {
                $sqlCount .= " AND c.idPlanoContas = :plano";
                $sql .= " AND c.idPlanoContas = :plano";
            }

            if (!empty($_GET['fornecedor'])) {
                $sqlCount .= " AND c.fornecedor = :forn";
                $sql .= " AND c.fornecedor = :forn";
            }

            $sql .= " ORDER BY c.vencimento";
            $sort[] = array('vencimento' => 'asc');

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":inicial", $inicio);
            $db->bind(":final", $fim);

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":plano", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['fornecedor'])) {
                $db->bind(":forn", $_GET['fornecedor']);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);
            $db->bind(":inicial", $inicio);
            $db->bind(":final", $fim);

            if (!empty($_GET['idPlanoContas'])) {
                $db->bind(":plano", $_GET['idPlanoContas']);
            }

            if (!empty($_GET['fornecedor'])) {
                $db->bind(":forn", $_GET['fornecedor']);
            }

            $dados = $db->getResults();

            $valorTotal = 0;
            foreach ($dados AS $d) {
                $valorTotal += $d->valor;
            }

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            $ret["valorTotal"] = $valorTotal;

            echo json_encode($ret);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }

    function getDeducoes()
    {
        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $conta = (int)$this->getParam('id');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM deducoes_contaspagar AS dc
                         WHERE dc.contaspagar_id = :conta";

            $sql = "SELECT dc.id, d.nome AS deducao, dc.valor,
                    IF(c.dataPagamento IS NULL, TRUE , FALSE) AS pago
                    FROM deducoes_contaspagar AS dc
                    LEFT JOIN deducoes AS d ON d.id = dc.deducoes_id
                    LEFT JOIN contaspagar AS c ON c.id = dc.contaspagar_id
                    WHERE dc.contaspagar_id = :conta";

            $sortCount = count($sort);
            if ($sortCount < 1) {
                $sql .= " ORDER BY d.nome";
                $sort[] = array('nome' => 'asc');
            } else {
                $sql .= " ORDER BY";
                for ($i = 0; $i < $sortCount; $i++) {
                    if ($i > 0)
                        $sql .= ", ";

                    $sql .= " " . $sort[$i][0] . " " . $sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":conta", $conta);

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);
            $db->bind(":conta", $conta);

            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }

    function contasLista()
    {
        $this->setTitle('Contas a pagar');
        $c = new Criteria();
        $p = new Paginate('Contaspagar', 100);
        $id = $this->getParam('id');
        $c->addCondition('solicitacao_id', '=', $id);
        $c->addCondition('solicitacao_id', '=', $id);
        //$this->set('Solicitacao', new Solicitacao($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        $this->set('Contaspagar', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('id', $id);
    }

    function solicitacao()
    {
        $p = new Paginate('Solicitacao', 10);
        $c = new Criteria();


        $c->setOrder('situacao_solicitacao_id ASC');
        $user = Session::get('user');
        $c->addCondition('responsavel', "=", $user);
        $c->addCondition('situacao_solicitacao_id', "!=", 4);

        $filtro = false;
        if (!empty($this->getParam('idFornecedor'))) {
            $c->addCondition('idFornecedor', "=", $this->getParam('idFornecedor'));
        }
        if (!empty($this->getParam('situacao_solicitacao_id'))) {
            $filtro = true;
            $c->addCondition('situacao_solicitacao_id', "=", $this->getParam('situacao_solicitacao_id'));
        }
        if (!empty($this->getParam('projeto'))) {
            $c->addCondition('projeto', "LIKE", "%" . $this->getParam('projeto') . "%");
        }
        if (!empty($this->getParam('inicio'))) {
            $inicio = convertDataBR4SQL($this->getParam('inicio'));
            $fim = convertDataBR4SQL($this->getParam('fim'));
            $c->addCondition('data', ">=", $inicio);
            $c->addCondition('data', "<=", $fim);
        }


        $d = new Criteria();
        $d->addCondition('id', "!=", 4);
        $queryFornecedor = new Criteria();
        $queryFornecedor->addCondition('status', '=', 1);
        $queryFornecedor->setOrder('nome ASC');

        $this->set('Situacao_solicitacao', Situacao_solicitacao::getList($d));
        $this->set('Solicitacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList($queryFornecedor));
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Tipo_pagamentos', Tipo_pagamento::getList());
        $this->set('Tipo_documentos', Tipo_documento::getList());
        $this->set('Programacaos', Programacao::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('filtro', $filtro);

        /**
         * filtra apenas os modulos dos usuarios da localidade
         */
        $lo = new Criteria();
        $lo->addCondition('id', "!=", 4);
        $status = [];
        foreach (Situacao_solicitacao::getList($lo) as $situacoes) {
            $status[] = $situacoes->id;
        }
        $this->set('status', $status);


        //*******************monta os contadores filtrado pelo responsavel de autorizar a solicitacao*************************** */
        $responsavel = $user = Session::get('user');
        $solicitacaoAguardandoAceiteTotal = $this->query_count("SELECT COUNT(DISTINCT(s.`id`)) AS total
        FROM solicitacao s 
        LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
        LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
        LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
        LEFT JOIN `usuario` usu ON usu.`id` = s.responsavel
        WHERE s.`status` = 1
        AND s.responsavel = $responsavel
        AND s.`situacao_solicitacao_id` = 1");
        $this->set('solicitacaoAguardandoAceiteTotal', $solicitacaoAguardandoAceiteTotal);

        $solicitacaoAguardandoAceite = $this->query("SELECT s.id, s.`solicitante`, s.`data`, ss.`nome` AS situacao, 
           f.`nome` AS fornecedor, con.nome AS empresa  , s.projeto, s.`observacao`, s.`valor_solicitacao`, usu.nome
           FROM solicitacao s 
           LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.`situacao_solicitacao_id` = 1
           ANd s.responsavel = $responsavel");
        $this->set('solicitacaoAguardandoAceite', $solicitacaoAguardandoAceite);

        $solicitacaoRecusada = $this->query_count("SELECT s.id, s.`solicitante`, ss.`nome` AS situacao, 
           f.`nome` AS fornecedor, con.nome AS filial  
           FROM solicitacao s 
           LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.responsavel = $responsavel
           AND s.`situacao_solicitacao_id` = 2");
        $this->set('solicitacaoRecusada', $solicitacaoRecusada);

        $solicitacaoRecusadaTotal = $this->query_count("SELECT COUNT(DISTINCT(s.`id`)) AS total
           FROM solicitacao s 
           LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.responsavel = $responsavel
           AND s.`situacao_solicitacao_id` = 2");
        $this->set('solicitacaoRecusadaTotal', $solicitacaoRecusadaTotal);

        $solicitacaoAceitaTotal = $this->query_count("SELECT COUNT(DISTINCT(s.`id`)) AS total
           FROM solicitacao s 
           LEFT  JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT  JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT  JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.responsavel = $responsavel
           AND s.`situacao_solicitacao_id` = 3");
        $this->set('solicitacaoAceitaTotal', $solicitacaoAceitaTotal);
    }

    function acompanhar_solicitacao()
    {
        $p = new Paginate('Solicitacao', 10);
        $c = new Criteria();

        $filtro = false;

        $g = new Criteria();
        $g->addCondition("id", "!=", 3);
        $g->addCondition("realiza_aprovacao_financeiro", "=", 1);
        $this->set('Usuarios', Usuario::getList($g));
        $c->addCondition('responsavel', "!=", 3);
        $c->addCondition('status', "=", 1);

        if (!empty($this->getParam('responsavel'))) {
            $c->addCondition('responsavel', "=", $this->getParam('responsavel'));
        }


        if (!empty($this->getParam('idFornecedor'))) {
            $c->addCondition('idFornecedor', "=", $this->getParam('idFornecedor'));
        }

        if (!empty($this->getParam('situacao_solicitacao_id'))) {
            $filtro = true;
            $c->addCondition('situacao_solicitacao_id', "=", $this->getParam('situacao_solicitacao_id'));
        } else {
            $c->addCondition('situacao_solicitacao_id', "!=", 4);
        }

        if (!empty($this->getParam('projeto'))) {
            $c->addCondition('projeto', "LIKE", "%" . $this->getParam('projeto') . "%");
        }

        if (!empty($this->getParam('inicio'))) {
            $inicio = convertDataBR4SQL($this->getParam('inicio'));
            $fim = convertDataBR4SQL($this->getParam('fim'));
            $c->addCondition('data', ">=", $inicio);
            $c->addCondition('data', "<=", $fim);
        }


        $d = new Criteria();
        $d->addCondition('id', "!=", 4);

        $queryFornecedor = new Criteria();
        $queryFornecedor->addCondition('status', '=', 1);
        $queryFornecedor->setOrder('nome ASC');
        $c->setOrder('situacao_solicitacao_id ASC');
        $this->set('Situacao_solicitacao', Situacao_solicitacao::getList($d));
        $this->set('Solicitacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Fornecedores', Fornecedor::getList($queryFornecedor));
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Tipo_pagamentos', Tipo_pagamento::getList());
        $this->set('Tipo_documentos', Tipo_documento::getList());
        $this->set('Programacaos', Programacao::getList());
        $this->set('filtro', $filtro);

        /**
         * filtra apenas os modulos dos usuarios da localidade
         */
        $lo = new Criteria();
        $lo->addCondition('id', "!=", 4);
        $status = [];
        foreach (Situacao_solicitacao::getList($lo) as $situacoes) {
            $status[] = $situacoes->id;
        }
        $this->set('status', $status);


        //*******************monta os contadores filtrado pelo responsavel de autorizar a solicitacao*************************** */
        $responsavel = $user = Session::get('user');
        $solicitacaoAguardandoAceiteTotal = $this->query_count("SELECT COUNT(DISTINCT(s.`id`)) AS total
        FROM solicitacao s 
        LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
        LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
        LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
        LEFT JOIN `usuario` usu ON usu.`id` = s.responsavel
        WHERE s.`status` = 1
        AND s.responsavel <> 3
        AND s.`situacao_solicitacao_id` = 1");

        $this->set('solicitacaoAguardandoAceiteTotal', $solicitacaoAguardandoAceiteTotal);

        $solicitacaoAguardandoAceite = $this->query("SELECT s.id, s.`solicitante`, s.`data`, ss.`nome` AS situacao, 
           f.`nome` AS fornecedor, con.nome AS empresa  , s.projeto, s.`observacao`, s.`valor_solicitacao`, usu.nome
           FROM solicitacao s 
           LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.responsavel <> 3
           AND s.`situacao_solicitacao_id` = 1
           ");

        $this->set('solicitacaoAguardandoAceite', $solicitacaoAguardandoAceite);

        $solicitacaoRecusada = $this->query_count("SELECT s.id, s.`solicitante`, ss.`nome` AS situacao, 
           f.`nome` AS fornecedor, con.nome AS filial  
           FROM solicitacao s 
           LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.responsavel <> 3
           AND s.`situacao_solicitacao_id` = 2");

        $this->set('solicitacaoRecusada', $solicitacaoRecusada);

        $solicitacaoRecusadaTotal = $this->query_count("SELECT COUNT(DISTINCT(s.`id`)) AS total
           FROM solicitacao s 
           LEFT JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.responsavel <> 3
           AND s.`situacao_solicitacao_id` = 2");

        $this->set('solicitacaoRecusadaTotal', $solicitacaoRecusadaTotal);

        $solicitacaoAceitaTotal = $this->query_count("SELECT COUNT(DISTINCT(s.`id`)) AS total
           FROM solicitacao s 
           LEFT  JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
           LEFT  JOIN fornecedor f ON f.`id` = s.`idFornecedor`
           LEFT  JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
           LEFT JOIN `usuario` usu ON usu.`id` = s.`responsavel`
           WHERE s.`status` = 1
           AND s.responsavel <> 3
           AND s.`situacao_solicitacao_id` = 3");

        $this->set('solicitacaoAceitaTotal', $solicitacaoAceitaTotal);
    }

    function solicitacaoContasPagar()
    {
        //lista os dados para a view solicitacaoContasPagar
        $this->setTitle('Contas a pagar');

        $c = new Criteria();
        $solicitacao_id = $_GET['id'];
        $this->set('Solicitacao_id', $solicitacao_id);

        $p = new Paginate('Contaspagar', 10);
        $c->addCondition('status', '!=', 3);
        $c->addCondition('solicitacao_id', '=', $solicitacao_id);
        $this->set('Contaspagar', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $b = new Criteria();
        $b->addCondition('status', '=', '1');
        $b->setOrder('nome');
        $contabilidade = Contabilidade::getList($b);

        $bb = new Criteria();
        $bb->addCondition('status', '=', '1');
        $bb->setOrder('nome');

        $cS = new Criteria();
        $cS->addCondition('id', '<>', 3);

        $this->set('StatusList', Status::getList($cS));

        $this->set('idPlanoContas', Planocontas::TipoPlano(2));
        $this->set('contabilidade', $contabilidade);

        $g = new Criteria();
        $g->addCondition('id', '=', $solicitacao_id);
        $g->addCondition('status', '=', 1);

        $Solicitacao = Solicitacao::getList($g);
        $this->set('Solicitacao', $Solicitacao);

    }

    function post_solicitacaoContasPagar()
    {
        /*
        para que os valores do contaspagar de uma solicitacao fique ativo
        precisa mudar o status para 1, esta funcao faz isso e tbm muda o situacao_solicitacao_id
        da solicitacao para aceite, tirando da listagem de esperando aceite
        */
        $this->setTitle('Aceite Solicitacoes');
        $contasIds = $_POST['contaspagar_id'];
        $recusado = $_POST['recusar'];
        $meioDPagamnto = $_POST['pagamento'];
        if ($recusado == "Recusar") {
            foreach ($contasIds as $t) {
                $Contaspagar = new Contaspagar((int)$t);
                $Contaspagar->status = 3;
                $Contaspagar->save($Contaspagar);
            }
            try {
                $Solicitacao = new Solicitacao((int)$_POST['solicitacao_id']);
                $Solicitacao->situacao_solicitacao_id = 2; //aceita
                $Solicitacao->motivo_recusa = $_POST['motivo_recusa'];
                $Solicitacao->autorizado_por = Session::get('user')->id; //salva quem autorizou
                $Solicitacao->autorizado_dt = date('Y-m-d H:i:s');
                $Solicitacao->save($Solicitacao);
                new Msg(__('Recusado com sucesso'));
                $this->go('Contaspagar', 'solicitacao');
            } catch (Exception $e) {
                new Msg($e->getMessage(), 3);
            }
        }
        else {
            try {
                $Solicitacao = new Solicitacao((int)$_POST['solicitacao_id']);
                $revisorCriteria = new Criteria();
                $revisorCriteria->addCondition('permitir_revisar_solicitacao', '=', '1');
                $revisor = Usuario::getFirst($revisorCriteria);

                 if ($Solicitacao->valor_solicitacao >= 5000) {
                     if(Session::get('user')->permitir_revisar_solicitacao == 0) {
                         $Solicitacao->autorizado_por = Session::get('user')->id; //salva quem autorizou
                         $Solicitacao->autorizado_dt = date('Y-m-d H:i:s');
                         $Solicitacao->situacao_solicitacao_id = 5;
                         $Solicitacao->responsavel_anterior = $Solicitacao->responsavel;
                         $Solicitacao->responsavel = $revisor->id;
                         $Solicitacao->save($Solicitacao);
                         new Msg(__('Aguardando Revisão do '.$revisor->nome));
                     } else{
                         if(Session::get('user')->id == $Solicitacao->responsavel){
                             $Solicitacao->autorizado_por = Session::get('user')->id; //salva quem autorizou
                             $Solicitacao->autorizado_dt = date('Y-m-d H:i:s');
                         }
                         $Solicitacao->situacao_solicitacao_id = 3;
                         $Solicitacao->revisado_por = Session::get('user')->id;
                         $Solicitacao->dt_revisao = date('Y-m-d H:i:s');
                         $Solicitacao->save($Solicitacao);
                         foreach ($contasIds as $t) {
                             $Contaspagar = new Contaspagar((int)$t);
                             $Contaspagar->status = 1;
                             $Contaspagar->save($Contaspagar);
                         }
                         new Msg(__('Aceito com sucesso'));
                     }

                } else {
                    $Solicitacao->autorizado_por = Session::get('user')->id; //salva quem autorizou
                    $Solicitacao->autorizado_dt = date('Y-m-d H:i:s');
                    $Solicitacao->situacao_solicitacao_id = 3; //aceita
                    $Solicitacao->save($Solicitacao);

                    foreach ($contasIds as $t) {
                        $Contaspagar = new Contaspagar((int)$t);
                        $Contaspagar->status = 1;
                        $Contaspagar->save($Contaspagar);
                    }
                     new Msg(__('Aceito com sucesso'));
                }
                $this->go('Contaspagar', 'solicitacao');
            } catch (Exception $e) {
                new Msg($e->getMessage(), 3);
            }
        }
    }

    # Confirma a exclusão ou não de um(a) Contaspagar
    # renderiza a /view/Contaspagar/delete.php
    function deletes()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Contaspagar = new Contaspagar((int)$_POST['id']);
            //$Contaspagar->status = 3;
            $Contaspagar->delete();
            $ret['result'] = true;
            $ret['msg'] = 'Parcela apagada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Parcela !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function gerarParcelas()
    {
        $ordNome = new Criteria();
        $ordNome->setOrder('nome');
        $this->set('Programacao', new Programacao);

        $solicitacaoCriteria = new Criteria();
        $solicitacaoCriteria->addCondition('id', '=', $this->getParam('id'));
        $this->set('Solicitacao', Solicitacao::getFirst($solicitacaoCriteria));

        $this->set('Planocontas', Planocontas::TipoPlano(2));

        $this->set('Contabilidades', Contabilidade::getList($ordNome));

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        //$m->addSqlConditions('(usado = "Contas Pagar" OR usado = "Ambas")');
        $this->set("tipoDocumento", Tipo_documento::getList($m));

        $mm = new Criteria();
        $mm->addCondition("status_id", "=", 1);
        //$mm->addSqlConditions('(usado = "Contas Pagar" OR usado = "Ambas")');
        $this->set("tipoPagamento", Tipo_pagamento::getList($mm));

        $this->set('forncedorSelect', new Fornecedor());

        $l = new Criteria();
        $l->addCondition("status", "=", "1");
        $l->setOrder('nome');
        $this->set('Periodicidade', Periodicidade::getList($l));
    }


    function post_gerarParcelas()
    {
        $Solicitacao = new Solicitacao((int)$_POST['solicitacao_id']);
        $Solicitacao->situacao_solicitacao_id = 1; //aceita
        $Solicitacao->save($Solicitacao);

        $l = new Criteria();
        $l->addCondition("status", "=", "1");
        $l->addCondition("id", "=", $_POST['periodicidade']);
        $l->setOrder('nome');
        $periodo = Periodicidade::getFirst($l);

//        //caso nao aja prazo determinado
        if (empty($_POST['prazoDeterminado']) || $_POST['prazoDeterminado'] <= 0) {
            //caso a periodicidade seja menor que 31 dias irei gerar 12 parcelas

            if ($periodo->dias <= 31) {
                $cont_per = 12;
            } else {
                $cont_per = 6;
            }
            $inicio = !empty($_POST['primeiro_vencimento']) ? convertDataBR4SQL($_POST['primeiro_vencimento']) : date('Y-m-d', strtotime('+' . $periodo->dias . ' days', strtotime(date('Y-m-d'))));
        } else {
            $cont_per = $_POST['prazoDeterminado'];
            $inicio = !empty($_POST['primeiro_vencimento']) ? convertDataBR4SQL($_POST['primeiro_vencimento']) : date('Y-m-d', strtotime('+' . $periodo->dias . ' days', strtotime(date('Y-m-d'))));
//                    $feriados = getFeriados(date('Y', strtotime($inicio)));
//                    $inicio = tratar_feriados($inicio, $feriados);
        }

        $aux_x = 1;
        for ($x = 1; $x <= $cont_per; $x++) {
            $concasPagar = new Contaspagar();
            $concasPagar->data = !empty($_POST['data_documento']) ? convertDataBR4SQL($_POST['data_documento']) : date('Y-m-d');

            if ($x > $aux_x) {
                $inicio = date('Y-m-d', strtotime('+' . $periodo->dias . ' days', strtotime($inicio)));
                if (!empty($_POST['diaVencimento']) && $periodo->dias > 15)
                    $inicio = tratar_ultimos_dias_do_mes($_POST['diaVencimento'], $inicio);
//                    $feriados = getFeriados(date('Y', strtotime($inicio)));
//                    $inicio = tratar_feriados($inicio, $feriados);
            }

            $concasPagar->fin_projeto_unidade_id = $_POST['fin_projeto_unidade_id'];
            $concasPagar->vencimento = $inicio;
            $concasPagar->valor = getAmount($_POST['valorParcela']);
            $concasPagar->juros = 0;
            $concasPagar->multa = 0;
            $concasPagar->dataPagamento = NULL;
            $concasPagar->idPlanoContas = $_POST['idPlanoContas'];
            $concasPagar->idFornecedor = $_POST['idFornecedor'];
            $concasPagar->status = 4;
            $concasPagar->complemento = $_POST['complemento'];
            $concasPagar->valorBruto = getAmount($_POST['valorParcela']);
            $concasPagar->desconto = 0;
            $concasPagar->tipoPagamento = $_POST['tipoPagamento'];
            $concasPagar->tipoDocumento = $_POST['tipoDocumento'];
            $concasPagar->solicitacao_id = $_POST['solicitacao_id'];
            $concasPagar->contabilidade = $_POST['contabilidade'];
            $concasPagar->numerodocumento = $_POST['mumero_documento'];
            $concasPagar->data_cadastro = date('Y-m-d H:i:s');
            $concasPagar->parcela = $x . '/' . $cont_per;
            $concasPagar->save();
        }
        $this->go('Contaspagar', 'contasLista', array('id' => $_POST['solicitacao_id']));
    }
}
   