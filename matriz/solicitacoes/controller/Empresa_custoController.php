<?php
final class Empresa_custoController extends AppController{ 

    # página inicial do módulo Empresa_custo
    function index(){
        $this->setTitle('Empresa_custo');
    }

    # lista de Empresa_custos
    # renderiza a visão /view/Empresa_custo/all.php
    function all(){
        $this->setTitle('Empresa_custos');
    }

    # visualiza um(a) Empresa_custo
    # renderiza a visão /view/Empresa_custo/view.php
    function view(){
        try {
            $this->set('Empresa_custo', new Empresa_custo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Empresa_custo', 'all');
        }
    }

    # formulário de cadastro de Empresa_custo
    # renderiza a visão /view/Empresa_custo/add.php
    function add(){
        $this->setTitle('Adicionar Empresa_custo');
        $this->set('Empresa_custo', new Empresa_custo);
        $this->set('Centro_custos',  Centro_custo::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
    }

    # recebe os dados enviados via post do cadastro de Empresa_custo
    # (true)redireciona ou (false) renderiza a visão /view/Empresa_custo/add.php
    function post_add(){
        $this->setTitle('Adicionar Empresa_custo');
        $Empresa_custo = new Empresa_custo();
        $this->set('Empresa_custo', $Empresa_custo);
        try {
            $Empresa_custo->save($_POST);
            new Msg(__('Empresa_custo cadastrado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('Empresa_custo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Centro_custos',  Centro_custo::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
    }

    # formulário de edição de Empresa_custo
    # renderiza a visão /view/Empresa_custo/edit.php
    function edit(){
        $this->setTitle('Editar Empresa_custo');
        try {
            $this->set('Empresa_custo', new Empresa_custo((int) $this->getParam('id')));
            $this->set('Centro_custos',  Centro_custo::getList());
            $this->set('Contabilidades',  Contabilidade::getList());
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Empresa_custo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Empresa_custo
    # (true)redireciona ou (false) renderiza a visão /view/Empresa_custo/edit.php
    function post_edit(){
        $this->setTitle('Editar Empresa_custo');
        try {
            $Empresa_custo = new Empresa_custo((int) $_POST['id']);
            $this->set('Empresa_custo', $Empresa_custo);
            $Empresa_custo->save($_POST);
            new Msg(__('Empresa_custo atualizado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('Empresa_custo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Centro_custos',  Centro_custo::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
    }

    # Recebe o id via post e exclui um(a) Empresa_custo
    # redireciona para Empresa_custo/all
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $Empresa_custo = new Empresa_custo((int) $_POST['id']);
            $Empresa_custo->status=3;
            $Empresa_custo->save();
            $ret['result'] = true;
            $ret['msg'] = 'Centro de custo apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Centro de custo !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function combo(){
        $a = new Criteria();
        $a->addCondition("empresa",'=',$_POST['id']);
        $Empresa_custo = Empresa_custo::getList($a);
        $option = "<option value=''>Selecione</option>";
        foreach ($Empresa_custo as $e) {
            $option .= "<option value='".$e->getCentro_custo()->id."'>". $e->getCentro_custo()->descricao ."</option>";
        }
        echo $option;

        exit;
    }

    function getEmpresaCusto(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM empresa_custo AS e WHERE e.status <> 3";
            $sql = "SELECT e.id, c.nome AS empresa, t.descricao AS centro, s.descricao AS status
                    FROM empresa_custo AS e INNER JOIN status AS s ON (s.id = e.status)
                    INNER JOIN contabilidade AS c ON (c.id = e.empresa)
                    INNER JOIN centro_custo AS t ON (t.id = e.centro_custo)
                    WHERE e.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND (c.nome LIKE :termo OR t.descricao LIKE :termo)";
                $sql .= " AND (c.nome LIKE :termo OR t.descricao LIKE :termo)";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.nome";
                $sort[] = array('empresa' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }
}