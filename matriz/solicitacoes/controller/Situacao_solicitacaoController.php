<?php
final class Situacao_solicitacaoController extends AppController{ 

    # página inicial do módulo Situacao_solicitacao
    function index(){
        $this->setTitle('Visualização de Situacao_solicitacao');
    }

    # lista de Situacao_solicitacaos
    # renderiza a visão /view/Situacao_solicitacao/all.php
    function all(){
        $this->setTitle('Listagem de Situacao_solicitacao');
        $p = new Paginate('Situacao_solicitacao', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Situacao_solicitacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Situacao_solicitacao
    # renderiza a visão /view/Situacao_solicitacao/view.php
    function view(){
        $this->setTitle('Visualização de Situacao_solicitacao');
        try {
            $this->set('Situacao_solicitacao', new Situacao_solicitacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Situacao_solicitacao', 'all');
        }
    }

    # formulário de cadastro de Situacao_solicitacao
    # renderiza a visão /view/Situacao_solicitacao/add.php
    function add(){
        $this->setTitle('Cadastro de Situacao_solicitacao');
        $this->set('Situacao_solicitacao', new Situacao_solicitacao);
    }

    # recebe os dados enviados via post do cadastro de Situacao_solicitacao
    # (true)redireciona ou (false) renderiza a visão /view/Situacao_solicitacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Situacao_solicitacao');
        $Situacao_solicitacao = new Situacao_solicitacao();
        $this->set('Situacao_solicitacao', $Situacao_solicitacao);
        try {
            $Situacao_solicitacao->save($_POST);
            new Msg(__('Situacao_solicitacao cadastrado com sucesso'));
            $this->go('Situacao_solicitacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Situacao_solicitacao
    # renderiza a visão /view/Situacao_solicitacao/edit.php
    function edit(){
        $this->setTitle('Edição de Situacao_solicitacao');
        try {
            $this->set('Situacao_solicitacao', new Situacao_solicitacao((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Situacao_solicitacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Situacao_solicitacao
    # (true)redireciona ou (false) renderiza a visão /view/Situacao_solicitacao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Situacao_solicitacao');
        try {
            $Situacao_solicitacao = new Situacao_solicitacao((int) $_POST['id']);
            $this->set('Situacao_solicitacao', $Situacao_solicitacao);
            $Situacao_solicitacao->save($_POST);
            new Msg(__('Situacao_solicitacao atualizado com sucesso'));
            $this->go('Situacao_solicitacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Situacao_solicitacao
    # renderiza a /view/Situacao_solicitacao/delete.php
    function delete(){
        $this->setTitle('Apagar Situacao_solicitacao');
        try {
            $this->set('Situacao_solicitacao', new Situacao_solicitacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Situacao_solicitacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Situacao_solicitacao
    # redireciona para Situacao_solicitacao/all
    function post_delete(){
        try {
            $Situacao_solicitacao = new Situacao_solicitacao((int) $_POST['id']);
            $Situacao_solicitacao->delete();
            new Msg(__('Situacao_solicitacao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Situacao_solicitacao', 'all');
    }

}