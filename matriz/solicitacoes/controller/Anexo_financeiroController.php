<?php

final class Anexo_financeiroController extends AppController
{

    # página inicial do módulo Anexo_financeiro
    function index()
    {
        $this->setTitle('Visualização de Anexo');
    }

    # lista de Anexo_financeiros
    # renderiza a visão /view/Anexo_financeiro/all.php
    function all()
    {
        $this->setTitle('Listagem de Anexo');
        $p = new Paginate('Anexo_financeiro', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        $c->addCondition('id_externo','=',$this->getParam('id_externo'));
        $c->addCondition('situacao','=',1);

        if(empty($this->getParam('tipo'))) {
            $db = $this::getConn();
            $db->query("SELECT at.id FROM anexo_tipo at WHERE at.origem = :origem");
            $db->bind(':origem', $this->getParam('origem'));
            $db->execute();
            $tipos = $db->fetchAll(PDO::FETCH_COLUMN);
            $tipos = implode(',', $tipos);
            $c->addSqlConditions("tipo IN ($tipos)");
        }else{
            $c->addCondition('tipo','=',$this->getParam('tipo'));
        }

        $anexoTiposCriteria = new Criteria();
        $anexoTiposCriteria->addCondition('origem','=',$this->getParam('origem'));
        $anexoTiposCriteria->addCondition('situacao','=',1);
        $anexoTiposCriteria->setOrder('nome ASC');


        $this->set("tiposDoc",Anexo_tipo::getList($anexoTiposCriteria));
        $this->set('Anexo_financeiros', $p->getPage($c));

        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Anexo_financeiro
    # renderiza a visão /view/Anexo_financeiro/view.php
    function view()
    {
        $this->setTitle('Visualização de Anexo');
        try {
            $this->set('Anexo_financeiro', new Anexo_financeiro((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexo_financeiro', 'all');
        }
    }

    function galeria(){
        $this->setTitle('Listagem de Anexo');
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $db = $this::getConn();
        $db->query("SELECT at.id FROM anexo_tipo at WHERE at.origem = :origem");
        $db->bind(':origem',$this->getParam('origem'));
        $db->execute();
        $tipos = $db->fetchAll(PDO::FETCH_COLUMN);
        $tipos = implode(',',$tipos);

        $c->addCondition('id_externo','=',$this->getParam('id_externo'));
        $c->addCondition('situacao','=',1);
        $c->addSqlConditions("tipo IN ($tipos)");


        $this->set('Anexo_financeiros', Anexo_financeiro::getList($c));
    }


    # formulário de cadastro de Anexo_financeiro
    # renderiza a visão /view/Anexo_financeiro/add.php
    function add()
    {
        $this->setTitle('Cadastro de Anexo');
        $anexoTipoCriteria = new Criteria();
        $anexoTipoCriteria->addCondition('origem','=',$this->getParam('origem'));
        $anexoTipoCriteria->addCondition('situacao','=',1);
        $anexoTipoCriteria->setOrder('nome ASC');
        $tipos = Anexo_tipo::getList($anexoTipoCriteria);
        $this->set('Anexo_financeiro', new Anexo_financeiro);
        $this->set('tipos',$tipos);

    }

    # recebe os dados enviados via post do cadastro de Anexo_financeiro
    # (true)redireciona ou (false) renderiza a visão /view/Anexo_financeiro/add.php
    function post_add()
    {
        $maxfile = 50000000;
        try {

            $numFile = count(array_filter($_FILES['caminho']['name']));
            if (empty($_POST['tipo']))
                throw new Exception('Houve perda de variavel do tipo anexo');

            if (empty($_POST['id_externo']))
                throw new Exception('Houve perda de variavel do id externo');

            if (empty($_FILES['caminho']))
                throw new Exception('Ensira um anexo');

            $extencoes = array('jpg', 'pdf', 'gif', 'mp3', 'mp4', 'odf', 'docx', 'doc', 'txt', 'ppd', 'ppx', 'xlsx', 'pptx', 'xls', 'png', 'zip', 'rar');

            //busca do banco o diretorio
            $anexoTipoCriteria = new criteria();
            $anexoTipoCriteria->addCondition('id', '=', $_POST['tipo']);
            $anexoTipo = Anexo_tipo::getFirst($anexoTipoCriteria);
            $dir = $anexoTipo->dir . '/' . $_POST['id_externo'];

            //use sempre $_FILES para não perder referencia
            for ($i = 0; $i < $numFile; $i++) {

                $Anexo_financeiro = new Anexo_financeiro();

                //gera o nome do arquivo unico
                //pathinfo pega extensao
                $nome = uniqid() . '.' . pathinfo($_FILES['caminho']['name'][$i], PATHINFO_EXTENSION);

                $files = array(
                    "name" => $_FILES['caminho']['name'][$i],
                    "type" => $_FILES['caminho']['type'][$i],
                    "tmp_name" => $_FILES['caminho']['tmp_name'][$i],
                    "error" => $_FILES['caminho']['error'][$i],
                    "size" => $_FILES['caminho']['size'][$i],
                );

                $Arquivos = new FileUploader($files, $maxfile, $extencoes);
                $user = Session::get('user');

                //se salvar o arquivo certinho salva no banco
                if ($Arquivos->save($nome, $dir)) {
                    $file = array(
                        'titulo' => $_POST['titulo'],
                        'caminho' => SITE_PATH . "/uploads/" . $dir . '/' . $nome,
                        'tipo' => $_POST['tipo'],
                        'situacao' => 1,
                        'id_externo' => $_POST['id_externo'],
                        'criadopor' => $user->id,
                        'dtcriacao'=> date("Y-m-d H:i:s")
                    );
                    $Anexo_financeiro->save($file);
                }
            }
            echo "cadastro realizado com sucesso!";
            http_response_code(200);
            exit();
        } catch (Exception $e) {
            echo $e->getMessage();
            http_response_code(500);
            exit();
        }
    }

    # formulário de edição de Anexo_financeiro
    # renderiza a visão /view/Anexo_financeiro/edit.php
    function edit()
    {
        $this->setTitle('Edição de Anexo');
        try {
            $this->set('Anexo_financeiro', new Anexo_financeiro((int)$this->getParam('id')));
            $anexoTipoCriteria = new Criteria();
            $anexoTipoCriteria->addCondition('origem','=',$this->getParam('origem'));
            $anexoTipoCriteria->addCondition('situacao','=',1);
            $anexoTipoCriteria->setOrder('nome ASC');
            $tipos = Anexo_tipo::getList($anexoTipoCriteria);
            $this->set('tipos',$tipos);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Anexo_financeiro', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Anexo_financeiro
    # (true)redireciona ou (false) renderiza a visão /view/Anexo_financeiro/edit.php
    function post_edit()
    {
        $Anexo_financeiro = new Anexo_financeiro((int)$_POST['id']);

        $maxfile = 50000000;
        try {

            $numFile = count(array_filter($_FILES['caminho']['name']));
            if (empty($_POST['id']))
                throw new Exception('Houve perda de variavel do id do anexo');

            if (empty($_POST['tipo']))
                throw new Exception('Houve perda de variavel do tipo anexo');

            if (empty($_POST['id_externo']))
                throw new Exception('Houve perda de variavel do id externo');

            $user = Session::get('user');
            $file = array(
                'id' => $_POST['id'],
                'titulo' => $_POST['titulo'],
                'tipo' => $_POST['tipo'],
                'situacao' => 1,
                'id_externo' => $_POST['id_externo'],
                'alteradopor'=> $user->id,
                'dtalteracao'=> date("Y-m-d H:i:s")
            );

            if(!empty($_FILES['caminho']))
            {

                if(!empty($Anexo_financeiro->caminho))
                    unlink($Anexo_financeiro->caminho);

                $extencoes = array('jpg', 'pdf', 'gif', 'mp3', 'mp4', 'odf', 'docx', 'doc', 'txt', 'ppd', 'ppx', 'xlsx', 'pptx', 'xls', 'png', 'zip', 'rar');

                //busca do banco o diretorio
                $anexoTipoCriteria = new criteria();
                $anexoTipoCriteria->addCondition('id', '=', $_POST['tipo']);
                $anexoTipo = Anexo_tipo::getFirst($anexoTipoCriteria);
                $dir = $anexoTipo->dir . '/' . $_POST['id_externo'];

                //use sempre $_FILES para não perder referencia
                for ($i = 0; $i < $numFile; $i++) {

                    //gera o nome do arquivo unico
                    //pathinfo pega extensao
                    $nome = uniqid() . '.' . pathinfo($_FILES['caminho']['name'][$i], PATHINFO_EXTENSION);

                    $files = array(
                        "name" => $_FILES['caminho']['name'][$i],
                        "type" => $_FILES['caminho']['type'][$i],
                        "tmp_name" => $_FILES['caminho']['tmp_name'][$i],
                        "error" => $_FILES['caminho']['error'][$i],
                        "size" => $_FILES['caminho']['size'][$i],
                    );

                    $Arquivos = new FileUploader($files, $maxfile, $extencoes);

                    //se salvar o arquivo certinho salva no banco
                    if ($Arquivos->save($nome, $dir)) {
                        $file['caminho'] = SITE_PATH . "/uploads/" . $dir . '/' . $nome;
                        $Anexo_financeiro->save($file);
                    }
                }
            }
            else if(empty($_FILES['caminho'])) {
                $file['caminho'] = $_POST['path'];
                $Anexo_financeiro->save($file);
            }
            echo "Anexo atualizado com sucesso!";
            http_response_code(200);
            exit();
        } catch (Exception $e) {
            echo $e->getMessage();
            http_response_code(500);
            exit();
        }

    }

    # Confirma a exclusão ou não de um(a) Anexo_financeiro
    # renderiza a /view/Anexo_financeiro/delete.php
    function delete()
    {

        $this->setTitle('Apagar Anexo');
        try {
            $anexo = new Anexo_financeiro((int)$this->getParam('id'));
            $this->set('Anexo_financeiro', $anexo);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexo_financeiro', 'all',array('id_externo'=>$this->getParam('id_externo'), 'tipo'=>$this->getParam('tipo')));
        }
    }

    function delete_anexo(){
        $Anexo_financeiro = new Anexo_financeiro((int)$this->getParam('id'));
        unlink($Anexo_financeiro->caminho);
        $file['caminho'] = '';
        $Anexo_financeiro->save($file);
        echo json_encode(1);
        exit();
    }

    # Recebe o id via post e exclui um(a) Anexo_financeiro
    # redireciona para Anexo_financeiro/all
    function post_delete()
    {
        try {
            $user = Session::get('user');
            $Anexo_financeiro = new Anexo_financeiro((int)$_POST['id']);
            $Anexo_financeiro->situacao = 3;
            $Anexo_financeiro->alteradopor = $user->id;
            $Anexo_financeiro->dtalteracao = date("Y-m-d H:i:s");
            $Anexo_financeiro->save();
            new Msg(__('Anexo apagado com sucesso'), 1);
            $this->go('Anexo_financeiro', 'all',array('id_externo'=>$Anexo_financeiro->id_externo,'tipo'=>$Anexo_financeiro->tipo));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }

    }

}