<?php
final class PlanocontasController extends AppController{ 

    # página inicial do módulo Planocontas
    function index(){
        $this->setTitle('Planocontas');
    }

    # lista de Planocontas
    # renderiza a visão /view/Planocontas/all.php
    function all(){
        $this->setTitle('Planocontas');

        $a = new Criteria();
        $a->addCondition('status','=',1);
        $a->setOrder('descricao ASC');
        $this->set('Classificacao',Classificacao::getList($a));

    }

    # visualiza um(a) Planocontas
    # renderiza a visão /view/Planocontas/view.php
    function view(){
        try {
            $this->set('Planocontas', new Planocontas((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Planocontas', 'all');
        }
    }

    # formulário de cadastro de Planocontas
    # renderiza a visão /view/Planocontas/add.php
    function add(){
        $this->setTitle('Adicionar Planocontas');
        $this->set('Planocontas', new Planocontas);

        $a = new Criteria();
        $a->setOrder('descricao');
        $this->set('Classificacaos', Classificacao::getList($a));
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Planocontas
    # (true)redireciona ou (false) renderiza a visão /view/Planocontas/add.php
    function post_add(){
        $this->setTitle('Adicionar Planocontas');
        $Planocontas = new Planocontas();
        $this->set('Planocontas', $Planocontas);
        try {
            $Planocontas->save($_POST);
            new Msg(__('Planocontas cadastrado com sucesso'));
            $this->go('Planocontas', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Classificacaos',  Classificacao::getList());
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Planocontas
    # renderiza a visão /view/Planocontas/edit.php
    function edit(){
        $this->setTitle('Editar Planocontas');
        try {
            $this->set('Planocontas', new Planocontas((int) $this->getParam('id')));
            $a = new Criteria();
            $a->setOrder('descricao');
            $this->set('Classificacaos', Classificacao::getList($a));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Planocontas', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Planocontas
    # (true)redireciona ou (false) renderiza a visão /view/Planocontas/edit.php
    function post_edit(){
        $this->setTitle('Editar Planocontas');
        try {
            $Planocontas = new Planocontas((int) $_POST['id']);
            $this->set('Planocontas', $Planocontas);
            $Planocontas->save($_POST);
            new Msg(__('Planocontas atualizado com sucesso'));
            $this->go('Planocontas', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Classificacaos',  Classificacao::getList());
        $this->set('Status',  Status::getList());
    }

    # Recebe o id via post e exclui um(a) Planocontas
    # redireciona para Planocontas/all
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $Planocontas = new Planocontas((int) $_POST['id']);
            $Planocontas->status=3;
            $Planocontas->save();
            $ret['result'] = true;
            $ret['msg'] = 'Cliente apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir cliente !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function automatico(){
        $plano = new Planocontas((int)$_POST['id']);
        echo $plano->rateio;
        exit;
    }

    function findPlanos(){
        try {
            $termo = $this->getParam('termo');
            $size = (int)$this->getParam('size');
            $page = (int)$this->getParam('page');

            if(!isset($termo))
                $termo = '';

            if(!isset($size) || $size < 1)
                $size = 10;

            if(!isset($page) || $page < 1)
                $page = 1;

            $db = $this::getConn();
            $sqlCount = "SELECT count(*) AS count FROM `planocontas` WHERE status =1 AND `nome` LIKE :termo";
            $sql = "SELECT `id`,`nome` FROM `planocontas` WHERE status = 1 AND `nome` LIKE :termo LIMIT :li OFFSET :off";

            $db->query($sqlCount);
            $db->bind(":termo", $termo . "%", PDO::PARAM_STR);
            $db->execute();

            $ret = array();
            $ret["total"] = $db->getResults()[0]->count;
            $ret["dados"] = array();

            $db->query($sql);
            $db->bind(":termo", $termo . "%", PDO::PARAM_STR);
            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

            $dados = $db->getResults();

            foreach ($dados as $d) {
                $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
            }

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    function getPlanos(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $classificacao = $this->getParam('classificacao');
            $tipo = $this->getParam('tipo');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM planocontas AS p WHERE p.status <> 3";
            $sql = "SELECT p.id, p.nome, c.descricao AS classificacao, s.descricao AS status,
                    IF(p.rateio = 1, 'Sim', 'N&atilde;o') AS rateio
                    FROM planocontas AS p INNER JOIN status AS s ON (s.id = p.status)
                    INNER JOIN classificacaocontas AS c ON (c.id = p.classificacao)
                    WHERE p.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND p.nome LIKE :termo";
                $sql .= " AND p.nome LIKE :termo";
            }

            if (!empty($classificacao)) {
                $sqlCount .= " AND c.id = :classificacao";
                $sql .= " AND c.id = :classificacao";
            }

            if (!empty($tipo)) {
                $sqlCount .= " AND c.tipo = :tipo";
                $sql .= " AND c.tipo = :tipo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY nome";
                $sort[] = array('nome' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            if (!empty($classificacao)) {
                $db->bind(":classificacao", $classificacao);
            }

            if (!empty($tipo)) {
                $db->bind(":tipo", $tipo);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            if (!empty($classificacao)) {
                $db->bind(":classificacao", $classificacao);
            }

            if (!empty($tipo)) {
                $db->bind(":tipo", $tipo);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            //$ret['sql'] = $sql;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }
}