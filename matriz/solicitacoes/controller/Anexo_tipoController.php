<?php
final class Anexo_tipoController extends AppController{ 

    # página inicial do módulo Anexo_tipo
    function index(){
        $this->setTitle('Visualização de Anexo_tipo');
    }

    # lista de Anexo_tipos
    # renderiza a visão /view/Anexo_tipo/all.php
    function all(){
        $this->setTitle('Listagem de Anexo_tipo');
        $p = new Paginate('Anexo_tipo', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Anexo_tipos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Anexo_tipo
    # renderiza a visão /view/Anexo_tipo/view.php
    function view(){
        $this->setTitle('Visualização de Anexo_tipo');
        try {
            $this->set('Anexo_tipo', new Anexo_tipo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexo_tipo', 'all');
        }
    }

    # formulário de cadastro de Anexo_tipo
    # renderiza a visão /view/Anexo_tipo/add.php
    function add(){
        $this->setTitle('Cadastro de Anexo_tipo');
        $this->set('Anexo_tipo', new Anexo_tipo);
    }

    # recebe os dados enviados via post do cadastro de Anexo_tipo
    # (true)redireciona ou (false) renderiza a visão /view/Anexo_tipo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Anexo_tipo');
        $Anexo_tipo = new Anexo_tipo();
        $this->set('Anexo_tipo', $Anexo_tipo);
        try {
            $Anexo_tipo->save($_POST);
            new Msg(__('Anexo_tipo cadastrado com sucesso'));
            $this->go('Anexo_tipo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Anexo_tipo
    # renderiza a visão /view/Anexo_tipo/edit.php
    function edit(){
        $this->setTitle('Edição de Anexo_tipo');
        try {
            $this->set('Anexo_tipo', new Anexo_tipo((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Anexo_tipo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Anexo_tipo
    # (true)redireciona ou (false) renderiza a visão /view/Anexo_tipo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Anexo_tipo');
        try {
            $Anexo_tipo = new Anexo_tipo((int) $_POST['id']);
            $this->set('Anexo_tipo', $Anexo_tipo);
            $Anexo_tipo->save($_POST);
            new Msg(__('Anexo_tipo atualizado com sucesso'));
            $this->go('Anexo_tipo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Anexo_tipo
    # renderiza a /view/Anexo_tipo/delete.php
    function delete(){
        $this->setTitle('Apagar Anexo_tipo');
        try {
            $this->set('Anexo_tipo', new Anexo_tipo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexo_tipo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Anexo_tipo
    # redireciona para Anexo_tipo/all
    function post_delete(){
        try {
            $Anexo_tipo = new Anexo_tipo((int) $_POST['id']);
            $Anexo_tipo->delete();
            new Msg(__('Anexo_tipo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Anexo_tipo', 'all');
    }

}