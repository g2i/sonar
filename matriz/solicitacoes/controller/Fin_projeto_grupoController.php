<?php
final class Fin_projeto_grupoController extends AppController{ 

    # página inicial do módulo Fin_projeto_grupo
    function index(){
        $this->setTitle('Visualização de Fin_projeto_grupo');
    }

    # lista de Fin_projeto_grupos
    # renderiza a visão /view/Fin_projeto_grupo/all.php
    function all(){
        $this->setTitle('Listagem de Fin_projeto_grupo');
        $p = new Paginate('Fin_projeto_grupo', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Fin_projeto_grupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Fin_projeto_grupo
    # renderiza a visão /view/Fin_projeto_grupo/view.php
    function view(){
        $this->setTitle('Visualização de Fin_projeto_grupo');
        try {
            $this->set('Fin_projeto_grupo', new Fin_projeto_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fin_projeto_grupo', 'all');
        }
    }

    # formulário de cadastro de Fin_projeto_grupo
    # renderiza a visão /view/Fin_projeto_grupo/add.php
    function add(){
        $this->setTitle('Cadastro de Fin_projeto_grupo');
        $this->set('Fin_projeto_grupo', new Fin_projeto_grupo);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Fin_projeto_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Fin_projeto_grupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Fin_projeto_grupo');
        $Fin_projeto_grupo = new Fin_projeto_grupo();
        $this->set('Fin_projeto_grupo', $Fin_projeto_grupo);
        try {
            $Fin_projeto_grupo->save($_POST);
            new Msg(__('Fin_projeto_grupo cadastrado com sucesso'));
            $this->go('Fin_projeto_grupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Fin_projeto_grupo
    # renderiza a visão /view/Fin_projeto_grupo/edit.php
    function edit(){
        $this->setTitle('Edição de Fin_projeto_grupo');
        try {
            $this->set('Fin_projeto_grupo', new Fin_projeto_grupo((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Fin_projeto_grupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Fin_projeto_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Fin_projeto_grupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Fin_projeto_grupo');
        try {
            $Fin_projeto_grupo = new Fin_projeto_grupo((int) $_POST['id']);
            $this->set('Fin_projeto_grupo', $Fin_projeto_grupo);
            $Fin_projeto_grupo->save($_POST);
            new Msg(__('Fin_projeto_grupo atualizado com sucesso'));
            $this->go('Fin_projeto_grupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Fin_projeto_grupo
    # renderiza a /view/Fin_projeto_grupo/delete.php
    function delete(){
        $this->setTitle('Apagar Fin_projeto_grupo');
        try {
            $this->set('Fin_projeto_grupo', new Fin_projeto_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fin_projeto_grupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Fin_projeto_grupo
    # redireciona para Fin_projeto_grupo/all
    function post_delete(){
        try {
            $Fin_projeto_grupo = new Fin_projeto_grupo((int) $_POST['id']);
            $Fin_projeto_grupo->delete();
            new Msg(__('Fin_projeto_grupo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Fin_projeto_grupo', 'all');
    }

}