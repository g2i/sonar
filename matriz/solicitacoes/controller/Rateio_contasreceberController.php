<?php
final class Rateio_contasreceberController extends AppController{ 

    # página inicial do módulo Rateio_contasreceber
    function index(){
        $this->setTitle('Rateio_contasreceber');
    }

    # lista de Rateio_contasreceberes
    # renderiza a visão /view/Rateio_contasreceber/all.php
    function all(){
        $this->setTitle('Rateio Contas Receber');
        $conta = new Contasreceber((int)$this->getParam("contas_receber"));
        $this->set('contaId', $conta->id);
        $this->set('valor', $conta->valor);
    }

    # visualiza um(a) Rateio_contasreceber
    # renderiza a visão /view/Rateio_contasreceber/view.php
    function view(){
        try {
            $this->set('Rateio_contasreceber', new Rateio_contasreceber((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rateio_contasreceber', 'all');
        }
    }

    # formulário de cadastro de Rateio_contasreceber
    # renderiza a visão /view/Rateio_contasreceber/add.php
    function add(){
        $this->setTitle('Adicionar Rateio');
        $this->set('Contasreceberes', new Contasreceber($this->getParam('id')));
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Clientes', Cliente::getList());
    }

    function getRateios(){
        $ret = array();
        $ret['rateios'] = array();

        try{

            $sql = "SELECT r.id, r.plano_contas_id, r.data_documento, r.cliente_id, r.centro_custo AS centroId, r.contas_receber AS conta,
                    ct.descricao AS centro, e.id AS empresaId, e.nome AS empresa,
                    r.observacao, r.valor, 'S' AS situacao FROM rateio_contasreceber AS r
                    LEFT JOIN contasreceber AS c ON (c.id = r.contas_receber)
                    LEFT JOIN centro_custo AS ct ON (ct.id = r.centro_custo)
                    LEFT JOIN contabilidade AS e ON (e.id = r.empresa)
                    WHERE r.contas_receber = :conta";

            $db = $this::getConn();
            $db->query($sql);
            $db->bind(':conta', $this->getParam('id'));
            $dados = $db->getResults();
            $ret['rateios'] =  empty($dados) ? array() : $dados;

        }catch (Exception $e){
            $ret['error'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function saveRateios(){
        $rateios = json_decode(str_replace('&#34;', '"', $_POST['rateios']));
        $ret = array();
        $ret['result'] = false;

        try {

            foreach ($rateios AS $r) {
                switch ($r->situacao) {
                    case 'D':
                        $rateio = new Rateio_contasreceber((int)$r->id);
                        $rateio->delete();
                        break;

                    case 'E':
                        $rateio = new Rateio_contasreceber((int)$r->id);
                        $rateio->contas_receber = $r->conta;
                        $rateio->valor = $r->valor;
                        $rateio->empresa = $r->empresaId;
                        $rateio->centro_custo = $r->centroId;
                        $rateio->observacao = $r->observacao;
                        $rateio->status = 1;
                        $rateio->plano_contas_id = $r->plano_contas_id;
                        $rateio->cliente_id = $r->cliente_id;
                        $rateio->data_documento = $r->data_documento;
                        $rateio->save();
                        break;

                    case 'N':
                        $rateio = new Rateio_contasreceber();
                        $rateio->contas_receber = $r->conta;
                        $rateio->valor = $r->valor;
                        $rateio->empresa = $r->empresaId;
                        $rateio->centro_custo = $r->centroId;
                        $rateio->observacao = $r->observacao;
                        $rateio->status = 1;
                        $rateio->plano_contas_id = $r->plano_contas_id;
                        $rateio->cliente_id = $r->cliente_id;
                        $rateio->data_documento = $r->data_documento;
                        $rateio->save();
                        break;
                }
            }

            $ret['result'] = true;
            $ret['msg'] = 'Rateios salvo com sucesso !';
        }catch (Exception $e){
            $ret['result'] = false;
            $ret['msg'] = 'Erro ao salvar rateios !';
            $ret['error'] = $e->getMessage();
            $ret['trace'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }
}