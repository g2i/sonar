<?php

final class SolicitacaoController extends AppController
{

    # página inicial do módulo Solicitacao
    function index()
    {
        $this->setTitle('Visualização de Solicitacao');
    }

    # lista de Solicitacaos
    # renderiza a visão /view/Solicitacao/all.php
    function all()
    {
        $this->setTitle('Listagem de Solicitacao');
        $p = new Paginate('Solicitacao', 10);
        $c = new Criteria();

        if (!empty($_GET['data'])) {
            $this->setParam('data', $_GET['data']);
            $c->addCondition('data', ">=", $_GET['data']);
        }

        if (!empty($_GET['idFornecedor'])) {
            $this->setParam('idFornecedor', $_GET['idFornecedor']);
            $c->addCondition('idFornecedor', "=", $_GET['idFornecedor']);
        }

        if (!empty($_GET['situacao_solicitacao_id'])) {
            $this->setParam('situacao_solicitacao_id', $_GET['situacao_solicitacao_id']);
            $c->addCondition('situacao_solicitacao_id', "=", $_GET['situacao_solicitacao_id']);
        } else {
            $c->addCondition('situacao_solicitacao_id', "=", 1);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $user = Session::get('user');
        $c->addCondition('cadastrado_por', "=", $user);
        $this->set('Situacao_solicitacao', Situacao_solicitacao::getList());
        $this->set('Solicitacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $queryFornecedor = new Criteria();
        $queryFornecedor->addCondition('status', '=', 1);
        $queryFornecedor->setOrder('nome ASC');
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList($queryFornecedor));
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Tipo_pagamentos', Tipo_pagamento::getList());
        $this->set('Tipo_documentos', Tipo_documento::getList());
        $this->set('Programacaos', Programacao::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Usuarios', Usuario::getList());


    }

    # visualiza um(a) Solicitacao
    # renderiza a visão /view/Solicitacao/view.php
    function view()
    {
        $this->setTitle('Visualização de Solicitacao');
        try {
            $this->set('Solicitacao', new Solicitacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Solicitacao', 'all');
        }
    }

    # formulário de cadastro de Solicitacao
    # renderiza a visão /view/Solicitacao/add.php
    function add()
    {
        $this->setTitle('Cadastro de Solicitacao');
        $c = new Criteria();
        $c->setOrder("nome");
        $cc = new Criteria();
        $cc->setOrder("nome");

        $m = new Criteria();
        $m->addCondition("status_id", "=", 1);
        $this->set("tipoPagamento", Tipo_pagamento::getList($m));
        $mm = new Criteria();
        $mm->addCondition("status_id", "=", 1);
        $this->set("tipoDocumento", Tipo_documento::getList($mm));

        $this->set('Planocontas', Planocontas::TipoPlano(2));
        $this->set('Contabilidade', Contabilidade::getList($cc));

        $j = new Criteria();
        $j->addCondition('id', '=', 1);
        $this->set('Situacao_solicitacao', Situacao_solicitacao::getList($j));
        $l = new Criteria();
        $l->setOrder("nome");
        $l->addCondition("status", "=", 1);
        $this->set('Periodicidade', Periodicidade::getList($l));

        $g = new Criteria();
        $g->addCondition("realiza_aprovacao_financeiro", "=", 1);
        $this->set('Usuarios', Usuario::getList($g));

        $g = new Criteria();
        $g->addCondition("status", "=", 1);
        $this->set("Centro_custo", Centro_custo::getList($g));

        $criteriaGrupo = new Criteria();
        $criteriaGrupo->addCondition("situacao_id", "=", 1);
        $this->set("Fin_projeto_grupo", Fin_projeto_grupo::getList($criteriaGrupo));


    }

    function teste()
    {
        // emails para quem será enviado o formulário
        require 'vendor/autoload.php';

        $from = new SendGrid\Email(null, "goulart.bass@gmail.com");
        $subject = "G2I Solicitação";
        $to = new SendGrid\Email(null, "goulart.bass@gmail.com");
        $content = new SendGrid\Content("text/html", "Olá, você tem uma nova solicitação em aberta. <br>Link:<br>http://g2ioperacional.com.br/JS/matriz/solicitacoes/login/login/");
        $mail = new SendGrid\Mail($from, $subject, $to, $content);

        //Necessário inserir a chave
        $apiKey = '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';
        $sg = new \SendGrid($apiKey);

        $response = $sg->client->mail()->send()->post($mail);
        //echo $response->statusCode();
        //echo $response->headers();
        //echo $response->body();

    }
    # recebe os dados enviados via post do cadastro de Solicitacao
    # (true)redireciona ou (false) renderiza a visão /view/Solicitacao/add.php
    function post_add()
    {
        $Usuario = new Usuario((int)$_POST['responsavel']);
        //Envio de e-mail
        require 'vendor/autoload.php';
        $from = new SendGrid\Email(null, "suporte@grupog2i.com.br");
        $subject = "G2I Solicitação";
        $to = new SendGrid\Email(null, $Usuario->email);
        $content = new SendGrid\Content("text/html", "Nova Solicitação para autorização, acessar o sistema ou pelo link abaixo. <br>Link:<br>http://g2ioperacional.com.br/JS/matriz/solicitacoes/login/login/");
        $mail = new SendGrid\Mail($from, $subject, $to, $content);

        //Necessário inserir a chave
        $apiKey = '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';
        $sg = new \SendGrid($apiKey);

        $response = $sg->client->mail()->send()->post($mail);
        //echo $response->statusCode();
        //echo $response->headers();
        //echo $response->body();

        /*** */
        $this->setTitle('Cadastro de Solicitacao');
        $Solicitacao = new Solicitacao();
        $this->set('Solicitacao', $Solicitacao);

        try {

            if (!empty($_POST['valor_solicitacao'])) {
                $_POST['valor_solicitacao'] = getAmount($_POST['valor_solicitacao']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (@$_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if (@$_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if (@$_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['datafiscal'])) {
                $_POST['datafiscal'] = convertDataBR4SQL($_POST['datafiscal']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }
            $user = Session::get('user');
            $Solicitacao->data_cadastro = date('Y-m-d H:i:s');
            $Solicitacao->situacao_solicitacao_id = 4;
            $Solicitacao->status = 1;
            $Solicitacao->cadastrado_por = $user->id;
            if ($Solicitacao->save($_POST)) {


            }
            new Msg(__('Solicitacao cadastrado com sucesso'));
            $this->go('Solicitacao', 'edit', array('id' => $Solicitacao->id));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Tipo_pagamentos', Tipo_pagamento::getList());
        $this->set('Tipo_documentos', Tipo_documento::getList());
        $this->set('Programacaos', Programacao::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Usuarios', Usuario::getList());
    }

    # formulário de edição de Solicitacao
    # renderiza a visão /view/Solicitacao/edit.php
    function edit()
    {
        $this->setTitle('Edição de Solicitacao');
        try {
            $solicitacao = new Solicitacao((int)$this->getParam('id'));
            $this->set('Solicitacao', $solicitacao);
            $this->set('Planocontas', Planocontas::TipoPlano(2));

            $this->set('Fornecedores', Fornecedor::getList());
            $c = new Criteria();
            $c->setOrder("nome");

            $cc = new Criteria();
            $cc->setOrder("nome");
            $this->set('Contabilidade', Contabilidade::getList($c));
            $m = new Criteria();
            $m->addCondition("status_id", "=", 1);

            $this->set("tipoPagamento", Tipo_pagamento::getList($m));
            $mm = new Criteria();
            $mm->addCondition("status_id", "=", 1);
            $this->set("tipoDocumento", Tipo_documento::getList($mm));
            $this->set('Programacaos', Programacao::getList());

            $j = new Criteria();
            $j->addCondition('id', '=', 1);
            $this->set('Situacao_solicitacao', Situacao_solicitacao::getList($j));

            $g = new Criteria();
            $g->addCondition("realiza_aprovacao_financeiro", "=", 1);
            $this->set('Usuarios', Usuario::getList($g));

            $sql =  "Select * From view_grupo_empresa_unidade WHERE unidade_id = :unidade";
            $db = $this::getConn();
            $db->query($sql);
            $db->bind(":unidade",$solicitacao->fin_projeto_unidade_id);
            $dados = $db->getResults();
            $dados = $dados[0];

            $criteriaGrupo = new Criteria();
            $criteriaGrupo->addCondition("situacao_id", "=", 1);
            $this->set("Fin_projeto_grupo", Fin_projeto_grupo::getList($criteriaGrupo));

            $criteriaEmpresa = new Criteria();
            $criteriaEmpresa->addCondition("situacao_id", "=", 1);
            $criteriaEmpresa->addCondition("fin_projeto_grupo_id", "=", $dados->grupo_id);
            $this->set("Fin_projeto_empresa", Fin_projeto_empresa::getList($criteriaEmpresa));

            $criteriaUnidade = new Criteria();
            $criteriaUnidade->addCondition("situacao_id", "=", 1);
            $criteriaUnidade->addCondition("fin_projeto_empresa_id", "=", $dados->empresa_id);
            $this->set("Fin_projeto_unidade", Fin_projeto_unidade::getList($criteriaUnidade));

            $this->set("Fin_projeto", $dados);

        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Solicitacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Solicitacao
    # (true)redireciona ou (false) renderiza a visão /view/Solicitacao/edit.php

    function view_solicitacao(){

        $c = new Criteria();
        $solicitacao_id = (int)$this->getParam('id');
        $this->set('Solicitacao_id', $solicitacao_id);

        $p = new Paginate('Contaspagar', 10);
        $c->addCondition('status', '!=', 3);
        $c->addCondition('solicitacao_id', '=', $solicitacao_id);
        $this->set('Contaspagar', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $b = new Criteria();
        $b->addCondition('status', '=', '1');
        $b->setOrder('nome');
        $contabilidade = Contabilidade::getList($b);

        $bb = new Criteria();
        $bb->addCondition('status', '=', '1');
        $bb->setOrder('nome');

        $cS = new Criteria();
        $cS->addCondition('id', '<>', 3);

        $this->set('StatusList', Status::getList($cS));

        $this->set('idPlanoContas', Planocontas::TipoPlano(2));
        $this->set('contabilidade', $contabilidade);

        $g = new Criteria();
        $g->addCondition('id', '=', $solicitacao_id);
        $g->addCondition('status', '=', '1');


        $Solicitacao = Solicitacao::getList($g);
        $this->set('Solicitacao', $Solicitacao);

    }

    function post_edit()
    {
        $this->setTitle('Edição de Solicitacao');
        try {
            $Solicitacao = new Solicitacao((int)$_POST['id']);
            $this->set('Solicitacao', $Solicitacao);

            if (!empty($_POST['valor_solicitacao'])) {
                $_POST['valor_solicitacao'] = getAmount($_POST['valor_solicitacao']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (@$_POST['idFornecedor'] == 0) {
                $_POST['idFornecedor'] = NULL;
            }
            if (@$_POST['idPlanoContas'] == 0) {
                $_POST['idPlanoContas'] = NULL;
            }
            if (@$_POST['contabilidade'] == 0) {
                $_POST['contabilidade'] = NULL;
            }
            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['datafiscal'])) {
                $_POST['datafiscal'] = convertDataBR4SQL($_POST['datafiscal']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }
            $_POST['dt_modificado'] = date('Y-m-d H:i:s');
            $user = Session::get('user');
            $Solicitacao->alterado_por = $user;
            //$Solicitacao->situacao_solicitacao_id = 1;

            $Solicitacao->save($_POST);
            new Msg(__('Solicitacao atualizado com sucesso'));
            $this->go('Solicitacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Tipo_pagamentos', Tipo_pagamento::getList());
        $this->set('Tipo_documentos', Tipo_documento::getList());
        $this->set('Programacaos', Programacao::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Usuarios', Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Solicitacao
    # renderiza a /view/Solicitacao/delete.php
    function delete()
    {
        $this->setTitle('Apagar Solicitacao');
        try {
            $this->set('Solicitacao', new Solicitacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Solicitacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Solicitacao
    # redireciona para Solicitacao/all
    function post_delete()
    {
        try {
            $Solicitacao = new Solicitacao((int)$_POST['id']);
            $Solicitacao->delete();
            new Msg(__('Solicitacao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Solicitacao', 'all');
    }

    function aceitar()
    {
        $this->setTitle('Edição de Solicitacao');
        try {
            $this->set('Solicitacao', new Solicitacao((int)$this->getParam('id')));
            $this->set('Planocontas', Planocontas::getList());
            $this->set('Usuarios', Usuario::getList());
            $this->set('Fornecedores', Fornecedor::getList());
            $c = new Criteria();
            $c->setOrder("nome");

            $cc = new Criteria();
            $cc->setOrder("nome");
            $this->set('Contabilidade', Contabilidade::getList($c));
            $m = new Criteria();
            $m->addCondition("status_id", "=", 1);

            $this->set("tipoPagamento", Tipo_pagamento::getList($m));
            $mm = new Criteria();
            $mm->addCondition("status_id", "=", 1);
            $this->set("tipoDocumento", Tipo_documento::getList($mm));
            $this->set('Programacaos', Programacao::getList());
            $this->set('Usuarios', Usuario::getList());
            $j = new Criteria();
            $j->addCondition('id', '=', 3);
            $this->set('Situacao_solicitacao', Situacao_solicitacao::getList($j));

        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Solicitacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Solicitacao
    # (true)redireciona ou (false) renderiza a visão /view/Solicitacao/edit.php
    /*
    se for do tipo cartão tem que ficar inativo, pois sera vinculado a um contasPagar criado no financeiro 
    que se tornara um rateio
    */
    function post_aceitar()
    {
        $this->setTitle('Aceitar Solicitacao');
        try {
            $Solicitacao = new Solicitacao((int)$_POST['id']);
            $this->set('Solicitacao', $Solicitacao);

            if (!empty($_POST['valor'])) {
                $_POST['valor'] = getAmount($_POST['valor']);
            }
            if (!empty($_POST['valorBruto'])) {
                $_POST['valorBruto'] = getAmount($_POST['valorBruto']);
            }
            if (!empty($_POST['juros'])) {
                $_POST['juros'] = getAmount($_POST['juros']);
            }
            if (!empty($_POST['multa'])) {
                $_POST['multa'] = getAmount($_POST['multa']);
            }
            if (!empty($_POST['desconto'])) {
                $_POST['desconto'] = getAmount($_POST['desconto']);
            }

            if (!empty($_POST['data'])) {
                $_POST['data'] = convertDataBR4SQL($_POST['data']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['vencimento'])) {
                $_POST['vencimento'] = convertDataBR4SQL($_POST['vencimento']);
            }
            if (!empty($_POST['dataPagamento'])) {
                $_POST['dataPagamento'] = convertDataBR4SQL($_POST['dataPagamento']);
            }
            $_POST['autorizado_dt'] = date('Y-m-d H:i:s');
            $user = Session::get('user');
            $_POST['autorizado_por'] = $user->id; // para salvar o usuario que está fazendo
            $Solicitacao->save($_POST);

            for ($x = 1; $x <= $_POST['qtd_parcela']; $x++) {
                $Contaspagar = new Contaspagar();
                $Contaspagar->solicitacao_id = $Solicitacao->id;
                $Contaspagar->data = $Solicitacao->data;
                $Contaspagar->vencimento = $Solicitacao->vencimento;
                $Contaspagar->valor = $Solicitacao->valor;
                $Contaspagar->juros = $Solicitacao->juros;
                $Contaspagar->multa = $Solicitacao->multa;
                $Contaspagar->dataPagamento = $Solicitacao->dataPagamento;
                $Contaspagar->idFornecedor = $Solicitacao->idFornecedor;
                $Contaspagar->status = 1;
                $Contaspagar->complemento = $Solicitacao->observacao;
                $Contaspagar->contabilidade = $Solicitacao->contabilidade;
                $Contaspagar->valorBruto = $Solicitacao->valorBruto;
                $Contaspagar->desconto = $Solicitacao->desconto;
                $Contaspagar->tipoPagamento = $Solicitacao->tipoPagamento;
                $Contaspagar->tipoDocumento = $Solicitacao->tipoDocumento;
                $Contaspagar->numerodocumento = $Solicitacao->numerodocumento;
                $Contaspagar->save($Contaspagar);
            }


            new Msg(__('Solicitacao atualizado com sucesso'));
            $this->go('Solicitacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível aceitar.'), 2);
        }
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Tipo_pagamentos', Tipo_pagamento::getList());
        $this->set('Tipo_documentos', Tipo_documento::getList());
        $this->set('Programacaos', Programacao::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Usuarios', Usuario::getList());
    }

}