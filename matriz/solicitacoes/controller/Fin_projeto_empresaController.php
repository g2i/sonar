<?php
final class Fin_projeto_empresaController extends AppController{ 

    # página inicial do módulo Fin_projeto_empresa
    function index(){
        $this->setTitle('Visualização de Fin_projeto_empresa');
    }

    # lista de Fin_projeto_empresas
    # renderiza a visão /view/Fin_projeto_empresa/all.php
    function all(){
        $this->setTitle('Listagem de Fin_projeto_empresa');
        $p = new Paginate('Fin_projeto_empresa', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Fin_projeto_empresas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Fin_projeto_empresa
    # renderiza a visão /view/Fin_projeto_empresa/view.php
    function view(){
        $this->setTitle('Visualização de Fin_projeto_empresa');
        try {
            $this->set('Fin_projeto_empresa', new Fin_projeto_empresa((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fin_projeto_empresa', 'all');
        }
    }

    # formulário de cadastro de Fin_projeto_empresa
    # renderiza a visão /view/Fin_projeto_empresa/add.php
    function add(){
        $this->setTitle('Cadastro de Fin_projeto_empresa');
        $this->set('Fin_projeto_empresa', new Fin_projeto_empresa);
        $this->set('Fin_projeto_grupos',  Fin_projeto_grupo::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Fin_projeto_empresa
    # (true)redireciona ou (false) renderiza a visão /view/Fin_projeto_empresa/add.php
    function post_add(){
        $this->setTitle('Cadastro de Fin_projeto_empresa');
        $Fin_projeto_empresa = new Fin_projeto_empresa();
        $this->set('Fin_projeto_empresa', $Fin_projeto_empresa);
        try {
            $Fin_projeto_empresa->save($_POST);
            new Msg(__('Fin_projeto_empresa cadastrado com sucesso'));
            $this->go('Fin_projeto_empresa', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Fin_projeto_grupos',  Fin_projeto_grupo::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Fin_projeto_empresa
    # renderiza a visão /view/Fin_projeto_empresa/edit.php
    function edit(){
        $this->setTitle('Edição de Fin_projeto_empresa');
        try {
            $this->set('Fin_projeto_empresa', new Fin_projeto_empresa((int) $this->getParam('id')));
            $this->set('Fin_projeto_grupos',  Fin_projeto_grupo::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Fin_projeto_empresa', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Fin_projeto_empresa
    # (true)redireciona ou (false) renderiza a visão /view/Fin_projeto_empresa/edit.php
    function post_edit(){
        $this->setTitle('Edição de Fin_projeto_empresa');
        try {
            $Fin_projeto_empresa = new Fin_projeto_empresa((int) $_POST['id']);
            $this->set('Fin_projeto_empresa', $Fin_projeto_empresa);
            $Fin_projeto_empresa->save($_POST);
            new Msg(__('Fin_projeto_empresa atualizado com sucesso'));
            $this->go('Fin_projeto_empresa', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Fin_projeto_grupos',  Fin_projeto_grupo::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Fin_projeto_empresa
    # renderiza a /view/Fin_projeto_empresa/delete.php
    function delete(){
        $this->setTitle('Apagar Fin_projeto_empresa');
        try {
            $this->set('Fin_projeto_empresa', new Fin_projeto_empresa((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fin_projeto_empresa', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Fin_projeto_empresa
    # redireciona para Fin_projeto_empresa/all
    function post_delete(){
        try {
            $Fin_projeto_empresa = new Fin_projeto_empresa((int) $_POST['id']);
            $Fin_projeto_empresa->delete();
            new Msg(__('Fin_projeto_empresa apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Fin_projeto_empresa', 'all');
    }

    function getListaEmpresa(){
        $empresaCriteria = new Criteria();
        $empresaCriteria->addCondition('situacao_id','=',1);
        $empresaCriteria->addCondition('fin_projeto_grupo_id','=',$this->getParam('fin_projeto_grupo_id'));

        $data = Fin_projeto_empresa::getList($empresaCriteria);

        echo json_encode($data);
        exit();

    }
}