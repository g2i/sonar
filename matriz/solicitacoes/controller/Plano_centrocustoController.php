<?php
final class Plano_centrocustoController extends AppController{ 

    # página inicial do módulo Plano_centrocusto
    function index(){
        $this->setTitle('Plano_centrocusto');
    }

    # lista de Plano_centrocustos
    # renderiza a visão /view/Plano_centrocusto/all.php
    function all(){
        $this->setTitle('Plano_centrocustos');
        $p = new Paginate('Plano_centrocusto', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (!empty($_GET['search'])) {
            $c->addCondition('contabilidade.nome', 'LIKE', $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        if($this->getParam('id')){
            $c->addCondition('plano_contas','=',$this->getParam('id'));
        }

        $this->set('Plano_centrocustos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Plano_centrocusto
    # renderiza a visão /view/Plano_centrocusto/view.php
    function view(){
        try {
            $this->set('Plano_centrocusto', new Plano_centrocusto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Plano_centrocusto', 'all');
        }
    }

    # formulário de cadastro de Plano_centrocusto
    # renderiza a visão /view/Plano_centrocusto/add.php
    function add(){
        $this->setTitle('Adicionar Plano_centrocusto');
        $this->set('Plano_centrocusto', new Plano_centrocusto);
        $this->set('Planocontas',  Planocontas::getList());
        $this->set('Centro_custos',  Centro_custo::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Plano_centrocusto
    # (true)redireciona ou (false) renderiza a visão /view/Plano_centrocusto/add.php
    function post_add(){
        $this->setTitle('Adicionar Plano_centrocusto');
        $Plano_centrocusto = new Plano_centrocusto();
        $this->set('Plano_centrocusto', $Plano_centrocusto);
        $_POST['porcentagem'] = str_replace(".", "", $_POST['porcentagem']);
        $_POST['porcentagem'] = str_replace(",", ".", $_POST['porcentagem']);
        try {
            $Plano_centrocusto->save($_POST);
            $plano_contas = new Planocontas((int)$_POST['plano_contas']);
            $plano_contas->rateio=1;
            $plano_contas->save();

            new Msg(__('Plano_centrocusto cadastrado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('Plano_centrocusto', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Planocontas',  Planocontas::getList());
        $this->set('Centro_custos',  Centro_custo::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Plano_centrocusto
    # renderiza a visão /view/Plano_centrocusto/edit.php
    function edit(){
        $this->setTitle('Editar Plano_centrocusto');
        try {
            $this->set('Plano_centrocusto', new Plano_centrocusto((int) $this->getParam('id')));
            $this->set('Planocontas',  Planocontas::getList());
            $this->set('Centro_custos',  Centro_custo::getList());
            $this->set('Contabilidades',  Contabilidade::getList());
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Plano_centrocusto', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Plano_centrocusto
    # (true)redireciona ou (false) renderiza a visão /view/Plano_centrocusto/edit.php
    function post_edit(){
        $this->setTitle('Editar Plano_centrocusto');
        try {
            $Plano_centrocusto = new Plano_centrocusto((int) $_POST['id']);
            $this->set('Plano_centrocusto', $Plano_centrocusto);
            $_POST['porcentagem'] = str_replace(".", "", $_POST['porcentagem']);
            $_POST['porcentagem'] = str_replace(",", ".", $_POST['porcentagem']);
            $Plano_centrocusto->save($_POST);
            new Msg(__('Plano_centrocusto atualizado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('Plano_centrocusto', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Planocontas',  Planocontas::getList());
        $this->set('Centro_custos',  Centro_custo::getList());
        $this->set('Contabilidades',  Contabilidade::getList());
        $this->set('Status',  Status::getList());
    }

    # Confirma a exclusão ou não de um(a) Plano_centrocusto
    # renderiza a /view/Plano_centrocusto/delete.php
    function delete(){
        $this->setTitle('Apagar Plano_centrocusto');
        try {
            $this->set('Plano_centrocusto', new Plano_centrocusto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Plano_centrocusto', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Plano_centrocusto
    # redireciona para Plano_centrocusto/all
    function post_delete(){
        try {
            $Plano_centrocusto = new Plano_centrocusto((int) $_POST['id']);
            $Plano_centrocusto->status=3;
            $Plano_centrocusto->save();
            new Msg(__('Plano_centrocusto apagado com sucesso'), 1);
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Plano_centrocusto', 'all');
    }

}