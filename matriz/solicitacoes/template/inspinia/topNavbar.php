<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a role="open-adm" class="animatedClick" data-target='clickExample'>
                    <i class="fa fa-tasks"></i>
                </a>
            </li>
            <li>
                <?php echo $this->Html->getLink('<i class="fa fa-sign-out"></i> Sair', 'Login', 'logout'); ?>
            </li>
        </ul>
    </nav>
</div>

<div id="right-sidebar" style="display: none" role="adm" class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
    <div class="sidebar-container" full-scroll>

        <ul class="nav nav-tabs navs-2" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Administração</a>
            </li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuários</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content" style="background: #2f4050">
            <div role="tabpanel" class="tab-pane active" id="home" >
                <div class="sidebar-title">
                    <h3><i class="fa fa-gears"></i> Ajustes </h3>
                    <small><i class="fa fa-tim"></i> Área do administrador.</small>
                </div>
                <nav class="navbar-default navbar-static" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav" id="adm-menu">
<!--
                            <?php  if(Config::get('origem')==2):?>
                                <li <?php echo(CONTROLLER == "Cliente" ? 'class="active"' : ''); ?>>
                                    <a href="#">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Clientes</span><span
                                            class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-second-level">
                                        <li <?php echo(CONTROLLER == "Cliente" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Cliente', 'all'); ?>
                                        </li>
                                        <li <?php echo(CONTROLLER == "Cliente" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Cliente', 'add'); ?>
                                        </li>
                                    </ul>
                                </li>
                            <?php endif; ?>

                            <li <?php echo(CONTROLLER == "Fornecedor" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Credores</span><span
                                        class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Fornecedor" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Fornecedor', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Fornecedor" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Fornecedor', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Planocontas" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Plano de Contas</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Planocontas" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Planocontas', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Planocontas" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Planocontas', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Classificacao" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Classificação Plano de Contas</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Classificacao" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Classificacao', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Classificacao" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Classificacao', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Centro_custo" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Centro de Custos</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Centro_custo" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Centro_custo', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Centro_custo" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Centro_custo', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Tipo_pagamento" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Tipo Pagamento</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Tipo_pagamento" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Tipo_pagamento', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Tipo_pagamento" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Tipo_pagamento', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Tipo_documento" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Tipo Documento</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Tipo_documento" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Tipo_documento', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Tipo_documento" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Tipo_documento', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Banco" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Bancos</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Banco" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Banco', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Banco" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Banco', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Contabilidade" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Empresas</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Contabilidade" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Contabilidade', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Contabilidade" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Contabilidade', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Empresa_custo" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Empresa x Centro Custos</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Empresa_custo" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Empresa_custo', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Empresa_custo" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Empresa_custo', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "GrupoEmpresa" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Grupo Empresa</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "GrupoEmpresa" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'GrupoEmpresa', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "GrupoEmpresa" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'GrupoEmpresa', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Deducoes" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Deduções</span>
                                    <span class="fa arrow"></span>
                                </a>                     <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Deducoes" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Deducoes', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Deducoes" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Deducoes', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "CorrecaoMonetaria" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Índice de Correção Monetária</span><span
                                        class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "CorrecaoMonetaria" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'CorrecaoMonetaria', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "CorrecaoMonetaria" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'CorrecaoMonetaria', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?php echo(CONTROLLER == "Parametros_contas" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Parâmetros de Contas</span><span
                                        class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Parametros_contas" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> listar', 'Parametros_contas', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Parametros_contas" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Parametros_contas', 'add'); ?>
                                    </li>
                                </ul>
                            </li>

                            <li <?= CONTROLLER == "Usuario" && ACTION != "conta" ? 'class="active"' : '' ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Usuarios</span><span
                                        class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Usuario" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Listar', 'Usuario', 'all'); ?>
                                    </li>
                                    <li <?php echo(CONTROLLER == "Usuario" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Usuario', 'add'); ?>
                                    </li>
                                </ul>
                            </li>
-->
                        </ul>
                    </div>
                </nav>

            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="sidebar-title">
                    <h3><i class="fa fa-users"></i> Usuário</h3>
                    <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                </div>
                <nav class="navbar-default navbar-static" role="navigation">
                    <div class="sidebar-collapse">
                        <ul class="nav" id="user-menu">
                            <li <?php echo(CONTROLLER == "Usuario" && ACTION == "conta" ? 'class="active"' : ''); ?>>
                                <a href="#">
                                    <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-second-level">
                                    <li <?php echo(CONTROLLER == "Usuario" && ACTION == "conta" ? 'class="active"' : ''); ?>>
                                        <?php echo $this->Html->getLink('<i class="fa fa-user"></i>Conta', 'Usuario', 'conta'); ?>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>