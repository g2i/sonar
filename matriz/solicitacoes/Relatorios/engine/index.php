<?php
	require_once("localization.php");
	require_once("database_firebird.php");
	require_once("database_mongodb.php");
	require_once("database_mssql.php");
	require_once("database_mysql.php");
	require_once("database_odbc.php");
	require_once("database_pg.php");
	require_once("database_oracle.php");
	require_once("database_xml.php");
	require_once("handler.php");
	require_once("class.phpmailer.php");
	require_once("class.pop3.php");
	require_once("class.smtp.php");
	require_once("PHPMailerAutoload.php");
	require_once("../../lib/core/Config.php");
	include '../../config.php';

	$enable_compression = true;

	$report_key = sti_get_parameter_value("stimulsoft_report_key");
	$client_key = sti_get_parameter_value("stimulsoft_client_key");
	$client_data = file_get_contents("php://input");


	/**
	 *  Directory, which contains the localization XML files.
	 */
	function sti_get_localization_directory()
	{
		return "./localization";
	}


	/**
	 *  Returns .mrt or .mdc file by string ID that was set when running.
	 *  If necessary, it is possible to change the code for getting a report by its ID from file or from database.
	 */
	function sti_get_report($report_key)
	{
		switch ($report_key) {
			case "cr_g":
				return file_get_contents("../contasReceber_g.mrt");
				break;

			case "cp_g":
				return file_get_contents("../contasPagar_g.mrt");
				break;

			case "cp_new":
				return file_get_contents("../new_contaspagar.mrt");
				break;

			case "cr_new":
				return file_get_contents("../new_contasreceber.mrt");
				break;

			case "pr_r":
				return file_get_contents("../programacao_r.mrt");
				break;

			case "cr_c":
				return file_get_contents("../cr_quitacao.mrt");
				break;

			case "cp_q":
				return file_get_contents("../cp_quitacao.mrt");
				break;

			case "movi":
				return file_get_contents("../movimentacao.mrt");
				break;

			case "cp_prog":
				return file_get_contents("../programacao_c.mrt");
				break;

			case "analit":
				return file_get_contents("../mapa_analitico.mrt");
				break;

			case "sint":
				return file_get_contents("../mapa_sintetico.mrt");
				break;

			case "d_analitico":
				return file_get_contents("../demonstracao_analitico.mrt");
				break;

			case "d_sint":
				return file_get_contents("../demonstracao_sintetico.mrt");
				break;

			case "movi_geral":
				return file_get_contents("../movimentacao_geral.mrt");
				break;
		}

		//if (file_exists("../reports/$report_key")) return file_get_contents("../reports/$report_key");
		// If there is no need to load the report, then the empty string will be sent
		// If you want to display an error message, please use the following format
		return "Relatorio Nao Encontrado";
	}


	/**
	 *  The code for saving a report can be placed in this function.
	 *  
	 *  Response to the client - report key and error code.
	 *  You can use the next error codes:
	 *     -1: the message box is not shown
	 *      0: shows the "Report is successfully saved" message
	 *  In other cases shows a window with the defined value
	 */
	function sti_save_report($report, $report_key, $report_file_name, $new_report_flag)
	{
		// You can change the report key, if necessary
		//if ($new_report_flag == "True") $report_key = "MyReport.mrt";
		
		$error_code = "-1";
		$error_description = "";
		if (file_put_contents("../reports/$report_key", $report) === false)
		{
			$error_code = "Error when saving a report.";
			$arr = error_get_last();
			if (count($arr) > 0)
			{
				$error_description = $arr["message"];
				$error_description = str_replace("&", "&amp;", $error_description);
				$error_description = str_replace("<", "&lt;", $error_description);
				$error_description = str_replace(">", "&gt;", $error_description);
			}
		}
		
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SaveReport><ReportKey>$report_key</ReportKey><ErrorCode>$error_code</ErrorCode><ErrorDescription>$error_description</ErrorDescription></SaveReport>";
	}


	/**
	 *  The function for changing values on parameters by their name in the SQL query.
	 *  Parameters can be set as {ParamName} in the SQL query.
	 *  By default values are taken according to the name of a parameter in the URL string or in the POST data.
	 */
	function sti_get_parameter($parameter_name, $default_value)
	{
		switch ($parameter_name) {

			case "credor":
				if(empty($_GET['credor'])){
					return $_GET['credor']=-1;
				}else
					if($_GET['credor']=='undefined')
						return $_GET['credor']=-1;
				break;

			case "forn":
				if(empty($_GET['forn'])){
					return $_GET['forn']=-1;
				}else
					if($_GET['forn']=='undefined')
						return $_GET['forn']=-1;
				break;

			case "cliente":
				if(empty($_GET['cliente'])){
					return $_GET['cliente']=-1;
				}else
					if($_GET['cliente']=='undefined')
						return $_GET['cliente']=-1;

				break;
		}

		if(!empty($_GET[$parameter_name]))
			return $_GET[$parameter_name];
        else
            return $default_value;
	}


	/**
	 *  Getting the Connection String when requesting the client's Flash application to a database.
	 *  In this function you can change the Connection String of a report.
	 */
	function sti_get_connection_string($connection_type, $connection_string)
	{
		switch ($connection_type) {
			case "StiMySqlDatabase":
				return "Server=".Config::get("db_host").";Database=".Config::get("db_name").";Port=3306;User=".Config::get("db_user").";Password=".Config::get("db_password").";charset=utf8"; break;
		}

		return $connection_string;
	}


	/**
	 *  Saving an exported report.
	 *  Response to the client - error code. Standard codes:
	 *      -1: the message box is not shown
	 *       0: shows the "Report is successfully saved." message
	 *  In other cases shows a window with the defined value
	 */
	function sti_export_report($format, $report_key, $file_name, $data)
	{
		if (file_put_contents("../exports/$file_name", $data) === false) return "Error when saving an exported report.";
		return "-1";
	}


	/**
	 *  Send report by Email
	 */
	function sti_send_email_report($format, $report_key, $email, $subject, $message, $file_name, $data)
	{
		// You should change these parameters according to the requirements.
		$_options = array(
			// Email address of the sender
			"from" => "emailfrom@domain.com",
			// Name and surname of the sender
			"name" => "John Smith",
			// Email address of the recipient
			"to" => $email,
			// Email Subject
			"subject" => $subject,
			// Text of the Email
			"message" => $message,
			// Show a message when Email is successfully sent
			"successfully" => true,
			// The text of the error message if unable to send Email 
			"error" => "An error occurred while sending Email.",
			
			
			// Set to true if authentication is required
			"auth" => false,
			// Address of the SMTP server
			"host" => "smtp.gmail.com",
			// Port of the SMTP server
			"port" => 465,
			// The secure connection prefix - ssl or tls
			"secure" => "ssl",
			// Login (Username or Email)
			"login" => "login",
			// Password
			"password" => "password"
		);
		
		$guid = substr(md5(uniqid().mt_rand()), 0, 12);
		if (!file_exists('tmp')) mkdir('tmp');
		file_put_contents('tmp/'.$guid.'.'.$file_name, $data);
		
		$error = $_options['error'];
		$error_description = '';
		
		$mail = new PHPMailer(true);
		if ($_options['auth']) $mail->IsSMTP();
		try {
			$mail->CharSet = 'UTF-8';
			$mail->IsHTML(false);
			$mail->From = $_options['from'];
			$mail->FromName = $_options['name'];
			
			// Add Emails list
			$emails = preg_split('/,|;/', $_options['to']);
			foreach ($emails as $email) {
				$mail->AddAddress(trim($email));
			}
			
			$mail->Subject = htmlspecialchars($_options['subject']);
			$mail->Body = $_options['message'];
			$mail->AddAttachment('tmp/'.$guid.'.'.$file_name, $file_name);
			
			if ($_options['auth']) {
				$mail->Host = $_options['host'];
				$mail->Port = $_options['port'];
				$mail->SMTPAuth = $_options['auth'];
				$mail->SMTPSecure = $_options['secure'];
				$mail->Username = $_options['login'];
				$mail->Password = $_options['password'];
			}
			
			$mail->Send();
			$error = $_options['successfully'] ? '0' : '-1';
		}
		catch (phpmailerException $e) {
			$error_description = strip_tags($e->errorMessage());
		}
		catch (Exception $e) {
			$error_description = strip_tags($e->getMessage());
		}
		
		unlink('tmp/'.$guid.'.'.$file_name);
		
		if ($error == "0" || $error == "-1") return $error;
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><SendEmail><ErrorCode>$error</ErrorCode><ErrorDescription>$error_description</ErrorDescription></SendEmail>";
	}


	// Processing client Flash application commands
	if (isset($client_key))
	{
		if (!function_exists("gzuncompress")) $enable_compression = false;
		
		if ($enable_compression && $client_key != "ViewerFx" && $client_key != "DesignerFx" && strlen($client_data) > 0)
		{
			$client_data = base64_decode($client_data);
			$client_data = gzuncompress($client_data);
		}
		
		$response = sti_client_event_handler($client_key, $report_key, $client_data, $enable_compression);
		
		if ($enable_compression && strlen($response) > 0 && $client_key != "ViewerFx" && $client_key != "DesignerFx")
		{
			$response = gzcompress($response);
			$response = base64_encode($response);
		}
		
		echo $response;
	}

?>