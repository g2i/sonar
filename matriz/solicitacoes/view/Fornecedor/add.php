<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Credor</h2>
        <ol class="breadcrumb">
            <li> credor</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Fornecedor', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome" class="required">Nome <span
                                    class="glyphicon glyphicon-asterisk"></span> </label>
                            <input type="text" required="required" name="nome" id="nome" class="form-control"
                                   placeholder="Nome">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input type="text" required="required" name="telefone" id="telefone"
                                   class="form-control telefone"
                                   placeholder="Telefone">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="celular">Celular</label>
                            <input type="text" name="celular" id="celular" class="form-control telefone"
                                   placeholder="Celular">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cep">Cep</label>
                            <input type="text" rel="txtTooltip" role="cep" cep-init="LoadGif" cep-done="CloseGif"
                                   name="cep" id="cep" class="form-control cep"
                                   placeholder="Cep"
                                   title="Informe o CEP - Preenchimento Automático" data-toggle="tooltip"
                                   data-placement="top">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" data-cep="cidade" id="cidade" class="form-control"
                                   placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="uf">Uf</label>
                            <select name="uf" id="uf" data-cep="uf" class="form-control selectPicker">
                                <option value=""></option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="numero">Numero</label>
                            <input type="text" name="numero" id="numero" class="form-control"
                                   placeholder="Numero">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="rua">Rua</label>
                            <input type="text" name="rua" data-cep="endereco" id="rua" class="form-control"
                                   placeholder="Rua">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input type="text" name="bairro" data-cep="bairro" id="bairro" class="form-control"
                                   placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" required="required" name="email" id="email" class="form-control"
                                   placeholder="Email">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cnpj">Cnpj</label>
                            <input type="text" name="cnpj" id="cnpj" class="form-control cnpj"
                                   placeholder="Cnpj">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cpf">Cpf</label>
                            <input type="text" name="cpf" id="cpf" class="form-control cpf"
                                   placeholder="CPF">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="banco">Banco</label>
                            <input type="text" name="banco" id="banco" class="form-control "
                                   placeholder="Nome do Banco">
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="agencia">Agência</label>
                            <input type="text" name="agencia" id="agencia" class="form-control "
                                   placeholder="Código da Agência">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control selectPicker" id="status">
                                <?php foreach ($Status as $s): ?>
                                    <option value="<?php echo $s->id ?>"><?php echo $s->descricao ?></option>';
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="conta">Conta</label>
                            <input type="text" name="conta" id="conta" class="form-control "
                                   placeholder="Número da Conta Bancária">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="operacao">Operação</label>
                            <input type="text" name="operacao" id="operacao" class="form-control "
                                   placeholder="Número da Operação">
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao" class="form-control"
                                          placeholder="Observação"></textarea>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Fornecedor', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <?php if ($this->getParam('origem')): ?>
                            <input type="hidden" name="origem" id="origem" value="1">
                            <input type="submit" class="btn btn-primary" value="salvar"
                                   onclick="FormularioCredor('form')"/> >
                        <?php else: ?>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[rel="txtTooltip"]').tooltip();

        $("#cnpj").blur(function () {
            if($(this).val() == '')
                return;

            if (!validarCNPJ($(this).val())) {
                BootstrapDialog.alert("CNPJ Inválido!");
                $("#cnpj").val("");
            }
        });

        $('.cep').inputmask('99999-999');
        $('.telefone').inputmask('(99) 9999[9]-9999');
        $('.cnpj').inputmask('99.999.999/9999-99');
        $('.cpf').inputmask('999.999.999-99');
    });
</script>
