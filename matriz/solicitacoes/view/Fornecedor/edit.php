<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Credor</h2>
        <ol class="breadcrumb">
            <li> credor</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Fornecedor', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome" class="required">Nome <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Fornecedor->nome ?>" placeholder="Nome">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input type="text" name="telefone" id="telefone" class="form-control telefone"
                                   value="<?php echo $Fornecedor->telefone ?>" placeholder="Telefone">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="celular">Celular</label>
                            <input type="text" name="celular" id="celular" class="form-control telefone"
                                   value="<?php echo $Fornecedor->celular ?>" placeholder="Celular">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cep">Cep</label>
                            <input type="text" name="cep" id="cep" class="form-control cep" role="cep"
                                   cep-init="LoadGif" cep-done="CloseGif" value="<?php echo $Fornecedor->cep ?>"
                                   placeholder="Cep">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" id="cidade" data-cep="cidade" class="form-control"
                                   value="<?php echo $Fornecedor->cidade ?>" placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="uf">Uf</label>
                            <select name="uf" id="uf" data-cep="uf" class="form-control selectPicker">
                                <option value="">Selecione</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "AC" ? "selected" : "" ?> value="AC">AC</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "AL" ? "selected" : "" ?> value="AL">AL</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "AM" ? "selected" : "" ?> value="AM">AM</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "AP" ? "selected" : "" ?> value="AP">AP</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "BA" ? "selected" : "" ?> value="BA">BA</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "CE" ? "selected" : "" ?> value="CE">CE</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "DF" ? "selected" : "" ?> value="DF">DF</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "ES" ? "selected" : "" ?> value="ES">ES</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "GO" ? "selected" : "" ?> value="GO">GO</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "MA" ? "selected" : "" ?> value="MA">MA</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "MG" ? "selected" : "" ?> value="MG">MG</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "MS" ? "selected" : "" ?> value="MS">MS</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "MT" ? "selected" : "" ?> value="MT">MT</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "PA" ? "selected" : "" ?> value="PA">PA</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "PB" ? "selected" : "" ?> value="PB">PB</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "PE" ? "selected" : "" ?> value="PE">PE</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "PI" ? "selected" : "" ?> value="PI">PI</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "PR" ? "selected" : "" ?> value="PR">PR</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "RJ" ? "selected" : "" ?> value="RJ">RJ</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "RN" ? "selected" : "" ?> value="RN">RN</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "RS" ? "selected" : "" ?> value="RS">RS</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "RO" ? "selected" : "" ?> value="RO">RO</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "RR" ? "selected" : "" ?> value="RR">RR</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "SC" ? "selected" : "" ?> value="SC">SC</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "SE" ? "selected" : "" ?> value="SE">SE</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "SP" ? "selected" : "" ?> value="SP">SP</option>
                                <option <?php echo strtoupper($Fornecedor->uf) == "TO" ? "selected" : "" ?> value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="numero">Numero</label>
                            <input type="text" name="numero" id="numero" class="form-control"
                                   value="<?php echo $Fornecedor->numero ?>" placeholder="Numero">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="rua">Rua</label>
                            <input type="text" name="rua" id="rua" data-cep="endereco" class="form-control"
                                   value="<?php echo $Fornecedor->rua ?>" placeholder="Rua">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input type="text" name="bairro" id="bairro" data-cep="bairro" class="form-control"
                                   value="<?php echo $Fornecedor->bairro ?>" placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" name="email" id="email" class="form-control"
                                   value="<?php echo $Fornecedor->email ?>" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cnpj">Cnpj</label>
                            <input type="text" name="cnpj" id="cnpj" class="form-control cnpj"
                                   value="<?php echo $Fornecedor->cnpj ?>" placeholder="Cnpj">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cpf">Cpf</label>
                            <input type="text" name="cpf" id="cpf" class="form-control cpf "
                                   value="<?php echo $Fornecedor->cpf ?>" placeholder="CPF">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="banco">Banco</label>
                            <input type="text" name="banco" id="banco" class="form-control "
                                   value="<?php echo $Fornecedor->banco ?>" placeholder="Nome do Banco">
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="agencia">Agência</label>
                            <input type="text" name="agencia" id="agencia" class="form-control "
                                   value="<?php echo $Fornecedor->agencia ?>" placeholder="Código da Agência">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Fornecedor->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="conta">Conta</label>
                            <input type="text" name="conta" id="conta" class="form-control "
                                   value="<?php echo $Fornecedor->conta ?>" placeholder="Número da Conta Bancária">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="operacao">Operação</label>
                            <input type="text" name="operacao" id="operacao" class="form-control "
                                   value="<?php echo $Fornecedor->operacao ?>" placeholder="Número da Operação">
                        </div>
                    </div>


                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao" class="form-control"
                                          placeholder="Observação"><?php echo $Fornecedor->observacao ?></textarea>
                        </div>
                    </div>
                    <div style="clear:both;"></div>

                    <input type="hidden" name="id" value="<?php echo $Fornecedor->id; ?>">

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Fornecedor', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[rel="txtTooltip"]').tooltip();

        $("#cnpj").blur(function () {
            if($(this).val() == '')
                return;

            if (!validarCNPJ($(this).val())) {
                BootstrapDialog.alert("CNPJ Inválido!");
                $("#cnpj").val("");
            }
        });

        $('.cep').inputmask('9{5}-9{3}');
        $('.telefone').inputmask('(9{2}) 9{4, 5}-9{4}');
        $('.cnpj').inputmask('9{2}.9{3}.9{3}/9{4}-9{2}');
        $('.cpf').inputmask('9{3}.9{3}.9{3}-9{2}');
    });
</script>
