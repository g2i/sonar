<div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#configs-acordion"
         href="#collapseListagem_<?php echo $t->name; ?>">
        <h4 class="panel-title">Configurações de listagem</h4>
    </div>
    <div id="collapseListagem_<?php echo $t->name; ?>" class="panel-collapse collapse">
        <div class="panel-body">

            <div class="col-md-12">

                <h4>Colunas que deverão aparecer na listagem</h4>
                <hr/>

                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th>Listagem</th>
                        <th>Titulo</th>
                        <th>Coluna de referência</th>
                    </tr>

                    <?php
                    $tbn = $t->name;
                    foreach ($tables_cols->$tbn as $row): ?>
                        <tr>
                            <td><?php echo $row->apresentacao; ?></td>
                            <td>
                                <input name="colunasAll_<?php echo $t->name; ?>[]"
                                       value="<?php echo $row->coluna; ?>" type="checkbox" checked/>
                            </td>
                            <td>
                                <input
                                    name="colunasTitle[<?php echo $t->name; ?>][<?php echo $row->coluna; ?>]"
                                    value="<?php echo $row->coluna; ?>" type="text"
                                    class="form-control"/>
                            </td>
                            <td>
                                <?php
                                foreach (InstallController::relacaoDB() as $dba) {
                                    if ($t->name == $dba->table) {
                                        if ($row->coluna == $dba->fk) {
                                            echo '<select class="form-control" name="colunasTitleRef['.$t->name.']['.$row->coluna.']">';
                                            foreach (InstallController::relacaoTB($dba->reftable) as $tbl) {
                                                echo '<option value="' . $tbl->Field . '">' . $tbl->Field . '</option>';
                                            }
                                            echo "</select>";
                                        }
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>


        </div>
    </div>
</div>