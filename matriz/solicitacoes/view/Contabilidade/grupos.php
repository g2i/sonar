<style>
    .col-opcoes{
        min-width: 50px; !important;
        max-width: 50px; !important;
        width: 50px; !important;
    }
    tr > td > button {
        margin-left: 2px;
    }
</style>
<div class="table-responsive">
    <table id="tblGrupos" class="table table-hover">
        <thead>
        <tr>
            <th data-column-id="id" data-identifier="true" data-visible="false"
                data-visible-in-selection="false">Id</th>
            <th data-column-id="nome">Nome</th>
            <th data-column-id="opcoes" data-formatter="opcoes"
                data-header-css-class="col-opcoes" data-visible-in-selection="false"
                data-sortable="false"></th>
        </tr>
        </thead>
    </table>
</div>