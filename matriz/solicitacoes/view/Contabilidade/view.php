<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Empresa</h2>
        <ol class="breadcrumb">
            <li>empresa</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Nome</strong>: <?php echo $Contabilidade->nome; ?></p>

                <p><strong>Descricao</strong>: <?php echo $Contabilidade->descricao; ?></p>

                <p><strong>Cep</strong>: <?php echo $Contabilidade->cep; ?></p>

                <p><strong>Rua</strong>: <?php echo $Contabilidade->rua; ?></p>

                <p><strong>Bairro</strong>: <?php echo $Contabilidade->bairro; ?></p>

                <p><strong>Numero</strong>: <?php echo $Contabilidade->numero; ?></p>

                <p><strong>Cidade</strong>: <?php echo $Contabilidade->cidade; ?></p>

                <p><strong>Uf</strong>: <?php echo $Contabilidade->uf; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Contabilidade->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Contabilidade->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>