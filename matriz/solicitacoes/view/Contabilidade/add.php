<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Empresas</h2>
        <ol class="breadcrumb">
            <li>empresa</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Contabilidade', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" name="nome" required="required" id="nome" class="form-control"
                                   placeholder="Nome">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cep">Cep</label>
                            <input type="text" name="cep" required="required" id="cep"
                                   onblur="preencheCep($(this).val())" class="form-control cep"
                                   placeholder="Cep">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="rua">Rua</label>
                            <input type="text" name="rua" required="required" id="rua" class="form-control"
                                   placeholder="Rua">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input type="text" name="bairro" required="required" id="bairro" class="form-control"
                                   placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="numero">Numero</label>
                            <input type="number" name="numero" required="required" id="numero" class="form-control"
                                   placeholder="Numero">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" required="required" id="cidade" class="form-control"
                                   placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="uf">Uf</label>
                            <select name="uf" id="uf" data-cep="uf" class="form-control selectPicker">
                                <option value="">Selecione</option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
                                <?php foreach ($Status as $s): ?>
                                   <option value="<?php echo $s->id; ?>"><?php echo $s->descricao; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="descricao">Descricao</label>
                                <textarea name="descricao" id="descricao"
                                          class="form-control"></textarea>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Contabilidade', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.cep').inputmask('9{5}-9{3}');
    });
</script>