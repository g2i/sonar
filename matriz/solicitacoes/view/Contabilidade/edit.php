<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Empresa</h2>
        <ol class="breadcrumb">
            <li>empresa</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Contabilidade', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" name="nome" id="nome" required="required" class="form-control"
                                   value="<?php echo $Contabilidade->nome ?>" placeholder="Nome">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cep">Cep</label>
                            <input type="text" name="cep" id="cep" required="required"
                                   onblur="preencheCep($(this).val())" class="form-control cep"
                                   value="<?php echo $Contabilidade->cep ?>" placeholder="Cep">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="rua">Rua</label>
                            <input type="text" name="rua" id="rua" required="required" class="form-control"
                                   value="<?php echo $Contabilidade->rua ?>" placeholder="Rua">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input type="text" name="bairro" id="bairro" required="required" class="form-control"
                                   value="<?php echo $Contabilidade->bairro ?>" placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="numero">Numero</label>
                            <input type="number" name="numero" id="numero" required="required" class="form-control"
                                   value="<?php echo $Contabilidade->numero ?>" placeholder="Numero">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" id="cidade" required="required" class="form-control"
                                   value="<?php echo $Contabilidade->cidade ?>" placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="uf">Uf</label>
                            <select name="uf" id="uf" data-cep="uf" class="form-control selectPicker">
                                <option value="">Selecione</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "AC" ? "selected" : "" ?> value="AC">AC</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "AL" ? "selected" : "" ?> value="AL">AL</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "AM" ? "selected" : "" ?> value="AM">AM</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "AP" ? "selected" : "" ?> value="AP">AP</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "BA" ? "selected" : "" ?> value="BA">BA</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "CE" ? "selected" : "" ?> value="CE">CE</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "DF" ? "selected" : "" ?> value="DF">DF</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "ES" ? "selected" : "" ?> value="ES">ES</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "GO" ? "selected" : "" ?> value="GO">GO</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "MA" ? "selected" : "" ?> value="MA">MA</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "MG" ? "selected" : "" ?> value="MG">MG</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "MS" ? "selected" : "" ?> value="MS">MS</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "MT" ? "selected" : "" ?> value="MT">MT</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "PA" ? "selected" : "" ?> value="PA">PA</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "PB" ? "selected" : "" ?> value="PB">PB</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "PE" ? "selected" : "" ?> value="PE">PE</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "PI" ? "selected" : "" ?> value="PI">PI</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "PR" ? "selected" : "" ?> value="PR">PR</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "RJ" ? "selected" : "" ?> value="RJ">RJ</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "RN" ? "selected" : "" ?> value="RN">RN</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "RS" ? "selected" : "" ?> value="RS">RS</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "RO" ? "selected" : "" ?> value="RO">RO</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "RR" ? "selected" : "" ?> value="RR">RR</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "SC" ? "selected" : "" ?> value="SC">SC</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "SE" ? "selected" : "" ?> value="SE">SE</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "SP" ? "selected" : "" ?> value="SP">SP</option>
                                <option <?php echo strtoupper($Contabilidade->uf) == "TO" ? "selected" : "" ?> value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Contabilidade->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="descricao">Descricao</label>
                                <textarea name="descricao" id="descricao"
                                          class="form-control"><?php echo $Contabilidade->descricao ?></textarea>
                        </div>
                    </div>
                    <input type="hidden" id="id" name="id" value="<?php echo $Contabilidade->id; ?>">

                    <div style="clear:both"></div>
                    <div class="text-right">
                        <button type="button" id="btnGrupos" class="btn btn-info">Grupos</button>
                        <a href="<?php echo $this->Html->getUrl('Contabilidade', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.cep').inputmask('9{5}-9{3}');
        $('#btnGrupos').click(function(){
            var empresa = $('#id').val();
            var url = root + '/Contabilidade/grupos?ajax=True';
            $('<div></div>').load(url, function(){
                BootstrapDialog.show({
                    title: 'Grupos',
                    message: this,
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function(dialog){
                        var grid = dialog.getModalBody().find('#tblGrupos');
                        grid.bootgrid({
                            rowSelect: true,
                            multiSort: false,
                            rowCount: [10, 25, 50],
                            ajax: true,
                            url: root + '/Contabilidade/getGrupos',
                            ajaxSettings: {
                                method: "GET",
                                cache: true
                            },
                            formatters:{
                                opcoes: function(column, row){
                                    return '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                                }
                            },
                            labels: {
                                search: 'Procura por nome'
                            },
                            templates:{
                                search: '<div class="{{css.search}} pull-left" style="width: auto"><div class="input-group"><span class="{{css.icon}} input-group-addon {{css.iconSearch}}"></span><input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div></div>',
                            },
                            requestHandler: function (request) {
                                request.cont = empresa;
                                if(request.sort){
                                    var sort = [];
                                    $.each(request.sort, function(key, value){
                                        sort.push([key, value]);
                                    });

                                    delete request.sort;
                                    request.sort = $.toJSON(sort);
                                }

                                return request;
                            }
                        });

                        grid.on("loaded.rs.jquery.bootgrid", function() {
                            grid.find('[data-toggle="tooltip"]').tooltip();

                            grid.find(".command-delete").unbind('click');
                            grid.find(".command-delete").on("click", function(e) {
                                var conta = $(this).data("row-id");
                                BootstrapDialog.confirm({
                                    title: 'Aviso',
                                    message: 'Voc\u00ea tem certeza ?',
                                    type: BootstrapDialog.TYPE_WARNING,
                                    closable: true,
                                    draggable: true,
                                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                                    btnOKLabel: 'Sim desejo excluir!',
                                    btnOKClass: 'btn-warning',
                                    callback: function(result) {
                                        if(result) {
                                            var url = root + '/Contabilidade/deleteGrupo';
                                            var data = {
                                                id: conta
                                            }

                                            $.post(url, data, function(ret){
                                                if(ret.result){
                                                    BootstrapDialog.success(ret.msg);
                                                    grid.bootgrid('reload');
                                                }else{
                                                    BootstrapDialog.warning(ret.msg);
                                                }
                                            });
                                        }
                                    }
                                });
                            });
                        });
                    },
                    buttons: [{
                        label: 'Adicionar',
                        icon: 'glyphicon glyphicon-plus-sign',
                        cssClass: 'btn-primary',
                        action: function (dialog) {
                            var grid = dialog.getModalBody().find('#tblGrupos');
                            BootstrapDialog.show({
                                title: 'Adicionar Contabilidades',
                                message: '<form class="form"><div class="form-group">' +
                                '<div class="input-group select2-bootstrap-append">' +
                                '<select id="grupos" class="form-control">' +
                                '<option></option></select><span class="input-group-btn">' +
                                '<button class="btn btn-default" type="button" data-select2-clear="grupos"' +
                                'data-toggle="tooltip" data-placement="top" title="Limpar" >' +
                                '<i class="fa fa-times"></i></button>' +
                                '</span></div></div></form>',
                                type: BootstrapDialog.TYPE_DEFAULT,
                                onshown: function(dialog){
                                    var grupos = dialog.getModalBody().find('#grupos');
                                    var url = root + '/Contabilidade/getContGrupos';
                                    var data = { id: empresa  }

                                    $.get(url, data, function(ret){
                                        if(ret.result) {
                                            ret.dados.forEach(function(dado){
                                                var opt = $('<option value="'+dado.id+'">'+dado.nome+'</option>');
                                                $(grupos).append(opt);
                                            });
                                        }

                                        grupos.select2({
                                            language: 'pt-BR',
                                            theme: 'bootstrap',
                                            width: '100%',
                                            minimumResultsForSearch: 6,
                                            placeholder: 'Selecione:'
                                        });

                                        dialog.getModalBody().find('[data-toggle="tooltip"]').tooltip();
                                        var buttons = dialog.getModalBody().find('button[data-select2-clear]');

                                        $(buttons).click(function() {
                                            $("#" + $(this).data("select2-clear")).val('').trigger('change');
                                        });

                                        var modal = dialog.getModal();
                                        $(modal).removeAttr('tabindex');
                                    });
                                },
                                buttons: [{
                                    label: 'Cancelar',
                                    icon: 'fa fa-times',
                                    cssClass: 'btn-danger',
                                    action: function (dialog) {
                                        dialog.close();
                                    }
                                },{
                                    label: 'Adicionar',
                                    icon: 'glyphicon glyphicon-plus-sign',
                                    cssClass: 'btn-primary',
                                    action: function (dialog) {
                                        var grupo = dialog.getModalBody().find('#grupos');
                                        var value = $(grupo).val();
                                        if(!value){
                                            BootstrapDialog.alert('Selecione um grupo !');
                                            return;
                                        }

                                        var url = root + '/Contabilidade/addGrupo';
                                        var data = {
                                            grupo: value,
                                            contabilidade: empresa
                                        }

                                        $.post(url, data, function(ret){
                                            if(ret.result){
                                                grid.bootgrid('reload');
                                                BootstrapDialog.success(ret.msg);
                                                dialog.close();
                                            }else{
                                                BootstrapDialog.warning(ret.msg);
                                            }
                                        });
                                    }
                                }]
                            });
                        }
                    },{
                        label: 'Fechar',
                        icon: 'fa fa-times',
                        cssClass: 'btn-danger',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            });
        });
    });
</script>