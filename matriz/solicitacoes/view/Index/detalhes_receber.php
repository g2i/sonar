<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fluxo</h2>
        <ol class="breadcrumb">
            <li>Contas Receber</li>
            <li class="active">
                <strong>detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="table-responsive">

                    <table id="tbl_detalhes" class="table table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Vencimento</th>
                            <th>Cliente</th>
                            <th>Plano de Contas</th>
                            <th>Número Documento</th>
                            <th>Documento Fiscal</th>
                            <th>Valor</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $total = 0;
                        foreach ($contas_receber as $contas) {
                            $total+=$contas->valor;
                            echo '<tr>';
                                    echo '<td>'.$contas->id.'</td>';
                                    echo '<td>'.ConvertData($contas->vencimento).'</td>';
                                    echo '<td>'.$contas->getCliente()->nome.'</td>';
                                    echo '<td>'.$contas->getPlanocontas()->nome.'</td>';
                                    echo '<td>'.$contas->numerodocumento.'</td>';
                                    echo '<td>'.$contas->documento.'</td>';
                                    echo '<td>'.number_format($contas->valor,2,',','.').'</td>';
                            echo '</tr>';
                        }
                        ?>
                        <tr>
                            <td colspan="6"></td>
                            <th >R$<?= number_format($total,2,',','.')?></th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
