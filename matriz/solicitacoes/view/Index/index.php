<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Bem vindo <?php echo Session::get('user')->nome; ?></h2>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top: 20px">
        <form role="form" id="form" class="col-md-6 col-md-offset-6">
            <div class="form-group col-md-6">
                <label for="grupos">Grupos</label>
                <div class="input-group select2-bootstrap-append">
                    <select name="grupos" class="form-control" id="grupos">
                        <option></option>
                        <?php foreach ($grupos as $e): ?>
                            <option value="<?php echo $e->id ?>"><?php echo $e->descricao ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="grupos"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                 </span>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="empresa">Empresa</label>
                <div class="input-group select2-bootstrap-append">
                    <select name="empresa" class="form-control" id="empresa"></select>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="empresa"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <small style="font-weight: bold">Saldo Atual</small>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table id="tblBancos" class="table table-hover">
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <small style="font-weight: bold">Proje&ccedil;&atilde;o de Fluxo de Caixa</small>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row" style="border-bottom:1px solid #E5E5E5;">
                        <div class="col-xs-6 col-md-3">
                            <label for="periodo">Periodo</label>
                            <select id="periodo" class="form-control selectPicker">
                                <option value="0">Semana</option>
                                <option value="1">Quinzena</option>
                                <option selected value="2">Mensal</option>
                                <option value="3">M&ecirc;s</option>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label for="inicio">Inicio</label>
                            <div class='input-group dateInicio'>
                                <input type='text' class="form-control dateFormat" name="inicio" id="inicio"
                                       value="<?php echo $inicio; ?>"/>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label for="fim">Fim</label>
                            <div class='input-group dateFim'>
                                <input type='text' class="form-control dateFormat" name="fim" id="fim"
                                       value="<?php echo $fim; ?>"/>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <div class="form-group col-md-9">
                                <label>Desconsiderar acumulado de:</label>
                                <div class="checkbox">
                                    <input id="chkPagar" type="checkbox" class="checkbox checkbox-primary">
                                    <label for="chkPagar">
                                        Contas Pagar
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <input id="chkReceber" type="checkbox" class="checkbox checkbox-primary">
                                    <label for="chkReceber">
                                        Contas Receber
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <button id="btnFluxo" style="margin-top: 25px" data-toggle="tooltip"
                                        data-placement="top" title="Atualizar Fluxo"
                                        class="btn btn-sm btn-default">
                                    <i class="fa fa-refresh"></i></button>
                            </div>
                        </div>
                    </div>
                    <div id="flxRow" class="row" style="overflow: auto; margin-top: 10px">
                        <table id="tblFluxo" class="table table-hover">
                        </table>
                    </div>
                    <div id="flxChartRow" class="row" style="border-top:1px solid #E5E5E5; margin-top: 10px">
                        <div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function updateFluxo(){
        var table = $('#tblFluxo');
        var chart = $('#flxChartRow');
        table.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
        chart.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        var inicio = moment($('#inicio').val(), 'DD/MM/YYYY');
        var fim = moment($('#fim').val(), 'DD/MM/YYYY');
        var grupo = $('#grupos').val();

        if(!grupo){
            table.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            return;
        }

        var empresas = '';
        if($('#empresa').val()){
            empresas = $('#empresa').val().join(',');
        }

        var url =  root+'/Index/getFluxo'
        var data = {
            inicio: $('#inicio').val(),
            fim: $('#fim').val(),
            grupo: grupo,
            empresas: empresas,
            pagar: $('#chkPagar').is(":checked"),
            receber: $('#chkReceber').is(":checked")
        }

        $.get(url, data, function(ret){
            var chartData = {
                data: {
                    labels: [],
                    datasets: [{
                        label: "Bancos",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(223, 214, 0, 1)',
                        backgroundColor: 'rgba(223, 214, 0, 1)',
                        pointBorderColor: 'rgba(223, 214, 0, 1)',
                        pointBackgroundColor: 'rgba(223, 214, 0, 0.8)',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 2
                    },{
                        label: "Contas Receber",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(40, 152, 44, 1)',
                        backgroundColor: 'rgba(40, 152, 44, 1)',
                        pointBorderColor: 'rgba(40, 152, 44, 1)',
                        pointBackgroundColor: 'rgba(40, 152, 44, 0.8)',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 2
                    },{
                        label: "Contas Pagar",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(215, 44, 44, 1)',
                        backgroundColor: 'rgba(215, 44, 44, 1)',
                        pointBorderColor: 'rgba(215, 44, 44, 1)',
                        pointBackgroundColor: 'rgba(215, 44, 44, 0.8)',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 2
                    },{
                        label: "Saldo",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(120, 203, 250, 1)',
                        backgroundColor: 'rgba(120, 203, 250, 1)',
                        pointBorderColor: 'rgba(120, 203, 250, 1)',
                        pointBackgroundColor: 'rgba(120, 203, 250, 0.8)',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBorderWidth: 2
                    }]
                },
                options: {
                    responsive: true,
                    tooltips: {
                        mode: 'label'
                    },
                    hover: {
                        mode: 'dataset'
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Data'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Valor'
                            },
                            ticks: {
                                suggestedMin: -10,
                                suggestedMax: 250,
                            }
                        }]
                    }
                }
            };

            if(!ret.result){
                table.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            var duration = fim.diff(inicio, 'days', true) + 1;
            var header = $('<thead></thead>');
            var columns = $('<tr></tr>');
            columns.append('<th></th>');
            columns.append('<th>Acumulado</th>');
            chartData.data.labels.push('Acumulado');

            var day = inicio.clone();
            for(var i = 1; i <= duration; i++){
                columns.append('<th class="small '+i+'">' + day.format('DD/MM/YYYY') + '</th>');
                chartData.data.labels.push(day.format('DD/MM/YYYY'));
                day.add(1, 'day');
            }

            header.append(columns);

            var body = $('<tbody></tbody>');
            for(i = 0; i < 4; i++){
                var col = $('<tr></tr>');

                switch(i) {
                    case 0:
                        col.append('<td style="font-weight: bold">Banco</td>');
                        break;

                    case 1:
                        col.append('<td style="font-weight: bold">Receber</td>');
                        break;

                    case 2:
                        col.append('<td style="font-weight: bold">Pagar</td>');
                        break;

                    case 3:
                        col.append('<td style="font-weight: bold">Saldo</td>');
                        break;
                }
                var aux_col =0;
                ret.dados.forEach(function(fluxo){
                    switch(i) {
                        case 0:
                            var aux = 'text-success';
                            if(fluxo.banco < 0)
                                aux = 'text-danger';

                            col.append('<td class="' + aux + ' " data-paran="banco" data-col="'+aux_col+'">R$ ' + $.number(fluxo.banco, 2) + '</td>');
                            chartData.data.datasets[0].data.push(fluxo.banco);
                            break;

                        case 1:
                            col.append('<td class="success td_result" data-paran="receber" data-col="'+aux_col+'">R$ ' + $.number(fluxo.receber, 2) + '</td>');
                            chartData.data.datasets[1].data.push(fluxo.receber);
                            break;

                        case 2:
                            col.append('<td class="default td_result" data-paran="pagar" data-col="'+aux_col+'">R$ ' + $.number(fluxo.pagar, 2) + '</td>');
                            chartData.data.datasets[2].data.push(fluxo.pagar);
                            break;

                        case 3:
                            var aux = 'text-success';
                            if(fluxo.saldo < 0)
                                aux = 'text-danger';

                            col.append('<td class="' + aux + '">R$ ' + $.number(fluxo.saldo, 2) + '</td>');
                            chartData.data.datasets[3].data.push(fluxo.saldo);
                            break;

                    }
                    aux_col++;
                });

                body.append(col);
            }

            table.html('');
            table.append(header);
            table.append(body);
            var ctx = $('<canvas></canvas>');
            chart.html('');
            chart.append(ctx);
            var lineChart = Chart.Line(ctx, chartData);
            lineChart.update();
            $('#flxRow').mCustomScrollbar("update");

            $(".td_result").css({"cursor":"pointer"})

            $(".td_result").click(function(){
                console.log($(this).attr('data-paran'))
                var col_header = $("#tblFluxo thead ."+$(this).attr('data-col')).text();
                if($(this).attr('data-paran')=='receber'){
                    var titulo ="Contas Receber"
                    var url = root+'/Index/detalhes_receber'
                }else{
                    var titulo ="Contas Pagar"
                    var url = root+'/Index/detalhes_pagar'
                }

                var fil = {
                    grupos:!empty($("#grupos").val()) ? $("#grupos").val() : null,
                    empresas:!empty($("#empresa").val()) ? $("#empresa").val() : '',
                    dia:col_header,
                    ajax:1,
                    coluna:$(this).attr('data-col'),
                    inicio:$("#inicio").val()
                }
                BootstrapDialog.show({
                    title:titulo,
                    message: $('<div class="table-responsive"></div>').loadGrid(url,fil,null,null),
                    closable: true,
                    size: BootstrapDialog.SIZE_WIDE
                });
            })

        });
    }

    function updateBancos(){
        var table = $('#tblBancos');
        table.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        var grupo = $('#grupos').val();
        if(!grupo){
            table.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            return;
        }

        var empresas = '';
        if($('#empresa').val()){
            empresas = $('#empresa').val().join(',');
        }

        var url =  root+'/Index/getMovimento'
        var data = {
            grupo: grupo,
            empresas: empresas
        }

        $.get(url, data, function(ret) {
            if (!ret.result) {
                table.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            var header = $('<thead></thead>');
            var columns = $('<tr></tr>');
            columns.append('<th>Banco</th>');
            columns.append('<th>Saldo</th>');
            columns.append('<th>Movimento</th>');
            header.append(columns);

            var body = $('<tbody></tbody>');
            ret.dados.forEach(function(fluxo) {
                var col = $('<tr></tr>');
                for(i = 0; i < 3; i++) {
                    switch(i) {
                        case 0:
                            col.append('<td>' + fluxo.banco + '</td>');
                            break;

                        case 1:
                            var aux = 'success';
                            if(fluxo.saldo < 0)
                                aux = 'danger';

                            col.append('<td class="' + aux + '">R$ ' + $.number(fluxo.saldo, 2) + '</td>');
                            break;

                        case 2:
                            col.append('<td>' + fluxo.movimento + '</td>');
                            break;
                    }
                }
                body.append(col);
            });

            var footer = $('<tfoot></tfoot>');
            var fcolumns = $('<tr></tr>');
            fcolumns.append($('<td></td>'));
            var aux = 'success';
            if(ret.total < 0)
                aux = 'danger';

            fcolumns.append($('<td class="' + aux + '">R$ ' + $.number(ret.total, 2) + '</td>'));
            fcolumns.append($('<td></td>'));
            footer.append(fcolumns);

            table.html('');
            table.append(header);
            table.append(body);
            table.append(footer);
        });
    }

    function updateFinanceiro(){
        var chart = $('#rstChartRow');
        chart.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        var grupo = $('#grupos').val();
        if(!grupo){
            chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            return;
        }

        var empresas = '';
        if($('#empresa').val()){
            empresas = $('#empresa').val().join(',');
        }

        var url =  root+'/Index/getFinanceiro'
        var data = {
            ano: $('#ano').val(),
            grupo: grupo,
            empresas: empresas
        }

        $.get(url, data, function(ret) {
            if (!ret.result) {
                chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            var chartData = {
                data: {
                    labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    datasets: [{
                        label: "Resultado",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(223, 214, 0, 0.8)',
                        backgroundColor: 'rgba(223, 214, 0, 0.8)',
                        hoverBorderColor: 'rgba(223, 214, 0, 1)',
                        hoverBackgroundColor: 'rgba(223, 214, 0, 1)',
                        borderWidth: 1
                    },{
                        label: "Entradas",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(40, 152, 44, 0.8)',
                        backgroundColor: 'rgba(40, 152, 44, 0.8)',
                        hoverBorderColor: 'rgba(40, 152, 44, 1)',
                        hoverBackgroundColor: 'rgba(40, 152, 44, 1)',
                        borderWidth: 1,
                    },{
                        label: "Saidas",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(215, 44, 44, 0.8)',
                        backgroundColor: 'rgba(215, 44, 44, 0.8)',
                        hoverBorderColor: 'rgba(215, 44, 44, 1)',
                        hoverBackgroundColor: 'rgba(215, 44, 44, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    tooltips: {
                        mode: 'label'
                    },
                    hover: {
                        mode: 'dataset'
                    }
                }
            };

            console.log(ret.dados);
            ret.dados.forEach(function(dados) {
                chartData.data.datasets[0].data.push(dados.resultado);
                chartData.data.datasets[1].data.push(dados.entrada);
                chartData.data.datasets[2].data.push(dados.saida);
            });

            var ctx = $('<canvas></canvas>');
            chart.html('');
            chart.append(ctx);
            var barChart = Chart.Bar(ctx, chartData);
            barChart.update();
        });
    }

    /*$(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();

        $('#flxRow').mCustomScrollbar({
            axis:"x",
            theme:"minimal-dark",
        });*/

        var inicio = moment();
        $('#inicio').val(inicio.format('DD/MM/YYYY'));
        $('#fim').val(inicio.add(30, 'day').format('DD/MM/YYYY'));

        var grupos = $('#grupos').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione um grupo'
        });

        var empresa = $('#empresa').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            multiple: true,
            placeholder: 'Selecione as empresas'
        });

        var ano = $('#ano').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Atual'
        });

        grupos.change(function(){
            if($(this).val() == '') {
                empresa.select2('destroy');
                empresa.val('');
                empresa.html('');
                var option = $('<option></option>');
                empresa.append(option);
                empresa.select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    multiple: true,
                    placeholder: 'Selecione as empresas'
                });
            }
            else {
                var url = root + '/Index/getEmpresas';
                var data = {
                    id: $(this).val()
                }

                $.get(url, data, function (ret) {
                    if (ret.result) {
                        empresa.select2('destroy');
                        empresa.html('');
                        ret.dados.forEach(function (dado) {
                            var option = $('<option value="' + dado.id + '" >' + dado.nome + '</option>');
                            empresa.append(option);
                        });

                        empresa.select2({
                            language: 'pt-BR',
                            theme: 'bootstrap',
                            width: '100%',
                            multiple: true,
                            placeholder: 'Selecione as empresas'
                        });
                    } else {
                        BootstrapDialog.alert(ret.msg);
                        console.log(ret.error);
                    }
                });
            }

            updateFluxo();
            updateBancos();
            updateFinanceiro();
        });

        empresa.change(function(){
            updateFluxo();
            updateBancos();
            updateFinanceiro();
        });

        ano.change(function(){
            updateFinanceiro();
        });

        $("button[data-select2-clear]").click(function() {
            $("#" + $(this).data("select2-clear")).val('').trigger('change');
        });

        $('#btnFluxo').click(function(){
            updateFluxo();
        });

        $('#periodo').change(function(){
            var value = $(this).val();
            switch(value){
                case '0':
                    var inicio = moment();
                    $('#inicio').val(inicio.format('DD/MM/YYYY'));
                    $('#fim').val(inicio.add(7, 'day').format('DD/MM/YYYY'));
                    break;

                case '1':
                    var inicio = moment();
                    $('#inicio').val(inicio.format('DD/MM/YYYY'));
                    $('#fim').val(inicio.add(15, 'day').format('DD/MM/YYYY'));
                    break;

                case '2':
                    var inicio = moment();
                    $('#inicio').val(inicio.format('DD/MM/YYYY'));
                    $('#fim').val(inicio.add(30, 'day').format('DD/MM/YYYY'));
                    break;

                case '3':
                    var inicio = moment().date(1);
                    $('#inicio').val(inicio.format('DD/MM/YYYY'));
                    $('#fim').val(inicio.endOf('month').format('DD/MM/YYYY'));
                    break;
            }

            updateFluxo();
        });

        updateFluxo();
        updateBancos();
        updateFinanceiro();
    });
</script>