<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Clientes</h2>
        <ol class="breadcrumb">
            <li>cliente</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Cliente', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome" class="required">Nome <span
                                    class="glyphicon glyphicon-asterisk"></span> </label>
                            <input type="text" required="required" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Cliente->nome ?>" placeholder="Nome">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input type="text" name="telefone" id="telefone" class="form-control" maxlength="30"
                                   value="<?php echo $Cliente->telefone ?>" placeholder="Telefone">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="celular" required="required">Celular <span
                                    class="glyphicon glyphicon-asterisc"></span> </label>
                            <input type="text" name="celular" id="celular" class="form-control" maxlength="30"
                                   value="<?php echo $Cliente->celular ?>" placeholder="Celular">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cep">Cep</label>
                            <input type="text" rel="txtTooltip" role="cep" cep-init="LoadGif" cep-done="CloseGif"
                                   name="cep" id="cep" class="form-control cep" value="<?php echo $Cliente->cep ?>"
                                   placeholder="Cep" title="Informe o CEP - Preenchimento Automático"
                                   data-toggle="tooltip" data-placement="top">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="endereco">Endereço</label>
                            <input type="text" name="endereco" data-cep="endereco" id="endereco"
                                   class="form-control" value="<?php echo $Cliente->endereco ?>"
                                   placeholder="Informe o nome da rua">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="uf">Uf</label>
                            <select name="uf" id="uf" data-cep="uf" class="form-control selectPicker">
                                <option value="">Selecione</option>
                                <option <?php echo strtoupper($Cliente->uf) == "AC" ? "selected" : "" ?> value="AC">AC</option>
                                <option <?php echo strtoupper($Cliente->uf) == "AL" ? "selected" : "" ?> value="AL">AL</option>
                                <option <?php echo strtoupper($Cliente->uf) == "AM" ? "selected" : "" ?> value="AM">AM</option>
                                <option <?php echo strtoupper($Cliente->uf) == "AP" ? "selected" : "" ?> value="AP">AP</option>
                                <option <?php echo strtoupper($Cliente->uf) == "BA" ? "selected" : "" ?> value="BA">BA</option>
                                <option <?php echo strtoupper($Cliente->uf) == "CE" ? "selected" : "" ?> value="CE">CE</option>
                                <option <?php echo strtoupper($Cliente->uf) == "DF" ? "selected" : "" ?> value="DF">DF</option>
                                <option <?php echo strtoupper($Cliente->uf) == "ES" ? "selected" : "" ?> value="ES">ES</option>
                                <option <?php echo strtoupper($Cliente->uf) == "GO" ? "selected" : "" ?> value="GO">GO</option>
                                <option <?php echo strtoupper($Cliente->uf) == "MA" ? "selected" : "" ?> value="MA">MA</option>
                                <option <?php echo strtoupper($Cliente->uf) == "MG" ? "selected" : "" ?> value="MG">MG</option>
                                <option <?php echo strtoupper($Cliente->uf) == "MS" ? "selected" : "" ?> value="MS">MS</option>
                                <option <?php echo strtoupper($Cliente->uf) == "MT" ? "selected" : "" ?> value="MT">MT</option>
                                <option <?php echo strtoupper($Cliente->uf) == "PA" ? "selected" : "" ?> value="PA">PA</option>
                                <option <?php echo strtoupper($Cliente->uf) == "PB" ? "selected" : "" ?> value="PB">PB</option>
                                <option <?php echo strtoupper($Cliente->uf) == "PE" ? "selected" : "" ?> value="PE">PE</option>
                                <option <?php echo strtoupper($Cliente->uf) == "PI" ? "selected" : "" ?> value="PI">PI</option>
                                <option <?php echo strtoupper($Cliente->uf) == "PR" ? "selected" : "" ?> value="PR">PR</option>
                                <option <?php echo strtoupper($Cliente->uf) == "RJ" ? "selected" : "" ?> value="RJ">RJ</option>
                                <option <?php echo strtoupper($Cliente->uf) == "RN" ? "selected" : "" ?> value="RN">RN</option>
                                <option <?php echo strtoupper($Cliente->uf) == "RS" ? "selected" : "" ?> value="RS">RS</option>
                                <option <?php echo strtoupper($Cliente->uf) == "RO" ? "selected" : "" ?> value="RO">RO</option>
                                <option <?php echo strtoupper($Cliente->uf) == "RR" ? "selected" : "" ?> value="RR">RR</option>
                                <option <?php echo strtoupper($Cliente->uf) == "SC" ? "selected" : "" ?> value="SC">SC</option>
                                <option <?php echo strtoupper($Cliente->uf) == "SE" ? "selected" : "" ?> value="SE">SE</option>
                                <option <?php echo strtoupper($Cliente->uf) == "SP" ? "selected" : "" ?> value="SP">SP</option>
                                <option <?php echo strtoupper($Cliente->uf) == "TO" ? "selected" : "" ?> value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" data-cep="cidade" id="cidade" class="form-control"
                                   value="<?php echo $Cliente->cidade ?>" placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input type="text" name="bairro" data-cep="bairro" id="bairro" class="form-control"
                                   value="<?php echo $Cliente->bairro ?>" placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="numero">Numero</label>
                            <input type="number" name="numero" id="numero" class="form-control"
                                   value="<?php echo $Cliente->numero ?>" placeholder="Numero">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="complemento">Complemento</label>
                            <input type="text" name="complemento" id="complemento" class="form-control"
                                   value="<?php echo $Cliente->complemento ?>"
                                   placeholder="Complemento de endereço">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cnpj">Cnpj</label>
                            <input type="text" name="cnpj" id="cnpj" class="form-control cnpj"
                                   value="<?php echo $Cliente->cnpj ?>" placeholder="Cnpj">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cpf">Cpf</label>
                            <input type="text" name="cpf" id="cpf" class="form-control cpf"
                                   value="<?php echo $Cliente->cpf ?>" placeholder="CPF">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" required="required" name="email" id="email" class="form-control"
                                   value="<?php echo $Cliente->email ?>" placeholder="Email">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="email_cobranca">Email de cobança</label>
                            <input type="email" required="required" name="email_cobranca" id="email_cobranca"
                                   class="form-control" value="<?php echo $Cliente->email_cobranca ?>"
                                   placeholder="Email">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="rg">RG</label>
                            <input type="text" required="required" name="rg" id="rg" class="form-control"
                                   value="<?php echo $Cliente->rg ?>" placeholder="RG">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control selectPicker" id="status">
                                <?php foreach ($Status as $s):
                                    $aux = $s->id == $Cliente->status ? "selectd" : "";?>
                                    <option <?php echo $aux ?> value="<?php echo $s->id ?>"><?php echo $s->descricao ?></option>';
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao" class="form-control"
                                          placeholder="Observação"><?php echo $Cliente->observacao ?></textarea>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <input type="hidden" name="id" value="<?php echo $Cliente->id; ?>">

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Cliente', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <?php if ($this->getParam('origem')) { ?>
                            <input type="hidden" name="origem" id="origem" value="1">
                            <input type="submit" class="btn btn-primary" value="salvar"
                                   onclick="FormularioCredor('form')"/> >
                        <?php } else { ?>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        <?php } ?>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('input[rel="txtTooltip"]').tooltip();

        $("#cnpj").blur(function () {
            if($(this).val() == '')
                return;

            if (!validarCNPJ($(this).val())) {
                BootstrapDialog.alert("CNPJ Inválido!");
                $("#cnpj").val("");
            }
        });

        $('.cep').inputmask('9{5}-9{3}');
        $('.telefone').inputmask('(9{2}) 9{4, 5}-9{4}');
        $('.cnpj').inputmask('9{2}.9{3}.9{3}/9{4}-9{2}');
        $('.cpf').inputmask('9{3}.9{3}.9{3}-9{2}');
    });
</script>
