<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Clientes</h2>
        <ol class="breadcrumb">
            <li>cliente</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Cliente', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome" class="required">Nome <span
                                    class="glyphicon glyphicon-asterisk"></span> </label>
                            <input type="text" required="required" name="nome" id="nome" class="form-control"
                                   placeholder="Nome">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input type="text" name="telefone" id="telefone" class="form-control" maxlength="30"
                                   placeholder="Telefone">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="celular" required="required">Celular <span
                                    class="glyphicon glyphicon-asterisc"></span> </label>
                            <input type="text" name="celular" id="celular" class="form-control" maxlength="30"
                                   value="<?php echo $Cliente->celular ?>" placeholder="Celular">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cep">Cep</label>
                            <input type="text" rel="txtTooltip" role="cep" cep-init="LoadGif" cep-done="CloseGif"
                                   name="cep" id="cep" class="form-control cep"
                                   placeholder="Cep" title="Informe o CEP - Preenchimento Automático"
                                   data-toggle="tooltip" data-placement="top">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="endereco">Endereço</label>
                            <input type="text" name="endereco" data-cep="endereco" id="endereco"
                                   class="form-control" placeholder="Informe o nome da rua">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="uf">Uf</label>
                            <select name="uf" id="uf" data-cep="uf" class="form-control selectPicker">
                                <option value="">Selecione</option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" data-cep="cidade" id="cidade" class="form-control"
                                   placeholder="Cidade">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="bairro">Bairro</label>
                            <input type="text" name="bairro" data-cep="bairro" id="bairro" class="form-control"
                                   placeholder="Bairro">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="numero">Numero</label>
                            <input type="number" name="numero" id="numero" class="form-control"
                                   placeholder="Numero">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="complemento">Complemento</label>
                            <input type="text" name="complemento" id="complemento" class="form-control"
                                   placeholder="Complemento do endereço">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cnpj">Cnpj</label>
                            <input type="text" name="cnpj" id="cnpj" class="form-control cnpj" placeholder="Cnpj">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="cpf">Cpf</label>
                            <input type="text" name="cpf" id="cpf" class="form-control cpf" placeholder="CPF">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" required="required" name="email" id="email" class="form-control email"
                                   placeholder="Email">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="email_cobranca">Email de cobança</label>
                            <input type="email" required="required" name="email_cobranca" id="email_cobranca"
                                   class="form-control email" placeholder="Email">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="rg">RG</label>
                            <input type="text" required="required" name="rg" id="rg" class="form-control"
                                   placeholder="RG">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control selectPicker" id="status">
                                <?php foreach ($Status as $s): ?>
                                    <option value="<?php echo $s->id ?>"><?php echo $s->descricao ?></option>';
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao" class="form-control"
                                          placeholder="Observação"></textarea>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Cliente', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <?php if ($this->getParam('origem')) { ?>
                            <input type="hidden" name="origem" id="origem" value="1">
                            <input type="submit" class="btn btn-primary" value="salvar"
                                   onclick="FormularioCredor('form')"/> >
                        <?php } else { ?>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        <?php } ?>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){
        $('input[rel="txtTooltip"]').tooltip();

        $("#cnpj").blur(function () {
            if($(this).val() == '')
                return;

            if (!validarCNPJ($(this).val())) {
                BootstrapDialog.alert("CNPJ Inválido!");
                $("#cnpj").val("");
            }
        });

        $('.cep').inputmask('9{5}-9{3}');
        $('.telefone').inputmask('(9{2}) 9{4, 5}-9{4}');
        $('.cnpj').inputmask('9{2}.9{3}.9{3}/9{4}-9{2}');
        $('.cpf').inputmask('9{3}.9{3}.9{3}-9{2}');
    });
</script>
