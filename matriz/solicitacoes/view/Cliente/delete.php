<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Clientes</h2>
        <ol class="breadcrumb">
            <li>cliente</li>
            <li class="active">
                <strong>Deletar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form class="form" method="post" action="<?php echo $this->Html->getUrl('Fornecedor', 'delete') ?>">
                    <h1>Confirmação</h1>

                    <div class="well well-lg">
                        <p>Voce tem certeza que deseja excluir o Credor
                            <strong><?php echo $Fornecedor->nome; ?></strong>?</p>
                    </div>
                    <div class="text-right">
                        <input type="hidden" name="id" value="<?php echo $Fornecedor->id; ?>">
                        <a href="<?php echo $this->Html->getUrl('Fornecedor', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-danger" value="Excluir">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>