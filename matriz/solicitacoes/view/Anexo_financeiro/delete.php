<form class="form" method="post" action="<?php echo $this->Html->getUrl('Anexo_financeiro', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Anexo_financeiro->titulo; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Anexo_financeiro->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Anexo_financeiro', 'all',array('id_externo'=> $Anexo_financeiro->id_externo,'tipo'=>$Anexo_financeiro->tipo)) ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>