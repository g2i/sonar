<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexo</h2>
        <ol class="breadcrumb">
            <li>Anexo</li>
            <li class="active">
                <strong>Adicionar Anexo</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" id="form-anexos" role="form" enctype="multipart/form-data"
                          action="<?php echo $this->Html->getUrl('Anexo_financeiro', 'edit') ?>">
                        <input type="hidden" name="id_externo" id="id_externo" class="form-control"
                               value="<?php echo $Anexo_financeiro->id_externo ?>" placeholder="Id_externo">

                        <input type="hidden" name="path" id="caminho" class="form-control"
                               value="<?php echo $Anexo_financeiro->caminho ?>" placeholder="Tipo">

                        <input type="hidden" name="id" value="<?php echo $Anexo_financeiro->id; ?>">

                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="titulo">Titulo</label>
                                <input type="text" name="titulo" id="titulo" class="form-control"
                                       value="<?php echo $Anexo_financeiro->titulo ?>" placeholder="Titulo">
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="titulo">Tipo de documento</label>
                                <select name="tipo" class="form-control selectPicker" id="tipo" required>
                                    <?php
                                    foreach ($tipos as $t) {
                                        if($Anexo_financeiro->tipo == $t->id)
                                            echo '<option selected value="' . $t->id . '">' . $t->nome . '</option>';
                                        else
                                            echo '<option value="' . $t->id . '">' . $t->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label class="required" for="capa">Anexo</label>
                                <?
                                $url = '';
                                $fileObj = array();
                                if(!empty($Anexo_financeiro->caminho)) {
                                    $extensao = pathinfo($Anexo_financeiro->caminho, PATHINFO_EXTENSION);
                                    if ($extensao == 'pdf' || $extensao == 'txt' || $extensao == 'rar')
                                        $fileObj['type'] = $extensao;

                                    $url = 'http://' . $_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT']. $Anexo_financeiro->caminho;

                                    $html = new html();
                                    $urlDel = $html->getUrl('Anexo_financeiro', 'delete_anexo', array('id' => $Anexo_financeiro->id, 'ajax' => true));

                                    $fileObj['url'] = $urlDel;
                                    $fileObj['caption'] = $Anexo_financeiro->titulo;
                                    $fileObj['width'] = "120px";
                                    $fileObj['key'] = $Anexo_financeiro->id;
                                }
                                    $fileObj = json_encode($fileObj);

                                ?>

                                <input type="file" class="input-file-edit" name="caminho[]" id="anexo"
                                       link-img="<?= $url ?>"
                                       data-object='<?= $fileObj?>'
                                       placeholder="Anexo">
                            </div>
                            <div class="clearfix"></div>
                            <div class="progress" style="display: none;">
                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span class="sr-only">0% Complete</span>
                                </div>
                            </div>

                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Anexo_financeiro', 'all',array('id_externo'=>$this->getParam('id_externo'),'origem'=>$this->getParam('origem'))) ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar" onclick="enviar_anexos()">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function enviar_anexos() {

        $('form#form-anexos').ajaxForm({
            uploadProgress: function (event, position, total, percentComplete) {
                $(".progress").css({"display": "block"});
                var percentVal = percentComplete + '%';
                $("[role='progressbar']").attr('style', 'width: ' + percentVal);
            },
            success: function (data) {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_SUCCESS,
                    message: data,
                    buttons: [{
                        label: 'OK',
                        action: function (dialogRef) {
                            location.href = root+'/Anexo_financeiro/all/id_externo:<?=$this->getParam('id_externo')?>/origem:<?=$this->getParam('origem')?>';
                            dialogRef.close();
                        }}]
                });
            },
            error: function(data,status,xhr){
                BootstrapDialog.alert({
                    title:'Atenção',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: data.responseText
                });
            }
        });
    }
</script>


