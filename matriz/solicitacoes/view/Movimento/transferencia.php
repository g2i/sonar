<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
    .has-error .select2-selection {
        border: 1px solid #a94442;
        border-radius: 4px;
    }
</style>

<form class="form" method="post"
      action="<?php echo $this->Html->getUrl('Movimento', 'transferencia') ?>">
    <input type="hidden" name="idPlanoContas" value="-1"/>
    <input type="hidden" name="idCliente" value="-1"/>
    <input type="hidden" name="idFornecedor" value="-1"/>

    <div class="alert alert-info">Os campos marcados com <span
            class="small glyphicon glyphicon-asterisk"></span> s&atilde;o de
        preenchimento obrigat&oacute;rio.
    </div>

    <div class="form-group">
        <label for="de" class="required">De<span
                class="glyphicon glyphicon-asterisk"></span></label>
        <select id="de" name="de" class="form-control deTransferencia" required>
            <option></option>
            <?php foreach ($movimento as $m): ?>
                <option value="<?= $m[0] ?>"><?= $m[1]?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group">
        <label for="para" class="required">Para<span
                class="glyphicon glyphicon-asterisk"></span></label>
        <select id="para" name="para" class="form-control paraTransferencia" required>
            <option value="">Selecione</option>
        </select>
    </div>


    <div class="form-group">
        <label for="data" class="required">Data do Documento
            <span class="glyphicon glyphicon-asterisk"></span>
        </label>
        <div class='input-group date'>
            <input type='text' class="form-control dateFormat" name="data" id="data" required/>
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        </div>
    </div>

    <div class="form-group">
        <label for="situacao" class="required">Situa&ccedil;&atilde;o <span
                class="glyphicon glyphicon-asterisk"></span></label>
        <select id="situacao" name="situacao" class="form-control selectPicker" required>
            <option value="1">Consolidado</option>
            <option value="2">A cair</option>
        </select>
    </div>
    <div class="form-group">
        <label for="documento">Documento</label>
        <input type="text" name="documento" id="documento" class="form-control" placeholder="Documento">
    </div>

    <div class="form-group">
        <label for="valor" class="required">Valor
            <span class="glyphicon glyphicon-asterisk"></span>
        </label>
        <div class='input-group'>
            <span class="input-group-addon">R$</span>
            <input type="text" id="valor" value="0" name="valor" class="form-control money2">
        </div>
    </div>

    <div class="form-group">
        <label for="movimento" class="required">Empresa <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <select id="idContabilidade" name="idContabilidade" class="form-control selectPicker" required>
            <option></option>
            <?php foreach ($empresa as $e): ?>
                <option value="<?= $e->id ?>"><?= $e->nome ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group">
        <label for="complemento">Observa&ccedil;&atilde;o</label>
        <textarea name="complemento" id="complemento" class="form-control"></textarea>
    </div>

    <div class="text-right">
        <a class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" onclick="EnviarDados('form')" class="btn btn-primary" value="Salvar">
    </div>
</form>
</div>


<script type="text/javascript">
    $(function () {
        $(".de").change(function(){
            var url = root+"/Movimento/getPara";
            var data = {
                id: $(this).val()
            }

            $.post(url, data, function(txt){
                $(".para").html(txt);
            });
        });
    });
</script>