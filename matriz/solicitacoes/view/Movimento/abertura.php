<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Movimento</h2>
        <ol class="breadcrumb">
            <li> Movimento</li>
            <li class="active">
                <strong>Abertura</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Movimento', 'abertura') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="mes">M&ecirc;s</label>
                            <input type="text" name="mes" id="mes" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="ano">Ano</label>
                            <input type="text" name="ano" id="ano" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="banco">Banco</label>
                            <select name="banco" id="banco" class="form-control selectPicker">
                                <?php
                                foreach ($banco as $b) {
                                    echo "<option value='" . $b->id . "'>" . $b->nome . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="saldoInicial">Saldo</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="saldoInicial" id="saldoInicial" value="0.00" class="form-control money2">
                            </div>
                        </div>
                    </div>

                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Movimento', 'controle') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#mes').inputmask("m",{ "placeholder": "mm" });
        $('#ano').inputmask("y",{ "placeholder": "yyyy" });
    });
</script>