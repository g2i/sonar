<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Movimento</h2>
        <ol class="breadcrumb">
            <li> Movimento</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Situacao</strong>: <?php echo $Movimento->situacao; ?></p>

                <p><strong>Data</strong>: <?php echo $Movimento->data; ?></p>

                <p><strong>IdPlanoContas</strong>: <?php echo $Movimento->idPlanoContas; ?></p>

                <p><strong>Tipo</strong>: <?php echo $Movimento->tipo; ?></p>

                <p><strong>IdFornecedor</strong>: <?php echo $Movimento->idFornecedor; ?></p>

                <p><strong>Complemento</strong>: <?php echo $Movimento->complemento; ?></p>

                <p><strong>Documento</strong>: <?php echo $Movimento->documento; ?></p>

                <p><strong>Credito</strong>: <?php echo $Movimento->credito; ?></p>

                <p><strong>Debito</strong>: <?php echo $Movimento->debito; ?></p>

                <p><strong>Banco</strong>: <?php echo $Movimento->banco; ?></p>

                <p><strong>Status</strong>: <?php echo $Movimento->status; ?></p>

                <p><strong>Categoria</strong>: <?php echo $Movimento->categoria; ?></p>
            </div>
        </div>
    </div>
</div>