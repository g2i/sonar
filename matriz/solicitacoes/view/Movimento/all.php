<style>
    .col-opcoes{
        min-width: 90px; !important;
        max-width: 90px; !important;
        width: 90px; !important;
    }
    tr > td > button {
        margin-left: 2px;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Movimento</h2>
        <ol class="breadcrumb">
            <li> Movimento</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-12" style="margin-top: 50px">
        <form id='pesquisa' role="form">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="categoria">Categoria</label>
                    <select id="categoria" name="categoria" class="form-control selectPicker">
                        <option value=""></option>
                        <option value="1">Encerrado</option>
                        <option value="2">Aberto</option>
                    </select>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="banco">Banco</label>
                    <select id="banco" name="banco" class="form-control selectPicker">
                        <option value=""></option>
                        <?php foreach ($Banco as $b): ?>
                            <option value="<?php echo $b->id ?>"><?php echo $b->nome ?></option>;
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="movimento">Movimento</label>
                    <select id="movimento" name="movimento" class="form-control selectPicker">
                        <option value=""></option>
                    </select>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="situacao">Situação</label>
                    <select id="situacao" name="situacao" class="form-control selectPicker">
                        <option selected value="1">Consolidado</option>
                        <option value="2">A Cair</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-6 col-md-3">
                <label for="inicio">Inicio</label>
                <div class='input-group dateInicio'>
                    <input type='text' class="form-control dateFormat" name="inicio" id="inicio"/>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <label for="fim">Fim</label>
                <div class='input-group dateFim'>
                    <input type='text' class="form-control dateFormat" name="fim" id="fim"/>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>

            <div class="col-md-6 text-right">
                <div class="form-group"  style="margin-top: 22px">
                    <button type="button" class="btn btn-default" id="filtro">
                        <span class="fa fa-search"></span>
                    </button>

                    <button id="btnPrint" title="Gerar Relatório" type="button" class="btn btn-default">
                        <span class="fa fa-print"></span>
                    </button>
                    <?php if(Config::get('lucros')): ?>
                        <button id="btnLucro" title="Relatório Lucro e Dividendos" type="button" class="btn btn-default">
                            <span class="fa fa-print"></span>
                        </button>
                    <?php endif; ?>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row text-right pull-right">
                    <p>
                        <button id="btnTransferencia" class="btn btn-success"><span class="fa fa-plus-circle"></span> Transferência</button>
                        <button id="btnNovo" class="btn btn-success"><span class="fa fa-plus-circle"></span> Movimento</button>
                    </p>
                </div>
                <div class="table-responsive">
                    <table id="tblDados" class="table table-hover">
                        <thead>
                        <tr>
                            <th data-column-id="id" data-identifier="true">Nr. Lanc.</th>
                            <th data-column-id="data" data-converter="date">Data</th>
                            <th data-column-id="nome">Credor/Cliente</th>
                            <th data-column-id="plano">Plano de Contas</th>
                            <th data-column-id="empresa">Empresa</th>
                            <th data-column-id="documento">Documento</th>
                            <th data-column-id="credito" data-converter="currency">Crédito</th>
                            <th data-column-id="debito" data-converter="currency">Débito</th>
                            <th data-column-id="saldo" data-converter="currency">Saldo</th>
                            <th data-column-id="opcoes" data-formatter="opcoes"
                                data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                data-sortable="false"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var totais = null;
    $(document).ready(function () {

        var situacao = $('#situacao');
        var categoria = $('#categoria');
        var movimento = $('#movimento');
        var banco = $('#banco');

        var periodo = moment().date(1);
        $('#inicio').val(periodo.format('DD/MM/YYYY'));
        $('#fim').val(periodo.endOf('month').format('DD/MM/YYYY'));

        var grid = $('#tblDados').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: -1,
            ajax: true,
            url: root + '/Movimento/getMovimento',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            converters:{
                date: {
                    from: function (value) {
                        if(value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                    }
                },
                currency: {
                    from: function (value) { return value; },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return $.number(value, 2);
                    }
                }
            },
            formatters:{
                opcoes: function(column, row){
                    return  '<button type="button" data-toggle="tooltip" data-placement="top" title="Anexos" class="btn btn-xs btn-default command-anexo" data-row-id="' + row.id + '"><span class="fa fa fa-paperclip"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Estonar" class="btn btn-xs btn-danger command-estornar" data-row-id="' + row.id + '"><span class="fa fa-undo"></span></button>';
                }
            },
            templates:{
                search: '',
            },
            requestHandler: function (request) {
                request.inicio = $('#inicio').val();
                request.fim = $('#fim').val();
                request.movimento = movimento.val();
                request.situacao = situacao.val();
                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                totais = null;
                return request;
            },
            responseHandler: function (response){
                totais = response.somatoria;
                return response;
            },
        });

        grid.on('load.rs.jquery.bootgrid', function(){
            var header = grid.find('#gridHeader');
            header.remove();

            var footer = grid.find('tfoot');
            footer.remove();
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {
            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-anexo").unbind('click');
            grid.find(".command-anexo").on("click", function(e) {
                var movId = $(this).data("row-id");
                var url = root + '/Anexo/all?ajax=true&tipo=3&id_externo=' + movId;
                $('<div></div>').load(url, function(){
                    BootstrapDialog.show({
                        title: 'Anexos',
                        message: this,
                        type: BootstrapDialog.TYPE_DEFAULT,
                        buttons: [{
                            label: 'Fechar',
                            icon: 'fa fa-times-circle',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                });
            });

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var id = $(this).data("row-id");
                var url = root + '/Movimento/edit/ajax:true/modal:1/first:1/id:' + id;
                showOnModal(url, 'Editar movimento', BootstrapDialog.SIZE_WIDE, function(dialog){
                    grid.bootgrid('reload');
                    return true;
                });
            });

            grid.find(".command-estornar").unbind('click');
            grid.find(".command-estornar").on("click", function(e) {
                var movId = $(this).data("row-id");

                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea  tem certeza que deseja estornar este Movimento ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o estornar!',
                    btnOKLabel: 'Sim estornar!',
                    btnOKClass: 'btn-warning',
                    callback: function(result) {
                        if(result) {
                            var url = root + '/Movimento/estornar';
                            var data = {
                                id: movId
                            }

                            $.post(url, data, function(ret){
                                if(ret.result){
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                }else{
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });

            var header = $('<thead id="gridHeader"></thead>');
            var headeIni = $('<tr></tr>');
            headeIni.append($('<td colspan="8" class="text-right" style="font-weight: bold">Valor Inicial:</td>'));
            headeIni.append($('<td style="font-weight: bold">' + $.number(totais.inicial, 2) + '</td>'));
            headeIni.append($('<td></td>'));
            header.append(headeIni);
            grid.prepend(header);

            var footer = $('<tfoot></tfoot>');
            var sumario = $('<tr></tr>');

            sumario.append($('<td colspan="6" class="text-right" style="font-weight: bold">Total:</td>'));
            sumario.append($('<td style="font-weight: bold">' + $.number(totais.credito, 2) + '</td>'));
            sumario.append($('<td style="font-weight: bold">' + $.number(totais.debito, 2) + '</td>'));
            sumario.append($('<td style="font-weight: bold">' + $.number(totais.final, 2) + '</td>'));
            sumario.append($('<td></td>'));
            footer.append(sumario);
            grid.append(footer);
        });

        categoria.change(function () {
            movimento.select2('destroy');
            movimento.val();
            movimento.html('');
            movimento.select2({
                language: 'pt-BR',
                theme: 'bootstrap',
                width: '100%',
                placeholder: 'Selecione:'
            })

            if(banco.val() != ''){
                banco.trigger('change');
            }
        });

        situacao.change(function () {
            grid.bootgrid('reload');
        });

        movimento.change(function () {
            grid.bootgrid('reload');
        });

        banco.change(function () {
            $('body').css("cursor", "progress");
            var url = root + '/Movimento/getMovimentoBanco';
            var data = {
                banco: $('#banco').val(),
                categoria: $('#categoria').val()
            }

            $.post(url, data, function (txt) {
                $('body').css("cursor", "default");
                $('#movimento').select2('destroy');
                $('#movimento').html(txt);
                $('#movimento').select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    placeholder: 'Selecione:'
                })
            });
        });

        $('#filtro').click(function(){
            grid.bootgrid('reload');
        });

        $('#btnTransferencia').click(function(){
            var url = root + '/Movimento/transferencia/ajax:true/modal:1/first:1/';
            console.log(url);
            showOnModal(url, 'Transferir fundos', BootstrapDialog.SIZE_NORMAL, function(dialog){
                grid.bootgrid('reload');
                return true;
            });

        });

        $('#btnNovo').click(function(){
            var url = root + '/Movimento/add/ajax:true/modal:1/first:1/';
            showOnModal(url, 'Novo movimento', BootstrapDialog.SIZE_WIDE, function(dialog){
                grid.bootgrid('reload');
                return true;
            });

        });

        $('#btnPrint').click(function (e) {
            var data = {
                inicio: -1,
                fim: -1,
                banco: $("#banco").val(),
                mov: $("#movimento").val(),
                cat: $("#categoria").val(),
                sit: $("#situacao").val(),
                saldo: 0
            };

            if (data.banco == 0) {
                BootstrapDialog.alert("Selecione um banco!");
                return false;
            }

            if (data.mov == 0) {
                BootstrapDialog.alert("Selecione um movimento!");
                return false;
            }

            if (data.cat == "") {
                BootstrapDialog.alert("Selecione uma categoria!");
                return false;
            }

            if (data.sit == "") {
                BootstrapDialog.alert("Selecione uma situação!");
                return false;
            }

            BootstrapDialog.show({
                title: 'Deseja selecionar um algum intervalo de datas ?',
                message: '<form class="form-horizontal"><div class="row">' +
                '<div class="form-group col-md-6">Inicio:' +
                '<div class="input-group .dateInicio">' +
                '<input type="text" class="form-control dateInicio" name="inicio" id="inicio"/>' +
                '<span class="input-group-addon"><i class="fa fa-calendar"></i></span></div></div>' +
                '<div style="margin-left: 5px" class="form-group col-md-6">Fim: <div class="input-group dateFim">' +
                '<input type="text" class="form-control dateFormat" name="fim" id="fim"/>' +
                '<span class="input-group-addon"><i class="fa fa-calendar"></i></span></div>' +
                '</div></div></form>',
                type: BootstrapDialog.TYPE_DEFAULT,
                closable: false,
                draggable: true,
                onshown: function (dialog) {
                    var body = dialog.getModalBody();
                    var inicio = body.find('.dateInicio');
                    inicio.datetimepicker({
                        showTodayButton: true,
                        showClear: true,
                        showClose: true,
                        allowInputToggle: true,
                        locale: 'pt-Br',
                        format: 'L',
                        tooltips: {
                            today: 'Ir para Hoje',
                            clear: 'Limpar sele\u00e7\u00e3o',
                            close: 'Fechar',
                            selectMonth: 'Selecione o M\u00eas',
                            prevMonth: 'M\u00eas Anterior',
                            nextMonth: 'Pr\u00f3ximo M\u00eas',
                            selectYear: 'Selecione o Ano',
                            prevYear: 'Ano Anterior',
                            nextYear: 'Pr\u00f3ximo Ano',
                            selectDecade: 'Selecione a D\u00e9cada',
                            prevDecade: 'D\u00e9cada Anterior',
                            nextDecade: 'Pr\u00f3xima D\u00e9cada',
                            prevCentury: 'S\u00e9culo Anterior',
                            nextCentury: 'Pr\u00f3ximo S\u00e9culo'
                        }
                    });

                    var fim = body.find('.dateFim');
                    fim.datetimepicker({
                        useCurrent: false,
                        showTodayButton: true,
                        showClear: true,
                        showClose: true,
                        allowInputToggle: true,
                        locale: 'pt-Br',
                        format: 'L',
                        tooltips: {
                            today: 'Ir para Hoje',
                            clear: 'Limpar sele\u00e7\u00e3o',
                            close: 'Fechar',
                            selectMonth: 'Selecione o M\u00eas',
                            prevMonth: 'M\u00eas Anterior',
                            nextMonth: 'Pr\u00f3ximo M\u00eas',
                            selectYear: 'Selecione o Ano',
                            prevYear: 'Ano Anterior',
                            nextYear: 'Pr\u00f3ximo Ano',
                            selectDecade: 'Selecione a D\u00e9cada',
                            prevDecade: 'D\u00e9cada Anterior',
                            nextDecade: 'Pr\u00f3xima D\u00e9cada',
                            prevCentury: 'S\u00e9culo Anterior',
                            nextCentury: 'Pr\u00f3ximo S\u00e9culo'
                        }
                    });

                    inicio.on("dp.change", function (e) {
                        fim.data("DateTimePicker").minDate(e.date);
                    });

                    fim.on("dp.change", function (e) {
                        inicio.data("DateTimePicker").maxDate(e.date);
                    });

                    var date = body.find('.dateFormat');
                    date.inputmask("d/m/y",{ "placeholder": "dd/mm/yyyy" });

                    body.find("#inicio").val($('#inicio').val());
                    body.find("#fim").val($('#fim').val());
                },
                buttons: [{
                    label: 'Cancelar',
                    icon: 'fa fa-ban',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        dialog.close();
                    }
                }, {
                    label: 'Gerar Relatório',
                    icon: 'fa fa-file-code-o',
                    cssClass: 'btn-success',
                    action: function (dialog) {
                        var inicio = dialog.getModalBody().find("#inicio").val();
                        var fim = dialog.getModalBody().find("#fim").val();

                        LoadGif();
                        var url = root + '/Movimento/SaldoInicialData';
                        data.inicio = inicio;
                        data.fim = fim;

                        $.post(url, data, function(saldo){
                            if (inicio == "") {
                                data.inicio = -1;
                            }else{
                                data.inicio = moment(inicio, 'DD/MM/YYYY').format('YYYY-MM-DD');
                            }

                            if (fim == "") {
                                data.fim = -1;
                            }else{
                                data.fim = moment(fim, 'DD/MM/YYYY').format('YYYY-MM-DD');
                            }

                            data.saldo = saldo;
                            $.ReportPost('movi', data);
                        }).always(function(){
                            CloseGif();
                            dialog.close();
                        });
                    }
                }]
            });
        });

        <?php if(Config::get('lucros')): ?>
        $('#btnLucro').click(function (e) {
            var data = {
                inicio: -1,
                fim: -1,
            };

            var inicio = $('#inicio').val();
            var fim = $('#fim').val();

            if (inicio == "") {
                data.inicio = -1;
            }else{
                data.inicio = moment(inicio, 'DD/MM/YYYY').format('YYYY-MM-DD');
            }

            if (fim == "") {
                data.fim = -1;
            }else{
                data.fim = moment(fim, 'DD/MM/YYYY').format('YYYY-MM-DD');
            }

            $.ReportPost('lucros', data);
        });
        <?php endif; ?>

        if(banco.val() != ''){
            banco.trigger('change');
        }
    });
</script>

