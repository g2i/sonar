<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Documentos</h2>
        <ol class="breadcrumb">
            <li>Tipo Documento</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Tipo_documento', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="form-group">
                        <label for="descricao">Descricao</label>
                        <input type="text" name="descricao" id="descricao" required="required" class="form-control"
                               value="<?php echo $Tipo_documento->descricao ?>" placeholder="Descricao">
                    </div>
                    <div class="form-group">
                        <label for="status_id">Status</label>
                        <select name="status_id" class="form-control selectPicker" id="status_id">
                            <?php
                            foreach ($Status as $s) {
                                if ($s->id == $Tipo_documento->status_id)
                                    echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                else
                                    echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Tipo_documento->id; ?>">

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Tipo_documento', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>