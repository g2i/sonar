<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Banco</h2>
        <ol class="breadcrumb">
            <li>Banco</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Banco', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome">Nome *</label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   placeholder="Nome" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="agencia">Agência *</label>
                            <input type="text" name="agencia" id="agencia" class="form-control"
                                   placeholder="Agência" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="conta">Conta *</label>
                            <input type="text" name="conta" id="conta" class="form-control"
                                   placeholder="Conta" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="limite">Limite</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="limite" id="limite" class="form-control money2"
                                       value="0.00"
                                       placeholder="Limite">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="mostra_fluxo">Mostrar Fluxo?</label>
                            <select name="mostra_fluxo" id="mostra_fluxo" class="form-control">
                                <option value="1">Sim</option>
                                <option value="0" selected>Não</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="mostra_saldo_geral">Mostrar Saldo Geral?</label>
                            <select name="mostra_saldo_geral" id="mostra_saldo_geral" class="form-control">
                                <option value="1">Sim</option>
                                <option value="0" selected>Não</option>
                            </select>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Banco', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>