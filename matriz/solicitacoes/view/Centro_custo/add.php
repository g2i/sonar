<?php if ($this->getParam('modal') != 1): ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Centro de Custos</h2>
        <ol class="breadcrumb">
            <li>centro de custos</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
<?php endif; ?>
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Centro_custo', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="form-group">
                        <label class="required" for="descricao">Descrição <span
                                class="glyphicon glyphicon-asterisk"></span></label>
                        <input type="text" name="descricao" id="descricao" class="form-control"
                               value="<?php echo $Centro_custo->descricao ?>" placeholder="Descricao" required>
                    </div>
                    <input type="hidden" name="status" value="1"/>
                    <div class="row">
                        <?php if ($this->getParam('modal') == 1): ?>
                            <input type="hidden" name="modal" value="1"/>
                            <div class="text-right">
                                <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
                                   data-toggle="tooltip2" title="Voltar"
                                   onclick="Navegar('','back')">
                                    Cancelar</a>
                                <input type="submit" onclick="FormularioAll('form','Centro_custo','centro_custo')"
                                       class="btn btn-primary" value="salvar">
                            </div>
                        <?php else: ?>
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Centro_custo', 'all') ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                        <?php endif; ?>
                    </div>
                </form>
<?php if ($this->getParam('modal') != 1): ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>