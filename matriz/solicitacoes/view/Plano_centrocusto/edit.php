<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Plano/Centro de Custo</h2>
        <ol class="breadcrumb">
            <li> Plano/Centro de Custo</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Plano_centrocusto', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">

                        <div class="form-group">
                            <label for="plano_contas">Plano de contas</label>
                            <input type="hidden" name="plano_contas"
                                   value="<?php echo $Plano_centrocusto->plano_contas ?>"/>
                            <input type="text" name="plano_conta"
                                   value="<?php echo $Plano_centrocusto->getPlanocontas()->nome ?>" class="form-control"
                                   disabled/>

                        </div>
                        <div class="form-group">
                            <label for="centro_custo">Centro custo</label>
                            <input type="hidden" name="centro_custo"
                                   value="<?php echo $Plano_centrocusto->centro_custo ?>"/>
                            <input type="text" name="centro_cust"
                                   value="<?php echo $Plano_centrocusto->getCentro_custo()->descricao ?>"
                                   class="form-control" disabled/>
                        </div>

                        <div class="form-group">
                            <label for="empresa">Empresa</label>
                            <input type="hidden" name="empresa" value="<?php echo $Plano_centrocusto->empresa ?>"/>
                            <input type="text" name="empres"
                                   value="<?php echo $Plano_centrocusto->getContabilidade()->nome ?>"
                                   class="form-control" disabled/>

                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Plano_centrocusto->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="required" for="porcentagem">Porcentagem <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" value="<?php echo $Plano_centrocusto->porcentagem ?>"
                                   class="form-control porcentagem" name="porcentagem" id="porcentagem"
                                   placeholder="Porcentagem"/>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Plano_centrocusto->id; ?>">

                    <div class="row">
                        <?php if ($this->getParam('modal') == 1) { ?>
                            <input type="hidden" name="modal" value="1"/>

                            <div class="text-right">
                                <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
                                   data-toggle="tooltip2" title="Voltar"
                                   onclick="Navegar('','back')">
                                    Cancelar</a>
                                <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                                       value="salvar">
                            </div>
                        <?php } else { ?>
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Plano_centrocusto', 'all') ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                        <?php } ?>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('.porcentagem').mask("#.##0,00", {reverse: true, maxlength: false});
</script>