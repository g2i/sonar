<div class="row">
    <div class="col-md-12">
        <div class="col-sm-6  text-right pull-right">
            <h1>
                <?php echo '<a href="javascript:;" data-placement="bottom" class="btn btn-info" data-toggle="tooltip2" title="Adicionar Rateio" onclick="Navegar(\''.$this->Html->getUrl("Plano_centrocusto","add", array("ajax" => true, "modal"=>"1","plano"=>$this->getParam('id'))).'\',\'go\')">'; ?>
                <span class="glyphicon glyphicon-plus"></span> Rateio</a>

            </h1>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-md-12">
        <div class="table-responsive table_plano_centro">
            <table class="table table-hover">
                <tr>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Plano_centrocusto', 'all', array('orderBy' => 'plano_contas', 'search' => $search)); ?>'>
                            Plano Contas
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Plano_centrocusto', 'all', array('orderBy' => 'centro_custo', 'search' => $search)); ?>'>
                            Centro Custo
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Plano_centrocusto', 'all', array('orderBy' => 'empresa', 'search' => $search)); ?>'>
                            Contabilidade
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Plano_centrocusto', 'all', array('orderBy' => 'porcentagem', 'search' => $search)); ?>'>
                            Porcentagem
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Plano_centrocusto', 'all', array('orderBy' => 'status', 'search' => $search)); ?>'>
                            Status
                        </a>
                    </th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                <?php
                $aux = 0;
                foreach ($Plano_centrocustos as $p) {
                    echo '<tr>';

                    echo '<td>';
                    echo $this->Html->getLink($p->getPlanocontas()->nome, 'Planocontas', 'view',
                        array('id' => $p->getPlanocontas()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    echo '</td>';

                    echo '<td>';
                    echo $this->Html->getLink($p->getCentro_custo()->descricao, 'Centro_custo', 'view',
                        array('id' => $p->getCentro_custo()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    echo '</td>';

                    echo '<td>';
                    echo $this->Html->getLink($p->getContabilidade()->nome, 'Contabilidade', 'view',
                        array('id' => $p->getContabilidade()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    echo '</td>';

                    echo '<td>';
                    echo $this->Html->getLink($p->porcentagem, 'Plano_centrocusto', 'view',
                        array('id' => $p->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    echo '</td>';
                    $aux += $p->porcentagem;
                    echo '<td>';
                    echo $this->Html->getLink($p->getStatus()->descricao, 'Status', 'view',
                        array('id' => $p->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    echo '</td>';
                    if ($this->getParam('modal')) {
                        echo '<td>';
                        echo '<a href="javascript:;" data-placement="bottom" data-toggle="tooltip2" title="Editar" onclick="Navegar(\'' . $this->Html->getUrl("Plano_centrocusto", "edit", array("ajax" => true, "id" => $p->id, "modal" => "1")) . '\',\'go\')">
                                <span class="glyphicon update"></span></a>';
                        echo '</td>';

                        echo '<td>';
                        echo '<a href="javascript:;" data-placement="bottom" data-toggle="tooltip2" title="Deletar" onclick="Navegar(\'' . $this->Html->getUrl("Plano_centrocusto", "delete", array("ajax" => true, "id" => $p->id, "modal" => "1")) . '\',\'go\')">
                                <span class="glyphicon delet"></span></a>';
                        echo '</td>';
                    } else {
                        echo '<td>';
                        echo $this->Html->getLink('<span class="glyphicon update"></span> ', 'Plano_centrocusto', 'edit',array('id' => $p->id),array('data-role'=>'tooltip' ,'data-placement'=>'bottom','title'=>'Editar'));
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink('<span class="glyphicon delet"></span> ', 'Plano_centrocusto', 'delete',array('id' => $p->id),array('data-role'=>'tooltip' ,'data-placement'=>'bottom','title'=>'Deletar', 'data-toggle' => 'modal'));
                        echo '</td>';
                    }
                    echo '</tr>';
                }
                echo '<tr>';
                echo '<td>&nbsp;</td>';
                echo '<td>&nbsp;</td>';
                echo '<td>Total:</td>';
                echo '<td>' . $aux . '%</td>';
                echo '<td>&nbsp;</td>';
                echo '<td>&nbsp;</td>';

                echo '</tr>';
                ?>
            </table>
            <!-- menu de paginação -->
            <div style="text-align:center"><?php echo $nav; ?></div>
        </div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function () {
        $('#buscar').click(function () {
            var r = true;
            if (r) {
                r = false;
                $("div.table_plano_centro").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Plano_centrocusto', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($(".search").val()) + " .table_plano_centro"';
                    else
                        echo '"' . $this->Html->getUrl('Plano_centrocusto', 'all') . '&search=" + encodeURIComponent($(".search").val()) + " .table_plano_centro"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });
    });
</script>