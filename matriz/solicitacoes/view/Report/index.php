<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relat&oacute;rios</h2>
        <ol class="breadcrumb">
            <li>Relat&oacute;rio</li>
            <li class="active">
                <strong><?php echo $relatorio; ?></strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div id="viewerContent"></div>
                <div class="clear-fix"></div>
            </div>
        </div>
    </div>
</div>
<script>
    var reportId = '<?php echo $report ?>';
    $(document).ready(function () {
        var url = root + '/Report/getReport?id=' + reportId;
        var report = new Stimulsoft.Report.StiReport();
        report.loadFile(url);

        // View report in Viewer
        var viewer = new Stimulsoft.Viewer.StiViewer(null, "StiViewer", null);
        viewer.report = report;
        viewer.onGetReportData = function (event) {
            $.ajax({
                url: root + '/Report/getDados',
                data:{
                  id: reportId
                },
                async: false,
                success: function(data) {
                    var dataSet = new System.Data.DataSet("Data");
                    dataSet.readJson(data);
                    event.report.regData("Data", "Data", dataSet);
                    console.log(data);
                }
            });
        }
        viewer.renderHtml("viewerContent");
    });
</script>