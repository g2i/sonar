<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>relatorio</li>
            <li class="active">
                <strong>mensal</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <form class="form-inline">
                        <div class="form-group" style="width: 250px">
                            <label for="grupos" class="form-group">Grupos</label>
                            <div class="input-group select2-bootstrap-append">
                                <select name="grupos" class="form-control" id="grupos">
                                    <option></option>
                                    <?php foreach ($grupos as $e): ?>
                                        <option value="<?php echo $e->id ?>"><?php echo $e->descricao ?></option>
                                    <?php endforeach; ?>
                                </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-clear="grupos"
                                                data-toggle="tooltip" data-placement="top" title="Limpar">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group" style="width: 250px">
                            <label for="empresa">Empresa</label>
                            <div class="input-group select2-bootstrap-append">
                                <select name="empresa" class="form-control" id="empresa">
                                    <option></option>
                                </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-clear="empresa"
                                                data-toggle="tooltip" data-placement="top" title="Limpar">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                            </div>
                        </div>
<!--                        <div class="form-group " style="width: 110px">-->
<!--                            <label for="ano" class="form-group">Ano</label><br/>-->
<!--                            <div class="input-group select2-bootstrap-append">-->
<!--                                <select id="ano" name="ano" class="form-control"-->
<!--                                        data-placeholder="Atual">-->
<!--                                    <option></option>-->
<!--                                    --><?php //for ($i = date('Y', strtotime("-1 year")); $i >= date("Y", strtotime("-5 year")); $i--): ?>
<!--                                        <option value='--><?//= $i ?><!--'>--><?//= $i ?><!--</option>-->
<!--                                    --><?php //endfor; ?>
<!--                                </select>-->
<!--                                <span class="input-group-btn">-->
<!--                                    <button class="btn btn-default" type="button" data-select2-clear="ano"-->
<!--                                            data-toggle="tooltip" data-placement="top" title="Ano Atual">-->
<!--                                        <i class="fa fa-calendar-check-o"></i>-->
<!--                                    </button>-->
<!--                                </span>-->
<!--                            </div>-->
<!--                        </div>-->


                        <div class="form-group " style="width: 150px">
                            <label for="ano" class="form-group">Ano</label><br/>
                            <div class="input-group select2-bootstrap-append">
                                <select id="ano" name="ano" class="form-control selectPicker"
                                        data-placeholder="Selecione">
                                    <option></option>
                                    <?php
                                    for ($i = date('Y'); $i >= date("Y", strtotime("-5 year")); $i--) {
                                        echo "<option value='" . $i . "'>" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-clear="ano"
                                            data-toggle="tooltip" data-placement="top" title="Ano Atual">
                                        <i class="fa fa-calendar-check-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group" style="width: 100px">
                            <label for="mes" class="form-group">Mês</label>
                            <select id="mes" name="mes" class="form-control selectPicker"
                                    data-placeholder="Todos">
                                <option></option>
                                <?php
                                for ($i = 1; $i <= count($Mes); $i++) {
                                    echo "<option value='" . $i . "'>" . $Mes[$i] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" style="margin-top: 25px">
                            <button id="btnFiltro" type="button" class="btn btn-default" id="filtro"><span
                                    class="glyphicon glyphicon-search"></span></button>
                            <button id="btnRelAna" data-role="tooltip" data-placement="bottom" title="Relatório Analítico"
                                    type="button" class="btn btn-default">
                                <span class="fa fa-print"></span> Analítico
                            </button>
                            <button id="btnRelSint" data-role="tooltip" data-placement="bottom"
                                    title="Relatório Sintético" type="button" class="btn btn-default">
                                <span class="fa fa-print"></span> Sintético
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <div class="ibox">
            <div class="ibox-content">
                <div id="dados" class="row clearfix">
                    <div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function getDados() {
        var url = root + '/Relatorio/Demonstrativo';
        var data1 = {
            tipo: 1,
            grupo: $('#grupos').val(),
            empresa: $('#empresa').val() ? $('#empresa').val().join(',') : '',
            mes: $("#mes").val(),
            ano: $("#ano").val()
        }

        var data2 = {
            tipo: 2,
            grupo: $('#grupos').val(),
            empresa: $('#empresa').val() ? $('#empresa').val().join(',') : '',
            mes: $("#mes").val(),
            ano: $("#ano").val()
        }

        var dados = $("#dados");
        dados.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        var entrada  = $.post(url, data1);
        var saida  = $.post(url, data2);
        $.when(entrada, saida).done(function(entradaResult, saidaResult){
            if(entradaResult[1] != 'success' || saidaResult[1] != 'success'){
                dados.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            var lances = $('<div class="col-md-12" id="lances"></div>');
            lances.html(entradaResult[0]);

            var saida = $('<div class="col-md-12" id="saida"></div>');
            saida.html(saidaResult[0]);

            var table = $('<table><thead><tr><td colspan="4"></td></tr></thead></table>');
            var tBody = $('<tbody></tbody>');

            var entRown = $('<tr></tr>');
            var entCol = $('<td colspan="4"></td>');
            entCol.append('<div class="col-md-12"><h2>Entradas</h2></div>');
            entCol.append(lances);
            entRown.append(entCol);
            tBody.append(entRown);

            var saidRown = $('<tr></tr>');
            var saidCol = $('<td colspan="4"></td>');
            saidCol.append('<div class="col-md-12"><h2>Saídas</h2></div>');
            saidCol.append(saida);
            saidRown.append(saidCol);
            tBody.append(saidRown);

            var TEcredito = $(entRown.find("#TEcredito")[0]).text();
            TEcredito = replaceAll(TEcredito, '.', '');
            TEcredito = TEcredito.replace(',', '.');
            TEcredito = TEcredito.replace('R$', '');

            var TEdebito = $(entRown.find("#TEdebito")[0]).text();
            TEdebito = replaceAll(TEdebito, '.', '');
            TEdebito = TEdebito.replace(',', '.');
            TEdebito = TEdebito.replace('R$', '');

            var TScredito = $(saidRown.find("#TScredito")[0]).text();
            TScredito = replaceAll(TScredito, '.', '');
            TScredito = TScredito.replace(',', '.');
            TScredito = TScredito.replace('R$', '');

            var TSdebito = $(saidRown.find("#TSdebito")[0]).text();
            TSdebito = replaceAll(TSdebito, '.', '');
            TSdebito = TSdebito.replace(',', '.');
            TSdebito = TSdebito.replace('R$', '');

            var TEsoma = $(entRown.find("#TEsoma")[0]).text();
            TEsoma = replaceAll(TEsoma, '.', '');
            TEsoma = TEsoma.replace(',', '.');
            TEsoma = TEsoma.replace('R$', '');

            var TSsoma = $(saidRown.find("#TSsoma")[0]).text();
            TSsoma = replaceAll(TSsoma, '.', '');
            TSsoma = TSsoma.replace(',', '.');
            TSsoma = TSsoma.replace('R$', '');
            TSsoma = TSsoma.replace('-', '');

            var tcredito = parseFloat(Number(TEcredito) - Number(TScredito));
            var tdebito = parseFloat(Number(TEdebito) - Number(TSdebito));
            var tsoma = parseFloat(Number(TEsoma) - Number(TSsoma));

            var footer = $('<tfoot></tfoot>');
            var footRown = $('<tr></tr>');
            footRown.append('<td style="padding-left: 40px"><h4>Resultado:</h4></td>');
            footRown.append('<td style="text-align: right; padding-left: 120px;"><h4>R$ ' + tcredito.formatMoney(2, ',', '.') + '</h4></td>');
            footRown.append('<td style="text-align: right; padding-left: 30px;"><h4>R$ ' + tdebito.formatMoney(2, ',', '.') + '</h4></td>');
            footRown.append('<td style="text-align: right; padding-right: 60px"><h4>R$ ' + tsoma.formatMoney(2, ',', '.') + '</h4></td>');
            footer.append(footRown);

            table.append(tBody);
            table.append(footer);
            dados.html('');
            var responsive = $('<div class="table-responsive"></div>');
            responsive.append(table);
            dados.append(responsive);
            responsive.mCustomScrollbar({
                axis:"x",
                theme:"minimal-dark",
            });
        });
    }

    $(document).ready(function(){
        var grupos = $('#grupos').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Todos'
        });

        var empresa = $('#empresa').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            multiple: true,
            placeholder: 'Todas'
        });

        grupos.change(function() {
            if ($(this).val() == '') {
                empresa.select2('destroy');
                empresa.val('');
                empresa.html('');
                empresa.append($('<option></option>'));
                empresa.select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    multiple: true,
                    placeholder: 'Todas'
                });
            }
            else {
                var url = root + '/Index/getEmpresas';
                var data = {
                    id: $(this).val()
                }

                $.get(url, data, function (ret) {
                    if (ret.result) {
                        empresa.select2('destroy');
                        empresa.html('');
                        empresa.append($('<option></option>'));
                        ret.dados.forEach(function (dado) {
                            var option = $('<option value="' + dado.id + '" >' + dado.nome + '</option>');
                            empresa.append(option);
                        });
                        empresa.select2({
                            language: 'pt-BR',
                            theme: 'bootstrap',
                            width: '100%',
                            multiple: true,
                            placeholder: 'Todas'
                        });
                    } else {
                        BootstrapDialog.alert(ret.msg);
                    }
                });
            }
        });

        $('#ano').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Atual'
        });

        $("button[data-select2-clear]").click(function() {
            $("#" + $(this).data("select2-clear")).val('').trigger('change');
        });

        $('#btnFiltro').click(function(){
            getDados();
        });

        $('#btnRelAna').click(function(){
            var data ={
                mes: $("#mes").val(),
                ano: $("#ano").val(),
                grupo: $("#grupos").val(),
                empresa: $("#empresa").val() ?  $('#empresa').val().join(',') : ''
            }

            if(data.grupo == ''){
                data.grupo = -1;
            }

            if(data.empresa == ''){
                data.empresa = -1;
            }

            if (data.mes == '') {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione um mes!',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return false;
            }

            if (data.ano == '') {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione um ano!',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return false;
            }

//            if (data.ano == '') {
//                data.ano = 2016;
//            }

            $.ReportPost('d_analitico', data);
        });

        $('#btnRelSint').click(function(){
            var data ={
                mes: $("#mes").val(),
                ano: $("#ano").val(),
                grupo: $("#grupos").val(),
                empresa: $("#empresa").val() ?  $('#empresa').val().join(',') : ''
            }

            if(data.grupo == ''){
                data.grupo = -1;
            }

            if(data.empresa == ''){
                data.empresa = -1;
            }

            if (data.mes == '') {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione um mes!',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return false;
            }

            if (data.ano == '') {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione um ano!',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return false;
            }

//            if (data.ano == "") {
//                data.ano = 2016;
//            }

            $.ReportPost('d_sint', data);
        });
    });
</script>
