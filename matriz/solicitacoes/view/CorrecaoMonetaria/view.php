<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Corre��o Monet�ria</h2>
        <ol class="breadcrumb">
            <li>corre��o monet�ria</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Nome</strong>: <?php echo $CorrecaoMonetaria->nome; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($CorrecaoMonetaria->getStatus()->descricao, 'Status', 'view',
                        array('id' => $CorrecaoMonetaria->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>