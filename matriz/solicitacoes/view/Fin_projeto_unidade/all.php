
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Fin_projeto_unidade</h2>
    <ol class="breadcrumb">
    <li>Fin_projeto_unidade</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Fin_projeto_unidade', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'fin_projeto_empresa_id')); ?>'>
                        fin_projeto_empresa_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'nome')); ?>'>
                        nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'situacao_id')); ?>'>
                        situacao_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'created')); ?>'>
                        created
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'modified')); ?>'>
                        modified
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'user_modified')); ?>'>
                        user_modified
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => 'user_created')); ?>'>
                        user_created
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Fin_projeto_unidades as $f) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($f->id, 'Fin_projeto_unidade', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->nome, 'Fin_projeto_unidade', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->created, 'Fin_projeto_unidade', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->modified, 'Fin_projeto_unidade', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->getFin_projeto_empresa()->nome, 'Fin_projeto_empresa', 'view',
                    array('id' => $f->getFin_projeto_empresa()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->getSituacao()->nome, 'Situacao', 'view',
                    array('id' => $f->getSituacao()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->getUsuario()->nome, 'Usuario', 'view',
                    array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->getUsuario2()->nome, 'Usuario', 'view',
                    array('id' => $f->getUsuario2()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Fin_projeto_unidade', 'edit', 
                    array('id' => $f->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Fin_projeto_unidade', 'delete', 
                    array('id' => $f->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Fin_projeto_unidade', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Fin_projeto_unidade', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>