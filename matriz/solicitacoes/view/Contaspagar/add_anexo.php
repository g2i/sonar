<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas Receber</h2>
        <ol class="breadcrumb">
            <li>Receber</li>
            <li class="active">
                <strong>Cadastrar Anexo</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div id="msg_error"></div>

                <div class="clearfix">
                    <form method="post" role="form" id="formAnexo" action="<?php echo $_POST['redirecionar']; ?>">
                        <input type="hidden" name="contaspagar_id" id="contaspagar_id"
                               value="<?php echo $_POST['contaspagar_id']; ?>"/>
                        <input type="hidden" name="upload_anexo" id="upload_anexo" value="1"/>

                        <div class="form-group">
                            <label for="titulo">Titulo</label>
                            <input type="text" name="titulo" id="titulo" class="form-control" placeholder="Titulo">
                        </div>
                        <div class="form-group">
                            <label for="tipo">Tarefa</label>
                            <select name="tipo" class="form-control" id="tipo">
                                <option value="Abertura">Abertura</option>
                                <option value="Conclusao">Conclusao</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="arquivo">Arquivo</label>
                            <input type="file" name="arquivo" id="arquivo" class="form-control">
                        </div>
                        <div style="clear: both"></div>
                        <div class="text-right">
                            <a href="#" class="cancelar btn btn-default">voltar</a>
                            <a href="#" class="enviar btn btn-primary">salvar</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.cancelar').click(function () {
            $('div.modal').find('.modal-content').load('<?php echo $_POST['redirecionar']; ?>');
        })

        $('div#msg_error').click(function () {
            $(this).html('');
        });

        $('.enviar').click(function () {
            $.ajax({
                type: "POST",
                url: "<?php echo $_POST['redirecionar']; ?>",
                data: $('#formAnexo').serialize(),
                success: function (data) {
                    if (data == "true") {
                        $('div.modal').find('.modal-content').load('<?php echo $_POST['redirecionar']; ?>');
                    } else {
                        $('div#msg_error').html('<div class="alert alert-warning">' + data + '</div>');
                    }
                }
            });

            return false;
        })
    })
</script>