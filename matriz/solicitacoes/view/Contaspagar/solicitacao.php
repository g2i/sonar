<style>.colorido {
  background: red;
}
</style>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-info pull-right">Aceitas</span>
                            <h5>Solicitações</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                <?php echo $solicitacaoAceitaTotal->total; ?>
                            </h1>
                            <div class="stat-percent font-bold text-success"><i class="fa fa-bolt""></i></div>
                            <small>Aceitas</small>
                        </div>
                    </div>
                </div>
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">Esperando aceite</span>
                        <h5>Solicitações</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $solicitacaoAguardandoAceiteTotal->total; ?>
                        </h1>
                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>Esperando aceite</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">Recusadas</span>
                        <h5>Solicitações</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $solicitacaoRecusadaTotal->total; ?>
                        </h1>
                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>Recusadas</small>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- formulario de pesquisa -->
                        <div class="filtros well">
                            <div class="form">
                                <form role="form"
                                      action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                      method="post" enctype="application/x-www-form-urlencoded">
                                    <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                    <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                    <div class="col-xs-6 col-md-3">
                                        <label for="inicio">Inicio</label>
                                        <div class='input-group dateInicio'>
                                            <input type='text' class="form-control dateFormat" name="inicio" id="inicio"
                                                   value="<?php echo $inicio; ?>"/>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <label for="fim">Fim</label>
                                        <div class='input-group dateFim'>
                                            <input type='text' class="form-control dateFormat" name="fim" id="fim"
                                                   value="<?php echo $fim; ?>"/>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="idFornecedor">Fornecedor</label>
                                        <select name="idFornecedor" class="form-control select-fornecedor"
                                                id="idFornecedor">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Fornecedores as $f): ?>
                                                <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>'; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="projeto">Projeto</label>
                                        <input type="text" name="projeto" id="projeto" class="form-control"
                                               value="<?php echo $this->getParam('projeto'); ?>">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="situacao_solicitacao_id">Situação</label>
                                        <select name="situacao_solicitacao_id" class="form-control"
                                                id="situacao_solicitacao_id">
                                            <?php foreach ($Situacao_solicitacao as $f): ?>
                                                <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>'; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                           class="btn btn-default"
                                           data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                           title="Recarregar a página"><span
                                                    class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="button" class="btn btn-default" id="buscar-filtro"
                                                data-tool="tooltip" data-placement="bottom" title="Pesquisar"><span
                                                    class="glyphicon glyphicon-search"></span></button>

                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Solicitações</h5>
                     <!-- botao de cadastro -->
                <div class="text-right">
                    <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Solicitação', 'Solicitacao', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Solicitante</th>
                                <th>Data Documento</th>
                                <th>Fornecedor</th>
                                <th>Empresa</th>
                                <th>Aplicação/Projeto</th>
                                <th>Observações</th>
                                <th>Valor</th>
                                <th>Resp. Autorizar</th>
                                <th>Situação</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody class="solicitacaoT">
                            <?php // bg-success .bg-warning
                            foreach ($Solicitacaos as $d) {
                                //$class = ($d->situacao_solicitacao_id == 2) ? 'bg-danger' : '';
                                if ($d->situacao_solicitacao_id == 1) {
                                    $class = "bg-warning";
                                } else if ($d->situacao_solicitacao_id == 2) {
                                    $class = "bg-danger";
                                } else if ($d->situacao_solicitacao_id == 3) {
                                    $class = "bg-success";
                                } else if ($d->situacao_solicitacao_id == 4) {
                                    $class = "";
                                } else if ($d->situacao_solicitacao_id == 5) {
                                    $class = "bg-white";
                                }
                                echo '<tr class="' . $class . '">';
                                echo '<td>';
                                echo $d->id;
                                echo '</td>';
                                echo '<td>';
                                echo $d->solicitante;
                                echo '</td>';
                                echo '<td>';
                                echo convertDataSQL4BR($d->data);
                                echo '</td>';
                                echo '<td>';
                                echo $d->getFornecedor()->nome;
                                echo '</td>';
                                echo '<td>';
                                echo $d->getContabilidade()->nome;
                                echo '</td>';
                                echo '<td>';
                                echo $d->projeto;
                                echo '</td>';
                                echo '<td>';
                                echo $d->observacao;
                                echo '</td>';
                                echo '<td>';
                                echo 'R$ ';
                                echo number_format($d->valor_solicitacao, 2, ',', '.');
                                echo '</td>';
                                echo '<td>';
                                echo $d->getResponsavel()->nome;
                                echo '</td>';
                                echo '<td id="status">';
                                echo $d->getSituacao_solicitacao()->nome;
                                echo '</td>';
                                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                if ($d->situacao_solicitacao_id == 1 || $d->situacao_solicitacao_id == 5) {
                                    echo $this->Html->getLink('<span class="fa fa-exclamation-triangle" data-placement="top"></span> ', 'Contaspagar', 'solicitacaoContasPagar',
                                        array('id' => $d->id),
                                        array('class' => 'btn btn-warning btn-sm'));
                                } else {
                                    echo $this->Html->getLink('<span class="fa fa-folder-open-o"> </span> ', 'Contaspagar', 'solicitacaoContasPagar',
                                        array('id' => $d->id),
                                        array('class' => 'btn btn-info btn-sm'));
                                }
                                echo '</td>';
                                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                echo '<button type="button" data-toggle="tooltip" data-placement="top" title="Pasta de Documentos/Anexos" class="btn btn-primary command-galeria" data-row-id="' . $d->id . '"><span class="fa fa-paperclip"></span></button>';
                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>
                            </tbody>
                        </table>

                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php
                            echo $nav;
                            ?></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
//function para o btn de pesquisa em form
$(function(){
    $('.select-fornecedor').select2({
        language: 'pt-BR',
        theme: 'bootstrap',
        width: '100%',
        placeholder: 'Selecione:'
    });
});

$('.filtros').find('#buscar-filtro').click(function(){
        let form_action = $(this).closest('form').attr('action');
        let form_serialize = $(this).closest('form').serialize();
        let url_completa = form_action + '?' + form_serialize;
        console.log(url_completa);
        carregaTabelaResponsiva(url_completa);
    });

//function para paginar por ajax
    $(document).on('click','.pagination a, .table thead a', function(e){
        e.preventDefault();
        let url = $(this).attr('href');

        if(url != "")
        {
            carregaTabelaResponsiva(url);
        }
        return false;
    });

    $('button.command-galeria').each(function () {
        var conta = $(this).data("row-id");
        $(this).click(function () {
            var urlLista = root + '/Anexo_financeiro/all/origem:4/id_externo:' + conta;
            window.open(urlLista, '_blank');

        });
    });
   //function para pegar as URLs
function carregaTabelaResponsiva(url) {
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: () => {
        },
        success: (data) => {
            let conteudo = $('<div>').append(data).find('.table-responsive');
            $(".table-responsive").html(conteudo);
        },
        complete: () => {
        },
        error: () => {
        },
    });
}
</script>