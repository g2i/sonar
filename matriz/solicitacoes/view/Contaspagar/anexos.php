<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas a Pagar</h2>
        <ol class="breadcrumb">
            <li>Pagar</li>
            <li class="active">
                <strong>Anexos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- botao de cadastro -->
                <?php if ($Contaspagar): ?>
                    <div class="text-right pull-right">
                        <p><?php
                            echo $this->Html->getLink('Anexar arquivo', 'Contaspagar', 'add_anexo',
                                array(
                                    'contaspagar_id' => $Contaspagar->id,
                                    'ajax' => 'true'
                                ),
                                array(
                                    'class' => 'btn btn-success btn-sm',
                                    'title' => 'Anexar arquivo',
                                    'id' => 'abrir-modal',
                                    'data-url' => $this->Html->getUrl('Contaspagar', 'anexos', array('contaspagar_id' => $Contaspagar->id, 'ajax' => 'true'))
                                )
                            );
                            ?></p>
                    </div><br/>
                <?php endif; ?>


                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>Titulo</th>
                                <th>Tipo</th>
                                <th>Anexo</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php
                            foreach ($Anexos as $a) {
                                echo '<tr>';
                                echo '<td>' . $a->titulo . '</td>';
                                echo '<td>' . $a->tipo . '</td>';

                                if (file_exists($a->caminho)) {
                                    echo '<td width="50">';
                                    echo '<a href="' . $a->caminho . '" target="_new" title="Download do anexo" class="btn btn-success btn-sm">
                            <span class="glyphicon glyphicon-download"></span>
                        </a>';
                                    echo '</td>';
                                } else {
                                    echo '<td width="100"> Sem arquivo </td>';
                                }


                                echo '<td width="50">';
                                echo '<a href="' . $this->Html->getUrl('Contaspagar', 'delete_anexo', array('id' => $a->id, 'ajax' => 'true')) . '" title="Excluir anexo" class="btn btn-danger btn-sm btn-remove_anexo">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>';
                                echo '</td>';

                                echo '</tr>';
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('#abrir-modal').click(function () {
            $('div.modal').find('.modal-content').load($(this).attr('href'), {
                'redirecionar': $(this).data('url'),
                'contaspagar_id': '<?php echo $Contaspagar->id; ?>'
            });
            return false;
        })

        $('.btn-remove_anexo').click(function () {
            var pop = confirm('Deseja mesmo excluir este anexo?');
            if (pop) {
                $('div.modal').find('.modal-content').load($(this).attr('href'), {
                    'redirecionar': '<?php echo $this->Html->getUrl('Contaspagar', 'anexos',array( 'contaspagar_id' => $Contaspagar->id, 'ajax'=>'true' ))?>',
                    'contaspagar_id': '<?php echo $Contaspagar->id; ?>'
                });
            }
            return false;
        })
    })
</script>