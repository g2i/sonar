<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Solicitação</h2>
        <ol class="breadcrumb">
            <li>Solicitacao</li>
            <li class="active">
                <strong>Parcelas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="border-color: #428bca;">
                    <h5>Solicitações</h5>
                </div>
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->

                    <div class="form">
                        <form role="form" id="solicitacao"
                              action="<?php echo $this->Html->getUrl('Contaspagar', 'solicitacaoContasPagar') ?>"
                              method="post" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                            <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                            <div class="clearfix"></div>
                            <input type="hidden" name="solicitacao_id" value="<?php echo $Solicitacao_id; ?>">
                            <input type="hidden" id="motivo_recusa" name="motivo_recusa" value="">
                            <input type="hidden" id="recusar_action" name="recusar" value="">
                    </div>

                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Solicitante</th>
                                    <th>Data Documento</th>
                                    <th>Empresa</th>
                                    <th>Aplicação/Projeto</th>
                                    <th>Meio de Pagamento</th>
                                    <th>Observações</th>
                                    <th>Opções</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($Solicitacao as $d) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $d->id;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->solicitante;
                                    echo '</td>';
                                    echo '<td>';
                                    echo convertDataSQL4BR($d->data);
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->getContabilidade()->nome;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->projeto;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->meio_pagamento;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->observacao;
                                    echo '</td>';
                                    echo '<td>';
                                    echo '<button type="button" data-toggle="tooltip" data-placement="top" title="Anexos" style="margin-right:3px" class="btn btn-xs btn-info command-anexos-solicitacao" data-row-id="' . $d->id . '"><span class="fa fa-book"></span></button>';
                                    echo $this->Html->getLink('<span class="fa fa-paperclip"></span> ', 'Anexo_financeiro', 'all',
                                        array('origem'=>4,'id_externo'=>$d->id),
                                        array('class' => 'btn btn-xs btn-info '));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="filtros well">
                        <div class="ibox-title" style="border-color: #428bca;">
                            <h5>Parcelas</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Valor</th>
                                        <th>Fornecedor</th>
                                        <th>Empresa</th>
                                        <th>Tipo Documento</th>
                                        <th>Vencimento</th>
                                        <th>Observação</th>
                                        <th>Parcela</th>
                                        <th>opções</th>
                                    </tr>
                                    <?php
                                    foreach ($Contaspagar as $r) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo "R$ ";
                                        echo $r->valor;
                                        echo '<input type="hidden" name="contaspagar_id[]" value="' . $r->id . '" >';
                                        echo '</td>';
                                        echo '<td>';
                                        echo $r->getFornecedor()->nome;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $r->getContabilidade()->nome;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $r->getTipo_documento()->descricao;
                                        echo '</td>';
                                        echo '<td>';
                                        echo convertDataSQL4BR($r->vencimento);
                                        echo '</td>';
                                        echo '<td>';
                                        echo $r->complemento;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $r->parcela;
                                        echo '</td>';
                                        echo '<td width="30" style="padding-top: 5px; padding-bottom: 0px;">';
                                        echo '<button type="button" data-toggle="tooltip" data-placement="top" title="Pasta de Documentos/Anexos" style="margin-right:3px" class="btn btn-xs btn-primary command-galeria" data-row-id="' . $r->id . '"><span class="fa fa-book"></span></button>';
                                        echo $this->Html->getLink('<span class="fa fa-paperclip"></span> ', 'Anexo_financeiro', 'all',
                                            array('origem'=>1,'id_externo'=>$r->id),
                                            array('class' => 'btn btn-xs btn-info '));
                                        echo '</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                </table>

                                <div style="text-align:center">
                                    <?php echo $nav; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <?php if ($Solicitacao[0]->situacao_solicitacao_id == 1 || $Solicitacao[0]->situacao_solicitacao_id == 5) { ?>
                            <input type="submit" class="btn btn-primary" value="Aceitar">
                            <input type="button" class="btn btn-danger" value="Recusar" id="Recusar">
                            <a href="<?php echo $this->Html->getUrl('Contaspagar', 'solicitacao') ?>"
                               class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                        <?php }
                        else {
                            echo $this->Html->getLink('<span class="fa fa-folder-open-o"> Visualizar/Editar</span> ', 'Solicitacao', 'edit',
                                array('id' => $d->id, 'visualizar' => 40),
                                array('class' => 'btn btn-info'));
                        } ?>
                    </div>


                    <script>

                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {

                            $('#Recusar').on('click',function(ev){
                                BootstrapDialog.show({
                                    title: 'Recusar',
                                    message: $('<textarea id="recusa" class="form-control" placeholder="Informe o motivo da recusa"></textarea>'),
                                    type: BootstrapDialog.TYPE_DEFAULT,
                                    buttons: [{
                                        icon: 'fa fa-ban',
                                        label: 'Cancelar',
                                        cssClass: 'btn-danger',
                                        action: function (dialog) {
                                            dialog.close();
                                        }
                                    },{
                                        icon: 'fa fa-floppy-o',
                                        label: 'Salvar',
                                        cssClass: 'btn-info',
                                        autospin: true,
                                        action: function (dialog) {
                                            $('#recusar_action').val('Recusar');

                                            setTimeout(function(){
                                                $('#solicitacao').submit();
                                                dialog.close();
                                            }, 2000);
                                        }
                                    }],
                                    onshown: function(dialog){
                                        $('#recusa').on('keypress',function(ev){
                                            $('#motivo_recusa').val($(this).val());
                                        });
                                    },
                                    onhidden: function(dialogRef){
                                        $('#motivo_recusa').val('');
                                    }
                                });
                            });
                            
                            $('.command-anexos-solicitacao').on('click', function () {
                                var conta = $(this).attr('data-row-id');
                                var origem = 4;//solicitacao
                                var url = root + '/Anexo_financeiro/galeria/modal:1/ajax:true/origem:' + origem + '/id_externo:' + conta + '/hide:1';

                                $('<div></div>').load(url, function () {
                                    BootstrapDialog.show({
                                        title: 'Visualizar Documentos Anexados',
                                        message: this,
                                        size: 'size-wide',
                                        type: BootstrapDialog.TYPE_DEFAULT,
                                        onshown: function (dialog) {
                                            var body = dialog.getModalBody();
                                            initAnexos(body);
                                            dialog.getModal().removeAttr('tabindex');
                                        },
                                        buttons: [{
                                            label: 'Fechar',
                                            action: function (dialog) {
                                                dialog.close();
                                            }
                                        }]
                                    });

                                });
                            });

                            $('.command-galeria').on('click', function () {
                                var conta = $(this).attr('data-row-id');
                                var origem = 1;//contas a pagar
                                var url = root + '/Anexo_financeiro/galeria/modal:1/ajax:true/origem:' + origem + '/id_externo:' + conta + '/hide:1';

                                $('<div></div>').load(url, function () {
                                    BootstrapDialog.show({
                                        title: 'Visualizar Documentos Anexados',
                                        message: this,
                                        size: 'size-wide',
                                        type: BootstrapDialog.TYPE_DEFAULT,
                                        onshown: function (dialog) {
                                            var body = dialog.getModalBody();
                                            initAnexos(body);
                                            dialog.getModal().removeAttr('tabindex');
                                        },
                                        buttons: [{
                                            label: 'Fechar',
                                            action: function (dialog) {
                                                dialog.close();
                                            }
                                        }]
                                    });

                                });
                            });

                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Rhprojetos', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Rhprojetos', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });

                        });

                    </script>
                </div>
            </div>
        </div>
    </div>
</div>