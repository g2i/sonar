<style type="text/css">
    .nav-tabs > li > a {
        border: 1px solid #ddd;
    }
    .nav-tabs {
        border-bottom: none;
    }
</style>
<?php if($this->getParam('modal')): ?>
    <style>
        .select2-close-mask{
            z-index: 2099;
        }
        .select2-dropdown{
            z-index: 3051;
        }
        .has-error .select2-selection {
            border: 1px solid #a94442;
            border-radius: 4px;
        }
    </style>
<?php else: ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas a Pagar</h2>
        <ol class="breadcrumb">
            <li>Pagar</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <?php endif; ?>
                <div class="alert alert-info">Os campos marcados com <span
                        class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                </div>
                <form id="frmConta" method="post" role="form" action="<?php echo $this->Html->getUrl('Contaspagar', 'add') ?>"
                      id="formIndividual">
                    <input type="hidden" name="formulario" value="individual">
                                    <!-- passa o parametro da id do profissional -->
           
                <input type="hidden" name="solicitacao_id" value="<?php echo $this->getParam('id'); ?>" />
                <input type="hidden" name="meio_pagamento" value="<?php echo $meioPagamento ?>" /> 
                <input type="hidden" name="fin_projeto_unidade_id" value="<?php echo $Solicitacao->fin_projeto_unidade_id ?>" />

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Data do Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="data" id="data" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="vencimento">Vencimento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group dateVencimento'>
                                <input type='text' class="form-control dateFormat" name="vencimento" id="vencimento" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="valorBruto">Valor Bruto (Parcela)<span class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" value="0.00" name="valorBruto" id="valorBruto"
                                       class="form-control money2" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="juros">Juros <span class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" value="0.00" name="juros" id="juros"
                                       class="form-control money2" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label  class="required" for="multa">Multa<span class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" value="0.00" name="multa" id="multa"
                                       class="form-control money2" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="desconto">Desconto<span class="glyphicon glyphicon-asterisk"></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="desconto" value="0.00" onblur="calculaValor()" id="desconto"
                                       class="form-control money2" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="valor">Parcela</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input readOnly type="text" name="valor" id="valor" class="form-control money2" value="0.00"
                                       placeholder="Valor">
                            </div>
                        </div>
                    </div>                    
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="idPlanoContas">Plano de contas <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas" required>
                                <option></option>
                                <?php
                                foreach ($Planocontas as $p) {
                                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="custo_id">Centro Custo<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="custo_id" class="form-control selectPicker" id="custo_id" required>
                                <option></option>
                                <?php
                                foreach ($Centro_custo as $t) {
                                    echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="idFornecedor">Credor <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <td width="50">
                                <button type="button" id="btnAddFornecedor" class="btn btn-info btn-xs">Novo</button>
                            </td>
                            <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax"
                                    role="credor" required>
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="tipoDocumento">Tipo de Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento" required>
                                <option></option>
                                <?php
                                foreach ($tipoDocumento as $t) {
                                    echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="numerodocumento">Número do documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" name="numerodocumento" id="numerodocumento" class="form-control" required>
                        </div>
                    </div>


                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="tipoPagamento">Forma de Pagamento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento" required>
                                <option></option>
                                <?php
                                foreach ($tipoPagamento as $t) {
                                    echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">

                        <div class="form-group">
                            <label class="required" for="contabilidade">Empresas <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="contabilidade" name="contabilidade" class="form-control selectPicker" required>
                                <option></option>
                                <?php
                                foreach ($Contabilidade as $c) {
                                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                                <textarea name="complemento" id="complemento"
                                          class="form-control"></textarea>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <?php if(!$this->getParam('modal')): ?>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Contaspagar', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                    <?php endif; ?>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function calculaValor() {
      
        if($('#vencimento').val()){
         //var vencimento = $('#vencimento').val();
    
            let data_hoje = moment().add(5, 'days').format('YYYY-MM-DD');

            /* Data do usuario */
            var dataDigitada = $('#vencimento').val();//.format('YYYY-MM-DD');
            var vencimento = moment(dataDigitada, 'DD/MM/YYYY').format('YYYY-MM-DD');
  
            if(vencimento <= data_hoje) {
                BootstrapDialog.alert('Data de vencimento não pode ser menor que 5 dias apartir da data atual!');
            }

        }
        if ($('#multa').val() != "undefined" && $('#multa').val() != "") {
            var multa = $('#multa').unmask();
        } else {
            var multa = 0;
        }

        if ($('#desconto').val() != "undefined" && $('#desconto').val() != "") {
            var desconto = $('#desconto').unmask();
        } else {
            var desconto = 0;
        }
        if ($('#juros').val() != "undefined" && $('#juros').val() != "") {
            var juros = $('#juros').unmask();
        } else {
            var juros = 0;
        }

        if ($('#valorBruto').val() != "undefined" && $('#valorBruto').val() != "") {
            var valorBruto = $('#valorBruto').unmask();
        } else {
            var valorBruto = 0;
        }

        var valorLiquido = (valorBruto + juros + multa) - desconto;
        valorLiquido = Math.round(valorLiquido*100)/100;

        $('#valor').unpriceFormat();
        $('#valor').val(valorLiquido.toFixed(2));
        $('#valor').priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
        $('#valor').change();
    }

    $(document).ready(function () {
        $('#btnAddFornecedor').click(function(){
            var url = root + '/Fornecedor/add/origem:-1/ajax:true/modal:1/';
            LoadGif();
            $('<div></div>').load(url, function(){
                BootstrapDialog.show({
                    title: 'Adicionar Fornecedor',
                    message: this,
                    size: 'size-wide',
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function(dialog){
                        initeComponentes(dialog.getModalBody());
                        CloseGif();
                    }
                });
            });
        });
    });
</script>