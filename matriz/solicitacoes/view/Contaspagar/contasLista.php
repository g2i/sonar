<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Forma de Pagamento</h2>

        <ol class="breadcrumb">
            <strong>
                <li>Solicitação: <?php echo $id; ?> </li>
            </strong>

        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- botao de cadastro -->

                    <input type="hidden" id="solicitacao_id" value="<?= $id ?>">


                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table id="tblContas" class="table table-hover table-condensed">
                                <thead>
                                <tr>
                                    <th data-column-id="id" data-identifier="true" data-visible="false"
                                        data-visible-in-selection="false">Id
                                    </th>
                                    <th data-column-id="recebido" data-identifier="true" data-visible="false"
                                        data-visible-in-selection="true">Recebido
                                    </th>
                                    <th data-column-id="vencimento" data-converter="date">Vencimento</th>
                                    <th data-column-id="credor">Credores</th>
                                    <th data-column-id="plano">Plano de Contas</th>
                                    <th data-column-id="empresa">Empresa</th>
                                    <th data-column-id="tipo">Tipo. Documento</th>
                                    <th data-column-id="obs">Obs.</th>
                                    <th data-column-id="valor" data-converter="currency">Valor</th>
                                    <th data-column-id="opcoes" data-formatter="opcoes" data-width="113"
                                        data-visible-in-selection="false" data-sortable="false"></th>
                                </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var valorTotal = 0;

    $(document).ready(function () {

        $('[data-role="tooltip"]').tooltip();

        var periodo = moment().date(1);


        var grid = $('#tblContas').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: -1,
            ajax: true,
            url: root + '/Contaspagar/getContas',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            converters: {
                currency: {
                    from: function (value) {
                        return value;
                    },
                    to: function (value) {
                        if (value == null)
                            return null;

                        return 'R$ ' + $.number(value, 2);
                    }
                },
                date: {
                    from: function (value) {
                        if (value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if (value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                    }
                }
            },
            formatters: {
                opcoes: function (column, row) {
                    var aux = 'disabled ';
                    if (!row.dataPagamento)
                        aux = '';

                    var rateio = ' btn-default ';
                    if (row.rateio == 1)
                        rateio = ' btn-primary ';

                    var recebido = ' btn-warning ';
                    if (row.recebido == 1)
                        recebido = ' btn-success ';

                    return '<button type="button" data-toggle="tooltip" style="margin-right: 2px" data-placement="top" title="Pasta de Documentos/Anexos" class="btn btn-xs btn-primary command-galeria" data-row-id="' + row.id + '"><span class="fa fa-paperclip"></span></button>' +
                        '<button type="button" data-toggle="tooltip" style="margin-right: 2px" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                        '<button type="button" data-toggle="tooltip" style="margin-right: 2px" data-placement="top" title="Apagar" class="btn btn-xs btn-danger ' + aux + 'command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                }
            },
            requestHandler: function (request) {
                request.solicitacaoId = <?=$id?>;
                if (request.sort) {
                    var sort = [];
                    $.each(request.sort, function (key, value) {
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                valorTotal = 0;
                return request;
            },
            responseHandler: function (response) {
                valorTotal = response.valorTotal;
                return response;
            },
            templates: {
                search: '<div class="form-group" ><p style="margin-top: 10px">' +
                    '<button id="btnIndividual" class="btn btn-success" style="width: auto; margin-right: 2px" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Individual</button>' +
                    '<button id="btnGerarParcelas" class="btn btn-primary" style="width: auto; margin-right: 2px" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Gerar Parcelas</button>' +
                    '</p></div>',

            }

        });

        grid.on('load.rs.jquery.bootgrid', function () {
            var footer = grid.find('tfoot');
            footer.remove();
        });

        grid.on("loaded.rs.jquery.bootgrid", function () {

            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-galeria").unbind('click');
            grid.find(".command-galeria").on("click", function (e) {
                var conta = $(this).data("row-id");
                var urlLista = root + '/Anexo_financeiro/all/origem:1/id_externo:' + conta;
                window.open(urlLista, '_blank');

            });

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Contaspagar/edit/ajax:true/modal:1/first:1/id:' + conta;
                LoadGif();
                $('<div></div>').load(url, function () {
                    CloseGif();
                    BootstrapDialog.show({
                        title: 'Editar Conta',
                        message: this,
                        size: 'size-wide',
                        type: BootstrapDialog.TYPE_DEFAULT,
                        onshown: function (dialog) {
                            var body = dialog.getModalBody();
                            initeComponentes(body);
                            var form = body.find('#frmConta');
                            var valor = body.find('#valor');
                            console.log(valor);

                            valor.change(function () {
                                $(this).valid();
                            });

                            valor.blur(function () {
                                $(this).valid();
                            });

                            var selects = body.find('.selectPicker');
                            selects.change(function () {
                                $(this).valid();
                            });

                            selects.blur(function () {
                                $(this).valid();
                            });

                            form.validate({
                                rules: {
                                    valor: {greaterThanZero: true},
                                    valorBruto: {greaterThanZero: true}
                                }
                            });
                            dialog.getModal().removeAttr('tabindex');
                        },
                        buttons: [{
                            icon: 'fa fa-ban',
                            label: 'Cancelar',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            icon: 'fa fa-floppy-o',
                            label: 'Salvar',
                            cssClass: 'btn-info',
                            autospin: true,
                            action: function (dialog) {
                                var body = dialog.getModalBody();
                                var form = body.find('#frmConta');
                                if (!form.valid()) {
                                    dialog.updateButtons();
                                    return;
                                }

                                dialog.enableButtons(false);
                                dialog.setClosable(false);
                                var dados = body.find('#frmConta').serialize();
                                var saveUrl = root + '/Contaspagar/post_editModal/';
                                $.post(saveUrl, dados, function (ret) {
                                    if (ret.result) {
                                        BootstrapDialog.success(ret.msg);
                                        grid.bootgrid('reload');
                                        dialog.close();
                                    } else {
                                        BootstrapDialog.warning(ret.msg);
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.updateButtons();
                                    }
                                });
                            }
                        }]
                    });
                });
            });

            grid.find(".command-delete").unbind('click');
            grid.find(".command-delete").on("click", function (e) {
                var conta = $(this).data("row-id");
                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function (result) {
                        if (result) {
                            var url = '<?php echo $this->Html->getUrl('Contaspagar', 'delete') ?>';
                            var data = {
                                id: conta
                            }

                            $.post(url, data, function (ret) {
                                if (ret.result) {
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                } else {
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });


            var footer = $('<tfoot></tfoot>');
            var sumario = $('<tr></tr>');

            sumario.append($('<td colspan="6" style="font-weight: bold">Total:</td>'));
            sumario.append($('<td style="font-weight: bold">R$ ' + $.number(valorTotal, 2) + '</td>'));
            sumario.append($('<td colspan="3"></td>'));
            footer.append(sumario);
            grid.append(footer);

            $('#btnIndividual').unbind('click');
            $('#btnIndividual').click(function () {
                var solicitacao_id = $('#solicitacao_id').val();
                var url = root + '/Contaspagar/add/id:' + solicitacao_id + '/ajax:true/modal:1/';
                LoadGif();
                $('<div></div>').load(url, function () {
                    CloseGif();
                    BootstrapDialog.show({
                        title: 'Solicitações (Forma de Pagamentos)',
                        message: this,
                        size: 'size-wide',
                        type: BootstrapDialog.TYPE_DEFAULT,
                        onshown: function (dialog) {
                            var body = dialog.getModalBody();
                            initeComponentes(body);

                            $(body).find('.dateVencimento').each(function (ev) {
                                $(this).datetimepicker({
                                    useCurrent: false,
                                    showTodayButton: true,
                                    showClear: true,
                                    showClose: true,
                                    allowInputToggle: true,
                                    locale: 'pt-Br',
                                    format: 'L',
                                    extraFormats: ['YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss'],
                                    tooltips: {
                                        today: 'Ir para Hoje',
                                        clear: 'Limpar sele\u00e7\u00e3o',
                                        close: 'Fechar',
                                        selectMonth: 'Selecione o M\u00eas',
                                        prevMonth: 'M\u00eas Anterior',
                                        nextMonth: 'Pr\u00f3ximo M\u00eas',
                                        selectYear: 'Selecione o Ano',
                                        prevYear: 'Ano Anterior',
                                        nextYear: 'Pr\u00f3ximo Ano',
                                        selectDecade: 'Selecione a D\u00e9cada',
                                        prevDecade: 'D\u00e9cada Anterior',
                                        nextDecade: 'Pr\u00f3xima D\u00e9cada',
                                        prevCentury: 'S\u00e9culo Anterior',
                                        nextCentury: 'Pr\u00f3ximo S\u00e9culo'
                                    }
                                }).data("DateTimePicker").minDate(moment().add(6, 'days'));
                            });



                            var form = body.find('#frmConta');
                            var valor = body.find('#valor');

                            valor.change(function () {
                                $(this).valid();
                            });

                            valor.blur(function () {
                                $(this).valid();
                            });

                            var selects = body.find('.selectPicker');
                            selects.change(function () {
                                $(this).valid();
                            });

                            selects.blur(function () {
                                $(this).valid();
                            });

                            form.validate({
                                rules: {
                                    valor: {greaterThanZero: true},
                                    valorBruto: {greaterThanZero: true}
                                }
                            });

                            dialog.getModal().removeAttr('tabindex');
                        },
                        buttons: [{
                            icon: 'fa fa-ban',
                            label: 'Cancelar',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        },
                            {
                                icon: 'fa fa-floppy-o',
                                label: 'Salvar',
                                cssClass: 'btn-info',
                                autospin: true,
                                action: function (dialog) {
                                    var body = dialog.getModalBody();
                                    var form = body.find('#frmConta');
                                    if (!form.valid()) {
                                        dialog.updateButtons();
                                        return;
                                    }

                                    dialog.enableButtons(false);
                                    dialog.setClosable(false);
                                    var dados = form.serialize();
                                    var saveUrl = root + '/Contaspagar/post_addModal/';
                                    $.post(saveUrl, dados, function (ret) {
                                        if (ret.result) {
                                            BootstrapDialog.success(ret.msg, function (diag) {

                                                //if(ret.conta)
                                                //rateioContasPagar(ret.conta, grid);
                                            });
                                            grid.bootgrid('reload');
                                            dialog.close();
                                            $('.modal').modal('hide');
                                        } else {
                                            BootstrapDialog.warning(ret.msg);
                                            dialog.enableButtons(true);
                                            dialog.setClosable(true);
                                            dialog.updateButtons();
                                        }
                                    });
                                }
                            }]
                    });
                });
            });

            $('#btnGerarParcelas').unbind('click');
            $('#btnGerarParcelas').click(function () {
                var solicitacao_id = $('#solicitacao_id').val();
                var url = root + '/Contaspagar/gerarParcelas/id:' + solicitacao_id + '';
                window.open(url,'_blank');
            });

        });


    });

</script>