<style>
    .colorido {
        background: red;
    }
</style>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Acompanhar Solicitações</h2>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="border-color: #28a745;">
                        <span class="label label-success  pull-right">Aceitas</span>
                        <h5>Solicitações</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $solicitacaoAceitaTotal->total; ?>
                        </h1>
                        <div class="stat-percent font-bold "><i class="fa fa-bolt""></i></div>
                        <small>Aceitas</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="border-color: #ffc107;">
                        <span class="label label-warning pull-right">Esperando aceite</span>
                        <h5>Solicitações</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $solicitacaoAguardandoAceiteTotal->total; ?>
                        </h1>
                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>Esperando aceite</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="border-color: #dc3545;">
                        <span class="label label-danger pull-right">Recusadas</span>
                        <h5>Solicitações</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $solicitacaoRecusadaTotal->total; ?>
                        </h1>
                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>Recusadas</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="border-color: #428bca;">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->

                    <div class="form">
                        <form role="form" id="form"
                              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                              method="post" enctype="application/x-www-form-urlencoded">

                            <div class="col-xs-6 col-md-4">
                                <label for="inicio">Inicio</label>
                                <div class='input-group dateInicio'>
                                    <input type='text' class="form-control dateFormat" name="inicio" id="inicio"
                                           value="<?php echo $inicio; ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-xs-6 col-md-4">
                                <label for="fim">Fim</label>
                                <div class='input-group dateFim'>
                                    <input type='text' class="form-control dateFormat" name="fim" id="fim"
                                           value="<?php echo $fim; ?>"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="idFornecedor">Fornecedor</label>
                                <select name="idFornecedor" class="form-control select-fornecedor"
                                        id="idFornecedor">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Fornecedores as $f): ?>
                                        <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="projeto">Projeto</label>
                                <input type="text" name="projeto" id="projeto" class="form-control"
                                       value="<?php echo $this->getParam('projeto'); ?>">
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="situacao_solicitacao_id">Situação</label>
                                <select name="situacao_solicitacao_id" class="form-control"
                                        id="situacao_solicitacao_id">
                                    <option value="">Selecione: </option>
                                    <?php foreach ($Situacao_solicitacao as $f): ?>
                                        <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="idFornecedor">Responsavel por Autorizar</label>
                                <select name="responsavel" class="form-control select-responsavel"
                                        id="responsavel">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Usuarios as $f): ?>
                                        <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-12 text-right">
                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                   title="Recarregar a página"><span
                                            class="glyphicon glyphicon-refresh "></span></a>

                                <button type="button" class="btn btn-default" id="buscar-filtro"
                                        data-tool="tooltip" data-placement="bottom" title="Pesquisar"><span
                                            class="glyphicon glyphicon-search"></span></button>

                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="border-color: #428bca;">
                    <h5>Solicitações</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">

                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Solicitante</th>
                                <th>Data Documento</th>
                                <th>Fornecedor</th>
                                <th>Empresa</th>
                                <th>Aplicação/Projeto</th>
                                <th>Observações</th>
                                <th>Valor</th>
                                <th>Resp. Autorizar</th>
                                <th>Situação</th>
                                <th>opções</th>
                            </tr>
                            </thead>
                            <tbody class="solicitacaoT">
                            <?php // bg-success .bg-warning
                            foreach ($Solicitacaos as $d) {
                                //$class = ($d->situacao_solicitacao_id == 2) ? 'bg-danger' : '';
                                if ($d->situacao_solicitacao_id == 1) {
                                    $class = "bg-warning";
                                } else if ($d->situacao_solicitacao_id == 2) {
                                    $class = "bg-danger";
                                } else if ($d->situacao_solicitacao_id == 3) {
                                    $class = "bg-success";
                                } else if ($d->situacao_solicitacao_id == 4) {
                                    $class = "";
                                }
                                echo '<tr class="' . $class . '">';
                                echo '<td>';
                                echo $d->id;
                                echo '</td>';
                                echo '<td>';
                                echo $d->solicitante;
                                echo '</td>';
                                echo '<td>';
                                echo convertDataSQL4BR($d->data);
                                echo '</td>';
                                echo '<td>';
                                echo $d->getFornecedor()->nome;
                                echo '</td>';
                                echo '<td>';
                                echo $d->getContabilidade()->nome;
                                echo '</td>';
                                echo '<td>';
                                echo $d->projeto;
                                echo '</td>';
                                echo '<td>';
                                echo $d->observacao;
                                echo '</td>';
                                echo '<td>';
                                echo 'R$ ';
                                echo number_format($d->valor_solicitacao, 2, ',', '.');
                                echo '</td>';
                                echo '<td>';
                                echo $d->getResponsavel()->nome;
                                echo '</td>';
                                echo '<td id="status">';
                                echo $d->getSituacao_solicitacao()->nome;
                                echo '</td>';

                                echo '<td width="30" style="padding-top: 5px; padding-bottom: 0px;">';

                                echo $this->Html->getLink('<span class="fa fa-folder-open-o"></span>', 'Solicitacao', 'view_solicitacao',
                                    array('id' => $d->id),
                                    array('class' => 'btn btn-xs btn-info'));

                                echo '<button type="button" data-toggle="tooltip" data-placement="top" title="Pasta de Documentos/Anexos" class="btn btn-xs btn-primary command-galeria" data-row-id="' . $d->id . '"><span class="fa fa-paperclip"></span></button>';

                                echo '</td>';


                                echo '</tr>';
                            }
                            ?>
                            </tbody>
                        </table>

                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php
                            echo $nav;
                            ?></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    //function para paginar por ajax
    $(document).on('click', '.pagination a, .table thead a', function (e) {
        e.preventDefault();
        let url = $(this).attr('href');

        if (url != "") {
            carregaTabelaResponsiva(url);
        }
        return false;
    });

    //function para pegar as URLs
    function carregaTabelaResponsiva(url) {
        $.ajax({
            type: 'GET',
            url: url,
            beforeSend: () => {
            },
            success: (data) => {
                let conteudo = $('<div>').append(data).find('.table-responsive');
                $(".table-responsive").html(conteudo);
            },
            complete: () => {
            },
            error: () => {
            },
        });
    }
    //function para o btn de pesquisa em form
    $(function () {

        $('.command-galeria').on('click', function () {
            var conta = $(this).attr('data-row-id');
            var origem = 4;//solicitacao
            var url = root + '/Anexo_financeiro/galeria/modal:1/ajax:true/origem:' + origem + '/id_externo:' + conta+'/hide:1';

            $('<div></div>').load(url, function () {
                BootstrapDialog.show({
                    title: 'Visualizar Documentos Anexados',
                    message: this,
                    size: 'size-wide',
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function (dialog) {
                        var body = dialog.getModalBody();
                        initAnexos(body);
                        dialog.getModal().removeAttr('tabindex');
                    },
                    buttons: [{
                        label: 'Fechar',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            });
        });

        $('.select-fornecedor').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione:'
        });

        $('.select-responsavel').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione:'
        });

        $('#buscar-filtro').click(function(){
            let form_action = $('#form').attr('action');
            let form_serialize =  $('#form').serialize();
            let url_completa = form_action + '?' + form_serialize;
            console.log(url_completa);
            carregaTabelaResponsiva(url_completa);
        });
    });
</script>