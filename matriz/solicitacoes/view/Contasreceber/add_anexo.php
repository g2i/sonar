<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexo</h2>
        <ol class="breadcrumb">
            <li>Anexo</li>
            <li class="active">
                <strong>Cadastrar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div id="msg_error"></div>

                <p>Extensões permitidas: <i>zip,doc,docx,pdf,rar,png,jpg,jpeg,gif,odt,txt,xls,xlsx;</i></p>

                <br/>

                <div class="clearfix">
                    <form enctype="multipart/form-data" method="post" role="form" id="formAnexo"
                          action="<?php echo $_POST['redirecionar']; ?>">
                        <input type="hidden" name="upload_anexo" id="upload_anexo" value="1"/>
                        <table>
                            <tr class="tr_anexo">
                                <td width="35"></td>
                                <td width="10"> &nbsp; </td>
                                <td><input type="text" class="form-control" name="add_anexo[0][titulo]"
                                           placeholder="Titulo do anexo"/></td>
                                <td width="10"> &nbsp; </td>
                                <td>
                                    <input type="file" name="add_anexo[0][arquivo]"/>
                                    <input type="hidden" name="add_anexo[0][tipo]"
                                           value="<?php echo $_POST['tipo']; ?>"/>
                                    <input type="hidden" name="add_anexo[0][atividade_id]"
                                           value="<?php echo $_POST['atividade_id']; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5"> &nbsp; </td>
                            </tr>

                            <tbody id="content_arquivos"></tbody>
                        </table>

                        <div style="clear: both"></div>
                        <br/>

                        <div class="text-right">

                            <a href="#" id="adicionar_anexo" class="pull-left btn btn-primary">adicionar</a>
                            <a href="#" class="pull-right enviar btn btn-primary">salvar</a>

                            <span class="pull-right" style="width: 10px;height: 1px;pull-right"></span>

                            <a href="#" class="pull-right cancelar btn btn-default">voltar</a>
                        </div>
                        <br/><br/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('.enviar').click(function () {
            $('form#formAnexo').submit();
        });

        $('form#formAnexo').ajaxForm({
            success: function (d) {
                if (d != "true") {
                    $('#msg_error').html(d);
                } else {
                    $('div.modal').find('.modal-content').load('<?php echo $_POST['redirecionar']; ?>');
                }
            }
        });

        $('#adicionar_anexo').click(function () {
            var i_next = parseInt($("tr.tr_anexo").length - 1) + 1;

            var string = ''
                + '<tr class="tr_anexo row_anexo_' + i_next + '">'
                + ' <td><span data-rm="row_anexo_' + i_next + '" class="btn btn-danger btn_remover_anexo">X</span></td>'
                + ' <td width="10"> &nbsp; </td>'
                + ' <td><input type="text" class="form-control" name="add_anexo[' + i_next + '][titulo]" placeholder="Titulo do anexo" /></td>'
                + ' <td width="10"> &nbsp; </td>'
                + ' <td>'
                + '     <input type="file" name="add_anexo[' + i_next + '][arquivo]" />'
                + '     <input type="hidden" name="add_anexo[' + i_next + '][tipo]" value="<?php echo $_POST['tipo']; ?>" />'
                + '     <input type="hidden" name="add_anexo[' + i_next + '][atividade_id]" value="<?php echo $_POST['atividade_id']; ?>" />'
                + ' </td>'
                + ' </tr> <tr class="row_anexo_' + i_next + '"><td colspan="5"> &nbsp; </td></tr>';

            $('#content_arquivos').append(string);

            $('#content_arquivos').find('.btn_remover_anexo').bind('click', function () {
                $('.' + $(this).data('rm')).remove();
            })


            return false;
        });

        $('.cancelar').click(function () {
            $('div.modal').find('.modal-content').load('<?php echo $_POST['redirecionar']; ?>');
        })

        $('div#msg_error').click(function () {
            $(this).html('');
        });
    });
</script>