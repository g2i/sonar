<style>
    .col-opcoes{
        width: 113px; !important;
    }
    .bootgrid-table > tr > td > button {
        margin-left: 2px;
        margin-top: 2px;
    }
</style>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas a Receber</h2>
        <ol class="breadcrumb">
            <li>Receber</li>
            <li class="active">
                <strong>Gerenciamento</strong>
            </li>
        </ol>
    </div>

    <div class="col-sm-12" style="margin-top: 50px">
        <!-- formulario de pesquisa -->
        <form id="pesquisa" role="form">

            <div class="col-xs-6 col-md-3">
                <label for="inicio">Inicio</label>
                <div class='input-group dateInicio'>
                    <input type='text' class="form-control dateFormat" name="inicio" id="inicio"
                           value="<?php echo $inicio; ?>"/>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <label for="fim">Fim</label>
                <div class='input-group dateFim'>
                    <input type='text' class="form-control dateFormat" name="fim" id="fim"
                           value="<?php echo $fim; ?>"/>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="form-group">
                    <label for="cliente">Cliente</label>
                    <select id="cliente" name="cliente" class="form-control clientes-ajax"></select>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="form-group">
                    <label for="idPlanoContas">Plano de Contas</label>
                    <select id="idPlanoContas" name="idPlanoContas" class="form-control selectPicker">
                        <option></option>
                        <?php foreach ($idPlanoContas as $p): ?>
                            <option value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-md-3">
                <div class="form-group">
                    <label for="contabilidade">Empresas</label>
                    <select name="contabilidade" id="contabilidade" class="form-control selectPicker">
                        <option></option>
                        <?php foreach ($contabilidade as $c): ?>
                            <option value="<?php echo $c->id ?>"><?php echo $c->nome ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="form-group">
                    <label for="situacao">Pagamento: </label>
                    <select name="situacao" id="situacao" class="form-control selectPicker">
                        <option></option>
                        <option value="1">Pagas</option>
                        <option selected value="2">Pendentes</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-md-3">
                <div class="form-group">
                    <label for="status">Status: </label>
                    <select id="status" name="status" class="form-control selectPicker">
                        <option></option>
                        <?php foreach ($StatusList as $sl): ?>
                            <option value="<?php echo $sl->id ?>"><?php echo $sl->descricao ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-md-3">
                <div class="form-group">
                    <button id="btnPesquisar" style="margin-top: 20px;" type="button" data-role="tooltip"
                            data-placement="bottom" title="Pesquisar" class="btn btn-default">
                        <i class="fa fa-search"></i></button>
                    <button id="btnReset" type="button" style='margin-top:20px;' data-role="tooltip" data-placement="bottom"
                            title="Limpar Campos" class="btn btn-default">
                        <i class="fa fa-refresh"></i></button>
                    <button id="btnRelVec" style='margin-top:20px;' title="Relatório por vencimento" type="button"
                            data-role="tooltip" data-placement="bottom" class="btn btn-default">
                        <i class="fa fa-print"></i></button>
                    <button id="btnRel" style='margin-top:20px;' title="Relatório geral" type="button" data-role="tooltip"
                            data-placement="bottom" class="btn btn-default">
                        <i class="fa fa-print"></i></button>
                </div>
            </div>

            <div style="clear:both"></div>
        </form>
    </div>

</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row clearfix">
                    <div class="table-responsive">
                        <table id="tblContas" class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <th data-column-id="id" data-identifier="true" data-visible="false"
                                    data-visible-in-selection="false">Id</th>
                                <th data-column-id="data" data-converter="date">Data</th>
                                <th data-column-id="vencimento" data-converter="date">Vencimento</th>
                                <th data-column-id="plano">Plano de Contas</th>
                                <th data-column-id="cliente">Cliente</th>
                                <th data-column-id="documento">Num. Documento</th>
                                <th data-column-id="valor" data-converter="currency">Valor</th>
                                <th data-column-id="deducao" data-converter="currency">Valor c/ Dedução</th>
                                <th data-column-id="dataPagamento" data-converter="date">Data Pagamento</th>
                                <th data-column-id="status">Status.</th>
                                <th data-column-id="opcoes" data-formatter="opcoes"
                                    data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                    data-sortable="false"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function rateioContasReceber(conta, grid){
        var urlRateios = root + '/Rateio_contasreceber/getRateios/id:' + conta;
        var url = root + '/Rateio_contasreceber/all/contas_receber:' + conta +'/ajax:true';
        LoadGif();
        $.post(urlRateios, function(ret){
            var rGrid = null;
            var rateiosExcluidos = [];
            var rateios = ret.rateios;
            for(var i = 0; i < rateios.length; i++){
                rateios[i].id = parseFloat(rateios[i].id);
                rateios[i].valor = parseFloat(rateios[i].valor);
                rateios[i].empresaId = parseFloat(rateios[i].empresaId);
                rateios[i].centroId = parseFloat(rateios[i].centroId);
            }

            $('<span></span>').load(url, function () {
                CloseGif();
                BootstrapDialog.show({
                    size: 'size-wide',
                    title: 'Rateio Contas a Receber',
                    message: this,
                    type: BootstrapDialog.TYPE_DEFAULT,
                    spinicon: 'fa fa-spinner fa-pulse',
                    onshown: function (dialog) {
                        var rateioModal = dialog.getModalBody();
                        rGrid = rateioModal.find('#tblDados');

                        rGrid.bootgrid({
                            rowSelect: true,
                            multiSort: false,
                            navigation: 0,
                            rowCount: -1,
                            converters: {
                                currency: {
                                    from: function (value) {
                                        return value;
                                    },
                                    to: function (value) {
                                        if (value == null)
                                            return null;

                                        return 'R$ ' + $.number(value, 2);
                                    }
                                },
                                date: {
                                    from: function (value) {
                                        if (value == null)
                                            return null;

                                        return moment(value);
                                    },
                                    to: function (value) {
                                        if (value == null)
                                            return null;

                                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                                    }
                                }
                            },
                            formatters: {
                                opcoes: function (column, row) {
                                    return '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                                        '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                                }
                            }
                        });

                        rGrid.on("loaded.rs.jquery.bootgrid", function () {

                            rGrid.find('[data-toggle="tooltip"]').tooltip();

                            rGrid.find(".command-edit").unbind('click');
                            rGrid.find(".command-edit").on("click", function (e) {
                                var conta = $(this).data("row-id");
                                var dados = rGrid.bootgrid('getCurrentRows');
                                var total = rateioModal.find('#total');
                                var totalRateio = total.unmask();
                                var valorConta = rateioModal.find('#valor').unmask();
                                var rows = $.grep(dados, function (dado) {
                                    return dado.id == conta;
                                });

                                var rateio = rows[0];
                                totalRateio -= rateio.valor;
                                var sobra = valorConta - totalRateio;
                                var url = root + '/Rateio_contasreceber/add/id:' + rateio.conta + '/modal:1/ajax:true';
                                $('<span></span>').load(url, function () {
                                    BootstrapDialog.show({
                                        title: 'Editar Rateio',
                                        message: this,
                                        type: BootstrapDialog.TYPE_DEFAULT,
                                        onshown: function (dialog) {
                                            var body = dialog.getModalBody();
                                            var valor = body.find('#valor');
                                            valor.val($.number(rateio.valor, 2));

                                            var empresa = body.find('#empresaId');
                                            empresa.val(rateio.empresaId);
                                            empresa.trigger('change');

                                            var centro = body.find('#centroId');
                                            centro.val(rateio.centroId);

                                            //ID DO RATEIO
                                            body.find('#id').val(rateio.id);

                                            var observacao = body.find('#observacao');
                                            observacao.text(rateio.observacao);

                                            //DATA DO DOCUMENTO
                                            var data_documento = body.find('#data_documento');
                                            data_documento.val(rateio.data_documento);

                                            initeComponentes(dialog.getModalBody());

                                            var empresa = body.find('#empresaId');
                                            empresa.val(rateio.empresaId).trigger('change');

                                            var plano_contas = body.find('#plano_contas_id');
                                            plano_contas.val(rateio.plano_contas_id).trigger('change');

                                            var cliente = body.find('#cliente_id');
                                            cliente.val(rateio.cliente_id).trigger('change');

                                            var centro = body.find('#centroId');
                                            centro.val(rateio.centroId).trigger('change');
                                        },
                                        buttons: [{
                                            icon: 'fa fa-ban',
                                            label: 'Cancelar',
                                            cssClass: 'btn-danger',
                                            action: function (dialog) {
                                                dialog.close();
                                            }
                                        }, {
                                            icon: 'fa fa-floppy-o',
                                            label: 'Salvar',
                                            cssClass: 'btn-info',
                                            action: function (dialog) {
                                                var body = dialog.getModalBody();
                                                var valor = body.find('#valor').unmask();
                                                if (valor > sobra || valor < 0) {
                                                    BootstrapDialog.warning("Valor incorreto para rateio !");
                                                    return;
                                                }

                                                var form = body.find('form').serializeArray();
                                                $.each(form, function () {
                                                    rateio[this.name] = this.value || '';
                                                });

                                                if (rateio.empresaId < 1) {
                                                    BootstrapDialog.warning("Selecione uma empresa !");
                                                    return;
                                                }

                                                if (rateio.centroId < 1) {
                                                    BootstrapDialog.warning("Selecione um Centro de Custo !");
                                                    return;
                                                }

                                                var rows = [];
                                                var dados = rGrid.bootgrid('getCurrentRows');
                                                rows = $.grep(dados, function (dado) {
                                                    return dado.empresaId == rateio.empresaId &&
                                                        dado.centroId == rateio.centroId &&
                                                        dado.id != rateio.id;
                                                });

                                                if (rows.length > 0) {
                                                    BootstrapDialog.warning("Centro de Custo e empresa já informados !");
                                                    return;
                                                }

                                                if(rateio.situacao != 'N')
                                                    rateio.situacao = 'E';

                                                rateio.valor = parseFloat(rateio.valor.replace('.', '').replace(',', '.'));
                                                totalRateio += rateio.valor;
                                                total.unpriceFormat();
                                                total.text($.number(totalRateio, 2));
                                                total.priceFormat({
                                                    prefix: 'R$ ',
                                                    centsSeparator: ',',
                                                    thousandsSeparator: '.',
                                                    allowNegative: true
                                                });

                                                var empresa = body.find('#empresaId');
                                                var empresaObj = empresa.select2('data')[0];
                                                rateio.empresa = empresaObj.text;

                                                var centro = body.find('#centroId');
                                                var centroObj = centro.select2('data')[0];
                                                rateio.centro = centroObj.text;

                                                rGrid.bootgrid('reload');
                                                dialog.close();
                                            }
                                        }]
                                    });
                                });
                            });

                            rGrid.find(".command-delete").unbind('click');
                            rGrid.find(".command-delete").on("click", function (e) {
                                var conta = $(this).data("row-id");
                                BootstrapDialog.confirm({
                                    title: 'Aviso',
                                    message: 'Voc\u00ea tem certeza ?',
                                    type: BootstrapDialog.TYPE_WARNING,
                                    closable: false,
                                    draggable: false,
                                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                                    btnOKLabel: 'Sim desejo excluir!',
                                    btnOKClass: 'btn-warning',
                                    callback: function (result) {
                                        if (result) {
                                            var dados = rGrid.bootgrid('getCurrentRows');
                                            var rows = $.grep(dados, function (dado) {
                                                return dado.id == conta;
                                            });

                                            var rateio = rows[0];
                                            if (rateio.situacao != 'N') {
                                                rateio.situacao = 'D';
                                                rateiosExcluidos.push(rateio);
                                            }

                                            rGrid.bootgrid('remove', [conta]);
                                        }
                                    }
                                });
                            });
                        });

                        rGrid.on("appended.rs.jquery.bootgrid", function (e, rows){
                            var total = rateioModal.find('#total');
                            var totalRateio = total.unmask();
                            for(var i = 0; i < rows.length; i++){
                                totalRateio += rows[i].valor;
                            }

                            total.unpriceFormat();
                            total.text($.number(totalRateio, 2));
                            total.priceFormat({
                                prefix: 'R$ ',
                                centsSeparator: ',',
                                thousandsSeparator: '.',
                                allowNegative: true
                            });
                        });

                        rGrid.on("removed.rs.jquery.bootgrid", function (e, rows){
                            var total = rateioModal.find('#total');
                            var totalRateio = total.unmask();
                            for(var i = 0; i < rows.length; i++){
                                totalRateio -= rows[i].valor;
                            }

                            total.unpriceFormat();
                            total.text($.number(totalRateio, 2));
                            total.priceFormat({
                                prefix: 'R$ ',
                                centsSeparator: ',',
                                thousandsSeparator: '.',
                                allowNegative: true
                            });
                        });

                        rateioModal.find('#valor').priceFormat({
                            prefix: 'R$ ',
                            centsSeparator: ',',
                            thousandsSeparator: '.',
                            allowNegative: true
                        });

                        rGrid.bootgrid('append', rateios);
                    },
                    onhide: function(dialog){
                        if(dialog.getData('close'))
                            return true;

                        var body = dialog.getModalBody();
                        var valorConta = body.find('#valor').unmask();
                        var rateio = body.find('#total').unmask();
                        var rateios = rateiosExcluidos.concat(rGrid.bootgrid('getCurrentRows'));
                        var rows = $.grep(rateios, function (rateio) {
                            return rateio.situacao != 'S';
                        });

                        if (rows.length > 0) {
                            BootstrapDialog.confirm('Você tem modificações não salva, sair sem salvar ?', function (result) {
                                if (result) {
                                    dialog.setData('close', result);
                                    dialog.close();
                                }
                            });

                            return false;
                        }
                    },
                    buttons: [{
                        icon: 'glyphicon glyphicon-plus',
                        label: 'Cadastrar Rateio',
                        cssClass: 'btn-success',
                        action: function (dialog) {
                            var body = dialog.getModalBody();
                            var total = body.find('#total');
                            var totalRateio = total.unmask();
                            var valorConta = body.find('#valor').unmask();

                            var sobra = valorConta - totalRateio;
                            if (sobra <= 0) {
                                BootstrapDialog.alert("Para este lançamento já foi feito o rateio!");
                                return;
                            }
                            var url = root + '/Rateio_contasreceber/add/id:' + conta + '/modal:1/ajax:true';
                            $('<span></span>').load(url, function () {
                                BootstrapDialog.show({
                                    title: 'Adicionar Rateio',
                                    message: this,
                                    type: BootstrapDialog.TYPE_DEFAULT,
                                    onshow: function (dialog) {
                                        var body = dialog.getModalBody();
                                        var valor = body.find('#valor');
                                        valor.val($.number(sobra, 2));
                                    },
                                    onshown: function (dialog) {
                                        initeComponentes(dialog.getModalBody());
                                    },
                                    buttons: [{
                                        icon: 'fa fa-ban',
                                        label: 'Cancelar',
                                        cssClass: 'btn-danger',
                                        action: function (dialog) {
                                            dialog.close();
                                        }
                                    }, {
                                        icon: 'fa fa-floppy-o',
                                        label: 'Salvar',
                                        cssClass: 'btn-info',
                                        action: function (dialog) {
                                            var body = dialog.getModalBody();
                                            var valor = body.find('#valor').unmask();
                                            if (valor > sobra || valor < 0) {
                                                BootstrapDialog.warning("Valor incorreto para rateio !");
                                                return;
                                            }

                                            var form = body.find('form').serializeArray();
                                            var rateio = {};
                                            $.each(form, function () {
                                                if (rateio[this.name] !== undefined) {
                                                    if (!rateio[this.name].push) {
                                                        rateio[this.name] = [rateio[this.name]];
                                                    }
                                                    rateio[this.name].push(this.value || '');
                                                } else {
                                                    rateio[this.name] = this.value || '';
                                                }
                                            });

                                            if (rateio.empresaId < 1) {
                                                BootstrapDialog.warning("Selecione uma empresa !");
                                                return;
                                            }

                                            if (rateio.centroId < 1) {
                                                BootstrapDialog.warning("Selecione um Centro de Custo !");
                                                return;
                                            }

                                            var rows = [];
                                            var dados = rGrid.bootgrid('getCurrentRows');
                                            rows = $.grep(dados, function (dado) {
                                                return dado.empresaId == rateio.empresaId &&
                                                    dado.centroId == rateio.centroId;
                                            });

                                            if (rows.length > 0) {
                                                BootstrapDialog.warning("Centro de Custo e empresa já informados !");
                                                return;
                                            }

                                            rows = [];
                                            var newId = 0;

                                            do
                                            {
                                                newId = Math.round(new Date().getTime() + (Math.random() * 100));
                                                rows = $.grep(dados, function (dado) {
                                                    return dado.id == newId;
                                                });
                                            } while (rows.length > 0);

                                            rateio.id = newId;
                                            rateio.situacao = 'N';
                                            rateio.valor = parseFloat(rateio.valor.replace('.', '').replace(',', '.'));

                                            var empresa = body.find('#empresaId');
                                            var empresaObj = empresa.select2('data')[0];
                                            rateio.empresa = empresaObj.text;

                                            var centro = body.find('#centroId');
                                            var centroObj = centro.select2('data')[0];
                                            rateio.centro = centroObj.text;

                                            rGrid.bootgrid('append', [rateio]);
                                            dialog.close();
                                        }
                                    }]
                                });
                            });
                        }
                    }, {
                        icon: 'fa fa-ban',
                        label: 'Cancelar',
                        cssClass: 'btn-danger',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }, {
                        icon: 'fa fa-floppy-o',
                        label: 'Salvar',
                        cssClass: 'btn-info',
                        autospin: true,
                        action: function (dialog) {
                            var body = dialog.getModalBody();
                            var valorConta = body.find('#valor').unmask();
                            var rateio = body.find('#total').unmask();
                            if (rateio != valorConta && rateio != 0) {
                                BootstrapDialog.warning("Rateio incompleto!");
                                dialog.updateButtons();
                                return;
                            }

                            var rateios = rateiosExcluidos.concat(rGrid.bootgrid('getCurrentRows'));
                            var saveUrl = root + '/Rateio_contasreceber/saveRateios';
                            var data = {
                                rateios: $.toJSON(rateios)
                            }

                            $.post(saveUrl, data, function(ret){
                                if(ret.result){
                                    var rows = rGrid.bootgrid('getCurrentRows');
                                    for(var i = 0; i < rows.length; i++){
                                        rows[i].situacao = 'S';
                                    }

                                    rateiosExcluidos = [];
                                    BootstrapDialog.success(ret.msg, function(){
                                        if(grid)
                                            grid.bootgrid('reload');

                                        dialog.close();
                                    });
                                }else{
                                    BootstrapDialog.warning(ret.msg);
                                    dialog.updateButtons();
                                }
                            });
                        }
                    }]
                });
            });
        });
    }

    var valorTotal = 0;

    $(document).ready(function () {
        $('[data-role="tooltip"]').tooltip();

        var periodo = moment().date(1);
        $('#inicio').val(periodo.format('DD/MM/YYYY'));
        $('#fim').val(periodo.endOf('month').format('DD/MM/YYYY'));

        var grid = $('#tblContas').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: -1,
            ajax: true,
            url: root + '/Contasreceber/getContas',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            converters:{
                currency: {
                    from: function (value) { return value; },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return 'R$ ' + $.number(value, 2);
                    }
                },
                date: {
                    from: function (value) {
                        if(value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                    }
                }
            },
            formatters:{
                opcoes: function(column, row){
                    var aux = 'disabled ';
                    if(!row.dataPagamento)
                        aux = '';

                    var rateio = ' btn-default ';
                    if(row.rateio == 1)
                        rateio = ' btn-primary ';

                    var anexo = ' btn-default ';
                    if(row.anexo == 1)
                        anexo = ' btn-primary ';

                    return '<button type="button" data-toggle="tooltip" data-placement="top" title="Rateio" class="btn btn-xs' + rateio + 'command-rateio" data-row-id="' + row.id + '"><span class="fa fa-share-alt-square"></span></button>' +
                           '<button type="button" data-toggle="tooltip" data-placement="top" title="Anexos" class="btn btn-xs' + anexo + 'command-anexos" data-row-id="' + row.id + '"><span class="fa fa-paperclip"></span></button>' +
                           '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                           '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger '+ aux +'command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                }
            },
            requestHandler: function (request) {
                request.inicio = $('#inicio').val();
                request.fim = $('#fim').val();
                request.cliente = $('#cliente').val();
                request.idPlanoContas = $('#idPlanoContas').val();
                request.contabilidade = $('#contabilidade').val();
                request.situacao = $('#situacao').val();
                request.status = $('#status').val();

                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                valorTotal = 0;
                return request;
            },
            responseHandler: function (response){
                valorTotal = response.valorTotal;
                return response;
            },
            templates: {
                search: '<div class="{{css.search}}" style="width: 380px"><p style="margin-top: 10px">Novo lançamento: ' +
                        '<button id="btnIndividual" class="btn btn-success" style="width: auto; margin-right: 2px" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Individual</button>' +
                        '<button id="btnProg" class="btn btn-primary" style="width: auto" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Programa&ccedil;&atilde;o</button>' +
                        '</p></div>',
            }
        });

        grid.on('load.rs.jquery.bootgrid', function(){
            var footer = grid.find('tfoot');
            footer.remove();
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {

            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-rateio").unbind('click');
            grid.find(".command-rateio").on("click", function (e) {
                var conta = $(this).data("row-id");
                rateioContasReceber(conta, grid);
            });

            grid.find(".command-anexos").unbind('click');
            grid.find(".command-anexos").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Anexo/all/modal:1/ajax:true/first:1/tipo:2/id_externo=' + conta;
                showOnModal(url, 'Anexos');
            });

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Contasreceber/edit/ajax:true/modal:1/first:1/id:' + conta;
                LoadGif();
                $('<div></div>').load(url, function(){
                    CloseGif();
                    BootstrapDialog.show({
                        title: 'Editar Conta',
                        message: this,
                        size: 'size-wide',
                        type: BootstrapDialog.TYPE_DEFAULT,
                        onshown: function(dialog){
                            var body = dialog.getModalBody();
                            initeComponentes(body);
                            var form = body.find('#frmConta');
                            var valor = body.find('#valor');
                            valor.change(function(){
                                $(this).valid();
                            });

                            valor.blur(function(){
                                $(this).valid();
                            });

                            var selects = body.find('.selectPicker');
                            selects.change(function(){
                                $(this).valid();
                            });

                            selects.blur(function(){
                                $(this).valid();
                            });

                            form.validate({
                                rules:{
                                    valor: { greaterThanZero : true },
                                    valorBruto: { greaterThanZero : true }
                                }
                            });
                            dialog.getModal().removeAttr('tabindex');
                        },
                        buttons:  [{
                            icon: 'fa fa-ban',
                            label: 'Cancelar',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        },{
                            icon: 'fa fa-floppy-o',
                            label: 'Salvar',
                            cssClass: 'btn-info',
                            autospin: true,
                            action: function (dialog) {
                                var body = dialog.getModalBody();
                                var form = body.find('#frmConta');
                                if(!form.valid()) {
                                    dialog.updateButtons();
                                    return;
                                }

                                dialog.enableButtons(false);
                                dialog.setClosable(false);
                                var dados = body.find('#frmConta').serialize();
                                var saveUrl = root + '/Contasreceber/post_editModal/';
                                $.post(saveUrl, dados, function(ret){
                                    if(ret.result){
                                        BootstrapDialog.success(ret.msg);
                                        grid.bootgrid('reload');
                                        dialog.close();
                                    }else{
                                        BootstrapDialog.warning(ret.msg);
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.updateButtons();
                                    }
                                });
                            }
                        }]
                    });
                });
            });

            grid.find(".command-delete").unbind('click');
            grid.find(".command-delete").on("click", function(e) {
                var conta = $(this).data("row-id");
                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function(result) {
                        if(result) {
                            var url = '<?php echo $this->Html->getUrl('Contasreceber', 'delete') ?>';
                            var data = {
                                id: conta
                            }

                            $.post(url, data, function(ret){
                                if(ret.result){
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                }else{
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });

            $('#btnIndividual').unbind('click');
            $('#btnIndividual').click(function(){
                var url = root + '/Contasreceber/add/ajax:true/modal:1/';
                LoadGif();
                $('<div></div>').load(url, function(){
                    CloseGif();
                    BootstrapDialog.show({
                        title: 'Adicionar Conta',
                        message: this,
                        size: 'size-wide',
                        type: BootstrapDialog.TYPE_DEFAULT,
                        onshown: function(dialog){
                            var body = dialog.getModalBody();
                            initeComponentes(body);
                            var form = body.find('#frmConta');
                            var valor = body.find('#valor');
                            valor.change(function(){
                                $(this).valid();
                            });

                            valor.blur(function(){
                                $(this).valid();
                            });

                            var selects = body.find('.selectPicker');
                            selects.change(function(){
                                $(this).valid();
                            });

                            selects.blur(function(){
                                $(this).valid();
                            });

                            form.validate({
                                rules:{
                                    valor: { greaterThanZero : true },
                                    valorBruto: { greaterThanZero : true }
                                }
                            });
                            dialog.getModal().removeAttr('tabindex');
                        },
                        buttons: [{
                            icon: 'fa fa-ban',
                            label: 'Cancelar',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        },{
                            icon: 'fa fa-floppy-o',
                            label: 'Salvar',
                            cssClass: 'btn-info',
                            autospin: true,
                            action: function (dialog) {
                                var body = dialog.getModalBody();
                                var form = body.find('#frmConta');
                                if(!form.valid()) {
                                    dialog.updateButtons();
                                    return;
                                }

                                dialog.enableButtons(false);
                                dialog.setClosable(false);
                                var dados = form.serialize();
                                var saveUrl = root + '/Contasreceber/post_addModal/';
                                $.post(saveUrl, dados, function(ret){
                                    if(ret.result){
                                        BootstrapDialog.success(ret.msg, function(diag){
                                            if(ret.conta)
                                                rateioContasReceber(ret.conta, grid);
                                        });
                                        grid.bootgrid('reload');
                                        dialog.close();
                                    }else{
                                        BootstrapDialog.warning(ret.msg);
                                        dialog.enableButtons(true);
                                        dialog.setClosable(true);
                                        dialog.updateButtons();
                                    }
                                });
                            }
                        }]
                    });
                });
            });

            $('#btnProg').unbind('click');
            $('#btnProg').click(function(){
                var url = root + '/Programacao/addContasReceber';
                window.open(url);
            });

            var footer = $('<tfoot></tfoot>');
            var sumario = $('<tr></tr>');

            sumario.append($('<td colspan="6" style="font-weight: bold">Total:</td>'));
            sumario.append($('<td style="font-weight: bold">R$ ' + $.number(valorTotal, 2) + '</td>'));
            sumario.append($('<td colspan="3"></td>'));
            footer.append(sumario);
            grid.append(footer);
        });

        $('#btnPesquisar').click(function(){
            grid.bootgrid('reload');
        });

        $('#btnReset').click(function(){
            var data = moment().date(1);
            $('#inicio').val(data.format('DD/MM/YYYY'));
            $('#fim').val(data.endOf('month').format('DD/MM/YYYY'));
            $('#cliente').val("").trigger('change');
            $('#idPlanoContas').val("").trigger('change');
            $('#contabilidade').val("").trigger('change');
            $('#situacao').val("").trigger('change');
            $('#status').val("").trigger('change');
        });

        $('#btnRelVec').click(function(){
            var data = {
                inicio: moment($("#inicio").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                fim: moment($("#fim").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                cliente: $("#cliente").val(),
                plano: $("#idPlanoContas").val(),
                empresa: $("#contabilidade").val(),
                pagamento: $("#situacao").val(),
                stat: 1
            };

            if (data.inicio == "") {
                data.inicio = -1;
                data.fim = -1;
            }
            if (data.cliente == "") {
                data.cliente = -1;
            }
            if (data.plano == "") {
                data.plano = -1;
            }
            if (data.empresa == "") {
                data.empresa = -1;
            }

            if (data.pagamento == 3) {
                data.stat = 2;
                data.pagamento = -1;
            } else if (data.pagamento == "" || data.pagamento == null) {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione um pagamento!',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return false;
            }

            $.ReportPost('cr_g', data);
        });

        $('#btnRel').click(function(){
            var data = {
                inicio: moment($("#inicio").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                fim: moment($("#fim").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                cliente: $("#cliente").val(),
                plano: $("#idPlanoContas").val(),
                empresa: $("#contabilidade").val(),
                pagamento: $("#situacao").val(),
                stat: 1
            };

            if (data.inicio == "") {
                data.inicio = -1;
                data.fim = -1;
            }
            if (data.cliente == "") {
                data.cliente = -1;
            }
            if (data.plano == "") {
                data.plano = -1;
            }
            if (data.empresa == "") {
                data.empresa = -1;
            }

            if (data.pagamento == 3) {
                data.stat = 2;
                data.pagamento = -1;
            } else if (data.pagamento == "" || data.pagamento == null) {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione um pagamento!',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return false;
            }

            $.ReportPost('cr_new', data);
        });
    });
</script>