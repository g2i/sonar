<style>
    .col-edit-opcoes{
        min-width: 70px; !important;
        max-width: 70px; !important;
        width: 70px; !important;
    }
</style>
<?php if($this->getParam('modal')): ?>
    <style>
        .select2-close-mask{
            z-index: 2099;
        }
        .select2-dropdown{
            z-index: 3051;
        }
        .has-error .select2-selection {
            border: 1px solid #a94442;
            border-radius: 4px;
        }
    </style>
<?php else: ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas Receber</h2>
        <ol class="breadcrumb">
            <li>Receber</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="col-md-12">
                    <div class="row pull-right">
                        <p class="text-right ">
                            <button id="btnRateio" class="btn btn-info btn-sm"><span class="fa fa-share-alt-square"></span> Rateio</button>
                            <button id="btnAnexos" class="btn btn-info btn-sm"><span class="fa fa-paperclip"></span> Anexos</button>
                        </p>
                    </div>
                </div>
                <?php endif; ?>
                <div class="clearfix"></div>
                <?php if (isset($DadosMovimento)): ?>
                    <div class="alert alert-warning">
                        <h3>Dados da quitação</h3>
                        <table class="table">
                            <tr>
                                <td><strong>Banco:</strong> <br/> <?php echo $DadosMovimento['banco']; ?></td>
                                <td><strong>Movimento:</strong> <br/> <?php echo $DadosMovimento['movimento']; ?></td>
                                <td><strong>Data:</strong> <br/> <?php echo $DadosMovimento['data']; ?></td>
                            </tr>
                        </table>
                    </div>
                <?php endif; ?>
                <div class="clearfix"></div>

                <div class="alert alert-info">Os campos marcados com <span
                        class="small glyphicon glyphicon-asterisk"></span> são de
                    preenchimento obrigatório.
                </div>
                <form id="frmConta" method="post" role="form" action="<?php echo $this->Html->getUrl('Contasreceber', 'edit') ?>">
                    <input type="hidden" name="id" value="<?php echo $Contasreceber->id; ?>">
                    <input type="hidden" name="formulario" value="individual">

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Data do Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="data" id="data"
                                       value="<?php echo $Contasreceber->data ?>" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="vencimento">Vencimento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="vencimento" id="vencimento"
                                       value="<?php echo $Contasreceber->vencimento ?>" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="idCliente">Cliente <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="idCliente" name="idCliente" class="form-control clientes-ajax">
                                <?php if (!empty($clienteSelect)) echo '<option value="' . $clienteSelect->id . '">' . $clienteSelect->nome . '</option>'; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="idPlanoContas">Plano de Contas <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas">
                                <option></option>
                                <?php
                                foreach ($Planocontas as $p) {
                                    if ($p->id == $Contasreceber->idPlanoContas)
                                        echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                                    else
                                        echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="tipoDocumento">Tipo de Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento">
                                <option></option>
                                <?php
                                foreach ($tipoDocumento as $t) {
                                    if ($t->id == $Contasreceber->tipoDocumento)
                                        echo '<option selected value="' . $t->id . '">' . $t->descricao . '</option>';
                                    else
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoPagamento">Forma de Pagamento</label>
                            <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento">
                                <option></option>
                                <?php
                                foreach ($tipoPagamento as $t) {
                                    if ($t->id == $Contasreceber->tipoPagamento)
                                        echo '<option selected value="' . $t->id . '">' . $t->descricao . '</option>';
                                    else
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="numfiscal">N&uacute;mero do Documento Fiscal</label>
                            <input type="text" name="numfiscal" id="numfiscal" class="form-control"
                                   value="<?php echo $Contasreceber->numfiscal; ?>"
                                   placeholder="N&uacute;mero Fiscal">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="datafiscal">Data do Documento Fiscal</label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="datafiscal" id="datafiscal"
                                       value="<?php echo $Contasreceber->datafiscal ?>" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="valorBruto">Valor Bruto <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" name="valorBruto" id="valorBruto"
                                       class="form-control money2"
                                       value="<?php echo number_format($Contasreceber->valorBruto, 2, ',', '.') ?>"
                                       placeholder="Valor Bruto">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="juros">Juros</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" name="juros" id="juros"
                                       class="form-control money2"
                                       value="<?php echo number_format($Contasreceber->juros, 2, ',', '.') ?>"
                                       placeholder="Juros">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="multa">Multa</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="multa" onblur="calculaValor()" id="multa"
                                       class="form-control money2"
                                       value="<?php echo number_format($Contasreceber->multa, 2, ',', '.') ?>"
                                       placeholder="Juros">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="desconto">Desconto</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="desconto" onblur="calculaValor()" id="desconto"
                                       class="form-control money2"
                                       value="<?php echo number_format($Contasreceber->desconto, 2, ',', '.') ?>"
                                       placeholder="Desconto">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="valor">Valor L&iacute;quido</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input readOnly type="text" name="valor" id="valor" class="form-control"
                                       value="<?php echo number_format($Contasreceber->valor, 2, ',', '.') ?>"
                                       placeholder="Valor">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="correcaoMonetaria">Atualiza&ccedil;&atilde;o Monet&aacute;ria
                                <span class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="correcaoMonetaria" name="correcaoMonetaria" class="form-control selectPicker">
                                <?php
                                foreach ($correcaoMonetaria as $c) {
                                    if ($Contasreceber->correcaoMonetaria == $c->id) {
                                        echo "<option selected value='" . $c->id . "'>" . $c->nome . "</option>";
                                    } else {
                                        echo "<option value='" . $c->id . "'>" . $c->nome . "</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="dataPagamento">Data de Pagamento</label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="dataPagamento" id="dataPagamento"
                                       value="<?php echo $Contasreceber->dataPagamento ?>">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="contabilidade">Empresas <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="contabilidade" class="form-control selectPicker" id="contabilidade">
                                <option></option>
                                <?php
                                foreach ($Contabilidades as $c) {
                                    if ($c->id == $Contasreceber->contabilidade)
                                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                    else
                                        echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group" style="height:120px;">
                            <label for="status">Status</label>
                            <select name="status" class="form-control selectPicker" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Contasreceber->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>

                            <div class="tooltip bottom fade in" role="tooltip" style="z-index:1; display:block;">
                                <div class="tooltip-arrow"></div>
                                <div class="tooltip-inner" style="max-width:300px;text-align: left;">
                                    <span style='display:block;width:250px;font-size:10px;'> Ativo =  Em cobrança/ Quitada <br/> Inativo = Arquivada/Cobrança suspensa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6"></div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row clearfix">
                            <div class="table-responsive">
                                <div class="table-responsive">
                                    <table id="tblDeducoes" class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th data-column-id="id" data-identifier="true" data-visible="false"
                                                data-visible-in-selection="false">Id</th>
                                            <th data-column-id="deducao">Dedu&ccedil;&atilde;o</th>
                                            <th data-column-id="percentual" data-converter="percent">Valor</th>
                                            <th data-column-id="opcoes" data-formatter="opcoes"
                                                data-header-css-class="col-edit-opcoes"
                                                data-visible-in-selection="false"  data-sortable="false"></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                <textarea name="complemento" id="complemento"
                          class="form-control"><?php echo $Contasreceber->complemento ?></textarea>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <?php if(!$this->getParam('modal')): ?>
                            <?php if (empty($Contasreceber->dataPagamento)): ?>
                                <a href="<?php echo $this->Html->getUrl('Contasreceber', 'all') ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            <?php else: ?>
                                <a href="<?php echo $this->Html->getUrl('Contasreceber', 'all') ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal">Voltar</a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var conta = '<?php echo $Contasreceber->id; ?>';

    $(document).ready(function () {

        var grid = $('#tblDeducoes').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: -1,
            navigation: 1,
            ajax: true,
            url: root + '/Contasreceber/getDeducoes',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            converters:{
                percent: {
                    from: function (value) { return value; },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return $.number(value, 2) + '%';
                    }
                },
                date: {
                    from: function (value) {
                        if(value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                    }
                }
            },
            formatters:{
                opcoes: function(column, row){
                    var aux = ' ';
                    if(row.pago == 0)
                        aux = ' disabled ';

                    return  '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" ' +
                        'class="btn btn-xs btn-info'+ aux +'command-edit" data-row-id="' + row.id + '">' +
                        '<span class="fa fa-pencil-square"></span></button>' +
                        '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" ' +
                        'class="btn btn-xs btn-danger'+ aux +'command-delete" data-row-id="' + row.id + '">' +
                        '<span class="fa fa-trash-o"></span></button>';
                }
            },
            requestHandler: function (request) {
                request.id = conta;

                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                return request;
            },
            templates: {
                header: '<div id="{{ctx.id}}" class="{{css.header}}"><div class="row"><div class="col-sm-12 actionBar">' +
                '<p class="{{css.search}}"></p></div></div></div>',
                search: '<div class="{{css.search}}" style="width: 380px">' +
                '<button id="btnAddDeducao" class="btn btn-success" style="width: auto;" type="button">' +
                '<span class="glyphicon glyphicon-plus-sign"></span> Adicionar Dedu&ccedil;&atilde;o</button></div>',
            }
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {
            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-delete").unbind('click');
            grid.find(".command-delete").on("click", function (e){
                var deducao = $(this).data("row-id");
                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function (result) {
                        if (result) {
                            var url = root + '/Contasreceber/deleteDeducaoReceber';
                            var data = {
                                id: deducao
                            }

                            $.post(url, data, function (ret) {
                                if (ret.result) {
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                } else {
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var deducao = $(this).data("row-id");
                var url = root + '/Contasreceber/editDeducaoReceber/ajax:true/id:' + deducao;
                LoadGif();
                $('<div></div>').load(url, function(){
                    CloseGif();
                    BootstrapDialog.show({
                        title: 'Editar Dedu&ccedil;&atilde;o',
                        message: this,
                        type: BootstrapDialog.TYPE_DEFAULT,
                        buttons: [{
                            icon: 'fa fa-ban',
                            label: 'Cancelar',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        },{
                            icon: 'fa fa-floppy-o',
                            label: 'Salvar',
                            cssClass: 'btn-info',
                            autospin: true,
                            action: function (dialog) {
                                var body = dialog.getModalBody();
                                var valor = body.find('#percentual').unmask();
                                if(!(valor > 0)){
                                    BootstrapDialog.warning("Informe um percentual!");
                                    dialog.updateButtons();
                                    return;
                                }

                                dialog.enableButtons(false);
                                var dados = body.find('#frmDeducao').serialize();
                                var saveUrl = root + '/Contasreceber/post_editDeducaoReceber/';
                                $.post(saveUrl, dados, function(ret){
                                    if(ret.result){
                                        BootstrapDialog.success(ret.msg);
                                        grid.bootgrid('reload');
                                        dialog.close();
                                    }else{
                                        BootstrapDialog.warning(ret.msg);
                                        dialog.enableButtons(true);
                                        dialog.updateButtons();
                                    }
                                });
                            }
                        }],
                        onshown: function(dialog){
                            initeComponentes(dialog.getModalBody());
                        }
                    });
                });
            });

            <?php if(!empty($Contaspagar->dataPagamento)): ?>
            $("#btnAddDeducao").hide();
            <?php else: ?>
            $("#btnAddDeducao").unbind('click');
            $('#btnAddDeducao').click(function(){
                var url = root + '/Contasreceber/addDeducaoReceber/ajax:true/contasreceber:' + conta;
                $('<div></div>').load(url, function(){
                    BootstrapDialog.show({
                        title: 'Adicionar Dedu&ccedil;&atilde;o',
                        message: this,
                        type: BootstrapDialog.TYPE_DEFAULT,
                        buttons: [{
                            icon: 'fa fa-ban',
                            label: 'Cancelar',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        },{
                            icon: 'fa fa-floppy-o',
                            label: 'Salvar',
                            cssClass: 'btn-info',
                            autospin: true,
                            action: function (dialog) {
                                var body = dialog.getModalBody();
                                var valor = body.find('#percentual').unmask();
                                if(!(valor > 0)){
                                    BootstrapDialog.warning("Informe um percentual!");
                                    dialog.updateButtons();
                                    return;
                                }

                                dialog.enableButtons(false);
                                var dados = body.find('#frmDeducao').serialize();
                                var saveUrl = root + '/Contasreceber/post_addDeducaoReceber/';
                                $.post(saveUrl, dados, function(ret){
                                    if(ret.result){
                                        BootstrapDialog.success(ret.msg);
                                        grid.bootgrid('reload');
                                        dialog.close();
                                    }else{
                                        BootstrapDialog.warning(ret.msg);
                                        dialog.enableButtons(true);
                                        dialog.updateButtons();
                                    }
                                });
                            }
                        }],
                        onshown: function(dialog){
                            initeComponentes(dialog.getModalBody());
                        }
                    });
                });
            });
            <?php endif; ?>
        });

        <?php if(!empty($Contasreceber->dataPagamento)): ?>
        BootstrapDialog.show({
            closable: true,
            title: 'Conta bloqueada',
            type: BootstrapDialog.TYPE_WARNING,
            message: '<p>Esta conta não poderá ser editada, pois a mesma já se encontra quitada.</p>' +
            '<p>O acesso está liberado apenas para <strong>visualização</strong></p>',
            buttons:[{
                icon: 'fa fa-ban',
                label: 'Fechar',
                cssClass: 'btn-danger',
                action: function (dialog) {
                    dialog.close();
                }
            }]

        });
        <?php endif; ?>

        <?php if(!$this->getParam('modal')): ?>
        $('#btnRateio').click(function(){
            var urlRateios = root + '/Rateio_contasreceber/getRateios/id:' + conta;
            var url = root + '/Rateio_contasreceber/all/contas_receber:' + conta +'/ajax:true';
            LoadGif();
            $.post(urlRateios, function(ret){
                var rGrid = null;
                var rateiosExcluidos = [];
                var rateios = ret.rateios;
                for(var i = 0; i < rateios.length; i++){
                    rateios[i].id = parseFloat(rateios[i].id);
                    rateios[i].valor = parseFloat(rateios[i].valor);
                    rateios[i].empresaId = parseFloat(rateios[i].empresaId);
                    rateios[i].centroId = parseFloat(rateios[i].centroId);
                }

                $('<span></span>').load(url, function () {
                    CloseGif();
                    BootstrapDialog.show({
                        size: 'size-wide',
                        title: 'Rateio Contas a Receber',
                        message: this,
                        type: BootstrapDialog.TYPE_DEFAULT,
                        spinicon: 'fa fa-spinner fa-pulse',
                        onshown: function (dialog) {
                            var rateioModal = dialog.getModalBody();
                            rGrid = rateioModal.find('#tblDados');

                            rGrid.bootgrid({
                                rowSelect: true,
                                multiSort: false,
                                navigation: 0,
                                rowCount: -1,
                                converters: {
                                    currency: {
                                        from: function (value) {
                                            return value;
                                        },
                                        to: function (value) {
                                            if (value == null)
                                                return null;

                                            return 'R$ ' + $.number(value, 2);
                                        }
                                    },
                                    date: {
                                        from: function (value) {
                                            if (value == null)
                                                return null;

                                            return moment(value);
                                        },
                                        to: function (value) {
                                            if (value == null)
                                                return null;

                                            return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                                        }
                                    }
                                },
                                formatters: {
                                    opcoes: function (column, row) {
                                        return '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                                    }
                                }
                            });

                            rGrid.on("loaded.rs.jquery.bootgrid", function () {

                                rGrid.find('[data-toggle="tooltip"]').tooltip();

                                rGrid.find(".command-edit").unbind('click');
                                rGrid.find(".command-edit").on("click", function (e) {
                                    var conta = $(this).data("row-id");
                                    var dados = rGrid.bootgrid('getCurrentRows');
                                    var total = rateioModal.find('#total');
                                    var totalRateio = total.unmask();
                                    var valorConta = rateioModal.find('#valor').unmask();
                                    var rows = $.grep(dados, function (dado) {
                                        return dado.id == conta;
                                    });

                                    var rateio = rows[0];
                                    totalRateio -= rateio.valor;
                                    var sobra = valorConta - totalRateio;
                                    var url = root + '/Rateio_contasreceber/add/id:' + rateio.conta + '/modal:1/ajax:true';
                                    $('<span></span>').load(url, function () {
                                        BootstrapDialog.show({
                                            title: 'Editar Rateio',
                                            message: this,
                                            type: BootstrapDialog.TYPE_DEFAULT,
                                            onshown: function (dialog) {
                                                var body = dialog.getModalBody();
                                                var valor = body.find('#valor');
                                                valor.val($.number(rateio.valor, 2));

                                                var empresa = body.find('#empresaId');
                                                empresa.val(rateio.empresaId);
                                                empresa.trigger('change');

                                                var centro = body.find('#centroId');
                                                centro.val(rateio.centroId);

                                                var observacao = body.find('#observacao');
                                                observacao.text(rateio.observacao);

                                                initeComponentes(dialog.getModalBody());

                                                var empresa = body.find('#empresaId');
                                                empresa.val(rateio.empresaId).trigger('change');

                                                var centro = body.find('#centroId');
                                                centro.val(rateio.centroId).trigger('change');
                                            },
                                            buttons: [{
                                                icon: 'fa fa-ban',
                                                label: 'Cancelar',
                                                cssClass: 'btn-danger',
                                                action: function (dialog) {
                                                    dialog.close();
                                                }
                                            }, {
                                                icon: 'fa fa-floppy-o',
                                                label: 'Salvar',
                                                cssClass: 'btn-info',
                                                action: function (dialog) {
                                                    var body = dialog.getModalBody();
                                                    var valor = body.find('#valor').unmask();
                                                    if (valor > sobra || valor < 0) {
                                                        BootstrapDialog.warning("Valor incorreto para rateio !");
                                                        return;
                                                    }

                                                    var form = body.find('form').serializeArray();
                                                    $.each(form, function () {
                                                        rateio[this.name] = this.value || '';
                                                    });

                                                    if (rateio.empresaId < 1) {
                                                        BootstrapDialog.warning("Selecione uma empresa !");
                                                        return;
                                                    }

                                                    if (rateio.centroId < 1) {
                                                        BootstrapDialog.warning("Selecione um Centro de Custo !");
                                                        return;
                                                    }

                                                    var rows = [];
                                                    var dados = rGrid.bootgrid('getCurrentRows');
                                                    rows = $.grep(dados, function (dado) {
                                                        return dado.empresaId == rateio.empresaId &&
                                                            dado.centroId == rateio.centroId &&
                                                            dado.id != rateio.id;
                                                    });

                                                    if (rows.length > 0) {
                                                        BootstrapDialog.warning("Centro de Custo e empresa já informados !");
                                                        return;
                                                    }

                                                    if(rateio.situacao != 'N')
                                                        rateio.situacao = 'E';

                                                    rateio.valor = parseFloat(rateio.valor.replace('.', '').replace(',', '.'));
                                                    totalRateio += rateio.valor;
                                                    total.unpriceFormat();
                                                    total.text($.number(totalRateio, 2));
                                                    total.priceFormat({
                                                        prefix: 'R$ ',
                                                        centsSeparator: ',',
                                                        thousandsSeparator: '.',
                                                        allowNegative: true
                                                    });

                                                    var empresa = body.find('#empresaId');
                                                    var empresaObj = empresa.select2('data')[0];
                                                    rateio.empresa = empresaObj.text;

                                                    var centro = body.find('#centroId');
                                                    var centroObj = centro.select2('data')[0];
                                                    rateio.centro = centroObj.text;

                                                    rGrid.bootgrid('reload');
                                                    dialog.close();
                                                }
                                            }]
                                        });
                                    });
                                });

                                rGrid.find(".command-delete").unbind('click');
                                rGrid.find(".command-delete").on("click", function (e) {
                                    var conta = $(this).data("row-id");
                                    BootstrapDialog.confirm({
                                        title: 'Aviso',
                                        message: 'Voc\u00ea tem certeza ?',
                                        type: BootstrapDialog.TYPE_WARNING,
                                        closable: false,
                                        draggable: false,
                                        btnCancelLabel: 'N\u00e3o desejo excluir!',
                                        btnOKLabel: 'Sim desejo excluir!',
                                        btnOKClass: 'btn-warning',
                                        callback: function (result) {
                                            if (result) {
                                                var dados = rGrid.bootgrid('getCurrentRows');
                                                var rows = $.grep(dados, function (dado) {
                                                    return dado.id == conta;
                                                });

                                                var rateio = rows[0];
                                                console.log(rateio);

                                                if (rateio.situacao != 'N') {
                                                    rateio.situacao = 'D';
                                                    rateiosExcluidos.push(rateio);
                                                }

                                                rGrid.bootgrid('remove', [conta]);
                                            }
                                        }
                                    });
                                });
                            });

                            rGrid.on("appended.rs.jquery.bootgrid", function (e, rows){
                                var total = rateioModal.find('#total');
                                var totalRateio = total.unmask();
                                for(var i = 0; i < rows.length; i++){
                                    totalRateio += rows[i].valor;
                                }

                                console.log(totalRateio);

                                total.unpriceFormat();
                                total.text($.number(totalRateio, 2));
                                total.priceFormat({
                                    prefix: 'R$ ',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    allowNegative: true
                                });
                            });

                            rGrid.on("removed.rs.jquery.bootgrid", function (e, rows){
                                var total = rateioModal.find('#total');
                                var totalRateio = total.unmask();
                                for(var i = 0; i < rows.length; i++){
                                    totalRateio -= rows[i].valor;
                                }

                                total.unpriceFormat();
                                total.text($.number(totalRateio, 2));
                                total.priceFormat({
                                    prefix: 'R$ ',
                                    centsSeparator: ',',
                                    thousandsSeparator: '.',
                                    allowNegative: true
                                });
                            });

                            rateioModal.find('#valor').priceFormat({
                                prefix: 'R$ ',
                                centsSeparator: ',',
                                thousandsSeparator: '.',
                                allowNegative: true
                            });

                            rGrid.bootgrid('append', rateios);
                        },
                        onhide: function(dialog){
                            if(dialog.getData('close'))
                                return true;

                            var body = dialog.getModalBody();
                            var valorConta = body.find('#valor').unmask();
                            var rateio = body.find('#total').unmask();
                            var rateios = rateiosExcluidos.concat(rGrid.bootgrid('getCurrentRows'));
                            var rows = $.grep(rateios, function (rateio) {
                                return rateio.situacao != 'S';
                            });

                            if (rows.length > 0) {
                                BootstrapDialog.confirm('Você tem modificações não salva, sair sem salvar ?', function (result) {
                                    if (result) {
                                        dialog.setData('close', result);
                                        dialog.close();
                                    }
                                });

                                return false;
                            }
                        },
                        buttons: [{
                            icon: 'glyphicon glyphicon-plus',
                            label: 'Cadastrar Rateio',
                            cssClass: 'btn-success',
                            action: function (dialog) {
                                var body = dialog.getModalBody();
                                var total = body.find('#total');
                                var totalRateio = total.unmask();
                                var valorConta = body.find('#valor').unmask();

                                var sobra = valorConta - totalRateio;
                                if (sobra <= 0) {
                                    BootstrapDialog.alert("Para este lançamento já foi feito o rateio!");
                                    return;
                                }
                                var url = root + '/Rateio_contasreceber/add/id:' + conta + '/modal:1/ajax:true';
                                $('<span></span>').load(url, function () {
                                    BootstrapDialog.show({
                                        title: 'Adicionar Rateio',
                                        message: this,
                                        type: BootstrapDialog.TYPE_DEFAULT,
                                        onshow: function (dialog) {
                                            var body = dialog.getModalBody();
                                            var valor = body.find('#valor');
                                            valor.val($.number(sobra, 2));
                                        },
                                        onshown: function (dialog) {
                                            initeComponentes(dialog.getModalBody());
                                        },
                                        buttons: [{
                                            icon: 'fa fa-ban',
                                            label: 'Cancelar',
                                            cssClass: 'btn-danger',
                                            action: function (dialog) {
                                                dialog.close();
                                            }
                                        }, {
                                            icon: 'fa fa-floppy-o',
                                            label: 'Salvar',
                                            cssClass: 'btn-info',
                                            action: function (dialog) {
                                                var body = dialog.getModalBody();
                                                var valor = body.find('#valor').unmask();
                                                if (valor > sobra || valor < 0) {
                                                    BootstrapDialog.warning("Valor incorreto para rateio !");
                                                    return;
                                                }

                                                var form = body.find('form').serializeArray();
                                                var rateio = {};
                                                $.each(form, function () {
                                                    if (rateio[this.name] !== undefined) {
                                                        if (!rateio[this.name].push) {
                                                            rateio[this.name] = [rateio[this.name]];
                                                        }
                                                        rateio[this.name].push(this.value || '');
                                                    } else {
                                                        rateio[this.name] = this.value || '';
                                                    }
                                                });

                                                if (rateio.empresaId < 1) {
                                                    BootstrapDialog.warning("Selecione uma empresa !");
                                                    return;
                                                }

                                                if (rateio.centroId < 1) {
                                                    BootstrapDialog.warning("Selecione um Centro de Custo !");
                                                    return;
                                                }

                                                var rows = [];
                                                var dados = rGrid.bootgrid('getCurrentRows');
                                                rows = $.grep(dados, function (dado) {
                                                    return dado.empresaId == rateio.empresaId &&
                                                        dado.centroId == rateio.centroId;
                                                });

                                                if (rows.length > 0) {
                                                    BootstrapDialog.warning("Centro de Custo e empresa já informados !");
                                                    return;
                                                }

                                                rows = [];
                                                var newId = 0;

                                                do
                                                {
                                                    newId = Math.round(new Date().getTime() + (Math.random() * 100));
                                                    rows = $.grep(dados, function (dado) {
                                                        return dado.id == newId;
                                                    });
                                                } while (rows.length > 0);

                                                rateio.id = newId;
                                                rateio.situacao = 'N';
                                                rateio.valor = parseFloat(rateio.valor.replace('.', '').replace(',', '.'));

                                                var empresa = body.find('#empresaId');
                                                var empresaObj = empresa.select2('data')[0];
                                                rateio.empresa = empresaObj.text;

                                                var centro = body.find('#centroId');
                                                var centroObj = centro.select2('data')[0];
                                                rateio.centro = centroObj.text;

                                                rGrid.bootgrid('append', [rateio]);
                                                dialog.close();
                                            }
                                        }]
                                    });
                                });
                            }
                        }, {
                            icon: 'fa fa-ban',
                            label: 'Cancelar',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }, {
                            icon: 'fa fa-floppy-o',
                            label: 'Salvar',
                            cssClass: 'btn-info',
                            autospin: true,
                            action: function (dialog) {
                                var body = dialog.getModalBody();
                                var valorConta = body.find('#valor').unmask();
                                var rateio = body.find('#total').unmask();
                                if (rateio != valorConta && rateio != 0) {
                                    BootstrapDialog.warning("Rateio incompleto!");
                                    dialog.updateButtons();
                                    return;
                                }

                                var rateios = rateiosExcluidos.concat(rGrid.bootgrid('getCurrentRows'));
                                var saveUrl = root + '/Rateio_contasreceber/saveRateios';
                                var data = {
                                    rateios: $.toJSON(rateios)
                                }

                                $.post(saveUrl, data, function(ret){
                                    if(ret.result){
                                        var rows = rGrid.bootgrid('getCurrentRows');
                                        for(var i = 0; i < rows.length; i++){
                                            rows[i].situacao = 'S';
                                        }

                                        rateiosExcluidos = [];
                                        BootstrapDialog.success(ret.msg, function(){
                                            dialog.close();
                                        });
                                    }else{
                                        BootstrapDialog.warning(ret.msg);
                                        dialog.updateButtons();
                                    }
                                });
                            }
                        }]
                    });
                });
            });
        });

        $('#btnAnexos').click(function(){
            var url = root + '/Anexo/all/modal:1/ajax:true/first:1/tipo:2/id_externo:' + conta;
            showOnModal(url, 'Anexos');
        });
        <?php endif; ?>

        setTimeout(function(){
            calculaValor();
        }, 2000);
    });

    function calculaValor() {
        if ($('#multa').val() != "undefined" && $('#multa').val() != "") {
            var multa = $('#multa').unmask();
        } else {
            var multa = 0;
        }

        if ($('#desconto').val() != "undefined" && $('#desconto').val() != "") {
            var desconto = $('#desconto').unmask();
        } else {
            var desconto = 0;
        }

        if ($('#valorBruto').val() != "undefined" && $('#valorBruto').val() != "") {
            var valorBruto = $('#valorBruto').unmask();
        } else {
            var valorBruto = 0;
        }

        if ($('#juros').val() != "undefined" && $('#juros').val() != "") {
            var juros = $('#juros').unmask();
        } else {
            var juros = 0;
        }

        var aux = (valorBruto + juros + multa) - desconto;
        aux = Math.round(aux*100)/100;

        var valorDeducao = aux * (parseFloat(<?php echo $percentual?>) / 100);
        valorDeducao = aux - valorDeducao;
        valorDeducao = Math.round(valorDeducao*100)/100;

        $('#valorDeduzido').unpriceFormat();
        $('#valorDeduzido').text(valorDeducao.toFixed(2));
        $('#valorDeduzido').priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });

        $('#valor').unpriceFormat();
        $('#valor').val(aux.toFixed(2));
        $('#valor').priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
    }
</script>