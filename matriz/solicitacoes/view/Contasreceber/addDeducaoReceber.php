<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
    .has-error .select2-selection {
        border: 1px solid #a94442;
        border-radius: 4px;
    }
</style>
<form id="frmDeducao">
    <input type="hidden" value="<?php echo $_GET['contasreceber'] ?>" name="contasreceber_id"
           id="contasreceber_id">

    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="deducoes_id">Dedu&ccedil;&atilde;o</label>
            <select name="deducoes_id" class="form-control selectPicker" id="deducoes_id">
                <?php
                foreach ($Deducao as $d) {
                    echo '<option selected value="' . $d->id . '">' . $d->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="percentual">Percentual</label>
            <div class='input-group'>
                <input type="text" id="percentual" name="percentual" class="form-control percent text-right">
                <span class="input-group-addon">%</span>
            </div>
        </div>
    </div>
</form>
<div class="clearfix"></div>