<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Periodicidade</h2>
        <ol class="breadcrumb">
            <li>Periodicidade</li>
            <li class="active">
                <strong>All</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtro</h3></div>
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                           value="<?php echo $this->getParam('nome'); ?>">
                                </div>
                                <div class="col-md-12 text-right">

                                    <button type="button" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- botao de cadastro -->
            <div class="text-right">
                <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova', 'Periodicidade', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Periodicidade', 'all', array('orderBy' => 'nome')); ?>'>
                                            Nome
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Periodicidade', 'all', array('orderBy' => 'dias')); ?>'>
                                            Dias
                                        </a>
                                    </th>
                                    <th></th>
                                </tr>
                                <?php
                                foreach ($Periodicidades as $p) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($p->nome, 'Periodicidade', 'view',
                                        array('id' => $p->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($p->dias, 'Periodicidade', 'view',
                                        array('id' => $p->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td class="text-right actions">';
                                    echo $this->Html->getLink('<span class="fa fa-pencil-square"></span>', 'Periodicidade', 'edit',
                                        array('id' => $p->id),
                                        array('class' => 'btn btn-info btn-xs'));
                                    echo $this->Html->getLink('<span class="fa fa-trash-o"></span>', 'Periodicidade', 'delete',
                                        array('id' => $p->id),
                                        array('class' => 'btn btn-danger btn-xs', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Periodicidade', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Periodicidade', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>