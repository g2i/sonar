<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Empresa/Centro Custo</h2>
        <ol class="breadcrumb">
            <li> Empresa/Centro Custo</li>
            <li class="active">
                <strong>Deletar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form class="form" method="post" action="<?php echo $this->Html->getUrl('Empresa_custo', 'delete') ?>">
                    <h1>Confirmação</h1>

                    <div class="well well-lg">
                        <p>Voce tem certeza que deseja excluir o Empresa_custo
                            <strong><?php echo $Empresa_custo->id; ?></strong>?</p>
                    </div>
                    <div class="text-right">
                        <input type="hidden" name="id" value="<?php echo $Empresa_custo->id; ?>">
                        <a href="<?php echo $this->Html->getUrl('Empresa_custo', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-danger" value="Excluir">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>