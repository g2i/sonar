<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
<form role="form">
    <div class="alert alert-info">Os campos marcados com <span
            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
    </div>
    <div class="form-group">
        <label for="conta" class="required">Contas Pagar <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <input type="hidden" name="conta" id="conta" value="<?php echo $Contaspagares->id ?>" />
        <input type="hidden" name="id" id="id" value="0">
        <input type="text" name="contas" class="form-control"
               value="<?php echo $Contaspagares->getPlanocontas()->nome . " - " . convertDataSQL4BR($Contaspagares->vencimento) ?>" disabled="true" />
    </div>

    <div class="form-group">
        <label for="data_documento" class="required">Data do Documento <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <input type="date" name="data_documento" id="data_documento" readonly class="form-control"  value="<?php echo $Contaspagares->data ?>"/>
    </div>

    <div class="form-group">
        <label for="plano_contas_id" class="required">Plano de contas <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <select name="plano_contas_id" class="form-control" id="plano_contas_id">
            <?php
            foreach ($Planocontas as $p) {
                if ($p->id == $Contaspagares->idPlanoContas)
                    echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                else
                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
            }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label for="fornecedor_id" class="required">Credor <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <select name="fornecedor_id" id="fornecedor_id" class="form-control">
            <?php
            foreach ($Fornecedores as $f) {
                if ($f->id == $Contaspagares->idFornecedor)
                    echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                else
                    echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
            }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label for="empresa">Empresa</label>
        <select name="empresaId" class="form-control selectPicker empresa" id="empresaId">
            <option value=""></option>
            <?php foreach ($Contabilidades as $c): ?>
                <option value="<?php echo $c->id ?>"><?php echo $c->nome?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <input type="hidden" class="emp" id="emp"/>

    <div class="form-group">
        <label for="centro_custo" class="required">Centro de Custo <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <select name="centroId" class="form-control selectPicker centro_custo"
                id="centroId" required>
            <option value=""></option>
        </select>
    </div>

    <div class="form-group">
        <label class="required" for="valor">Valor <span class="glyphicon glyphicon-asterisk"></span></label>
        <div class='input-group'>
            <span class="input-group-addon">R$</span>
            <input type="text" name="valor" id="valor" class="form-control valor money2"
                   value="<?php echo $Contaspagares->valorBruto ?>"
                   placeholder="Valor" required>
        </div>
    </div>

    <div class="form-group">
        <label for="observacao">Observação</label>
        <textarea name="observacao" id="observacao" class="form-control"></textarea>
    </div>
</form>

<script>
    $(document).ready(function () {
        $(".empresa").change(function () {
            $(".emp").val($(this).val());
            $.ajax({
                type: "POST",
                url: root + "/Empresa_custo/combo",
                data: "id=" + $(this).val(),
                async: false,
                success: function (txt) {
                    $(".centro_custo").select2('destroy');
                    $(".centro_custo").html(txt);
                    $(".centro_custo").select2({
                        language: 'pt-BR',
                        theme: 'bootstrap',
                        width: '100%',
                        minimumResultsForSearch: 6,
                        placeholder: 'Selecione:'
                    });
                }
            });
        });
    });
</script>