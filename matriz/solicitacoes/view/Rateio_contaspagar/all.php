<style>
    .col-rateio-opcoes{
        min-width: 70px; !important;
        max-width: 70px; !important;
        width: 70px; !important;
    }
</style>
<div class="row">
    <input type="hidden" id="contaId" value="<?php echo $contaId ?>">
    <div class="table-responsive">
        <table id="tblDados" class="table table-hover">
            <thead>
            <tr>
                <th data-column-id="id" data-identifier="true" data-visible="false"
                    data-visible-in-selection="false">Id</th>
                <th data-column-id="conta" data-visible="false"
                    data-visible-in-selection="false">Conta</th>
                <th data-column-id="centro">Centro de Custo</th>
                <th data-column-id="empresa">Empresa</th>
                <th data-column-id="valor" data-converter="currency">Valor</th>
                <th data-column-id="observacao">Observa&ccedil;&atilde;o</th>
                <th data-column-id="opcoes" data-formatter="opcoes"
                    data-header-css-class="col-rateio-opcoes" data-visible-in-selection="false"
                    data-sortable="false"></th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot>
            <tr>
                <td style="font-weight: bold">Valor:</td>
                <td id="valor" style="font-weight: bold"><?php echo $valor ?></td>
                <td style="font-weight: bold">Valor Rateio:</td>
                <td id="total" style="font-weight: bold">0.00</td>
                <td></td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>