<form class="form" id='frm' method="post" action="<?php echo $this->Html->getUrl('Boleto', 'boleto') ?>" target="_blank">
    <h1>Gerar Boleto</h1>
    <div class="well well-lg">
        <div class="form-group">
            <label for="cedente">Selecione um Cedente</label>
            <select name="cedente" id="cedente" class="form-control" required>
                <option value="-1">Selecione</option>
                <?php foreach($Cedente as $c){
                    if($c->id == $this->getParam('cedente'))
                        echo "<option value='".$c->id."' selected>".$c->cedente."</option>";
                    else
                        echo "<option value='".$c->id."' >".$c->cedente."</option>";
                } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="valor">Valor</label>
            <input type="text" class="form-control" id="valor" name="valor" onkeyup="Calcular()" value="<?php echo number_format($Contas->valor,2,',','.'); ?>" />
        </div>

        <div class="form-group">
            <label for="vencBoleto">Vencimento</label>
            <input type="hidden" id="dataAntiga" name="dataAntiga" class="form-control" required value="<?php echo $Contas->vencimento; ?>" />
            <input type="date" id="vencBoleto" name="vencBoleto" class="form-control" required value="<?php echo $Contas->vencimento; ?>" />
        </div>

        <div class="form-group">
            <label for="multa"><span id="txtmulta">Multa - R$: <?php echo number_format($Contas->multa,2,',','.'); ?></span></label>
            <input type="hidden" class="form-control" id="tmulta" name="tmulta" value="<?php echo number_format($Contas->multa,2,',','.'); ?>" />
            <input type="number" class="form-control" id="multa" name="multa" onkeyup="Calcular()" value="<?php echo $perMulta; ?>" />
        </div>

        <div class="form-group">
            <label for="juros"><span id="txtjuros">Juros - R$: <?php echo number_format($juros,2,',','.'); ?></span></label>
            <input type="hidden" class="form-control" id="tjuros" name="tjuros" value="<?php echo number_format($juros,2,',','.'); ?>" />
            <input type="number" class="form-control" id="juros" name="juros" onkeyup="Calcular()" value="<?php echo $perJuros; ?>" />
        </div>

        <div class="form-group">
            <label for="desconto">Desconto</label>
            <input type="text" class="form-control" id="desconto" name="desconto" onkeyup="Calcular()" value="<?php echo number_format($Contas->desconto,2,',','.'); ?>" />
        </div>

        <div class="form-group">
            <label for="recalculado">Recalculado</label>
            <input type="hidden" class="form-control" id="dias" name="dias" readonly value="<?php echo $dias; ?>" />
            <input type="text" class="form-control" id="recalculado" name="recalculado" readonly value="<?php echo number_format($recalculado,2,',','.'); ?>" />
        </div>
        <div class="form-group" id="apend">
        </div>
        <div class="form-group" id="validation">
        </div>
    </div>

    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Contas->id; ?>">
        <input type="hidden" name="print" id="print" value="">
        <a href="<?php echo $this->Html->getUrl('Contasreceber', 'all') ?>" class="btn btn-default" data-dismiss="modal" id="cancel">Cancelar</a>
        <input type="button" class="btn btn-warning" onclick="return validar()" value="Imprimir">
        <input type="button" class="btn btn-success" id="btnSend" onclick="return send()" value="Enviar por Email">

    </div>
</form>
<script>
    $("#vencBoleto").blur(function(){
        var inicio = new Date($("#dataAntiga").val());
        var fim  = new Date($("#vencBoleto").val());
        var dias = diferencaEmDias(inicio,fim);
        var j = $("#juros").val();
        j = parseFloat(j/100);
        var j = parseFloat((parseFloat($("#valor").val())*j)/30);
        if(dias>0) {
            var juros = parseFloat(j * dias);
            $("#txtjuros").val(juros.formatMoney(2, ',', '.'));
            $("#tjuros").val(juros.formatMoney(2, ',', '.'));
            Calcular();
        }
    });

    function Calcular(){
        var valor = $("#valor").val();
        valor = replaceAll(valor,'.','');
        valor=valor.replace(',','.');
        if(valor=="")
            valor=0;

        var desconto = $("#desconto").val();
        desconto = replaceAll(desconto,'.','');
        desconto=desconto.replace(',','.');
        if(desconto=="")
            desconto=0;
        if(parseInt($("#dias").val())>0){
            var juros = $("#juros").val();
            juros = juros/100;
            juros = (parseFloat(valor)*parseFloat(juros))/30;
            juros = (parseFloat(juros)*parseInt($("#dias").val()));

            var multa = $("#multa").val();
            multa=multa/100;
            multa = (parseFloat(multa)*parseFloat(valor));
        }else{
            juros=0;
            multa=0;
        }

        $("#txtjuros").text("Juros - R$ "+juros.formatMoney(2,',','.'));
        $("#tjuros").val(juros.formatMoney(2,',','.'));
        $("#txtmulta").text("Multa - R$ "+multa.formatMoney(2,',','.'));
        $("#tmulta").val(multa.formatMoney(2,',','.'));
        var saldo = ((parseFloat(valor) + parseFloat(juros) + parseFloat(multa)) - parseFloat(desconto));
        $('#recalculado').val(saldo.formatMoney(2,',', '.'));

    }
    function validar(){
        $("#print").val(1);
        if($("#cedente").val()=="-1"){
            OpenMensagem("Alerta","Selecione um cedente!");
            return false;
        }
        $("#frm").submit();
        $("#cancel").click();
    }
    $(document).ready(function(){
        $("#valor").maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
        $("#desconto").maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
        $("#recalculado").maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
    });

    function send(){
        $("#frm").removeAttr("action");
        $("#frm").removeAttr("target");
        $("#frm").attr("action",root+"/Boleto/enviar_email");
        $("#btnSend").attr("onclick","post_send()");
        $("#btnSend").attr("value","Enviar");

        var input = "<label for='email'>Email</label>";
            input+="<input type='mail' name='email' id='email' value='<?php echo $email; ?>' required class='form-control' placeholder='Email de Cobrança' />";
        $("#apend").html(input);
    }
    function post_send(){
        er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
        if( !er.exec($("#email").val()))
        {
            var a = "<div class='alert alert-danger' role='alert'>Email inválido!</div>";
            $("#validation").html(a);
            return false;
        }
        $("#frm").submit();
    }
</script>