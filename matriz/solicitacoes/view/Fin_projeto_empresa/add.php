
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Fin_projeto_empresa</h2>
    <ol class="breadcrumb">
    <li>Fin_projeto_empresa</li>
    <li class="active">
    <strong>Adicionar Fin_projeto_empresa</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Fin_projeto_empresa', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="nome">nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Fin_projeto_empresa->nome ?>" placeholder="Nome">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="created">created</label>
            <input type="datetime" name="created" id="created" class="form-control" value="<?php echo $Fin_projeto_empresa->created ?>" placeholder="Created">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="modified">modified</label>
            <input type="datetime" name="modified" id="modified" class="form-control" value="<?php echo $Fin_projeto_empresa->modified ?>" placeholder="Modified">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="fin_projeto_grupo_id">fin_projeto_grupo_id</label>
            <select name="fin_projeto_grupo_id" class="form-control" id="fin_projeto_grupo_id">
                <?php
                foreach ($Fin_projeto_grupos as $f) {
                    if ($f->id == $Fin_projeto_empresa->fin_projeto_grupo_id)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="situacao_id">situacao_id</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Fin_projeto_empresa->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="user_created">user_created</label>
            <select name="user_created" class="form-control" id="user_created">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Fin_projeto_empresa->user_created)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="user_modified">user_modified</label>
            <select name="user_modified" class="form-control" id="user_modified">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Fin_projeto_empresa->user_modified)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Fin_projeto_empresa', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>