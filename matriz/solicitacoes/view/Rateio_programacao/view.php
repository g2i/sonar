<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Rateio</h2>
        <ol class="breadcrumb">
            <li>Programação</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Valor</strong>: <?php echo $Rateio_contaspagar->valor; ?></p>

                <p><strong>Observacao</strong>: <?php echo $Rateio_contaspagar->observacao; ?></p>

                <p>
                    <strong>Contaspagar</strong>:
                    <?php
                    echo $this->Html->getLink($Rateio_contaspagar->getContaspagar()->numeroDocumento, 'Contaspagar', 'view',
                        array('id' => $Rateio_contaspagar->getContaspagar()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Contabilidade</strong>:
                    <?php
                    echo $this->Html->getLink($Rateio_contaspagar->getContabilidade()->nome, 'Contabilidade', 'view',
                        array('id' => $Rateio_contaspagar->getContabilidade()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>