<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rateio_programacao', 'add') ?>">
    <div class="alert alert-info">Os campos marcados com <span
            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
    </div>
    <div class="form-group">
        <label for="contas_receber" class="required">Programação<span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <?php
        if ($this->getParam('programacao')) {
            echo '<input type="hidden" name="programacao" id="programacao" class="programacao" value="' . $Programacao->id . '" />';
            echo '<input type="text" name="contas" class="form-control" value="' . $Programacao->getPlanocontas()->nome . "-" . $Programacao->diaVencimento . '" disabled="true" />';
        } else {
            ?>
            <select name="programacao" class="form-control programacao" id="programacao" required
                    onchange="ChecarRateios($(this).val()) ">
                <?php
                foreach ($Programacao as $c) {
                    if ($c->id == $this->getParam('programacao'))
                        echo '<option selected value="' . $c->id . '">' . $c->getPlanocontas()->nome . "-" . $c->diaVencimento . '</option>';
                    else
                        echo '<option value="' . $c->id . '">' . $c->getPlanocontas()->nome . "-" . $c->diaVencimento . '</option>';
                }
                ?>
            </select>
        <?php } ?>
    </div>

    <div class="form-group">
        <label for="data_documento">Data Documento</label>
        <input type="date" class="form-control" id="data_documento" name="data_documento" readonly
               value="<?php echo $Programacao->data_documento ?>">
    </div>

    <div class="form-group">
        <label for="empresa">Empresa</label>
        <select name="empresa" class="form-control empresa" id="empresa">
            <option value="">Selecione</option>
            <?php
            foreach ($Contabilidades as $c) {
                if ($c->id == $Rateio_programacao->empresa)
                    echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                else
                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
            }
            ?>
        </select>
    </div>

    <div class="form-group">
        <label for="centro_custo" class="required">Centro de Custo <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <select name="centro_custo" class="form-control centro_custo" id="centro_custo" required>
            <option value="">Selecione</option>
        </select>
    </div>

    <div class="form-group">
        <label for="plano_contas_id" class="required">Plano de contas <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <select name="plano_contas_id" class="form-control" id="plano_contas_id">
            <?php
            foreach ($Planocontas as $p) {
                if ($p->id == $Programacao->idPlanoContas)
                    echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                else
                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
            }
            ?>
        </select>
    </div>

    <?php if ($Programacao->tipo == "A pagar"): ?>
        <div class="form-group">
            <label for="fornecedor_id" class="required">Credor <span
                    class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="fornecedor_id" class="form-control" id="fornecedor_id">
                <?php
                foreach ($Fornecedor as $f) {
                    if ($f->id == $Programacao->idFornecedor)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
            </select>
        </div>
    <?php endif ?>

    <?php if ($Programacao->tipo == "A receber"): ?>
        <div class="form-group">
            <label for="cliente_id" class="required">Cliente <span
                    class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="cliente_id" class="form-control" id="cliente_id">
                <?php
                foreach ($Cliente as $c) {
                    if ($c->id == $Programacao->idCliente)
                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                    else
                        echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                }
                ?>
            </select>
        </div>
    <?php endif ?>

    <input type="hidden" class="emp" id="emp"/>

    <div class="form-group">
        <label class="required" for="valor">Valor <span class="glyphicon glyphicon-asterisk"></span></label>
        <?php
        if ($this->getParam('programacao')) { ?>
            <input type="text" step="0,01" name="valor" id="valor" class="form-control valor"
                   value="<?php echo number_format($Programacao->valorParcela, 2, ',', '.') ?>"
                   placeholder="Valor" required>
        <?php } else { ?>
            <input type="text" step="0,01" name="valor" id="valor" data-role="valor"
                   class="form-control valor"
                   value="<?php echo number_format($Rateio_programacao->valor, 2, ',', '.') ?>"
                   placeholder="Valor" required>
        <?php } ?>
    </div>

    <div class="form-group">
        <label for="observacao">Observação</label>
                            <textarea name="observacao" id="observacao"
                                      class="form-control"><?php echo $Rateio_programacao->observacao ?></textarea>
    </div>
    <?php
    if ($this->getParam('modal')) { ?>
        <input type="hidden" name="modal" value="1"/>
        <div class="text-right">
            <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
               data-toggle="tooltip2" title="Voltar" onclick="Navegar('','back')">Cancelar</a>
            <input type="submit" class="btn btn-primary save_rat" value="salvar"
                   onclick="EnviarContas('form','pagar')">
        </div>
    <?php } else { ?>
        <div class="text-right">
            <a href="<?php echo $this->Html->getUrl('Rateio_programacao', 'all') ?>"
               class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary save_rat" value="salvar">
        </div>
    <?php } ?>
</form>

<?php if ($this->getParam('programacao')) { ?>
    <script>
        $(function () {
            ChecarRateios(<?php echo $this->getParam('programacao'); ?>, "programacao");
        })
    </script>
<?php }
?>
<script type="text/javascript">

    $(".empresa").change(function () {
        $(".emp").val($(this).val());
        $.ajax({
            type: "POST",
            url: root + "/Empresa_custo/combo",
            data: "id=" + $(this).val(),
            success: function (txt) {
                $(".centro_custo").html(txt);
            }
        });
    });
    $(document).ready(function () {
        $(".centro_custo").change(function () {
            ChecarDuplicidade($(".programacao").val(), $(".emp").val(), $(this).val(), "programacao");
        })
    });


</script>