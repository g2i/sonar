<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Rateio</h2>
        <ol class="breadcrumb">
            <li>Programação</li>
            <li class="active">
                <strong>Deletar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form class="form" method="post"
                      action="<?php echo $this->Html->getUrl('Rateio_programacao', 'delete') ?>">
                    <h1>Confirmação</h1>

                    <div class="well well-lg">
                        <p>Voce tem certeza que deseja excluir o Rateio_programacao
                            <strong><?php echo $Rateio_programacao->id; ?></strong>?</p>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Rateio_programacao->id; ?>">

                    <div class="row">
                        <?php if ($this->getParam('modal') == 1) { ?>
                            <input type="hidden" name="modal" value="1"/>
                            <div class="text-right">
                                <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
                                   data-toggle="tooltip2" title="Voltar" onclick="Navegar('','back')">
                                    Cancelar</a>
                                <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-danger"
                                       value="Excluir">
                            </div>
                        <?php } else { ?>
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Rateio_programacao', 'all') ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-danger" value="Excluir">
                            </div>
                        <?php } ?>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>