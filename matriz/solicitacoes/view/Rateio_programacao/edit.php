<form method="post" role="form"
      action="<?php echo $this->Html->getUrl('Rateio_programacao', 'edit') ?>">
    <div class="alert alert-info">Os campos marcados com <span
            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
    </div>
    <div class="form-group">
        <label for="contas_receber">Programação</label>
        <input type="hidden" name="programacao"
               value="<?php echo $Rateio_programacao->programacao ?> "/>
        <input type="text" class="form-control"
               value="<?php echo $Rateio_programacao->getProgramacao()->getPlanocontas()->nome . ' - ' . $Rateio_programacao->getProgramacao()->diaVencimento; ?>"
               disabled/>
    </div>

    <div class="form-group">
        <label for="data_documento">Data Documento</label>
        <input type="date" class="form-control" id="data_documento" name="data_documento" readonly
               value="<?php echo $Rateio_programacao->data_documento ?>">
    </div>

    <div class="form-group">
        <label for="empresa">Empresa</label>
        <input type="hidden" name="empresa" value="<?php echo $Rateio_programacao->empresa ?> "/>
        <input type="text" class="form-control"
               value="<?php echo $Rateio_programacao->getContabilidade()->nome; ?>" disabled/>
    </div>

    <div class="form-group">
        <label for="plano_contas_id" class="required">Plano de contas <span
                class="glyphicon glyphicon-asterisk"></span> </label>
        <select name="plano_contas_id" class="form-control" id="plano_contas_id" readonly>
            <?php
            foreach ($Planocontas as $p) {
                if ($p->id == $Rateio_programacao->plano_contas_id)
                    echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                else
                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
            }
            ?>
        </select>
    </div>

    <?php if ($Rateio_programacao->getProgramacao()->tipo == "A pagar"): ?>
        <div class="form-group">
            <label for="fornecedor_id" class="required">Credor <span
                    class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="fornecedor_id" class="form-control" id="fornecedor_id" readonly>
                <?php
                foreach ($Fornecedor as $f) {
                    if ($f->id == $Rateio_programacao->fornecedor_id)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
            </select>
        </div>
    <?php endif ?>

    <?php if ($Rateio_programacao->getProgramacao()->tipo == "A receber"): ?>
        <div class="form-group">
            <label for="cliente_id" class="required">Cliente <span
                    class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="cliente_id" class="form-control" id="cliente_id" readonly>
                <?php
                foreach ($Cliente as $c) {
                    if ($c->id == $Rateio_programacao->cliente_id)
                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                    else
                        echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                }
                ?>
            </select>
        </div>
    <?php endif ?>

    <div class="form-group">
        <label class="required" for="valor">Valor <span class="glyphicon glyphicon-asterisk"></span></label>
        <input type="text" step="0,01" name="valor" id="valor" class="form-control"
               value="<?php echo number_format($Rateio_programacao->valor, 2, ',', '.') ?>"
               placeholder="Valor" required readonly>
    </div>

    <div class="form-group">
        <label for="observacao">Observação</label>
                            <textarea name="observacao" id="observacao"
                                      class="form-control"><?php echo $Rateio_programacao->observacao ?></textarea>
    </div>


    <input type="hidden" name="id" value="<?php echo $Rateio_programacao->id; ?>">

    <div class="row">
        <?php if ($this->getParam('modal') == 1) { ?>
            <input type="hidden" name="modal" value="1"/>

            <div class="text-right">
                <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
                   data-toggle="tooltip2" title="Voltar" onclick="Navegar('','back')">Cancelar</a>
                <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                       value="salvar">
            </div>
        <?php } else { ?>
            <div class="text-right">
                <a href="<?php echo $this->Html->getUrl('Rateio_programacao', 'all') ?>"
                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                <input type="submit" class="btn btn-primary" value="salvar"/>
            </div>
        <?php } ?>
    </div>

</form>