<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">

                <form method="post" role="form"
                      action="<?php echo $this->Html->getUrl('Programacao', 'addContasPagar') ?>"
                      id="formGerar">
                    <input type="hidden" name="formulario" value="gerar">

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="valorParcela" class="required">Valor da Parcela <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="valorParcela" id="valorParcela"
                                       value='0.00' class="form-control money2"
                                       placeholder="Valor da Parcela" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idPlanoContas" class="required">Plano de contas <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas"
                                    required="required">
                                <option></option>
                                <?php foreach ($Planocontas as $p): ?>
                                    <option value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoDocumento" class="required">Tipo de Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento"
                                    required="required">
                                <option></option>
                                <?php
                                foreach ($tipoDocumento as $t) {
                                    if ($Config_contas->documento_pagar == $t->id) {
                                        echo '<option value="' . $t->id . '" selected>' . $t->descricao . '</option>';
                                    } else {
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="numero_documento" >Número documento</label>
                            <input type="text" name="mumero_documento" id="mumero_documento" class="form-control " placeholder="Número do documento" >
                        </div>
                    </div>
                    
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoPagamento">Forma de Pagamento</label>
                            <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento"
                                    required="required">
                                <option></option>
                                <?php
                                foreach ($tipoPagamento as $t) {
                                    if ($Config_contas->pagamento_pagar == $t->id) {
                                        echo '<option value="' . $t->id . '" selected>' . $t->descricao . '</option>';
                                    } else {
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idFornecedor" class="required">Credor <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax" required></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Primeiro Vencimento<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="primeiro_vencimento" id="primeiro_vencimento" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Data do Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="data_documento" id="data_documento" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="diaVencimento" class="required">Dia do Vencimento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input required type="number" name="diaVencimento" max="30" required="required"
                                   id="diaVencimento" onkeydown='return SomenteNumero($(this).val())' class="form-control">
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6" id="divMesVencimento" style="display: none;">
                        <div class="form-group">
                            <label for="mesVencimento">Mês do Vencimento</label>
                            <input type="number" name="mesVencimento" id="mesVencimento"
                                   onkeydown='return SomenteNumero($(this).val())' class="form-control">
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="periodicidade">Periodicidade</label>
                            <select id="periodicidade" name="periodicidade" required="required"
                                    class="form-control selectPicker">
                                <option></option>
                                <?php foreach ($Periodicidade as $p): ?>
                                    <option value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="contabilidade" class="required">Empresas <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="contabilidade" class="form-control selectPicker" required="required"
                                    id="contabilidade">
                                <option></option>
                                <?php foreach ($Contabilidades as $c): ?>
                                    <option value="<?php echo $c->id ?>"><?php echo $c->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="prazoDeterminado">Prazo Determinado?</label> <input type='radio'
                                                                                        name='prazoDeter' value='1'>
                        Sim <input checked type='radio' name='prazoDeter' value='0'> N&atilde;o
                        <div id='prazoDeter' style='display:none'>
                            <input type="number" name="prazoDeterminado"
                                   id="prazoDeterminado" class="form-control" placeholder="Número de Parcelas">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                            <textarea name="complemento" id="complemento" class="form-control"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="rateio_automatico" id="rateio_automatico"/>

                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Programacao', 'contasPagar') ?>"
                           class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" onclick="return VerificarMes()" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#periodicidade').change(function () {
            if ($(this).find('option:selected').val() == '4') {
                $('#divMesVencimento').css('display', 'block');
            } else {
                $('#divMesVencimento').css('display', 'none');
                $('#mesVencimento').val('');
            }
        });

        $('input[name=prazoDeter]').change(function () {
            if ($('input[name=prazoDeter]:checked').val() == 1) {
                $('#prazoDeter').show(500);
            } else {
                $('#prazoDeterminado').val("");
                $('#prazoDeter').hide(500);
            }
        });
    });

    function VerificarMes() {
        if ($("#periodicidade").val() == 4) {
            if ($('#mesVencimento').val() == "") {
                OpenMensagem("Alerta", "Selecione um mes de vencimento!");
                return false;
            }
        }
        return true;
    }

    $("#idPlanoContas").change(function () {
        $.ajax({
            type: "POST",
            url: root + "/Planocontas/automatico",
            data: "id=" + $(this).val(),
            success: function (txt) {
                if (txt == 1) {
                    $("#rateio_automatico").val(1);
                } else {
                    $("#rateio_automatico").val(2);
                }
            }
        });
    });

    $("#diaVencimento").keyup(function () {
        if ($(this).val() > 30) {
            OpenMensagem("Alerta", "O valor nao pode ser superior a 30");
            $(this).val("");
        }
    })

    $("#mesVencimento").keyup(function () {
        if ($(this).val() > 12) {
            OpenMensagem("Alerta", "O mês de vencimento nao pode ser superior a 12");
            $(this).val("");
        }
    })
</script>