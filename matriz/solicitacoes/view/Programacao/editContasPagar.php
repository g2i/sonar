<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">

                <div class="col-md-12">
                    <div class="row pull-right">
                        <p class="text-right ">
                            <?php
                            echo $this->Html->getLink('<span class="glyphicon centro_view"></span> Rateio ', 'Rateio_programacao', 'all', array('programacao' => $Programacao->id, 'modal' => 1, 'ajax' => true, 'first' => 1), array('class' => 'btn btn-info btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-modal-lg'));
                            ?>
                        </p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <form method="post" role="form"
                      action="<?php echo $this->Html->getUrl('Programacao', 'editContasPagar') ?>" id="formGerar">
                    <input type="hidden" name="formulario" value="gerar">

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="valorParcela">Valor da Parcela</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="valorParcela" id="valorParcela"
                                       value='<?php echo $Programacao->valorParcela ?>' class="form-control money2"
                                       placeholder="Valor da Parcela">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idPlanoContas">Plano de contas</label>
                            <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas">
                                <option></option>
                                <?php foreach ($Planocontas as $p):
                                    $aux = $p->id == $Programacao->idPlanoContas ? ' selected' : ''; ?>
                                    <option <?php echo $aux ?> value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoDocumento">Tipo de Documento</label>
                            <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento">
                                <option></option>
                                <?php foreach ($tipoDocumento as $t):
                                    $aux = $t->id == $Programacao->tipoDocumento ? ' selected' : ''; ?>
                                    <option <?php echo $aux ?> value="<?php echo $t->id ?>"><?php echo $t->descricao ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="numero_documento" >Número documento</label>
                            <input type="text" name="numero_documento" id="numero_documento" class="form-control " placeholder="Número do documento" <?= $Programacao->numero_documento ?> >
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoPagamento">Forma de Pagamento</label>
                            <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento">
                                <option></option>
                                <?php foreach ($tipoPagamento as $t):
                                    $aux = $t->id == $Programacao->tipoPagamento ? ' selected' : ''; ?>
                                    <option <?php echo $aux ?> value="<?php echo $t->id ?>"><?php echo $t->descricao ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idFornecedor">Credor</label>
                            <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax" required>
                                <?php if (!empty($forncedorSelect)) echo '<option value="' . $forncedorSelect->id . '">' . $forncedorSelect->nome . '</option>'; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Primeiro Vencimento<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="primeiro_vencimento" id="primeiro_vencimento" value='<?php echo $Programacao->primeiro_vencimento ?>' required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Data do Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="data_documento" id="data_documento"  value='<?php echo $Programacao->data_documento ?>' required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="diaVencimento">Dia do Vencimento</label>
                            <input type="number" name="diaVencimento" max="30"
                                   value='<?php echo $Programacao->diaVencimento ?>' id="diaVencimento"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="periodicidade">Periodicidade</label>
                            <select id="periodicidade" name="periodicidade" class="form-control selectPicker">
                                <option></option>
                                <?php foreach ($Periodicidade as $p):
                                    $aux = $p->id == $Programacao->periodicidade ? ' selected' : ''; ?>
                                    <option <?php echo $aux ?> value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="contabilidade">Empresas</label>
                            <select name="contabilidade" class="form-control selectPicker" id="contabilidade">
                                <option></option>
                                <?php foreach ($Contabilidades as $c):
                                    $aux = $c->id == $Programacao->contabilidade ? ' selected' : ''; ?>
                                    <option <?php echo $aux ?> value="<?php echo $c->id ?>"><?php echo $c->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row text-right pull-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Adicionar Dedu&ccedil;&atilde;o', 'Programacao', 'addDeducaoPagar', array('programacao' => $Programacao->id), array('class' => 'btn btn-primary', "data-toggle" => 'modal')); ?></p>
                        </div>
                        <!-- tabela de resultados -->
                        <div class="row clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Dedu&ccedil;&atilde;o</th>
                                        <th>Valor</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <?php
                                    foreach ($Deducao as $d) {
                                        echo "<tr>";
                                        if ($Programacao->id == $d->programacao_id) {
                                            echo "<td>" . $d->getDeducoes()->nome . "</td>";
                                            echo "<td>" . number_format($d->valor, 2, ',', '.') . "</td>";
                                            echo '<td width="50">';
                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Programacao', 'editDeducaoPagar', array('id' => $d->id), array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                            echo '</td>';
                                            echo '<td width="50">';
                                            //echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Contasreceber', 'delete', array('id' => $d->id), array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                            echo '</td>';
                                        }
                                        echo "</tr>";
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="prazoDeterminado">Prazo Determinado?</label>
                        <input <?php echo (!empty($Programacao->prazoDeterminado)) ? "checked" : "" ?> type='radio'
                                                                                                       name='prazoDeter'
                                                                                                       value='1'>
                        Sim <input <?php echo (empty($Programacao->prazoDeterminado)) ? "checked" : "" ?>
                            type='radio' name='prazoDeter' value='0'> N&atilde;o
                        <div
                            id='prazoDeter' <?php echo (empty($Programacao->prazoDeterminado)) ? "style='display: none'" : "" ?>>
                            <input type="number" name="prazoDeterminado"
                                   value='<?php echo $Programacao->prazoDeterminado ?>' id="prazoDeterminado"
                                   class="form-control" placeholder="Número de Parcelas">
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                                <textarea name="complemento" id="complemento"
                                          class="form-control"><?php echo $Programacao->complemento ?></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Programacao->id; ?>">

                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Programacao', 'contasPagar') ?>"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('input[name=prazoDeter]').change(function () {
            if ($('input[name=prazoDeter]:checked').val() == 1) {
                $('#prazoDeter').show(500);
            } else {
                $('#prazoDeterminado').val("");
                $('#prazoDeter').hide(500);
            }
        });
    });

</script>