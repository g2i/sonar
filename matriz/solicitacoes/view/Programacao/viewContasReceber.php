<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Periodicidade</strong>: <?php echo $Programacao->periodicidade; ?></p>

                <p><strong>PrazoDeterminado</strong>: <?php echo $Programacao->prazoDeterminado; ?></p>

                <p><strong>ValorParcela</strong>: <?php echo $Programacao->valorParcela; ?></p>

                <p><strong>IdCliente</strong>: <?php echo $Programacao->idCliente; ?></p>

                <p><strong>IdFornecedor</strong>: <?php echo $Programacao->idFornecedor; ?></p>

                <p><strong>Contabilidade</strong>: <?php echo $Programacao->contabilidade; ?></p>

                <p><strong>Complemento</strong>: <?php echo $Programacao->complemento; ?></p>

                <p><strong>DiaVencimento</strong>: <?php echo $Programacao->diaVencimento; ?></p>

                <p><strong>Status</strong>: <?php echo $Programacao->status; ?></p>

                <p><strong>AVista</strong>: <?php echo $Programacao->aVista; ?></p>

                <p><strong>Tipo</strong>: <?php echo $Programacao->tipo; ?></p>

                <p><strong>IdPlanoContas</strong>: <?php echo $Programacao->idPlanoContas; ?></p>
            </div>
        </div>
    </div>
</div>