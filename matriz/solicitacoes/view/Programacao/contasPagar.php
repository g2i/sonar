<style>
    .col-opcoes{
        min-width: 90px; !important;
    }
    tr > td > button {
        margin-left: 2px;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>Contas a Pagar</li>
            <li class="active">
                <strong>Programação</strong>
            </li>
        </ol>
    </div>
    <div style="margin-top: 100px">
        <form id="frmPesquisa" class="form-inline">
            <label class="sr-only" for="search">Pesquisar</label>
            <div class='col-xs-12 col-sm-6 col-md-3'>
                <label for="fornecedorOpt" style="display: block">Credor</label>
                <select id="fornecedorOpt" name="fornecedorOpt" class="form-control credor-ajax"></select>
            </div>
            <div class='col-xs-12 col-sm-6 col-md-3'>
                <label for="planoContasOpt" style="display: block">Plano de Contas</label>
                <select class="form-control selectPicker" name="planoContasOpt" id="planoContasOpt"
                        style="max-width: 100%">
                    <option></option>
                    <?php foreach ($PlanoContas as $pc): ?>
                        <option value="<?php echo $pc->id ?>"><?php echo $pc->nome ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class='col-xs-12 col-sm-6 col-md-3'>
                <label for="contabilidade">Empresas</label>
                <select id="contabilidade" class="form-control selectPicker" name="contabilidade"
                        style="max-width: 100%">
                    <option></option>
                    <?php foreach ($contabilidadeCombo as $c): ?>
                        <option value="<?php echo $c->id ?>"><?php echo $c->nome ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class='col-xs-12 col-sm-6 col-md-3'>
                <label for="periodicidadeOpt" style="display: block">Periodicidade</label>
                <select class="form-control selectPicker" name="periodicidadeOpt" id="periodicidadeOpt"
                        style="max-width: 100%">
                    <option></option>
                    <?php foreach ($Periodicidade as $p): ?>
                        <option value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class='col-xs-12'>
                <button id="btnPesquisar" style="margin-top: 23px" data-role="tooltip" data-placement="bottom" title="Pesquisar" type="button"
                        class="btn btn-default"><i class="fa fa-search"></i>
                </button>
                <button id="btnReset" style="margin-top: 23px" data-role="tooltip" data-placement="bottom" title="Limpar" type="button"
                        class="btn btn-default"><i class="fa fa-refresh"></i>
                </button>
                <button id="btnImprimir" style='margin-top:23px;' data-role="tooltip" data-placement="bottom" title="Gerar Relatório" type="button"
                        class="btn btn-default"><i class="fa fa-print"></i>
                </button>
            </div>
            <div style="clear: both"></div>
        </form>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row text-right pull-right">
                    <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Cadastrar Programa&ccedil;&atilde;o', 'Programacao', 'addContasPagar', NULL, array('class' => 'btn btn-primary')); ?></p>
                </div>
                <div class="row clearfix">
                    <div class="table-responsive">
                        <table id="tblProgramacao" class="table table-hover">
                            <thead>
                            <tr>
                                <th data-column-id="id" data-identifier="true" data-visible="false"
                                    data-visible-in-selection="false">Id</th>
                                <th data-column-id="credor">Credor</th>
                                <th data-column-id="empresa">Empresa</th>
                                <th data-column-id="plano">Plano de Contas</th>
                                <th data-column-id="periodo">Periodicidade</th>
                                <th data-column-id="dia">Dia do Vencimento</th>
                                <th data-column-id="valor" data-converter="currency">Valor da Parcela</th>
                                <th data-column-id="status">Status</th>
                                <th data-column-id="data_documento" data-converter="date">Data Documento</th>
                                <th data-column-id="primeiro_vencimento" data-converter="date">Primeiro Vencimento</th>
                                <th data-column-id="opcoes" data-formatter="opcoes"
                                    data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                    data-sortable="false"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var valorTotal = 0;

    $(document).ready(function () {
        $('[data-role="tooltip"]').tooltip();

        var grid = $('#tblProgramacao').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: -1,
            ajax: true,
            url: root + '/Programacao/getContasPagar',
            ajaxSettings: {
                method: "POST",
                cache: true
            },
            converters:{
                currency: {
                    from: function (value) { return value; },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return 'R$ ' + $.number(value, 2);
                    }
                },
                date: {
                    from: function (value) {
                        if(value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                    }
                }
            },
            formatters:{
                opcoes: function(column, row){
                    return '<button type="button" data-toggle="tooltip" data-placement="top" title="Rateio" class="btn btn-xs btn-default command-rateio" data-row-id="' + row.id + '"><span class="fa fa-share-alt-square"></span></button>' +
                        '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                        '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                }
            },
            requestHandler: function (request) {
                request.planoContasOpt = $('#planoContasOpt').val();
                request.periodicidadeOpt = $('#periodicidadeOpt').val();
                request.contabilidade = $('#contabilidade').val();
                request.fornecedorOpt = $('#fornecedorOpt').val();

                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                valorTotal = 0;
                return request;
            },
            responseHandler: function (response){
                valorTotal = response.valorTotal;
                return response;
            },
            templates: {
                search: ""
            }
        });

        grid.on('load.rs.jquery.bootgrid', function(){
            var footer = grid.find('tfoot');
            footer.remove();
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {

            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-rateio").unbind('click');
            grid.find(".command-rateio").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Rateio_programacao/all?programacao=' + conta +'&modal=1&ajax=true&first=1';
                Acrescentar(url)
                $('<div></div>').load(url, function(){
                    BootstrapDialog.show({
                        title: 'Rateio Programa\u00e7\u00e3o',
                        message: this,
                        type: BootstrapDialog.TYPE_DEFAULT,
                        buttons: [{
                            label: 'Fechar',
                            icon: 'fa fa-times-circle',
                            cssClass: 'btn-danger',
                            action: function (dialog) {
                                dialog.close();
                            }
                        }]
                    });
                });
            });

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = '<?php echo $this->Html->getUrl('Programacao', 'editContasPagar') ?>?id=' + conta;
                document.location = url;
            });

            grid.find(".command-delete").unbind('click');
            grid.find(".command-delete").on("click", function(e) {
                var conta = $(this).data("row-id");

                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ? Todas as contas pendentes geradas a partir desta data serão excluidas!',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function(result) {
                        if(result) {
                            var url = root + '/Programacao/delete';
                            var data = {
                                id: conta
                            }

                            $.post(url, data, function(ret){
                                if(ret.result){
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                }else{
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });

            var footer = $('<tfoot></tfoot>');
            var sumario = $('<tr></tr>');

            sumario.append($('<td colspan="5" style="font-weight: bold">Total:</td>'));
            sumario.append($('<td style="font-weight: bold">R$ ' + $.number(valorTotal, 2) + '</td>'));
            sumario.append($('<td colspan="4"></td>'));
            footer.append(sumario);
            grid.append(footer);
        });

        $('#btnPesquisar').click(function(){
            grid.bootgrid('reload');
        });

        $('#btnReset').click(function(){
            $('#contabilidade').val("").trigger('change');
            $('#planoContasOpt').val("").trigger('change');
            $('#periodicidadeOpt').val("").trigger('change');
            $('#fornecedorOpt').val("").trigger('change');
        });

        $('#btnImprimir').click(function(){
            var data = {
                fornecedor: $("#fornecedorOpt").val(),
                plano: $("#planoContasOpt").val(),
                per: $("#periodicidadeOpt").val()
            }

            if (data.fornecedor == "0" || data.fornecedor == null) {
                data.fornecedor = -1;
            }
            if (data.plano == "0" || data.plano == '') {
                data.plano = -1;
            }
            if (data.per == "0" || data.per == '') {
                data.per = -1;
            }

            $.ReportPost('cp_prog', data);
        });
    });
</script>