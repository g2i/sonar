<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Deduções Contas a Receber</h2>
        <ol class="breadcrumb">
            <li>deduções contas receber</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form method="post" role="form"
                      action="<?php echo $this->Html->getUrl('DeducoesContasreceber', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group">
                            <label for="percentual">Percentual</label>
                            <input type="text" name="percentual" id="percentual" class="form-control"
                                   value="<?php echo $DeducoesContasreceber->percentual ?>" placeholder="Percentual">
                        </div>
                        <div class="form-group">
                            <label for="deducoes_id">Deducoes</label>
                            <select name="deducoes_id" class="form-control" id="deducoes_id">
                                <?php
                                foreach ($Deducoes as $d) {
                                    if ($d->id == $DeducoesContasreceber->deducoes_id)
                                        echo '<option selected value="' . $d->id . '">' . $d->nome . '</option>';
                                    else
                                        echo '<option value="' . $d->id . '">' . $d->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="contasreceber_id">Contasreceber</label>
                            <select name="contasreceber_id" class="form-control" id="contasreceber_id">
                                <?php
                                foreach ($Contasreceberes as $c) {
                                    if ($c->id == $DeducoesContasreceber->contasreceber_id)
                                        echo '<option selected value="' . $c->id . '">' . $c->documento . '</option>';
                                    else
                                        echo '<option value="' . $c->id . '">' . $c->documento . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('DeducoesContasreceber', 'all') ?>"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>