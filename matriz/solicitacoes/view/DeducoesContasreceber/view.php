<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Dedu��es Contas a Receber</h2>
        <ol class="breadcrumb">
            <li>dedu��es contas receber</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Percentual</strong>: <?php echo $DeducoesContasreceber->percentual; ?></p>

                <p>
                    <strong>Deducoes</strong>:
                    <?php
                    echo $this->Html->getLink($DeducoesContasreceber->getDeducoes()->nome, 'Deducoes', 'view',
                        array('id' => $DeducoesContasreceber->getDeducoes()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Contasreceber</strong>:
                    <?php
                    echo $this->Html->getLink($DeducoesContasreceber->getContasreceber()->documento, 'Contasreceber', 'view',
                        array('id' => $DeducoesContasreceber->getContasreceber()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>