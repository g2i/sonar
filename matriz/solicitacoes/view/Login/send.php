<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Login</h2>
        <ol class="breadcrumb">
            <li> login</li>
            <li class="active">
                <strong>Recuperar Senha</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="row" style="max-width: 350px; margin: auto">
                    <form class="form" method="post">
                        <div class="form-group">
                            <label>E-mail:</label>
                            <input type="text" name="email" class="form-control" placeholder="Email" autofocus="">

                        </div>
                        <div class="form-group">
                            <input type="submit" value="Enviar" class="btn btn-primary btn-block">
                        </div>
                        <p><span
                                class="small">* Você receberá no seu e-mail um link para cadastrar uma nova senha.</span>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>