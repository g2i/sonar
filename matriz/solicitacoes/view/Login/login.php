
<div class="box-login">
    <form action="" id="formLogin" method="POST" autocomplete="off">
        <input type="hidden" name="redirect" value="<?php echo $redirect ?>">
        <div class="col-md-12 form-group">
            <label for="usuario">Usuário: </label>
            <div class="offinput">
                <input type="text" name="login" placeholder="Nome de usuário"/>
                <span><img src="<?php echo SITE_PATH . "/img/bg_form_user.png"; ?>" alt=""/></span>
            </div>
        </div>

        <div class="col-md-12 form-group">
            <label for="senha">Senha: </label>
            <div class="offinput">
                <input type="password" name="password" placeholder="Palavra passe"/>
                <span><img src="<?php echo SITE_PATH . "/img/bg_form_password.png"; ?>" alt=""/></span>
            </div>
        </div>

        <div class="col-md-12 form-group text-right">
            <input class="btn btn-primary" type="submit" value="Entrar"/>
        </div>

        <div class="clearfix"></div>
    </form>
    <div class="clearfix"></div>
</div>
