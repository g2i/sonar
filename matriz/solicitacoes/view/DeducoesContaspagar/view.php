<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Dedu��es Contas a Pagar</h2>
        <ol class="breadcrumb">
            <li>dedu��es contas pagar</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Valor</strong>: <?php echo $DeducoesContaspagar->valor; ?></p>

                <p>
                    <strong>Deducoes</strong>:
                    <?php
                    echo $this->Html->getLink($DeducoesContaspagar->getDeducoes()->nome, 'Deducoes', 'view',
                        array('id' => $DeducoesContaspagar->getDeducoes()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Contaspagar</strong>:
                    <?php
                    echo $this->Html->getLink($DeducoesContaspagar->getContaspagar()->id, 'Contaspagar', 'view',
                        array('id' => $DeducoesContaspagar->getContaspagar()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>