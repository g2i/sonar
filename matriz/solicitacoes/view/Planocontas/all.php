<style>
    .classificacao{
        width: 200px;
        margin: 0 5px 0 0;
        display: inline-block;
        vertical-align: middle
    }
    .topbar{
        width: auto;
        margin: 0 5px 0 0;
        display: inline-block;
        vertical-align: middle
    }
    .col-opcoes{
        min-width: 90px; !important;
        max-width: 90px; !important;
        width: 90px; !important;
    }
    tr > td > button {
        margin-left: 2px;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Plano de Contas</h2>
        <ol class="breadcrumb">
            <li> plano de contas</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <!-- tabela de resultados -->
                <div class="row clearfix">
                    <div class="table-responsive">
                        <table id="tblPlanos" class="table table-hover">
                            <thead>
                            <tr>
                                <th data-column-id="id" data-identifier="true" data-visible="false"
                                    data-visible-in-selection="false">Id</th>
                                <th data-column-id="nome">Nome</th>
                                <th data-column-id="classificacao">Classfica&ccedil;&atilde;o</th>
                                <th data-column-id="status">Status</th>
                                <th data-column-id="rateio">Rateio Autom&aacute;tico</th>
                                <th data-column-id="opcoes" data-formatter="opcoes"
                                    data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                    data-sortable="false"></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function () {
        var grid = $('#tblPlanos').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: [30, 50, 100],
            ajax: true,
            url: root + '/Planocontas/getPlanos',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            formatters:{
                opcoes: function(column, row){
                    return  '<button type="button" data-toggle="tooltip" data-placement="top" title="Rateio" class="btn btn-xs btn-default command-rateio" data-row-id="' + row.id + '"><span class="fa fa-share-alt-square"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                }
            },
            labels: {
                search: 'Procura por nome'
            },
            templates:{
                search: '<div class="pull-left form-group topbar">' +
                        '<input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div>' +
                        '<div class="pull-left form-group text-left classificacao">' +
                        '<select name="classificacao" id="classificacao" style="height: 34px;">' +
                        '<option value=""></option>' +
                        <?php foreach ($Classificacao as $c): ?>
                        '<option value="<?php echo $c->id ?>"><?php echo $c->descricao ?></option>' +
                        <?php endforeach; ?>
                        '</select></div><div class="pull-left form-group topbar">' +
                        '<select name="tipo" id="tipo" style="height: 34px;">' +
                        '<option value=""></option><option value="1">Entrada</option>' +
                        '<option value="2">Sa&iacute;da</option></select></div>' +
                        '<div class="pull-left form-group topbar">' +
                        '<button id="btnReset" type="button" data-role="tooltip" data-placement="bottom" title="Limpar Campos" class="btn btn-default"><i class="fa fa-refresh"></i></button></div>'+
                        '<button id="btnNovo" class="btn btn-primary" style="width: auto; margin: 0 5px 0 0; display: inline-block; vertical-align: middle" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Cadastrar Plano de Contas</button>',
            },
            requestHandler: function (request) {
                request.classificacao = $('#classificacao').val();
                request.tipo = $('#tipo').val();
                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                return request;
            }
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {
            $('[data-tooltip="tooltip"]').tooltip();

            grid.find(".command-rateio").unbind('click');
            grid.find(".command-rateio").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Plano_centrocusto/all?id=' + conta +'&modal=1&ajax=true&first=1';
                showOnModal(url, 'Rateio Plano de Contas');
            });

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Planocontas/edit?id=' + conta;
                window.open(url);
            });

            grid.find(".command-delete").unbind('click');
            grid.find(".command-delete").on("click", function(e) {
                var conta = $(this).data("row-id");

                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function(result) {
                        if(result) {
                            var url = root + '/Planocontas/delete';
                            var data = {
                                id: conta
                            }

                            $.post(url, data, function(ret){
                                if(ret.result){
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                }else{
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });

            $('#btnNovo').unbind('click');
            $('#btnNovo').click(function(e){
                var url = root + '/Planocontas/add';
                window.open(url);
            });

            $('#btnReset').unbind('click');
            $('#btnReset').click(function(){
                $('#search').val("").trigger('change');
                $('#classificacao').val("").trigger('change');
                $('#tipo').val("").trigger('change');
                grid.bootgrid('reload');
            });

            $('#classificacao').select2({
                language: 'pt-BR',
                theme: 'bootstrap',
                width: '100%',
                placeholder: 'Selecione:'
            }).change(function(){
                grid.bootgrid('reload');
            });

            $('#tipo').select2({
                language: 'pt-BR',
                theme: 'bootstrap',
                width: '100%',
                placeholder: 'Selecione:'
            }).change(function(){
                grid.bootgrid('reload');
            });
        });
    });
</script>