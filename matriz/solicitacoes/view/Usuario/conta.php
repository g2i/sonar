<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Usuario</h2>
        <ol class="breadcrumb">
            <li>Conta</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-md-12">

                    <div class="col-md-4">
                        <div class="ibox float-e-margins panel">
                            <div class="ibox-title">
                                <h5>Detalhes Conta</h5>
                            </div>
                            <div>
                                <div class="ibox-content no-padding border-left-right">
                                    <?php if (empty($Usuario->imagem)) { ?>
                                        <img alt="image" class="img-responsive"
                                             src="<?php echo SITE_PATH; ?>/img/empty-avatar.jpg">
                                    <?php } else { ?>
                                        <img alt="image" class="img-responsive" src="<?php echo $Usuario->imagem; ?>">
                                    <?php } ?>
                                </div>

                                <div class="ibox-content profile-content">
                                    <h4><strong><?php echo $Usuario->nome; ?></strong></h4>

                                    <p><i class="fa fa-map-marker"></i> Login: <?php echo $Usuario->login; ?></p>

                                    <div class="user-button">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php echo $this->Html->getLink('<i class="fa fa-key"></i> Alterar Senha', 'Usuario', 'alterar_senha',array(),array('class' => 'btn btn-primary btn-sm btn-block', 'data-toggle' => 'modal'));
?>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="button" class="btn btn-default btn-sm btn-block"><i
                                                        class="fa fa-image"></i> Alterar Imagem
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Informa&ccedil;&otilde;es adicionais</h5>
                            </div>
                            <div class="ibox-content">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
