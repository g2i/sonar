<style type="text/css">
    .nav-tabs > li > a {
        border: 1px solid #ddd;
    }

    .nav-tabs {
        border-bottom: none;
    }
</style>
<?php if ($this->getParam('modal')): ?>
    <style>
        .select2-close-mask {
            z-index: 2099;
        }

        .select2-dropdown {
            z-index: 3051;
        }

        .has-error .select2-selection {
            border: 1px solid #a94442;
            border-radius: 4px;
        }

        #labelSolicitacao {
            text-align: center;
        }
    </style>
<?php else: ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Solicitação: <?php echo $Solicitacao->id ?></h2>
        </div>
    </div>
<?php endif; ?>
<div class="col-md-6">
    <div class="form-group">
        <br>

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="post" role="form" action="<?php echo $this->Html->getUrl('Solicitacao', 'edit') ?>">
            <input type="hidden" name="id" value="<?php echo $Solicitacao->id; ?>">
            <div class="ibox">
                <div class="ibox-title " style="border-color: #00c0ef;">
                    <h2 style="display: inline-block">Informações da Solicitação</h2>
                    <div style="float:right">
                        <?php
                        if ($Solicitacao->situacao_solicitacao_id != 3) {
                            echo $this->Html->getLink('<span class="img img-ocorrencia" data-toggle="tooltip" data-placement="auto"  title="Cadastrar parcelas"> Forma de Pagamento</span> ', 'Contaspagar', 'contasLista',
                                array('id' => $Solicitacao->id),
                                array('class' => 'btn btn-md btn-primary parcela', 'target' => '_blank'));
                        }
                        ?>

                    </div>
                </div>
                <div class="ibox-content">

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="solicitante">Solicitante</label>
                                    <input type="text" name="solicitante" id="solicitante"
                                           class="form-control" value="<?php echo $Solicitacao->solicitante ?>">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="data">Data do Documento <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control dateFormat" name="data" id="data"
                                               value="<?php echo $Solicitacao->data ?>" required>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="idFornecedor">Fornecedor <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <td width="50">
                                        <button type="button" id="btnAddFornecedor" class="btn btn-info btn-xs">Novo
                                        </button>
                                    </td>
                                    <select name="idFornecedor" class="form-control selectPicker" id="idFornecedor">
                                        <option value="0">Selecione</option>
                                        <?php
                                        foreach ($Fornecedores as $p) {
                                            if ($p->id == $Solicitacao->idFornecedor)
                                                echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                                            else
                                                echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Empresas <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="contabilidade" name="contabilidade" class="form-control selectPicker">
                                        <option value="0">Selecione</option>
                                        <?php
                                        foreach ($Contabilidade as $c) {
                                            if ($c->id == $Solicitacao->contabilidade)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="responsavel">Autorizador/Responsável <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select required id="responsavel" name="responsavel"
                                            class="form-control selectPicker">
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($Usuarios as $c) {
                                            if ($c->id == $Solicitacao->responsavel)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="meio_pagamento">Meio de pagamento <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="meio_pagamento" name="meio_pagamento" class="form-control selectPicker"
                                            required>
                                        <option value="0">Normal</option>
                                        <option value="1">Cartão</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projeto">Aplicação/Projeto</label>
                                    <input type="text" name="projeto" id="projeto" class="form-control"
                                           value="<?php echo $Solicitacao->projeto; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="valor_solicitacao">Valor</label>
                                    <input type="text" name="valor_solicitacao" id="valor_solicitacao"
                                           class="form-control"
                                           value="<?php echo number_format($Solicitacao->valor_solicitacao, 2, ',', '.') ?>">
                                </div>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="observacao">Descrição</label>
                            <textarea name="observacao" id="observacao"
                                      rows="12" class="form-control"><?php echo $Solicitacao->observacao ?></textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-title " style="border-color: #f8ac59;">
                    <h2>Situação da solicitação</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cadastrado_por">Cadastrado por</label>
                                    <input disabled name="cadastrado_por" id="cadastrado_por"
                                           class="form-control"
                                           value="<?php echo $Solicitacao->getUsuario22()->nome; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="alterado_por">Modificado por</label>
                                    <input disabled name="alterado_por" id="alterado_por"
                                           class="form-control" value="<?php echo $Solicitacao->getUsuario()->nome; ?>">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="situacao_solicitacao_id">Situação</label>
                                    <input disabled name="situacao_solicitacao_id" id="situacao_solicitacao_id"
                                           class="form-control"
                                           value="<?php echo $Solicitacao->getSituacao_solicitacao()->nome; ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cadastrado_por">Cadastrado em</label>
                                    <input disabled name="cadastrado_por" id="cadastrado_por"
                                           class="form-control"
                                           value="<?php echo DataTimeBr($Solicitacao->dt_cadastro); ?>">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cadastrado_por">Alterado em</label>
                                    <input disabled name="cadastrado_por" id="cadastrado_por"
                                           class="form-control"
                                           value="<?php echo DataTimeBr($Solicitacao->dt_modificado); ?>">
                                </div>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cadastrado_por">Motivo de recusa</label>
                            <textarea disabled name="motivo_recusa" id="Motivo"
                                      class="form-control"
                                      rows="8"><?php echo $Solicitacao->motivo_recusa; ?></textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-title" style="border-color: #00a65a;">
                    <h2>Projeto</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Grupo <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_grupo_id" name="fin_projeto_grupo_id"
                                            class="form-control selectPicker"
                                            required>
                                        <option>Selecione Um Grupo</option>
                                        <?php
                                        foreach ($Fin_projeto_grupo as $c) {
                                            if ($c->id == $Fin_projeto->grupo_id)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Empresa <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_empresa_id" name="fin_projeto_empresa_id"
                                            class="form-control selectPicker"
                                            required>
                                        <?php
                                        foreach ($Fin_projeto_empresa as $c) {
                                            if ($c->id == $Fin_projeto->empresa_id)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Unidade<span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_unidade_id" name="fin_projeto_unidade_id"
                                            class="form-control selectPicker"
                                            required>
                                        <?php
                                        foreach ($Fin_projeto_unidade as $c) {
                                            if ($c->id == $Fin_projeto->unidade_id)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <?php if (!$this->getParam('modal')): ?>
                        <div class="text-right">
                            <?php if ($this->getParam('visualizar') == 40): ?>
                                <a href="<?php echo $this->Html->getUrl('Contaspagar', 'solicitacao') ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal">Cancelar</a>
                            <?php else: ?>
                                <a href="<?php echo $this->Html->getUrl('Solicitacao', 'all') ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal">Cancelar</a>
                            <?php endif; ?>
                            <?php if ($Solicitacao->situacao_solicitacao_id != 3): ?>
                                <input type="submit" class="btn btn-primary" value="Salvar">
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </form>
    </div>
</div>


<style>
    .parcela {
        font-weight: bold;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('#meio_pagamento option[value="<?php echo $Solicitacao->meio_pagamento ?>"]').prop('selected', true);
    });

    function calculaValor() {
        if ($('#multa').val() != "undefined" && $('#multa').val() != "") {
            var multa = $('#multa').unmask();
        } else {
            var multa = 0;
        }

        if ($('#desconto').val() != "undefined" && $('#desconto').val() != "") {
            var desconto = $('#desconto').unmask();
        } else {
            var desconto = 0;
        }

        if ($('#valorBruto').val() != "undefined" && $('#valorBruto').val() != "") {
            var valorBruto = $('#valorBruto').unmask();
        } else {
            var valorBruto = 0;
        }

        if ($('#juros').val() != "undefined" && $('#juros').val() != "") {
            var juros = $('#juros').unmask();
        } else {
            var juros = 0;
        }
        var valorLiquido = (valorBruto + juros + multa) - desconto;
        valorLiquido = Math.round(valorLiquido * 100) / 100;

        $('#valor').unpriceFormat();
        $('#valor').val(valorLiquido.toFixed(2));
        $('#valor').priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
        $('#valor').change();
    }

    $(document).ready(function () {

        $('#fin_projeto_grupo_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_empresa', 'getListaEmpresa') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_empresa_id').prop("disabled", true);
            $('#fin_projeto_unidade_id').prop("disabled", true);
            $.get(url, {fin_projeto_grupo_id: id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_empresa_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').html('').trigger('change.select2');
                $('#fin_projeto_empresa_id').prop("disabled", false);
                $('#fin_projeto_unidade_id').prop("disabled", false);
            }, 'json');
        });

        $('#fin_projeto_empresa_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_unidade', 'getListaUnidade') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_unidade_id').prop("disabled", true);
            $.get(url, {fin_projeto_empresa_id: id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_unidade_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').prop("disabled", false);
            }, 'json');
        });

        $('#btnAddFornecedor').click(function () {
            var url = root + '/Fornecedor/add/origem:-1/ajax:true/modal:1/';
            LoadGif();
            $('<div></div>').load(url, function () {
                BootstrapDialog.show({
                    title: 'Adicionar Fornecedor',
                    message: this,
                    size: 'size-wide',
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function (dialog) {
                        initeComponentes(dialog.getModalBody());
                        CloseGif();
                    }
                });
            });
        });
    });
    $('button.command-anexos').each(function () {
        let conta_id = $(this).attr('data-row-id');
        $(this).click(function () {
            var url = root + '/Anexo/all/id_externo:' + conta_id + '/modal:1/ajax:true/first:1/tipo:1/';
            showOnModal(url, 'Anexos');
        })
    })
</script>