<style type="text/css">
    .nav-tabs > li > a {
        border: 1px solid #ddd;
    }

    .nav-tabs {
        border-bottom: none;
    }
</style>
<?php if ($this->getParam('modal')): ?>
    <style>
        .select2-close-mask {
            z-index: 2099;
        }

        .select2-dropdown {
            z-index: 3051;
        }

        .has-error .select2-selection {
            border: 1px solid #a94442;
            border-radius: 4px;
        }
    </style>
<?php else: ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Solicitações</h2>
            <ol class="breadcrumb">
                <li>Solicitações</li>
                <li class="active">
                    <strong>Adicionar</strong>
                </li>
            </ol>
        </div>
    </div>
<?php endif; ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form id="frmConta" method="post" role="form" action="<?php echo $this->Html->getUrl('Solicitacao', 'add') ?>"
              id="formIndividual">
            <div class="ibox">
                <div class="ibox-title" style="border-color: #00c0ef;">
                    <h2>Informações da Solicitação</h2>
                </div>
                <div class="ibox-content">
                    <input type="hidden" name="formulario" value="individual">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="solicitante">Solicitante</label>
                                    <input type="text" name="solicitante" id="solicitante"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="data">Data do Documento <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control dateFormat" name="data" id="data"
                                               required>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="idFornecedor">Fornecedor <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <td width="50">
                                        <button type="button" id="btnAddFornecedor" class="btn btn-info btn-xs">Novo
                                        </button>
                                    </td>
                                    <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax"
                                            role="credor" required>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Empresas <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="contabilidade" name="contabilidade" class="form-control selectPicker"
                                            required>
                                        <option></option>
                                        <?php
                                        foreach ($Contabilidade as $c) {
                                            echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="responsavel">Autorizador/Responsável <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select required id="responsavel" name="responsavel" class="form-control selectPicker">
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($Usuarios as $c) {
                                            if ($c->id == $Solicitacao->responsavel)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="required" for="meio_pagamento">Meio de pagamento <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="meio_pagamento" name="meio_pagamento" class="form-control selectPicker"
                                            required>
                                        <option value="0">Normal</option>
                                        <option value="1">Cartão</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="projeto">Aplicação/Projeto</label>
                                    <input type="text" name="projeto" id="projeto"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="valor_solicitacao">Valor</label>
                                    <input type="text" name="valor_solicitacao" id="valor_solicitacao"
                                           class="form-control">
                                </div>
                            </div>
                            <!--verifica a  forma de pagamento se for igual a 0 segue o fluxo normal para o contas a pagar se for igual a 1 segue o fluxo para tela de solicitacao_quitacao financeiro -->

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="observacao">Descrição</label>
                            <textarea name="observacao" id="observacao"
                                      rows="12" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="ibox ">
                <div class="ibox-title" style="border-color: #00a65a;">
                    <h2>Projeto</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Grupo <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_grupo_id" name="fin_projeto_grupo_id"
                                            class="form-control selectPicker"
                                            required>
                                        <option>Selecione Um Grupo</option>
                                        <?php
                                        foreach ($Fin_projeto_grupo as $c) {
                                            echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Empresa <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_empresa_id" name="fin_projeto_empresa_id"
                                            class="form-control selectPicker"
                                            required>

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Unidade<span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_unidade_id" name="fin_projeto_unidade_id"
                                            class="form-control selectPicker"
                                            required>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <?php if (!$this->getParam('modal')): ?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Solicitacao', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="Continuar">
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    function calculaValor() {
        if ($('#multa').val() != "undefined" && $('#multa').val() != "") {
            var multa = $('#multa').unmask();
        } else {
            var multa = 0;
        }

        if ($('#desconto').val() != "undefined" && $('#desconto').val() != "") {
            var desconto = $('#desconto').unmask();
        } else {
            var desconto = 0;
        }

        if ($('#valorBruto').val() != "undefined" && $('#valorBruto').val() != "") {
            var valorBruto = $('#valorBruto').unmask();
        } else {
            var valorBruto = 0;
        }

        if ($('#juros').val() != "undefined" && $('#juros').val() != "") {
            var juros = $('#juros').unmask();
        } else {
            var juros = 0;
        }
        var valorLiquido = (valorBruto + juros + multa) - desconto;
        valorLiquido = Math.round(valorLiquido * 100) / 100;

        $('#valor').unpriceFormat();
        $('#valor').val(valorLiquido.toFixed(2));
        $('#valor').priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
        $('#valor').change();
    }

    $(document).ready(function () {

        $('#fin_projeto_grupo_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_empresa', 'getListaEmpresa') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_empresa_id').prop( "disabled", true );
            $('#fin_projeto_unidade_id').prop( "disabled", true );
            $.get(url, {fin_projeto_grupo_id:id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_empresa_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').html('').trigger('change.select2');
                $('#fin_projeto_empresa_id').prop( "disabled", false );
                $('#fin_projeto_unidade_id').prop( "disabled", false );
            }, 'json');
        });

        $('#fin_projeto_empresa_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_unidade', 'getListaUnidade') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_unidade_id').prop( "disabled", true );
            $.get(url, {fin_projeto_empresa_id:id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_unidade_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').prop( "disabled", false );
            }, 'json');
        });

        $('#btnAddFornecedor').click(function () {
            var url = root + '/Fornecedor/add/origem:-1/ajax:true/modal:1/';
            LoadGif();
            $('<div></div>').load(url, function () {
                BootstrapDialog.show({
                    title: 'Adicionar Fornecedor',
                    message: this,
                    size: 'size-wide',
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function (dialog) {
                        initeComponentes(dialog.getModalBody());
                        CloseGif();
                    }
                });
            });
        });
    });
</script>