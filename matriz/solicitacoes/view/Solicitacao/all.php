
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Solicitações</h2>
    <ol class="breadcrumb">
    <li>Solicitações</li>
    <li class="active">
    <strong>Listas</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="get" enctype="application/x-www-form-urlencoded">
                <div class="col-md-3 form-group">
                    <label for="data">Data Solicitação</label>
                    <input type="date" name="data" id="data" class="form-control maskData" value="<?php echo $this->getParam('data'); ?>">
                </div>                 
                <div class="col-md-3 form-group">
                    <label for="idFornecedor">Fornecedor</label>
                    <select name="idFornecedor" class="form-control select-fornecedor" id="idFornecedor">
                        <option value="">Selecione:</option>
                        <?php foreach ($Fornecedores as $f):
                            if($f->id == $this->getParam('idFornecedor'))
                                echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                            else
                                echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                        endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="situacao_solicitacao_id">Situação</label>
                    <select name="situacao_solicitacao_id" class="form-control" id="situacao_solicitacao_id">
                            <option value="">Selecione:</option>
                        <?php foreach ($Situacao_solicitacao as $f):
                            if($f->id == $this->getParam('situacao_solicitacao_id'))
                                echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                            else if($f->id == 1)
                                echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                            else
                                echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                         endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                            class="btn btn-default"
                                            data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                            title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Solicitação', 'Solicitacao', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Solicitante</th>
                <th>Data Solicitação</th>
                <th>Fornecedor</th>
                <th>Empresa</th>
                <th>Aplicação/Projeto</th>
                <th>Observacao</th>
                <th>Valor</th>
                <th>Resp. Autorizar</th>
                <th>Situação</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            <?php
            foreach ($Solicitacaos as $s) {

                if ($s->situacao_solicitacao_id == 1) {
                    $class = "bg-warning";
                } else if ($s->situacao_solicitacao_id == 2) {
                    $class = "bg-danger";
                } else if ($s->situacao_solicitacao_id == 3) {
                    $class = "bg-success";
                } else if ($s->situacao_solicitacao_id == 4) {
                    $class = "";
                }

                echo '<tr class="'.$class.'" listaId="$s->id">';
                echo '<td>';
                echo  $s->id;
                echo '</td>';

                echo '<td>';
                echo $s->solicitante;
                echo '</td>';

                echo '<td>';
                echo convertData($s->data);
                echo '</td>';

                echo '<td>';
                echo $s->getFornecedor()->nome;;
                echo '</td>';

                echo '<td>';
                echo $s->getContabilidade()->nome;
                echo '</td>';

                echo '<td>';
                echo $s->projeto;
                echo '</td>';

                echo '<td>';
                echo $s->observacao;
                echo '</td>';

                echo '<td>';
                echo 'R$ ';
                echo number_format($s->valor_solicitacao, 2, ',', '.');
                echo '</td>';

                echo '<td>';
                echo $s->getResponsavel()->nome;
                echo '</td>';

                echo '<td>';
                echo $s->getSituacao_solicitacao()->nome;
                echo '</td>';

                if($s->situacao_solicitacao_id != 2 && $s->situacao_solicitacao_id !=3){
                echo '<td width="50">';
                echo '<button type="button" data-toggle="tooltip" data-placement="top" title="Pasta de Documentos/Anexos" class="btn btn-primary command-galeria" data-row-id="' .  $s->id .'"><span class="fa fa-paperclip"></span></button>';
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="fa fa-pencil-square-o"> Editar/Detalhar</span> ', 'Solicitacao', 'edit',
                    array('id' => $s->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>'; 
                }else{
                    echo '<td width="50">';
                    echo '</td>';
                    echo '<td width="50">';
                    echo '</td>';
                }
               /* echo '<td width="50">';
                echo $this->Html->getLink('<span class="fa fa-times"></span> ', 'Solicitacao', 'delete',
                    array('id' => $s->id),
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';*/
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */

    $(document).ready(function() {
        $('.select-fornecedor').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione:'
        });
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Solicitacao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Solicitacao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
      
    });

    $('button.command-galeria').each(function () {
        var conta = $(this).data("row-id");
        $(this).click(function () {
            var urlLista = root + '/Anexo_financeiro/all/origem:4/id_externo:' + conta;
            window.open(urlLista, '_blank');

        });
    });


</script>
</div>
</div>
</div>
</div>
</div>
