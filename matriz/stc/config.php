<?php
date_default_timezone_set("America/Campo_Grande");
##################################
# CONFIGURAÇÕES DO SGBD 
##################################

Config::set('db_host', '192.175.99.74');
Config::set('db_user', 'g2ioper_js');
Config::set('db_password', 'js6990');
Config::set('db_name', 'g2ioper_js');

//Config::set('db_host', 'localhost');
//Config::set('db_user', 'root');
//Config::set('db_password', '');
//Config::set('db_name', 'js_local_matriz');

# MODO DE DEPURAÇÃO
# desenvolvimento: true
# produção: false
Config::set('debug', false);

# TEMPLATE PADRÃO
# Ex: para o diretório /template/default:
# Config::set('template', 'default');
Config::set('template', 'inspinia');
//Config::set('template', 'sidebar');
// Config::set('template', 'topbar');
// Config::set('template', 'default');


# Chave da aplicação, para controle de sessões e criptografia
# Utilize uma cadeia alfanumérica aleatória única
Config::set('key', 'f345kh20120141130pjhygjjft875gfFDGfqqqk3djghkH&*');

# SALT - Utilizada na criptografia
# Utiliza uma chave alfa-numéria complexa de no mínimo 16 dígitos
Config::set('salt', 'TjcqT8jgR8H6v6vhJhBcdZmCvnZs4Ghs9JcvW48gCqUTtVEkcK5hYLpsw6As8AbU');

Config::set('lang', 'pt_br');
Config::set('rewriteURL', true);
Config::set('indexController', 'Index');
Config::set('indexAction', 'index');
Config::set('criptedGetParamns', array());