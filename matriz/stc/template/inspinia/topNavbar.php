<?php
$usuario=Session::get('user');
if($usuario !== null){
?>
<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a role="open-adm" class="animatedClick" data-target='clickExample'>
                    <i class="fa fa-tasks"></i>
                </a>
            </li>
            <li>
                <?php echo $this->Html->getLink('<i class="fa fa-sign-out"></i> Sair', 'Login', 'logout'); ?>
            </li>
        </ul>
    </nav>
</div>

    <div id="right-sidebar" style="display: none" role="adm" class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
        <div class="sidebar-container" full-scroll>
            <ul class="nav nav-tabs navs-2 " role="tablist">
                <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuários</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" style="background: #2f4050">
                <div role="tabpanel" class="tab-pane active" id="profile">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-users"></i> Usuário</h3>
                        <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                    </div>
                    <nav class="navbar-default navbar-static" role="tablist">
                        <div class="sidebar-collapse">
                            <ul class="nav">
                                <li <?php echo(CONTROLLER == "Usuario" ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse" href="#Usuario"
                                       aria-expanded="false" aria-controls="Usuario">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span><span
                                            class="fa arrow"></span></a>

                                    <div id="Usuario" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false" style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Usuario" && ACTION == "conta" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-user"></i>Conta', 'Usuario', 'conta'); ?>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
<?php } ?>