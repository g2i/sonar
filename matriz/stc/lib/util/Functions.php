<?php

function DataSQL($data) {
    if ($data != "") {
        list($d, $m, $y) = preg_split('/\//', $data);
        return sprintf('%04d-%02d-%02d', $y, $m, $d);
    }
}

function DataBR($data) {
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}
function ConvertData($data) {
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

function LimiteTexto($texto, $limite, $quebra = true){
    $tamanho = strlen($texto);
    if($tamanho <= $limite){ //Verifica se o tamanho do texto é menor ou igual ao limite
        $novo_texto = $texto;
    }else{ // Se o tamanho do texto for maior que o limite
        if($quebra == true){ // Verifica a opção de quebrar o texto
            $novo_texto = trim(substr($texto, 0, $limite))."...";
        }else{ // Se não, corta $texto na última palavra antes do limite
            $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
            $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."..."; // Corta o $texto até a posição localizada
        }
    }
    return $novo_texto; // Retorna o valor formatado
}

function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
            $item = utf8_encode($item);
        }
    });

    return $array;
}


function diferenca_datas($inicio,$fim){
    $diferenca = strtotime($fim) - strtotime($inicio);
    $dias = floor($diferenca / (60 * 60 * 24));
    return $dias;

}


function tratar_arquivos($string){
    // pegando a extensao do arquivo
    $partes 	= explode(".", $string);
    $extensao 	= $partes[count($partes)-1];
    // somente o nome do arquivo
    $nome	= preg_replace('/\.[^.]*$/', '', $string);
    // removendo simbolos, acentos etc
    $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýýþÿŔŕ?';
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuuyybyRr-';
    $nome = strtr($nome, utf8_decode($a), $b);
    $nome = str_replace(".","-",$nome);
    $nome = preg_replace( "/[^0-9a-zA-Z\.]+/",'-',$nome);
    return utf8_decode(strtolower($nome.".".$extensao));
}

function getAmount($money)
{
    try{
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }catch (Exception $e){
        return null;
    }
}


function Feriados($ano,$posicao){
    $dia = 86400;
    $datas = array();
    $datas['pascoa'] = easter_date($ano);
    $datas['sexta_santa'] = $datas['pascoa'] - (2 * $dia);
    $datas['carnaval'] = $datas['pascoa'] - (47 * $dia);
    $datas['corpus_cristi'] = $datas['pascoa'] + (60 * $dia);
    $feriados = array (
        '01/01',
        '02/02', // Navegantes
        date('d/m',$datas['carnaval']),
        date('d/m',$datas['sexta_santa']),
        date('d/m',$datas['pascoa']),
        '21/04',
        '01/05',
        date('d/m',$datas['corpus_cristi']),
        '20/09', // Revolução Farroupilha \m/
        '12/10',
        '02/11',
        '15/11',
        '25/12',
    );

    return $feriados[$posicao]."/".$ano;
}

//FORMATA COMO TIMESTAMP
function dataToTimestamp($data){
    $ano = substr($data, 6,4);
    $mes = substr($data, 3,2);
    $dia = substr($data, 0,2);
    return mktime(0, 0, 0, $mes, $dia, $ano);
}

//SOMA 01 DIA
function Soma1dia($data){
    $ano = substr($data, 6,4);
    $mes = substr($data, 3,2);
    $dia = substr($data, 0,2);
    return   date("d/m/Y", mktime(0, 0, 0, $mes, $dia+1, $ano));
}
function Subtraidias($data,$dias){
    $ano = substr($data, 6,4);
    $mes = substr($data, 3,2);
    $dia = substr($data, 0,2);
    return   date("d/m/Y", mktime(0, 0, 0, $mes, $dia-$dias, $ano));
}

function SomaDiasUteis($xDataInicial,$xSomarDias){
    $dias_sub=0;
    for($ii=1; $ii<=$xSomarDias; $ii++){

        $xDataInicial=Soma1dia($xDataInicial); //SOMA DIA NORMAL
        //VERIFICANDO SE EH DIA DE TRABALHO

        if(date("w", dataToTimestamp($xDataInicial))=="0"){
            $dias_sub++;

        }else if(date("w", dataToTimestamp($xDataInicial))=="6"){
            $dias_sub++;
        }
    }
    $xDataInicial = Subtraidias($xDataInicial,$dias_sub);
    return $xDataInicial;
}
