prevent_ajax_view = true;

$(document).ready(function() {
    $('button[data-dismiss="modal"]').click(function() {
        //history.go(-1);
    });
    
    $(document).on('hidden.bs.modal', function(e) {
        $(e.target).removeData('bs.modal');
    });

    $('a[data-toggle=modal]').click(function(event){
        event.preventDefault();
        modalharef = $(this).attr('href');
        if (modalharef.indexOf("?") == -1)
            modalharef += '?'
        modalharef += '&ajax=true';
        $(this).attr('href', modalharef);

        if(modalharef.indexOf("first")!=-1){
            Acrescentar(modalharef);
        }
        if (!$(this).attr('data-target')) {
            $(this).attr('data-target', '#modal');
        }
    });
});
