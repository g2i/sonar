<?php
final class LocalidadeController extends AppController{

    # página inicial do módulo Regioes
    function index(){
        $this->setTitle('Detalhes de Região');
    }

    # lista de Regioes
    # renderiza a visão /view/Regioes/all.php
    function all(){
        $this->setTitle('Listagem de Regioes');
        $p = new Paginate('Regioes', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao','=',1);
        $this->set('Regioes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    



    }

    # visualiza um(a) Regioes
    # renderiza a visão /view/Regioes/view.php
    function view(){
        $this->setTitle('Detalhes de Região');
        try {
            $this->set('Regioes', new Regioes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Localidade', 'all');
        }
    }

    # formulário de cadastro de Regioes
    # renderiza a visão /view/Regioes/add.php
    function add(){
        $this->setTitle('Cadastrar Região');
        $this->set('Regioes', new Regioes);
    }

    # recebe os dados enviados via post do cadastro de Regioes
    # (true)redireciona ou (false) renderiza a visão /view/Regioes/add.php
    function post_add(){
        $this->setTitle('Cadastrar Região');
        $Regioes = new Regioes();
        $this->set('Regioes', $Regioes);
        $_POST['situacao'] =1;
        $_POST['created'] =date('Y-m-d H:i:s');
        $_POST['modified'] =date('Y-m-d H:i:s');
        $_POST['user_id'] =Session::get('user')->id;
        try {
            $Regioes->save($_POST);
            new Msg(__('Regioes cadastrado com sucesso'));
            $this->go('Localidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Regioes
    # renderiza a visão /view/Regioes/edit.php
    function edit(){
        $this->setTitle('Editar Região');
        try {
            $this->set('Regioes', new Regioes((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Localidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Regioes
    # (true)redireciona ou (false) renderiza a visão /view/Regioes/edit.php
    function post_edit(){
        $this->setTitle('Editar Região');
        try {
            $Regioes = new Regioes((int) $_POST['id']);
            $this->set('Regioes', $Regioes);
            $_POST['situacao'] =1;
            $_POST['modified'] =date('Y-m-d H:i:s');
            $_POST['user_id'] =Session::get('user')->id;
            $Regioes->save($_POST);
            new Msg(__('Regioes atualizado com sucesso'));
            $this->go('Localidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Regioes
    # renderiza a /view/Regioes/delete.php
    function delete(){
        $this->setTitle('Apagar Regioes');
        try {
            $this->set('Regioes', new Regioes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Localidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Regioes
    # redireciona para Regioes/all
    function post_delete(){
        try {
            $Regioes = new Regioes((int) $_POST['id']);
            $Regioes->situacao =3;
            $Regioes->deleted =date('Y-m-d H:i:s');
            $Regioes->user_id =Session::get('user')->id;
            $Regioes->save();
            new Msg(__('Regioes apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Localidade', 'all');
    }

}