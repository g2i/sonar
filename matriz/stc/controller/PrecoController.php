<?php
final class PrecoController extends AppController
{

    # página inicial do módulo Preco
    function index()
    {
        $this->setTitle('Centro de Custo');
    }


    # lista de Precos
    # renderiza a visão /view/Preco/all.php
    function all(){
        $this->setTitle('Valores de Repasses');
        $p = new Paginate('Preco', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }


        $c->addCondition('situacao','=',1);
        $this->set('Preco', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $a = new Criteria();
        $a->addCondition('situacao','=',1);
        $this->set('Regiao', Regioes::getList($a));

    }

    # visualiza um(a) Preco
    # renderiza a visão /view/Preco/view.php
    function view(){
        $this->setTitle('Valores');
        try {
            $this->set('Preco', new Preco((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Preco', 'all');
        }
    }

    # formulário de cadastro de Preco
    # renderiza a visão /view/Preco/add.php
    function add(){
        $this->setTitle('Cadastro de Centro de Custo');
        $a = new Criteria();
        $a->addCondition('situacao','=',1);
        $this->set('Regiao', Regioes::getList($a));
        $this->set('Preco', new Preco);
    }

    # recebe os dados enviados via post do cadastro de Preco
    # (true)redireciona ou (false) renderiza a visão /view/Preco/add.php
    function post_add(){
        $this->setTitle('Cadastro de Preco');
        $Preco = new Preco();
        $this->set('Preco', $Preco);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $_POST['cadastradopor']=$user->id;
        $_POST['created']=date('Y-m-d H:i:s');
        $_POST['situacao']=1;
        $_POST['preco'] = getAmount($_POST['preco']);
        $_POST['comissao'] = getAmount($_POST['comissao']);

        try {
            $Preco->save($_POST);
            new Msg(__('Preco cadastrado com sucesso'));
            $this->go('Preco', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # formulário de edição de Preco
    # renderiza a visão /view/Preco/edit.php
    function edit(){
        $this->setTitle('Edição de Preco');
        try {
            $this->set('Preco', new Preco((int) $this->getParam('id')));
            $a = new Criteria();
            $a->addCondition('situacao','=',1);
            $this->set('Regiao', Regioes::getList($a));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Preco', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Preco
    # (true)redireciona ou (false) renderiza a visão /view/Preco/edit.php
    function post_edit(){
        $this->setTitle('Edição de Preco');
        try {
            $Preco = new Preco((int) $_POST['id']);
            $this->set('Preco', $Preco);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $_POST['modified_por']=$user->id;
            $_POST['modified']=date('Y-m-d H:i:s');
            $_POST['situacao']=1;
            $_POST['preco'] = getAmount($_POST['preco']);
            $_POST['comissao'] = getAmount($_POST['comissao']);

            $Preco->save($_POST);
            new Msg(__('Preco atualizado com sucesso'));
            $this->go('Preco', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Preco
    # renderiza a /view/Preco/delete.php
    function delete(){
        $this->setTitle('Apagar Preco');
        try {
            $this->set('Preco', new Preco((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Preco', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Preco
    # redireciona para Preco/all
    function post_delete(){
        try {
            $Preco = new Preco((int) $_POST['id']);
            $user=Session::get('user');
            $Preco->deleted_por=$user->id; //salva quem está fazendo
            $Preco->deleted=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Preco->situacao=3;
            $Preco->save();
            new Msg(__('Preco apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Preco', 'all');
    }

}