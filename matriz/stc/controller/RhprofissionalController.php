<?php

final class RhprofissionalController extends AppController
{

    # página inicial do módulo Rhprofissional
    function index()
    {
        $this->setTitle('Index');


    }

    # lista de Rhprofissionais
    # renderiza a visão /view/Rhprofissional/all.php
    function all()
    {
        $this->setTitle('Profissionais');
        $p = new Paginate('Rhprofissional', 10);
        $c = new Criteria();
        $this->set('status',null);
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->set($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->set($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
                if(empty($_POST['filtro']['externo']['status'])){
                    $c->addCondition('status','=',1);
                    $this->set('status',1);
                }
            }
        }else{
            $c->addCondition('status','=',1);
            $this->set('status',1);
        }

        $c->setOrder('nome');


        $this->set('Rhprofissionais', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Rhsetores', Rhsetor::getList());
        $this->set('Rhstatus', Rhstatus::getList());
    }

    # visualiza um(a) Rhprofissional
    # renderiza a visão /view/Rhprofissional/view.php
    function view()
    {
        $this->setTitle('Visualização de Profissional');
        $this->set('Rhstatus', Rhstatus::getList());
        $this->set('Usuario', Usuario::getList());
        try {
            $this->set('Rhprofissional', new Rhprofissional((int)$this->getParam('id')));

        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional', 'all');
        }
    }
    # visualiza um(a) Rhprofissional
    # renderiza a visão /view/Rhprofissional/view.php
    function pdf()
    {
        $this->setTitle('Visualização de Profissional');
        $this->set('Rhstatus', Rhstatus::getList());
        $this->set('Usuario', Usuario::getList());
        $this->set('Rhprofissional_funcao', Rhprofissional_funcao::getList());
        $Rhprofissional = new Rhprofissional((int)$this->getParam('id'));

        $content = '<page backcolor="#FFFFFF" backleft="5mm" backright="5mm" backtop="10mm" backbottom="10mm" >
<div style=" padding-left: 570px" >' . $data = date('d/m/Y H:i:s') . '</div>

<table align="left">
<tr>
<td style="padding-left: 50px">
<img src="./img/logo.png">
</td>
<td style="padding-left: 50px">
     <h3>Ficha Cadastral</h3>
</td>
</tr>
</table>
    <table  align="center" style="padding-top:-40px;">
    <tr>
    <td colspan="4" align="center"><h3><hr>Dados Pessoais<hr></h3></td>
    </tr>
    <tr>
            <td><strong>Nome :</strong></td>
            <td>' . $Rhprofissional->nome . '</td>
            <td><strong>Matricula :</strong></td>
            <td>' . $Rhprofissional->matricula . '</td>
        </tr>
        <tr>
            <td><strong>Data de Nascimento :</strong></td>
            <td>' . DataBR($Rhprofissional->dtnascimento) . '</td>
            <td><strong>Sexo :</strong></td>
            <td>' . $Rhprofissional->sexo . '</td>
        </tr>
        <tr>
            <td><strong>Escolaridade :</strong></td>
            <td>' . $Rhprofissional->grauinstrucao . '</td>
            <td><strong>Estado Civil :</strong></td>
            <td>' . $Rhprofissional->estadocivil . '</td>
        </tr>
        <tr>
            <td><strong>Naturalidade :</strong></td>
            <td>' . $Rhprofissional->naturalidade . '</td>
            <td><strong>Nacionalidade :</strong></td>
            <td>' . $Rhprofissional->nacionalidade . '</td>
        </tr>
        <tr>
            <td><strong>Nome da Mãe :</strong></td>
            <td>' . $Rhprofissional->mae . '</td>
            <td><strong>Setor :</strong></td>
            <td>' . $Rhprofissional->getRhsetor()->nome . '</td>
        </tr>
        <tr>
             <td><strong>Nome do Pai :</strong></td>
            <td>' . $Rhprofissional->pai . '</td>
            <td><strong>Status :</strong></td>
            <td>' . $Rhprofissional->getStatus()->descricao . '</td>
        </tr>
        <tr>
            <td colspan="4" align="center"><h3><hr>Endereço<hr></h3></td>
        </tr>
        <tr>
            <td><strong>Endereço :</strong></td>
            <td>' . $Rhprofissional->endereco . '</td>
            <td><strong>CEP :</strong></td>
            <td>' . $Rhprofissional->cep . '</td>

        </tr>
        <tr>
            <td><strong>Numero :</strong></td>
            <td>' . $Rhprofissional->numero . '</td>
            <td><strong>Complemento :</strong></td>
            <td>' . $Rhprofissional->complemento . '</td>
        </tr>
         <tr>
            <td><strong>Bairro :</strong></td>
            <td>' . $Rhprofissional->bairro . '</td>
            <td><strong>Cidade :</strong></td>
            <td>' . $Rhprofissional->cidade . '</td>
        </tr>
         <tr>
            <td><strong>Estado :</strong></td>
            <td colspan="3">' . $Rhprofissional->estado . '</td>
        </tr>
        <tr>
            <td colspan="4" align="center"><h3><hr>Contato<hr></h3></td>
        </tr>
        <tr>
            <td><strong>Celular :</strong></td>
            <td>' . $Rhprofissional->celular01 . '</td>
            <td><strong>Celular Corporativo :</strong></td>
            <td>' . $Rhprofissional->celular02 . '</td>
        </tr>
        <tr>
            <td><strong>Telefone Residencial :</strong></td>
            <td>' . $Rhprofissional->residencial . '</td>
            <td><strong>Telefone para Recado :</strong></td>
            <td>' . $Rhprofissional->recado . '</td>
        </tr>
         <tr>
            <td><strong>Telefone Corporativo :</strong></td>
            <td>' . $Rhprofissional->corporativo . '</td>
            <td><strong>E-mail :</strong></td>
            <td>' . $Rhprofissional->email . '</td>
        </tr>
         <tr>
            <td colspan="4" align="center"><h3><hr>Documentos<hr></h3></td>
        </tr>
          <tr>
            <td><strong>RG :</strong></td>
            <td>' . $Rhprofissional->rg . '</td>
            <td><strong>Data de Emissão :</strong></td>
            <td>' . DataBR($Rhprofissional->dtemissao) . '</td>
        </tr>
          <tr>
            <td><strong>Orgão Expeditor :</strong></td>
            <td>' . $Rhprofissional->orgaoexped . '</td>
            <td><strong>CPF :</strong></td>
            <td>' . $Rhprofissional->cpf . '</td>
</tr>
             <tr>
            <td><strong>Nº de Registro da CNH :</strong></td>
            <td>' . $Rhprofissional->carteiriahabilitacao . '</td>
            <td><strong>Categoria da CNH :</strong></td>
            <td>' . $Rhprofissional->habilitacao_categoria . '</td>
              </tr>
         <tr>
            <td><strong>Vencimento CNH :</strong></td>
            <td>' . DataBR($Rhprofissional->habilitacao_vencimento) . '</td>
            <td><strong>PIS :</strong></td>
            <td>' . $Rhprofissional->pis . '</td>
             </tr>
        <tr>
            <td><strong>Nº da Carteira de Trabalho :</strong></td>
            <td>' . $Rhprofissional->carteiratrabalho . '</td>

            <td><strong>Data de Emisão CTPS :</strong></td>
            <td>' . DataBR($Rhprofissional->dtcarteiratrabalho) . '</td>
              </tr>
        <tr>
            <td><strong>Orgão Expeditor CTPS  :</strong></td>
            <td>' . $Rhprofissional->expeditorctps . '</td>

            <td><strong>Numero da Reservista :</strong></td>
            <td>' . $Rhprofissional->reservista . '</td>
             </tr>
        <tr>
            <td><strong>Categoria da Reservista  :</strong></td>
            <td>' . $Rhprofissional->reservista_categoria . '</td>
            <td><strong>Nº Titulo do Eleitor :</strong></td>
            <td>' . $Rhprofissional->tituloeleitor . '</td>
             </tr>
        <tr>
            <td><strong>Zona  :</strong></td>
            <td>' . $Rhprofissional->zona . '</td>
            <td><strong>Seção :</strong></td>
            <td>' . $Rhprofissional->secao . '</td>
        </tr>
    </table>';
        $aux = count($Rhprofissional->getRhprofissional_contratacaos());
        if ($aux > 0) {
            $content .= '<table align="center" cellspacing="3">';
            $content .= '<tr>';
            $content .= '<td colspan="6" align="center"><h3><hr>Contratação<hr></h3></td>';
            $content .= '</tr>';

            foreach ($Rhprofissional->getRhprofissional_contratacaos() as $c) {
                $content .= '<tr>';
                $content .= '<td><strong>Tipo :</strong></td>';
                $content .= '<td>' . $c->tipo . '</td>';
                $content .= '<td><strong>Jornada de Trabalho :</strong></td>';
                $content .= '<td>' . $c->jornada . '</td>';
                $content .= '<td><strong>Tipo de Remuneração :</strong></td>';
                $content .= '<td>' . $c->tiporemuneracao . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Data Inicial :</strong></td>';
                $content .= '<td>' . DataBR($c->dtInicial) . '</td>';
                $content .= '<td><strong>Data Final :</strong></td>';
                $content .= '<td>' . DataBR($c->dtFinal) . '</td>';
                $content .= '<td><strong>Valor Remuneração :</strong></td>';
                $content .= '<td>' . number_format($c->valorremuneracao, 2, ',', '.') . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Vale Transporte ? :</strong></td>';
                $content .= '<td>' . $c->valetransporte . '</td>';
                $content .= '<td><strong>VT por Dia :</strong></td>';
                $content .= '<td>' . $c->valetransporteqtdia . '</td>';
                $content .= '<td><strong>Adicional :</strong></td>';
                $content .= '<td>' . $c->adicional . ' % </td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Horários :</strong></td>';
                $content .= '<td>' . $c->horario . '</td>';
                $content .= '<td><strong>Observação :</strong></td>';
                $content .= '<td colspan="3">' . $c->observacao . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Local de trabalho:</strong></td>';
                $content .= '<td>' . $c->local_trabalho . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Função :</strong></td>';
                $content .= '<td colspan="3">' . $c->getRhprofissional_funcao()->nome . '</td>';
                $content .= '</tr>';
                $content .= '<tr bgcolor="#dcdcdc">';
                $content .= '<td colspan="2" rowspan="2" align="center"><h5>Conta Salário</h5></td>';
                $content .= '<td><strong>Banco :</strong></td>';
                $content .= '<td>' . $c->banco . '</td>';
                $content .= '<td><strong>Agência :</strong></td>';
                $content .= '<td>' . $c->bancoagencia . '</td>';
                $content .= '</tr>';
                $content .= '<tr bgcolor="#dcdcdc">';
                $content .= '<td><strong>Conta :</strong></td>';
                $content .= '<td>' . $c->bancoconta . '</td>';
                $content .= '<td><strong>Operação :</strong></td>';
                $content .= '<td>' . $c->bancooperacao . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td colspan="6" bgcolor="#dcdcdc" align="center"></td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td colspan="2" rowspan="2" align="center"><h5>Conta Pessoal</h5></td>';
                $content .= '<td><strong>Banco :</strong></td>';
                $content .= '<td>' . $c->banco2 . '</td>';
                $content .= '<td><strong>Agência :</strong></td>';
                $content .= '<td>' . $c->bancoagencia2 . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Conta :</strong></td>';
                $content .= '<td>' . $c->bancoconta2 . '</td>';
                $content .= '<td><strong>Operação :</strong></td>';
                $content .= '<td>' . $c->bancooperacao2 . '</td>';
                $content .= '</tr>';
                if ($aux > 1) {
                    $content .= '<tr>';
                    $content .= '<td colspan="6" bgcolor="#696969" align="center"></td>';
                    $content .= '</tr>';
                    $aux--;
                }
            }
            $content .= '</table>';
        }
        $aux_dep = count($Rhprofissional->getRhprofissional_dependentes());
        if ($aux_dep > 0) {
            $content .= '<table align="center" cellspacing="5">';
            $content .= '<tr>';
            $content .= '<td colspan="4" align="center"><h3><hr>Dependentes <hr></h3></td>';
            $content .= '</tr>';
            foreach ($Rhprofissional->getRhprofissional_dependentes() as $d) {
                $content .= '<tr>';
                $content .= '<td><strong>Nome :</strong></td>';
                $content .= '<td>' . $d->nome . '</td>';
                $content .= '<td><strong>Tipo :</strong></td>';
                $content .= '<td>' . $d->tipo . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Registro :</strong></td>';
                $content .= '<td>' . $d->registro . '</td>';
                $content .= '<td><strong>Data de Nascimento :</strong></td>';
                $content .= '<td>' . DataBR($d->dtnascimento) . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td><strong>Possui Cartão de Vacinação? :</strong></td>';
                $content .= '<td>' . $d->cartaovacinacao . '</td>';
                $content .= '<td><strong>Possui Declaração Escolar? :</strong></td>';
                $content .= '<td>' . $d->declaracaoescolar . '</td>';
                $content .= '</tr>';
                $content .= '<tr>';
                $content .= '<td colspan="4" bgcolor="#696969" align="center"></td>';
                $content .= '</tr>';
            }
            $content .= '</table>';
        }
        $content .= '<br>';

        $content .= '<table><tr><td style="padding-left: 500px" >Copyright <a href="http://www.grupog2i.com.br/web/site/empresa.php"><img src="./img/g2i.png"></a> © 2015</td></tr></table>';

        $content .= '</page>';


        try {
            $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', 0);
            $html2pdf->writeHTML($content);
            $html2pdf->Output('profissional.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }

    }

    # formulário de cadastro de Rhprofissional
    # renderiza a visão /view/Rhprofissional/add.php
    function add()
    {
        $this->setTitle('Cadastro de Profissional');
        $this->set('Rhprofissional', new Rhprofissional);
        $this->set('Rhsetores', Rhsetor::getList());
        $this->set('Rhstatus', Rhstatus::getList());
        $this->set('Usuario', Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhprofissional
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Profissional');
        $Rhprofissional = new Rhprofissional();
        $user = Session::get('user');// para salvar o usuario que está fazendo
        $_POST['cadastradopor'] = $user->id;
        $_POST['dtCadastro'] = date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status'] = 1;
        try {
            if ($Rhprofissional->save($_POST)) { // se salvar foto
                if (!empty($_FILES['foto']['name'])) {
                    $img = $_FILES['foto'];
                    $foto = new ImageUploader($img, 150);
                    $path = "/fotos/" . $Rhprofissional->codigo;
                    $nome = uniqid() . $img['name'];
                    $foto->save($nome, $path);
                    $Rhprofissiona = new Rhprofissional($Rhprofissional->codigo);
                    $Rhprofissiona->foto = SITE_PATH . '/uploads' . $path . "/" . $nome;
                    $Rhprofissiona->save();
                }
            }
            new Msg(__('Profissional cadastrado com sucesso '));

            $this->go('Rhprofissional', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Rhsetores', Rhsetor::getList());
    }

    # formulário de edição de Rhprofissional
    # renderiza a visão /view/Rhprofissional/edit.php
    function edit()
    {
        $this->setTitle('Edição de Profissional');
        try {
            $this->set('Rhprofissional', new Rhprofissional((int)$this->getParam('id')));
            $this->set('Rhsetores', Rhsetor::getList());
            $this->set('Usuario', Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Rhprofissional', 'all');
        }
        $this->set('Rhstatus', Rhstatus::getList());
    }

    # recebe os dados enviados via post da edição de Rhprofissional
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Profissional');
        $Rhprofissional = new Rhprofissional((int)$_POST['codigo']);
        $this->set('Rhprofissional', $Rhprofissional);
        $Rhprofissional->dtAtualizacao = date('Y-m-d H:i:s');
        $user = Session::get('user');// para salvar o usuario que está fazendo
        $Rhprofissional->atualizadopor = $user->id; // para salvar o usuario que está fazendo
        try {
            if ($Rhprofissional->save($_POST)) {
                if (!empty($_FILES['foto']['name'])) {
                    $img = $_FILES['foto'];
                    $foto = new ImageUploader($img, 150);
                    $path = "/fotos/" . $Rhprofissional->codigo;
                    $nome = uniqid() . $img['name'];
                    $foto->save($nome, $path);
                    $Rhprofissiona = new Rhprofissional($Rhprofissional->codigo);
                    $Rhprofissiona->foto = SITE_PATH . '/uploads' . $path . "/" . $nome;
                    $Rhprofissiona->save();
                }
                new Msg(__('Profissional atualizado com sucesso'));
                $this->go('Rhprofissional', 'all');

            }
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhsetores', Rhsetor::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhprofissional
    # renderiza a /view/Rhprofissional/delete.php
    function delete()
    {
        $this->setTitle('Apagar Rhprofissional');
        try {
            $this->set('Rhprofissional', new Rhprofissional((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhprofissional
    # redireciona para Rhprofissional/all
    function post_delete()
    {
        try {
            $Rhprofissional = new Rhprofissional((int)$_POST['codigo']);
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Rhprofissional->atualizadopor = $user->id; // para salvar o usuario que está fazendo
            $Rhprofissional->dtAtualizacao = date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhprofissional->status = 3;
            $Rhprofissional->save();
            new Msg(__('Profissional desativado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Rhprofissional', 'all');
    }
}