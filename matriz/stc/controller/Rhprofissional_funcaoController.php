<?php
final class Rhprofissional_funcaoController extends AppController{ 

    # página inicial do módulo Rhprofissional_funcao
    function index(){
        $this->setTitle('Visualização de Função');
    }

    # lista de Rhprofissional_funcaos
    # renderiza a visão /view/Rhprofissional_funcao/all.php
    function all(){
        $this->setTitle('Funções');
        $p = new Paginate('Rhprofissional_funcao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->setOrder('nome');

        $this->set('Rhprofissional_funcaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhstatus',  Rhstatus::getList());
    

    }

    # visualiza um(a) Rhprofissional_funcao
    # renderiza a visão /view/Rhprofissional_funcao/view.php
    function view(){
        $this->setTitle('Visualização de Função');
        try {
            $this->set('Rhprofissional_funcao', new Rhprofissional_funcao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_funcao', 'all');
        }
    }

    # formulário de cadastro de Rhprofissional_funcao
    # renderiza a visão /view/Rhprofissional_funcao/add.php
    function add(){
        $this->setTitle('Cadastro de Função');
        $this->set('Rhprofissional_funcao', new Rhprofissional_funcao);
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhprofissional_funcao
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_funcao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Função');
        $Rhprofissional_funcao = new Rhprofissional_funcao();
        $this->set('Rhprofissional_funcao', $Rhprofissional_funcao);
        $user=Session::get('user');
        $Rhprofissional_funcao->cadastradopor=$user->id;
        $Rhprofissional_funcao->dtCadastro=date('Y-m-d H:i:s');
        $Rhprofissional_funcao->status=1;
        try {
            $Rhprofissional_funcao->save($_POST);
            new Msg(__('Função cadastrada com sucesso'));
            $this->go('Rhprofissional_funcao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # formulário de edição de Rhprofissional_funcao
    # renderiza a visão /view/Rhprofissional_funcao/edit.php
    function edit(){
        $this->setTitle('Edição de Função');
        try {
            $this->set('Rhprofissional_funcao', new Rhprofissional_funcao((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhprofissional_funcao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhprofissional_funcao
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_funcao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Função');
        try {
            $Rhprofissional_funcao = new Rhprofissional_funcao((int) $_POST['codigo']);
            $user=Session::get('user');
            $Rhprofissional_funcao->atualizadopor=$user->id;
            $Rhprofissional_funcao->dtAtualizacao=date('Y-m-d H:i:s');
            $this->set('Rhprofissional_funcao', $Rhprofissional_funcao);
            $Rhprofissional_funcao->save($_POST);
            new Msg(__('Função atualizada com sucesso'));
            $this->go('Rhprofissional_funcao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhprofissional_funcao
    # renderiza a /view/Rhprofissional_funcao/delete.php
    function delete(){
        $this->setTitle('Apagar Função');
        try {
            $this->set('Rhprofissional_funcao', new Rhprofissional_funcao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_funcao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhprofissional_funcao
    # redireciona para Rhprofissional_funcao/all
    function post_delete(){
        try {
            $Rhprofissional_funcao = new Rhprofissional_funcao((int) $_POST['id']);
            $Rhprofissional_funcao->status=3;
            $Rhprofissional_funcao->save();
            new Msg(__('Função desativada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprofissional_funcao', 'all');
    }

}