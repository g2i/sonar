<?php
final class RhsecaoController extends AppController{ 

    # página inicial do módulo Rhsecao
    function index(){
        $this->setTitle('Visualização de Seção');
    }

    # lista de Rhsecaos
    # renderiza a visão /view/Rhsecao/all.php
    function all(){
        $this->setTitle('Seção');
        $p = new Paginate('Rhsecao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhsecaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdivisao',  Rhdivisao::getList());
    

    }

    # visualiza um(a) Rhsecao
    # renderiza a visão /view/Rhsecao/view.php
    function view(){
        $this->setTitle('Visualização de Seção');
        try {
            $this->set('Rhsecao', new Rhsecao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhsecao', 'all');
        }
    }

    # formulário de cadastro de Rhsecao
    # renderiza a visão /view/Rhsecao/add.php
    function add(){
        $this->setTitle('Cadastro de Seção');
        $this->set('Rhsecao', new Rhsecao);
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdivisaos',  Rhdivisao::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhsecao
    # (true)redireciona ou (false) renderiza a visão /view/Rhsecao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Seção');
        $Rhsecao = new Rhsecao();
        $this->set('Rhsecao', $Rhsecao);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhsecao->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhsecao->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhsecao->save($_POST);
            new Msg(__('Seção cadastrada com sucesso'));
            $this->go('Rhsecao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdivisaos',  Rhdivisao::getList());
    }

    # formulário de edição de Rhsecao
    # renderiza a visão /view/Rhsecao/edit.php
    function edit(){
        $this->setTitle('Editar Seção');
        try {
            $this->set('Rhsecao', new Rhsecao((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
            $this->set('Rhdivisaos',  Rhdivisao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhsecao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhsecao
    # (true)redireciona ou (false) renderiza a visão /view/Rhsecao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Seção');
        try {
            $Rhsecao = new Rhsecao((int) $_POST['codigo']);
            $this->set('Rhsecao', $Rhsecao);
            $Rhsecao->dtAtualizacao=date('Y-m-d H:i:s');//para salvar a data e a hora da ultima atualização
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhsecao->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhsecao->save($_POST);
            new Msg(__('Seção atualizada com sucesso'));
            $this->go('Rhsecao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdivisaos',  Rhdivisao::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhsecao
    # renderiza a /view/Rhsecao/delete.php
    function delete(){
        $this->setTitle('Apagar Seção');
        try {
            $this->set('Rhsecao', new Rhsecao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhsecao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhsecao
    # redireciona para Rhsecao/all
    function post_delete(){
        try {
            $Rhsecao = new Rhsecao((int) $_POST['id']);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhsecao->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhsecao->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhsecao->status=3;
            $Rhsecao->save();
            new Msg(__('Seção apagada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhsecao', 'all');
    }

}