<?php
final class ImportController extends AppController{
    # página inicial do módulo Rhsetor
    function index(){
        $this->setTitle('Visualização de Dados');
    }

    # lista de Rhsetores
    # renderiza a visão /view/Rhsetor/all.php
    function all(){
        $this->setTitle('Dados comerciais');
        $p = new Paginate('Stc_dados_comerciais', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Stc_dados_comerciais', $p->getPage($c));
        $this->set('nav', $p->getNav());




    }

    # visualiza um(a) Rhsetor
    # renderiza a visão /view/Rhsetor/view.php
    function view(){
        $this->setTitle('Visualização de Dados');
        try {
            $this->set('Stc_dados_comerciais', new Rhsetor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Import', 'all');
        }
    }

    # formulário de cadastro de Rhsetor
    # renderiza a visão /view/Rhsetor/add.php
    function importar(){
        $this->setTitle('Importar Dados');

    }

    # recebe os dados enviados via post do cadastro de Rhsetor
    # (true)redireciona ou (false) renderiza a visão /view/Rhsetor/add.php
    function post_importar(){
        $this->setTitle('Importando Dados');
        if(!empty($_FILES['link']['name'])){
            $files = $_FILES['link'];

            $extencoes = array('csv','txt');
            $Arquivos = new FileUploader($files, NULL, $extencoes);
            $nome = uniqid() . tratar_arquivos($files['name']);
            $path = "arquivos/";
            try {
                if ($Arquivos->save($nome, $path)) {
                    $Arquivos_importacao = new Src_arquivos_importacao();
                    $Arquivos_importacao->data = date('Y-m-d H:i:s');
                    $Arquivos_importacao->caminho = "/uploads/" . $path  . $nome;
                    try {
                        if ($Arquivos_importacao->save()) {

                            $tabela = $this->query('DESCRIBE stc_dados_comerciais');


                            foreach ($tabela as $t) {
                                $fields[] = $t->Field;
                            }

                            if($_POST['limpar']==1) {
                                $this->query('TRUNCATE stc_dados_comerciais');
                            }
                            $csv =  $_SERVER['DOCUMENT_ROOT'].SITE_PATH.$Arquivos_importacao->caminho;
                            $fp = fopen ($csv, "r");
                            $aux=1;
                            $erros = [];
                            $er=0;
                            $col=0;
                            while ($data = fgetcsv ($fp, null, ";"))
                            {
                                $col++;
                                if($aux>1) {
                                    $sql = "INSERT INTO  stc_dados_comerciais ( ";
                                    $sql .= implode(',', $fields);
                                    $sql .= ")  VALUES ( NULL,";

                                    foreach ($tabela as $t => $v) {
                                        if($t<=count($tabela)-2) {
                                            $er++;
                                            if (!empty($data[$t])) {
                                                $val = utf8_encode($data[$t]);
                                                $val = str_replace('"', '', $val);
                                                $val = str_replace('=', '', $val);
                                            } else {
                                                $val = ' ';
                                            }
                                            if ($t <= count($tabela) - 3) {
                                                $sql .= '"' . $val . '",';
                                            } else {
                                                $sql .= 'NULL)';
                                            }
                                        }
                                    }
                                    if(!$this->query($sql)){
                                        $erros[]= ['col'=>$col,'count'=>$er];
                                    }

                                    $er=0;
                                }
                                $aux++;
                            }
                            fclose($fp);
                            $this->update_dados();
                            echo 1;
                            exit;
                        }
                    }catch(Exception $e){
                        new Msg($e->getMessage(),3);
                    }
                }
            }catch(Exception $e){
                new Msg($e->getMessage(),3);
            }
        }else{
            new Msg(__('Selecione um arquivo válido para continuar!'), 2);
        }


    }

    function i_import(){
        $this->setTitle('Leitura dos dados importados');

        $aux=0;

        $query = "SELECT COUNT(DISTINCT(D.num_OS)) AS num_os, SUBSTR(D.data_fim_serv,1,10) AS data_fim,

                    (SELECT COUNT(DISTINCT DT.equipe) FROM stc_dados_comerciais DT
                    WHERE STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y')  = STR_TO_DATE(SUBSTR(DT.data_fim_serv,1,10),'%d/%m/%Y') AND DT.tipo_conclusao <> 'Com Rejeição' AND DT.equipe <> 'equipe') AS equipes
                    FROM stc_dados_comerciais D
                    WHERE  tipo_conclusao <> 'Com Rejeição' AND D.equipe <> 'equipe'
                     AND D.`cod_sap` IS NOT NULL  ";

        $query_equipes = "SELECT D.`equipe`, SUBSTR(D.data_fim_serv,1,10) AS data_fim, COUNT(DISTINCT(D.num_OS)) AS num_os
                                FROM stc_dados_comerciais D
                                WHERE  D.tipo_conclusao <> 'Com Rejeição' AND D.equipe <> 'equipe' AND D.`cod_sap` IS NOT NULL ";

        $equip = "SELECT DISTINCT D.`equipe` FROM stc_dados_comerciais D WHERE D.`equipe` <> 'equipe' ";

        if($this->getParam('inicio') ){
            $query.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') >= '".DataSQL($this->getParam('inicio'))."' ";
            $equip.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') >= '".DataSQL($this->getParam('inicio'))."' ";
            $query_equipes.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') >= '".DataSQL($this->getParam('inicio'))."' ";
            $aux=1;

            $dias = diferenca_datas(DataSQL($this->getParam('inicio')),DataSQL($this->getParam('fim')));


            $dif_dias = SomaDiasUteis($this->getParam('inicio'),$dias);
            $result = diferenca_datas(DataSQL($this->getParam('inicio')),DataSQL($dif_dias));
            $this->set('dias',$result);
        }
        if($this->getParam('fim') ){
            $query.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') <= '".DataSQL($this->getParam('fim'))."' ";
            $equip.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') <= '".DataSQL($this->getParam('fim'))."' ";
            $query_equipes.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') <= '".DataSQL($this->getParam('fim'))."' ";
            $aux=1;
        }

        if($this->getParam('meta_projeto') ){
            $this->set('meta_projeto',getAmount($this->getParam('meta_projeto')) );
        }else{
            $this->set('meta_projeto',0);
        }
        if($this->getParam('meta_equipe') ){
            $this->set('meta_equipe', getAmount($this->getParam('meta_equipe')));
        }else{
            $this->set('meta_equipe', 0);
        }

        $query .= " GROUP BY STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') ";
        $query_equipes .= " GROUP BY D.`equipe` ";
        $equip.= " ORDER  by D.`equipe` ASC ";
        if($aux==1) {
            $dados = $this->query($query);
            $equipes = $this->query($query_equipes);
            $EQ= $this->query($equip);
            $this->set('Dados', $dados);
            $this->set('Equipes', $equipes);
            $this->set('EQ', $EQ);

        }else{
            $this->set('Dados', array());
            $this->set('Equipes', array());
            $this->set('EQ', array());

        }


    }

    function update_dados(){

/*        TODO regras antiga
        $sql = 'UPDATE stc_dados_comerciais
                SET cod_sap =
                    IF(tipo_conclusao = "Conclusão c/ Execução Ligação","1002192",
                    IF(tipo_conclusao = "Normal","1800312",
                    IF(((subtipo_tarefa = "056" OR subtipo_tarefa ="093") AND tipo_tarefa = "MS"),"1800312",
                    IF(((subtipo_tarefa = "907" OR subtipo_tarefa ="999" OR subtipo_tarefa ="906") AND tipo_tarefa = "MS"),"1800441",
                    IF((tipo_tarefa = "RI" AND subtipo_tarefa = "RLC"),"1802053",
                    IF(tipo_tarefa = "RT","1802058",
                    IF((tipo_tarefa = "TR" AND medidor = medidor_uc),"1802121",
                    IF(tipo_tarefa = "EQP","2005750",NULL))))))))';
 */

        $sql = 'UPDATE stc_dados_comerciais
                  SET cod_sap = 
                    IF((tipo_tarefa = "DS" OR (tipo_tarefa = "DUC" AND tipo_tarefa = "MO")),"1800312",
                    IF(tipo_tarefa = "LG","1002192",
                    IF((tipo_tarefa = "MS" OR tipo_tarefa = "VIS"),"1800441",
                    IF(subtipo_tarefa = "LG","1002192",
                    IF(subtipo_tarefa = "VIS","1800441",
                    IF(subtipo_tarefa = "MS","1800441",
                    IF(subtipo_tarefa = "DS","1800312",
                    IF(subtipo_tarefa = "RI","1802053",
                    IF(subtipo_tarefa = "RLC","1802053",NULL)))))))))';

        $this->query($sql);

    }
    # formulário de edição de Rhsetor
    # renderiza a visão /view/Rhsetor/edit.php
    function edit(){
        $this->setTitle('Edição de Setor');
        try {
            $this->set('Rhsetor', new Rhsetor((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
            $this->set('Rhsecaos',  Rhsecao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhsetor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhsetor
    # (true)redireciona ou (false) renderiza a visão /view/Rhsetor/edit.php
    function post_edit(){
        $this->setTitle('Edição de Setor');
        try {
            $Rhsetor = new Rhsetor((int) $_POST['codigo']);
            $this->set('Rhsetor', $Rhsetor);

            $Rhsetor->dtAtualizacao=date('Y-m-d H:i:s');//para salvar a data e a hora da ultima atualização
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhsetor->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhsetor->save($_POST);
            new Msg(__('Setor atualizado com sucesso'));
            $this->go('Rhsetor', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhsecaos',  Rhsecao::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhsetor
    # renderiza a /view/Rhsetor/delete.php
    function delete(){
        $this->setTitle('Apagar Setor');
        try {
            $this->set('Rhsetor', new Rhsetor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhsetor', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhsetor
    # redireciona para Rhsetor/all
    function post_delete(){
        try {
            $Rhsetor = new Rhsetor((int) $_POST['id']);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhsetor->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhsetor->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhsetor->status=3;
            $Rhsetor->save();
            new Msg(__('Setor deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhsetor', 'all');
    }

    function dados_grafico(){
        $query_equipes = "SELECT D.`equipe` AS label, SUBSTR(D.data_fim_serv,1,10) AS data_fim
	                      FROM stc_dados_comerciais D
                          WHERE  tipo_conclusao <> 'Com Rejeição' AND D.`cod_sap` IS NOT NULL ";

        $query_equipes.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') >= '".DataSQL($_POST['inicio'])."' ";
        $query_equipes.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') <= '".DataSQL($_POST['fim'])."' ";
        $query_equipes .= " GROUP BY D.`equipe` ";
        $equipes = $this->query($query_equipes);
        $res = array();
        foreach($equipes as $e){
            $res[] = array(
                'label'=>$e->label,
                'value'=>Stc_dados_comerciais::getPrecos_equipes($_POST['inicio'],$_POST['fim'],$e->label)
            );
        }
        echo json_encode($res);

        exit;
}

    function inconsistente(){
        $this->setTitle('Leitura dos dados importados');
        $aux=0;
        $query = "SELECT SUBSTR(D.data_fim_serv,1,10) AS data_fim, D.num_OS AS num_os ,
                  D.tipo_tarefa,D.subtipo_tarefa,D.tipo_fase,D.tipo_conclusao,D.equipe, D.`cod_sap` AS cod_dados
                    FROM stc_dados_comerciais D
                    WHERE IF(D.`cod_sap` IS NULL,D.`cod_sap` IS NULL, D.cod_sap NOT IN(SELECT P.cod_sap FROM stc_tab_precos P)) ";


        if($this->getParam('inicio') ){
            $query.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') >= '".DataSQL($this->getParam('inicio'))."' ";
            $aux=1;
        }
        if($this->getParam('fim') ){
            $query.=" AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') <= '".DataSQL($this->getParam('fim'))."' ";
            $aux=1;
        }


        if($aux==1) {
            $query.=' OR D.data_fim_serv =""';
            $dados = $this->query($query);
            $this->set('Dados_', $dados);
        }else{
            $this->set('Dados_', array());
        }
    }

}