<?php
final class RhcentrocustoController extends AppController{ 

    # página inicial do módulo Rhcentrocusto
    function index(){
        $this->setTitle('Centro de Custo');
    }

    # lista de Rhcentrocustos
    # renderiza a visão /view/Rhcentrocusto/all.php
    function all(){
        $this->setTitle('Centro de Custo');
        $p = new Paginate('Rhcentrocusto', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhcentrocustos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhstatus',  Rhstatus::getList());
    

    }

    # visualiza um(a) Rhcentrocusto
    # renderiza a visão /view/Rhcentrocusto/view.php
    function view(){
        $this->setTitle('Centro de Custo');
        try {
            $this->set('Rhcentrocusto', new Rhcentrocusto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhcentrocusto', 'all');
        }
    }

    # formulário de cadastro de Rhcentrocusto
    # renderiza a visão /view/Rhcentrocusto/add.php
    function add(){
        $this->setTitle('Cadastro de Centro de Custo');
        $this->set('Rhcentrocusto', new Rhcentrocusto);
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhcentrocusto
    # (true)redireciona ou (false) renderiza a visão /view/Rhcentrocusto/add.php
    function post_add(){
        $this->setTitle('Cadastro de Centro de Custo');
        $Rhcentrocusto = new Rhcentrocusto();
        $this->set('Rhcentrocusto', $Rhcentrocusto);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhcentrocusto->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhcentrocusto->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;

        try {
            $Rhcentrocusto->save($_POST);
            new Msg(__('Centro de Custo cadastrado com sucesso'));
            $this->go('Rhcentrocusto', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # formulário de edição de Rhcentrocusto
    # renderiza a visão /view/Rhcentrocusto/edit.php
    function edit(){
        $this->setTitle('Edição de Centro de Custo');
        try {
            $this->set('Rhcentrocusto', new Rhcentrocusto((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhcentrocusto', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhcentrocusto
    # (true)redireciona ou (false) renderiza a visão /view/Rhcentrocusto/edit.php
    function post_edit(){
        $this->setTitle('Edição de Centro de Custo');
        try {
            $Rhcentrocusto = new Rhcentrocusto((int) $_POST['codigo']);
            $this->set('Rhcentrocusto', $Rhcentrocusto);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhcentrocusto->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhcentrocusto->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhcentrocusto->save($_POST);
            new Msg(__('Centro de Custo atualizado com sucesso'));
            $this->go('Rhcentrocusto', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhcentrocusto
    # renderiza a /view/Rhcentrocusto/delete.php
    function delete(){
        $this->setTitle('Apagar Centro de Custo');
        try {
            $this->set('Rhcentrocusto', new Rhcentrocusto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhcentrocusto', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhcentrocusto
    # redireciona para Rhcentrocusto/all
    function post_delete(){
        try {
            $Rhcentrocusto = new Rhcentrocusto((int) $_POST['id']);
            $user=Session::get('user');
            $Rhcentrocusto->atualizadopor=$user->id; //salva quem está fazendo
            $Rhcentrocusto->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhcentrocusto->status=3;
            $Rhcentrocusto->save();
            new Msg(__('Centro de Custo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhcentrocusto', 'all');
    }

}