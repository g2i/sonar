<?php
final class RhdivisaoController extends AppController{ 

    # página inicial do módulo Rhdivisao
    function index(){
        $this->setTitle('Visualização de Divisão');
    }

    # lista de Rhdivisaos
    # renderiza a visão /view/Rhdivisao/all.php
    function all(){
        $this->setTitle('Divisão');
        $p = new Paginate('Rhdivisao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhdivisaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdepartamentos',  Rhdepartamento::getList());
    

    }

    # visualiza um(a) Rhdivisao
    # renderiza a visão /view/Rhdivisao/view.php
    function view(){
        $this->setTitle('Divisão');
        try {
            $this->set('Rhdivisao', new Rhdivisao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhdivisao', 'all');
        }
    }

    # formulário de cadastro de Rhdivisao
    # renderiza a visão /view/Rhdivisao/add.php
    function add(){
        $this->setTitle('Cadastro de Divisão');
        $this->set('Rhdivisao', new Rhdivisao);
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdepartamentos',  Rhdepartamento::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhdivisao
    # (true)redireciona ou (false) renderiza a visão /view/Rhdivisao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Divisão');
        $Rhdivisao = new Rhdivisao();
        $this->set('Rhdivisao', $Rhdivisao);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhdivisao->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhdivisao->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhdivisao->save($_POST);
            new Msg(__('Divisão cadastrado com sucesso'));
            $this->go('Rhdivisao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdepartamentos',  Rhdepartamento::getList());
    }

    # formulário de edição de Rhdivisao
    # renderiza a visão /view/Rhdivisao/edit.php
    function edit(){
        $this->setTitle('Edição de Divisão');
        try {
            $this->set('Rhdivisao', new Rhdivisao((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
            $this->set('Rhdepartamentos',  Rhdepartamento::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhdivisao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhdivisao
    # (true)redireciona ou (false) renderiza a visão /view/Rhdivisao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Divisão');
        try {
            $Rhdivisao = new Rhdivisao((int) $_POST['codigo']);
            $this->set('Rhdivisao', $Rhdivisao);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhdivisao->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhdivisao->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhdivisao->save($_POST);
            new Msg(__('Divisão atualizado com sucesso'));
            $this->go('Rhdivisao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdepartamentos',  Rhdepartamento::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhdivisao
    # renderiza a /view/Rhdivisao/delete.php
    function delete(){
        $this->setTitle('Apagar Divisão');
        try {
            $this->set('Rhdivisao', new Rhdivisao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhdivisao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhdivisao
    # redireciona para Rhdivisao/all
    function post_delete(){
        try {
            $Rhdivisao = new Rhdivisao((int) $_POST['id']);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhdivisao->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhdivisao->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhdivisao->status=3;
            $Rhdivisao->save();
            new Msg(__('Divisão apagada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhdivisao', 'all');
    }

}