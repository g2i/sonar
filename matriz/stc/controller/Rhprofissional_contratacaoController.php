<?php
final class Rhprofissional_contratacaoController extends AppController{ 

    # página inicial do módulo Rhprofissional_contratacao
    function index(){
        $this->setTitle('Visualização de Contratação');
    }

    # lista de Rhprofissional_contratacaos
    # renderiza a visão /view/Rhprofissional_contratacao/all.php
    function all(){
        $this->setTitle('Contratações');
        $p = new Paginate('Rhprofissional_contratacao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        //pega o id do profissional para listar
        if($this->getParam('id')){
            $c->addCondition('codigo_profissional','=',$this->getParam('id'));
            $this->set('Profissional', new Rhprofissional($this->getParam('id')));
        }

            $c->setOrder('dtinicial');

        $this->set('Rhprofissional_contratacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhcentrocustos',  Rhcentrocusto::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    

    }

    # visualiza um(a) Rhprofissional_contratacao
    # renderiza a visão /view/Rhprofissional_contratacao/view.php
    function view(){
        $this->setTitle('Visualização de Contratação');
        try {
            $this->set('Rhprofissional_contratacao', new Rhprofissional_contratacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_contratacao', 'all');
        }
    }

    # formulário de cadastro de Rhprofissional_contratacao
    # renderiza a visão /view/Rhprofissional_contratacao/add.php
    function add(){
        $this->setTitle('Cadastro de Contratação');
        $this->set('Rhprofissional_contratacao', new Rhprofissional_contratacao);
        $this->set('Rhcentrocustos',  Rhcentrocusto::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Rhprofissional_funcao',  Rhprofissional_funcao::getList());

    }

    # recebe os dados enviados via post do cadastro de Rhprofissional_contratacao
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_contratacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Contratação');
        $Rhprofissional_contratacao = new Rhprofissional_contratacao();
        $this->set('Rhprofissional_contratacao', $Rhprofissional_contratacao);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhprofissional_contratacao->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhprofissional_contratacao->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        if (!empty($_POST['valorremuneracao'])) {
            $_POST['valorremuneracao'] = str_replace(".", "", $_POST['valorremuneracao']);
            $_POST['valorremuneracao'] = str_replace(",", ".", $_POST['valorremuneracao']);
        }
        try {
            $Rhprofissional_contratacao->save($_POST);
            new Msg(__('Contratação realizada com sucesso !'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhprofissional_contratacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhcentrocustos',  Rhcentrocusto::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # formulário de edição de Rhprofissional_contratacao
    # renderiza a visão /view/Rhprofissional_contratacao/edit.php
    function edit(){
        $this->setTitle('Edição de Contratação');
        try {
            $this->set('Rhprofissional_contratacao', new Rhprofissional_contratacao((int) $this->getParam('id')));
            $this->set('Rhcentrocustos',  Rhcentrocusto::getList());
            $this->set('Rhprofissionais',  Rhprofissional::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhprofissional_contratacao', 'all');
        }
        $this->set('Rhprofissional_funcao',  Rhprofissional_funcao::getList());

    }

    # recebe os dados enviados via post da edição de Rhprofissional_contratacao
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_contratacao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Contratação');
        try {
            $Rhprofissional_contratacao = new Rhprofissional_contratacao((int) $_POST['codigo']);
            $this->set('Rhprofissional_contratacao', $Rhprofissional_contratacao);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhprofissional_contratacao->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhprofissional_contratacao->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            if (!empty($_POST['valorremuneracao'])) {
                $_POST['valorremuneracao'] = str_replace(".", "", $_POST['valorremuneracao']);
                $_POST['valorremuneracao'] = str_replace(",", ".", $_POST['valorremuneracao']);
            }
            if (!empty($_POST['adicional'])) {
                $_POST['adicional'] = str_replace(".", "", $_POST['adicional']);
                $_POST['adicional'] = str_replace(",", ".", $_POST['adicional']);
            }
            $Rhprofissional_contratacao->save($_POST);
            new Msg(__('Contratação atualizada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhprofissional_contratacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhcentrocustos',  Rhcentrocusto::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhprofissional_contratacao
    # renderiza a /view/Rhprofissional_contratacao/delete.php
    function delete(){
        $this->setTitle('Apagar Contratação');
        try {
            $this->set('Rhprofissional_contratacao', new Rhprofissional_contratacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_contratacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhprofissional_contratacao
    # redireciona para Rhprofissional_contratacao/all
    function post_delete(){
        try {
            $Rhprofissional_contratacao = new Rhprofissional_contratacao((int) $_POST['id']);
            $Rhprofissional_contratacao->delete();
            new Msg(__('Contratação apagada com sucesso'), 1);
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprofissional_contratacao', 'all');
    }

}