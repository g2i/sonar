<?php

class IndexController extends AppController
{
    public function index()
    {
        $this->setTitle('Início');

        $prof_ativos = $this->query_count("SELECT COUNT(DISTINCT(P.codigo)) AS total FROM rhprofissional P
                        LEFT JOIN rhprofissional_contratacao C
                        ON C.codigo_profissional = P.codigo
                        WHERE C.tipo <> 'Demissão' AND P.status <> 3
                        AND IF(C.dtFinal IS NULL,C.dtFinal IS NULL,C.dtFinal >= CURDATE())");

        $this->set('prof_ativos',$prof_ativos);

        $demissoes = $this->query_count("SELECT COUNT(DISTINCT(P.codigo)) as total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Demissão' AND C.dtFinal >= DATE_SUB(CURDATE(),INTERVAL 30 DAY)");

        $this->set('demissoes',$demissoes);

        $admissao = $this->query_count("SELECT COUNT(P.codigo) AS total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Admissão' AND
                      C.dtInicial >= DATE_SUB(CURDATE(),INTERVAL 30 DAY) ");

        $this->set('admissao',$admissao);

        $prim_periodo= $this->query_count("SELECT COUNT(P.codigo) AS total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 45 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 45 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('prim_periodo',$prim_periodo);

        $seg_periodo= $this->query_count("SELECT COUNT(P.codigo) AS total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 90 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 90 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('seg_periodo',$seg_periodo);


            $ocorencias_vencidas = $this->query_count("SELECT COUNT(DISTINCT(O.`codigo_profissional`)) AS total
	FROM rhocorrencia O
                LEFT JOIN rhocorrencia_historico H
                ON H.codigo = O.`historico`
                LEFT JOIN rhprofissional P
                ON P.`codigo` = O.`codigo_profissional`
                LEFT JOIN rhprofissional_contratacao C
                ON C.`codigo_profissional` = O.`codigo_profissional`
                WHERE O.`status` <> 3
                AND P.`status`<> 3
                AND C.`tipo`<>'Demissão'
                AND (C.dtFinal IS NULL OR C.dtFinal > CURDATE())
                AND CASE
                    WHEN H.`periodicidade` = 'Semestral' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 6 MONTH)
                    WHEN H.`periodicidade` = 'Anual' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 YEAR)
                    WHEN H.`periodicidade` = 'BiAnual' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 2 YEAR)
                    WHEN H.`periodicidade` = 'Mensal' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 MONTH)
                    WHEN H.`periodicidade` = 'Semanal' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 WEEK)
                    WHEN H.`periodicidade` = 'Diaria' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 DAY)
                  ELSE
                    H.`periodicidade`<>'Nenhuma'
                  END AND P.dtemissao
                  ORDER BY O.`data` DESC
                  ");

        $this->set('ocorencias_vencidas',$ocorencias_vencidas);


        $ocorrencias = new Rhocorrencia();
        $this->set('ocorrencias',$ocorrencias->getTabela_ocorrencia());
    }


    function setor(){
            $setor = $this->query("SELECT S.nome as label, COUNT(P.codigo) as value
                        FROM rhprofissional P
                          LEFT JOIN rhsetor S
                          on S.codigo = P.setor
                        WHERE P.setor IS NOT NULL GROUP BY P.setor");

            echo json_encode($setor);
        exit;
    }

    function seg_periodo(){
        $this->setTitle('Segundo período');
        $periodo = $this->query("SELECT P.codigo,P.nome,F.nome as funcao, C.dtInicial, DATE_ADD(C.dtInicial,INTERVAL 90 DAY) as termino  FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      LEFT join rhprofissional_funcao F
                      on F.codigo = C.funcao

                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 90 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 90 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('seg_periogo',$periodo);
    }
    function prim_periodo(){
        $this->setTitle('Primeiro período');
        $periodo = $this->query("SELECT P.codigo,P.nome,F.nome as funcao, C.dtInicial, DATE_ADD(C.dtInicial,INTERVAL 90 DAY) as termino  FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      LEFT join rhprofissional_funcao F
                      on F.codigo = C.funcao

                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 45 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 45 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('prim_periogo',$periodo);
    }
}