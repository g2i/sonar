<?php
final class Rhstatus extends Record{ 

    const TABLE = 'rhstatus';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rhstatus possui Rhcentrocustos
    * @return array de Rhcentrocustos
    */
    function getRhcentrocustos($criteria=NULL) {
        return $this->hasMany('Rhcentrocusto','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhdepartamentos
    * @return array de Rhdepartamentos
    */
    function getRhdepartamentos($criteria=NULL) {
        return $this->hasMany('Rhdepartamento','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhdivisaos
    * @return array de Rhdivisaos
    */
    function getRhdivisaos($criteria=NULL) {
        return $this->hasMany('Rhdivisao','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhgrupo_histocorres
    * @return array de Rhgrupo_histocorres
    */
    function getRhgrupo_histocorres($criteria=NULL) {
        return $this->hasMany('Rhgrupo_histocorr','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhocorrencia_historicos
    * @return array de Rhocorrencia_historicos
    */
    function getRhocorrencia_historicos($criteria=NULL) {
        return $this->hasMany('Rhocorrencia_historico','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhprofissionais
    * @return array de Rhprofissionais
    */
    function getRhprofissionais($criteria=NULL) {
        return $this->hasMany('Rhprofissional','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhprofissional_dependentes
    * @return array de Rhprofissional_dependentes
    */
    function getRhprofissional_dependentes($criteria=NULL) {
        return $this->hasMany('Rhprofissional_dependente','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhsecaos
    * @return array de Rhsecaos
    */
    function getRhsecaos($criteria=NULL) {
        return $this->hasMany('Rhsecao','status',$criteria);
    }
    
    /**
    * Rhstatus possui Rhsetores
    * @return array de Rhsetores
    */
    function getRhsetores($criteria=NULL) {
        return $this->hasMany('Rhsetor','status',$criteria);
    }
    
    /**
    * Rhstatus possui Usuarios
    * @return array de Usuarios
    */
    function getUsuarios($criteria=NULL) {
        return $this->hasMany('Usuario','status',$criteria);
    }
}