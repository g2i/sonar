<?php
final class Stc_dados_comerciais extends Record{

    const TABLE = 'stc_dados_comerciais';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }


    public static function getResultados($equipe, $inicio,$fim) {
        $db = new MysqlDB();
        $sql = "SELECT DISTINCT(`D`.`equipe`)    AS `equipe`, SUBSTR(`D`.`data_fim_serv`,1,10) AS `data_fim`, COUNT(DISTINCT(`D`.`num_OS`)) AS `num_os`, `P`.`preco`     AS `preco`, `P`.`atividade` AS `atividade`, `P`.`comissao`  AS `comissao`
              FROM (stc_dados_comerciais `D`
                INNER JOIN `stc_tab_precos` `P` ON ((`P`.`cod_sap` = `D`.`cod_sap`)))
              WHERE D.equipe = :equipe AND (`D`.`tipo_conclusao` <> 'Com Rejeição') AND
              STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') >=:inicio
              AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') <=:fim
            GROUP BY `D`.`equipe`,`P`.`atividade`";

        $db->query($sql);

        $db->bind(':equipe',$equipe);
        $db->bind(':inicio',DataSQL($inicio));
        $db->bind(':fim',DataSQL($fim));
        return $db->getResults();
    }

    public static function getPrecos($data){
        $db = new MysqlDB();
        $sql = "SELECT PC.preco AS preco
		FROM stc_dados_comerciais D
                    LEFT JOIN stc_tab_precos PC
                    ON PC.cod_sap = D.`cod_sap`
                    WHERE  tipo_conclusao <> 'Com Rejeição' AND D.equipe <> 'equipe'
                    AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') = :data
                    AND D.`cod_sap` IS NOT NULL
                    GROUP BY D.`num_OS`";

        $db->query($sql);

        $db->bind(':data',DataSQL($data));
        $result = $db->getResults();
$valor=0;
        foreach ($result as $r) {
            $valor+= $r->preco;
        }

        return $valor;
    }
    public static function getPrecos_equipes($inicio,$fim,$equipe){
        $db = new MysqlDB();
        $sql = "SELECT PC.preco AS preco
		FROM stc_dados_comerciais D
                    LEFT JOIN stc_tab_precos PC
                    ON PC.cod_sap = D.`cod_sap`
                    WHERE  tipo_conclusao <> 'Com Rejeição' AND D.equipe <> 'equipe'
                    AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') >= :inicio
                    AND STR_TO_DATE(SUBSTR(D.data_fim_serv,1,10),'%d/%m/%Y') <= :fim
                    AND D.`equipe` = :equipe
                    AND D.`cod_sap` IS NOT NULL
                    GROUP BY D.`num_OS`";

        $db->query($sql);
        $db->bind(':inicio',DataSQL($inicio));
        $db->bind(':fim',DataSQL($fim));
        $db->bind(':equipe',$equipe);
        $result = $db->getResults();
        $valor=0;
        foreach ($result as $r) {
            $valor+= $r->preco;
        }

        return $valor;
    }
}