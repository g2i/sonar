<?php
final class Preco extends Record{

    const TABLE = 'stc_tab_precos';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){

    }


    public function getLocalidade(){
        return $this->belongsTo('Regioes','regiao_id');

    }

}