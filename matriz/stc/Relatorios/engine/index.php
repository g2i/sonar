<?php

//ob_start();        
session_start();
require("localization.php");
require("database_mssql.php");
require("database_mysql.php");
require("database_odbc.php");
require("database_pg.php");
require("database_oracle.php");
require("database_xml.php");
require("handler.php");
//require_once '../../src/Util.php';

$report_key = sti_get_parameter_value("stimulsoft_report_key");
$client_key = sti_get_parameter_value("stimulsoft_client_key");
$parameter_name = sti_get_parameter_value("keys");

$client_data = null;
if (isset($HTTP_RAW_POST_DATA))
    $client_data = $HTTP_RAW_POST_DATA;
if ($client_data == null)
    $client_data = file_get_contents("php://input");

/**
 *  Directory, which contains the localization XML files.
 */
function sti_get_localization_directory() {
    return "localization";
}
/**
 *  Returns .mrt or .mdc file by string ID that was set when running.
 *  If necessary, it is possible to change the code for getting a report by its ID from file or from database.
 */
function sti_get_report($report_key) {
    switch ($report_key) {
        case "1":
            return file_get_contents("../reports/cautela.mrt");
            break;
        case "pos_estoque":
            return file_get_contents("../reports/posicao_estoque.mrt");
            break;

        case "ficha_estoque":
            return file_get_contents("../reports/ficha_estoque.mrt");
            break;
			
		case "ficha_cadastral":
            return file_get_contents("../reports/ficha_cadastral.mrt");
            break;



    }

    //if (file_exists("../reports/$report_key")) return file_get_contents("../reports/$report_key");
    // If there is no need to load the report, then the empty string will be sent
    // If you want to display an error message, please use the following format
    return "Relatorio Nao Encontrado";
}

/**
 *  Saving a report.
 *  Response to the client - error code. Standard codes:
 *      -1: the message box is not shown
 *       0: shows the "Report is successfully saved" message
 *  In other cases shows a window with the defined value
 */
function sti_save_report($report, $report_key, $new_report_flag) {
    // The code for saving a report can be placed here

    if (file_put_contents("../reports/$report_key", $report) === false)
        return "Error when saving a report";
    return "-1";
}

/**
 *  The function for changing values on parameters by their name in the SQL query.
 *  Parameters can be set as {ParamName} in the SQL query.
 *  By default values are taken according to the name of a parameter in the URL string or in the POST data.
 */
function sti_get_parameter($parameter_name, $default_value) {
    switch ($parameter_name) {



        //case "cods": return $_SESSION['cods'];
        //case "cods": return "140,141,142,143";
        //case "cods":
//            $enc = Util::encode64($cods);
//            $dec = Util::decode64($enc);
//           // $dec = Util::decode64($keys);
//
//            return $dec;
//        case "cods": return Util::decode64($keys);			
    }

    return $default_value;
}

/**
 *  Getting the Connection String when requesting the client's Flash application to a database.
 *  In this function you can change the Connection String of a report.
 */
function sti_get_connection_string($connection_type, $connection_string) {
    switch ($connection_type) {
        case "StiSqlDatabase": return "Data Source=SERVER\SQLEXPRESS;Initial Catalog=master;Integrated Security=True";

        //JS------//
        case "StiMySqlDatabase": return "Server=192.175.99.74;Database=g2ioper_js;Port=3306;User=g2ioper_js;Password=js6990;";

        //CULTURA------//
//        case "StiMySqlDatabase": return "Server=184.107.23.204;Database=g2iens_cultura;Port=3306;User=g2iens_cultura;Password=cultura6990;";

    //LOCALHOST------//
//        case "StiMySqlDatabase": return "Server=localhost;Database=financeiro_unic;Port=3306;User=root;Password=;";

        case "StiOdbcDatabase": return "DSN=MS Access Database;DBQ=D:\NWIND.MDB;DefaultDir=D:;DriverId=281;FIL=MS Access;MaxBufferSize=2048;PageTimeout=5;UID=admin;";
        case "StiPostgreSQLDatabase": return "Server=localhost;Database=db_name;Port=5432;User=postgres;Password=postgres;";
    }
    return $connection_string;
}

// Processing client Flash application commands
if (isset($client_key))
    echo sti_client_event_handler($client_key, $report_key, $client_data);
//ob_flush();
?>