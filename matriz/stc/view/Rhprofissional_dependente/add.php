<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">

            <div class="text-right" style="padding-bottom: 20px">
    <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
</div>
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhprofissional_dependente', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-6">
            <label for="tipo" class="required">Tipo<span class="glyphicon glyphicon-asterisk"></span></label>
            <select name="tipo" class="form-control" id="tipo" required>
                <option value="">Selecione:</option>
                <option value="Esposo">Esposo(a)</option>
                <option value="Filho">Filho(a)</option>
                <option value="Guarda Judicial">Guarda Judicial</option>
            </select>
        </div>
        <div class="form-group col-md-offset-6">
            <label for="nome" class="required">Nome<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Rhprofissional_dependente->nome ?>" placeholder="Nome" required>
        </div>
        <div class="form-group col-md-6">
            <label for="dtnascimento" class="required">Data de Nascimento<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="date" name="dtnascimento" id="dtnascimento" class="form-control" value="<?php echo $Rhprofissional_dependente->dtnascimento ?>" placeholder="Data de Nascimento" required>
        </div>
        <div class="form-group col-md-offset-6">
            <label for="registro" class="required">Registro<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="registro" id="registro" class="form-control" value="<?php echo $Rhprofissional_dependente->registro ?>" placeholder="Registro" required>
        </div>
        <div class="form-group col-md-6">
            <label for="cartaovacinacao" class="required">Possui Cartão de Vacinação?<span class="glyphicon glyphicon-asterisk"></span></label>
            <select name="cartaovacinacao" class="form-control" id="cartaovacinacao" required>
                <option value="">Selecione:</option>
                <option value="Sim">Sim</option>
                <option value="Não">Não</option>
            </select>
        </div>
        <div class="form-group col-md-offset-6">
            <label for="declaracaoescolhar">Possui Declaração Escolar?</label>
            <select name="declaracaoescolhar" class="form-control" id="declaracaoescolhar">
                <option value="">Selecione:</option>
                <option value="Sim">Sim</option>
                <option value="Não">Não</option>
            </select>
        </div>
        <!-- passa o parametro da id do profissional -->
        <?php if($this->getParam('modal')){ ?>
            <input type="hidden" name="codigo_profissional" value="<?php echo $this->getParam('id'); ?>" />
        <?php }else{ ?>
            <!-- / passa o parametro da id do profissional -->
            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label for="codigo_profissional">Profissional</label>
                <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                    <?php
                    foreach ($Rhprofissionais as $r) {
                        if ($r->codigo == $Rhprofissional_dependente->codigo_profissional)
                            echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                        else
                            echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                    }
                    ?>
                </select>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
    <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
    <?php if($this->getParam('modal')){ ?>
        <input type="hidden" name="modal" value="1" />
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary" value="salvar">
        </div>
    <?php }else{ ?>
        <div class="text-right">
            <a href="<?php echo $this->Html->getUrl('Rhprofissional_dependente', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="salvar">
        </div>
    <?php } ?>
    <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
</form>
            </div><!-- /ibox content -->
        </div><!-- / ibox -->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->
<script>

</script>