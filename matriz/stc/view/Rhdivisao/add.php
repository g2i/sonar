<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Divisão</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Cadastro de Divisões</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content no-borders">

            <form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhdivisao', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="nome">Nome <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Rhdivisao->nome ?>" placeholder="Nome" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="codigo_departamento" class="required">Departamento<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="codigo_departamento" class="form-control" id="codigo_departamento" required>
                <?php echo '<option value="">Selecione:</option>';  ?>
                <?php
                foreach ($Rhdepartamentos as $r) {
                    if ($r->codigo == $Rhdivisao->codigo_departamento)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhdivisao', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div><!-- /ibox content -->
</div><!-- / ibox -->
        </div><!-- /wrapper-->
</div> <!-- /col-lg 12 -->
</div><!-- /row -->
