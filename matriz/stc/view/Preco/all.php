<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="filtro[interno][atividade]" id="atividade" class="form-control"
                                           value="<?php echo $this->getParam('atividade'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="equipe">Equipe</label>
                                    <input type="text" name="filtro[interno][equipe]" id="equipe" class="form-control"
                                           value="<?php echo $this->getParam('equipe'); ?>">
                                </div>
                                <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                    <label for="regiao_id">Localidade</label>
                                    <select name="filtro[externo][regiao_id]" class="form-control" id="regiao_id">
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($Regiao as $r) {
                                            if ($r->id == $this->getParam('regiao_id'))
                                                echo '<option selected value="' . $r->id. '">' . $r->nome . '</option>';
                                            else
                                                echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">

                                    <button type="button" class="btn btn-default botao-reset" id="btn-clear"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                    <div>
                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Anexo', 'Anexoprofissional', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                        </div>

                        <!-- tabela de resultados -->
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Nome</th>
                                        <th>Cod. SAP</th>
                                        <th>Preço</th>
                                        <th>Comissão</th>
                                        <th>Localidade</th>
                                        <th>Equipe</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    foreach ($Preco as $a) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->atividade, 'Preco', 'view',
                                            array('id' => $a->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->cod_sap, 'Preco', 'view',
                                            array('id' => $a->cod_sap), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->preco, 'Preco', 'view',
                                            array('id' => $a->preco), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->comissao, 'Preco', 'view',
                                            array('id' => $a->comissao), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        echo $this->Html->getLink($a->getLocalidade()->nome, 'Localidade', 'view',
                                            array('id' => $a->regiao_id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        echo $this->Html->getLink($a->equipe, 'Preco', 'view',
                                            array('id' => $a->equipe), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Preco', 'edit',
                                                array('id' => $a->id),
                                                array('class' => 'btn btn-info btn-xs actions'));

                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Preco', 'delete',
                                                array('id' => $a->id),
                                                array('class' => 'btn btn-danger btn-xs actions', 'data-toggle' => 'modal'));
                                            echo '</td>';
                                        echo '</tr>';

                                    }
                                    ?>
                                </table>

                                <!-- menu de paginação -->
                                <div style="text-align:center"><?php echo $nav; ?></div>
                            </div>
                        </div>
                    </div><!-- /ibox content -->
                </div><!-- / ibox -->
            </div><!-- wrapper -->
        </div> <!-- /col-lg 12 -->
    </div><!-- /row -->

    <script>
        /* faz a pesquisa com ajax */
        $(document).ready(function () {
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Anexoprofissional', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Anexoprofissional', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });
        });
    </script>