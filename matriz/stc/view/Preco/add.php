<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Preços</h2>
        <ol class="breadcrumb">
            <li>
               Preço
            </li>
            <li class="active">
                <strong>Cadastrar Preço</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">

                <form method="post" role="form"
                      action="<?php echo $this->Html->getUrl('Preco', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div >

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="regiao_id">Localidade</label>
                            <select name="regiao_id" class="form-control" id="regiao_id">
                                <option value="">Selecione</option>
                                <?php
                                foreach ($Regiao as $r) {
                                    if ($r->id == $Preco->regiao_id)
                                        echo '<option selected value="' . $r->id. '">' . $r->nome . '</option>';
                                    else
                                        echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="equipe">Equipe</label>
                            <input type="text" name="equipe" id="equipe" class="form-control"
                                   value="<?php echo $Preco->equipe ?>" placeholder="Equipe">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="atividade">Atividade</label>
                            <input type="text" name="atividade" id="atividade" class="form-control"
                                   value="<?php echo $Preco->atividade ?>" placeholder="Atividade">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="cod_sap">Código SAP</label>
                            <input type="text" name="cod_sap" id="cod_sap" class="form-control"
                                   value="<?php echo $Preco->cod_sap ?>" placeholder="Código SAP">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="preco">Preço</label>
                            <input type="text" name="preco" id="preco" class="form-control money"
                                   value="<?php echo $Preco->preco ?>" placeholder="Preço">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="comissao">Comissão</label>
                            <input type="text" name="comissao" id="comissao" class="form-control percent"
                                   value="<?php echo $Preco->comissao ?>" placeholder="Comissão">
                        </div>


                        <div class="clearfix"></div>
                    </div>

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Preco', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>
