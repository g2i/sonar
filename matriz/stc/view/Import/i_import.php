<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Dados</h2>
        <ol class="breadcrumb">
            <li>Importação</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Itens da Importação</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="get">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="nome">Data Inicial</label>
                                    <input type="text" name="inicio" id="inicio" class="form-control datePicker"
                                           value="<?php echo $this->getParam('inicio'); ?>">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="nome">Data Final</label>
                                    <input type="text" name="fim" id="fim" class="form-control datePicker"
                                           value="<?php echo $this->getParam('fim'); ?>">
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group col-md-4">
                                    <label for="meta_equipe">Metas Equipe</label>
                                    <input type="text" name="meta_equipe" id="meta_equipe" class="form-control money"
                                           placeholder="Meta por equipe"
                                           value="<?php echo $this->getParam('meta_equipe'); ?>"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="meta_projeto">Metas Projeto</label>
                                    <input type="text" name="meta_projeto" id="meta_projeto" class="form-control money"
                                           placeholder="Meta por Projeto"
                                           value="<?php echo $this->getParam('meta_projeto'); ?>"/>
                                </div>


                                <div class="col-md-4 text-right" style="margin-top: 25px">

                                    <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                       class="btn btn-default"
                                       data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                       title="Recarregar a página"><span
                                            class="glyphicon glyphicon-refresh "></span></a>
                                    <button type="submit" class="btn btn-default" data-tool="tooltip"
                                            data-placement="bottom" title="Pesquisar"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="tabs-container">
                <div class="ibox-content">
                    <div class="ibox float-e-margins">
                        <div class="panel with-nav-tabs panel-primary">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs">
                                    <li class="info active"><a data-toggle="tab" href="#tab-3"> Resultado Geral</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab-4">Por equipes</a></li>
                                    <li class="" id="gafi_"><a data-toggle="tab" href="#tab-5">Gráfico por equipe</a></li>
                                    <li class="" id="incon"><a data-toggle="tab" href="#tab-6" >Inconsistencia</a></li>
                                </ul>
                            </div>

                            <div class="panel-body">
                                <div class="tab-content">
                                    <div id="tab-3" class="tab-pane active">
                                        <div class="panel-body">
                                            <div class="clearfix">
                                                <div class="table-responsive">
                                                    <table class="table table-hover" id="import">
                                                        <tr>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                                                    Data
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                                                    Numero de OS
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                                                    Faturado
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                                                                    Equipes
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                                                                    Média
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                                                                    Méta
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                                                                    Saldo
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                                                                    %
                                                                </a>
                                                            </th>

                                                        </tr>

                                                        <?php
                                                        $med_equip = 0;
                                                        $num_lin = 0;
                                                        $saldo = 0;
                                                        $t_os = 0;
                                                        foreach ($Dados as $i) {
                                                            $num_lin++;
                                                            $preco = Stc_dados_comerciais::getPrecos($i->data_fim);
                                                            $saldo += $preco;
                                                            $t_os += $i->num_os;
                                                            $med_equip += $i->equipes;
                                                            echo '<td>';
                                                            echo $i->data_fim; // atributos HTML opcionais
                                                            echo '</td>';
                                                            echo '<td>';
                                                            echo $i->num_os; // atributos HTML opcionais
                                                            echo '</td>';
                                                            echo '<td>';
                                                            echo number_format($preco, 2, ',', '.'); // atributos HTML opcionais
                                                            echo '</td>';
                                                            echo '<td>';
                                                            echo $i->equipes; // atributos HTML opcionais
                                                            echo '</td>';
                                                            echo '<td>';
                                                            echo number_format($preco / $i->equipes, 2, ',', '.'); // atributos HTML opcionais
                                                            echo '</td>';
                                                            if ($dias > 0) {
                                                                $m = $meta_equipe / $dias;
                                                            } else {
                                                                $m = $meta_equipe;
                                                            }
                                                            echo '<td>';
                                                            echo number_format($m, 2, ',', '.'); // atributos HTML opcionais
                                                            echo '</td>';
                                                            echo '<td>';
                                                            echo number_format($preco - $m, 2, ',', '.'); // atributos HTML opcionais
                                                            echo '</td>';
                                                            $_res = 0;
                                                            if ($m > 0) {
                                                                $_res = ($preco / $i->equipes) / $m;
                                                            }
                                                            echo '<td>';
                                                            echo number_format($_res, 2, ',', '.'); // atributos HTML opcionais
                                                            echo '</td>';


                                                            echo '</tr>';
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><strong><?= $t_os ?></strong></td>
                                                            <td>
                                                                <strong>R$ <?= number_format($saldo, 2, ',', '.') ?></strong>
                                                            </td>
                                                            <td><strong><?= ($med_equip) > 0 ? round($num_lin / $med_equip,2) : 0; ?></strong>
                                                            </td>
                                                            <td colspan="3">&nbsp;</td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-6">
                                                <br/>
                                                <?php if (!empty($Dados)): ?>
                                                    <div class="table-responsive">
                                                        <table class="table table-hover" id="import">
                                                            <tr>
                                                                <td>Meta de Faturamento por Equipe</td>
                                                                <td>
                                                                    R$ <?= number_format($meta_equipe, 2, ',', '.'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Meta de Faturamento Projeto</td>
                                                                <td>
                                                                    R$ <?= number_format($meta_projeto, 2, ',', '.'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total faturado no Período</td>
                                                                <td>R$ <?= number_format($saldo, 2, ',', '.'); ?></td>
                                                            </tr>

                                                            <tr>
                                                                <td>Saldo Real</td>
                                                                <td>
                                                                    R$ <?= number_format($meta_projeto - $saldo, 2, ',', '.'); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total de dias úteis</td>
                                                                <td> <?= $dias; ?></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <strong>Resultado por equipe</strong>
                                                <?php if (!empty($Dados)): ?>
                                                    <div class="table-responsive">
                                                        <table class="table table-hover" id="import">
                                                            <thead>
                                                            <tr>
                                                                <td>Equipe</td>
                                                                <td>Total de OS</td>
                                                                <td>Faturamento</td>
                                                                <td>Peso</td>
                                                            </tr>
                                                            </thead>
                                                            <?php
                                                            $prev_os = 0;
                                                            $prev_fat = 0;
                                                            $prev_med = 0;

                                                            foreach ($Equipes as $e):
                                                                $prev_os += $e->num_os;
                                                                $perc = Stc_dados_comerciais::getPrecos_equipes($this->getParam('inicio'),$this->getParam('fim'),$e->equipe);
                                                                $prev_fat += $perc;
                                                                $prev_med += ($perc / $e->num_os);
                                                                ?>
                                                                <tr>
                                                                    <td> <?= $e->equipe ?></td>
                                                                    <td class="text-left"> <?= $e->num_os ?></td>
                                                                    <td class="text-left">
                                                                        R$ <?= number_format($perc, 2, ',', '.'); ?></td>
                                                                    <td class="text-left"> <?= number_format($perc / $e->num_os, 1, ',', '') ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <th class="text-left"> <?= $prev_os ?></th>
                                                                <th class="text-left">
                                                                    R$ <?= number_format($prev_fat, 2, ',', '.'); ?></th>
                                                                <th class="text-left"> <?= number_format($prev_med, 1, ',', '') ?></th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div id="tab-4" class="tab-pane">
                                        <div class="panel-body">
                                            <div class="clearfix">
                                                <div class="table-responsive">
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id', 'tab' => 'equipes')); ?>'>
                                                                    Equipe
                                                                </a>
                                                            </th>
                                                            <th>
                                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id', 'tab' => 'equipes')); ?>'>
                                                                    Novas Atividades
                                                                </a>
                                                            </th>
                                                            <th class="text-right">
                                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id', 'tab' => 'equipes')); ?>'>
                                                                    Total de OS
                                                                </a>
                                                            </th>

                                                            <th class="text-right">
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id', 'tab' => 'equipes')); ?>'>
                                                                    Preço unitário
                                                                </a>
                                                            </th>
                                                            <th class="text-right">
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id', 'tab' => 'equipes')); ?>'>
                                                                    Faturado
                                                                </a>
                                                            </th>
                                                            <th class="text-right">
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id', 'tab' => 'equipes')); ?>'>
                                                                    Comissão
                                                                </a>
                                                            </th>
                                                            <th class="text-right">
                                                                <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id', 'tab' => 'equipes')); ?>'>
                                                                    Produção
                                                                </a>
                                                            </th>

                                                        </tr>
                                                        <?php
                                                        $tgeral_os = 0;
                                                        $tgeral_fat = 0;
                                                        $tgeral_com = 0;
                                                        $tgeral_prod = 0;
                                                        foreach ($EQ as $e_): ?>
                                                            <tr>
                                                                <td colspan="7">
                                                                    <h2>
                                                                        <a href="Javascript:void(0)"> <?= $e_->equipe ?></a>
                                                                    </h2>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $to_os = 0;
                                                            $to_fat = 0;
                                                            $to_com = 0;
                                                            $to_prod = 0;
                                                            foreach (Stc_dados_comerciais::getResultados($e_->equipe, $this->getParam('inicio'), $this->getParam('fim')) as $result):
                                                                $to_os += $result->num_os;
                                                                $to_fat += ($result->preco * $result->num_os);
                                                                $to_com += $result->comissao;
                                                                $to_prod += $result->comissao * $result->num_os;

                                                                ?>
                                                                <tr>
                                                                    <td>&nbsp;</td>
                                                                    <td><?= $result->atividade ?></td>
                                                                    <td class="text-right"><?= $result->num_os ?></td>
                                                                    <td class="text-right"><?= number_format($result->preco, 2, ',', '.') ?></td>
                                                                    <td class="text-right"><?= number_format($result->preco * $result->num_os, 2, ',', '.') ?></td>
                                                                    <td class="text-right"><?= number_format($result->comissao, 2, ',', '.') ?></td>
                                                                    <td class="text-right"><?= number_format($result->comissao * $result->num_os, 2, ',', '.') ?></td>
                                                                </tr>

                                                            <?php endforeach; ?>

                                                            <?php
                                                            $tgeral_os += $to_os;
                                                            $tgeral_fat += $to_fat;
                                                            $tgeral_com += $to_com;
                                                            $tgeral_prod += $to_prod;
                                                            ?>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <th class="text-right"><?= $to_os ?></th>
                                                                <td>&nbsp;</td>
                                                                <th class="text-right"><?= number_format($to_fat, 2, ',', '.') ?></th>
                                                                <th class="text-right"><?= number_format($to_com, 2, ',', '.') ?></th>
                                                                <th class="text-right"><?= number_format($to_prod, 2, ',', '.') ?></th>
                                                            </tr>

                                                        <?php endforeach; ?>

                                                        <tr class="active">
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <th class="text-right"><?= $tgeral_os ?></th>
                                                            <td>&nbsp;</td>
                                                            <th class="text-right"><?= number_format($tgeral_fat, 2, ',', '.') ?></th>
                                                            <th class="text-right"><?= number_format($tgeral_com, 2, ',', '.') ?></th>
                                                            <th class="text-right"><?= number_format($tgeral_prod, 2, ',', '.') ?></th>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-5" class="tab-pane">

                                        <canvas id="barChart-dados" height="100"></canvas>
                                    </div>
                                    <div id="tab-6" class="tab-pane">
                                        <div class="alert alert-warning" role="alert">
                                            <strong>Regras do sistema</strong><br />
                                            <p>Se tipo de tarefas for diferente DS,(DUC E  MO),LG,MS,VIS</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="table-responsive inconsistente" id="inconsistente">
                                            <table class="table table-hover" >
                                                <thead>
                                                <tr>
                                                    <td>Data Fim</td>
                                                    <td>Tipo de tarefa</td>
                                                    <td>SubTipo Tarefa</td>
                                                    <td>Tipo Fase</td>
                                                    <td>Tipo Conclusão</td>
                                                    <td>Equipe</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    if(!empty($Dados_)):
                                                foreach($Dados_ as $d): ?>
                                                    <tr>
                                                        <td><?=$d->data_fim?></td>
                                                        <td><?=$d->tipo_tarefa?></td>
                                                        <td><?=$d->subtipo_tarefa?></td>
                                                        <td><?=$d->tipo_fase?></td>
                                                        <td><?=$d->tipo_conclusao?></td>
                                                        <td><?=$d->equipe?></td>
                                                    </tr>
                                                <?php endforeach;
                                                    endif;
                                                ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $("#gafi_").click(function () {
            if ($("#inicio").val() != "" && $("#fim").val() != "") {
                $.ajax({
                    type: 'POST',
                    url: root + '/Import/dados_grafico',
                    data: {
                        inicio: $("#inicio").val(),
                        fim: $("#fim").val()
                    },
                    dataType: 'JSON',
                    success: function (txt) {
                        var lab = [];
                        var dat = [];
                        $.each(txt, function (key, val) {
                            lab.push(val.label)
                            dat.push(number_format(val.value, 2, '.', ''));
                        })


                        var barData = {
                            labels: lab,
                            datasets: [
                                {
                                    label: "My Second dataset",
                                    fillColor: "rgba(26,179,148,0.5)",
                                    strokeColor: "rgba(26,179,148,0.8)",
                                    highlightFill: "rgba(26,179,148,0.75)",
                                    highlightStroke: "rgba(26,179,148,1)",
                                    data: dat
                                }
                            ]
                        };


                        var barOptions = {
                            scaleBeginAtZero: true,
                            scaleShowGridLines: true,
                            scaleGridLineColor: "rgba(0,0,0,.05)",
                            scaleGridLineWidth: 1,
                            barShowStroke: true,
                            barStrokeWidth: 2,
                            barValueSpacing: 5,
                            barDatasetSpacing: 1,
                            responsive: true
                        }

                        var ctx = document.getElementById("barChart-dados").getContext("2d");
                        var myNewChart = new Chart(ctx).Bar(barData, barOptions);
                    }
                })
            }
        });
        <?php
        if ($this->getParam('inicio')): ?>
            $('#incon').click(function() {
                var r = true;
                if (r) {
                    r = false;
                    $("div.inconsistente").load(
                        <?php
                            echo '"' . $this->Html->getUrl('Import', 'inconsistente') . '&inicio="+$("#inicio").val()+"&fim=" + $("#fim").val() + " .inconsistente"';
                        ?>
                        , function() {
                            r = true;
                        });
                }
            });
        <?php endif; ?>
    </script>

