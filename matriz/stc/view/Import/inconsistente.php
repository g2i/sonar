<div class="table-responsive inconsistente" id="inconsistente">
    <table class="table table-hover" >
        <thead>
        <tr>
            <td>Data Fim</td>
            <td>Tipo de tarefa</td>
            <td>SubTipo Tarefa</td>
            <td>Tipo Fase</td>
            <td>Tipo Conclusão</td>
            <td>Equipe</td>
        </tr>
        </thead>
        <tbody>
            <?php foreach($Dados_ as $d): ?>
                <tr>
                    <td><?=$d->data_fim?></td>
                    <td><?=$d->tipo_tarefa?></td>
                    <td><?=$d->subtipo_tarefa?></td>
                    <td><?=$d->tipo_fase?></td>
                    <td><?=$d->tipo_conclusao?></td>
                    <td><?=$d->equipe?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <th colspan="6">Total: <?=count($Dados_) ?> </th>

            </tr>
        </tbody>
    </table>
</div>