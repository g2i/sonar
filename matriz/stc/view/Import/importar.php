<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Dados</h2>
        <ol class="breadcrumb">
            <li>Detalhes</li>
            <li class="active">
                <strong>Importação</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" id="form-import"
                          action="<?php echo $this->Html->getUrl('Import', 'post_importar') ?>"
                          enctype="multipart/form-data">
                        <div class="alert alert-info">Todos os campos abaixo devem ser preenchidos!
                        </div>
                        <div>

                            <div class="form-group col-md-6">
                                <label for="limpar">Limpar Importações anteriores</label>
                                <select name="limpar" id="limpar" class="form-control"  >
                                    <option value="">Selecione</option>
                                    <option value="1">Sim</option>
                                    <option value="2">Não</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <label class="required" for="link">Arquivo<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="file" class="link" name="link" id="link"/>

                            </div>
                            <div class="clearfix"></div>

                            <div class="progress" style="display: none;">
                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span class="percent">0% Completos</span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <input type="submit" class="btn btn-primary" value="Importar" onclick="enviar_anexos()">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $(".link").fileinput({showUpload: false, maxFileCount: 10, mainClass: "input-group-md"});

    })
    function enviar_anexos() {
        $('form#form-import').ajaxForm({
            beforeSubmit: function () {
                LoadGif()
            },
            progress: function(evt) {
                console.log(evt)
                if (evt.lengthComputable) {
                    console.log("Loaded " + parseInt( (evt.loaded / evt.total * 100), 10) + "%");
                }
                else {
                    console.log("Length not computable.");
                }
            },
            uploadProgress: function (event, position, total, percentComplete) {
                console.log(event)
                $(".progress").css({"display": "block"});

                var percentVal = percentComplete + '%';
                $("[role='progressbar']").attr('style', 'width: ' + percentVal);
                $(".percent").text(percentVal + " Completos");

            },
            success: function (d) {
                CloseGif();
                if (d != 1) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        message: 'Erro ao enviar arquivo!'
                    });
                } else {
                    location.href = root + '/Import/i_import/';

                }
            }

        });
    }


</script>