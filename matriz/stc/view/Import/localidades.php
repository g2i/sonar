<div class="panel-body localidade">
    <div class="clearfix">
        <div class="table-responsive">
            <table class="table table-hover" id="import">
                <tr>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                            Localidade
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                            Data
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                            Numero de OS
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                            Faturado
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                            Equipes
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                            Média
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                            Méta
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                            Saldo
                        </a>
                    </th>
                    <th>
                        <a href='<?php echo $this->Html->getUrl('Import', 'i_import', array('orderBy' => 'id')); ?>'>
                            %
                        </a>
                    </th>

                </tr>

                <?php
                $med_equip = 0;
                $num_lin = 0;
                $saldo = 0;
                $t_os = 0;
                foreach ($Localidades as $i) {

                    $num_lin++;
                    $preco = Stc_dados_comerciais::getPrecos($i->data_fim,$i->regiao_id);

                    $saldo += $preco;
                    $t_os += $i->num_os;
                    $med_equip += $i->equipes;

                    echo '<td>';
                    echo $this->Html->getLink($i->localidade, 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    echo '<td>';
                    echo $this->Html->getLink($i->data_fim, 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    echo '<td>';
                    echo $this->Html->getLink($i->num_os, 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    echo '<td>';
                    echo $this->Html->getLink(number_format($preco, 2, ',', '.'), 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    echo '<td>';
                    echo $this->Html->getLink($i->equipes, 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    echo '<td>';
                    echo $this->Html->getLink(number_format($preco / $i->equipes, 2, ',', '.'), 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    if ($dias > 0) {
                        $m = $meta_equipe / $dias;
                    } else {
                        $m = $meta_equipe;
                    }
                    echo '<td>';
                    echo $this->Html->getLink(number_format($m, 2, ',', '.'), 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    echo '<td>';
                    echo $this->Html->getLink(number_format($preco - $m, 2, ',', '.'), 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';
                    $_res = 0;
                    if ($m > 0) {
                        $_res = ($preco / $i->equipes) / $m;
                    }
                    echo '<td>';
                    echo $this->Html->getLink(number_format($_res, 2, ',', '.'), 'Import', 'view',
                        array('id' => $i->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                    echo '</td>';


                    echo '</tr>';
                }
                ?>
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td><strong><?= $t_os ?></strong></td>
                    <td>
                        <strong>R$ <?= number_format($saldo, 2, ',', '.') ?></strong>
                    </td>
                    <td><strong><?= ($med_equip) > 0 ? round($num_lin / $med_equip,2) : 0; ?></strong>
                    </td>
                    <td colspan="4">&nbsp;</td>
                </tr>

            </table>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6">
        <br/>
        <?php if (!empty($Localidades)): ?>
            <div class="table-responsive">
                <table class="table table-hover" id="import">
                    <tr>
                        <td>Meta de Faturamento por Equipe</td>
                        <td>
                            R$ <?= number_format($meta_equipe, 2, ',', '.'); ?></td>
                    </tr>
                    <tr>
                        <td>Meta de Faturamento Projeto</td>
                        <td>
                            R$ <?= number_format($meta_projeto, 2, ',', '.'); ?></td>
                    </tr>
                    <tr>
                        <td>Total faturado no Período</td>
                        <td>R$ <?= number_format($saldo, 2, ',', '.'); ?></td>
                    </tr>

                    <tr>
                        <td>Saldo Real</td>
                        <td>
                            R$ <?= number_format($meta_projeto - $saldo, 2, ',', '.'); ?></td>
                    </tr>
                    <tr>
                        <td>Total de dias úteis</td>
                        <td> <?= $dias; ?></td>
                    </tr>
                </table>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
    </div>
    <div class="col-md-6">
        <strong>Resultado por equipe</strong>
        <?php if (!empty($Localidades)): ?>
            <div class="table-responsive">
                <table class="table table-hover" id="import">
                    <thead>
                    <tr>
                        <td>Equipe</td>
                        <td>Localidade</td>
                        <td>Total de OS</td>
                        <td>Faturamento</td>
                        <td>Peso</td>
                    </tr>
                    </thead>
                    <?php
                    $prev_os = 0;
                    $prev_fat = 0;
                    $prev_med = 0;

                    foreach ($Equipes as $e):
                        $prev_os += $e->num_os;
                        $perc = Stc_dados_comerciais::getPrecos_equipes($this->getParam('inicio'),$this->getParam('fim'),$e->equipe,$e->regiao_id, true);
                        $prev_fat += $perc;
                        $prev_med += ($perc / $e->num_os);
                        ?>
                        <tr>
                            <td> <?= $e->equipe ?></td>
                            <td> <?= $e->localidade ?></td>
                            <td class="text-left"> <?= $e->num_os ?></td>
                            <td class="text-left">
                                R$ <?= number_format($perc, 2, ',', '.'); ?></td>
                            <td class="text-left"> <?= number_format($perc / $e->num_os, 1, ',', '') ?></td>
                        </tr>
                    <?php endforeach; ?>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <th class="text-left"> <?= $prev_os ?></th>
                        <th class="text-left">
                            R$ <?= number_format($prev_fat, 2, ',', '.'); ?></th>
                        <th class="text-left"> <?= number_format($prev_med, 1, ',', '') ?></th>
                    </tr>
                </table>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>