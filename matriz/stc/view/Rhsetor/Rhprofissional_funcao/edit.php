
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhprofissional_funcao', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="nome">Nome <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Rhprofissional_funcao->nome ?>" placeholder="Nome" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="status" class="required">Status<span class="glyphicon glyphicon-asterisk"></span></label>
            <select name="status" class="form-control" id="status" required>
                <option value="">Selecione:</option>
                <?php
                foreach ($Rhstatus as $r) {
                    if ($r->codigo == $Rhprofissional_funcao->status)
                        echo '<option selected value="' . $r->codigo . '">' . $r->descricao . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->descricao . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <input type="hidden" name="codigo" value="<?php echo $Rhprofissional_funcao->codigo;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhprofissional_funcao', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
<script>
    $(document).ready(function(){
        $('#status option[value="<?php echo $Rhprofissional_funcao->status ?>"]').prop('selected', true);
    });
</script>