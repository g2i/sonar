<div class="row">
    <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="text-right" style="padding-bottom: 20px">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Rhprofissional', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja desativar o registro <strong><?php echo $Rhprofissional->nome; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="codigo" value="<?php echo $Rhprofissional->codigo; ?>">
        <a href="<?php echo $this->Html->getUrl('Rhprofissional', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Desativar">
    </div>
</form>
                    </div> <!-- / ibox content -->
            </div>
        </div>
    </div>