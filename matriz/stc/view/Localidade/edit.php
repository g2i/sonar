<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Regioes</h2>
        <ol class="breadcrumb">
            <li>Regioes</li>
            <li class="active">
                <strong>Editar Regioes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Localidade', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label class="required" for="nome">Nome <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Regioes->nome ?>" placeholder="Nome" required>
                            </div>

                        </div>
                        <input type="hidden" name="id" value="<?php echo $Regioes->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Localidade', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>