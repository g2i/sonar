<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Seção</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Edição de Seções</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">


                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhsecao', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="nome">Nome <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Rhsecao->nome ?>" placeholder="Nome" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="codigo_divisao" class="required">Divisão<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="codigo_divisao" class="form-control" id="codigo_divisao" required>
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Rhdivisaos as $r) {
                                        if ($r->codigo == $Rhsecao->codigo_divisao)
                                            echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                                        else
                                            echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="status" class="required">Status<span
                                        class="glyphicon glyphicon-asterisk"></span> </label>
                                <select name="status" class="form-control" id="status" required>
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Rhstatus as $r) {
                                        if ($r->codigo == $Rhsecao->status)
                                            echo '<option selected value="' . $r->codigo . '">' . $r->descricao . '</option>';
                                        else
                                            echo '<option value="' . $r->codigo . '">' . $r->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="codigo" value="<?php echo $Rhsecao->codigo; ?>">

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Rhsecao', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar</button>
                            <input type="submit" id="validForm" style="display: none;" />
                        </div>
                    </form>

                </div>
                <!-- /ibox content -->
            </div>
            <!-- / ibox -->
        </div>
        <!-- /wrapper-->
    </div>
    <!-- /col-lg 12 -->
</div><!-- /row -->