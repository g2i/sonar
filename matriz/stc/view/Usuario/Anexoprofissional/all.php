<?php if($this->getParam('modal')){ ?>
<?php }else{ ?>
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="nome">Nome</label>
            <input type="text" name="filtro[interno][nome]" id="nome" class="form-control" value="<?php echo $this->getParam('nome'); ?>">
        </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
<div>
    <?php } ?>
    <?php if($this->getParam('modal')){ ?>
        <div class="text-right">
            <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Anexoprofissional","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                    <span class="img img-add"></span> Novo Anexo</a></p>
        </div>

    <?php }else{ ?>
    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Anexo', 'Anexoprofissional', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>
    <?php } ?>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexoprofissional', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexoprofissional', 'all', array('orderBy' => 'codigo_profissional')); ?>'>
                        Profissional
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Anexoprofissionais as $a) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($a->nome, 'Anexoprofissional', 'view',
                    array('id' => $a->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($a->getRhprofissional()->nome, 'Rhprofissional', 'view',
                    array('id' => $a->getRhprofissional()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                if($this->getParam('modal')==1){
                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip2" title="Deletar Anexo" class="btn btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Anexoprofissional", "delete", array("ajax" => true, "modal" => "1", 'id' => $a->codigo)).'\',\'go\')">
    <span class="img img-remove"></span></a>';
                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo $this->Html->getLink('<span class="img img-down"></span> ', 'Download', 'baixar',
                        array('u' => Cript::cript($a->anexo)),
                        array('class' => 'btn btn-sm'));
                    echo '</td>';
                }else {
                    echo '<td width="50">';
                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Anexoprofissional', 'edit',
                        array('id' => $a->codigo),
                        array('class' => 'btn btn-warning btn-sm'));
                    echo '</td>';
                    echo '<td width="50">';
                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Anexoprofissional', 'delete',
                        array('id' => $a->codigo),
                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                    echo '</td>';
                    echo '</tr>';
                }
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Anexoprofissional', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Anexoprofissional', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>