
<div class="wrapper wrapper-content ibox" style="padding-left: 300px; padding-right: 300px; padding-top: 50px">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form class="form" method="post" action="<?php echo $this->Html->getUrl('Usuario', 'resetar_senha') ?>">
                    <h1>Resetar senha</h1>
                    <div class="form-group">
                        <label for="senha" class="required">Senha <span class="glyphicon glyphicon-asterisk"></span>
                        </label>
                        <input type="password" name="senha" id="senha" placeholder="Senha" class="form-control senha" required/>
                    </div>
                    <div class="form-group">
                        <label for="con_senha" class="required">Confirmar a Senha<span class="glyphicon glyphicon-asterisk"></span>
                        </label>
                        <input type="password" name="con_senha" id="con_senha" placeholder="Confirmar a Senha" class="form-control con_senha" required/>
                    </div>
                    <div class="text-right">
                        <input type="hidden" name="id" value="<?php echo $Usuario->id; ?>">
<!--                        <a href="--><?php //echo $this->Html->getUrl('Login', 'login') ?><!--" class="btn btn-default"-->
<!--                           data-dismiss="modal">Cancelar</a>-->
                        <input type="submit" class="btn btn-primary" onclick="return ChecarSenha()" value="Resetar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function ChecarSenha(){
        if($(".senha").val()!=$(".con_senha").val()){
            OpenMensagem("Alerta","As senhas n�o s�o iguais!");
            return false;
        }
        return true;
    }
</script>