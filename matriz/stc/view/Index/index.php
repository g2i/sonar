<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">ativos</span>
                        <h5>Profissionais</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php echo $prof_ativos->total; ?></h1>

                        <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                        <small>Profissionais</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">últimos 30 dias</span>
                        <h5>Demissões </h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php echo $demissoes->total; ?></h1>

                        <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                        <small><?php echo ($demissoes->total > 1) ? "Profissionais" : "Profissional"; ?></small>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">últimos 30 dias</span>
                        <h5>Admissões</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php echo $admissao->total; ?></h1>

                        <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                        <small><?php echo ($admissao->total > 1) ? "Profissionais" : "Profissional"; ?></small>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">vencidas</span>
                        <h5>Ocorrências</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php echo $ocorencias_vencidas->total; ?></h1>

                        <div class="stat-percent font-bold text-danger"> <i class="fa fa-level-down"></i></div>
                        <small>ocorrências</small>
                    </div>
                </div>
            </div>


        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Gráfico por Setor</h5>
                </div>
                <div class="ibox-content">
                    <div id="morris-donut-chart"></div>
                </div>
            </div>
        </div>


    </div>
</div>

<script>
    $(function() {
        $.ajax({
            type:'POST',
            url:root+'/Index/setor',
            success:function(txt) {
                Morris.Donut({
                    element: 'morris-donut-chart',
                    data: jQuery.parseJSON(txt),
                    resize: true,
                    colors: ['#87d6c6', '#54cdb4', '#1ab394', '#9de8d8', '#b7eee2']
                });
            }
        });

    });
</script>