<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Funções</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Funções</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">

                <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="nome">Nome</label>
            <input type="text" name="filtro[interno][nome]" id="nome" class="form-control" value="<?php echo $this->getParam('nome'); ?>">
        </div>
                <div class="col-md-3 form-group">
                    <label for="status">Status</label>
                    <select name="filtro[externo][status]" class="form-control" id="status">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Rhstatus as $r): ?>
                            <?php echo '<option value="' . $r->codigo . '">' . $r->descricao . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
<div>
    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="img img-add"></span> Nova Função', 'Rhprofissional_funcao', 'add', NULL, array('class' => 'btn btn-default')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional_funcao', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional_funcao', 'all', array('orderBy' => 'nome')); ?>'>
                        Status
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Rhprofissional_funcaos as $r) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($r->nome, 'Rhprofissional_funcao', 'view',
                    array('id' => $r->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->getRhstatus()->descricao, 'Rhstatus', 'view',
                    array('id' => $r->getRhstatus()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                echo $this->Html->getLink('<span class="img img-edit"></span> ', 'Rhprofissional_funcao', 'edit',
                    array('id' => $r->codigo), 
                    array('class' => 'btn btn-sm'));
                echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                echo $this->Html->getLink('<span class="img img-remove"></span> ', 'Rhprofissional_funcao', 'delete',
                    array('id' => $r->codigo), 
                    array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>
</div><!-- /ibox content -->
                </div><!-- / ibox -->
            </div><!-- /wrapper-->
        </div> <!-- /col-lg 12 -->
    </div><!-- /row -->

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Rhprofissional_funcao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Rhprofissional_funcao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>