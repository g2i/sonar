<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php //$this->getHeaders();?>
</head>
<style>
 @page {
  /*size: A4;*/
  margin: 0mm 17mm 0mm 17mm;
}
.tabela-medidas {
  margin-top: -100px;
  font-size: 12px;
  
}
.tabela-medidas th {
  text-align: left !important;
}
.tabela-medidas .title{
  /*background-color:  lightblue;*/
  -webkit-print-color-adjust: exact;
  padding: 7px;
  display:inline;
}
.header {
  padding-top: 160px;
  font-size: 12px;
  
}
footer {
  height: 100px;
}
/* css padrão - não deletar */
table.report-container {
  page-break-after:always;
}
thead.report-header {
  display:table-header-group;
}
tfoot.report-footer {
  display:table-footer-group;
} 
.td-paciente {
  min-width: 200px;
}
.td-localidade {
  max-width: 150px;
}
div p{
text-align: right;
}

td {
    text-align: left;
    padding: 12px; 
    height: 10px;
    border-bottom: 1px solid #D5D5D5;

}

tr:nth-child(even) {background-color: #f2f2f2;}

.footer {
    position: fixed;

    bottom: 0;
  
    text-align: center;

</style>
<body>
<style type="text/css">
    .container {
        max-width: 1000px;
        margin: auto;
    }
</style>

    <div class="container">
    <img src="<?php echo SITE_PATH ?>/lib/css/images/print_cabecalho.png" title="Upload Foto" id="charFoto" 
    style="width:650px;cursor:pointer; position: fixed;">

        <?php $this->getContents(); ?>
       <footer class="footer"> <img src="<?php echo SITE_PATH ?>/lib/css/images/print_footer.png" title="Upload Foto" id="charFoto" 
    style="width:700px;cursor:pointer;"></footer> 
    </div>

</body>

</html>
