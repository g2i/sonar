<li class="nav-header">
    <div class="dropdown profile-element">
    <span>
    <?php echo(file_exists(SITE_PATH . "/content/img/" . sprintf("%06d", @Session::get("user")->id) . ".png") ? '<img class="img-circle" src="' . SITE_PATH . '/content/img/' . sprintf("%06d", @Session::get("user")->id) . '.png" alt="Foto"/>' : '<img class="img-circle" src="' . SITE_PATH . '/content/img/empty-avatar.png" alt="Foto"/>') ?>
    </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <span class="clear"> <span class="block m-t-xs"> <strong
                        class="font-bold"><?php echo @Session::get("user")->nome; ?> <b
                            class="caret"></b></span> </span></strong> </a>
        <ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><?php echo $this->Html->getLink('Sair', 'Login', 'logout'); ?></li>
        </ul>
    </div>
    <div class="logo-element">
        G2i
    </div>
</li>
<li <?php echo(CONTROLLER == "Index"? 'class="active"' : ''); ?>>
    <?php echo $this->Html->getLink('<i class="fa fa-thumb-tack"></i> SESMET', 'Index', 'index'); ?>
</li>
<!-- 
<li <?php echo(CONTROLLER == "Rhprofissional"? 'class="active"' : ''); ?>>
   <//?php echo $this->Html->getLink('<i class="fa fa-users"></i> Profissional', 'Rhprofissional', 'all'); ?>
    <a href="#"><i class="fa fa-plus-square"></i><span class="nav-label">Profissional</span><span
            class="fa arrow"></span></a>
    <ul class="nav nav-third-level">
        <li <?php echo(CONTROLLER == "Rhprofissional" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-bars"></i>Funcionarios', 'Rhprofissional', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhprofissional"  ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-users"></i> Cadastro', 'Rhprofissional', 'add'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhprofissional"  ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-users"></i> Ocorrencias', 'Rhprofissional', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhprofissional" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-file-text"></i> Anexos', 'Anexoprofissional', 'index'); ?>
        </li>
    </ul>

<li <?php echo((CONTROLLER == "Rhcentrocusto" || CONTROLLER == "Rhdepartamento" || CONTROLLER == "Rhdivisao" || CONTROLLER == "Rhsecao" || CONTROLLER == "Rhsetor"|| CONTROLLER == "Rhgrupo_histocorr" || CONTROLLER == "Rhocorrencia_historico" || CONTROLLER == "Rhprofissional_funcao" || CONTROLLER == "Usuario") ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-plus-square"></i><span class="nav-label">Cadastros</span><span
            class="fa arrow"></span></a>
    <ul class="nav nav-third-level">
        <li <?php echo(CONTROLLER == "Rhcentrocusto" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Centro de Custo', 'Rhcentrocusto', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhdepartamento"  ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Departamento', 'Rhdepartamento', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhdivisao" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Divisão', 'Rhdivisao', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhsecao" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Seção', 'Rhsecao', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhsetor"  ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Setor', 'Rhsetor', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhgrupo_histocorr"  ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i>Ocorrência - Grupo', 'Rhgrupo_histocorr', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhocorrencia_historico"  ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Ocorrência - Histórico', 'Rhocorrencia_historico', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Rhprofissional_funcao" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Funções', 'Rhprofissional_funcao', 'all'); ?>
        </li>
       <li <?php echo(CONTROLLER == "Usuario" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Usuarios', 'Usuario', 'all'); ?>
        </li> 
    </ul>
</li>
<li <?php echo(CONTROLLER == "Rhprofissional_funcao" ? 'class="active"' : ''); ?>>
    <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i>Requisitos das Funções', 'Rhprofissional_funcao', 'all'); ?>
</li>
<li <?php echo(CONTROLLER == "Rhrelatorio" ? 'class="active"' : ''); ?>>
    <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Listagens', 'Rhrelatorio', 'all'); ?>
</li>
<li>
    <a href="<?= SITE_PATH . "/uploads/" ?>Manual.pdf" target="_blank"><i  class="fa fa-file-pdf-o"></i> Manual</a>
</li>
<li <?php /*echo(CONTROLLER == "Financeiro" ? 'class="active"' : ''); */?>>
    <a href="#"><i class="fa fa-th-large"></i><span class="nav-label">Financeiro</span><span
            class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php /*echo(CONTROLLER == "Compra" && ACTION == "all" ? 'class="active"' : ''); */?>>
            <?php /*echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Compra', 'all'); */?>
        </li>
        <li <?php /*echo(CONTROLLER == "Compra" && ACTION == "add" ? 'class="active"' : ''); */?>>
            <?php /*echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Compra', 'add'); */?>
        </li>
    </ul>
</li>
<li <?php /*echo(CONTROLLER == "Imagens" ? 'class="active"' : ''); */?>>
    <a href="#"><i class="fa fa-th-large"></i><span class="nav-label">Logistica</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php /*echo(CONTROLLER == "Imagens" && ACTION == "all" ? 'class="active"' : ''); */?>>
            <?php /*echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Imagens', 'all'); */?>
        </li>
        <li <?php /*echo(CONTROLLER == "Imagens" && ACTION == "add" ? 'class="active"' : ''); */?>>
            <?php /*echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Imagens', 'add'); */?>
        </li>
    </ul>
</li>
<li <?php /*echo(CONTROLLER == "Marcas" ? 'class="active"' : ''); */?>>
    <a href="#"><i class="fa fa-th-large"></i><span class="nav-label">Almoxarifado</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php /*echo(CONTROLLER == "Marcas" && ACTION == "all" ? 'class="active"' : ''); */?>>
            <?php /*echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Marcas', 'all'); */?>
        </li>
        <li <?php /*echo(CONTROLLER == "Marcas" && ACTION == "add" ? 'class="active"' : ''); */?>>
            <?php /*echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Marcas', 'add'); */?>
        </li>
    </ul>
</li>-->










