<!DOCTYPE html>
<html lang="pt-br" xmlns:h="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $this->getHeaders(); ?>
    <style>
        body {

        }

        .starter-template {
            padding: 0px 15px;
        }
    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php
    $usuario=Session::get('user');
    if($usuario !== null){
?>
<div class="col-md-2 col-sm-3 col-xs-12 navbar-black">
    <ul class="menu nav">
        <!-- parte superior do menu -->
        <li class="header-profile">
            <div class="profile-element">
                <img src="<?php echo SITE_PATH; ?>/template/images/profilesmall.jpg" alt="Imagem" class="img-circle"  width="60" height="60"></span>
                <a>
                                    <span class="clear"> <span class="block m-t-xs">
                                                <?php

                                                if ($usuario !== null) {
                                                   // new Msg( $_POST['login']);
                                                    echo $usuario->nome;
                                                } else {
                                                    echo '<strong class="<font-bold">  Deslogado </strong>';
                                                }
                                                ?>
                                    </span></span>
                </a>
            </div>
        </li>
        <!-- /parte superior do menu -->
        <li><?php echo $this->Html->getLink(__('Início'), Config::get('indexController'), Config::get('indexAction')); ?></li>
        <?php include 'template/menu.php' ?>
    </ul>
</div>

<div class="col-md-10 col-sm-9 col-xs-12 container-block">
    <div class="navbar navbar-inverse hidden-xs" role="navigation">
        <div class="container-fluid">
            <ul class="nav navbar-nav navbar-left">
              <!-- <li><a class="navbar-minimalize btn btn-primary " href="#"><i class="glyphicon glyphicon-th-list"></i> </a></li>  Botão para esconder o menu        -->
               <li><a class="navbar-brand" href="#"><?php echo Config::get('titulo'); ?></a></li>
            </ul>
                <ul class="nav navbar-nav navbar-right">
                <li>
                    <a>
                    <i class="glyphicon glyphicon-bell"></i>
                    <span class="label label-primary">8</span>
                    </a>
                </li>
                <li>
                        <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-share"></span></span> Logout','Login','logout');?>

                </li>
            </ul>
        </div>
    </div>
    <?php } ?>
    <div class="container-fluid">
        <div class="starter-template">
            <?php $this->getContents(); ?>
        </div>
    </div>
    <!-- /.container -->
</div>
<div class="clearfix"></div>

<!-- Generic Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div style="text-align:center">
                <img src="<?php echo SITE_PATH; ?>/template/default/images/loading.gif" alt="LazyPHP">
            </div>
        </div>
    </div>
</div>
</body>

</html>
