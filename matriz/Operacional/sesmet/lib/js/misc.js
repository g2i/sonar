prevent_ajax_view = true;

$(document).ready(function() {
    $('button[data-dismiss="modal"]').click(function() {
        //history.go(-1);
    });
    
    $(document).on('hidden.bs.modal', function(e) {
        $(e.target).removeData('bs.modal');
    });

    $('body').on('click', 'a[data-toggle=modal]', function(event){
        event.preventDefault();
        modalharef = $(this).attr('href');
        if (modalharef.indexOf("?") == -1)
            modalharef += '?'
        modalharef += '&ajax=true';
        $(this).attr('href', modalharef);

        if(modalharef.indexOf("first")!=-1){
            Acrescentar(modalharef);
        }
        if (!$(this).attr('data-target')) {
            $(this).attr('data-target', '#modal');
        }
    });

   //function para o btn de pesquisa em form 
    $('.filtros').find('#buscar-filtro').click(function(){
        let form_action = $(this).closest('form').attr('action');
        let form_serialize = $(this).closest('form').serialize();
        let url_completa = form_action + '?' + form_serialize;
        carregaTabelaResponsiva(url_completa);
    });
    
//function para paginar por ajax
    $(document).on('click','.pagination a, .table thead a', function(e){
        e.preventDefault();
        let url = $(this).attr('href');

        if(url != "")
        {            
            carregaTabelaResponsiva(url);
        }
        return false;
    });

});
//function para pegar as URLs 
function carregaTabelaResponsiva(url) {
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: () => {
        },
        success: (data) => {
            let conteudo = $('<div>').append(data).find('.table-responsive');
            $(".table-responsive").html(conteudo);
        },
        complete: () => {
        },
        error: () => {
        },
    });
}
