<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">
                <legend>Centro de Custo</legend>

                <p><strong>Nome</strong>: <?php echo $Rhcentrocusto->nome; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Rhcentrocusto->getRhstatus()->descricao, 'Rhstatus', 'view',
                        array('id' => $Rhcentrocusto->getRhstatus()->codigo), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
            <!-- /ibox content -->
        </div>
        <!-- / ibox -->
    </div>
    <!-- /col-lg 12 -->
</div><!-- /row -->
