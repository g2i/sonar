<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content no-borders">

                <!--  todos os if's e else's em php são necessários para navegação entre MODAIS -->

                <div class="text-right" style="padding-bottom: 20px">
                    <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                </div>
                <?php if ($this->getParam('modal')) { ?>
                <?php } else { ?>
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                                <div class="col-md-3 form-group">
                                    <label for="codigo_profissional">Profissional</label>
                                    <select name="filtro[externo][codigo_profissional]" class="form-control"
                                            id="codigo_profissional">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Rhprofissionais as $r): ?>
                                            <?php echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tipo">Tipo</label>
                                    <input type="text" name="filtro[interno][tipo]" id="tipo" class="form-control"
                                           value="<?php echo $this->getParam('tipo'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                           value="<?php echo $this->getParam('nome'); ?>">
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-impressao"><span
                                            class="glyphicon glyphicon-print"></span></button>
                                    <button type="button" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                <?php } ?>
                <div>
                    <!-- botão de cadastro dentro da modal -->
                    <?php if ($this->getParam('modal')) { ?>
                        <div class="text-left col-md-6 ">
                            <h3 class="text-left"><?php echo $Profissional->nome; ?></h3>
                        </div>
                        <div class="text-right col-md-6" >
                            <p><a href="Javascript:void(0)" class="btn btn-default"
                                  onclick="Navegar('<?php echo $this->Html->getUrl("Rhprofissional_dependente", "add", array('modal' => 1, 'ajax' => true, 'id' => $this->getParam('id'))) ?>','go')">
                                    <span class="img img-add"></span> Novo Dependente</a></p>
                        </div>

                    <?php } else { ?>
                        <!-- / botão de cadastro dentro da modal  e senão for modal , rola esse botão definido a baixo-->
                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p><?php echo $this->Html->getLink('<span class="img img-add"></span> Novo Dependente', 'Rhprofissional_dependente', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                        </div>
                    <?php } ?>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a>
                                            Tipo
                                        </a>
                                    </th>
                                    <th>
                                        <a>
                                            Nome
                                        </a>
                                    </th>
                                    <th>
                                        <a>
                                            Data de Nascimento
                                        </a>
                                    </th>
                                    <th>
                                        <a>
                                            Status
                                        </a>
                                    </th>
                                    <th>
                                        <a>
                                            Profissional
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Rhprofissional_dependentes as $r) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($r->tipo, 'Rhprofissional_dependente', 'view',
                                        array('id' => $r->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($r->nome, 'Rhprofissional_dependente', 'view',
                                        array('id' => $r->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';

                                    echo $this->Html->getLink(DataBR($r->dtnascimento), 'Rhprofissional_dependente', 'view',
                                        array('id' => $r->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($r->getRhstatus()->descricao, 'Rhstatus', 'view',
                                        array('id' => $r->getRhstatus()->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($r->getRhprofissional()->nome, 'Rhprofissional', 'view',
                                        array('id' => $r->getRhprofissional()->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    if ($this->getParam('modal') == 1) {
                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar Dependente" class="btn btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Rhprofissional_dependente", "edit", array("ajax" => true, "modal" => "1", 'id' => $r->codigo, 'profissional' => $r->getRhprofissional()->codigo)) . '\',\'go\')">
    <span class="img img-edit"></span></a>';
                                        echo '</td>';
                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar Ocorrência" class="btn btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Rhprofissional_dependente", "delete", array("ajax" => true, "modal" => "1", 'id' => $r->codigo)) . '\',\'go\')">
    <span class="img img-remove"></span></a>';
                                        echo '</td>';
                                    } else {
                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo $this->Html->getLink('<span class="img img-edit"></span> ', 'Rhprofissional_dependente', 'edit',
                                            array('id' => $r->codigo),
                                            array('class' => 'btn btn-warning btn-sm'));
                                        echo '</td>';
                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo $this->Html->getLink('<span class="img img-remove"></span> ', 'Rhprofissional_dependente', 'delete',
                                            array('id' => $r->codigo),
                                            array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';
                                    }
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>
                </div>
                <!-- /ibox content -->
            </div>
            <!-- / ibox -->
            </div>
            <!--wrapper -->
        </div>
        <!-- /col-lg 12 -->
    </div>
    <!-- /row -->

    <script>
        //toltip
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
        //fim toltip
        /* faz a pesquisa com ajax */
        $(document).ready(function () {
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Rhprofissional_dependente', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Rhprofissional_dependente', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });
        });
    </script>