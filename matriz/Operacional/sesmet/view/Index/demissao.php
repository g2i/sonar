<div class="col-lg-12">
    <div class="ibox-content">
        <div class="col-lg-9">
            <h2>Demissão</h2>
            <ol class="breadcrumb">
                <li>Demissão</li>
                <li class="active">
                    <strong>Últimos 30 dias</strong>
                </li>
            </ol>
        </div>
        <?php if ($this->getParam('modal') || $this->getParam('ajax')) { ?>
            <div class="col-lg-1 pull-right">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

        <?php } ?>
        <div class="clearfix"></div>
    </div>
</div>


<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Colaboradores</h5>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Data Demisssão</th>
                        <th>Função</th>
                        <th>Localidade</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        foreach($demissao as $a) { 
                            echo '<tr>';
                            echo '<td>'.$a->nome.'</td>';
                            echo '<td>'.DataBR($a->contratacao).'</td>';
                            echo '<td>'.$a->funcao.'</td>';
                            echo '<td>'.$a->local_trabalho.'</td>';
                            echo '</tr>';
                        }
                    ?>            
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>