<div class="col-lg-12">
    <div class="ibox-content">
        <div class="col-lg-9">
            <h2>Período de Experiência</h2>
            <ol class="breadcrumb">
                <li>experiência</li>
                <li class="active">
                    <strong>a vencer</strong>
                </li>
            </ol>
        </div>
        <?php if ($this->getParam('modal') || $this->getParam('ajax')) { ?>
            <div class="col-lg-1 pull-right">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

        <?php } ?>
        <div class="clearfix"></div>
    </div>
</div>


<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Colaboradores</h5>
        </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nome</th>                        
                        <th>Data Contratação</th>
                        <th>Função</th>
                        <th>Localidade</th>
                        <th>Término do periodo</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach($prim_periogo as $o) {
                        echo '<tr>';
                        echo '<td>'.$o->nome.'</td>';                        
                        echo '<td>'.ConvertData($o->dtInicial).'</td>';
                        echo '<td>'.$o->funcao.'</td>';
                        echo '<td>'.$o->local_trabalho.'</td>';
                        echo '<td>'.ConvertData($o->termino).'</td>';
                        echo '</tr>';
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>