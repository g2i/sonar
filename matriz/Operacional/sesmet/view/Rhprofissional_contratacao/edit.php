<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">
<form method="post" role="form" id="formRhProfissionalContratacao" action="<?php echo $this->Html->getUrl('Rhprofissional_contratacao', 'edit') ?>">
    <div class="
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ">
        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
    </div>
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <legend>Dados da Contratação</legend>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="tipo" class="required">Tipo<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select class="form-control" name="tipo" id="tipo" required>
                <option value="">Selecione:</option>
                <option value="Admissão">Admissão</option>
                <option value="Demissão">Demissão</option>
                <option value="Movimentação">Movimentação</option>
                <option value="Cancelamento">Cancelamento</option>
            </select>
        </div>
        
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="funcao" class="required">Função<span class="glyphicon glyphicon-asterisk"></span></label>
            <select name="funcao" class="form-control" id="funcao" required>
                <option value="">Selecione:</option>
                <?php
                foreach ($Rhprofissional_funcao as $r) {
                    if ($r->codigo == $Rhprofissional_contratacao->funcao)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>

       <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="adicionais" class="required">Adicional</label>
            <select class="form-control" name="adicionais" id="adicionais" >
            <option value="">Selecione:</option>
                <?php if ($Rhprofissional_contratacao->adicional == 30) { ?>
                    <option selected value="1">Sim</option>
                    <option value="2">Não</option>
                <?php } else { ?>
                    <option value="1">Sim</option>
                    <option selected value="2">Não</option>
                <?php } ?>
            </select>
        </div>
         
    
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="tiporemuneracao" class="required">Tipo de Remuneração<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select class="form-control" name="tiporemuneracao" id="tiporemuneracao" required>
                <option value="">Selecione:</option>
                <option value="Diaria">Diária</option>
                <option value="Semanal">Semanal</option>
                <option value="Quinzenal">Quinzenal</option>
                <option value="Mensal">Mensal</option>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="jornada" class="required">Jornada de Trabalho<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="jornada" id="jornada" class="form-control" value="<?php echo $Rhprofissional_contratacao->jornada ?>" placeholder="Jornada de Trabalho" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="valorremuneracao" class="required">Salário Atual<span class="glyphicon glyphicon-asterisk"></span> </label>
            <input type="text" name="valorremuneracao" id="valorremuneracao"  class="form-control" value="<?php echo $Rhprofissional_contratacao->valorremuneracao ?>" placeholder="Valor da Remuneração" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label>Salário Inicial</label>
            <input type="text"  class="form-control" value="<?php echo 'R$ '; echo $Rhprofissional_contratacao->salario_inicial ?>" readonly>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dtInicial" class="required">Data Inicial<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="date" name="dtInicial" id="dtInicial" class="form-control" value="<?php echo $Rhprofissional_contratacao->dtInicial ?>" placeholder="DtInicial" required>
        </div>

        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="valetransporte" class="required">Vale Transporte ?<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select class="form-control" name="valetransporte" id="valetransporte" required>
                <option value="">Selecione:</option>
                <option value="SIM">Sim</option>
                <option value="NAO">Não</option>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="valetransporteqtdia" class="required">VT por Dia<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="number" name="valetransporteqtdia" id="valetransporteqtdia" class="form-control" value="<?php echo $Rhprofissional_contratacao->valetransporteqtdia ?>" placeholder="Quantidade de VT ao dia" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dtFinal">Data Final</label>
            <input type="date" name="dtFinal" id="dtFinal" class="form-control" value="<?php echo $Rhprofissional_contratacao->dtFinal ?>" placeholder="DtFinal">
        </div>
        <div class="form-group col-xs-12">
            <label for="horario">Horários</label>
            <input type="text" name="horario" id="horario" class="form-control" value="<?php echo $Rhprofissional_contratacao->horario ?>" placeholder="Horários">
        </div>
        <div class="form-group col-xs-12">
            <label for="local_trabalho">Local de Trabalho</label>
            <input type="text" name="local_trabalho" id="local_trabalho" class="form-control" value="<?php echo $Rhprofissional_contratacao->local_trabalho ?>" placeholder="Local de trabalho">
        </div>
        <div class="form-group col-xs-12">
            <label for="observacao">Observação</label>
            <input type="text" name="observacao" id="observacao" class="form-control" value="<?php echo $Rhprofissional_contratacao->observacao ?>" placeholder="Observação">
        </div>
        <div class="form-group col-xs-12"">
        <label for="motivo">Motivo da Demissão</label>
        <input type="text" name="motivo" id="motivo" class="form-control motivo" value="<?php echo $Rhprofissional_contratacao->motivo ?>" placeholder="Motivo da Demissão">
    </div>
    <div class="clearfix"></div>
        <legend>Conta Salário</legend>
        <div class="form-group  col-xs-3">
            <label for="banco" class="required">Banco<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="banco" id="banco" class="form-control" value="<?php echo $Rhprofissional_contratacao->banco ?>" placeholder="Banco" required>
        </div>
        <div class="form-group col-xs-3">
            <label for="bancoagencia" class="required">Agência<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="bancoagencia" id="bancoagencia" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoagencia ?>" placeholder="Agência" required>
        </div>
        <div class="form-group col-xs-3">
            <label for="bancoconta" class="required">Conta<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="bancoconta" id="bancoconta" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoconta ?>" placeholder="Conta" required>
        </div>
        <div class="form-group  col-xs-3">
            <label for="bancooperacao" class="required">Operação<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="bancooperacao" id="bancooperacao" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancooperacao ?>" placeholder="Operação" required>
        </div>
        <legend>Conta Pessoal</legend>
        <div class="form-group  col-xs-3">
            <label for="banco2">Banco</label>
            <input type="text" name="banco2" id="banco2" class="form-control" value="<?php echo $Rhprofissional_contratacao->banco2 ?>" placeholder="Banco">
        </div>
        <div class="form-group col-xs-3">
            <label for="bancoagencia2">Agência</label>
            <input type="text" name="bancoagencia2" id="bancoagencia2" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoagencia2 ?>" placeholder="Agência">
        </div>
        <div class="form-group col-xs-3">
            <label for="bancoconta2">Conta</label>
            <input type="text" name="bancoconta2" id="bancoconta2" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoconta2 ?>" placeholder="Conta" >
        </div>
        <div class="form-group  col-xs-3">
            <label for="bancooperacao2">Operação</label>
            <input type="text" name="bancooperacao2" id="bancooperacao2" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancooperacao2 ?>" placeholder="Operação" >
        </div>
        <!-- passa o parametro da id do profissional -->
        <?php if($this->getParam('modal')){ ?>
            <input type="hidden" name="codigo_profissional" value="<?php echo $this->getParam('profissional'); ?>" />
        <?php }else{ ?>
            <!-- / passa o parametro da id do profissional -->
            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label for="codigo_profissional">Profissional</label>
                <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                    <?php
                    foreach ($Rhprofissionais as $r) {
                        if ($r->codigo == $Rhprofissional_contratacao->codigo_profissional)
                            echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                        else
                            echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                    }
                    ?>
                </select>
            </div>
        <?php } ?>
        <input type="hidden" name="codigo" value="<?php echo $Rhprofissional_contratacao->codigo;?>">
        <div class="clearfix"></div>
    </div>
    <!--vou mecher pra ver se consigo abrir a modal dentro da modal daqui -->
    <!-- comando para navegação entre modais -->

    <?php if($this->getParam('modal')){ ?>
    <input type="hidden" name="modal" value="1"/>
        <div class="text-
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        ">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <button type="button" class="btn btn-primary" onclick="submitFormModal('formRhProfissionalContratacao')"> Salvar</button>

            <!--<button type="button" class="btn btn-primary" data-toggle="modal" onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar</button>
            <input type="submit" onclick="EnviarFormulario('form')" id="validForm" style="display: none;" />-->
        </div>
    <?php }else{ ?>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhprofissional_contratacao', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Salvar</button>
    </div>
        <!-- modal de confirmação -->
        <div class="modal fade bs-example-modal-sm" id="ConfirmModal" role="dialog" aria-labelledby="ConfirmModal" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content modal-sm">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h1 class="modal-title" id="ConfimModal">Confirmação</h1>
                    </div>
                    <div class="modal-body">

                        <p>Deseja salvar as Alterações ?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Não</button>
                        <button type="submit" class="btn btn-primary" value="salvar">Sim</button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
        <!-- /modal de confirmação -->
    <!-- /vou mecher pra ver se consigo abrir a modal dentro da modal até aqui -->
</form>
            </div><!-- /ibox content -->
        </div><!-- / ibox -->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->
<script>
    $(document).ready(function(){
        $('#tiporemuneracao option[value="<?php echo $Rhprofissional_contratacao->tiporemuneracao ?>"]').prop('selected', true);
        $('#tipo option[value="<?php echo $Rhprofissional_contratacao->tipo ?>"]').prop('selected', true);
        $('#funcao option[value="<?php echo $Rhprofissional_contratacao->funcao ?>"]').prop('selected', true);
        $('#valetransporte option[value="<?php echo $Rhprofissional_contratacao->valetransporte ?>"]').prop('selected', true);
        $('#centrocusto option[value="<?php echo $Rhprofissional_contratacao->centrocusto ?>"]').prop('selected', true);
    });
</script>