<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content no-borders">
<!--  todos os if's e else's em php são necessários para navegação entre MODAIS -->
<?php if($this->getParam('modal')){ ?>
    <div class="right">
        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="well">
            <div class="col-md-3 form-group">
                    <label for="historico">Histórico</label>
                    <select class="form-control txtColuna2" id="txtColuna2">
                        <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Rhocorrencia_historicos as $r): ?>
                            <?php echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
            </div>
         <h3 class="text-right"><?php echo $Profissional->nome; ?></h3>
            <div class="clearfix"></div>
    </div>
      <?php }else{ ?>
    <div class="filtros well">
        <div class="form">
            <form role="form"
                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                  method="get" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                <div class="col-md-3 form-group">
                    <label for="historico">Histórico</label>
                    <select name="filtro[externo][historico]" class="form-control" id="historico">
                        <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Rhocorrencia_historicos as $r): ?>
                            <?php echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

<?php } ?>

<div>
<!-- passa o parametro da modal -->
    <?php if($this->getParam('modal')){ ?>
        <div class="text-right">
        <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Rhocorrencia","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                <span class="img img-add"></span> Nova Ocorrência
            </a></p>
        </div>
    <?php }else{ ?>
        <!-- botao de cadastro -->
        <div class="text-right">
            <p><?php echo $this->Html->getLink('<span class="img img-add"></span> Nova Ocorrência', 'Rhocorrencia', 'add', NULL, array('class' => 'btn btn-default')); ?></p>
        </div>
    <?php } ?>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="ocorrencia">
        <table class="table table-hover" id="tabela">
            <thead>
            <tr>
                <th>
                    <a>
                        Data
                    </a>
                </th>
                <th>
                    <a>
                        Histórico
                    </a>
                </th>
                <th>
                    <a>
                        Periodicidade
                    </a>
                </th>
                <th>
                    <a>
                        Vencimento
                    </a>
                </th>
                <th>
                    <a>
                        Descrição
                    </a>
                </th>
                <th>
                    <a>
                        Status
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            
            foreach ($Rhocorrencias as $r) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink(DataBR($r->data), 'Rhocorrencia', 'view',
                array('id' => $r->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->getRhocorrencia_historico()->nome, 'Rhocorrencia_historico', 'view',
                    array('id' => $r->getRhocorrencia_historico()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink($r->getRhocorrencia_historico()->periodicidade, 'Rhocorrencia_historico', 'view',
                    array('id' => $r->getRhocorrencia_historico()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink(DataBR($r->data_especifica), 'Rhocorrencia_historico', 'view',
                    array('id' => $r->getRhocorrencia_historico()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink($r->descricao, 'Rhocorrencia', 'view',
                    array('id' => $r->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->getStatus()->descricao, 'Rhocorrencia', 'view',
                    array('id' => $r->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                
            if($this->getParam('modal')==1) {
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Renovar Ocorrência" class="btn  btn-info" style="max-width:50px; max-height:50px;"
                    onclick="Navegar(\'' . $this->Html->getUrl("Rhocorrencia", "renovar", array("ajax" => true, "modal" => "1", 'id' => $r->codigo,'profissional'=>$r->getRhprofissional()->codigo)).'\',\'go\')">
            <span class="glyphicon glyphicon-repeat"></span></a>';
                    echo '</td>'; 

                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar Ocorrência" class="btn  btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Rhocorrencia", "edit", array("ajax" => true, "modal" => "1", 'id' => $r->codigo,'profissional'=>$r->getRhprofissional()->codigo)).'\',\'go\')">
    <span class="img img-edit"></span></a>';
                    echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar Ocorrência" class="btn btn-warn btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Rhocorrencia", "delete", array("ajax" => true, "modal" => "1", 'id' => $r->codigo)).'\',\'go\')">
    <span class="img img-remove"></span></a>';
                    echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Anexos" class="btn btn-warn btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Anexoocorrencia", "all", array("ajax" => true, "modal" => "1", 'id' => $r->codigo)).'\',\'go\')">
    <span class="img img-anexo"></span></a>';
                echo '</td>';
                }else {
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo $this->Html->getLink('<span class="img img-edit"></span> ', 'Rhocorrencia', 'edit',
                        array('id' => $r->codigo),
                        array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                    echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo $this->Html->getLink('<span class="img img-ocorr-remove"></span> ', 'Rhocorrencia', 'delete',
                        array('id' => $r->codigo),
                        array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                    echo '</td>';
                }
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>
</div><!-- /ibox content -->
                </div><!--wrapper-->
            </div><!-- / ibox -->
        </div> <!-- /col-lg 12 -->
    </div><!-- /row -->

<script>
    //toltip
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    //fim toltip
    //função de busca

    // termina função de busca aqui

    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#txtColuna2').change(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.ocorrencia").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Rhocorrencia', 'all'). '&search=" + encodeURIComponent($("#search").val()) + "&historico="+$(this).val()+"&modal=1&id='.$this->getParam('id').' .ocorrencia"';
                    else
                        echo '"' . $this->Html->getUrl('Rhocorrencia', 'all') . '&search=" + encodeURIComponent($("#search").val()) + "&historico="+$(this).val()+"&modal=1&id='.$this->getParam('id').' .ocorrencia"';
                    ?>
                    , function() {
                        r = true;
                    });
            }
        });
    });
</script>