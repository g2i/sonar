<div class="row">
    <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <legend>Seção</legend>
<p><strong>Nome</strong>: <?php echo $Rhsecao->nome;?></p>
<p>
    <strong>Status</strong>:
    <?php
    echo $this->Html->getLink($Rhsecao->getRhstatus()->descricao, 'Rhstatus', 'view',
    array('id' => $Rhsecao->getRhstatus()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Divisão</strong>:
    <?php
    echo $this->Html->getLink($Rhsecao->getRhdivisao()->nome, 'Rhdivisao', 'view',
    array('id' => $Rhsecao->getRhdivisao()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>

                </div><!-- /ibox content -->
            </div><!-- / ibox -->
</div> <!-- /col-lg 12 -->
</div><!-- /row -->
