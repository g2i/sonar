<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">

            <div class="col-lg-9">
                <h2>Localidade</h2>
                <ol class="breadcrumb">
                    <li>Localidade</li>
                    <li class="active">
                        <strong>Adicionar Localidade</strong>
                    </li>
                </ol>
                <br>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Rhlocalidade', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="cidade">Cidade <span class="glyphicon glyphicon-asterisk"></label>
                                <input type="text" name="cidade" id="cidade" class="form-control"
                                       value="<?php echo $Rhlocalidade->cidade ?>" required placeholder="Cidade">
                            </div>
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="estado">Estado <span class="glyphicon glyphicon-asterisk"></label>
                                <input type="text" name="estado" id="estado" class="form-control"
                                       value="<?php echo $Rhlocalidade->estado ?>" required placeholder="Estado">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Rhlocalidade', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>