
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Rhprofissional_salario_historico</h2>
    <ol class="breadcrumb">
    <li>Rhprofissional_salario_historico</li>
    <li class="active">
    <strong>Editar Rhprofissional_salario_historico</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhprofissional_salario_historico', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="valorremuneracao">Valorremuneracao</label>
        </div>
        <div class="form-group">
            <label for="rhprofissional_contratacao_id">Rhprofissional_contratacao</label>
            <select name="rhprofissional_contratacao_id" class="form-control" id="rhprofissional_contratacao_id">
                <?php
                foreach ($Rhprofissional_contratacaos as $r) {
                    if ($r->codigo == $Rhprofissional_salario_historico->rhprofissional_contratacao_id)
                        echo '<option selected value="' . $r->codigo . '">' . $r->tipo . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->tipo . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Rhprofissional_salario_historico->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Rhprofissional_salario_historico->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhprofissional_salario_historico', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>