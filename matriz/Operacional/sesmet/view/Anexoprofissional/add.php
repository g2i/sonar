<div class="row">
    <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
<div class="text-right" style="padding-bottom: 30px">
    <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
</div>
<form method="post" role="form" enctype="multipart/form-data" action="<?php echo $this->Html->getUrl('Anexoprofissional', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Anexoprofissional->nome ?>" placeholder="Nome">
        </div>
          <!-- / passa o parametro da id do profissional -->
          <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label for="tipo_anexo_id">Tipo Anexo</label>
                <select name="tipo_anexo_id" class="form-control" id="tipo_anexo_id">
                    <?php
                    foreach ($Tipo_anexos as $r) {
                        if ($r->id == $Anexoprofissional->tipo_anexo_id)
                            echo '<option selected value="' . $r->id . '">' . $r->nome . '</option>';
                        else
                            echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                    }
                    ?>
                </select>
            </div>          
        <?php if($this->getParam('modal')){ ?>
            <input type="hidden" name="codigo_profissional" value="<?php echo $this->getParam('id');?>" />
        <?php }else{ ?>
            <!-- / passa o parametro da id do profissional -->
            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label for="codigo_profissional">Profissional</label>
                <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                    <?php
                    foreach ($Rhprofissionais as $r) {
                        if ($r->codigo == $Anexoprofissional->codigo_profissional)
                            echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                        else
                            echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                    }
                    ?>
                </select>
            </div>
        <?php } ?>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="anexo">Anexo <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="file" name="anexo" id="anexo" value="<?php echo $Anexoprofissional->anexo ?>" placeholder="Anexo">
        </div>
        <div class="clearfix"></div>
    </div>
    <?php if($this->getParam('modal')){ ?>
        <input type="hidden" name="modal" value="1" />
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary" value="salvar">
        </div>
    <?php }else{ ?>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Anexoprofissional', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
    <?php } ?>
</form>
                </div><!-- /ibox content -->
            </div><!-- / ibox -->
        </div> <!-- /col-lg 12 -->
    </div><!-- /row -->
