<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">
                <form method="post" role="form" enctype="multipart/form-data"
                      action="<?php echo $this->Html->getUrl('Anexocontratacao', 'add') ?>">
                    <div class="right">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="nome">Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Anexocontratacao->nome ?>" placeholder="Nome">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label class="required" for="anexo">Anexo <span class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="file" name="anexo" id="anexo" value="<?php echo $Anexocontratacao->anexo ?>"
                                   placeholder="Anexo" required>
                        </div>
                        <?php if ($this->getParam('modal')) { ?>
                            <input type="hidden" name="codigo_contratacao"
                                   value="<?php echo $this->getParam('id'); ?>"/>

                        <?php } else { ?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="codigo_contratacao">codigo_contratacao</label>
                                <select name="codigo_contratacao" class="form-control" id="codigo_contratacao">
                                    <?php
                                    foreach ($Rhprofissional_contratacaos as $r) {
                                        if ($r->codigo == $Anexocontratacao->codigo_contratacao)
                                            echo '<option selected value="' . $r->codigo . '">' . $r->tipo . '</option>';
                                        else
                                            echo '<option value="' . $r->codigo . '">' . $r->tipo . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                    <?php if ($this->getParam('modal')) { ?>
                        <input type="hidden" name="modal" value="1"/>
                        <div class="text-right">
                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                                Cancelar
                            </a>
                            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                                   value="salvar">
                        </div>
                    <?php } else { ?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Anexocontratacao', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    <?php } ?>
                </form>
            </div>
            <!-- /ibox content -->
        </div>
        <!-- / ibox -->
    </div>
    <!-- /col-lg 12 -->
</div><!-- /row -->
