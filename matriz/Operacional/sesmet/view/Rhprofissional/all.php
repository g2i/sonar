<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Profissional</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Profissionais</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
            <div class="col-lg-12">
        <!-- formulario de pesquisa -->
        <div class="filtros well">
            <div class="form">
                <form role="form"
                      action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                      method="get" enctype="application/x-www-form-urlencoded">
                    <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                    <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $this->getParam('nome'); ?>">
                    </div>
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <label for="cpf">CPF</label>
                        <input type="text" name="cpf" id="cpf" class="form-control cpf" value="<?php echo $this->getParam('cpf'); ?>" placeholder="CPF">
                    </div>
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <label for="setor">Setor</label>
                        <select name="setor" class="form-control" id="setor">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                            <?php foreach ($Rhsetores as $r): ?>
                                <?php echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';  ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <label for="status">Status</label>
                        <select name="status" class="form-control" id="status">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                            <?php foreach ($Rhstatus as $r){
                                if($r->codigo==$status)
                                echo '<option value="' . $r->codigo . '" selected>' . $r->descricao . '</option>';
                                else
                                echo '<option value="' . $r->codigo . '">' . $r->descricao . '</option>';
                            } ?>
                        </select>
                    </div>
                    <!--filtro para localidade-->
                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                        <label for="localidade">Localidade</label>
                        <select name="rhlocalidade_id" class="form-control" id="localidade" required>
         
                            <?php foreach ($localidade as $l) {
                                if($this->getParam('rhlocalidade_id') == $l->id){
                                    echo '<option value="' . $l->id . '" selected>' . $l->cidade . ' - ' . $l->estado .'</option>';
                                }else {
                                    echo '<option value="' . $l->id . '">' . $l->cidade . ' - ' . $l->estado .'</option>';
                                }                                
                            } ?>
                        </select>
                    </div>

                    <div class="col-md-8 text-right"style="margin-top: 25px">
                    
                        <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                           class="btn btn-default"
                           data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                           title="Recarregar a página"><span
                                class="glyphicon glyphicon-refresh "></span></a>
                        <button type="button" class="btn btn-default" id="buscar-filtro" data-tool="tooltip"
                                data-placement="bottom" title="Pesquisar"><span
                                class="glyphicon glyphicon-search"></span></button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <!-- /formulario de pesquisa -->

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content">
<!-- page -->
    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="img img-add"></span> Novo Profissional', 'Rhprofissional', 'add', NULL, array('class' => 'btn btn-default','data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Cadastrar Profissional")); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional', 'all', array('orderBy' => 'nome')); ?>'>
                        Celular
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional', 'all', array('orderBy' => 'nome')); ?>'>
                        Status
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional', 'all', array('orderBy' => 'nome')); ?>'>
                    dt Cadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional', 'all', array('orderBy' => 'nome')); ?>'>
                    Cadastrado por
                    </a>
                </th>
                 <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
                if($filtro) {
                    foreach ($Rhprofissionais as $r) {
                        echo '<tr>';
                        echo '<td>';
                        echo $this->Html->getLink($r->nome, 'Rhprofissional', 'view',
                            array('id' => $r->codigo), // variaveis via GET opcionais
                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($r->celular01, 'Rhprofissional', 'view',
                            array('id' => $r->codigo), // variaveis via GET opcionais
                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                        echo '</td>';
                        echo '<td>';
                        echo $this->Html->getLink($r->getStatus()->descricao, 'Rhstatus', 'view',
                            array('id' => $r->getStatus()->codigo), // variaveis via GET opcionais
                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                        echo '</td>';

                        echo '<td>';
                        echo $this->Html->getLink(DataBR($r->dtCadastro), 'Rhprofissional', 'view',
                            array('id' => $r->codigo), // variaveis via GET opcionais
                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                        echo '</td>';
                        
                        echo '<td>';
                        echo $this->Html->getLink($r->getCadastradoPor()->nome, 'Rhprofissional', 'view',
                            array('id' => $r->codigo), // variaveis via GET opcionais
                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                        echo '</td>';

                        echo '<td>';
                         echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                        echo $this->Html->getLink('<span class="img img-edit" data-toggle="tooltip" data-placement="auto" title="Editar Profissional"></span> ', 'Rhprofissional', 'edit',
                            array('id' => $r->codigo),
                            array('class' => 'btn btn-sm'));
                        echo '</td>';
                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                        echo $this->Html->getLink('<span class="img img-ocorrencia" data-toggle="tooltip" data-placement="auto" title="Ocorrências"></span> ', 'Rhocorrencia', 'all',
                            array('id' => $r->codigo,'first' => 1,'modal' => 1),
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</td>';
                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                        echo $this->Html->getLink('<span class="img img-cc-add" data-placement="top" data-toggle="tooltip" title="Contratações"></span> ', 'Rhprofissional_contratacao', 'all',
                            array('id' => $r->codigo,'first' => 1,'modal' => 1),
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</td>';
                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                        echo $this->Html->getLink('<span class="img img-dependent" data-toggle="tooltip" data-placement="top" title="Dependentes"></span> ', 'Rhprofissional_dependente', 'all',
                            array('id' => $r->codigo,'first' => 1,'modal' => 1),
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</td>';
                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                        echo $this->Html->getLink('<span class="img img-anexo" data-toggle="tooltip" data-placement="top" title="Anexos"></span> ', 'Anexoprofissional', 'all',
                            array('id' => $r->codigo,'first' => 1,'modal' => 1),
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</td>';
                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                        echo $this->Html->getLink('<span class="img img-remove" data-toggle="tooltip" data-placement="auto" title="Desativar Profissional"></span> ', 'Rhprofissional', 'delete',
                            array('id' => $r->codigo),
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</td>';
                        echo '</tr>';
                    }
                }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center">
        <?php 
         if($filtro) {
        echo $nav; 
         }
        ?></div>
    </div>
</div>

                    </div>
                </div>
            </div>


<script>
    //toltip
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    //fim toltip
    //função de busca

    // termina função de busca aqui

    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#txtColuna2').change(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.ocorrencia").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Rhocorrencia', 'all'). '&search=" + encodeURIComponent($("#search").val()) + "&historico="+$(this).val()+"&modal=1&id='.$this->getParam('id').' .ocorrencia"';
                    else
                        echo '"' . $this->Html->getUrl('Rhocorrencia', 'all') . '&search=" + encodeURIComponent($("#search").val()) + "&historico="+$(this).val()+"&modal=1&id='.$this->getParam('id').' .ocorrencia"';
                    ?>
                    , function() {
                        r = true;
                    });
            }
        });
    });
</script>










