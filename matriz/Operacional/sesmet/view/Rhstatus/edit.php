
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhstatus', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="descricao">Descricao</label>
            <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Rhstatus->descricao ?>" placeholder="Descricao">
        </div>
    </div>
    <input type="hidden" name="codigo" value="<?php echo $Rhstatus->codigo;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhstatus', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>