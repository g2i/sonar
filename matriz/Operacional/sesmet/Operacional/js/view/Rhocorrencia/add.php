<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content no-borders">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhocorrencia', 'add') ?>">
    <div class="right">
        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
    </div>
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-sm-6">
            <label for="data" class="required">Data<span class="glyphicon glyphicon-asterisk"></span> </label>
            <input type="date" name="data" id="data" class="form-control" value="<?php echo $Rhocorrencia->data ?>" placeholder="Data" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="historico" class="required">Histórico<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="historico" class="form-control" id="historico" required>
                <option value="">Selecione:</option>
                <?php
                foreach ($Rhocorrencia_historicos as $r) {
                    if ($r->codigo == $Rhocorrencia->historico)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-xs-12">
            <label for="descricao">Descrição</label>
            <textarea name="descricao" id="descricao" class="form-control" rows="3"><?php echo $Rhocorrencia->descricao ?></textarea>
        </div>
        <!-- passa o parametro da id do profissional -->
        <?php if($this->getParam('modal')){ ?>
            <input type="hidden" name="codigo_profissional" value="<?php echo $this->getParam('id'); ?>" />
        <?php }else{ ?>
            <!-- / passa o parametro da id do profissional -->

        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="codigo_profissional">Profissional</label>
            <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                <?php
                foreach ($Rhprofissionais as $r) {
                    if ($r->codigo == $Rhocorrencia->codigo_profissional)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <?php } ?>

        <div class="clearfix"></div>
    </div>
    <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
    <?php if($this->getParam('modal')){ ?>
    <input type="hidden" name="modal" value="1" />
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="salvar">
        </div>
    <?php }else{ ?>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhocorrencia', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
    <?php } ?>
    <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
</form>
            </div><!-- /ibox content -->
        </div><!-- / ibox -->
            </div><!-- wrapper -->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->