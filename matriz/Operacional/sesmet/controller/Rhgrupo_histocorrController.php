<?php
final class Rhgrupo_histocorrController extends AppController{

    # página inicial do módulo Rhgrupo_histocorr
    function index(){
        $this->setTitle('Ocorrência - Grupo');
    }

    # lista de Rhgrupo_histocorres
    # renderiza a visão /view/Rhgrupo_histocorr/all.php
    function all(){
        $this->setTitle('Ocorrência - Grupo');
        $p = new Paginate('Rhgrupo_histocorr', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhgrupo_histocorr', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # visualiza um(a) Rhgrupo_histocorr
    # renderiza a visão /view/Rhgrupo_histocorr/view.php
    function view(){
        $this->setTitle('Visualização de Rhgrupo_histocorr');
        try {
            $this->set('Rhgrupo_histocorr', new Rhgrupo_histocorr((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhgrupo_histocorr', 'all');
        }
    }

    # formulário de cadastro de Rhgrupo_histocorr
    # renderiza a visão /view/Rhgrupo_histocorr/add.php
    function add(){
        $this->setTitle('Cadastro de Ocorrência - Grupo');
        $this->set('Rhgrupo_histocorr', new Rhgrupo_histocorr);
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhgrupo_histocorr
    # (true)redireciona ou (false) renderiza a visão /view/Rhgrupo_histocorr/add.php
    function post_add(){
        $this->setTitle('Cadastro de Ocorrência - Grupo');
        $Rhgrupo_histocorr = new Rhgrupo_histocorr();
        $this->set('Rhgrupo_histocorr', $Rhgrupo_histocorr);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhgrupo_histocorr->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhgrupo_histocorr->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhgrupo_histocorr->save($_POST);
            new Msg(__('Ocorrência - Grupo cadastrada com sucesso'));
            $this->go('Rhgrupo_histocorr', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # formulário de edição de Rhgrupo_histocorr
    # renderiza a visão /view/Rhgrupo_histocorr/edit.php
    function edit(){
        $this->setTitle('Edição de Ocorrência - Grupo');
        try {
            $this->set('Rhgrupo_histocorr', new Rhgrupo_histocorr((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhgrupo_histocorr', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhgrupo_histocorr
    # (true)redireciona ou (false) renderiza a visão /view/Rhgrupo_histocorr/edit.php
    function post_edit(){
        $this->setTitle('Edição de Ocorrência - Grupo');
        try {
            $Rhgrupo_histocorr = new Rhgrupo_histocorr((int) $_POST['codigo']);
            $this->set('Rhgrupo_histocorr', $Rhgrupo_histocorr);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhgrupo_histocorr->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhgrupo_histocorr->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhgrupo_histocorr->save($_POST);
            new Msg(__('Ocorrência - Grupo atualizada com sucesso'));
            $this->go('Rhgrupo_histocorr', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhgrupo_histocorr
    # renderiza a /view/Rhgrupo_histocorr/delete.php
    function delete(){
        $this->setTitle('Apagar Ocorrência - Grupo');
        try {
            $this->set('Rhgrupo_histocorr', new Rhgrupo_histocorr((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhgrupo_histocorr', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhgrupo_histocorr
    # redireciona para Rhgrupo_histocorr/all
    function post_delete(){
        try {
            $Rhgrupo_histocorr = new Rhgrupo_histocorr((int) $_POST['id']);
            $user=Session::get('user');
            $Rhgrupo_histocorr->atualizadopor=$user->id;
            $Rhgrupo_histocorr->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhgrupo_histocorr->status=3;
            $Rhgrupo_histocorr->save();
            new Msg(__('Ocorrência - Grupo deletada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhgrupo_histocorr', 'all');
    }

}