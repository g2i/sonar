<?php
final class RhrelatorioController extends AppController{ 

    # página inicial do módulo Rhrelatorio
    function index(){
        $this->setTitle('Visualização de Rhrelatorio');
    }

    # lista de Rhrelatorios
    # renderiza a visão /view/Rhrelatorio/all.php
    function all(){
        $this->setTitle('Listagem de Rhrelatorio');
        $p = new Paginate('Rhrelatorio', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
   
        //busca ocorrencias apenas aso DISTINCT
       $query = $this->query("SELECT DISTINCT * FROM `rhocorrencia_historico` oh
       WHERE  oh.`codigo` = 3
       OR oh.`codigo` = 14
       OR oh.`codigo` = 24
       OR oh.`codigo` = 27
       OR oh.`codigo` = 23");


        $this->set('Rhocorrencia_historicos',   $query);
        $this->set('Rhlocalidade', Rhlocalidade::getList());
        $this->set('Rhrelatorios', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/view.php
    function view(){
        $this->setTitle('Visualização de Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # formulário de cadastro de Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/add.php
    function add(){
        $this->setTitle('Cadastro de Rhrelatorio');
        $this->set('Rhrelatorio', new Rhrelatorio);
    }

    # recebe os dados enviados via post do cadastro de Rhrelatorio
    # (true)redireciona ou (false) renderiza a visão /view/Rhrelatorio/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rhrelatorio');
        $Rhrelatorio = new Rhrelatorio();
        $this->set('Rhrelatorio', $Rhrelatorio);
        try {
            $Rhrelatorio->save($_POST);
            new Msg(__('Rhrelatorio cadastrado com sucesso'));
            $this->go('Rhrelatorio', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/edit.php
    function edit(){
        $this->setTitle('Edição de Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhrelatorio
    # (true)redireciona ou (false) renderiza a visão /view/Rhrelatorio/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhrelatorio');
        try {
            $Rhrelatorio = new Rhrelatorio((int) $_POST['inicio']);
            $this->set('Rhrelatorio', $Rhrelatorio);
            $Rhrelatorio->save($_POST);
            new Msg(__('Rhrelatorio atualizado com sucesso'));
            $this->go('Rhrelatorio', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhrelatorio
    # renderiza a /view/Rhrelatorio/delete.php
    function delete(){
        $this->setTitle('Apagar Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhrelatorio
    # redireciona para Rhrelatorio/all
    function post_delete(){
        try {
            $Rhrelatorio = new Rhrelatorio((int) $_POST['id']);
            $Rhrelatorio->delete();
            new Msg(__('Rhrelatorio apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhrelatorio', 'all');
    }


    function print_distribuicao(){
        $this->setTemplate('empty');
        $this->setTitle('Relatórios');
        $inicio =$_POST['inicio'];
        $fim = $_POST['fim'];
        $tipo = $_POST['tipo'];
        $historico = $_POST['historico'];       
        $localidade = $_POST['localidade'];


        $titulo = ' ';
        if($tipo == 1){
            $admissao = print_admitidos($inicio, $fim,$localidade);
            $this->set('dados',  $admissao);
            $titulo = "Listagens Contratação (Por Período)";            
        }else if($tipo == 2){
            $demissao = print_demitidos($inicio, $fim,$localidade);
            $this->set('dados',  $demissao);
            $titulo = "Listagens Demissão (Por Período)";            
        }else if($tipo == 3){
            $ferias = print_ferias($inicio, $fim,$localidade);
            $this->set('dados',  $ferias);
            $titulo = "Listagem - Férias a Conceder";            
        }else if($tipo == 4){
            $aso = print_aso($inicio, $fim, $historico, $localidade);
            $this->set('dados',$aso);
            $titulo = "Listagens - ASO (Período Vencimento - Ativos)";            
        }else if($tipo == 5){
            $iniss = print_inss($inicio, $fim, $localidade);
            $this->set('dados', $iniss);
            $titulo = "Listagens INSS - Afastamento";            
        }else if($tipo == 6){
            $atestado = print_atestado($inicio, $fim, $localidade);
            $this->set('dados',$atestado);
            $titulo = "Listagens Atestados(Por Período) ";            
        }

        $this->set('titulo', $titulo);
        $this->set('inicio', $inicio);
        $this->set('fim', $fim);
    }

}

