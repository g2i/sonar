<?php
final class RhlocalidadeController extends AppController{ 

    # página inicial do módulo Rhlocalidade
    function index(){
        $this->setTitle('Visualizar Localidade');
    }

    # lista de Rhlocalidade
    # renderiza a visão /view/Rhlocalidade/all.php
    function all(){
        $this->setTitle('Listagem de Localidades');
        $p = new Paginate('Rhlocalidade', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

       // $c->addCondition('situacao_id', '=', 1);

        $this->set('Rhlocalidades', $p->getPage($c));
        $this->set('nav', $p->getNav());
    
    }

    # visualiza um(a) Medidores_localidade
    # renderiza a visão /view/Rhlocalidade/view.php
    function view(){
        $this->setTitle('Visualizar Localidade');
        try {
            $this->set('Rhlocalidade', new Rhlocalidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhlocalidade', 'all');
        }
    }

    # formulário de cadastro de Rhlocalidade
    # renderiza a visão /view/Rhlocalidade/add.php
    function add(){
        $this->setTitle('Cadastrar Localidade');
        $this->set('Rhlocalidade', new Rhlocalidade);
    }

    # recebe os dados enviados via post do cadastro de Rhlocalidade
    # (true)redireciona ou (false) renderiza a visão /view/Rhlocalidade/add.php
    function post_add(){
        $this->setTitle('Cadastrar Localidade');
        $Rhlocalidade = new Rhlocalidade();
        $this->set('Rhlocalidade', $Rhlocalidade);
        $Rhlocalidade->dt_cadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $Rhlocalidade->dt_modificacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['localidade_id']= 1;
        try {
            $Rhlocalidade->save($_POST);
            new Msg(__('Rhlocalidade cadastrado com sucesso'));
            $this->go('Rhlocalidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }
   
    # formulário de edição de Rhlocalidade
    # renderiza a visão /view/Rhlocalidade/edit.php
    function edit(){
        $this->setTitle('Editar Localidade ');
        try {
            $this->set('Rhlocalidade', new Rhlocalidade((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhlocalidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhlocalidade
    # (true)redireciona ou (false) renderiza a visão /view/Rhlocalidade/edit.php
    function post_edit(){
        $this->setTitle('Editar Localidade ');
        try {
            $Rhlocalidade = new Rhlocalidade((int) $_POST['id']);
            $this->set('Rhlocalidade', $Rhlocalidade);
            $Rhlocalidade->save($_POST);
            new Msg(__('Rhlocalidade atualizado com sucesso'));
            $this->go('Rhlocalidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhlocalidade
    # renderiza a /view/Rhlocalidade/delete.php
    function delete(){
        $this->setTitle('Apagar Rhlocalidade');
        try {
            $this->set('Rhlocalidade', new Rhlocalidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhlocalidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhlocalidade
    # redireciona para Rhlocalidade/all
    function post_delete(){
        try {
            $Rhlocalidade = new Rhlocalidade((int) $_POST['id']);
            $Rhlocalidade->situacao_id = 3;
            $Rhlocalidade->save();
            new Msg(__('Localidade deletada apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhlocalidade', 'all');
    }

}