<?php
final class RhdepartamentoController extends AppController{ 

    # página inicial do módulo Rhdepartamento
    function index(){
        $this->setTitle('Departamento');
    }

    # lista de Rhdepartamentos
    # renderiza a visão /view/Rhdepartamento/all.php
    function all(){
        $this->setTitle('Departamento');
        $p = new Paginate('Rhdepartamento', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhdepartamentos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhstatus',  Rhstatus::getList());
    

    }

    # visualiza um(a) Rhdepartamento
    # renderiza a visão /view/Rhdepartamento/view.php
    function view(){
        $this->setTitle('Visualização de Departamento');
        try {
            $this->set('Rhdepartamento', new Rhdepartamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhdepartamento', 'all');
        }
    }

    # formulário de cadastro de Rhdepartamento
    # renderiza a visão /view/Rhdepartamento/add.php
    function add(){
        $this->setTitle('Cadastro de Departamento');
        $this->set('Rhdepartamento', new Rhdepartamento);
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhdepartamento
    # (true)redireciona ou (false) renderiza a visão /view/Rhdepartamento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Departamento');
        $Rhdepartamento = new Rhdepartamento();
        $this->set('Rhdepartamento', $Rhdepartamento);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhdepartamento->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhdepartamento->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhdepartamento->save($_POST);
            new Msg(__('Departamento cadastrado com sucesso'));
            $this->go('Rhdepartamento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # formulário de edição de Rhdepartamento
    # renderiza a visão /view/Rhdepartamento/edit.php
    function edit(){
        $this->setTitle('Edição de Departamento');
        try {
            $this->set('Rhdepartamento', new Rhdepartamento((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhdepartamento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhdepartamento
    # (true)redireciona ou (false) renderiza a visão /view/Rhdepartamento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Departamento');
        try {
            $Rhdepartamento = new Rhdepartamento((int) $_POST['codigo']);
            $this->set('Rhdepartamento', $Rhdepartamento);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhdepartamento->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhdepartamento->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhdepartamento->save($_POST);
            new Msg(__('Departamento atualizado com sucesso'));
            $this->go('Rhdepartamento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhdepartamento
    # renderiza a /view/Rhdepartamento/delete.php
    function delete(){
        $this->setTitle('Apagar Departamento');
        try {
            $this->set('Rhdepartamento', new Rhdepartamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhdepartamento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhdepartamento
    # redireciona para Rhdepartamento/all
    function post_delete(){
        try {
            $Rhdepartamento = new Rhdepartamento((int) $_POST['id']);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhdepartamento->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhdepartamento->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhdepartamento->status=3;
            $Rhdepartamento->save();
            new Msg(__('Departamento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhdepartamento', 'all');
    }

}