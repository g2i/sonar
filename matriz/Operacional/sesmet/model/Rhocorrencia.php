<?php

final class Rhocorrencia extends Record
{

    const TABLE = 'rhocorrencia';
    const PK = 'codigo';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure()
    {
        $criteria = new Criteria();
        $criteria->addCondition('status', '<>', 3);
        return $criteria;
    }

    /**
     * Rhocorrencia pertence a Rhocorrencia_historico
     * @return Rhocorrencia_historico $Rhocorrencia_historico
     */
    function getRhocorrencia_historico()
    {
        return $this->belongsTo('Rhocorrencia_historico', 'historico');
    }
    function getStatus()
    {
        return $this->belongsTo('Rhstatus', 'status');
    }

    /**
     * Rhocorrencia pertence a Rhprofissional
     * @return Rhprofissional $Rhprofissional
     */
    function getRhprofissional()
    {
        return $this->belongsTo('Rhprofissional', 'codigo_profissional');
    }

    function getUsuario()
    {
        return $this->belongsTo('Usuario', 'cadastradopor');
    }

    //realiza a consult para ver quais as rhocorrencia estão vencidas, esta funcao esta em uso para todas as rhocorrencia
    function getRhocorrencia(){
      $db = new MysqlDB();
      $sql = "SELECT o.codigo AS ocorrencia_id , p.`nome`, oh.`nome` AS descricao, o.data,DATEDIFF (o.data, o.`data_especifica`) AS dias, 
      o.`data_especifica` AS vencimento, p.`codigo` AS id_prof, oh.`historico_complementar` AS comp,
      p.`codigo`, oh.`periodicidade`
      FROM rhocorrencia o
     LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
      LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
      LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
      WHERE o.`status` != 3
      AND o.`status` != 4
      AND p.`status` != 3
      AND oh.codigo != 27
      AND oh.codigo != 28
      AND CURDATE() > o.`data_especifica`
      AND oh.`periodicidade` != 'Nenhuma'
      AND C.`tipo` != 'Demissão'
      ORDER BY UNIX_TIMESTAMP(vencimento) DESC";

      $db->query($sql);
        return $db->getResults();
    }
//não esta em uso, não deletar
    function getTabela_ocorrencia()
    {
        $db = new MysqlDB();
        $sql = "SELECT H.`codigo`, H.`historico_complementar` AS comp, P.codigo AS id_prof,P.`nome`,H.`nome` AS descricao,O.`data`,
                  (CASE
                    WHEN H.`periodicidade` = 'Semestral'
                    THEN DATE_ADD(O.`data`, INTERVAL 6 MONTH)    WHEN H.`periodicidade` = 'Anual' 
                    THEN DATE_ADD(O.`data`, INTERVAL 1 YEAR)     WHEN H.`periodicidade` = 'BiAnual'
                    THEN DATE_ADD(O.`data`, INTERVAL 2 YEAR)     WHEN H.`periodicidade` = 'Mensal'
                    THEN DATE_ADD(O.`data`, INTERVAL 1 MONTH)    WHEN H.`periodicidade` = 'Semanal'
                    THEN DATE_ADD(O.`data`, INTERVAL 1 WEEK)     WHEN H.`periodicidade` = 'Diaria'
                    THEN DATE_ADD(O.`data`, INTERVAL 1 DAY)
                    ELSE O.`data`
                  END) AS data_out
                    FROM rhocorrencia O
                      LEFT JOIN rhocorrencia_historico H
                        ON H.codigo = O.`historico`
                      LEFT JOIN rhprofissional P
                        ON P.`codigo` = O.`codigo_profissional`
                      LEFT JOIN rhprofissional_contratacao C
                        ON C.`codigo_profissional` = O.`codigo_profissional`
                    WHERE O.`status` <> 3
                      AND P.`status` <> 3
                      AND C.`tipo` <> 'Demissão'
                      AND (C.dtFinal IS NULL OR C.dtFinal > CURDATE())
                      AND (CASE
                        WHEN H.`periodicidade` = 'Semestral' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 6 MONTH)
                        WHEN H.`periodicidade` = 'Anual' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 YEAR)
                        WHEN H.`periodicidade` = 'BiAnual' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 2 YEAR)
                        WHEN H.`periodicidade` = 'Mensal' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 MONTH)
                        WHEN H.`periodicidade` = 'Semanal' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 WEEK)
                        WHEN H.`periodicidade` = 'Diaria' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 DAY)
                      ELSE
                      H.`periodicidade`<>'Nenhuma'
                     END
                     OR ((CASE
                        WHEN H.`periodicidade` = 'Semestral'
                        THEN DATE_ADD(O.`data`, INTERVAL 6 MONTH)    WHEN H.`periodicidade` = 'Anual'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 YEAR)     WHEN H.`periodicidade` = 'BiAnual'
                        THEN DATE_ADD(O.`data`, INTERVAL 2 YEAR)     WHEN H.`periodicidade` = 'Mensal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 MONTH)    WHEN H.`periodicidade` = 'Semanal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 WEEK)     WHEN H.`periodicidade` = 'Diaria'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 DAY)
                        ELSE O.`data`
                      END) >= CURDATE()
                      AND
			(CASE
                        WHEN H.`periodicidade` = 'Semestral'
                        THEN DATE_ADD(O.`data`, INTERVAL 6 MONTH)    WHEN H.`periodicidade` = 'Anual'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 YEAR)     WHEN H.`periodicidade` = 'BiAnual'
                        THEN DATE_ADD(O.`data`, INTERVAL 2 YEAR)     WHEN H.`periodicidade` = 'Mensal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 MONTH)    WHEN H.`periodicidade` = 'Semanal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 WEEK)     WHEN H.`periodicidade` = 'Diaria'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 DAY)
                        ELSE O.`data`
                      END) <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)
                      ) )


                      GROUP BY O.`codigo_profissional`
                    ORDER BY data_out ASC ";

        $db->query($sql);
        return $db->getResults();
    }
//não esta em uso, não deletar
   public static function getOcorrencia_individual($cod_prof, $hist_comp)
    {
        $db = new MysqlDB();
        $sql = "SELECT H.`codigo`, H.`historico_complementar` AS comp, P.codigo AS id_prof,P.`nome`,H.`nome` AS descricao,O.`data`,
                  (CASE
                    WHEN H.`periodicidade` = 'Semestral'
                    THEN DATE_ADD(O.`data`, INTERVAL 6 MONTH)    WHEN H.`periodicidade` = 'Anual'
                    THEN DATE_ADD(O.`data`, INTERVAL 1 YEAR)     WHEN H.`periodicidade` = 'BiAnual'
                    THEN DATE_ADD(O.`data`, INTERVAL 2 YEAR)     WHEN H.`periodicidade` = 'Mensal'
                    THEN DATE_ADD(O.`data`, INTERVAL 1 MONTH)    WHEN H.`periodicidade` = 'Semanal'
                    THEN DATE_ADD(O.`data`, INTERVAL 1 WEEK)     WHEN H.`periodicidade` = 'Diaria'
                    THEN DATE_ADD(O.`data`, INTERVAL 1 DAY)
                    ELSE O.`data`
                  END) AS data_out
                    FROM rhocorrencia O
                      LEFT JOIN rhocorrencia_historico H 
                        ON H.codigo = O.`historico`
                      LEFT JOIN rhprofissional P
                        ON P.`codigo` = O.`codigo_profissional`
                      LEFT JOIN rhprofissional_contratacao C
                        ON C.`codigo_profissional` = O.`codigo_profissional`
                    WHERE O.`status` <> 3
                      AND P.`status` <> 3
                      AND H.`status`<> 3
                      AND P.`codigo` = :cod_prof
                      AND H.`codigo` = :hist_comp
                      AND C.`tipo` <> 'Demissão'
                      AND (C.dtFinal IS NULL OR C.dtFinal > CURDATE())
                      AND (CASE
                        WHEN H.`periodicidade` = 'Semestral' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 6 MONTH)
                        WHEN H.`periodicidade` = 'Anual' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 YEAR)
                        WHEN H.`periodicidade` = 'BiAnual' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 2 YEAR)
                        WHEN H.`periodicidade` = 'Mensal' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 MONTH)
                        WHEN H.`periodicidade` = 'Semanal' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 WEEK)
                        WHEN H.`periodicidade` = 'Diaria' THEN CURDATE() > DATE_ADD(O.`data`,INTERVAL 1 DAY)
                      ELSE
                      H.`periodicidade`<>'Nenhuma'
                     END
                     OR ((CASE
                        WHEN H.`periodicidade` = 'Semestral'
                        THEN DATE_ADD(O.`data`, INTERVAL 6 MONTH)    WHEN H.`periodicidade` = 'Anual'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 YEAR)     WHEN H.`periodicidade` = 'BiAnual'
                        THEN DATE_ADD(O.`data`, INTERVAL 2 YEAR)     WHEN H.`periodicidade` = 'Mensal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 MONTH)    WHEN H.`periodicidade` = 'Semanal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 WEEK)     WHEN H.`periodicidade` = 'Diaria'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 DAY)
                        ELSE O.`data`
                      END) >= CURDATE()
                      AND
		              	(CASE
                        WHEN H.`periodicidade` = 'Semestral'
                        THEN DATE_ADD(O.`data`, INTERVAL 6 MONTH)    WHEN H.`periodicidade` = 'Anual'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 YEAR)     WHEN H.`periodicidade` = 'BiAnual'
                        THEN DATE_ADD(O.`data`, INTERVAL 2 YEAR)     WHEN H.`periodicidade` = 'Mensal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 MONTH)    WHEN H.`periodicidade` = 'Semanal'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 WEEK)     WHEN H.`periodicidade` = 'Diaria'
                        THEN DATE_ADD(O.`data`, INTERVAL 1 DAY)
                        ELSE O.`data`
                      END) <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)
                      ) )


                      GROUP BY O.`codigo_profissional`
                    ORDER BY data_out ASC ";


        $db->query($sql);
        $db->bind(':cod_prof', $cod_prof);
        $db->bind(':hist_comp', $hist_comp);
        return $db->getRow();
    }
    

}