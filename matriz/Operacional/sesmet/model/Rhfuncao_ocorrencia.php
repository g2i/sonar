<?php

final class Rhfuncao_ocorrencia extends Record
{

    const TABLE = 'rhfuncao_ocorrencia';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure()
    {
        $criteria = new Criteria();
        $criteria->addCondition('situacao_id', '!=', 3);
        return $criteria;
    }

    /**
     * Rhfuncao_ocorrencia pertence a Rhocorrencia_historico
     * @return Rhocorrencia_historico $Rhocorrencia_historico
     */
    function getRhocorrencia_historico()
    {
        return $this->belongsTo('Rhocorrencia_historico', 'historico_id');
    }

    /**
     * Rhfuncao_ocorrencia pertence a rhprofissional_funcao
     * @return Rhprofissional_funcao $rhprofissional_funcao 
     */
    function getRhprof()
    {
        return $this->belongsTo('Rhprofissional_funcao', 'funcao_id');
    }                            

    }