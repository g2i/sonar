<?php
final class Rhsecao extends Record{ 

    const TABLE = 'rhsecao';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
        return $criteria;
    }
    
    /**
    * Rhsecao pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    
    /**
    * Rhsecao pertence a Rhdivisao
    * @return Rhdivisao $Rhdivisao
    */
    function getRhdivisao() {
        return $this->belongsTo('Rhdivisao','codigo_divisao');
    }
    
    /**
    * Rhsecao possui Rhsetores
    * @return array de Rhsetores
    */
    function getRhsetores($criteria=NULL) {
        return $this->hasMany('Rhsetor','codigo_secao',$criteria);
    }
}