<?php
final class Estq_artigocaracteristicas extends Record{ 

    const TABLE = 'estq_artigocaracteristicas';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('situacao','!=','3');
         return $criteria;
    }
    
    /**
    * Estq_artigocaracteristicas pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_artigocaracteristicas pertence a Estq_artigo
    * @return Estq_artigo $Estq_artigo
    */
    function getEstq_artigo() {
        return $this->belongsTo('Estq_artigo','artigo');
    }
    
    /**
    * Estq_artigocaracteristicas pertence a Stq_caracteristica
    * @return Stq_caracteristica $Stq_caracteristica
    */
    function getStq_caracteristica() {
        return $this->belongsTo('Stq_caracteristica','caracteristica');
    }
}