<?php
final class Estq_concessionaria extends Record{ 

    const TABLE = 'estq_concessionaria';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }

    function getEstq_artigo_concessionaria($criteria=NULL) {
        return $this->hasMany('Estq_artigo_concessionaria','id_concessionaria',$criteria);
    }
}