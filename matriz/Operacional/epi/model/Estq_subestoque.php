<?php
final class Estq_subestoque extends Record{ 

    const TABLE = 'estq_subestoque';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
            $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_subestoque possui Estq_lote_substoques
    * @return array de Estq_lote_substoques
    */
    function getEstq_lote_substoques($criteria=NULL) {
        return $this->hasMany('Estq_lote_substoque','subestoque',$criteria);
    }
    
    /**
    * Estq_subestoque pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_subestoque pertence a Estq_tiposubestoque
    * @return Estq_tiposubestoque $Estq_tiposubestoque
    */
    function getEstq_tiposubestoque() {
        return $this->belongsTo('Estq_tiposubestoque','tipo');
    }
}