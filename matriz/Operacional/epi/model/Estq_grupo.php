<?php
final class Estq_grupo extends Record{ 

    const TABLE = 'estq_grupo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=','3');
        return $criteria;
    }
    
    /**
    * Estq_grupo pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_grupo possui Estq_subgrupos
    * @return array de Estq_subgrupos
    */
    function getEstq_subgrupos($criteria=NULL) {
        return $this->hasMany('Estq_subgrupo','grupo',$criteria);
    }
}