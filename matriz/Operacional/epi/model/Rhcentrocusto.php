<?php
final class Rhcentrocusto extends Record{ 

    const TABLE = 'rhcentrocusto';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Rhcentrocusto pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    
    /**
    * Rhcentrocusto possui Rhprofissional_contratacaos
    * @return array de Rhprofissional_contratacaos
    */
    function getRhprofissional_contratacaos($criteria=NULL) {
        return $this->hasMany('Rhprofissional_contratacao','centrocusto',$criteria);
    }
}