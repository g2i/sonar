<?php
final class Estq_fornecedor extends Record{ 

    const TABLE = 'estq_fornecedor';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */

    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=','3');
         return $criteria;
    }
    function getEstq_mov() {
        return $this->belongsTo('Estq_mov','movimento');
    }
}