<?php
final class Estq_artigo_subestoque extends Record{ 

    const TABLE = 'estq_artigo_subestoque';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_artigo_subestoque pertence a Estq_artigo
    * @return Estq_artigo $Estq_artigo
    */
    function getEstq_artigo() {
        return $this->belongsTo('Estq_artigo','artigo_id');
    }
    
    /**
    * Estq_artigo_subestoque pertence a Estq_subestoque
    * @return Estq_subestoque $Estq_subestoque
    */
    function getEstq_subestoque() {
        return $this->belongsTo('Estq_subestoque','substoque_id');
    }
    
    /**
    * Estq_artigo_subestoque pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Estq_artigo_subestoque pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
    
    /**
    * Estq_artigo_subestoque pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}