<?php
final class Stq_caracteristica extends Record{ 

    const TABLE = 'stq_caracteristica';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Stq_caracteristica possui Estq_artigocaracteristicas
    * @return array de Estq_artigocaracteristicas
    */
    function getEstq_artigocaracteristicas($criteria=NULL) {
        return $this->hasMany('Estq_artigocaracteristicas','caracteristica',$criteria);
    }
    
    /**
    * Stq_caracteristica pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
}