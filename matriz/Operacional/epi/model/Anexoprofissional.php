<?php
final class Anexoprofissional extends Record{ 

    const TABLE = 'anexoprofissional';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Anexoprofissional pertence a Rhprofissional
    * @return Rhprofissional $Rhprofissional
    */
    function getRhprofissional() {
        return $this->belongsTo('Rhprofissional','codigo_profissional');
    }
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
}