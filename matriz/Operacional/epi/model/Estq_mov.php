<?php
final class Estq_mov extends Record{ 

    const TABLE = 'estq_mov';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_mov pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_mov pertence a Estq_tipomov
    * @return Estq_tipomov $Estq_tipomov
    */
    function getEstq_tipomov() {
        return $this->belongsTo('Estq_tipomov','tipo');
    }
    
    /**
    * Estq_mov possui Estq_movdetalhes
    * @return array de Estq_movdetalhes
    */
    function getEstq_movdetalhes($criteria=NULL) {
        return $this->hasMany('Estq_movdetalhes','movimento',$criteria);
    }

    function getFornecedors() {
        return $this->belongsTo('Fornecedor','fornecedor');
    }
    function getCliente() {
        return $this->belongsTo('Cliente','cliente');
    }
    function getRhprofissional() {
        return $this->belongsTo('Rhprofissional','colaborador');
    }

}
