<?php
final class Estq_users extends Record{ 

    const TABLE = 'estq_users';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_users pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_users pertence a Estq_group_user
    * @return Estq_group_user $Estq_group_user
    */
    function getEstq_group_user() {
        return $this->belongsTo('Estq_group_user','grupo');
    }
}