<?php
final class Estq_artigo_concessionaria extends Record{ 

    const TABLE = 'estq_artigo_concessionaria';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }

    function getEstq_artigo() {
        return $this->belongsTo('Estq_artigo','id_artigo');
    }

    function getEstq_concessionaria() {
        return $this->belongsTo('Estq_concessionaria','id_concessionaria');
    }
}