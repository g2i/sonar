<?php
final class Estq_usuario_substoque extends Record{ 

    const TABLE = 'estq_usuario_substoque';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_usuario_substoque pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usario_id');
    }
    
    /**
    * Estq_usuario_substoque pertence a Estq_subestoque
    * @return Estq_subestoque $Estq_subestoque
    */
    function getEstq_subestoque() {
        return $this->belongsTo('Estq_subestoque','substoque_id');
    }
    
    /**
    * Estq_usuario_substoque pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}