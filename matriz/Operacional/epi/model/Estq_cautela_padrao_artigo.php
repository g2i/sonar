<?php
final class Estq_cautela_padrao_artigo extends Record{ 

    const TABLE = 'estq_cautela_padrao_artigo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_cautela_padrao_artigo pertence a Estq_artigo
    * @return Estq_artigo $Estq_artigo
    */
    function getEstq_artigo() {
        return $this->belongsTo('Estq_artigo','artigo_id');
    }
    
    /**
    * Estq_cautela_padrao_artigo pertence a Estq_cautela_padrao
    * @return Estq_cautela_padrao $Estq_cautela_padrao
    */
    function getEstq_cautela_padrao() {
        return $this->belongsTo('Estq_cautela_padrao','cautela_padrao_id');
    }
    
    /**
    * Estq_cautela_padrao_artigo pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Estq_cautela_padrao_artigo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Estq_cautela_padrao_artigo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}