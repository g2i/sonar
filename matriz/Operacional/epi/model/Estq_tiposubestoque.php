<?php
final class Estq_tiposubestoque extends Record{ 

    const TABLE = 'estq_tiposubestoque';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
         $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_tiposubestoque possui Estq_subestoques
    * @return array de Estq_subestoques
    */
    function getEstq_subestoques($criteria=NULL) {
        return $this->hasMany('Estq_subestoque','tipo',$criteria);
    }
    
    /**
    * Estq_tiposubestoque pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
}