<?php
final class Rhdivisao extends Record{ 

    const TABLE = 'rhdivisao';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
        return $criteria;
    }
    
    /**
    * Rhdivisao pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    
    /**
    * Rhdivisao pertence a Rhdepartamento
    * @return Rhdepartamento $Rhdepartamento
    */
    function getRhdepartamento() {
        return $this->belongsTo('Rhdepartamento','codigo_departamento');
    }
    
    /**
    * Rhdivisao possui Rhsecaos
    * @return array de Rhsecaos
    */
    function getRhsecaos($criteria=NULL) {
        return $this->hasMany('Rhsecao','codigo_divisao',$criteria);
    }
}