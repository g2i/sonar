<?php
final class Estq_tipomov extends Record{ 

    const TABLE = 'estq_tipomov';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_tipomov possui Estq_movs
    * @return array de Estq_movs
    */
    function getEstq_movs($criteria=NULL) {
        return $this->hasMany('Estq_mov','tipo',$criteria);
    }
    
    /**
    * Estq_tipomov pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
}