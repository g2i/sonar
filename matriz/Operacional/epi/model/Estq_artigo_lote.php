<?php
final class Estq_artigo_lote extends Record{ 

    const TABLE = 'estq_artigo_lote';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('situacao','!=','3');
         return $criteria;
    }
    
    /**
    * Estq_artigo_lote pertence a Estq_artigo
    * @return Estq_artigo $Estq_artigo
    */
    function getEstq_artigo() {
        return $this->belongsTo('Estq_artigo','artigo');
    }
    
    /**
    * Estq_artigo_lote pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_artigo_lote possui Estq_lote_substoques
    * @return array de Estq_lote_substoques
    */
    function getEstq_lote_substoques($criteria=NULL) {
        return $this->hasMany('Estq_lote_substoque','lote',$criteria);
    }
}