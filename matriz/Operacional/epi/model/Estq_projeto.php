<?php
final class Estq_projeto extends Record{ 

    const TABLE = 'estq_projeto';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
     /**
    * Rhprojetos pertence a Usuario
    * @return Usuario $Usuario
    */

    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Rhprojetos pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Rhprojetos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
}