<?php
final class Estq_group_user extends Record{ 

    const TABLE = 'estq_group_user';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_group_user pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_group_user possui Estq_users
    * @return array de Estq_users
    */
    function getEstq_users($criteria=NULL) {
        return $this->hasMany('Estq_users','grupo',$criteria);
    }
}