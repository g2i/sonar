<?php
final class Fornecedor extends Record{ 

    const TABLE = 'fornecedor';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Fornecedor possui Contaspagares
    * @return array de Contaspagares
    */
    function getContaspagares($criteria=NULL) {
        return $this->hasMany('Contaspagar','idFornecedor',$criteria);
    }
    
    /**
    * Fornecedor possui Estq_movs
    * @return array de Estq_movs
    */
    function getEstq_movs($criteria=NULL) {
        return $this->hasMany('Estq_mov','fornecedor',$criteria);
    }
    
    /**
    * Fornecedor possui Frota_abastecimentos
    * @return array de Frota_abastecimentos
    */
    function getFrota_abastecimentos($criteria=NULL) {
        return $this->hasMany('Frota_abastecimentos','fornecedor_id',$criteria);
    }
    
    /**
    * Fornecedor possui Frota_manutencoes
    * @return array de Frota_manutencoes
    */
    function getFrota_manutencoes($criteria=NULL) {
        return $this->hasMany('Frota_manutencoes','fornecedor_id',$criteria);
    }
    
    /**
    * Fornecedor possui Programacaos
    * @return array de Programacaos
    */
    function getProgramacaos($criteria=NULL) {
        return $this->hasMany('Programacao','idFornecedor',$criteria);
    }
    
    /**
    * Fornecedor possui Rateio_contaspagares
    * @return array de Rateio_contaspagares
    */
    function getRateio_contaspagares($criteria=NULL) {
        return $this->hasMany('Rateio_contaspagar','fornecedor_id',$criteria);
    }
    
    /**
    * Fornecedor possui Rateio_programacaos
    * @return array de Rateio_programacaos
    */
    function getRateio_programacaos($criteria=NULL) {
        return $this->hasMany('Rateio_programacao','fornecedor_id',$criteria);
    }
    
    /**
    * Fornecedor possui Solicitacaos
    * @return array de Solicitacaos
    */
    function getSolicitacaos($criteria=NULL) {
        return $this->hasMany('Solicitacao','idFornecedor',$criteria);
    }

    function getRhstatus() {
        return $this->belongsTo('Status','status');
    }
}