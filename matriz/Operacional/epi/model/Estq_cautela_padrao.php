<?php
final class Estq_cautela_padrao extends Record{ 

    const TABLE = 'estq_cautela_padrao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_cautela_padrao pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Estq_cautela_padrao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Estq_cautela_padrao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
    
    /**
    * Estq_cautela_padrao possui Estq_cautela_padrao_artigos
    * @return array de Estq_cautela_padrao_artigos
    */
    function getEstq_cautela_padrao_artigos($criteria=NULL) {
        return $this->hasMany('Estq_cautela_padrao_artigo','cautela_padrao_id',$criteria);
    }
}