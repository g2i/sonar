<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo SITE_PATH; ?>/img/g2i.png">
    <?php $this->getHeaders(); ?>
</head>

<body>
<?php
$usuario=Session::get('user_epi');
if($usuario !== null && $usuario->primeiro_acesso != 1 ){
?>
<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <?php include 'template/menu.php' ?>
            </ul>

        </div>
    </nav>
    <?php } ?>
    <div <?php if($usuario !== null){ echo 'id="page-wrapper"' ; } ?> class="gray-bg">

        <?php if($usuario->primeiro_acesso != 1) { include 'topNavbar.php';} ?>

           <?php $this->getContents(); ?>
        </div>
    <?php if($usuario !== null){ ?>
        <div class="footer fixed">
            <div>
                <strong>Copyright</strong> G2i &copy; 2014-2015
            </div>
        </div>
    <?php } ?>
    </div>
</div>
<div class="clearfix"></div>

<!-- Generic Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div style="text-align:center">
                <img src="<?php echo SITE_PATH; ?>/template/default/images/loading.gif" alt="LazyPHP">
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-lg" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div style="text-align:center">
                <img src="<?php echo SITE_PATH; ?>/template/default/images/loading.gif" alt="LazyPHP">
            </div>
        </div>
    </div>
</div>
<?php

if($usuario !== null){
?>
<?php $this->getScripts(); ?>
<?php } ?>

</body>

</html>
