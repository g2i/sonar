<?php
$usuario = Session::get('user_epi');
if ($usuario !== null) {
    ?>
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    Sub-estoque: <?php echo @$subestoque->nome; if(@Session::get('user_epi')->gestor_estoque == 1) {echo ' (GESTOR)';} ?>
                </li>
                <li>
                    <a role="open-adm" class="animatedClick" data-target='clickExample'>
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>
                <li>
                    <?php echo $this->Html->getLink('<i class="fa fa-sign-out"></i> Sair', 'Login', 'logout'); ?>
                </li>
            </ul>
        </nav>
    </div>

    <div id="right-sidebar" style="display: none" role="adm"
         class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
        <div class="sidebar-container" full-scroll>

            <ul class="nav nav-tabs navs-2" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                          data-toggle="tab">Administração</a>
                </li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                           data-toggle="tab">Usuários</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" style="background: #2f4050">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-gears"></i> Ajustes </h3>
                        <small><i class="fa fa-tim"></i> Área do administrador.</small>
                    </div>
                    <nav class="navbar-default navbar-static" role="tablist">
                        <div class="sidebar-collapse">
                            <ul class="nav">
                                <?php if ($usuario->id == 1) { ?>
                                    <li <?php echo(CONTROLLER == "Estq_group_user" ? 'class="active"' : ''); ?>>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                           href="#Estq_group_user"
                                           aria-expanded="false" aria-controls="Estq_group_user">
                                            <i class="fa fa-th-large"></i><span
                                                    class="nav-label">Grupos de Usuários</span><span
                                                    class="fa arrow"></span></a>

                                        <div id="Estq_group_user" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                             style="height: 0px;">
                                            <ul class="nav nav-second-level">
                                                <li <?php echo(CONTROLLER == "Estq_group_user" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                                    <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Estq_group_user', 'all'); ?>
                                                </li>
                                                <li <?php echo(CONTROLLER == "Estq_group_user" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                                    <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Estq_group_user', 'add'); ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li <?php echo(CONTROLLER == "Estq_users" ? 'class="active"' : ''); ?>>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                           href="#Estq_users"
                                           aria-expanded="false" aria-controls="Estq_users">
                                            <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span><span
                                                    class="fa arrow"></span></a>

                                        <div id="Estq_users" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                             style="height: 0px;">
                                            <ul class="nav nav-second-level">
                                                <li <?php echo(CONTROLLER == "Estq_users" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                                    <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Estq_users', 'all'); ?>
                                                </li>
                                                <li <?php echo(CONTROLLER == "Estq_users" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                                    <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Estq_users', 'add'); ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li <?php echo(CONTROLLER == "Estq_situacao" ? 'class="active"' : ''); ?>>
                                        <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                           href="#Estq_situacao"
                                           aria-expanded="false" aria-controls="Estq_situacao">
                                            <i class="fa fa-th-large"></i><span class="nav-label">Situação</span><span
                                                    class="fa arrow"></span></a>

                                        <div id="Estq_situacao" class="panel-collapse collapse" role="tabpanel"
                                             aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                             style="height: 0px;">
                                            <ul class="nav nav-second-level">
                                                <li <?php echo(CONTROLLER == "Estq_situacao" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                                    <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Estq_situacao', 'all'); ?>
                                                </li>
                                                <li <?php echo(CONTROLLER == "Estq_situacao" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                                    <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Estq_situacao', 'add'); ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                <?php } ?>
                                <li <?php echo((CONTROLLER == "Estq_tipomov" || CONTROLLER == "Estq_subestoque" || CONTROLLER == "Estq_tiposubestoque") ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                       href="#Estq_adm"
                                       aria-expanded="false" aria-controls="Estq_adm">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Administração</span><span
                                                class="fa arrow"></span></a>

                                    <div id="Estq_adm" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                         style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Estq_tipomov" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Tipo de Movimento', 'Estq_tipomov', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_subestoque" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Sub-Estoque', 'Estq_subestoque', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_tiposubestoque" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Tipo Subestoque', 'Estq_tiposubestoque', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_grupo" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Grupo', 'Estq_grupo', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_subgrupo" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Subgrupo', 'Estq_subgrupo', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Stq_caracteristica" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Características', 'Stq_caracteristica', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_unidade" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i>Tipos de Unidade', 'Estq_unidade', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_concessionaria" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i>Concessionarias', 'Estq_concessionaria', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_projeto" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i>Projetos', 'Estq_projeto', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Fornecedor" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Fornecedor', 'Fornecedor', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_cautela_padrao" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Cautela Padão', 'Estq_cautela_padrao', 'all'); ?>
                                            </li>                                            
                                        </ul>
                                    </div>
                                </li>


                            </ul>
                        </div>
                    </nav>

                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-users"></i> Usuário</h3>
                        <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                    </div>
                    <nav class="navbar-default navbar-static" role="tablist">
                        <div class="sidebar-collapse">
                            <ul class="nav">
                                <li <?php echo(CONTROLLER == "Estq_users" ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                       href="#Estq_users2"
                                       aria-expanded="false" aria-controls="Estq_users">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span><span
                                                class="fa arrow"></span></a>

                                    <div id="Estq_users2" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                         style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Usuario" && ACTION == "conta" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-user"></i>Conta', 'Usuario', 'conta'); ?>
                                            </li>

                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

<?php } ?>