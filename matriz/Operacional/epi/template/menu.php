<li class="nav-header">
    <div class="dropdown profile-element">
    <span>
    <?php echo(file_exists(SITE_PATH . "/content/img/" . sprintf("%06d", @Session::get("user_epi")->id) . ".png") ? '<img class="img-circle" src="' . SITE_PATH . '/content/img/' . sprintf("%06d", @Session::get("user_epi")->id) . '.png" alt="Foto"/>' : '<img class="img-circle" src="' . SITE_PATH . '/content/img/empty-avatar.png" alt="Foto"/>') ?>
    </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
    <span class="clear"> <span class="block m-t-xs"> <strong
                class="font-bold"><?php echo @Session::get("user_epi")->nome; ?></strong>
    </span> <span class="text-muted text-xs block">Colaborador <b class="caret"></b></span> </span> </a>
        <ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><?php echo $this->Html->getLink('Sair', 'Login', 'logout'); ?></li>
        </ul>
    </div>
    <div class="logo-element">
        G2i
    </div>
</li>
<li <?php echo(CONTROLLER == "Index"  && ACTION=='index' ? 'class="active"' : ''); ?>>
    <?php echo $this->Html->getLink('<i class="fa fa-thumb-tack"></i><span class="nav-label"> Estoque</span>', 'Index', 'index'); ?>
</li>
<li <?php echo(CONTROLLER == "Estq_artigo"? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-barcode"></i> Artigos', 'Estq_artigo', 'all'); ?>
</li>

<!-- <li <?php echo(CONTROLLER == "Estq_fornecedor" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Fornecedor', 'Estq_fornecedor', 'all'); ?>
</li>
<li <?php echo((CONTROLLER == "Estq_grupo" || CONTROLLER == "Estq_subgrupo"||CONTROLLER == "Stq_caracteristica"||CONTROLLER == "Estq_fornecedor" || CONTROLLER =='Estq_unidade') ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-plus-square"></i><span class="nav-label">Cadastros</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Estq_grupo" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Grupo', 'Estq_grupo', 'all'); ?>
        </li> 
         <li <?php echo(CONTROLLER == "Estq_subgrupo" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Subgrupo', 'Estq_subgrupo', 'all'); ?>
        </li> 
         <li <?php echo(CONTROLLER == "Stq_caracteristica" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Características', 'Stq_caracteristica', 'all'); ?>
        </li> 
        <li <?php echo(CONTROLLER == "Estq_fornecedor" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Fornecedor', 'Estq_fornecedor', 'all'); ?>
        </li>
         <li <?php echo(CONTROLLER == "Estq_unidade" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i>Tipos de Unidade', 'Estq_unidade', 'all'); ?>
        </li> 
    </ul>
</li>-->
<li <?php echo(CONTROLLER == "Estq_mov" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Entradas/Devoluções', 'Estq_mov', 'all'); ?>
</li>
<!--li <?php echo((CONTROLLER == "Estq_mov" || CONTROLLER == "Estq_movdetalhes" )? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-exchange"></i><span class="nav-label">Movimento</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Estq_mov" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Entradas/Devoluções', 'Estq_mov', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Estq_mov" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Novo', 'Estq_mov', 'novo'); ?>
        </li>
    </ul>
</li>-->
<li <?php echo((CONTROLLER == "Cautela" )? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-exchange"></i><span class="nav-label">Cautelas</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Cautela" && ACTION=="all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Consulta Geral', 'Cautela', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Cautela" && ACTION=="all_colaborador" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Consulta Colaborador ', 'Cautela', 'all_colaborador'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Estq_mov" ? 'class="active"' : ''); ?>>
           <!-- <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Novo', 'Estq_mov', 'novo',array('cautela'=>1)); ?> -->
            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Primeira Cautela (Padrão)', 'Cautela', 'add_padrao'); ?>
            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Cautelar', 'Cautela', 'add_cautela',array('cautelar'=>1)); ?>
            <?php echo $this->Html->getLink('<i class="fa fa-minus"></i> Devolução', 'Cautela', 'add_cautela',array('devolucao'=>1)); ?>
        </li>
    </ul>
</li>
<li>
    <a href="#"><i class="fa fa-file-text"></i><span class="nav-label">Relatórios </span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li >
            <a href="<?php echo SITE_PATH; ?>/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=pos_estoque" target="_blank"><i class="fa fa-file-text"></i>Posição do Estoque Geral</a>
        </li>
        <li >
            <a href="<?php echo SITE_PATH; ?>/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=ensaiados" target="_blank"><i class="fa fa-file-text"></i>Posição do Estoque - Ensaiados</a>
        </li>
        <li >
            <a href="<?php echo SITE_PATH; ?>/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=nao_ensaiados" target="_blank"><i class="fa fa-file-text"></i>Posição do Estoque - Não Ensaiados</a>
        </li>
        <li >
            <a href="Javascript:void(0)" onclick="relatorios()"><i class="fa fa-file-text"></i>Ficha de Estoque</a>
        </li>
    </ul>
</li>

<li <?php echo(CONTROLLER == "Index" && ACTION=='rh' ? 'class="active"' : ''); ?>>
    <?php echo $this->Html->getLink('<i class="fa fa-thumb-tack"></i><span class="nav-label"> RH</span>', 'Index', 'rh'); ?>
</li>

