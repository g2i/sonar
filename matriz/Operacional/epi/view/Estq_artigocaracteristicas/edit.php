<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <div class="ibox-content">
                        <form method="post" role="form"
                              action="<?php echo $this->Html->getUrl('Estq_artigocaracteristicas', 'edit') ?>">
                            <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de
                                preenchimento obrigatório.
                            </div>
                            <div>
                                <input type="hidden" name="situacao" value="1"/>
                                <?php if ($this->getParam('modal')) { ?>
                                <?php } else { ?>
                                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                        <label for="artigo">Artigo</label>
                                        <select name="artigo" class="form-control" id="artigo">
                                            <option value="">Selecione</option>
                                            <?php
                                            foreach ($Estq_artigos as $e) {
                                                if ($e->id == $Estq_artigocaracteristicas->artigo)
                                                    echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                else
                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                <?php } ?>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="caracteristica">Característica</label>
                                    <select name="caracteristica" class="form-control" id="caracteristica">
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($Stq_caracteristicas as $s) {
                                            if ($s->id == $Estq_artigocaracteristicas->caracteristica)
                                                echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                                            else
                                                echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label class="required" for="conteudo">Conteúdo <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="conteudo" id="conteudo" class="form-control"
                                           value="<?php echo $Estq_artigocaracteristicas->conteudo ?>"
                                           placeholder="Conteudo"
                                           required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $Estq_artigocaracteristicas->id; ?>">
                            <?php if ($this->getParam('modal')) { ?>
                                <input type="hidden" name="modal" value="1"/>
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                            onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')">
                                        Salvar
                                    </button>
                                    <input type="submit" onclick="EnviarFormulario('form')" id="validForm"
                                           style="display: none;"/>
                                </div>
                            <?php } else { ?>
                                <div class="text-right">
                                    <a href="<?php echo $this->Html->getUrl('Estq_artigocaracteristicas', 'all') ?>"
                                       class="btn btn-default"
                                       data-dismiss="modal">Cancelar</a>
                                    <input type="submit" class="btn btn-primary" value="salvar">
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
