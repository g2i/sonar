<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;    </span></button>
        <h2><?php echo $Estq_artigocaracteristicas->conteudo; ?></h2>
    </div>
    <div class="panel-body">
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_artigocaracteristicas->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_artigocaracteristicas->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>

<p>
    <strong>Artigo</strong>:
    <?php
    echo $this->Html->getLink($Estq_artigocaracteristicas->getEstq_artigo()->nome, 'Estq_artigo', 'view',
    array('id' => $Estq_artigocaracteristicas->getEstq_artigo()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>

<p>
    <strong>Característica</strong>:
    <?php
    echo $this->Html->getLink($Estq_artigocaracteristicas->getStq_caracteristica()->nome, 'Stq_caracteristica', 'view',
    array('id' => $Estq_artigocaracteristicas->getStq_caracteristica()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
    </div>
</div>
