<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Concessionaria</h2>
                <ol class="breadcrumb">
                    <li>Concessionaria</li>
                    <li class="active">
                        <strong>Editar Concessionaria</strong>
                    </li>
                </ol>
                <br>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Estq_concessionaria', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="descricao" class="required">Nome <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="descricao" id="descricao" class="form-control"
                                       value="<?php echo $Estq_concessionaria->descricao ?>" placeholder="Nome">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Estq_concessionaria->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_concessionaria', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="Salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>