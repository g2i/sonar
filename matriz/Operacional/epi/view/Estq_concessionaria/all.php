<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Concessionaria</h2>
        <ol class="breadcrumb">
            <li>Concessionaria</li>
            <li class="active">
                <strong>Listagem</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<i class="glyphicon glyphicon-plus-sign"></i> Novo', 'Estq_concessionaria', 'add', array(), array('class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '.bs-lg')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>

                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_concessionaria', 'all', array('orderBy' => 'descricao')); ?>'>
                                            Nome
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Estq_concessionarias as $e) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->id, 'Estq_concessionaria', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->descricao, 'Estq_concessionaria', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Estq_concessionaria', 'edit',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_concessionaria', 'delete',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Estq_concessionaria', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Estq_concessionaria', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>