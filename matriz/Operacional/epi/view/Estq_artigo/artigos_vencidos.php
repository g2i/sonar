<div class="col-md-12 wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Artigos</h2>
        <ol class="breadcrumb">
            <li>Artigos</li>
            <li class="active">
                <strong>Vencidos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Artigos</h5>
                </div>
                <div class="ibox-content">
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='Javascript:void(0)'>
                                            Artigo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='Javascript:void(0)'>
                                            Venciento
                                        </a>
                                    </th>                                   
                                </tr>

                                <?php

                                foreach ($artigosVencidos as $item) {
                                    echo '<tr>';
                                        echo '<td>'.$item->nome.'</td>';
                                        echo '<td>'.DataBR($item->validade).'</td>';                                      
                                    echo '</tr>';
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>