<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
        <legend><?php echo $Estq_artigo->nome;?></legend>
    </div>
    <div class="panel-body">
        <p><strong>Código</strong>: <?php echo $Estq_artigo->codigo_livre;?></p>

        <p><strong>Controla Lote</strong>: <?php
    if($Estq_artigo->controla_lote=='1') {
        echo 'Sim';
    }else{
        echo 'Não';
    }

    ?></p>
<p><strong>Valor Comercial</strong>: <?php echo number_format($Estq_artigo->vlcomercial,2,',','.');?></p>
<p><strong>Valor Custo</strong>: <?php echo number_format($Estq_artigo->vlcusto,2,',','.');?></p>
<p><strong>Valor Custo Médio</strong>: <?php echo number_format($Estq_artigo->vlcustomedio,2,',','.');?></p>
<p><strong>Unidade</strong>: <?php echo $Estq_artigo->unidade;?></p>
<p><strong>Tamanho</strong>: <?php echo $Estq_artigo->tamanho;?></p>
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_artigo->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_artigo->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Sub-Grupo</strong>:
    <?php
    echo $this->Html->getLink($Estq_artigo->getEstq_subgrupo()->nome, 'Estq_subgrupo', 'view',
    array('id' => $Estq_artigo->getEstq_subgrupo()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
        </div>
                </div>
        </div>
        </div>
    </div>
