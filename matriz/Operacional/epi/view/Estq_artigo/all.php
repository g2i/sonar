<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Artigos</h2>
        <ol class="breadcrumb">
            <li>Artigos</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <div class="form">
                                <form role="form"
                                      id="form"
                                      action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                      method="post" enctype="application/x-www-form-urlencoded">
                                    <input type="hidden" id="m" name="m" value="<?php echo CONTROLLER; ?>">
                                    <input type="hidden" name="p" id="p" value="<?php echo ACTION; ?>">

                                    <div class="col-md-3 form-group">
                                        <label for="subgrupo">Sub-Grupo</label>
                                        <select name="filtro[externo][subgrupo]" class="form-control " id="subgrupo">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Estq_subgrupos as $e): ?>
                                                <?php
                                                if ($this->getParam('subgrupo') == $e->id) {
                                                    echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                } else {
                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                }
                                                ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="codigo_livre">Código</label>
                                        <input type="number" name="filtro[interno][codigo_livre]" id="codigo_livre"
                                               class="form-control maskInt"
                                               value="<?php echo $this->getParam('codigo_livre'); ?>">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text" name="filtro[interno][nome]" id="nome"
                                               class="form-control search"
                                               value="<?php echo $this->getParam('nome'); ?>">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="situacao">situacao</label>
                                        <select name="filtro[externo][situacao]" class="form-control" id="situacao">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Estq_situacaos as $e): ?>
                                                <?php
                                                if ($this->getParam('situacao') == $e->id) {
                                                    echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                } else {
                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                }
                                                ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-default botao-impressao"
                                                data-tool="tooltip"
                                                data-placement="bottom" title="Impressão"><span
                                                    class="glyphicon glyphicon-print"></span></button>
                                        <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                           class="btn btn-default"
                                           data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                           title="Recarregar a página"><span
                                                    class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="submit" class="btn btn-default" data-tool="tooltip"
                                                data-placement="bottom" title="Pesquisar"><span
                                                    class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- botao de cadastro -->
                <div class="text-right">
                    <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Artigo', 'Estq_artigo', 'add', NULL, array('class' => 'btn btn-primary', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Cadastrar Artigo")); ?></p>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Artigos</h5>
                    </div>
                    <div class="ibox-content">
                        <!-- tabela de resultados -->
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Estq_artigo', 'all', array('orderBy' => 'nome')); ?>'>
                                                Nome
                                            </a>
                                        </th>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Estq_artigo', 'all', array('orderBy' => 'vlcomercial')); ?>'>
                                                Código
                                            </a>
                                        </th>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Estq_artigo', 'all', array('orderBy' => 'subgrupo')); ?>'>
                                                Sub-Grupo
                                            </a>
                                        </th>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Estq_artigo', 'all', array('orderBy' => 'vlcusto')); ?>'>
                                                Estoque
                                            </a>
                                        </th>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Estq_artigo', 'all', array('orderBy' => 'unidade')); ?>'>
                                                Unidade
                                            </a>
                                        </th>
                                        <!-- <th>&nbsp;</th> -->
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    foreach ($Estq_artigos as $e) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->nome, 'Estq_artigo', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->codigo_livre, 'Estq_artigo', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->getEstq_subgrupo()->nome, 'Estq_subgrupo', 'view',
                                            array('id' => $e->getEstq_subgrupo()->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo '<div class="dropdown">
                                                      <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" onclick="get_estoque(' . $e->id . ')">Estoque
                                                        <span class="caret"></span>
                                                      </a>
                                                      <ul class="dropdown-menu" id="' . $e->id . 'dropdown-menu" aria-labelledby="dLabel" style="min-width: 559px !important;min-height: 92px;padding-top: 10px;">
                                                      </ul>
                                                    </div>';
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->getUnidade()->sigla, 'Estq_artigo', 'view',
                                            array('id' => $e->getUnidade()->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                        echo '<td class="actions text-right">';
                            echo '<div class="dropdown">';
                                echo '<button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                    <span class="caret"></span></button>';       
                                    echo '<ul class="dropdown-menu">';
                                        echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-pencil" data-toggle="tooltip" data-placement="auto" title="Editar Artigo"> Editar Artigo</span> ', 'Estq_artigo', 'edit',
                                            array('id' => $e->id),
                                            array('class' => 'btn  btn-sm data-toggle'));
                                        echo '</li>';

                                        echo '<li>';
                                        echo $this->Html->getLink('<span class="fa fa-thumbs-o-up" data-toggle="tooltip" data-placement="auto" title="Concessionarias Habilitadas"> Concessionarias Habilitadas</span> ', 'Estq_artigo', 'concessionarias_habilitadas',
                                            array('id' => $e->id),
                                            array('class' => 'btn  btn-sm', 'data-toggle' => 'modal'));
                                        echo '</li>';

                                        if ($e->controla_lote == 1) {
                                            echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-indent" data-toggle="tooltip" data-placement="auto" title=" Artigo - Lote"> Artigo - Lote</span>', 'Estq_artigo_lote', 'all',
                                                array('id' => $e->id, 'first' => 1, 'modal' => 1),
                                                array('class' => 'btn  btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                            echo '</li>';
                                        } else {
                                            echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-indent" data-toggle="tooltip" data-placement="auto" title=" Artigo - Lote"> Artigo - Lote</span>', 'Estq_artigo_lote', 'all',
                                                array('id' => $e->id, 'first' => 1, 'modal' => 1),
                                                array('class' => 'btn  btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                            echo '</li>';
                                        }
                                        echo '<li>';
                                        echo $this->Html->getLink('<span class="fa fa-list-ol" data-toggle="tooltip" data-placement="auto" title=" Artigo - Lote Subestoque ">   Artigo - Lote Subestoque</span>', 'Estq_lote_substoque', 'all',
                                            array('id' => $e->id, 'first' => 1, 'modal' => 1),
                                            array('class' => 'btn  btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                        echo '</li>';

                                        echo '<li>';
                                        echo $this->Html->getLink('<span class="fa fa-arrows-alt" data-toggle="tooltip" data-placement="auto" title="Excluir artigo " >  Deletar - Lote Subestoque</span> ', 'Estq_artigo', 'delete',
                                            array('id' => $e->id),
                                            array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                                        echo '</li>';

                                        
                                    echo '</ul>';
                            echo '</div>';
                        echo '</td>';
                        
                                        echo '</tr>';
                                    }
                                    ?>
                                </table>

                                <!-- menu de paginação -->
                                <div style="text-align:center"><?php echo $nav; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function () {

        $('.search').keyup(function () {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Estq_artigo', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    else
                        echo '"' . $this->Html->getUrl('Estq_artigo', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });

        $('[data-toggle="tooltip"]').tooltip();
    });

    function get_estoque(id) {
        $.ajax({
            type: 'POST',
            url: root + '/Estq_lote_substoque/get_dados',
            data: 'id=' + id,
            success: function (txt) {
                $("#" + id + "dropdown-menu").html(txt);
            }
        })
    }
</script>