<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Concessionarias Habilitadas</h2>
                <ol class="breadcrumb">
                    <li>Concessionarias Habilitadas</li>
                    <li class="active">
                        <strong>Listagem</strong>
                    </li>
                </ol>
                <br>
            </div>
            <div class="ibox float-e-margins">
                <!-- div class="ibox-content"> -->

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>

                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_concessionaria', 'all', array('orderBy' => 'descricao')); ?>'>
                                            <h2>Nome</h2>
                                        </a>
                                    </th>

                                </tr>
                                <?php
                                foreach ($artigo_concessionarias as $e) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink('<h4>'.$e->getEstq_concessionaria()->descricao.'</h4>', 'Estq_concessionaria', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <!-- <div style="text-align:center"><?php echo $nav; ?></div> -->
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Estq_concessionaria', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Estq_concessionaria', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
               <!--  </div> -->
            </div>
        </div>
    </div>
</div>