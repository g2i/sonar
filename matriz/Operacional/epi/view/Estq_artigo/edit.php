<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Artigos</h2>
        <ol class="breadcrumb">
            <li>Artigos</li>
            <li class="active">
                <strong>Edição de Artigos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <form method="post" role="form" id="form-artigo"  enctype="multipart/form-data"
                              action="<?php echo $this->Html->getUrl('Estq_artigo', 'edit') ?>">
                            <div class="alert alert-info">Os campos marcados com <span
                                        class="small glyphicon glyphicon-asterisk"></span> são de
                                preenchimento obrigatório.
                            </div>
                            <div>
                            <div class="form-group col-md-3 pull-right">
                                <a onclick="upload()" id="uploadFoto" style="cursor:pointer">
                                    <?php if (empty($Estq_artigo->foto)) { ?>
                                        <img src="<?php echo SITE_PATH ?>/lib/css/images/photo23x4.png" title="Upload Foto" id="charFoto" style="width:128px;height:128px;cursor:pointer;">
                                    <?php } else { ?>
                                        <img src="<?php echo $Estq_artigo->foto?>" title='Foto Carregada' id="charFoto" style="width:128px;height:128px;cursor:pointer;">
                                    <?php } ?>
                                </a>
                                <input type="file" name="foto" id="foto" style="visibility: hidden;" >
                            </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="codigo_livre">Código</label>
                                    <input type="text" name="codigo_livre" id="codigo_livre" class="form-control"
                                           value="<?php echo $Estq_artigo->codigo_livre ?>" placeholder="Código"
                                           onblur="dublicidade('edit',$('#codigo_livre').val(),<?php echo $Estq_artigo->id ?>)">
                                </div>


                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label class="required" for="nome">Nome <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="nome" id="nome" class="form-control"
                                           value="<?php echo $Estq_artigo->nome ?>"
                                           placeholder="Nome" required>
                                </div>
                                <!-- <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="controla_lote" class="required">Controla Lote<span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="controla_lote" class="form-control" id="controla_lote" required>
                                        <option value="">Selecione :</option>
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div> 
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="vlcomercial">Valor Comercial</label>
                                    <input type="text" name="vlcomercial" id="vlcomercial"
                                           class="form-control money"
                                           value="<?php echo $Estq_artigo->vlcomercial ?>"
                                           placeholder="Valor Comercial">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="vlcusto">Valor Custo</label>
                                    <input type="text" name="vlcusto" id="vlcusto" class="form-control money"
                                           value="<?php echo $Estq_artigo->vlcusto ?>" placeholder="Valor Custo">
                                </div> -->
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="vlcustomedio">Valor Custo </label>
                                    <input type="text" name="vlcustomedio" id="vlcustomedio"
                                           class="form-control money"
                                           value="<?php echo $Estq_artigo->vlcustomedio ?>"
                                           placeholder="Valor Custo Médio">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="unidade">Unidade</label>
                                    <select name="unidade" class="form-control" id="unidade" required>
                                        <option value="">Selecione :</option>
                                        <?php
                                        foreach ($Estq_unidade as $e) {
                                            if ($e->id == $Estq_artigo->unidade)
                                                echo '<option selected value="' . $e->id . '">' . $e->sigla . '</option>';
                                            else
                                                echo '<option value="' . $e->id . '">' . $e->sigla . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="tamanho">Tamanho</label>
                                    <input type="text" name="tamanho" id="tamanho" class="form-control"
                                           value="<?php echo $Estq_artigo->tamanho ?>" placeholder="Tamanho">
                                </div>
                                <input type="hidden" name="situacao" value="1"/>

                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="subgrupo" class="required">Sub-Grupo<span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="subgrupo" class="form-control" id="subgrupo" required>
                                        <option value="">Selecione :</option>

                                        <?php
                                        foreach ($Estq_subgrupos as $e) {
                                            if ($e->id == $Estq_artigo->subgrupo)
                                                echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                            else
                                                echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                             
                                <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                    <label for="concessionaria">Concessionarias</label><br>
                                    <select name="concessionaria[]" id="concessionaria"
                                            class="form-control selectpicker" multiple required multiple data-actions-box="true">
                                        <?php foreach ($Estq_concessionarias as $c): ?>
                                            <option value="<?= $c->id ?>" <?php echo (in_array($c->id, $Estq_artigo_concessionarias)) ? "selected" : "" ?>> <?= $c->descricao ?> </option>;
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="ensaio">Isolado?</label>
                                    <select name="ensaio" id="ensaio" class="form-control" required>
                                        <option value="">Selecione</option>
                                        <option value="1" <?= $Estq_artigo->ensaio == 1 ? 'selected' : '' ?>>Sim
                                        </option>
                                        <option value="2" <?= $Estq_artigo->ensaio == 2 ? 'selected' : '' ?>> Não
                                        </option>
                                    </select>
                                </div>
                                <!--Estq_artigo_subestoque-->
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="substoque_id" class="required">Estoque Subestoque<span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="substoque_id" class="form-control" id="substoque_id" required>
                                        <?php foreach ($Estq_subestoque as $c):
                                            echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                        endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="marca">Marca</label>
                                    <input type="text" name="marca" id="marca" class="form-control"
                                           value="<?php echo $Estq_artigo->marca ?>" placeholder="Marca">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="quantidade">Quantidade</label>
                                    <input type="text" name="quantidade" id="quantidade" class="form-control"
                                           value="<?php echo $Estq_artigo->quantidade ?>" placeholder="Quantidade">
                                </div>
                                <!--<div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="est_minimo">Estoque mínimo</label>
                                    <input type="text" name="est_minimo" id="est_minimo" class="form-control"
                                           value="<?php echo $Estq_artigo->est_minimo ?>" placeholder="Estoque mínimo">
                                </div>-->
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="vida_util">Vida util</label>
                                    <input type="text" name="vida_util" id="vida_util" class="form-control"
                                           value="<?php echo $Estq_artigo->vida_util ?>" placeholder="Vida útil">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $Estq_artigo->id; ?>">

                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Estq_artigo', 'all') ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal">Cancelar</a>
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar
                                </button>
                                <input type="submit" id="validForm" style="display: none;"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('#concessionaria').selectpicker();

            $('#concessionaria').change(function () {
                console.log($('#concessionaria').val());
            });
            $('#controla_lote option[value="<?php echo $Estq_artigo->controla_lote ?>"]').prop('selected', true);
             //Estq_subestoque
             $('[data-toggle ="tooltip"]').tooltip();
            $('#substoques').selectpicker();

            $('#substoques').change(function () {
                console.log($('#substoques').val());
            });

            $('#substoques').change(function () {
                console.log($('#substoques').val());
            });
        });
        function upload() {
        $('#foto').click();
    }

    $('document').ready(function() {
        $('#foto').change(function() {
            $('#uploadFoto').html('<output id="list" style="width:128px;height:128px;"></output>');
        });
    });

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }
            var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '" width="128" height="128"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('foto').addEventListener('change', handleFileSelect, false);
    </script>