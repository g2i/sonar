
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Cautela Padrão</h2>
    <ol class="breadcrumb">
    <li>Cautela Padrão</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="descricao">Descrição</label>
            <input type="text" name="filtro[interno][descricao]" id="descricao" class="form-control" value="<?php echo $this->getParam('descricao'); ?>">
        </div>
                <div class="col-md-12 text-right">
                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"  class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                           title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                    <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pesquisar"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Estq_cautela_padrao', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_cautela_padrao', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_cautela_padrao', 'all', array('orderBy' => 'descricao')); ?>'>
                        Descrição
                    </a>
                </th>               
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Estq_cautela_padraos as $e) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($e->id, 'Estq_cautela_padrao', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->descricao, 'Estq_cautela_padrao', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';   
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="fa fa-archive" title="Serviços/Produtos" ></span>', 'Estq_cautela_padrao_artigo', 'all',
                    array('id' => $e->id, 'first' => 1, 'modal' => 1),
                    array('class' => 'btn  btn-xs btn-primary', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                echo '</td>';             
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="fa fa-pencil-square"></span> ', 'Estq_cautela_padrao', 'edit', 
                    array('id' => $e->id), 
                    array('class' => 'btn btn-xs btn-warning'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="fa fa-trash-o"></span> ', 'Estq_cautela_padrao', 'delete', 
                    array('id' => $e->id), 
                    array('class' => 'btn btn-xs btn-danger','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Estq_cautela_padrao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Estq_cautela_padrao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>