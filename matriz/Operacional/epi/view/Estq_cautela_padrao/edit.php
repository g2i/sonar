
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Cautela Padrão</h2>
    <ol class="breadcrumb">
    <li>Cautela Padrão</li>
    <li class="active">
    <strong>Editar</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_cautela_padrao', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="descricao">Descrição</label>
            <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Estq_cautela_padrao->descricao ?>" placeholder="Descrição">
        </div>     
        <div class="clearfix"></div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Estq_cautela_padrao->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Estq_cautela_padrao', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>