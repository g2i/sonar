
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Estq_projeto</h2>
    <ol class="breadcrumb">
    <li>Estq_projeto</li>
    <li class="active">
    <strong>Editar Estq_projeto</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_projeto', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="nome">nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Estq_projeto->nome ?>" placeholder="Nome">
        </div>       
        <div class="clearfix"></div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Estq_projeto->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Estq_projeto', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>