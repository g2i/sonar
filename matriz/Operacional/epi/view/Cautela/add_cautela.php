<div class="row wrapper border-bottom white-bg page-heading">

    <?php if (isset($_GET['cautelar'])) { ?>
        <div class="col-lg-9">
            <h2>Cautela</h2>
            <ol class="breadcrumb">
                <li>Cautela</li>
                <li class="active">
                    <strong>Adicionar Cautela</strong>
                </li>
            </ol>
        </div>
    <?php } else { ?>

        <div class="col-lg-9">
            <h2>Cautela - Devolução</h2>
            <ol class="breadcrumb">
                <li>Devolução</li>
                <li class="active">
                    <strong>Adicionar Devolução</strong>
                </li>
            </ol>
        </div>
    <?php } ?>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_mov', 'novo') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div>
                            <div class="col-md-6">
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label class="required" for="tipo">Tipo de Movimento <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="tipo" class="form-control tipo" id="tipo" required readonly="true">
                                        <?php
                                        foreach ($Estq_tipomovs as $e) {
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <!-- <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label class="required" for="padrao">Estoque Padrão?<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="padrao" id="padrao" class="form-control" required>
                                        <option value="">Selecione</option>
                                        <option value="1">Sim</option>
                                        <option value="2">Não</option>
                                    </select>
                                </div> -->

                                <!-- <div class="form-group col-md-12 col-sm-6 col-xs-12 sub_padrao">

                                    <label class="required" for="sub_padrao">Lote-subestoque<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="sub_padrao" id="sub_padrao" required class="form-control"
                                            readonly="true">
                                         <option value="">Selecione</option>
                                        <?php
                                foreach ($sub_estoque as $e) {
                                    if ($e->id == $Estq_mov->sub_padrao)
                                        echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                    else
                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                }
                                ?>
                                    </select>
                                </div> -->

                                <div class="form-group col-md-12 col-sm-12 col-xs-12 colaborador"
                                     style="display:<?= $this->getParam('cautela') ? 'block' : 'none'; ?>">
                                    <label for="colaborador">Colaborador</label><br/>
                                    <select name="colaborador" class="form-control" id="colaborador"
                                            style="width: 100%">
                                        <option value="">Selecione :</option>
                                        <?php
                                        foreach ($Rhprofissionals as $e) {
                                            echo '<option value="' . $e->codigo . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 fornecedor" style="display: none">
                                    <label for="fornecedor">Fornecedor</label> <br/>
                                    <select name="fornecedor" class="form-control" id="fornecedor" style="width: 100%">
                                        <option value="">Selecione :</option>
                                        <?php
                                        foreach ($Fornecedors as $e) {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 cliente" style="display: none">
                                    <label for="cliente">Cliente</label><br/>
                                    <select name="cliente" class="form-control" id="cliente" style="width: 100%">
                                        <option value="">Selecione :</option>
                                        <?php
                                        foreach ($Clientes as $e) {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-12 col-sm-12 col-xs-12"
                                     style="display:<?= !$this->getParam('cautela') ? 'block' : 'none'; ?>">
                                    <p><label class="required" for="requer">Selecione :<span
                                                    class="glyphicon glyphicon-asterisk"></span></label></p>

                                    <label class="radio-inline">
                                        <input type="radio" name="requer" id="requer" role="requer" value="Fornecedor"
                                               required>
                                        Fornecedor
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="requer" id="requer" role="requer" value="Cliente">
                                        Cliente
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="requer" id="requer" role="requer"
                                               value="Colaborador" <?= $this->getParam('cautela') ? 'checked' : ''; ?>>
                                        Colaborador
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="requer" id="requer" role="requer" value="4">
                                        Outros
                                    </label>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group col-md-6">
                                    <label for="artigo" class="required">Artigo <span
                                                class="glyphicon glyphicon-asterisk"></span></label><br/>
                                    <select name="artigo" id="artigo" class="form-control artigo artigo-ajax">
                                        <option value="">Selecione:</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="codigo_livre" class="required">Código</label>
                                    <select name="codigo_livre" class="form-control codigo codigo-ajax"
                                            id="codigo_livre">
                                        <option value="">Selecione:</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="marca">Marca</label>
                                    <input type="text" id="marca" class="form-control"
                                           name="marca"/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="validade">Validade</label>
                                    <input type="text" id="validade" class="form-control date"
                                           name="validade"/>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="novo_usado">Novo / Usado</label>
                                    <select id="novo_usado" class="form-control"
                                            name="novo_usado">
                                        <option value="NOVO">Novo</option>
                                        <option value="USADO">Usado</option>

                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="quantidade">Quantidade</label>
                                    <input type="number" id="quantidade" class="form-control" step="0.01"
                                           name="quantidade"/>
                                </div>
                                <div class="clearfix"></div>

                                <div class="text-right form-group col-md-12 col-sm-12 col-xs-12">
                                    <input type="button" value="Adicionar artigo" class="btn btn-success"
                                           id="btn_add_artigo"/>
                                </div>

                                <!--<div class="form-group col-md-6 col-sm-12 col-xs-12 subestoque">
                                    <label for="lote_substoque" class="required">Lote Sub-Estoque Artigos<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="lote_substoque_artigos" class="form-control lote_substoque_artigos"
                                            id="lote_substoque_artigos"
                                            required>
                                    </select>
                                </div> -->

                            </div>


                            <div class="col-md-12">
                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Artigos</label><br/>
                                <div class="table-responsive list-dep col-md-12">
                                    <table class="table table-hover ">
                                    </table>
                                </div>
                            </div>
                            <input type="hidden" name="tipodoc" id="tipodoc"/>
                            <input type="hidden" name="datadoc" id="datadoc" value="<?= date('d/m/Y') ?>"/>
                            <input type="hidden" name="data" id="data" value="<?= date('d/m/Y') ?>"/>
                            <input type="hidden" name="numdoc" id="numdoc" value="0"/>
                            <input type="hidden" name="situacao" value="1"/>
                            <input type="hidden" name="lote_substoque_artigos" id="lote_substoque_artigos"/>


                            <input type="hidden" name="sub_padrao" id="sub_padrao" value="<?= @$subestoque->id ?>"/>

                            <?php if (isset($_GET['cautelar']) || isset($_GET['devolucao'])) { ?>
                                <input type="hidden" name="cautela" id="cautela" value="1">
                            <?php } ?>

                            <div class="clearfix"></div>

                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default"
                                   data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" onclick="return prosseguir()"
                                       value="salvar">
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $("#novo_usado").val([]);

    $(function () {
//        $("#tipo").change(function () {
//            $("#tipodoc").val($("#tipo :selected").text());
//        });

        $("#tipodoc").val($("#tipo :selected").text());

        $("input[name='requer']").click(function () {
            if ($(this).val() == 'Cliente') {
                $(".cliente").show(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Colaborador') {
                $(".cliente").hide(500);
                $(".colaborador").show(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Fornecedor') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").show(500);
            }
            else if ($(this).val() == '4') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }

        })
    });

    //    $("#padrao").change(function () {
    //        if ($(this).val() == 1) {
    //            $(".sub_padrao").show(500);
    //            $("#sub_padrao").attr("required", "required")
    //        } else {
    //            $(".sub_padrao").hide(500);
    //            $("#sub_padrao").removeAttr("required")
    //        }
    //    })

    $(function () {

    });

    $(document).ready(function () {

        $('select.codigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: false});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            })
        });
        $('select.artigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar2/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: false});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            })
        });

        moment.locale('pt-BR');
        $('.artigo').change(function () {

            if ($('.artigo').val()) {

                $.ajax({
                    type: 'POST',
                    url: root + '/Estq_movdetalhes/GetLoteSubestoqueCautela',
                    data: 'artigo=' + $(this).val(),
                    success: function (txt) {
                        //$("select#lote_substoque_artigos").html(txt);
                        $('#lote_substoque_artigos').val(txt);
                        console.log($('#lote_substoque_artigos').val());
                    }
                });
            }
            $.ajax({
                type: 'POST',
                url: root + '/Estq_movdetalhes/GetCodigo',
                data: 'artigo=' + $(this).val(),
                success: function (txt) {
                    $(".codigo").html(txt);
                    setTimeout(function () {
                        $(".codigo").trigger("chosen:updated");
                    }, 20);

                }
            });

        });

        $('.codigo').change(function () {

            if ($('.codigo').val()) {
                $.ajax({
                    type: 'POST',
                    url: root + '/Estq_movdetalhes/GetLoteSubestoqueCautela',
                    data: 'artigo=' + $(this).val(),
                    success: function (txt) {
                        //$("#lote_substoque_artigos").html(txt);
                        $('#lote_substoque_artigos').val(txt);
                        console.log($('#lote_substoque_artigos').val());
                    }
                });
            }
            $.ajax({
                type: 'POST',
                url: root + '/Estq_movdetalhes/GetArtigo',
                data: 'artigo=' + $(this).val(),
                success: function (txt) {
                    $(".artigo").html(txt);
                    setTimeout(function () {
                        $(".artigo").trigger("chosen:updated");
                    }, 20);
                }
            });
        });


        var url = '<?= $this->Html->getUrl('Estq_movdetalhes', 'list_art_cautela') ?>';
        var filtro = {
            artigo: ''
        };
//        $(".lote_substoque_artigos").change(function () {
//            if ($("#quantidade").val() == "") {
//                BootstrapDialog.alert('Insira a quantidade!');
//                return false;
//            }
//            filtro.lote_id = $(this).val();
//            filtro.artigo = $("#artigo").val();
//            filtro.quantidade = $("#quantidade").val();
//            $("div.list-dep").loadGrid(url, filtro, '.arts', null);
//        });

//        $("#quantidade").blur(function () {
//            if ($("#quantidade").val() == "") {
//                BootstrapDialog.alert('Insira a quantidade!');
//                return false;
//            } else if ($("#marca").val()) {
//
//            }
//            filtro.lote_id = $('#lote_substoque_artigos').val();
//            filtro.artigo = $("#artigo").val();
//            filtro.quantidade = $("#quantidade").val();
//            filtro.marca = $("#marca").val();
//            filtro.validade = $("#validade").val();
//            filtro.novo_usado = $("#novo_usado").val();
//            $("#quantidade").val('');
//            $("#marca").val('');
//            $("#validade").val('');
//            $("#novo_usado").val([]);
//
//            $("div.list-dep").loadGrid(url, filtro, '.arts', null);
//        });


        $('#btn_add_artigo').click(function () {

            if ($("#quantidade").val() == "") {
                BootstrapDialog.alert('Insira a quantidade!');
                return false;
            }
            filtro.lote_id = $('#lote_substoque_artigos').val();
            filtro.artigo = $(".artigo").val();
            filtro.quantidade = $("#quantidade").val();
            filtro.marca = $("#marca").val();
            filtro.validade = $("#validade").val();
            filtro.novo_usado = $("#novo_usado").val();


            $("div.list-dep").loadGrid(url, filtro, '.arts', null);

            $("#quantidade").val('');
            $("#marca").val('');
            $("#validade").val('');
            $("#novo_usado").val([]);
            $(".artigo").val('').trigger('change');
            $(".codigo").val('').trigger('change');

        });

        $("div.list-dep").loadGrid(url, filtro, '.arts', null);
    });

</script>


<script async type="text/javascript">
    function prosseguir() {
        if ($("#count_itens").val() > 0) {
            return true;
        } else {
            BootstrapDialog.alert('Preencha todos os itens obrigatórios!');
            return false;

        }
    }

    console.log('');

</script>

