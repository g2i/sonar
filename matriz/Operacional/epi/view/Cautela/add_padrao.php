<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cautela Padrão</h2>
        <ol class="breadcrumb">
            <li>Cautela Padrão</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Cautela', 'add_padrao') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">                          
                                <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                    <label class="required" for="tipo">Tipo de Movimento <span class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="tipo" class="form-control tipo" id="tipo" required readonly="true">
                                        <?php
                                        foreach ($Estq_tipomovs as $e) {
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                    <label class="required" for="colaborador">Colaborador <span class="glyphicon glyphicon-asterisk"></span></label><br/>
                                    <select name="colaborador" class="form-control" id="colaborador" required style="width: 100%">
                                        <option value="">Selecione :</option>
                                        <?php
                                        foreach ($Rhprofissionals as $e) {
                                            echo '<option value="' . $e->codigo . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                    <label class="required" for="cautela_padrao_id">Cautela Padrão <span class="glyphicon glyphicon-asterisk"></span></label><br/>
                                    <select name="cautela_padrao_id" class="form-control" id="cautela_padrao_id" required style="width: 100%">
                                        <option value="">Selecione :</option>
                                       <?php
                                        foreach ($Estq_cautela_padrao as $e) {
                                            echo '<option value="' . $e->id . '">' . $e->descricao . '</option>';
                                        }
                                        ?>
                                    </select>
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="novo_usado">Novo / Usado</label>
                                    <select id="novo_usado" class="form-control"
                                            name="novo_usado">
                                        <option value="NOVO">Novo</option>
                                        <option value="USADO">Usado</option>

                                    </select>
                                </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <label for="descricao">Descrição</label>
                                <input type="text" name="descricao" id="descricao" class="form-control"
                                    placeholder="Descrição">
                            </div>
                            <input type="hidden" name="tipodoc" id="tipodoc"/>
                            <input type="hidden" name="datadoc" id="datadoc" value="<?= date('d/m/Y') ?>"/>
                            <input type="hidden" name="data" id="data" value="<?= date('d/m/Y') ?>"/>
                            <input type="hidden" name="numdoc" id="numdoc" value="0"/>
                            <input type="hidden" name="situacao" value="1"/>
                            <input type="hidden" name="lote_substoque_artigos" id="lote_substoque_artigos"/>
                            <input type="hidden" name="sub_padrao" id="sub_padrao" value="<?= @$subestoque->id ?>"/>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_cautela_padrao', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>