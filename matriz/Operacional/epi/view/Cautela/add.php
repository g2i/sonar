<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimento</h2>
        <ol class="breadcrumb">
            <li>Movimento</li>
            <li class="active">
                <strong>Adicionar Cautela</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="frmAdd" method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_mov', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com
                            <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="tipo">Tipo de Movimento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="tipo" class="form-control tipo" id="tipo">
                                    <option value="">Selecione :</option>
                                    <?php foreach ($Estq_tipomovs as $e):
                                        if ($e->id == $Estq_mov->tipo): ?>
                                            <option selected value="<?php echo $e->id; ?>"><?php echo $e->nome ?></option>
                                        <?php else: ?>
                                            <option value="<?php echo $e->id ?>"><?php echo $e->nome ?></option>
                                        <?php endif;
                                    endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="tipodoc">Tipo de Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="tipodoc" id="tipodoc" class="form-control"
                                       value="<?php echo $Estq_mov->tipodoc ?>" placeholder="Tipo Documento" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="datadoc">Data do Documento</label>

                                <div class='input-group datePicker'>
                                    <input type='text' name="datadoc" id="datadoc" class="form-control date"
                                           value="<?php echo $Estq_mov->datadoc ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data">Data da Movimentação</label>

                                <div class='input-group datetimepicker '>
                                    <input type='text' name="data" id="data" class="form-control datetime"
                                           value="<?php echo $Estq_mov->data ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="numdoc">Número do Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="numdoc" id="numdoc" class="form-control"
                                       value="<?php echo $Estq_mov->numdoc ?>" placeholder="Número Documento" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 colaborador" >
                                <label for="colaborador">Colaborador</label>
                                <select name="colaborador" class="form-control" id="colaborador">
                                    <option value="">Selecione :</option>
                                    <?php foreach ($Rhprofissionals as $e): ?>
                                        <option value="<?php echo $e->codigo ?>"><?php  echo $e->nome ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <input type="hidden" name="requer" value="Colaborador" />

                            <input type="hidden" name="situacao" value="1"/>

                            <div class="clearfix"></div>

                            <div class="text-right">
                                <button id="btnCancelar" type="button" class="btn btn-danger">
                                    <i class="fa fa-ban"> Cancelar</i>
                                </button>
                                <button id="btnSalvar" type="button" class="btn btn-primary">
                                    <i class="fa fa-floppy-o"> Salvar</i>
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $("input[name='requer']").click(function () {
            if ($(this).val() == 'Cliente') {
                $(".cliente").show(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Colaborador') {
                $(".cliente").hide(500);
                $(".colaborador").show(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Fornecedor') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").show(500);
            }
            else if ($(this).val() == '4') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }

        })

        $("#colaborador").select2();

        $("#tipo").select2();

        $("#btnCancelar").click(function(){
            window.location = root + '/Cautela/all/';
        });

        $("#btnSalvar").click(function(){
            var that = $(this);
            var url = root + '/Cautela/addAjax';
            var data = $('#frmAdd').serialize();

            that.prop('disabled', true);
            $("#btnCancelar").prop('disabled', true);
            $.post(url, data, function(ret){
                if(ret.result){
                    var url = root + '/Estq_movdetalhes/all/ajax:true' +
                                     '/modal:1/first:1/id:' + ret.id;

                    showOnModal(url, 'Detalhes', BootstrapDialog.SIZE_WIDE, null, function(dialog){
                        window.location = root + '/Cautela/all/';
                        return true;
                    });
                }else{
                    BootstrapDialog.show({
                        title: 'Aviso',
                        message: ret.msg,
                        type: BootstrapDialog.TYPE_WARNING,
                        closable: true,
                        draggable: true,
                        buttons: [{
                            id: 'btn-ok',
                            icon: 'fa fa-check-square-o',
                            label: 'OK',
                            action: function(dialog){
                                dialog.close();
                            }
                        }],
                        onhidden: function(dialog) {
                            $("#btnCancelar").prop('disabled', false);
                            that.prop('disabled', false);
                        }
                    });
                }
            });
        });
    });
</script>




