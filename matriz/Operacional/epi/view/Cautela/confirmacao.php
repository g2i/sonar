<div class="row alert alert-danger">
    Após clicar no botão <strong>Executar</strong>, você não poderá mais fazer alterações neste movimento!
    <br />
    <strong>Deseja continuar?</strong>
</div>