<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimento</h2>
        <ol class="breadcrumb">
            <li>Movimento</li>
            <li class="active">
                <strong>Listagem de Cautelas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <div class="form">
                                <form role="form" id="form"
                                      action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                      method="post" enctype="application/x-www-form-urlencoded">
                                    <input type="hidden" name="m" id="m" value="<?php echo CONTROLLER; ?>">
                                    <input type="hidden" name="p" id="p" value="<?php echo ACTION; ?>">

                                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                        <label for="cod_mov">Movimento</label>
                                        <input type="number" class="form-control" name="cod_mov" id="cod_mov"
                                               value="<?php echo $cod_mov; ?>" placeholder="Código do Movimento"/>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="tipo">Tipo de Movimento</label>
                                        <select name="filtro[externo][tipo]" class="form-control" id="tipo">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Estq_tipomovs as $e): ?>
                                                <?php
                                                if ($this->getParam('tipo') == $e->id) {
                                                    echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                } else {
                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                } ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4 form-group">
                                        <label for="numdoc">Nº do Documento</label>
                                        <input type="numdoc" name="filtro[interno][numdoc]" id="numdoc"
                                               class="form-control maskInt"
                                               value="<?php echo $this->getParam('numdoc'); ?>">
                                    </div>

                                    <div id="charger_tipo"></div>

                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-default botao-impressao"
                                                data-tool="tooltip"
                                                data-placement="bottom" title="Impressão"><span
                                                    class="glyphicon glyphicon-print"></span></button>
                                        <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                           class="btn btn-default"
                                           data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                           title="Recarregar a página"><span
                                                    class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="submit" class="btn btn-default" data-tool="tooltip"
                                                data-placement="bottom" title="Pesquisar"><span
                                                    class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div>


                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Cautela', 'Cautela', 'add_cautela', array('cautelar' => 1), array('class' => 'btn btn-primary', 'title' => "Cadastrar Cautela")); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Cautelas</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'tipo')); ?>'>
                                                    Código
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'tipo')); ?>'>
                                                    Tipo de Movimento
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Data Doc
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'cliente')); ?>'>
                                                    Nº do Documento
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'cliente')); ?>'>
                                                    Nome (Fornecedor,Cliente ou Colaborador)
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'tipodoc')); ?>'>
                                                    Tipo Doc
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'data')); ?>'>
                                                    Data
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'status')); ?>'>
                                                    Status
                                                </a>
                                            </th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        <?php
                                        foreach ($Estq_movs as $e) {
                                            echo '<tr>';
                                            echo '<td>';
                                            echo $this->Html->getLink($e->id, 'Estq_mov', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            echo $this->Html->getLink($e->getEstq_tipomov()->nome, 'Estq_tipomov', 'view',
                                                array('id' => $e->getEstq_tipomov()->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            echo $this->Html->getLink(DataBR($e->datadoc), 'Estq_mov', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            echo $this->Html->getLink($e->numdoc, 'Estq_mov', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            if ($e->requer != NULL) {
                                                if ($e->requer == "Fornecedor") {
                                                    echo $this->Html->getLink($e->getFornecedors()->nome, 'Estq_mov', 'view',
                                                        array('id' => $e->id), // variaveis via GET opcionais
                                                        array('data-toggle' => 'modal')); // atributos HTML opcionais

                                                } elseif ($e->requer == "Colaborador") {
                                                    echo $this->Html->getLink($e->getRhprofissional()->nome, 'Estq_mov', 'view',
                                                        array('id' => $e->id), // variaveis via GET opcionais
                                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                                } elseif ($e->requer == "Cliente") {
                                                    echo $this->Html->getLink($e->getCliente()->nome, 'Estq_mov', 'view',
                                                        array('id' => $e->id), // variaveis via GET opcionais
                                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                                } else if ($e->requer == 4) {
                                                    echo $this->Html->getLink($e->getFornecedors()->nome, 'Estq_mov', 'view',
                                                        array('id' => $e->id), // variaveis via GET opcionais
                                                        array('data-toggle' => 'modal')); // atributos HTML opcionais

                                                }
                                            }
                                            echo '</td>';


                                            echo '<td>';
                                            echo $this->Html->getLink($e->tipodoc, 'Estq_mov', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            echo $this->Html->getLink(DataBR($e->data), 'Estq_mov', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            echo $this->Html->getLink(($e->status == 2) ? 'Encerrado' : 'Aberto', 'Estq_mov', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';


                                            if ($e->status == 2) {
                                                echo '<td width="30" style="padding:2px;">';
                                                echo '<a href="Javascript:void(0)" class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="auto" title="Processado">';
                                                echo '<span class="glyphicon glyphicon-lock"  > </span>'; // atributos HTML opcionais
                                                echo '</a>';
                                                echo '</td>';
                                            } else {
                                                echo '<td width="30" style="padding:2px;">';
                                                echo '<a href="Javascript:void(0)" onclick="encerrar_movimento(' . $e->id . ')" class="btn btn-success btn-sm"  data-toggle="tooltip" data-placement="auto" title="Encerrar Movimento">';
                                                echo '<span class="glyphicon glyphicon-ok" > </span>';
                                                echo '</a>';
                                                echo '</td>';
                                            }
                                            echo '<td width="30" style="padding:2px;">';
                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="auto" title="Editar Movimento"></span> ', 'Cautela', 'edit',
                                                array('id' => $e->id),
                                                array('class' => 'btn btn-warning btn-sm'));
                                            echo '</td>';

                                            echo '<td width="30" style="padding: 2px;">';
                                            echo $this->Html->getLink('<span class="fa fa-trash-o" data-toggle="tooltip" data-placement="auto" title="Excluir Movimento"></span> ', 'Cautela', 'delete',
                                                array('id' => $e->id),
                                                array('class' => ($e->status == 2) ? 'btn btn-danger btn-sm disabled' : 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                            echo '</td>';


                                            echo '<td width="30" style="padding: 2px;">';
                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-align-justify" data-toggle="tooltip" data-placement="auto" title="Detalhes de Movimento"></span> ', 'Estq_movdetalhes', 'all',
                                                array('id' => $e->id, 'first' => 1, 'modal' => 1),
                                                array('class' => 'btn btn-info btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                            echo '</td>';

                                            echo '<td width="30" style="padding: 2px;">';
                                            echo '<a href="' . SITE_PATH . '/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=1&mov=' . $e->id . '" target="_blanck" class="btn btn-default btn-sm">';
                                            echo '<span class="glyphicon glyphicon-print" data-toggle="tooltip" data-placement="auto" title="Imprimir Cautela"></span> ';
                                            echo '</a>';
                                            echo '</td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                    </table>

                                    <!-- menu de paginação -->
                                    <div style="text-align:center"><?php echo $nav; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(function () {
            carregar_tipo();
        });

        function carregar_tipo() {
            getTipo(3, '<?php echo $this->getParam('colaborador'); ?>');

        }

        //toltip
        $(document).ready(function () {
            carregar_tipo();
            $('[data-toggle="tooltip"]').tooltip();
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Estq_mov', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });
        });

        $(function () {
            $("input[id='requer']").click(function () {
                if ($(this).val() == 'Cliente') {
                    $(".cliente").show(500);
                    $(".colaborador").hide(500);
                    $(".fornecedor").hide(500);
                }
                else if ($(this).val() == 'Colaborador') {
                    $(".cliente").hide(500);
                    $(".colaborador").show(500);
                    $(".fornecedor").hide(500);
                }
                else if ($(this).val() == 'Fornecedor') {
                    $(".cliente").hide(500);
                    $(".colaborador").hide(500);
                    $(".fornecedor").show(500);
                }

            })
        });


    </script>