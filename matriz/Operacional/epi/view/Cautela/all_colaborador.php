<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cautela</h2>
        <ol class="breadcrumb">
            <li>Cautela</li>
            <li class="active">
                <strong>Listagem Colaborador</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <div class="form">
                                <form role="form" id="form"
                                      action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                      method="post" enctype="application/x-www-form-urlencoded">
                                    <input type="hidden" name="m" id="m" value="<?php echo CONTROLLER; ?>">
                                    <input type="hidden" name="p" id="p" value="<?php echo ACTION; ?>">


                                    <!-- <div class="col-md-4 form-group">
                                        <label for="tipo">Tipo de Movimento</label>
                                        <select name="filtro[externo][tipo]" class="form-control" id="tipo">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Estq_tipomovs as $e): ?>
                                                <?php
                                        if ($this->getParam('tipo') == $e->id) {
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        } ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div> -->

                                    <div class="col-md-4 form-group">
                                        <label for="tipo">Tipo de Movimento</label>
                                        <select name="tipo" class="form-control" id="tipo">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Estq_tipomovs as $e): ?>
                                                <?php
                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                 ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="colaborador">Colaborador</label>
                                        <select name="colaborador" class="form-control" id="colaborador">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Rhprofissionais as $p): ?>
                                                <?php
                                                    echo '<option value="' . $p->codigo . '">' . $p->nome . '</option>';
                                                ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="artigo">Artigo</label><br/>
                                        <select name="artigo" id="artigo" class="form-control artigo artigo-ajax">
                                            <option value="">Selecione:</option>
                                        </select>
                                    </div>

                                    <div class="clearfix"></div>


                                    <!-- <div id="charger_tipo"></div> -->

                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-default botao-impressao"
                                                data-tool="tooltip"
                                                data-placement="bottom" title="Impressão"><span
                                                class="glyphicon glyphicon-print"></span></button>
                                        <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                           class="btn btn-default"
                                           data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                           title="Recarregar a página"><span
                                                class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="button" class="btn btn-default" id="btn-filtro" data-tool="tooltip"
                                                data-placement="bottom" title="Pesquisar"><span
                                                class="glyphicon glyphicon-search" ></span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div>


                    <!-- botao de cadastro -->
                    <!-- <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Cautela', 'Cautela', 'add', NULL, array('class' => 'btn btn-primary', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Cadastrar Cautela")); ?></p>
                    </div> -->

                    <!-- tabela de resultados -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Cautelas</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'tipo')); ?>'>
                                                    Código
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'tipo')); ?>'>
                                                    Tipo de Movimento
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Data Doc
                                                </a>
                                            </th>

                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Cod. Artigo
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Nome Artigo
                                                </a>
                                            </th>
                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Colaborador
                                                </a>
                                            </th>

                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Quantidade
                                                </a>
                                            </th>

                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Marca
                                                </a>
                                            </th>

                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Validade
                                                </a>
                                            </th>

                                            <th>
                                                <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'datadoc')); ?>'>
                                                    Novo / Usado
                                                </a>
                                            </th>

                                        </tr>
                                        <?php
                                        foreach ($Estq_movs as $e) {
                                            echo '<tr>';
                                            echo '<td>';
                                            echo $this->Html->getLink($e->movimento, 'Estq_mov', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            echo $this->Html->getLink($e->getEstq_mov()->getEstq_tipomov()->nome, 'Estq_tipomov', 'view',
                                                array('id' => $e->getEstq_mov()->getEstq_tipomov()->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            echo '<td>';
                                            echo $this->Html->getLink(DataBR($e->getEstq_mov()->datadoc), 'Estq_mov', 'view',
                                                array('id' => $e->getEstq_mov()->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';
                                            //Cod. Artigo
                                            echo '<td>';
                                            echo $this->Html->getLink($e->artigo, 'Estq_movdetalhes', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';

                                            //Nome. Artigo
                                            echo '<td>';
                                            echo $this->Html->getLink($e->getEstq_artigo()->nome, 'Estq_artigo', 'view',
                                                array('id' => $e->getEstq_artigo()->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';

                                            //Colaborador
                                            echo '<td>';
                                            echo $this->Html->getLink($e->getEstq_mov()->getRhprofissional()->nome, 'Rhprofissional', 'view',
                                                array('id' => $e->getEstq_mov()->getRhprofissional()->codigo), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';

                                            //Quantidade
                                            echo '<td>';
                                            echo $this->Html->getLink($e->quantidade, 'Estq_movdetalhes', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';

                                            //Marca
                                            echo '<td>';
                                            echo $this->Html->getLink($e->marca, 'Estq_movdetalhes', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';

                                            //Validade
                                            echo '<td>';
                                            echo $this->Html->getLink(DataBR($e->validade), 'Estq_movdetalhes', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';

                                            //Novo / Usado
                                            echo '<td>';
                                            echo $this->Html->getLink($e->novo_usado, 'Estq_movdetalhes', 'view',
                                                array('id' => $e->id), // variaveis via GET opcionais
                                                array('data-toggle' => 'modal')); // atributos HTML opcionais
                                            echo '</td>';

                                            echo '</tr>';
                                        }
                                        ?>
                                    </table>

                                    <!-- menu de paginação -->
                                    <div style="text-align:center"><?php echo $nav; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">




        $(function () {
            carregar_tipo();
        });

        function carregar_tipo() {
            getTipo(3, '<?php echo $this->getParam('colaborador'); ?>');

        }
        //toltip
        $(document).ready(function () {
            initComponente(document);
            carregar_tipo();
            $('[data-toggle="tooltip"]').tooltip();
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Estq_mov', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });

            $('select.codigo-ajax').each(function () {
                $(this).ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: root + "/Estq_movdetalhes/listar/",
                    allow_single_deselect: true
                }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: false});
                $(this).change(function () {
                    $(this).trigger("chosen:updated");
                })
            });
            $('select.artigo-ajax').each(function () {
                $(this).ajaxChosen({
                    dataType: "json",
                    type: "POST",
                    url: root + "/Estq_movdetalhes/listar2/",
                    allow_single_deselect: true
                }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: false});
                $(this).change(function () {
                    $(this).trigger("chosen:updated");
                })
            });

        });

        $(function () {
            $("input[id='requer']").click(function () {
                if ($(this).val() == 'Cliente') {
                    $(".cliente").show(500);
                    $(".colaborador").hide(500);
                    $(".fornecedor").hide(500);
                }
                else if ($(this).val() == 'Colaborador') {
                    $(".cliente").hide(500);
                    $(".colaborador").show(500);
                    $(".fornecedor").hide(500);
                }
                else if ($(this).val() == 'Fornecedor') {
                    $(".cliente").hide(500);
                    $(".colaborador").hide(500);
                    $(".fornecedor").show(500);
                }

            })


        });




    </script>