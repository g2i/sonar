<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipos de Unidade</h2>
        <ol class="breadcrumb">
            <li>Unidade</li>
            <li class="active">
                <strong>Listagem Tipos de Unidades</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <!-- formulario de pesquisa -->
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                                <div class="col-md-3 form-group">
                                    <label for="sigla">Sigla</label>
                                    <input type="text" name="filtro[interno][sigla]" id="sigla"
                                           class="form-control"
                                           value="<?php echo $this->getParam('sigla'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="descricao">Descrição</label>
                                    <input type="text" name="filtro[interno][descricao]" id="descricao"
                                           class="form-control"
                                           value="<?php echo $this->getParam('descricao'); ?>">
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-impressao"><span
                                            class="glyphicon glyphicon-print"></span></button>
                                    <button type="button" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- botao de cadastro -->
            <div class="text-right">
                <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Tipo de Unidade', 'Estq_unidade', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
            </div>

            <!-- tabela de resultados -->
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Tipos de Unidades</h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_unidade', 'all', array('orderBy' => 'sigla')); ?>'>
                                            Sigla
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_unidade', 'all', array('orderBy' => 'descricao')); ?>'>
                                            Descrição
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Estq_unidades as $e) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->sigla, 'Estq_unidade', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->descricao, 'Estq_unidade', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Estq_unidade', 'edit',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-warning btn-sm'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_unidade', 'delete',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                /* faz a pesquisa com ajax */
                $(document).ready(function () {
                    $('#search').keyup(function () {
                        var r = true;
                        if (r) {
                            r = false;
                            $("div.table-responsive").load(
                                <?php
                                if (isset($_GET['orderBy']))
                                    echo '"' . $this->Html->getUrl('Estq_unidade', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                else
                                    echo '"' . $this->Html->getUrl('Estq_unidade', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                ?>
                                , function () {
                                    r = true;
                                });
                        }
                    });
                });
            </script>
        </div>
    </div>
</div>