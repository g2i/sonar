<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;    </span></button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <legend>Tipo de Unidade</legend>
<p><strong>Sigla</strong>: <?php echo $Estq_unidade->sigla;?></p>
<p><strong>Descrição</strong>: <?php echo $Estq_unidade->descricao;?></p>
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_unidade->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_unidade->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
                </div>
            </div>
        </div>
    </div>
</div>