<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipos de Unidade</h2>
        <ol class="breadcrumb">
            <li>Unidade</li>
            <li class="active">
                <strong>Listagem de Tipos Unidades</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="ibox-content">
                        <form method="post" role="form"
                              action="<?php echo $this->Html->getUrl('Estq_unidade', 'edit') ?>">
                            <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                            </div>
                            <div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="sigla">Sigla</label>
                                    <input type="text" name="sigla" id="sigla" class="form-control"
                                           value="<?php echo $Estq_unidade->sigla ?>" placeholder="Sigla">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="descricao">Descrição</label>
                                    <input type="text" name="descricao" id="descricao" class="form-control"
                                           value="<?php echo $Estq_unidade->descricao ?>" placeholder="Descricao">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <input type="hidden" name="situacao" value="1">
                            <input type="hidden" name="id" value="<?php echo $Estq_unidade->id; ?>">

                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Estq_unidade', 'all') ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>