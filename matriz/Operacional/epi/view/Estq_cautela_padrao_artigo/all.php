
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Cautela Padrão Artigo</h2>
    <ol class="breadcrumb">
    <li>Cautela Padrão Artigo</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <?php if($this->getParam('modal')){ ?>
    <!-- formulario de pesquisa -->
    <?php }else{?>
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                
                <div class="col-md-3 form-group">
                    <label for="artigo_id">Artigo</label>
                    <select name="filtro[externo][artigo_id]" class="form-control" id="artigo_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Estq_artigos as $e): ?>
                            <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>                    
                <div class="col-md-3 form-group">
                    <label for="cautela_padrao_id">Cautela Padrão</label>
                    <select name="filtro[externo][cautela_padrao_id]" class="form-control" id="cautela_padrao_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Estq_cautela_padraos as $e): ?>
                            <?php echo '<option value="' . $e->id . '">' . $e->descricao . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>               
                <div class="col-md-12 text-right">
                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"  class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                           title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                    <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pesquisar"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div> <?php }?>

    <!-- botao de cadastro -->
    <div class="text-right">    
        <?php if($this->getParam('modal')){ ?>
            <!--passa param para a modal-->      
            <p><a href="Javascript:void(0)" class="btn btn-primary" 
            onclick="Navegar('<?php echo $this->Html->getUrl("Estq_cautela_padrao_artigo","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                <span class="glyphicon glyphicon-plus-sign"></span> Novo registro</a></p>
        <?php }else{?>
            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Estq_cautela_padrao_artigo', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>

           <?php }?>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='#'>
                        id
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Artigo
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Cautela Padrão
                    </a>
                </th>
                <th>
                    <a href="#">
                        Quantidade
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Estq_cautela_padrao_artigos as $e) {
                echo '<tr>';
                echo '<td>';
                echo '<a href="#">';
                echo    $e->id;
                echo '</a>';
                echo '</td>';
                echo '<td>';
                echo '<a href="#">';
                echo  $e->getEstq_artigo()->nome; 
                echo '</a>';
                echo '</td>';
                echo '<td>';
                echo '<a href="#">';
                echo   $e->getEstq_cautela_padrao()->descricao; 
                echo '</a>';
                echo '</td>';
                echo '<td>';
                echo '<a href="#">';
                echo $e->quantidade;
                echo '</a>';
                echo '</td>';
                if($this->getParam('modal')==1) { 
                    echo '<td width="20">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar " class="btn btn-xs btn-info" style="max-width:50px; max-height:50px;"
                            onclick="Navegar(\''.$this->Html->getUrl("Estq_cautela_padrao_artigo", "edit", array("ajax" => true, "modal" => "1", 
                            'id' => $e->id,'estq_artigo'=>$e->getEstq_artigo()->id)).'\',\'go\')">
                            <span class="fa fa-pencil-square"></span></a>';
                    echo '</td>'; 
                    echo '<td width="20">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar " class="btn btn-xs btn-danger" style="max-width:50px; max-height:50px;"
                            onclick="Navegar(\''.$this->Html->getUrl("Estq_cautela_padrao_artigo", "delete", array("ajax" => true, "modal" => "1", 'id' => $e->id)).'\',\'go\')">
                            <span class="fa fa-trash-o"></span></a>';
                    echo '</td>';  
                }else{
                    echo '<td width="50">';
                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Estq_cautela_padrao_artigo', 'edit', 
                        array('id' => $e->id), 
                        array('class' => 'btn btn-warning btn-sm'));
                    echo '</td>';
                    echo '<td width="50">';
                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_cautela_padrao_artigo', 'delete', 
                        array('id' => $e->id), 
                        array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                    echo '</td>';
                }
              
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Estq_cautela_padrao_artigo', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Estq_cautela_padrao_artigo', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>