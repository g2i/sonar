<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cautela Padrão Artigo</h2>
        <ol class="breadcrumb">
            <li>Cautela Padrão Artigo</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_cautela_padrao_artigo', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="artigo_id">Artigo</label>
                                <select name="artigo_id" class="form-control" id="artigo_id">
                                <?php
                                foreach ($Estq_artigos as $e) {
                                    if ($e->id == $Estq_cautela_padrao_artigo->artigo_id)
                                        echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                    else
                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                }
                                ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="quantidade">Quantidade</label>
                                <input type="text" name="quantidade" id="quantidade" class="form-control"  value="<?php echo $Estq_cautela_padrao_artigo->quantidade ?>" placeholder="Quantidade">
                            </div>
                            <?php if($this->getParam('modal')){ ?>
                            <!--passa param para a modal-->     
                            <input type="hidden" name="cautela_padrao_id" value="<?php echo $Estq_cautela_padrao_artigo->cautela_padrao_id;?>"> 
                            <?php }else{?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="cautela_padrao_id">Cautela Padrão</label>
                                <select name="cautela_padrao_id" class="form-control" id="cautela_padrao_id">
                                    <?php
                                    foreach ($Estq_cautela_padraos as $e) {
                                        if ($e->id == $Estq_cautela_padrao_artigo->cautela_padrao_id)
                                            echo '<option selected value="' . $e->id . '">' . $e->descricao . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php }?>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Estq_cautela_padrao_artigo->id;?>">
                         <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                        <?php if($this->getParam('modal')){ ?>
                            <div class="text-right">
                                <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')"> Cancelar </a>
                                <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="salvar">
                            </div>
                        <?php }else{?>
                            <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_cautela_padrao_artigo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                        <?php }?>
                       
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>