<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;    </span></button>
        <h2><?php echo $Estq_lote_substoque->quantidade; ?></h2>
    </div>
    <div class="panel-body">
<p><strong>Estoque Mínimo</strong>: <?php echo $Estq_lote_substoque->estoque_minimo;?></p>
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_lote_substoque->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_lote_substoque->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Artigo</strong>:
    <?php
    echo $this->Html->getLink($Estq_lote_substoque->getEstq_artigo()->nome, 'Estq_artigo', 'view',
    array('id' => $Estq_lote_substoque->getEstq_artigo()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Artigo Lote</strong>:
    <?php
    echo $this->Html->getLink($Estq_lote_substoque->getEstq_artigo_lote()->nome, 'Estq_artigo_lote', 'view',
    array('id' => $Estq_lote_substoque->getEstq_artigo_lote()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Sub-Estoque</strong>:
    <?php
    echo $this->Html->getLink($Estq_lote_substoque->getEstq_subestoque()->nome, 'Estq_subestoque', 'view',
    array('id' => $Estq_lote_substoque->getEstq_subestoque()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
    </div>
</div>