<?php if ($this->getParam('modal')) { ?>
<?php } else { ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Lote Sub-Estoque</h2>
            <ol class="breadcrumb">
                <li>Lote Sub-Estoque</li>
                <li class="active">
                    <strong>Editar Lote Sub-Estoque</strong>
                </li>
            </ol>
        </div>
    </div>
<?php } ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <div class="ibox-content">
                        <form method="post" role="form"
                              action="<?php echo $this->Html->getUrl('Estq_lote_substoque', 'edit') ?>">
                            <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                            </div>
                            <div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="lote" class="required"><span
                                            class="glyphicon glyphicon-asterisk"></span>
                                        Lote</label>
                                    <select name="lote" class="form-control" id="lote" required>
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($Estq_artigo_lotes as $e) {
                                            if ($e->id == $Estq_lote_substoque->lote)
                                                echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                            else
                                                echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="subestoque"><span
                                            class="glyphicon glyphicon-asterisk"></span>Sub-Estoque</label>
                                    <select name="subestoque" class="form-control" id="subestoque" required>
                                        <option value="">Selecione</option>
                                        <?php
                                        foreach ($Estq_subestoques as $e) {
                                            if ($e->id == $Estq_lote_substoque->subestoque)
                                                echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                            else
                                                echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="quantidade"><span
                                            class="glyphicon glyphicon-asterisk"></span>Quantidade</label>
                                    <input type="number" step="0,01" name="quantidade" id="quantidade"
                                           class="form-control"
                                           value="<?php echo $Estq_lote_substoque->quantidade ?>"
                                           placeholder="Quantidade"
                                           required readonly>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="estoque_minimo"><span class="glyphicon glyphicon-asterisk"></span>Estoque
                                        mínimo</label>
                                    <input type="number" step="0,0001" name="estoque_minimo" id="estoque_minimo"
                                           class="form-control"
                                           value="<?php echo $Estq_lote_substoque->estoque_minimo ?>"
                                           placeholder="Estoque Mínimo" required>
                                </div>

                                <input type="hidden" name="situacao" value="1"/>
                                <?php if ($this->getParam('modal')) { ?>
                                    <input type="hidden" name="modal" value="1"/>
                                    <input type="hidden" name="artigo" value="<?php echo $Estq_lote_substoque->artigo; ?>"/>
                                <?php } else { ?>
                                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                        <label for="artigo">Artigo</label>
                                        <select name="artigo" class="form-control" id="artigo">
                                            <option value="">Selecione</option>
                                            <?php
                                            foreach ($Estq_artigos as $e) {
                                                if ($e->id == $Estq_lote_substoque->artigo)
                                                    echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                else
                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                <?php } ?>

                                <div class="clearfix"></div>
                            </div>


                            <input type="hidden" name="id" value="<?php echo $Estq_lote_substoque->id; ?>">
                            <?php if ($this->getParam('modal')) { ?>
                                <input type="hidden" name="modal" value="1"/>
                                <div class="text-right">
                                    <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                                        Cancelar
                                    </a>
                                    <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                                           value="salvar">
                                </div>
                            <?php } else { ?>
                                <div class="text-right">
                                    <a href="<?php echo $this->Html->getUrl('Estq_lote_substoque', 'all') ?>"
                                       class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                    <input type="submit" class="btn btn-primary" value="salvar">
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
