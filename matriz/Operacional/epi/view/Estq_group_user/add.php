<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo de Usuários</h2>
        <ol class="breadcrumb">
            <li>Grupo de Usuários</li>
            <li class="active">
                <strong>Adicionar Grupo</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <div class="ibox-content">
                        <form method="post" role="form"
                              action="<?php echo $this->Html->getUrl('Estq_group_user', 'add') ?>">
                            <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                            </div>
                            <div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label class="required" for="nome">Nome <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="nome" id="nome" class="form-control"
                                           value="<?php echo $Estq_group_user->nome ?>" placeholder="Nome" required>
                                </div>
                                <input type="hidden" name="situacao" value="1">

                                <div class="clearfix"></div>
                            </div>
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Estq_group_user', 'all') ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>