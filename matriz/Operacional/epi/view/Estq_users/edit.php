<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Usuário</h2>
        <ol class="breadcrumb">
            <li>Usuário</li>
            <li class="active">
                <strong>Editar Usuário</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content no-borders">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_users', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="col-md-8">
                                <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                    <label class="required" for="nome">Nome <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="nome" id="nome" class="form-control"
                                           value="<?php echo $Estq_users->nome ?>" placeholder="Nome" required>
                                </div>
                                <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                    <label class="required" for="login">Login <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="login" id="login" class="form-control"
                                           value="<?php echo $Estq_users->login ?>" placeholder="Login" required>
                                </div>
                                <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                    <label class="required" for="email">Email <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="email" name="email" id="email" class="form-control"
                                           value="<?php echo $Estq_users->email ?>" placeholder="Email" required>
                                </div>

                                <input type="hidden" name="situacao" value="1"/>

                                <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                    <label for="grupo">Grupo</label>
                                    <select name="grupo" class="form-control" id="grupo">
                                        <?php
                                        foreach ($Estq_group_useres as $e) {
                                            if ($e->id == $Estq_users->grupo)
                                                echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                            else
                                                echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="col-md-12">
                                    <label for="imagem">Imagem</label><br/>

                                    <div class="img-preview preview-lg"></div>
                                    <div class="clearfix"></div>
                                    <label class="btn btn-primary btn-upload" for="inputImage"
                                           title="Upload image file">
                                        <input class="sr-only" id="inputImage" name="file" type="file" accept="image/*"
                                               disabled>
                                    <span class="docs-tooltip" data-toggle="tooltip" title=""
                                          data-original-title="Import image with Blob URLs"><span
                                            class="glyphicon glyphicon-search"></span> Imagem</span>
                                    </label>
                                </div>
                                <textarea name="imagem" id="imagem" style="display: none"></textarea>
                            </div>

                            <div class="clearfix"></div>
                        </div>


                        <input type="hidden" name="id" value="<?php echo $Estq_users->id; ?>">

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_users', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>

                            <input type="submit" value="Enviar" class="btn btn-default"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>