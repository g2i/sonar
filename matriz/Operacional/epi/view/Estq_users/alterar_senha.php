<div
    class="row wrapper <?php echo ($this->getParam('ajax') || $this->getParam('modal')) ? '' : 'border-bottom white-bg'; ?> page-heading">
    <div class="col-lg-9">
        <h2>Usuário</h2>
        <ol class="breadcrumb">
            <li>Alterar</li>
            <li class="active">
                <strong>Senha</strong>
            </li>
        </ol>
    </div>
    <?php if ($this->getParam('modal') || $this->getParam('ajax')) { ?>
        <div class="col-lg-1 pull-right">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>

    <?php } ?>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form class="form" method="post" action="<?php echo $this->Html->getUrl(CONTROLLER, 'alterar_senha') ?>">

                    <div class="well well-lg">
                        <div class="form-group">
                            <label for="senha" class="required">Senha <span class="glyphicon glyphicon-asterisk"></span>
                            </label>
                            <input type="password" name="senha" id="senha" placeholder="Senha" class="form-control senha" required/>
                        </div>
                        <div class="form-group">
                            <label for="con_senha" class="required">Confirmar a Senha<span class="glyphicon glyphicon-asterisk"></span>
                            </label>
                            <input type="password" name="con_senha" id="con_senha" placeholder="Confirmar a Senha" class="form-control con_senha" required/>
                        </div>
                    </div>
                    <div class="text-right">
                        <input type="hidden" name="id" value="<?php echo $Usuario->id; ?>">
                        <a href="<?php echo $this->Html->getUrl('Usuario', 'conta') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" onclick="return ChecarSenha()" value="Alterar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function ChecarSenha(){
        if($(".senha").val()!=$(".con_senha").val()){
            OpenMensagem("Alerta","As senhas não são iguais!");
            return false;
        }
        return true;
    }
</script>