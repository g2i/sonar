<h1><?= __('Bem vindo ao Instalador') ?></h1>
<div class="jumbotron">
    <p>O instalador do <em>lazyphp</em> permite a geração automática dos models, controllers e views a partir da
        estrutura de sua base de dados, facilitando o início do desenvolvimento de seu sistema.</p>

    <p><strong>Antes de instalar, leia estas recomendações:</strong></p>
    <ul>
        <li>Verifique atentamente o arquivo \config.php;</li>
        <li>Cada tabela do seu BD deve ter uma chave primária única e auto-incremental;</li>
        <li>Verifique as chaves estrangeiras; Eventuais mapeamentos serão gerados a partir delas;</li>
        <li>Embora não seja obrigatório, recomendo que o nome das tabelas esteja no singular;</li>
        <li>Verifique as permissões de escrita nos diretórios;</li>
        <li>Apague o arquivo <em>\controller\InstallController.php</em> após a instalação.</li>
    </ul>

</div>
<div class="panel panel-default">
    <?php if ($ok) { ?>
        <div class="panel-heading">
            <h3 class="panel-title">Escolha os arquivos que deseja instalar</h3>
        </div>
        <form method='post' role="form">


            <div class="col-md-12" style="margin-top: 25px;">

                <div class="text-right">
                    <input class='btn btn-default btn-sm' type='button' value='Desmarcar todos' onclick='uncheck();'>
                    <input class='btn btn-default btn-sm' type='button' value='Marcar todos' onclick='check();'>
                </div>

                <hr/>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach ($tables as $t): ?>
                        <?php

                        $id = "id-" . $t->name;

                        ?>


                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <button class="btn btn-xs btn-default" type="button"  data-toggle="collapse" data-parent="#accordion"
                                            href="#<?php echo $id; ?>">
                                        <span class="glyphicon glyphicon-chevron-down"></span>
                                    </button>

                                    <?php echo $t->name; ?>

                                    <div class="pull-right">
                                        <label><input type="checkbox" class="principal"
                                                      name="model<?php echo $t->name; ?>"
                                                      checked/>
                                            <small style="margin-right: 15px;">Modelo</small>
                                        </label>
                                        <label><input type="checkbox" class="principal"
                                                      name="controller<?php echo $t->name; ?>"
                                                      checked/>
                                            <small style="margin-right: 15px;">Controlador</small>
                                        </label>
                                        <label><input type="checkbox" class="principal"
                                                      name="view<?php echo $t->name; ?>"
                                                      checked/>
                                            <small style="margin-right: 15px;">Visão</small>
                                        </label>
                                        <label><input type="checkbox" class="principal"
                                                      name="menu<?php echo $t->name; ?>"
                                                      checked/>
                                            <small style="margin-right: 15px;">Menu</small>
                                        </label>
                                    </div>
                                </h4>
                            </div>
                            <div id="<?php echo $id; ?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="panel-group" id="configs-acordion" area-multiselectable="true">
                                        <?php include "view/Install/config-titulos.php"; ?>
                                        <?php include "view/Install/config-filtro.php"; ?>
                                        <?php include "view/Install/config-listagem.php"; ?>
                                        <?php include "view/Install/config-cadastro.php"; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="text-right col-md-12">
                <h4>Menu de Navegação</h4>

                <div class="clearfix"></div>
                <div class="checkbox pull-right">
                    <label>
                        <input type="checkbox" name="menu" checked>
                        <small>(Re)Instalar menu</small>
                    </label>
                </div>
                <div class="clearfix"></div>
                <h4>Sobrescrever Modelos, Visões e Controladores caso existam?</h4>

                <div class="clearfix"></div>
                <div class="checkbox pull-right">
                    <label>
                        <input type="checkbox" name="sobrescrever">
                        <small>sobrescrever?</small>
                    </label>
                </div>
                <div class="clearfix"></div>
            </div>


            <div class="clearfix"></div>


            <div class="panel-footer">
                <input type="submit" value="<?= __('Instalar'); ?>" class="btn btn-primary pull-right">
                <br class="clearfix"><br class="clearfix">
            </div>
        </form>
    <?php } ?>
</div>
<script>
    function check() {
        var formularios = $("input.principal");
        for (var i = 0, j = 0; i < formularios.length; i++) {
            if (formularios[i].type == "checkbox") {
                j++;
                if (j > 0)
                    formularios[i].checked = true;
            }
        }
    }
    function uncheck() {
        var formularios = $("input.principal");
        for (var i = 0, j = 0; i < formularios.length; i++) {
            if (formularios[i].type == "checkbox") {
                j++;
                if (j > 0)
                    formularios[i].checked = false;
            }
        }
    }
</script>
