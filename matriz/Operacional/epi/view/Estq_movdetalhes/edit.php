<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_movdetalhes', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group col-md-6">
                            <label for="artigo" class="required">Artigo <span class="glyphicon glyphicon-asterisk"></span></label><br/>
                            <select name="artigo" id="artigo" class="form-control artigo artigo-ajax">
                                <option value="">Selecione:</option>
                                <?php
                                    echo '<option selected value="' . $artigo->id . '">' . $artigo->nome . '</option>';
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="codigo_livre" class="required">Código</label>
                            <select name="codigo_livre" class="form-control codigo codigo-ajax" id="codigo_livre">
                                <option value="">Selecione:</option>
                                <?php
                                        echo '<option selected value="' . $artigo->id . '">' . $artigo->codigo_livre . '</option>';
                                ?>
                            </select>
                        </div>
                        <?php if($movimento->padrao==2){ ?>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="lote_substoque" class="required">Lote Sub-Estoque<span
                                    class="glyphicon glyphicon-asterisk"></span></label>

                            <select name="lote_substoque" class="form-control lote_substoque" id="lote_substoque" required>
                                <?php
                                     echo '<option value="' . $lote_substoque->id . '">' .$lote_substoque->getEstq_artigo_lote()->nome ." - ".  $lote_substoque->getEstq_subestoque()->nome . '</option>';
                                 ?>
                            </select>
                        </div>
                        <?php }else{ ?>
                            <input type="hidden" name="lote_substoque" id="lote_substoque" class="lote_substoque" value="<?= $movimento->sub_padrao ?>" />
                        <?php } ?>

                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="quantidade" class="required">Quantidade<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="number" step="0,01" name="quantidade" id="quantidade" class="form-control"
                                   value="<?php echo $Estq_movdetalhes->quantidade ?>" placeholder="Quantidade" required>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="vlcusto">Valor Custo</label>
                            <input type="text" name="vlcusto" id="vlcusto" class="form-control"
                                   value="<?php echo $Estq_movdetalhes->vlcusto ?>" data-mask="real"
                                   placeholder="Valor Custo">
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="vlnf">Valor Nota Fiscal</label>
                            <input type="text" data-mask="real" name="vlnf" id="vlnf" class="form-control"
                                   value="<?php echo $Estq_movdetalhes->vlnf ?>" placeholder="Valor Nota Fiscal">
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="vlcustomedio">Valor Custo Médio</label>
                            <input type="text" data-mask="real" name="vlcustomedio" id="vlcustomedio"
                                   class="form-control" value="<?php echo $Estq_movdetalhes->vlcustomedio ?>"
                                   placeholder="Valor Custo Médio">
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="marca">Marca</label>
                            <input type="text" name="marca" id="marca" class="form-control"
                                   value="<?php echo $Estq_movdetalhes->marca ?>" placeholder="Marca">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="validade">Validade</label>
                            <div class='input-group datePicker'>
                                <input type='text' name="validade" id="validade" class="form-control date"  value="<?php echo $Estq_movdetalhes->validade ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                            </div>
                        </div>
                        <input type="hidden" name="situacao" value="1"/>
                        <input type="hidden" class="padrao" value="<?php echo $movimento->padrao; ?>"/>
                        <input type="hidden" class="sub_padrao" value="<?php echo $movimento->sub_padrao; ?>"/>

                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Estq_movdetalhes->id;?>">
                    <?php if($this->getParam('modal')){ ?>
                        <input type="hidden" name="modal" value="1"/>
                        <div class="text-right">
                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                                Cancelar
                            </a>
                            <button type="button" class="btn btn-primary btn_sub" data-toggle="modal" onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar</button>
                            <input type="submit" onclick="EnviarFormulario('form')"  id="validForm" style="display: none;" />
                        </div>
                    <?php }else{ ?>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary btn_sub" value="salvar" >
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
<input type="hidden" id="const" class="const" value="0" />
<script>
    $(document).ready(function () {
        $('.money').mask('000.000.000.000.000,00', {reverse: true});

        $('select.codigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            })
        });
        $('select.artigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar2/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            })
        });

        moment.locale('pt-BR');

        $('.datePicker').datetimepicker({
            format: 'L',
            extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
            showTodayButton: true,
            useStrict: true,
            locale: 'pt-BR',
            showClear: true,
            allowInputToggle: true
        });

        if ($(".padrao").val() == 2) {
            $('.artigo').change(function () {
                if ($(".const").val() != 1) {
                    $.ajax({
                        type: 'POST',
                        url: root + '/Estq_movdetalhes/GetLoteSubestoque',
                        data: 'artigo=' + $(this).val(),
                        success: function (txt) {
                            $(".lote_substoque").html(txt);
                            $(".const").val(1);
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: root + '/Estq_movdetalhes/GetCodigo',
                        data: 'artigo=' + $(this).val(),
                        success: function (txt) {
                            $(".codigo").html(txt);
                            setTimeout(function () {
                                $(".codigo").trigger("chosen:updated");
                            }, 20);
                            $(".const").val(1);
                        }
                    });
                }
            });
            $('.codigo').change(function () {
                if ($(".const").val() != 1) {
                    $.ajax({
                        type: 'POST',
                        url: root + '/Estq_movdetalhes/GetLoteSubestoque',
                        data: 'artigo=' + $(this).val(),
                        success: function (txt) {
                            $(".lote_substoque").html(txt);
                            $(".const").val(1);
                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: root + '/Estq_movdetalhes/GetArtigo',
                        data: 'artigo=' + $(this).val(),
                        success: function (txt) {
                            $(".artigo").html(txt);
                            setTimeout(function () {
                                $(".artigo").trigger("chosen:updated");
                            }, 20);
                            $(".const").val(1);

                        }
                    });
                }
            });
        }else {
            if ($(".const").val() != 1) {
                $('.artigo').change(function () {
                    $.ajax({
                        type: 'POST',
                        url: root + '/Estq_movdetalhes/GetSubestoque_padrao',
                        data: {
                            artigo: $(this).val(),
                            sub_padrao: $(".sub_padrao").val()
                        },
                        dataType: 'JSON',
                        success: function (txt) {
                            if (txt != 2) {
                                if ($("#operacao").val() == '-') {
                                    if (parseInt(txt.quantidade) <= 0) {
                                        BootstrapDialog.show({
                                            title: 'Alerta',
                                            message: 'Não existe artigos disponíveis neste Subestoque! \n Favor escolher outro!',
                                            type: BootstrapDialog.TYPE_DANGER,
                                            buttons: [{
                                                label: 'OK',
                                                action: function (dialogItself) {
                                                    dialogItself.close();
                                                }
                                            }]
                                        })
                                        $(".btn_sub").attr("disabled","disabled");
                                        return false;
                                    } else {
                                        $(".qtde").text(' Disponível: ' + txt.quantidade);
                                        $(".quantidade").removeAttr('disabled');
                                        $(".quantidade").attr("max", parseInt(txt.quantidade));
                                        $(".btn_sub").removeAttr("disabled");
                                    }

                                }
                                $(".btn_sub").removeAttr("disabled");
                                $(".lote_substoque").val(txt.id);
                            } else {
                                BootstrapDialog.alert('Artigo não disponível no sub-estoque!');
                                $(".btn_sub").attr("disabled","disabled");
                                return false;
                            }

                        }
                    });
                    $.ajax({
                        type: 'POST',
                        url: root + '/Estq_movdetalhes/GetCodigo',
                        data: 'artigo=' + $(this).val(),
                        success: function (txt) {
                            $(".codigo").html(txt);
                            setTimeout(function () {
                                $(".codigo").trigger("chosen:updated");
                            }, 20);

                        }
                    });

                });

                $('.codigo').change(function () {
                    $.ajax({
                            type: 'POST',
                            url: root + '/Estq_movdetalhes/GetSubestoque_padrao',
                            data: {
                                artigo: $(this).val(),
                                sub_padrao: $(".sub_padrao").val()
                            },
                            dataType: 'JSON',
                            success: function (txt) {
                                if (txt != 2) {
                                    if ($("#operacao").val() == '-') {
                                        if (parseInt(txt.quantidade) <= 0) {
                                            BootstrapDialog.show({
                                                title: 'Alerta',
                                                message: 'Não existe artigos disponíveis neste Subestoque! \n Favor escolher outro!',
                                                type: BootstrapDialog.TYPE_DANGER,
                                                buttons: [{
                                                    label: 'OK',
                                                    action: function (dialogItself) {
                                                        dialogItself.close();
                                                    }
                                                }]
                                            });
                                            $(".btn_sub").attr("disabled","disabled");
                                            return false;
                                        }
                                        else {

                                            $(".qtde").text(' Disponível: ' + txt.quantidade);
                                            $(".quantidade").removeAttr('disabled');
                                            $(".quantidade").attr("max", parseInt(txt.quantidade));
                                        }
                                        $(".lote_substoque").val(txt.id);
                                        $(".btn_sub").removeAttr("disabled");

                                    }
                                } else {
                                    $(".btn_sub").attr("disabled","disabled");
                                    BootstrapDialog.alert('Artigo não disponível no sub-estoque!');
                                    return false;
                                }

                            }
                        }
                    );
                    $.ajax({
                        type: 'POST',
                        url: root + '/Estq_movdetalhes/GetArtigo',
                        data: 'artigo=' + $(this).val(),
                        success: function (txt) {
                            $(".artigo").html(txt);
                            setTimeout(function () {
                                $(".artigo").trigger("chosen:updated");
                            }, 20);
                        }
                    });
                });

            }
        }

    });

</script>