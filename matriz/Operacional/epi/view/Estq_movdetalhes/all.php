<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <?php if ($this->getParam('modal')) {
                        echo '<div class="row">';
                        echo '<div class="text-right pull-right col-md-1">';
                        echo '<a href="' . SITE_PATH . '/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=1&mov=' . $Estq_mov->id . '" target="_blanck" class="btn btn-default btn-sm">';
                        echo '<span class="glyphicon glyphicon-print" data-toggle="tooltip" data-placement="auto" title="Imprimir Cautela"></span> ';
                        echo '</a>';
                        echo '</div>';
                        if ($Estq_mov->status == 1 || empty($Estq_mov->status)) {

                            ?>
                            <div class="text-right col-md-6 pull-right">

                                <a href="Javascript:void(0)" class="btn btn-default"
                                   onclick="Navegar('<?php echo $this->Html->getUrl("Estq_movdetalhes", "add", array('modal' => 1, 'ajax' => true, 'id' => $this->getParam('id'))) ?>','go')"
                                   data-tool="tooltip" data-placement="bottom" title="Cadatrar Detalhes">
                                    <span class="img img-add"></span> Novo Detalhe-Movimentação</a>
                            </div>

                        <?php }
                        echo '</div>';

                    } else { ?>
                        <!--
                            <!-- formulario de pesquisa -->
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Filtros</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="filtros ">
                                    <div class="form">
                                        <form role="form"
                                              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                              method="post" enctype="application/x-www-form-urlencoded">
                                            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                            <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                                            <div class="col-md-3 form-group">
                                                <label for="artigo">Artigo</label>
                                                <select name="filtro[externo][artigo]" class="form-control" id="artigo">
                                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                                    <?php foreach ($Estq_artigos as $e): ?>
                                                        <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <label for="lote_substoque">Lote Sub-Estoque</label>
                                                <select name="filtro[externo][lote_substoque]" class="form-control"
                                                        id="lote_substoque">
                                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                                    <?php foreach ($Estq_lote_substoques as $e): ?>
                                                        <?php echo '<option value="' . $e->id . '">' . $e->subestoque . '</option>'; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <label for="movimento">Movimento</label>
                                                <select name="filtro[externo][movimento]" class="form-control"
                                                        id="movimento">
                                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                                    <?php foreach ($Estq_movs as $e): ?>
                                                        <?php echo '<option value="' . $e->id . '">' . $e->data . '</option>'; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-3 form-group">
                                                <label for="situacao">Situação</label>
                                                <select name="filtro[externo][situacao]" class="form-control"
                                                        id="situacao">
                                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                                    <?php foreach ($Estq_situacaos as $e): ?>
                                                        <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-12 text-right">
                                                <button type="button" class="btn btn-default botao-impressao"
                                                        data-tool="tooltip"
                                                        data-placement="bottom" title="Impressão"><span
                                                        class="glyphicon glyphicon-print"></span></button>
                                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                                   class="btn btn-default"
                                                   data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                                   title="Recarregar a página"><span
                                                        class="glyphicon glyphicon-refresh "></span></a>
                                                <button type="submit" class="btn btn-default" data-tool="tooltip"
                                                        data-placement="bottom" title="Pesquisar"><span
                                                        class="glyphicon glyphicon-search"></span></button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($Estq_mov->status == 1 || empty($Estq_mov->status)) { ?>
                            <!-- botao de cadastro -->
                            <div class="text-right">
                                <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Estq_movdetalhes', 'add', array('id' => $this->getParam('id')), array('class' => 'btn btn-primary', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Cadastrar Detalhes")); ?></p>
                            </div>

                        <?php }
                    } ?>
                    <div>
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Detalhes do Movimento</h5>
                            </div>
                            <div class="ibox-content">
                                <!-- tabela de resultados -->
                                <div class="clearfix">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tr>
                                                <th>
                                                    <a href='<?php echo $this->Html->getUrl('Estq_movdetalhes', 'all', array('orderBy' => 'artigo')); ?>'>
                                                        Artigo
                                                    </a>
                                                </th>
                                                <th>
                                                    <a href='<?php echo $this->Html->getUrl('Estq_movdetalhes', 'all', array('orderBy' => 'quantidade')); ?>'>
                                                        Quantidade
                                                    </a>
                                                </th>
                                                <th>
                                                    <a href='<?php echo $this->Html->getUrl('Estq_movdetalhes', 'all', array('orderBy' => 'lote_substoque')); ?>'>
                                                        Lote/Subestoque
                                                    </a>
                                                </th>
                                                <th>
                                                    <a href='<?php echo $this->Html->getUrl('Estq_movdetalhes', 'all', array('orderBy' => 'marca')); ?>'>
                                                        Marca
                                                    </a>
                                                </th>
                                                <th>
                                                    <a href='<?php echo $this->Html->getUrl('Estq_movdetalhes', 'all', array('orderBy' => 'validade')); ?>'>
                                                        Validade
                                                    </a>
                                                </th>

                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            <?php
                                            foreach ($Estq_movdetalhes as $e) {
                                                echo '<tr>';
                                                echo '<td>';
                                                echo $this->Html->getLink($e->getEstq_artigo()->nome, 'Estq_artigo', 'view',
                                                    array('id' => $e->getEstq_artigo()->id), // variaveis via GET opcionais
                                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                                echo '</td>';
                                                echo '<td>';
                                                echo $this->Html->getLink($e->quantidade, 'Estq_movdetalhes', 'view',
                                                    array('id' => $e->id), // variaveis via GET opcionais
                                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                                echo '</td>';
                                                echo '<td>';
                                                echo $this->Html->getLink($e->getEstq_lote_substoque()->getEstq_artigo_lote()->nome . '-' . $e->getEstq_lote_substoque()->getEstq_subestoque()->nome, 'Estq_movdetalhes', 'view',
                                                    array('id' => $e->id), // variaveis via GET opcionais
                                                    array('data-toggle' => 'modal', "data-mask" => "real")); // atributos HTML opcionais
                                                echo '</td>';
                                                echo '<td>';
                                                echo $this->Html->getLink($e->marca, 'Estq_movdetalhes', 'view',
                                                    array('id' => $e->id), // variaveis via GET opcionais
                                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                                echo '</td>';
                                                echo '<td>';
                                                echo $this->Html->getLink(ConvertData($e->validade), 'Estq_movdetalhes', 'view',
                                                    array('id' => $e->id), // variaveis via GET opcionais
                                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                                echo '</td>';

                                                if ($Estq_mov->status == 1 || empty($Estq_mov->status)) {
                                                    if ($this->getParam('modal') == 1) {
                                                        echo '<td width="50" style="padding-top: 0px; padding-bottom: 0px;">';
                                                        echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar Detalhe-Movimentação" class="btn  btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Estq_movdetalhes", "edit", array("ajax" => true, "modal" => "1", 'id' => $e->id, 'movimento' => $e->movimento)) . '\',\'go\')">
    <span class="glyphicon glyphicon-edit"></span></a>';
                                                        echo '</td>';
                                                        echo '<td width="50" style="padding-top: 0px; padding-bottom: 0px;">';
                                                        echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Ecluir Detalhe-Movimentação" class="btn  btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Estq_movdetalhes", "delete", array("ajax" => true, "modal" => "1", 'id' => $e->id, 'movimento' => $e->movimento)) . '\',\'go\')">
    <span class="glyphicon glyphicon-remove"></span></a>';
                                                        echo '</td>';
                                                    } else {

                                                        echo '<td width="50">';
                                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Estq_movdetalhes', 'edit',
                                                            array('id' => $e->id, 'movimento' => $e->movimento),
                                                            array('class' => 'btn btn-warning btn-sm', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Editar"));
                                                        echo '</td>';
                                                        echo '<td width="50">';
                                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_movdetalhes', 'delete',
                                                            array('id' => $e->id, 'movimento' => $e->movimento),
                                                            array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Deletar"));
                                                        echo '</td>';
                                                        echo '</tr>';
                                                    }
                                                } else {
                                                    echo '<td>&nbsp;</td>';
                                                    echo '<td>&nbsp;</td>';
                                                }
                                            }
                                            ?>
                                        </table>

                                        <!-- menu de paginação -->
                                        <div style="text-align:center"><?php echo $nav; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function () {
        $('#search').keyup(function () {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Estq_movdetalhes', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    else
                        echo '"' . $this->Html->getUrl('Estq_movdetalhes', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>