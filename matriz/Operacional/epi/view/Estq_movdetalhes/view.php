<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">
                <?php if($this->getParam('modal')){ ?>
                <div class="right">
                    <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="text-left col-md-6">
                    <a href="Javascript:void(0)" class="btn btn-sm" onclick="Navegar('','back')">
                        <span class="img img-return"></span></a>
                </div>
                <?php } ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;    </span></button>
        <h2>Visualização de detalhes de movimento</h2>
    </div>
    <div class="panel-body">
<p><strong>Valor Custo</strong>: <?php echo $Estq_movdetalhes->vlcusto;?></p>
<p><strong>Valor Nota Fiscal </strong>: <?php echo $Estq_movdetalhes->vlnf;?></p>
<p><strong>Valor Custo Médio</strong>: <?php echo $Estq_movdetalhes->vlcustomedio;?></p>
<p><strong>Quantidade</strong>: <?php echo $Estq_movdetalhes->quantidade;?></p>
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_movdetalhes->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_movdetalhes->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Artigo</strong>:
    <?php
    echo $this->Html->getLink($Estq_movdetalhes->getEstq_artigo()->nome, 'Estq_artigo', 'view',
    array('id' => $Estq_movdetalhes->getEstq_artigo()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Movimento</strong>:
    <?php
    echo $this->Html->getLink($Estq_movdetalhes->getEstq_mov()->tipodoc, 'Estq_mov', 'view',
    array('id' => $Estq_movdetalhes->getEstq_mov()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Lote Sub-Estoque</strong>:
    <?php
    echo $this->Html->getLink($Estq_movdetalhes->getEstq_lote_substoque()->id, 'Estq_lote_substoque', 'view',
    array('id' => $Estq_movdetalhes->getEstq_lote_substoque()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
        <p><strong>Marca</strong>: <?php echo $Estq_movdetalhes->marca;?></p>
    </div>
</div>
                </div>
            </div>
        </div>
    </div>
    </div>
