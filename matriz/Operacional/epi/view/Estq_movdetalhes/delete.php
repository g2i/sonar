<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Estq_movdetalhes', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Estq_movdetalhes->getEstq_lote_substoque()->descricao; ?></strong>?</p>
    </div>
    <input type="hidden" name="id" value="<?php echo $Estq_movdetalhes->id; ?>">
    <?php if($this->getParam('modal')){ ?>
        <input type="hidden" name="modal" value="1" />
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary" value="Excluir">
        </div>
    <?php }else{ ?>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
    <?php } ?>
</form>
                    </div>
                </div>
            </div>
        </div>
    </div>

