<div class="arts">
    <div class="table-responsive">
        <table class="table table-hover ">
            <tr>
                <th>Codigo</th>
                <th>Artigo</th>
                <th>Lote-subestoque</th>
                <th>Quantidade</th>
                <th>Marca</th>
                <th>Validade</th>
                <th>Novo / Usado</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($ret as $r => $item) {
                echo '<tr>';
                echo '<td class="d-id">' . $item['codigo'] . '</td>';
                echo '<td>' . $item['artigo'] . '</td>';
                echo '<td>' . $item['lote_subestoque'] . '</td>';
                echo '<td>' . $item['quantidade'] . '</td>';
                echo '<td>' . $item['marca'] . '</td>';
                echo '<td>' . $item['validade'] . '</td>';
                echo '<td>' . $item['novo_usado'] . '</td>';
                echo '<td>';
                echo '<a href="Javascrip:void(0)" onclick="dell_iten(' . $r . ')" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>';
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>
    </div>
<input type="hidden" id="count_itens" value="<?= count($_SESSION['itens']) ?>"/>
</div>

