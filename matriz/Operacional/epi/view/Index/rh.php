<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">vencidas</span>
                    <h5>Cautelas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $cautelas_vencidas->total; ?></h1>

                    <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                    <small>cautelas</small>
                </div>
            </div>
        </div>

        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">a vencer</span>
                    <h5>Cautelas </h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $cautelas_avencer->total; ?></h1>

                    <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                    <small>cautelas</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">ativos</span>
                    <h5>Artigos</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $artigos->total; ?></h1>

                    <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                    <small>artigos</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <a href="<?php echo $this->Html->getUrl('Estq_artigo', 'a_comprar') ?>" data-toggle="modal">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">a comprar</span>
                        <h5>Artigos</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php echo $a_comprar->total; ?></h1>

                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>artigos</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ocorrências</h5>

                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-search"></span> Pesquisar Ocorrências', 'Rhocorrencia', 'all', array('ajax' => true, 'first' => 1, 'modal' => 1), array('class' => 'btn btn-default', 'data-toggle' => 'modal', 'data-target' => '.bs-lg')); ?>

                            <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus"></span> Nova Ocorrência', 'Rhocorrencia', 'add', NULL, array('class' => 'btn btn-default')); ?></p>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Colaborador</th>
                                <th>Ocorrência</th>
                                <th>Última Data</th>
                                <th>Dias</th>
                                <th>Vencimento</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($ocor as $o) {

                                if (!empty(Rhocorrencia::getOcorrencia_individual($o->id_prof, $o->comp)) && strtotime($o->data_out) < strtotime(date('Y-m-d'))) {

                                } else {
                                    $clas = "";
                                    if (strtotime($o->vencimento) < strtotime(date('Y-m-d'))) {
                                        $clas = 'class="text-danger"';
                                    }
                                  
                                    echo '<tr ' . $clas . '>';
                                    echo '<td>' . $o->nome . '</td>';
                                    echo '<td>' . $o->descricao . '</td>';
                                    echo '<td>' . ConvertData($o->data) . '</td>';
                                    //vencimento
                                    echo '<td>' . diferenca_datas($o->data, $o->vencimento) . '</td>';
                                    echo '<td>' . ConvertData($o->vencimento) . '</td>';

                                    
                                    echo '<td>';
                                    echo '<td style="padding: 0 2px 0 2px;">';
                                    echo $this->Html->getLink('<span class="img img-edit" data-toggle="tooltip" data-placement="auto" title="Editar Ocorrência"></span> ', 'Rhocorrencia', 'edit',
                                        array('id' => $o->codigo_ocorrencia),
                                        array('class' => 'btn btn-sm'));
                                    echo '</td>';
                                    echo '<td style="padding: 0 2px 0 2px;">';
                                    echo $this->Html->getLink('<span class="img img-remove" data-toggle="tooltip" data-placement="auto" title="Desativar Ocorrência"></span> ', 'Rhocorrencia', 'delete',
                                        array('id' => $o->codigo_ocorrencia),
                                        array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td  style="padding: 0 2px 0 2px;">';
                                    echo $this->Html->getLink('<span class="img img-anexo" data-toggle="tooltip" data-placement="top" title="Anexos"></span> ', 'Anexoocorrencia', 'all',
                                        array('id' => $o->codigo_ocorrencia, 'first' => 1, 'modal' => 1),
                                        array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>