<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-2">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">vencidas</span>
                    <h5>Cautelas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $cautelas_vencidas->total; ?></h1>

                    <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                    <small>cautelas</small>
                </div>
            </div>
        </div>

        <div class="col-lg-2">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">a vencer</span>
                    <h5>Cautelas </h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $cautelas_avencer->total; ?></h1>

                    <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                    <small>cautelas</small>
                </div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">ativos</span>
                    <h5>Artigos</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $artigos->total; ?></h1>

                    <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                    <small>artigos</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <a href="<?php echo $this->Html->getUrl('Estq_artigo', 'a_comprar') ?>" data-toggle="modal" >
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">a comprar</span>
                        <h5>Artigos</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php echo $a_comprar->total; ?></h1>

                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>artigos</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <a href="<?php echo $this->Html->getUrl('Estq_artigo', 'artigos_vencidos') ?>" data-toggle="modal" >
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">últimos 30 dias</span>
                        <h5>Artigos Vencidos</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><?php echo $artigoVencido->total; ?></h1>

                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>artigos</small>
                    </div>
                </a>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Controle de Vencimento</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Colaborador</th>
                                <th>Artigo</th>
                                <th>Movimento</th>
                                <th>Vencimento</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($movimento as $e) {
                                if(strtotime($e->validade)<strtotime(date('Y-m-d')))
                                    echo '<tr class="text-danger">';
                                else
                                    echo '<tr class="text-info">';
                                echo '<td>';
                                echo $e->colaborador;
                                echo '</td>';
                                echo '<td>';
                                echo $e->artigo;
                                echo '</td>';
                                echo '<td>';
                                echo $e->id.'-'.$e->tipo;
                                echo '</td>';
                                echo '<td>';
                                echo ConvertData($e->validade);
                                echo '</td>';
                                echo '<td width="30" style="padding: 2px;">';
                                echo $this->Html->getLink('<span class="glyphicon glyphicon-align-justify" data-toggle="tooltip" data-placement="auto" title="Detalhes de Movimento"></span> ', 'Estq_movdetalhes', 'all',
                                    array('id' => $e->id, 'first' => 1, 'modal' => 1),
                                    array('class' => 'btn btn-info btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>