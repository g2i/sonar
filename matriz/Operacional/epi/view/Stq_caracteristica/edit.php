<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Características</h2>
        <ol class="breadcrumb">
            <li>Características</li>
            <li class="active">
                <strong>Editar Características</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Stq_caracteristica', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Stq_caracteristica->nome ?>" placeholder="Nome">
                            </div>
                            <input type="hidden" name="situacao" value="1"/>

                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Stq_caracteristica->id; ?>">

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Stq_caracteristica', 'all') ?>"
                               class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar
                            </button>
                            <input type="submit" id="validForm" style="display: none;"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
