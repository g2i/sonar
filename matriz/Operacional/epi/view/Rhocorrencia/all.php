    <div class="wrapper wrapper-content ">
        <div class="ibox">
            <div class="ibox-content">

                <div class="right">
                    <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                </div>


                <div class="filtros well">
                    <div class="form">
                        <form role="form" class="form-ocorrencias"
                              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                              method="get">
                            <input type="hidden" name="modal" value="1">
                            <input type="hidden" name="ajax" value="1">
                            <div class="col-md-3 form-group">
                                <label for="profissional">Profissional</label>
                                <input type="text" name="profissional" class="form-control profissi" id="profissional"
                                       placeholder="Nome Profissional" value="<?= $profissional; ?>"/>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="historico">Histórico</label>
                                <select name="historico" class="form-control" id="historico">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Rhocorrencia_historicos as $r) {
                                        if ($r->codigo == $hist)
                                            echo '<option value="' . $r->codigo . '" selected>' . $r->nome . '</option>';
                                        else
                                            echo '<option value="' . $r->codigo . '" >' . $r->nome . '</option>';
                                    } ?>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="inicio_data">Data (inicio)</label>
                                <div class='input-group inicio_data datep'>
                                    <input type='text' class="form-control datep" name="inicio_data"
                                           id="inicio_data"
                                           placeholder='dd/mm/aaaa' value="<?= $inicio_data ?>"/>
                                        <span class="input-group-addon"> <span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="fim_data">Data (fim)</label>
                                <div class='input-group fim_data datep'>
                                    <input type='text' class="form-control datep" name="fim_data" id="fim_data"
                                           placeholder='dd/mm/aaaa' value="<?= $fim_data ?>"/>
                                        <span class="input-group-addon"> <span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>

                            <div class="col-md-12 text-right">

                                <button type="button" class="btn btn-default botao-reset"><span
                                        class="glyphicon glyphicon-refresh"></span></button>
                                <button type="button" class="btn btn-default btnFiltrar"><span
                                        class="glyphicon glyphicon-search"></span></button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="ocorrencia tabela-ocorrencia">
                            <table class="table table-hover ">
                                <thead>
                                <tr>
                                    <th><a href="Javascript:void(0)">Data</a></th>
                                    <th><a href="Javascript:void(0)">Profissional</a></th>
                                    <th><a href="Javascript:void(0)">Histórico</a></th>
                                    <th><a href="Javascript:void(0)">Descrição</a></th>
                                    <th href="Javascript:void(0)">&nbsp;</th>
                                    <th href="Javascript:void(0)">&nbsp;</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($Rhocorrencias as $r) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($r->data), 'Rhocorrencia', 'view',
                                        array('id' => $r->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($r->getRhprofissional()->nome, 'Rhocorrencia', 'view',
                                        array('id' => $r->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($r->getRhocorrencia_historico()->nome, 'Rhocorrencia_historico', 'view',
                                        array('id' => $r->getRhocorrencia_historico()->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($r->descricao, 'Rhocorrencia', 'view',
                                        array('id' => $r->codigo), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td style="padding: 4px 2px 0 2px;">';
                                    echo $this->Html->getNavegar('<span class="glyphicon glyphicon-pencil"></span> ', 'Rhocorrencia', 'edit', array('id' => $r->codigo, 'ajax' => true, 'modal' => 1),
                                        array('class' => 'btn btn-warning btn-xs'));
                                    echo '</td>';
                                    echo '<td style="padding: 4px 2px 0 2px;">';
                                    echo $this->Html->getNavegar('<span class=" glyphicon glyphicon-remove"></span> ', 'Rhocorrencia', 'delete', array('id' => $r->codigo, 'ajax' => true, 'modal' => 1),
                                        array('class' => 'btn btn-danger btn-xs'));
                                    echo '</td>';
                                    echo '<td style="padding: 4px 2px 0 2px;">';
                                    echo $this->Html->getNavegar('<span class=" glyphicon glyphicon-paperclip"></span> ', 'Anexoocorrencia', 'all', array('id' => $r->codigo, 'ajax' => true, 'modal' => 1),
                                        array('class' => 'btn btn-info btn-xs'));
                                    echo '</td>';

                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>
                </div><!-- /ibox content -->
            </div><!--wrapper-->
        </div><!-- / ibox -->
    </div><!-- /row -->
<div class="clearfix"></div>
<script>
    var url = '<?= $this->Html->getUrl('Rhocorrencia', 'all') ?>';
    var filtro = {
        modal: 1,
        ajax: 1,
        profissional: '',
        historico: '',
        inicio_data: '',
        fim_data: ''


    }
    $(document).ready(function () {

        function ajustarGrid() {
            var grid = $(".tabela-ocorrencia");
            grid.find("[data-toggle='tooltip']").tooltip();
        }

        $('.btnFiltrar').click(function () {
            filtro.inicio_data = $("#inicio_data").val();
            filtro.fim_data = $("#fim_data").val();
            filtro.historico = $("#historico").val();
            filtro.profissional = $(".profissi").val();

            $("div.tabela-ocorrencia").loadGrid(url, filtro, '.tabela-ocorrencia', function () {
                ajustarGrid();
            });
        })


        $('#btnClear').click(function () {
            filtro.inicio_data = '';
            filtro.fim_data = '';
            filtro.historico = '';
            filtro.profissional = '';

            $('.form-ocorrencias')[0].reset();

            $("div.tabela-ocorrencia").loadGrid(url, filtro, '.tabela-ocorrencia', function () {
                ajustarGrid();
            });
        });


        $('.datep').datetimepicker({
            format: 'L',
            locale: 'pt-br',
            sideBySide: true,
            extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
            showTodayButton: true,
            useStrict: true,
            showClear: true,
            allowInputToggle: true,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
    })
</script>