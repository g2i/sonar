
<p><strong>Data</strong>: <?php echo $Rhocorrencia->data;?></p>
<p><strong>Usuario</strong>: <?php echo $Rhocorrencia->usuario;?></p>
<p><strong>Descrição</strong>: <?php echo $Rhocorrencia->descricao;?></p>
<p>
    <strong>Ocorrência - Histórico</strong>:
    <?php
    echo $this->Html->getLink($Rhocorrencia->getRhocorrencia_historico()->nome, 'Rhocorrencia_historico', 'view',
    array('id' => $Rhocorrencia->getRhocorrencia_historico()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Profissional</strong>:
    <?php
    echo $this->Html->getLink($Rhocorrencia->getRhprofissional()->nome, 'Rhprofissional', 'view',
    array('id' => $Rhocorrencia->getRhprofissional()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>