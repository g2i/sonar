<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;    </span></button>
        <h2><?php echo $Estq_subestoque->nome;?></h2>
    </div>
    <div class="panel-body">
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_subestoque->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_subestoque->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Tipo Sub-Estoque</strong>:
    <?php
    echo $this->Html->getLink($Estq_subestoque->getEstq_tiposubestoque()->nome, 'Estq_tiposubestoque', 'view',
    array('id' => $Estq_subestoque->getEstq_tiposubestoque()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
    </div>
</div>