<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Produtos e Serviços</h2>
        <ol class="breadcrumb">
            <li>Produtos e Serviços</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Fornecedor_servico_produto', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <?php if($this->getParam('modal')){ ?>
                            <!--passa param para a modal-->
                            <input type="hidden" name="fornecedor_id" value="<?php echo $this->getParam('id');?>">
                            <?php }else{?>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="fornecedor_id">Fornecedor</label>
                                <select name="fornecedor_id" class="form-control" id="fornecedor_id">
                                    <?php
                foreach ($Fornecedores as $f) {
                    if ($f->id == $Fornecedor_servico_produto->fornecedor_id)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
                                </select>
                            </div>
                            <?php }?>
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label class="required" for="produto_servico"><span class="glyphicon glyphicon-asterisk"></span>
                                    Produtos/Serviços</label>
                                <textarea name="produto_servico" id="produto_servico" class="form-control" required><?php echo $Fornecedor_servico_produto->produto_servico ?></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                        <?php if($this->getParam('modal')){ ?>
                        <div class="text-right">
                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')"> Cancelar
                            </a>
                            <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="salvar">
                        </div>
                        <?php }else{?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Fornecedor_servico_produto', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                        <?php }?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>