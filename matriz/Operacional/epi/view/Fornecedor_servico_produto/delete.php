<form class="form" method="post" action="<?php echo $this->Html->getUrl('Fornecedor_servico_produto', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o Produtos/Serviços: <strong><?php echo $Fornecedor_servico_produto->produto_servico; ?></strong>?</p>
    </div>
    <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
    <?php if($this->getParam('modal')){?>
        <input type="hidden" name="modal" value="1"/>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Fornecedor_servico_produto->id; ?>">
        <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')"> Cancelar </a>
        <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="Excluir">
    </div>
    <?php }else{?>
        <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Fornecedor_servico_produto->id; ?>">
            <a href="<?php echo $this->Html->getUrl('Fornecedor_servico_produto', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-danger" value="Excluir">
        </div>
    <?php } ?> 
</form>