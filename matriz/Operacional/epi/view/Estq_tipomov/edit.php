<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo de Movimento</h2>
        <ol class="breadcrumb">
            <li>Tipo de Movimento</li>
            <li class="active">
                <strong>Editar Tipo de Movimento</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_tipomov', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="nome">Nome <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Estq_tipomov->nome ?>" placeholder="Nome" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="operacao">Operação</label>
                                <select name="operacao" class="form-control" id="operacao">
                                    <option value="">Selecione :</option>
                                    <option value="-"> -</option>
                                    <option value="+"> +</option>
                                </select>
                            </div>

                            <input type="hidden" name="situacao" value="1"/>

                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Estq_tipomov->id; ?>">

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_tipomov', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#operacao option[value="<?php echo $Estq_tipomov->operacao ?>"]').prop('selected', true);
    });
</script>