<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;    </span></button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <legend>Entradas/Devoluções</legend>
                    <p>
                        <strong>Tipo Movimento</strong>:
                        <?php
                        echo $this->Html->getLink($Estq_mov->getEstq_tipomov()->nome, 'Estq_tipomov', 'view',
                            array('id' => $Estq_mov->getEstq_tipomov()->id), // variaveis via GET opcionais
                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                        ?>
                    </p>

                    <p><strong>Data da Movimentação</strong>: <?php echo DataTimeBr($Estq_mov->data); ?></p>
                    <?php
                    if ($Estq_mov->requer != NULL) {
                        if ($Estq_mov->requer == "Fornecedor") {
                                echo '<p><strong>Fornecedor</strong>: '. $Estq_mov->getFornecedors()->nome;' </p>';

                        } elseif ($Estq_mov->requer == "Colaborador") {
                            echo '<p><strong>Colaborador</strong>: '. $Estq_mov->getRhprofissional()->nome;'</p>';


                        } elseif ($Estq_mov->requer == "Cliente") {
                            echo '<p><strong>Cliente</strong>: '. $Estq_mov->cliente; '</p>';

                        }
                    }
                    ?>



                    <p><strong>Tipo Documento</strong>: <?php echo $Estq_mov->tipodoc; ?></p>

                    <p><strong>Num Documento</strong>: <?php echo $Estq_mov->numdoc; ?></p>

                    <p><strong>Data do Documento</strong>: <?php echo DataBR($Estq_mov->datadoc); ?></p>

                    <p>
                        <strong>Situação</strong>:
                        <?php
                        echo $this->Html->getLink($Estq_mov->getEstq_situacao()->nome, 'Estq_situacao', 'view',
                            array('id' => $Estq_mov->getEstq_situacao()->id), // variaveis via GET opcionais
                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                        ?>
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>