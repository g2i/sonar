<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Entradas/Devoluções</h2>
        <ol class="breadcrumb">
            <li>Entradas/Devoluções</li>
            <li class="active">
                <strong>Editar </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_mov', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipo">Tipo</label>
                                <select name="tipo" class="form-control" id="tipo">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Estq_tipomovs as $e) {
                                        if ($e->id == $Estq_mov->tipo)
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipodoc">Tipo Documento</label>
                                <input type="text" name="tipodoc" id="tipodoc" class="form-control"
                                       value="<?php echo $Estq_mov->tipodoc ?>" placeholder="Tipo Documento">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="datadoc">Data do Documento</label>

                                <div class='input-group datePicker'>
                                    <input type='text' name="datadoc" id="datadoc" class="form-control date"
                                           value="<?php echo DataBR($Estq_mov->datadoc); ?>"/>
                                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data">Data da Movimentação</label>

                                <div class='input-group datetimepicker '>
                                    <input type='text' name="data" id="data" class="form-control datetime"
                                           value="<?php echo DataTimeBr($Estq_mov->data); ?>"/>
                                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="numdoc">Número Documento</label>
                                <input type="text" name="numdoc" id="numdoc" class="form-control"
                                       value="<?php echo $Estq_mov->numdoc ?>" placeholder="Número Documento">
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="padrao">Estoque Padrão?<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="padrao" id="padrao" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="1" <?= $Estq_mov->padrao == 1 ? 'selected' : '' ?>>Sim</option>
                                    <option value="2" <?= $Estq_mov->padrao == 2 ? 'selected' : '' ?>>Não</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12 sub_padrao"
                                 style="display: <?= $Estq_mov->padrao == 1 ? 'block' : 'none' ?> ">
                                <label class="required" for="sub_padrao">Lote-subestoque<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="sub_padrao" id="sub_padrao" class="form-control">
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($sub_estoque as $e) {
                                        if ($e->id == $Estq_mov->sub_padrao)
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div
                                    class="form-group col-md-4 col-sm-6 col-xs-12 colaborador" <?php echo $teste = $Estq_mov->requer == 'Colaborador' ?: 'style="display: none"'; ?> >
                                <label for="colaborador">Colaborador</label>
                                <select name="colaborador" class="form-control" id="colaborador">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Rhprofissionals as $e) {
                                        if ($e->id == $Estq_mov->colaborador)
                                            echo '<option selected value="' . $e->codigo . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->codigo . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div
                                    class="form-group col-md-4 col-sm-6 col-xs-12 fornecedor"<?php echo $teste2 = $Estq_mov->requer == 'Fornecedor' ?: 'style="display: none"'; ?> >
                                <label for="fornecedor">Fornecedor</label>
                                <select name="fornecedor" class="form-control" id="fornecedor">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Fornecedors as $e) {
                                        if ($e->id == $Estq_mov->fornecedor) {
                                            echo '<option value="' . $e->id . '" selected>' . $e->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }

                                    }
                                    ?>
                                </select>
                            </div>
                            <div
                                    class="form-group col-md-4 col-sm-6 col-xs-12 cliente" <?php echo $teste3 = $Estq_mov->requer == 'Cliente' ?: 'style="display: none"'; ?>>
                                <label for="cliente">Cliente</label>
                                <select name="cliente" class="form-control" id="cliente">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Clientes as $e) {
                                        if ($e->id == $Estq_mov->cliente) {
                                            echo '<option value="' . $e->id . '" selected>' . $e->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <p><label class="required" for="requer">Selecione :<span
                                                class="glyphicon glyphicon-asterisk"></span></label></p>

                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer"
                                           value="Fornecedor" <?php echo ($Estq_mov->requer == 'Fornecedor') ? "checked" : '' ?>>
                                    Fornecedor
                                </label>


                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer"
                                           value="Cliente" <?php echo ($Estq_mov->requer == 'Cliente') ? "checked" : '' ?>>
                                    Cliente
                                </label>


                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer"
                                           value="Colaborador" <?php echo ($Estq_mov->requer == 'Colaborador') ? "checked" : '' ?>>
                                    Colaborador
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer"
                                           value="4" <?php echo ($Estq_mov->requer == '4') ? "checked" : '' ?>>
                                    Outros
                                </label>
                            </div>

                            <div class="text-right">
                                <div class="text-right">
                                    <a href="Javascript:void(0)">
                                        <span class="img-processar processar" data-placement="top" data-toggle="tooltip"
                                              title="Processar"></span>
                                    </a>
                                </div>
                            </div>


                            <input type="hidden" name="situacao" value="1"/>

                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Estq_mov->id; ?>">

                        <div class="text-right">
                            <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-align-justify" data-toggle="tooltip" data-placement="auto" title="Detalhes de Movimento"></span>  Artigos', 'Estq_movdetalhes', 'all',
                                array('id' => $Estq_mov->id, 'first' => 1, 'modal' => 1),
                                array('class' => 'btn btn-info', 'data-toggle' => 'modal', 'data-target' => '.bs-lg')); ?>
                        </div>
                        <br>

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <?php if ($Estq_mov->status == 1 || empty($Estq_mov->status)) { ?>
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                        onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar
                                </button>
                                <input type="submit" id="validForm" style="display: none;"/>
                            <?php } ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#fornecedor option[value="<?php echo $Estq_mov->fornecedor ?>"]').prop('selected', true);
        $('#colaborador option[value="<?php echo $Estq_mov->colaborador ?>"]').prop('selected', true);
        $('[data-toggle="tooltip"]').tooltip();
        $("input[name='requer']").click(function () {
            if ($(this).val() == 'Cliente') {
                $(".cliente").show(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Colaborador') {
                $(".cliente").hide(500);
                $(".colaborador").show(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Fornecedor') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").show(500);
            }
            else if ($(this).val() == '4') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }
            $("input[name='requer']").click(function () {

            });

        });
        $('.processar').click(function () {
            DialogConfirma('Atenção !', 'Está operação não poderá ser revertida !', '<?php echo $Estq_mov->id ?>');
        });
    });
</script>
