<div class="wrapper wrapper-content  ">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <div class="ibox">
                <div class="ibox-content">
<span aria-hidden="true">&times;</span></button>
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Estq_mov', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Estq_mov->tipodoc; ?></strong>?</p>
    </div>

    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Estq_mov->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <?php if($Estq_mov->status==1 || empty($Estq_mov->status)){ ?>
        <input type="submit" class="btn btn-danger" value="Excluir">
        <?php } ?>
    </div>
</form>
                </div>
            </div>
        </div>
    </div>
</div>