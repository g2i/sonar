<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Entradas/Devoluções</h2>
        <ol class="breadcrumb">
            <li>Entradas/Devoluções</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_mov', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="tipo">Tipo de Movimento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="tipo" class="form-control tipo" id="tipo">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Estq_tipomovs as $e) {
                                        if ($e->id == $Estq_mov->tipo)
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="tipodoc">Tipo de Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="tipodoc" id="tipodoc" class="form-control"
                                       value="<?php echo $Estq_mov->tipodoc ?>" placeholder="Tipo Documento" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="datadoc">Data do Documento</label>

                                <div class='input-group datePicker'>
                                    <input type='text' name="datadoc" id="datadoc" class="form-control date"
                                           value="<?php echo $Estq_mov->datadoc ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data">Data da Movimentação</label>

                                <div class='input-group datetimepicker '>
                                    <input type='text' name="data" id="data" class="form-control datetime"
                                           value="<?php echo $Estq_mov->data ?>"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="numdoc">Número do Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="numdoc" id="numdoc" class="form-control"
                                       value="<?php echo $Estq_mov->numdoc ?>" placeholder="Número Documento" required>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="padrao">Estoque Padrão?<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="padrao" id="padrao" class="form-control" required>
                                    <option value="">Selecione</option>
                                    <option value="1">Sim</option>
                                    <option value="2">Não</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12 sub_padrao" style="display: none">
                                <label class="required" for="sub_padrao">Lote-subestoque<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="sub_padrao" id="sub_padrao" class="form-control">
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($sub_estoque as $e) {
                                        if ($e->id == $Estq_mov->sub_padrao)
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12 colaborador" style="display: none">
                                <label for="colaborador">Colaborador</label>
                                <select name="colaborador" class="form-control" id="colaborador">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Rhprofissionals as $e) {
                                        if ($e->id == $Estq_mov->colaborador)
                                            echo '<option selected value="' . $e->codigo . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->codigo . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 fornecedor" style="display: none">
                                <label for="fornecedor">Fornecedor</label>
                                <select name="fornecedor" class="form-control" id="fornecedor">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Fornecedors as $e) {
                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 cliente" style="display: none">
                                <label for="cliente">Cliente</label>
                                <select name="cliente" class="form-control" id="cliente">
                                    <option value="">Selecione :</option>
                                    <?php
                                    foreach ($Clientes as $e) {
                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <p><label class="required" for="requer">Selecione :<span
                                            class="glyphicon glyphicon-asterisk"></span></label></p>

                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer" value="Fornecedor"
                                           required>
                                    Fornecedor
                                </label>


                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer" value="Cliente">
                                    Cliente
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer" value="Colaborador">
                                    Colaborador
                                </label>

                                <label class="radio-inline">
                                    <input type="radio" name="requer" id="requer" role="requer" value="4">
                                    Outros
                                </label>

                            </div>

                            <input type="hidden" name="situacao" value="1"/>

                            <div class="clearfix"></div>

                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default"
                                   data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $("input[name='requer']").click(function () {
            if ($(this).val() == 'Cliente') {
                $(".cliente").show(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Colaborador') {
                $(".cliente").hide(500);
                $(".colaborador").show(500);
                $(".fornecedor").hide(500);
            }
            else if ($(this).val() == 'Fornecedor') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").show(500);
            }
            else if ($(this).val() == '4') {
                $(".cliente").hide(500);
                $(".colaborador").hide(500);
                $(".fornecedor").hide(500);
            }

        })
    });

    $("#padrao").change(function () {
        if($(this).val()==1){
            $(".sub_padrao").show(500);
            $("#sub_padrao").attr("required","required")
        }else{
            $(".sub_padrao").hide(500);
            $("#sub_padrao").removeAttr("required")
        }
    })

</script>




