<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Subgrupo</h2>
        <ol class="breadcrumb">
            <li>Subgrupo</li>
            <li class="active">
                <strong>Edição de Subgrupo</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_subgrupo', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="nome">Nome <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Estq_subgrupo->nome ?>" placeholder="Nome" required>
                            </div>

                            <input type="hidden" name="situacao" value="1"/>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="grupo">Grupo</label>
                                <select name="grupo" class="form-control" id="grupo">
                                    <option value="">Selecione:</option>
                                    <?php
                                    foreach ($Estq_grupos as $e) {
                                        if ($e->id == $Estq_subgrupo->grupo)
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Estq_subgrupo->id; ?>">

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_subgrupo', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#grupo option[value="<?php echo $Estq_subgrupo->grupo ?>"]').prop('selected', true);
    });
</script>