<div class="panel panel-default">
    <div class="panel-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;    </span></button>
        <h2><?php echo $Estq_subgrupo->nome;?></h2>
    </div>
    <div class="panel-body">
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_subgrupo->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_subgrupo->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Grupo</strong>:
    <?php
    echo $this->Html->getLink($Estq_subgrupo->getEstq_grupo()->nome, 'Estq_grupo', 'view',
    array('id' => $Estq_subgrupo->getEstq_grupo()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
    </div>
</div>