<?php
final class Estq_tipomovController extends AppController{ 

    # página inicial do módulo Estq_tipomov
    function index(){
        $this->setTitle('Visualização de Tipo Movimento');
    }

    # lista de Estq_tipomovs
    # renderiza a visão /view/Estq_tipomov/all.php
    function all(){
        $this->setTitle('Listagem de Tipo Movimento');
        $p = new Paginate('Estq_tipomov', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('origem','=',Config::get('origem')); //filtro Origem
        $this->set('Estq_tipomovs', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Estq_tipomov
    # renderiza a visão /view/Estq_tipomov/view.php
    function view(){
        $this->setTitle('Visualização de Tipo Movimento');
        try {
            $this->set('Estq_tipomov', new Estq_tipomov((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_tipomov', 'all');
        }
    }

    # formulário de cadastro de Estq_tipomov
    # renderiza a visão /view/Estq_tipomov/add.php
    function add(){
        $this->setTitle('Cadastro de Tipo Movimento');
        $this->set('Estq_tipomov', new Estq_tipomov);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_tipomov
    # (true)redireciona ou (false) renderiza a visão /view/Estq_tipomov/add.php
    function post_add(){
        $this->setTitle('Cadastro de Tipo Movimento');
        $Estq_tipomov = new Estq_tipomov();
        $this->set('Estq_tipomov', $Estq_tipomov);
        $_POST['origem']=Config::get('origem');//salvando a Origem
        $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_tipomov->usuario_id=$user; // para salvar o usuario que está fazendo

        try {
            $Estq_tipomov->save($_POST);
            new Msg(__('Tipo de Movimento cadastrado com sucesso'));
            $this->go('Estq_tipomov', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição de Estq_tipomov
    # renderiza a visão /view/Estq_tipomov/edit.php
    function edit(){
        $this->setTitle('Edição de Tipo Movimento');
        try {
            $this->set('Estq_tipomov', new Estq_tipomov((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_tipomov', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_tipomov
    # (true)redireciona ou (false) renderiza a visão /view/Estq_tipomov/edit.php
    function post_edit(){
        $this->setTitle('Edição de Tipo Movimento');
        try {
            $Estq_tipomov = new Estq_tipomov((int) $_POST['id']);
            $this->set('Estq_tipomov', $Estq_tipomov);
            $Estq_tipomov->usuario_dt = date('Y-m-d H:i:s');
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_tipomov->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_tipomov->save($_POST);
            new Msg(__('Tipo de Movimento atualizado com sucesso'));
            $this->go('Estq_tipomov', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_tipomov
    # renderiza a /view/Estq_tipomov/delete.php
    function delete(){
        $this->setTitle('Apagar Tipo de Movimento');
        try {
            $this->set('Estq_tipomov', new Estq_tipomov((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_tipomov', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_tipomov
    # redireciona para Estq_tipomov/all
    function post_delete(){
        try {
            $Estq_tipomov = new Estq_tipomov((int) $_POST['id']);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_tipomov->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_tipomov->usuario_dt = date('Y-m-d H:i:s');
            $Estq_tipomov->situacao=3;
            $Estq_tipomov->save();
            new Msg(__('Tipo de Movimento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_tipomov', 'all');
    }

}