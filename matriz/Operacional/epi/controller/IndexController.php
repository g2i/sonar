<?php

class IndexController extends AppController
{
    public function index()
    {
        $this->setTitle('Início');

        if (@Session::get("user_epi")->gestor_estoque == 2) {
            $cautelas_vencidas = $this->query_count("SELECT count(M.id) as total from estq_mov M
                LEFT JOIN estq_movdetalhes MD
                ON MD.movimento = M.id
                LEFT JOIN `estq_lote_substoque` LS
                ON LS.id = MD.`lote_substoque`
                INNER JOIN estq_tipomov T
                ON T.id = M.tipo
                WHERE M.situacao<> 3 AND M.origem = 2
                AND MD.validade < curdate() AND T.cautela = 1 AND LS.`subestoque` = " . Session::get('subestoque'));
        } else {
            $cautelas_vencidas = $this->query_count("SELECT count(M.id) as total from estq_mov M
                LEFT JOIN estq_movdetalhes MD
                ON MD.movimento = M.id
                LEFT JOIN `estq_lote_substoque` LS
                ON LS.id = MD.`lote_substoque`
                INNER JOIN estq_tipomov T
                ON T.id = M.tipo
                WHERE M.situacao<> 3 AND M.origem = 2
                AND MD.validade < curdate() AND T.cautela = 1");
        }
        $this->set('cautelas_vencidas', $cautelas_vencidas);

        if (@Session::get("user_epi")->gestor_estoque == 2) {

            $cautelas_avencer = $this->query_count("SELECT count(M.id) as total from  estq_mov M
            LEFT JOIN estq_movdetalhes MD
            ON MD.movimento = M.id
            LEFT JOIN estq_lote_substoque LS 
            ON LS.`id` = MD.`lote_substoque` 
            WHERE M.situacao<> 3 AND M.origem = 2 AND (MD.validade >= curdate() AND MD.validade    <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)) AND LS.`subestoque` = " . Session::get('subestoque'));

        } else {
            $cautelas_avencer = $this->query_count("SELECT count(M.id) as total from  estq_mov M
    LEFT JOIN estq_movdetalhes MD
    ON MD.movimento = M.id
    LEFT JOIN estq_lote_substoque LS 
    ON LS.`id` = MD.`lote_substoque` 
    WHERE M.situacao<> 3 AND M.origem = 2 AND (MD.validade >= curdate() AND MD.validade    <= DATE_ADD(CURDATE(), INTERVAL 30 DAY))");
        }
        $this->set('cautelas_avencer', $cautelas_avencer);

        $artigos = $this->query_count("SELECT count(A.id) as total FROM estq_artigo A WHERE A.origem =2 AND A.situacao <> 3");

        $this->set('artigos', $artigos);

        if (@Session::get("user_epi")->gestor_estoque == 2) {

            $movimento = $this->query("SELECT M.id,P.`nome` as colaborador,T.`nome` as tipo,A.nome as artigo, D.`validade` FROM estq_mov M
            LEFT JOIN estq_movdetalhes D
            ON D.movimento = M.id
            LEFT JOIN estq_lote_substoque LS 
            ON LS.`id` = D.`lote_substoque`
            LEFT JOIN rhprofissional P
            ON P.`codigo` = M.`colaborador`
            LEFT JOIN estq_tipomov T
            ON T.id = M.`tipo`
            LEFT JOIN estq_artigo A
            ON A.`id` = D.`artigo`
            WHERE M.origem=2 AND M.`situacao` <> 3 AND T.cautela=1
                       AND D.validade <=  DATE_ADD(CURDATE(), INTERVAL 30 DAY)
                       AND LS.`subestoque` = " . Session::get('subestoque'));
        } else {
            $movimento = $this->query("SELECT M.id,P.`nome` as colaborador,T.`nome` as tipo,A.nome as artigo, D.`validade` FROM estq_mov M
            LEFT JOIN estq_movdetalhes D
            ON D.movimento = M.id
            LEFT JOIN estq_lote_substoque LS 
            ON LS.`id` = D.`lote_substoque`
            LEFT JOIN rhprofissional P
            ON P.`codigo` = M.`colaborador`
            LEFT JOIN estq_tipomov T
            ON T.id = M.`tipo`
            LEFT JOIN estq_artigo A
            ON A.`id` = D.`artigo`
            WHERE M.origem=2 AND M.`situacao` <> 3 AND T.cautela=1
                       AND D.validade <=  DATE_ADD(CURDATE(), INTERVAL 30 DAY)");
        }

        $this->set('movimento', $movimento);

        if (@Session::get("user_epi")->gestor_estoque == 2) {

            $estq_minimoSubstoque = $this->query_count('SELECT COUNT(DISTINCT(S.artigo)) AS total
                                            FROM estq_lote_substoque S
                                            WHERE S.estoque_minimo > S.quantidade AND situacao <> 3 and S.origem = ' . Config::get('origem') . ' AND S.subestoque = ' . Session::get('subestoque'));
            $estq_minimoArtigo = $this->query_count('SELECT COUNT(S.id) AS total FROM estq_artigo S
                                                                            WHERE S.est_minimo > S.quantidade AND situacao <> 3
                                                                            and S.origem = ' . Config::get('origem'));
        } else {
            $estq_minimoSubstoque = $this->query_count('SELECT COUNT(DISTINCT(S.artigo)) AS total
                                            FROM estq_lote_substoque S
                                            WHERE S.estoque_minimo > S.quantidade AND situacao <> 3 and S.origem = ' . Config::get('origem'));
            $estq_minimoArtigo = $this->query_count('SELECT COUNT(S.id) AS total FROM estq_artigo S
                                             WHERE S.est_minimo > S.quantidade AND situacao <> 3
                                             and S.origem = ' . Config::get('origem'));
        }
        $this->set('a_comprar', $estq_minimoSubstoque);
      // $this->set('a_comprar', $estq_minimoArtigo);
      $artigoVencido = $this->query_count('SELECT COUNT(al.id) AS total FROM `estq_artigo_lote` al
      LEFT JOIN `estq_artigo` artigo ON artigo.id = al.artigo
      WHERE al.situacao = 1
      AND artigo.situacao = 1
      AND (al.validade >= CURDATE() 
      AND al.validade <= DATE_ADD(CURDATE(), INTERVAL 30 DAY))');
        $this->set('artigoVencido', $artigoVencido);
  

    }

    function rh()
    {
        $this->setTitle('Início');
        $cautelas_vencidas = $this->query_count("SELECT count(M.id) as total from estq_mov M
                LEFT JOIN estq_movdetalhes MD
                ON MD.movimento = M.id
                INNER JOIN estq_tipomov T
                ON T.id = M.tipo
                WHERE M.situacao<> 3 AND M.origem = 2
                AND MD.validade < curdate() AND T.cautela = 1 ");
        $this->set('cautelas_vencidas', $cautelas_vencidas);

        $cautelas_avencer = $this->query_count("SELECT count(M.id) as total from  estq_mov M
    LEFT JOIN estq_movdetalhes MD
    ON MD.movimento = M.id
    WHERE M.situacao<> 3 AND M.origem = 2 AND (MD.validade >= curdate() AND MD.validade    <= DATE_ADD(CURDATE(), INTERVAL 30 DAY))");
        $this->set('cautelas_avencer', $cautelas_avencer);


        $artigos =
            $this->query_count("SELECT count(A.id) as total FROM estq_artigo A WHERE A.origem =2 AND A.situacao <> 3");

        $this->set('artigos', $artigos);


        $estq_minimo =
            $this->query_count('SELECT COUNT(DISTINCT(S.artigo)) AS total
                        FROM estq_lote_substoque S
                        WHERE S.estoque_minimo > S.quantidade AND situacao <> 3 and S.origem = ' . Config::get('origem') . '');

        $this->set('a_comprar', $estq_minimo);

        $ocorrencias = new Rhocorrencia();
        $this->set('ocorrencias', $ocorrencias->getTabela_ocorrencia());

         //getRhocorrencia
         $ocor = new Rhocorrencia();
         $this->set('ocor',$ocor->getRhocorrencia());
    }
}