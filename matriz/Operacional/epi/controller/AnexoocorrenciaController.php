<?php
final class AnexoocorrenciaController extends AppController{ 

    # página inicial do módulo Anexoocorrencia
    function index(){
        $this->setTitle('Visualização de Anexo');
    }

    # lista de Anexoocorrencias
    # renderiza a visão /view/Anexoocorrencia/all.php
    function all(){
        $this->setTitle('Listagem de Anexos');
        $p = new Paginate('Anexoocorrencia', 10);
        $c = new Criteria();
        if($this->getParam('id')){
            $c->addCondition('codigo_ocorrencia','=',$this->getParam('id'));
            $this->set('Ocorrencia', new Rhocorrencia($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        }

            $c->setOrder('nome');

        $this->set('Anexoocorrencias', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Anexoocorrencia
    # renderiza a visão /view/Anexoocorrencia/view.php
    function view(){
        $this->setTitle('Visualização de Anexo');
        try {
            $this->set('Anexoocorrencia', new Anexoocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexoocorrencia', 'all');
        }
    }

    # formulário de cadastro de Anexoocorrencia
    # renderiza a visão /view/Anexoocorrencia/add.php
    function add(){
        $this->setTitle('Cadastro de Anexos');
        $this->set('Anexoocorrencia', new Anexoocorrencia);
        $this->set('Rhocorrencias',  Rhocorrencia::getList());
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # recebe os dados enviados via post do cadastro de Anexoocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Anexoocorrencia/add.php
    function post_add(){
        $this->setTitle('Cadastro de Anexos');
        $Anexoocorrencia = new Anexoocorrencia();
        if(!empty($_FILES['anexo']['name'])){
            $file = $_FILES['anexo'];
            $extencoes = array('jpg', 'pdf', 'gif', 'mp3', 'mp4', 'odf', 'docx', 'doc', 'txt', 'ppd', 'ppx', 'xlsx', 'pptx', 'xls', 'png', 'zip', 'rar');
            $anexo = new FileUploader($file,NULL,$extencoes);
            $path = "/anexosocorr/" .  $_POST['codigo_ocorrencia'];
            $nome = uniqid(). $anexo->TratarName($file['name']);
            $anexo->save($nome,$path);
            $Anexoocorrencia->anexo = SITE_PATH. '/uploads'.$path."/".$nome;
        }
        $user=Session::get('user_epi');
        $Anexoocorrencia->cadastradopor=$user->id;
        $Anexoocorrencia->dtCadastro=date('Y-m-d H:i:s');
        $Anexoocorrencia->status=1;

        $Anexoocorrencia->complemento_cad='Em: '.date('Y-m-d H:i:s').' - Atualizado no epi, por: '.$user->nome.'\n\n';
        $this->set('Anexoocorrencia', $Anexoocorrencia);
        try {
            if(!empty($_FILES['anexo']['name'])) {
                $Anexoocorrencia->save($_POST);
                new Msg(__('Anexo -Ocorrência cadastrado com sucesso'));
                if (!empty($_POST['modal'])) {
                    echo 1;
                    exit;
                }
                $this->go('Anexoocorrencia', 'all');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhocorrencias',  Rhocorrencia::getList());
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # Confirma a exclusão ou não de um(a) Anexoocorrencia
    # renderiza a /view/Anexoocorrencia/delete.php
    function delete(){
        $this->setTitle('Apagar Anexoocorrencia');
        try {
            $this->set('Anexoocorrencia', new Anexoocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexoocorrencia', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Anexoocorrencia
    # redireciona para Anexoocorrencia/all
    function post_delete(){
        try {
            $Anexoocorrencia = new Anexoocorrencia((int) $_POST['id']);
            $user=Session::get('user_epi');
            $Anexoocorrencia->atualizadopor=$user->id;
            $Anexoocorrencia->dtAtualizacao=date('Y-m-d H:i:s');
            $Anexoocorrencia->status=3;
            $Anexoocorrencia->complemento_del='Em: '.date('Y-m-d H:i:s').' - Atualizado no epi, por: '.$user->nome.'\n\n';
            $Anexoocorrencia->save();
            new Msg(__('Anexo apagado com sucesso'), 1);
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Anexoocorrencia', 'all');
    }

}