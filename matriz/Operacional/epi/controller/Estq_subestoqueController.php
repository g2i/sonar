<?php

final class Estq_subestoqueController extends AppController
{

    # página inicial do módulo Estq_subestoque
    function index()
    {
        $this->setTitle('Visualização de Sub-Estoque');
    }

    # lista de Estq_subestoques
    # renderiza a visão /view/Estq_subestoque/all.php
    function all()
    {
        $this->setTitle('Listagem de Subestoque');
        $p = new Paginate('Estq_subestoque', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $this->set('Estq_subestoques', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tiposubestoques', Estq_tiposubestoque::getList());


    }

    # visualiza um(a) Estq_subestoque
    # renderiza a visão /view/Estq_subestoque/view.php
    function view()
    {
        $this->setTitle('Visualização de Subestoque');
        try {
            $this->set('Estq_subestoque', new Estq_subestoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_subestoque', 'all');
        }
    }

    # formulário de cadastro de Estq_subestoque
    # renderiza a visão /view/Estq_subestoque/add.php
    function add()
    {
        $this->setTitle('Cadastro de Subestoque');
        $this->set('Estq_subestoque', new Estq_subestoque);
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $c = new Criteria();
        $c->addCondition('origem', '=', Config::get('origem')); //filtro
        $this->set('Estq_tiposubestoques', Estq_tiposubestoque::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Estq_subestoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_subestoque/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Sub-Estoque');
        $Estq_subestoque = new Estq_subestoque();
        $this->set('Estq_subestoque', $Estq_subestoque);
        $_POST['origem'] = Config::get('origem');//salvando a origem
        $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_subestoque->usuario_id = $user; // para salvar o usuario que está fazendo
        try {
            $Estq_subestoque->save($_POST);
            $d = new Criteria();
            $d->addCondition('situacao', '<>', 3);
            $Estq_artigo_lotes = Estq_artigo_lote::getList($d);

                foreach ($Estq_artigo_lotes as $a) {

                    $Estq_lote_substoques = new Estq_lote_substoque();
                    $Estq_lote_substoques->lote = $a->id;
                    $Estq_lote_substoques->artigo = $a->artigo;
                    $Estq_lote_substoques->quantidade = 0;
                    $Estq_lote_substoques->subestoque = $Estq_subestoque->id;
                    $Estq_lote_substoques->estoque_minimo = 0;
                    $Estq_lote_substoques->usuario_id = $user;
                    $Estq_lote_substoques->situacao = 1;
                    $Estq_lote_substoques->origem = Config::get('origem');//salvando a origem
                    $Estq_lote_substoques->save();

                }


            new Msg(__('Subestoque cadastrado com sucesso'));
            $this->go('Estq_subestoque', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tiposubestoques', Estq_tiposubestoque::getList());
    }

    # formulário de edição de Estq_subestoque
    # renderiza a visão /view/Estq_subestoque/edit.php
    function edit()
    {
        $this->setTitle('Edição de Subestoque');
        try {
            $this->set('Estq_subestoque', new Estq_subestoque((int)$this->getParam('id')));
            $this->set('Estq_situacaos', Estq_situacao::getList());
            $c = new Criteria();
            $c->addCondition('origem', '=', Config::get('origem')); //filtro
            $this->set('Estq_tiposubestoques', Estq_tiposubestoque::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_subestoque', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_subestoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_subestoque/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Sub-Estoque');
        try {
            $Estq_subestoque = new Estq_subestoque((int)$_POST['id']);
            $this->set('Estq_subestoque', $Estq_subestoque);
            $Estq_subestoque->usuario_dt = date('Y-m-d H:i:s');
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_subestoque->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_subestoque->save($_POST);
            new Msg(__('Subestoque atualizado com sucesso'));
            $this->go('Estq_subestoque', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tiposubestoques', Estq_tiposubestoque::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_subestoque
    # renderiza a /view/Estq_subestoque/delete.php
    function delete()
    {
        $this->setTitle('Apagar Subestoque');
        try {
            $this->set('Estq_subestoque', new Estq_subestoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_subestoque', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_subestoque
    # redireciona para Estq_subestoque/all
    function post_delete()
    {
        try {
            $Estq_subestoque = new Estq_subestoque((int)$_POST['id']);
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_subestoque->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_subestoque->usuario_dt = date('Y-m-d H:i:s');
            $Estq_subestoque->situacao = 3;
            $Estq_subestoque->save();
            new Msg(__('Subestoque apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_subestoque', 'all');
    }

}