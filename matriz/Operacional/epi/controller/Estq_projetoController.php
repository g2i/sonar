<?php
final class Estq_projetoController extends AppController{ 

    # página inicial do módulo Estq_projeto
    function index(){
        $this->setTitle('Visualização de Estq_projeto');
    }

    # lista de Estq_projetos
    # renderiza a visão /view/Estq_projeto/all.php
    function all(){
        $this->setTitle('Listagem de Estq_projeto');
        $p = new Paginate('Estq_projeto', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=','1');
        $this->set('Estq_projetos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Estq_projeto
    # renderiza a visão /view/Estq_projeto/view.php
    function view(){
        $this->setTitle('Visualização de Estq_projeto');
        try {
            $this->set('Estq_projeto', new Estq_projeto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_projeto', 'all');
        }
    }

    # formulário de cadastro de Estq_projeto
    # renderiza a visão /view/Estq_projeto/add.php
    function add(){
        $this->setTitle('Cadastro de Estq_projeto');
        $this->set('Estq_projeto', new Estq_projeto);
    }

    # recebe os dados enviados via post do cadastro de Estq_projeto
    # (true)redireciona ou (false) renderiza a visão /view/Estq_projeto/add.php
    function post_add(){
        $this->setTitle('Cadastro de Estq_projeto');
        $Estq_projeto = new Estq_projeto();
        $this->set('Estq_projeto', $Estq_projeto);
        try {
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_projeto->cadastradopor = $user; // para salvar o usuario que está fazendo
            $Estq_projeto->dt_cadastro = date('Y-m-d H:i:s');
            $Estq_projeto->situacao_id = 1;
            $Estq_projeto->save($_POST);
            new Msg(__('Projeto cadastrado com sucesso'));
            $this->go('Estq_projeto', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Estq_projeto
    # renderiza a visão /view/Estq_projeto/edit.php
    function edit(){
        $this->setTitle('Edição de Estq_projeto');
        try {
            $this->set('Estq_projeto', new Estq_projeto((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_projeto', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_projeto
    # (true)redireciona ou (false) renderiza a visão /view/Estq_projeto/edit.php
    function post_edit(){
        $this->setTitle('Edição de Estq_projeto');
        try {
            $Estq_projeto = new Estq_projeto((int) $_POST['id']);
            $this->set('Estq_projeto', $Estq_projeto);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_projeto->modificadopor = $user; // para salvar o usuario que está fazendo
            $Estq_projeto->dt_atualizacao = date('Y-m-d H:i:s');
            $Estq_projeto->save($_POST);
            new Msg(__('Projeto atualizado com sucesso'));
            $this->go('Estq_projeto', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Estq_projeto
    # renderiza a /view/Estq_projeto/delete.php
    function delete(){
        $this->setTitle('Apagar Estq_projeto');
        try {
            $this->set('Estq_projeto', new Estq_projeto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_projeto', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_projeto
    # redireciona para Estq_projeto/all
    function post_delete(){
        try {
            $Estq_projeto = new Estq_projeto((int) $_POST['id']);
            $Estq_projeto->situacao_id = 3;
            $Estq_projeto->save();
            new Msg(__('Estq_projeto apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_projeto', 'all');
    }

}