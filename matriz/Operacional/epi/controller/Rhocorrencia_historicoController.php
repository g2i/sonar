<?php
final class Rhocorrencia_historicoController extends AppController{ 

    # página inicial do módulo Rhocorrencia_historico
    function index(){
        $this->setTitle('Ocorrência - Histórico');
    }

    # lista de Rhocorrencia_historicos
    # renderiza a visão /view/Rhocorrencia_historico/all.php
    function all(){
        $this->setTitle('Ocorrência - Histórico');
        $p = new Paginate('Rhocorrencia_historico', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhocorrencia_historico', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhdivisaos',  Rhdivisao::getList());


    }

    # visualiza um(a) Rhocorrencia_historico
    # renderiza a visão /view/Rhocorrencia_historico/view.php
    function view(){
        $this->setTitle('Visualização de Ocorrência - Histórico');
        try {
            $this->set('Rhocorrencia_historico', new Rhocorrencia_historico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhocorrencia_historico', 'all');
        }
    }

    # formulário de cadastro de Rhocorrencia_historico
    # renderiza a visão /view/Rhocorrencia_historico/add.php
    function add(){
        $this->setTitle('Cadastro de Ocorrência - Histórico');
        $this->set('Rhocorrencia_historico', new Rhocorrencia_historico);
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhgrupo_histocorres',  Rhgrupo_histocorr::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhocorrencia_historico
    # (true)redireciona ou (false) renderiza a visão /view/Rhocorrencia_historico/add.php
    function post_add(){
        $this->setTitle('Cadastro de Ocorrência - Histórico');
        $Rhocorrencia_historico = new Rhocorrencia_historico();
        $this->set('Rhocorrencia_historico', $Rhocorrencia_historico);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhocorrencia_historico->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhocorrencia_historico->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhocorrencia_historico->save($_POST);
            new Msg(__('Ocorrência - Histórico cadastrada com sucesso'));
            $this->go('Rhocorrencia_historico', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhgrupo_histocorres',  Rhgrupo_histocorr::getList());
    }

    # formulário de edição de Rhocorrencia_historico
    # renderiza a visão /view/Rhocorrencia_historico/edit.php
    function edit(){
        $this->setTitle('Edição de Ocorrência - Histórico');
        try {
            $this->set('Rhocorrencia_historico', new Rhocorrencia_historico((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
            $this->set('Rhgrupo_histocorres',  Rhgrupo_histocorr::getList());
            $this->set('Rhstatus',  Rhstatus::getList());

        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhocorrencia_historico', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhocorrencia_historico
    # (true)redireciona ou (false) renderiza a visão /view/Rhocorrencia_historico/edit.php
    function post_edit(){
        $this->setTitle('Edição de Ocorrência - Histórico');
        try {
            $Rhocorrencia_historico = new Rhocorrencia_historico((int) $_POST['codigo']);
            $this->set('Rhocorrencia_historico', $Rhocorrencia_historico);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhocorrencia_historico->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhocorrencia_historico->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhocorrencia_historico->save($_POST);
            new Msg(__('Ocorrência - Histórico atualizado com sucesso'));
            $this->go('Rhocorrencia_historico', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhgrupo_histocorres',  Rhgrupo_histocorr::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhocorrencia_historico
    # renderiza a /view/Rhocorrencia_historico/delete.php
    function delete(){
        $this->setTitle('Apagar Ocorrência - Histórico');
        try {
            $this->set('Rhocorrencia_historico', new Rhocorrencia_historico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhocorrencia_historico', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhocorrencia_historico
    # redireciona para Rhocorrencia_historico/all
    function post_delete(){
        try {
            $Rhocorrencia_historico = new Rhocorrencia_historico((int) $_POST['id']);
            $user=Session::get('user');
            $Rhocorrencia_historico->atualizadopor=$user->id;
            $Rhocorrencia_historico->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhocorrencia_historico->status=3;
            $Rhocorrencia_historico->save();
            new Msg(__('Ocorrência - Histórico deletada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhocorrencia_historico', 'all');
    }

}