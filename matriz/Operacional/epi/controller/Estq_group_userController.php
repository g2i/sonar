<?php
final class Estq_group_userController extends AppController{ 

    # página inicial do módulo Estq_group_user
    function index(){
        $this->setTitle('Detalhes do Grupo');
    }

    # lista de Estq_group_useres
    # renderiza a visão /view/Estq_group_user/all.php
    function all(){
        $this->setTitle('Grupo de Usuários');
        $p = new Paginate('Estq_group_user', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Estq_group_useres', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Estq_group_user
    # renderiza a visão /view/Estq_group_user/view.php
    function view(){
        $this->setTitle('Detalhes do Grupo');
        try {
            $this->set('Estq_group_user', new Estq_group_user((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_group_user', 'all');
        }
    }

    # formulário de cadastro de Estq_group_user
    # renderiza a visão /view/Estq_group_user/add.php
    function add(){
        $this->setTitle('Cadastro de Grupos de Usuários');
        $this->set('Estq_group_user', new Estq_group_user);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_group_user
    # (true)redireciona ou (false) renderiza a visão /view/Estq_group_user/add.php
    function post_add(){
        $this->setTitle('Cadastro de Grupos de Usuários');
        $Estq_group_user = new Estq_group_user();
        $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_group_user->usuario_id=$user; // para salvar o usuario que está fazendo                                                                                                                                                                                $Estq_group_user->usuario_dt=date('Y-m-d H:i:s');
        $this->set('Estq_group_user', $Estq_group_user);
        try {
            $Estq_group_user->save($_POST);
            new Msg(__('Grupo de Usuários cadastrado com sucesso'));
            $this->go('Estq_group_user', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição de Estq_group_user
    # renderiza a visão /view/Estq_group_user/edit.php
    function edit(){
        $this->setTitle('Edição de Grupos de Usuários');
        try {
            $this->set('Estq_group_user', new Estq_group_user((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_group_user', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_group_user
    # (true)redireciona ou (false) renderiza a visão /view/Estq_group_user/edit.php
    function post_edit(){
        $this->setTitle('Edição de Grupos de Usuários');
        try {
            $Estq_group_user = new Estq_group_user((int) $_POST['id']);
            $this->set('Estq_group_user', $Estq_group_user);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_group_user->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_group_user->save($_POST);
            new Msg(__('Estq_group_user atualizado com sucesso'));
            $this->go('Estq_group_user', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_group_user
    # renderiza a /view/Estq_group_user/delete.php
    function delete(){
        $this->setTitle('Apagar Grupo de Usuários');
        try {
            $this->set('Estq_group_user', new Estq_group_user((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_group_user', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_group_user
    # redireciona para Estq_group_user/all
    function post_delete(){
        try {
            $Estq_group_user = new Estq_group_user((int) $_POST['id']);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_group_user->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_group_user->situacao=3;
            $Estq_group_user->save();
            new Msg(__('Grupo de Usuários apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_group_user', 'all');
    }

}