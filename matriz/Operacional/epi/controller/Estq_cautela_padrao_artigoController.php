<?php
final class Estq_cautela_padrao_artigoController extends AppController{ 

    # página inicial do módulo Estq_cautela_padrao_artigo
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Estq_cautela_padrao_artigos
    # renderiza a visão /view/Estq_cautela_padrao_artigo/all.php
    function all(){
        $this->setTitle('Lista');
        $p = new Paginate('Estq_cautela_padrao_artigo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
       
        $c->addCondition('cautela_padrao_id','=', $this->getParam('id'));
        $c->addCondition('situacao_id','=', 1);
        //$c->setOrder('artigo_id');

        $gg = new Criteria();
       // $gg->addCondition('situacao_id','=', 1);
        $gg->setOrder('artigo_id');
        $this->set('Estq_cautela_padrao_artigos', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_cautela_padraos',  Estq_cautela_padrao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Estq_cautela_padrao_artigo
    # renderiza a visão /view/Estq_cautela_padrao_artigo/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Estq_cautela_padrao_artigo', new Estq_cautela_padrao_artigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_cautela_padrao_artigo', 'all');
        }
    }

    # formulário de cadastro de Estq_cautela_padrao_artigo
    # renderiza a visão /view/Estq_cautela_padrao_artigo/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Estq_cautela_padrao_artigo', new Estq_cautela_padrao_artigo);
        $c = new Criteria();
        $c->addCondition('situacao','=', 1);
        $c->setOrder('nome');
        $this->set('Estq_artigos',  Estq_artigo::getList($c));
        $this->set('Estq_cautela_padraos',  Estq_cautela_padrao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_cautela_padrao_artigo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_cautela_padrao_artigo/add.php
    function post_add(){  
        $this->setTitle('Cadastro');
        $Estq_cautela_padrao_artigo = new Estq_cautela_padrao_artigo();
        $this->set('Estq_cautela_padrao_artigo', $Estq_cautela_padrao_artigo);
        try {
           $Estq_cautela_padrao_artigo->situacao_id = 1;
           $Estq_cautela_padrao_artigo->cadastrado_por = $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
           $Estq_cautela_padrao_artigo->dt_cadastro = date('Y-m-d H:i:s');
           $Estq_cautela_padrao_artigo->save($_POST);
            new Msg(__('Estq_cautela_padrao_artigo cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Estq_cautela_padrao_artigo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_cautela_padraos',  Estq_cautela_padrao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Estq_cautela_padrao_artigo
    # renderiza a visão /view/Estq_cautela_padrao_artigo/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Estq_cautela_padrao_artigo', new Estq_cautela_padrao_artigo((int) $this->getParam('id')));
            $this->set('Estq_artigos',  Estq_artigo::getList());
            $this->set('Estq_cautela_padraos',  Estq_cautela_padrao::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_cautela_padrao_artigo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_cautela_padrao_artigo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_cautela_padrao_artigo/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {                   
            $Estq_cautela_padrao_artigo = new Estq_cautela_padrao_artigo((int) $_POST['id']);
            $this->set('Estq_cautela_padrao_artigo', $Estq_cautela_padrao_artigo);
            $Estq_cautela_padrao_artigo->modificado_por = $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_cautela_padrao_artigo->dt_modificado = date('Y-m-d H:i:s');
            $Estq_cautela_padrao_artigo->save($_POST);
            new Msg(__('Estq_cautela_padrao_artigo atualizado com sucesso'));
              if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Estq_cautela_padrao_artigo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_cautela_padraos',  Estq_cautela_padrao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_cautela_padrao_artigo
    # renderiza a /view/Estq_cautela_padrao_artigo/delete.php
    function delete(){
        $this->setTitle('Apagar Estq_cautela_padrao_artigo');
        try {
            $this->set('Estq_cautela_padrao_artigo', new Estq_cautela_padrao_artigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_cautela_padrao_artigo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_cautela_padrao_artigo
    # redireciona para Estq_cautela_padrao_artigo/all
    function post_delete(){
        try {
            $Estq_cautela_padrao_artigo = new Estq_cautela_padrao_artigo((int) $_POST['id']);
            $Estq_cautela_padrao_artigo->situacao_id = 3;
            $Estq_cautela_padrao_artigo->save();
            new Msg(__('Estq_cautela_padrao_artigo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_cautela_padrao_artigo', 'all');
    }

}