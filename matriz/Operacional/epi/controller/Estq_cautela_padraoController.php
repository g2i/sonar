<?php
final class Estq_cautela_padraoController extends AppController{ 

    # página inicial do módulo Estq_cautela_padrao
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Estq_cautela_padraos
    # renderiza a visão /view/Estq_cautela_padrao/all.php
    function all(){
        $this->setTitle('Lista');
        $p = new Paginate('Estq_cautela_padrao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Estq_cautela_padraos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Estq_cautela_padrao
    # renderiza a visão /view/Estq_cautela_padrao/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Estq_cautela_padrao', new Estq_cautela_padrao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_cautela_padrao', 'all');
        }
    }

    # formulário de cadastro de Estq_cautela_padrao
    # renderiza a visão /view/Estq_cautela_padrao/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Estq_cautela_padrao', new Estq_cautela_padrao);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_cautela_padrao
    # (true)redireciona ou (false) renderiza a visão /view/Estq_cautela_padrao/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Estq_cautela_padrao = new Estq_cautela_padrao();
        $this->set('Estq_cautela_padrao', $Estq_cautela_padrao);
        try {
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_cautela_padrao->situacao_id = 1;
            $Estq_cautela_padrao->cadastrado_por =$user;
            $Estq_cautela_padrao->dt_cadastro = date('Y-m-d H:i:s');
            $Estq_cautela_padrao->save($_POST);
            new Msg(__('Cautela Padrão  cadastrado com sucesso'));
            $this->go('Estq_cautela_padrao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Estq_cautela_padrao
    # renderiza a visão /view/Estq_cautela_padrao/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Estq_cautela_padrao', new Estq_cautela_padrao((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_cautela_padrao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_cautela_padrao
    # (true)redireciona ou (false) renderiza a visão /view/Estq_cautela_padrao/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Estq_cautela_padrao = new Estq_cautela_padrao((int) $_POST['id']);
            $this->set('Estq_cautela_padrao', $Estq_cautela_padrao);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_cautela_padrao->modificado_por =$user;
            $Estq_cautela_padrao->dt_modificado = date('Y-m-d H:i:s');
            $Estq_cautela_padrao->save($_POST);
            new Msg(__('Cautela Padrão atualizado com sucesso'));
            $this->go('Estq_cautela_padrao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_cautela_padrao
    # renderiza a /view/Estq_cautela_padrao/delete.php
    function delete(){
        $this->setTitle('Apagar Estq_cautela_padrao');
        try {
            $this->set('Estq_cautela_padrao', new Estq_cautela_padrao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_cautela_padrao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_cautela_padrao
    # redireciona para Estq_cautela_padrao/all
    function post_delete(){
        try {
            $Estq_cautela_padrao = new Estq_cautela_padrao((int) $_POST['id']);
            $Estq_cautela_padrao->situacao_id = 3;
            $Estq_cautela_padrao->save();
            new Msg(__('Estq_cautela_padrao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_cautela_padrao', 'all');
    }

}