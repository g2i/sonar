<?php
final class Estq_unidadeController extends AppController{ 

    # página inicial do módulo Estq_unidade
    function index(){
        $this->setTitle('Visualização Tipo de Unidade');
    }

    # lista de Estq_unidades
    # renderiza a visão /view/Estq_unidade/all.php
    function all(){
        $this->setTitle('Listagem de Unidades');
        $p = new Paginate('Estq_unidade', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Estq_unidades', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Estq_unidade
    # renderiza a visão /view/Estq_unidade/view.php
    function view(){
        $this->setTitle('Visualização Tipo de Unidade');
        try {
            $this->set('Estq_unidade', new Estq_unidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_unidade', 'all');
        }
    }

    # formulário de cadastro de Estq_unidade
    # renderiza a visão /view/Estq_unidade/add.php
    function add(){
        $this->setTitle('Cadastro de Tipo de Unidade');
        $this->set('Estq_unidade', new Estq_unidade);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_unidade
    # (true)redireciona ou (false) renderiza a visão /view/Estq_unidade/add.php
    function post_add(){
        $this->setTitle('Cadastro Tipo de Unidade');
        $Estq_unidade = new Estq_unidade();
        $this->set('Estq_unidade', $Estq_unidade);
        $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_unidade->usuario_id=$user; // para salvar o usuario que está fazendo
        try {
            $Estq_unidade->save($_POST);
            new Msg(__('Tipo de Unidade cadastrada com sucesso'));
            $this->go('Estq_unidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição de Estq_unidade
    # renderiza a visão /view/Estq_unidade/edit.php
    function edit(){
        $this->setTitle('Edição de Tipo de Unidade');
        try {
            $this->set('Estq_unidade', new Estq_unidade((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_unidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_unidade
    # (true)redireciona ou (false) renderiza a visão /view/Estq_unidade/edit.php
    function post_edit(){
        $this->setTitle('Edição Tipo de Unidade');
        try {
            $Estq_unidade = new Estq_unidade((int) $_POST['id']);
            $this->set('Estq_unidade', $Estq_unidade);
            $Estq_unidade->usuario_dt=date('Y-m-d H:i:s');
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_unidade->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_unidade->save($_POST);
            new Msg(__('Tipo de Unidade atualizada com sucesso'));
            $this->go('Estq_unidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_unidade
    # renderiza a /view/Estq_unidade/delete.php
    function delete(){
        $this->setTitle('Apagar Tipo de Unidade');
        try {
            $this->set('Estq_unidade', new Estq_unidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_unidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_unidade
    # redireciona para Estq_unidade/all
    function post_delete(){
        try {
            $Estq_unidade = new Estq_unidade((int) $_POST['id']);
            $Estq_unidade->situacao=3;
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_unidade->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_unidade->save();
            new Msg(__('Tipo de Unidade deletada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_unidade', 'all');
    }

}