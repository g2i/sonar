<?php
final class Stq_caracteristicaController extends AppController{ 

    # página inicial do módulo Stq_caracteristica
    function index(){
        $this->setTitle('Visualização de Característica');
    }

    # lista de Stq_caracteristicas
    # renderiza a visão /view/Stq_caracteristica/all.php
    function all(){
        $this->setTitle('Listagem de Características');
        $p = new Paginate('Stq_caracteristica', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('origem','=',Config::get('origem')); //filtro Origem
        $this->set('Stq_caracteristicas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Stq_caracteristica
    # renderiza a visão /view/Stq_caracteristica/view.php
    function view(){
        $this->setTitle('Visualização de Característica');
        try {
            $this->set('Stq_caracteristica', new Stq_caracteristica((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Stq_caracteristica', 'all');
        }
    }

    # formulário de cadastro de Stq_caracteristica
    # renderiza a visão /view/Stq_caracteristica/add.php
    function add(){
        $this->setTitle('Cadastro de Característica');
        $this->set('Stq_caracteristica', new Stq_caracteristica);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Stq_caracteristica
    # (true)redireciona ou (false) renderiza a visão /view/Stq_caracteristica/add.php
    function post_add(){
        $this->setTitle('Cadastro de Característica');
        $Stq_caracteristica = new Stq_caracteristica();
        $this->set('Stq_caracteristica', $Stq_caracteristica);
        $_POST['origem']=Config::get('origem');//salvando a Origem
        $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
        $Stq_caracteristica->usuario_id=$user; // para salvar o usuario que está fazendo
        try {
            $Stq_caracteristica->save($_POST);
            new Msg(__('Características cadastrada com sucesso'));
            $this->go('Stq_caracteristica', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição de Stq_caracteristica
    # renderiza a visão /view/Stq_caracteristica/edit.php
    function edit(){
        $this->setTitle('Edição de Característica');
        try {
            $this->set('Stq_caracteristica', new Stq_caracteristica((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Stq_caracteristica', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Stq_caracteristica
    # (true)redireciona ou (false) renderiza a visão /view/Stq_caracteristica/edit.php
    function post_edit(){
        $this->setTitle('Edição de Característica');
        try {
            $Stq_caracteristica = new Stq_caracteristica((int) $_POST['id']);
            $this->set('Stq_caracteristica', $Stq_caracteristica);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Stq_caracteristica->usuario_id=$user; // para salvar o usuario que está fazendo
            $Stq_caracteristica->save($_POST);
            new Msg(__('Característica atualizada com sucesso'));
            $this->go('Stq_caracteristica', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Stq_caracteristica
    # renderiza a /view/Stq_caracteristica/delete.php
    function delete(){
        $this->setTitle('Apagar Características');
        try {
            $this->set('Stq_caracteristica', new Stq_caracteristica((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Stq_caracteristica', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Stq_caracteristica
    # redireciona para Stq_caracteristica/all
    function post_delete(){
        try {
            $Stq_caracteristica = new Stq_caracteristica((int) $_POST['id']);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Stq_caracteristica->usuario_id=$user; // para salvar o usuario que está fazendo
            $Stq_caracteristica->situacao=3;
            $Stq_caracteristica->save();
            new Msg(__('Característica apagada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Stq_caracteristica', 'all');
    }

}