<?php
final class UsuarioController extends AppController{ 

    # p�gina inicial do m�dulo Usuario
    function index(){
        $this->setTitle('Usuario');
    }

    # lista de Usuarios
    # renderiza a vis�o /view/Usuario/all.php
    function all(){
        $this->setTitle('Usuarios');
        $p = new Paginate('Usuario', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (isset($_GET['search'])) {
            $c->addCondition('nome', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Usuarios', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Usuario
    # renderiza a vis�o /view/Usuario/view.php
    function view(){
        try {
            $this->set('Usuario', new Usuario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'all');
        }
    }

    # formul�rio de cadastro de Usuario
    # renderiza a vis�o /view/Usuario/add.php
    function add(){
        $this->setTitle('Adicionar Usuario');
        $this->set('Usuario', new Usuario);
    }

    # recebe os dados enviados via post do cadastro de Usuario
    # (true)redireciona ou (false) renderiza a vis�o /view/Usuario/add.php
    function post_add(){
        $this->setTitle('Adicionar Usuario');
        $Usuario = new Usuario();
        $this->set('Usuario', $Usuario);
        try {
            $_POST['senha'] = md5(Config::get('salt').$_POST['senha']);
            $Usuario->save($_POST);
            new Msg(__('Usuario cadastrado com sucesso'));
            $this->go('Usuario', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formul�rio de edi��o de Usuario
    # renderiza a vis�o /view/Usuario/edit.php
    function edit(){
        $this->setTitle('Editar Usuario');
        try {
            $this->set('Usuario', new Usuario((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Usuario', 'all');
        }
    }

    # recebe os dados enviados via post da edi��o de Usuario
    # (true)redireciona ou (false) renderiza a vis�o /view/Usuario/edit.php
    function post_edit(){
        $this->setTitle('Editar Usuario');
        try {
            $Usuario = new Usuario((int) $_POST['id']);
            $this->set('Usuario', $Usuario);
            $Usuario->save($_POST);
            new Msg(__('Usuario atualizado com sucesso'));
            $this->go('Usuario', 'all');
        } catch (Exception $e) {
            new Msg(__('N�o foi poss�vel atualizar.'), 2);
        }
    }

    # Confirma a exclus�o ou n�o de um(a) Usuario
    # renderiza a /view/Usuario/delete.php
    function delete(){
        $this->setTitle('Apagar Usuario');
        try {
            $this->set('Usuario', new Usuario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Usuario
    # redireciona para Usuario/all
    function post_delete(){
        try {
            $Usuario = new Usuario((int) $_POST['id']);
            $Usuario->status=3;
            $Usuario->save();
            new Msg(__('Usuario apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Usuario', 'all');
    }

    function alterar_senha(){
        $this->setTitle("Alterar Senha");
        $user = Session::get('user_epi');
        $this->set('Usuario',$user);
    }

    function post_alterar_senha(){
        try {
            $Usuario = new Usuario((int) $_POST['id']);
            $Usuario->senha = md5(Config::get('salt').$_POST['senha']);
            $Usuario->save();
            new Msg(__('Senha Alterada com'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Usuario', 'conta');
    }

    function resetar_senha()
    {
        $this->setTitle("Resetar Senha");
        $user = Session::get('user_epi');
        $this->set('Usuario', $user);
    }

    function post_resetar_senha()
    {
        try {
            $Usuario = new Usuario((int)$_POST['id']);
            $Usuario->senha = md5(Config::get('salt') . $_POST['senha']);
            if($Usuario->primeiro_acesso == 1) {
                $Usuario->primeiro_acesso = 0;
            }

            $Usuario->save();
            new Msg(__('Senha resetada com sucesso!'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        Session::set('user_epi', NULL);
        $this->go('Login', 'login');
    }

    function conta(){
        $this->setTitle('Conta');
        $user = Session::get('user_epi');
        $this->set('Usuario',$user);
    }

}