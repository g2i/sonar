<?php
final class Estq_usersController extends AppController{ 

    # página inicial do módulo Estq_users
    function index(){
        $this->setTitle('Detalhes do Usuário');
    }

    # lista de Estq_users
    # renderiza a visão /view/Estq_users/all.php
    function all(){
        $this->setTitle('Listagem de Usuários');
        $p = new Paginate('Estq_users', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Estq_users', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_group_useres',  Estq_group_user::getList());
    

    }

    # visualiza um(a) Estq_users
    # renderiza a visão /view/Estq_users/view.php
    function view(){
        $this->setTitle('Detalhes do Usuário');
        try {
            $this->set('Estq_users', new Estq_users((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_users', 'all');
        }
    }

    # formulário de cadastro de Estq_users
    # renderiza a visão /view/Estq_users/add.php
    function add(){
        $this->setTitle('Cadastro de Usuários');
        $this->set('Estq_users', new Estq_users);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_group_useres',  Estq_group_user::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_users
    # (true)redireciona ou (false) renderiza a visão /view/Estq_users/add.php
    function post_add(){
        $this->setTitle('Cadastro de Usuários');
        $Estq_users = new Estq_users();
        $this->set('Estq_users', $Estq_users);
        $_POST['senha'] = md5(Config::get('salt') . $_POST['senha']);
        $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_users->usuario_id=$user; // para salvar o usuario que está fazendo
        try {
            $Estq_users->save($_POST);
            new Msg(__('Usuário cadastrado com sucesso'));
            $this->go('Estq_users', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_group_useres',  Estq_group_user::getList());
    }

    # formulário de edição de Estq_users
    # renderiza a visão /view/Estq_users/edit.php
    function edit(){
        $this->setTitle('Edição de Usuários');
        try {
            $this->set('Estq_users', new Estq_users((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
            $this->set('Estq_group_useres',  Estq_group_user::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_users', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_users
    # (true)redireciona ou (false) renderiza a visão /view/Estq_users/edit.php
    function post_edit(){
        $this->setTitle('Edição de Usuários');
        try {
            $Estq_users = new Estq_users((int) $_POST['id']);
            $this->set('Estq_users', $Estq_users);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_users->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_users->save($_POST);
            new Msg(__('Usuário atualizado com sucesso'));
            $this->go('Estq_users', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_group_useres',  Estq_group_user::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_users
    # renderiza a /view/Estq_users/delete.php
    function delete(){
        $this->setTitle('Apagar Usuário');
        try {
            $this->set('Estq_users', new Estq_users((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_users', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_users
    # redireciona para Estq_users/all
    function post_delete(){
        try {
            $Estq_users = new Estq_users((int) $_POST['id']);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_users->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_users->situacao=3;
            $Estq_users->save();
            new Msg(__('Usuário apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_users', 'all');
    }

    function alterar_senha(){
        $this->setTitle("Alterar Senha");
        $user = Session::get('user_epi');
        $this->set('Usuario',$user);
    }

    function post_alterar_senha(){
        try {
            $Usuario = new Estq_users((int) $_POST['id']);
            $Usuario->senha = md5(Config::get('salt').$_POST['senha']);
            $Usuario->save();
            new Msg(__('Senha Alterada com sucesso!'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_users', 'conta');
    }

    function conta(){
        $this->setTitle('Conta');
        $user = Session::get('user_epi');
        $this->set('Usuario',$user);
    }
}