<?php
final class StatusController extends AppController{ 

    # página inicial do módulo Status
    function index(){
        $this->setTitle('Visualização de Status');
    }

    # lista de Status
    # renderiza a visão /view/Status/all.php
    function all(){
        $this->setTitle('Listagem de Status');
        $p = new Paginate('Status', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Status', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Status
    # renderiza a visão /view/Status/view.php
    function view(){
        $this->setTitle('Visualização de Status');
        try {
            $this->set('Status', new Status((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Status', 'all');
        }
    }

    # formulário de cadastro de Status
    # renderiza a visão /view/Status/add.php
    function add(){
        $this->setTitle('Cadastro de Status');
        $this->set('Status', new Status);
    }

    # recebe os dados enviados via post do cadastro de Status
    # (true)redireciona ou (false) renderiza a visão /view/Status/add.php
    function post_add(){
        $this->setTitle('Cadastro de Status');
        $Status = new Status();
        $this->set('Status', $Status);
        try {
            $Status->save($_POST);
            new Msg(__('Status cadastrado com sucesso'));
            $this->go('Status', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Status
    # renderiza a visão /view/Status/edit.php
    function edit(){
        $this->setTitle('Edição de Status');
        try {
            $this->set('Status', new Status((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Status', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Status
    # (true)redireciona ou (false) renderiza a visão /view/Status/edit.php
    function post_edit(){
        $this->setTitle('Edição de Status');
        try {
            $Status = new Status((int) $_POST['id']);
            $this->set('Status', $Status);
            $Status->save($_POST);
            new Msg(__('Status atualizado com sucesso'));
            $this->go('Status', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Status
    # renderiza a /view/Status/delete.php
    function delete(){
        $this->setTitle('Apagar Status');
        try {
            $this->set('Status', new Status((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Status', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Status
    # redireciona para Status/all
    function post_delete(){
        try {
            $Status = new Status((int) $_POST['id']);
            $Status->delete();
            new Msg(__('Status apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Status', 'all');
    }

}