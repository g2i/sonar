<?php

final class Estq_movController extends AppController
{

    # página inicial do módulo Estq_mov
    function index()
    {
        $this->setTitle('Visualização de Movimento');
    }

    # lista de Estq_movs
    # renderiza a visão /view/Estq_mov/all.php
    function all()
    {
        $this->setTitle('Listagem de Movimentos');
        $p = new Paginate('Estq_mov', 10);
        $c = new Criteria();

        if (@Session::get("user_epi")->gestor_estoque == 2) {
            $c->addCondition('sub_padrao', '=', Session::get('subestoque'));
        }

        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $this->set('tipo', null);
        if (!empty($_POST['tipo_filtro'])) {
            $this->set('tipo', $_POST['tipo_filtro']);
            if ($_POST['tipo_filtro'] == 4) {
                $c->addCondition('estq_mov.fornecedor', '=', '-2');
            }
        }

        $this->set('cod_mov', null);
        if (!empty($_POST['cod_mov'])) {
            $this->set('cod_mov', $_POST['cod_mov']);
            $c->addCondition('id', "=", $_POST['cod_mov']);
        }


        $c->addCondition('estq_tipomov.cautela', '=', 0);

        $c->setOrder('id desc');
        $c->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $c->addCondition('situacao', '<>', '3'); //filtro

        $this->set('Estq_situacaos', Estq_situacao::getList());

        $f = new Criteria();
        $f->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $f->addCondition('situacao', '<>', 3); //filtro
        $f->addCondition('cautela', '=', 0); //filtro
        $f->setOrder('nome');

        $this->set('Estq_tipomovs', Estq_tipomov::getList($f));

        $this->set('Estq_movs', $p->getPage($c));
        $this->set('nav', $p->getNav());


    }

    # visualiza um(a) Estq_mov
    # renderiza a visão /view/Estq_mov/view.php
    function view()
    {
        $this->setTitle('Visualização de Movimento');
        try {
            $this->set('Estq_mov', new Estq_mov((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_mov', 'all');
        }
    }

    # formulário de cadastro de Estq_mov
    # renderiza a visão /view/Estq_mov/add.php
    function add()
    {
        $this->setTitle('Cadastro de Movimento');
        $a = new Criteria();
        $a->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $a->setOrder('nome');
        $a->addCondition('situacao', '<>', 3);
        $this->set('Estq_tipomovs', Estq_tipomov::getList($a));

        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $b->setOrder('nome');
        $this->set('Fornecedors', Fornecedor::getList($b));

        $c = new Criteria();
        $c->setOrder('nome');
        $c->addCondition('status', '<>', 3);
        $this->set('Clientes', Cliente::getList($c));

        $d = new Criteria();
        $d->addCondition('status', '<>', 3);
        $d->setOrder('nome ASC');
        $this->set('Rhprofissionals', Rhprofissional::getList($d));


        $this->set('Estq_mov', new Estq_mov);
        $this->set('Estq_situacaos', Estq_situacao::getList());

        $a = new Criteria();
        $a->addCondition('situacao', '<>', 3);
        $a->addCondition('origem', '=', Config::get('origem'));
        $a->setOrder('nome ASC');
        $this->set('sub_estoque', Estq_subestoque::getList($a));


    }

    function post_add()
    {
        $this->setTitle('Cadastro de Movimento');
        $Estq_mov = new Estq_mov();
        $this->set('Estq_mov', $Estq_mov);
        $Estq_mov->usuario_dt = date('Y-m-d H:i:s');
        $_POST['origem'] = Config::get('origem');//salvando a origem
        if ($_POST['datadoc'] != NULL) {
            $_POST['datadoc'] = DataSQL($_POST['datadoc']); //converte para o formato americano
        }
        if ($_POST['data'] != NULL) {
            $_POST['data'] = ConvertDateTime($_POST['data']);
        }
        if ($_POST['requer'] != NULL) {
            if ($_POST['requer'] == "Fornecedor") {
                $_POST['colaborador'] = null;
                $_POST['cliente'] = null;
            } elseif ($_POST['requer'] == "Colaborador") {
                $_POST['fornecedor'] = null;
                $_POST['cliente'] = null;
            } elseif ($_POST['requer'] == "Cliente") {
                $_POST['fornecedor'] = null;
                $_POST['colaborador'] = null;
            } elseif ($_POST['requer'] == 4) {
                $_POST['fornecedor'] = '-2';
                $_POST['colaborador'] = null;
                $_POST['cliente'] = null;
            }

        }

        if (!empty($_POST['padrao'])) {
            if ($_POST['padrao'] == 2) {
                $_POST['sub_padrao'] = null;
            }
        }

        $_POST['status'] = 1;
        $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo
        try {
            if ($_POST['fornecedor'] != null || $_POST['cliente'] != null || $_POST['colaborador'] != null) {
                $Estq_mov->save($_POST);
                new Msg(__('Movimento cadastrado com sucesso'));

                $this->go('Estq_movdetalhes', 'add', array('id' => $Estq_mov->id));
            } else {
                new Msg('Informe um Cliente , Fornecedor ou Colaborador !', 3);
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList());
        $this->set('Fornecedors', Fornecedor::getList($b));
    }

    function novo()
    {
        unset($_SESSION['itens']);

        $this->setTitle('Cadastro de Movimento');

        $a = new Criteria();
        $a->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $a->setOrder('nome');
        $a->addCondition('situacao', '<>', 3);
//        if($this->getParam('cautela')) {
//            $a->addCondition('cautela', '=', 1); //filtro
//        }
        $a->addCondition('cautela', '<>', 1); //filtro
        $this->set('Estq_tipomovs', Estq_tipomov::getList($a));


        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $b->setOrder('nome');
        $this->set('Fornecedors', Fornecedor::getList($b));

        $c = new Criteria();
        $c->setOrder('nome');
        $c->addCondition('status', '<>', 3);
        $this->set('Clientes', Cliente::getList($c));

        $d = new Criteria();
        $d->addCondition('status', '<>', 3);
        $d->setOrder('nome ASC');
        $this->set('Rhprofissionals', Rhprofissional::getList($d));


        $this->set('Estq_mov', new Estq_mov);
        $this->set('Estq_situacaos', Estq_situacao::getList());

        $a = new Criteria();
        $a->addCondition('situacao', '<>', 3);
        $a->addCondition('origem', '=', Config::get('origem'));
        $a->setOrder('nome ASC');
        $this->set('sub_estoque', Estq_subestoque::getList($a));
    }

    # recebe os dados enviados via post do cadastro de Estq_mov
    # (true)redireciona ou (false) renderiza a visão /view/Estq_mov/add.php
    function post_novo()
    {
        $this->setTitle('Cadastro de Movimento');
        if (count($_SESSION['itens']) <= 0) {
            $this->go('Estq_mov', 'novo');
            new Msg('Selecione algum artigo', 3);

        }
        $Estq_mov = new Estq_mov();
        $this->set('Estq_mov', $Estq_mov);
        $Estq_mov->usuario_dt = date('Y-m-d H:i:s');
        $_POST['origem'] = Config::get('origem');//salvando a origem
        if ($_POST['datadoc'] != NULL) {
            $_POST['datadoc'] = DataSQL($_POST['datadoc']); //converte para o formato americano
        }
        if ($_POST['data'] != NULL) {
            $_POST['data'] = ConvertDateTime($_POST['data']);
        }
        if ($_POST['requer'] != NULL) {
            if ($_POST['requer'] == "Fornecedor") {
                $_POST['colaborador'] = null;
                $_POST['cliente'] = null;
            } elseif ($_POST['requer'] == "Colaborador") {
                $_POST['fornecedor'] = null;
                $_POST['cliente'] = null;
            } elseif ($_POST['requer'] == "Cliente") {
                $_POST['fornecedor'] = null;
                $_POST['colaborador'] = null;
            } elseif ($_POST['requer'] == 4) {
                $_POST['fornecedor'] = '-2';
                $_POST['colaborador'] = null;
                $_POST['cliente'] = null;
            }

        }

        if (!empty($_POST['padrao'])) {
            if ($_POST['padrao'] == 2) {
                $_POST['sub_padrao'] = null;
            }
        }

        $_POST['status'] = 1;
        $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo
        try {
            if ($_POST['fornecedor'] != null || $_POST['cliente'] != null || $_POST['colaborador'] != null) {
                if ($Estq_mov->save($_POST)) {
                    foreach ($_SESSION['itens'] as $i => $item) {
                        $Estq_movdetalhes = new Estq_movdetalhes();
                        $Estq_movdetalhes->artigo = $item['artigo_id'];
                        $Estq_movdetalhes->quantidade = $item['quantidade'];
                        $Estq_movdetalhes->lote_substoque = $item['lote_id'];
                        $Estq_movdetalhes->marca = $item['marca'];
                        $Estq_movdetalhes->validade = stringDateBRToSQL($item['validade']);
                        $Estq_movdetalhes->novo_usado = $item['novo_usado'];
                        $Estq_movdetalhes->vlcusto = 0.0000;
                        $Estq_movdetalhes->vlnf = 0.0000;
                        $Estq_movdetalhes->vlcustomedio = 0.0000;
                        $Estq_movdetalhes->movimento = $Estq_mov->id;
                        $Estq_movdetalhes->usuario_dt = date('Y-m-d H:i:s');
                        $Estq_movdetalhes->usuario_id = $user->id;
                        $Estq_movdetalhes->situacao = 1;
                        $Estq_movdetalhes->origem = Config::get('origem');
                        $Estq_movdetalhes->qtrecebido = 0.0000;
                        $Estq_movdetalhes->qtseparado = 0.0000;
                        $Estq_movdetalhes->qtcautela_encarregado = 0.0000;
                        $Estq_movdetalhes->qtdevolucao_encarregado = 0.0000;
                        $Estq_movdetalhes->qtinventario_campo = 0.0000;
                        $Estq_movdetalhes->qtzmdv = 0.0000;
                        $Estq_movdetalhes->adicional = 0.0000;
                        $Estq_movdetalhes->save();

                    }
                    unset($_SESSION['itens']);
                }
                new Msg(__('Movimento cadastrado com sucesso'));

                //Se vier da tela de cadastro de cautela, vai para listagem de cautela
                //se não, vai para listagem normal
                if (isset($_POST['cautela'])) {
                    $this->go('Cautela', 'edit', array('id' => $Estq_mov->id));
                } else {
                    $this->go('Estq_mov', 'edit', array('id' => $Estq_mov->id));
                }

                //$this->go('Estq_mov', 'all');
            } else {
                new Msg('Informe um Cliente , Fornecedor ou Colaborador !', 3);
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList());
        $this->set('Fornecedors', Fornecedor::getList($b));
    }
    # formulário de edição de Estq_mov
    # renderiza a visão /view/Estq_mov/edit.php
    function edit()
    {
        $this->setTitle('Edição de Movimento');
        try {
            $this->set('Estq_mov', new Estq_mov((int)$this->getParam('id')));
            $a = new Criteria();
            $a->addCondition('origem', '=', Config::get('origem')); //filtro Origem
            $b = new Criteria();
            $b->setOrder('nome');
            $b->addCondition('situacao', '<>', 3);
            $c = new Criteria();
            $c->addCondition('status', '<>', 3);
            $c->setOrder('nome');
            $d = new Criteria();
            $d->addCondition('status', '<>', 3);
            $d->setOrder('nome');
            $this->set('Clientes', Cliente::getList($c));
            $this->set('Fornecedors', Fornecedor::getList($b));
            $this->set('Estq_situacaos', Estq_situacao::getList());
            $this->set('Estq_tipomovs', Estq_tipomov::getList($a));
            $this->set('Rhprofissionals', Rhprofissional::getList($d));

            $a = new Criteria();
            $a->addCondition('situacao', '<>', 3);
            $a->addCondition('origem', '=', Config::get('origem'));
            $a->setOrder('nome ASC');
            $this->set('sub_estoque', Estq_subestoque::getList($a));

        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_mov', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_mov
    # (true)redireciona ou (false) renderiza a visão /view/Estq_mov/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Movimento');
        try {
            $Estq_mov = new Estq_mov((int)$_POST['id']);
            $this->set('Estq_mov', $Estq_mov);
            if ($_POST['datadoc'] != NULL) {
                $_POST['datadoc'] = DataSQL($_POST['datadoc']); //converte para o formato americano
            }
            if ($_POST['data'] != NULL) {
                $_POST['data'] = ConvertDateTime($_POST['data']);
            }
            $Estq_mov->usuario_dt = date('Y-m-d H:i:s');
            if ($_POST['requer'] != NULL) {
                if ($_POST['requer'] == "Fornecedor") {
                    $_POST['colaborador'] = null;
                    $_POST['cliente'] = null;
                } elseif ($_POST['requer'] == "Colaborador") {
                    $_POST['fornecedor'] = null;
                    $_POST['cliente'] = null;
                } elseif ($_POST['requer'] == "Cliente") {
                    $_POST['fornecedor'] = null;
                    $_POST['colaborador'] = null;
                } elseif ($_POST['requer'] == 4) {
                    $_POST['fornecedor'] = '-2';
                    $_POST['colaborador'] = null;
                    $_POST['cliente'] = null;
                }
            }

            if (!empty($_POST['padrao'])) {
                if ($_POST['padrao'] == 2) {
                    $_POST['sub_padrao'] = null;
                }
            }

            $_POST['origem'] = Config::get('origem');//salvando a origem
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo
            if ($_POST['fornecedor'] != null || $_POST['cliente'] != null || $_POST['colaborador'] != null) {
                $Estq_mov->save($_POST);
                new Msg(__('Movimento atualizado com sucesso'));
                $this->go('Estq_mov', 'all');
            } else {
                new Msg('Informe um Cliente , Fornecedor ou Colaborador !', 3);
            }

        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $this->set('Fornecedors', Fornecedor::getList($b));
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList());
        $this->set('Rhprofissionals', Rhprofissional::getList());

    }

    # Confirma a exclusão ou não de um(a) Estq_mov
    # renderiza a /view/Estq_mov/delete.php
    function delete()
    {
        $this->setTitle('Apagar Movimento');
        try {
            $this->set('Estq_mov', new Estq_mov((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_mov', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_mov
    # redireciona para Estq_mov/all
    function post_delete()
    {
        try {
            $Estq_mov = new Estq_mov((int)$_POST['id']);
            $Estq_mov->usuario_dt = date('Y-m-d H:i:s');
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_mov->situacao = 3;
            $Estq_mov->save();
            new Msg(__('Movimento deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_mov', 'all');
    }

    function processamento()
    {

        //$Estq_mov->getEstq_tipomov()->operacao;
        try {
            $Estq_mov = new Estq_mov((int)$this->getParam('id'));
            $baixar = strtolower($this->getParam('baixar')) == 'true';

            if ($baixar) {
                $a = new Criteria();
                $a->addCondition('movimento', '=', $Estq_mov->id);
                $a->addCondition('situacao', '=', 1);
                $Estq_movdetalhe = Estq_movdetalhes::getList($a);

                $Tipo_movimento = new Estq_tipomov((int)$Estq_mov->tipo);
                if (!is_array($Estq_movdetalhe)) {
                    new Msg('Cadastre os detalhes do movimento', 2);
                } else {
                    foreach ($Estq_movdetalhe as $e) {
                        $Lote_substoques = new Estq_lote_substoque($e->lote_substoque);
                        if ($Tipo_movimento->operacao == '+')
                            $Lote_substoques->quantidade = $Lote_substoques->quantidade + $e->quantidade;
                        else
                            $Lote_substoques->quantidade = $Lote_substoques->quantidade - $e->quantidade;

                        $Lote_substoques->save();
                    }
                }
            }

            $user = @Session::get("user_epi")->nome;

            $Estq_mov->dtBaixa = date('Y-m-d H:i:s');
            $Estq_mov->baixado = $baixar ? 1 : 0;
            $Estq_mov->baixado_por = $user;
            $Estq_mov->status = 2;
            $Estq_mov->save();

            new Msg(__('Movimento processado com sucesso'));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }
        $this->go('Estq_mov', 'all');
    }

    function confirmacao()
    {
        $this->setTitle("Confirmação");
    }

    function get_tipo()
    {
        $this->setTitle('Tipo');

        switch ($_POST['tipo']) {
            case 1:
                $e = new Criteria();
                $e->addCondition('situacao', '<>', 3);
                $e->setOrder('nome');
                $Fornecedors = Fornecedor::getList($e);
                $html = "";
                $html .= ' <div class="form-group col-md-4 col-sm-6 col-xs-12 fornecedor">
                                <label for="fornecedor">Fornecedor</label>
                                <select name="filtro[externo][fornecedor]" class="form-control" id="fornecedor">
                                    <option value="">Selecione:</option>';
                foreach ($Fornecedors as $e) {
                    if ($_POST['id'] == $e->id) {
                        $html .= '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                    } else {
                        $html .= '<option value="' . $e->id . '">' . $e->nome . '</option>';
                    }
                }
                $html .= '</select>
                            </div>';
                echo $html;
                exit;

                break;
            case 2:
                $d = new Criteria();
                $d->addCondition('status', '<>', 3);
                $d->setOrder('nome');
                $Clientes = Cliente::getList($d);
                $html = "";
                $html .= ' <div class="form-group col-md-4 col-sm-6 col-xs-12 ">
                                <label for="cliente">Cliente</label>
                                <select name="filtro[externo][cliente]" class="form-control" id="cliente">
                                    <option value="">Selecione:</option>';
                foreach ($Clientes as $e) {
                    if ($_POST['id'] == $e->id) {
                        $html .= '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                    } else {
                        $html .= '<option value="' . $e->id . '">' . $e->nome . '</option>';
                    }
                }
                $html .= '</select>
                            </div>';
                echo $html;
                exit;
                break;

            case 3:
                $g = new Criteria();
                $g->addCondition('status', '<>', 3);
                $g->setOrder('nome');
                $Rhprofissionals = Rhprofissional::getList($g);
                $html = "";
                $html .= ' <div class="form-group col-md-4 col-sm-6 col-xs-12 ">
                            <label for="colaborador">Colaborador</label>
                                <select name="filtro[externo][colaborador]" class="form-control" id="colaborador">
                                    <option value="">Selecione:</option>';
                foreach ($Rhprofissionals as $e) {
                    if ($_POST['id'] == $e->codigo) {
                        $html .= '<option selected value="' . $e->codigo . '">' . $e->nome . '</option>';
                    } else {
                        $html .= '<option value="' . $e->codigo . '">' . $e->nome . '</option>';
                    }
                }
                $html .= '</select>
                            </div>';
                echo $html;
                exit;

                break;

        }
    }
   
}