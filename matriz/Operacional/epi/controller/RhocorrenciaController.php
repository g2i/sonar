<?php
final class RhocorrenciaController extends AppController{ 

    # página inicial do módulo Rhocorrencia
    function index(){
        $this->setTitle('Visualização de Ocorrência');
    }

    # lista de Rhocorrencias
    # renderiza a visão /view/Rhocorrencia/all.php
    function all(){
        $this->setTitle('Listagem de Ocorrência');
        $p = new Paginate('Rhocorrencia', 10);
        $this->set('inicio_data',NULL);
        $this->set('fim_data',NULL);
        $this->set('profissional',NULL);
        $this->set('hist',NULL);

        $c = new Criteria();


        if(!empty($_GET['inicio_data'])){
            $c->addCondition('data','>=',DataSQL($_GET['inicio_data']));
            $this->set('inicio_data',$_GET['inicio_data']);
        }
        if(!empty($_GET['fim_data'])){

            $c->addCondition('data','<=',DataSQL($_GET['fim_data']));
            $this->set('fim_data',$_GET['fim_data']);
        }

        if(!empty($_GET['profissional'])){
            $c->addCondition('rhprofissional.nome','LIKE',$_GET['profissional'].'%');
            $this->set('profissional',$_GET['profissional']);
        }

        if(!empty($_GET['historico'])){
            $c->addCondition('rhocorrencia_historico.codigo','=',$_GET['historico']);
            $this->set('hist',$_GET['historico']);
        }


        //pega o id do profissional para listar
        if($this->getParam('id')){
            $c->addCondition('codigo_profissional','=',$this->getParam('id'));
            $this->set('Profissional', new Rhprofissional($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        }

            $c->setOrder('data');

        $this->set('Rhocorrencias', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Usuario',  Usuario::getList());
        $this->set('Rhstatus',  Rhstatus::getList());



    }

    # visualiza um(a) Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/view.php
    function view(){
        $this->setTitle('Visualização de Ocorrência');
        try {
            $this->set('Rhocorrencia', new Rhocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhocorrencia', 'all');
        }
    }

    # formulário de cadastro de Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/add.php
    function add(){
        $this->setTitle('Cadastro de Ocorrência');
        $this->set('Rhocorrencia', new Rhocorrencia);
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Rhocorrencia/add.php
    function post_add(){
        $this->setTitle('Cadastro de Ocorrência');
        $Rhocorrencia = new Rhocorrencia();
        $this->set('Rhocorrencia', $Rhocorrencia);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhocorrencia->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhocorrencia->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
       // $Rhocorrencia->data_especifica=date('Y-m-d H:i:s');//salva a data especifica
        $_POST['status']=1;
    
        //grava o valor para data especifica ja com a regra por periocidade
        $periodicidade = $_POST['tipoPeriodicidade'];
        //Anaual
        if($periodicidade == "Anual"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 years'));//ok
        }
        if($periodicidade == "BiAnual"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+2 years'));
        }
        if($periodicidade == "Mensal"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 month'));//ok
        }
        if($periodicidade == "Semestral"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+6 week'));//ok
        }
        if($periodicidade == "Semanal"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 week'));//ok
        }
        if($periodicidade == "Diaria"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 day'));//ok
        }
        
        try {
            $Rhocorrencia->save($_POST);
            new Msg(__('Ocorrência cadastrada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhocorrencia', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }
   /* function post_add(){
        $this->setTitle('Cadastro de Ocorrência');
        $Rhocorrencia = new Rhocorrencia();
        $this->set('Rhocorrencia', $Rhocorrencia);
        $user=Session::get('user_epi');// para salvar o usuario que está fazendo
        $Rhocorrencia->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhocorrencia->complemento_cad='Em: '.date('Y-m-d H:i:s').' - Cadastrado no epi, por: '.$user->nome.'\n\n';
        $Rhocorrencia->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhocorrencia->save($_POST);
            new Msg(__('Ocorrência cadastrada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Index', 'rh');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }*/

    # formulário de edição de Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/edit.php
    function edit(){
        $this->setTitle('Edição de Ocorrência');
        try {
            $this->set('Rhocorrencia', new Rhocorrencia((int) $this->getParam('id')));
            $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
            $this->set('Rhprofissionais',  Rhprofissional::getList());
            $this->set('Rhstatus',  Rhstatus::getList());

        } catch (Exception $e) {
            new Msg($e->getMessage(),3);

            $this->go('Index', 'rh');
        }
    }

    # recebe os dados enviados via post da edição de Rhocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Rhocorrencia/edit.php
    function post_edit(){
        $this->setTitle('Edição de Ocorrência');
        try {
            $user = Session::get('user');
            $Rhocorrencia = new Rhocorrencia((int) $_POST['codigo']);
            $this->set('Rhocorrencia', $Rhocorrencia);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhocorrencia->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhocorrencia->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhocorrencia->data_especifica=date('Y-m-d H:i:s');//salva a data especifica

             //grava o valor para data especifica ja com a regra por periocidade
        $periodicidade = $_POST['tipoPeriodicidade'];
        //Anaual
        if($periodicidade == "Anual"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 years'));
        }
        if($periodicidade == "BiAnual"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 years'));
        }
        if($periodicidade == "Mensal"){ 
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 month'));
        }
        if($periodicidade == "Semestral"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+6 week'));
        }
        if($periodicidade == "Semanal"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 week'));
        }
        if($periodicidade == "Diaria"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 day'));
        }

            $Rhocorrencia->save($_POST);
            new Msg(__('Ocorrência atualizada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Index', 'rh');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }
   /* function post_edit(){
        $this->setTitle('Edição de Ocorrência');
        try {

            $Rhocorrencia = new Rhocorrencia((int) $_POST['codigo']);
            $this->set('Rhocorrencia', $Rhocorrencia);
            $user=Session::get('user_epi');// para salvar o usuario que está fazendo
            $Rhocorrencia->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $comp_ant = $Rhocorrencia->complemento_up;
            $Rhocorrencia->complemento_up='Em: '.date('Y-m-d H:i:s').' - Atualizado no epi, por: '.$user->nome.'\n\n'.$comp_ant; // para salvar o usuario que está fazendo

            $Rhocorrencia->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhocorrencia->save($_POST);
            new Msg(__('Ocorrência atualizada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI

            $this->go('Index', 'rh');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }*/

    # Confirma a exclusão ou não de um(a) Rhocorrencia
    # renderiza a /view/Rhocorrencia/delete.php
    function delete(){
        $this->setTitle('Apagar Rhocorrencia');
        try {
            $this->set('Rhocorrencia', new Rhocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);

            $this->go('Index', 'rh');
        }
    }

    # Recebe o id via post e exclui um(a) Rhocorrencia
    # redireciona para Rhocorrencia/all
    function post_delete(){
        try {
            $user=Session::get('user_epi');// para salvar o usuario que está fazendo
            $Rhocorrencia = new Rhocorrencia((int) $_POST['id']);
            $Rhocorrencia->status=3;

            $Rhocorrencia->complemento_del='Em: '.date('Y-m-d H:i:s').' - Deletadp no epi, por: '.$user->nome.'\n\n';
            $Rhocorrencia->save();
            new Msg(__('Ocorrência apagada com sucesso'), 1);
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }

        $this->go('Index', 'rh');
    }

}