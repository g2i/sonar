<?php

final class Estq_artigoController extends AppController
{

    # página inicial do módulo Estq_artigo
    function index()
    {
        $this->setTitle('Visualização de Artigos');
    }

    # lista de Estq_artigos
    # renderiza a visão /view/Estq_artigo/all.php
    function all()
    {
        $this->setTitle('Listagem de Artigos');
        $p = new Paginate('Estq_artigo', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->setOrder('nome');
        //var_dump(Session::get('subestoque'));exit;
        $c->addCondition('origem', '=', Config::get('origem')); //filtro
        //$c->addCondition('Estq_usuario_substoque.substoque_id', '=' , Session::get('subestoque'); //filtro
         $c->addCondition('substoque_id', '=', Session::get('subestoque'));


        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $b->addCondition('origem', '=', Config::get('origem'));
        $this->set('Estq_artigos', $p->getPage($c));

        $this->set('nav', $p->getNav());

        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_subgrupos', Estq_subgrupo::getList($b));
        $this->set('Estq_unidade', Estq_unidade::getList());


    }

    # visualiza um(a) Estq_artigo
    # renderiza a visão /view/Estq_artigo/view.php
    function view()
    {
        $this->setTitle('Visualização de Artigos');
        try {
            $this->set('Estq_artigo', new Estq_artigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo', 'all');
        }
    }

    # formulário de cadastro de Estq_artigo
    # renderiza a visão /view/Estq_artigo/add.php
    function add()
    {
        $this->setTitle('Cadastro de Artigos');
        $this->set('Estq_artigo', new Estq_artigo);
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $c = new Criteria();
        $c->addCondition('origem', '=', Config::get('origem'));
        $c->addCondition('situacao', '<>', 3);
        $this->set('Estq_subgrupo', Estq_subgrupo::getList());
        $this->set('Estq_unidade', Estq_unidade::getList());
        $this->set('Estq_concessionarias', Estq_concessionaria::getList());


        $g = new Criteria();
        $g->addCondition('id', '=', Session::get('subestoque'));
        $this->set('Estq_subestoque', Estq_subestoque::getList($g));

    }

    # recebe os dados enviados via post do cadastro de Estq_artigo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo/add.php
    function post_add()
    {           
        $this->setTitle('Cadastro de Artigos');
        $Estq_artigo = new Estq_artigo();
        $this->set('Estq_artigo', $Estq_artigo);
        try {
            if (!empty($_POST['vlcomercial'])) {
                $_POST['vlcomercial'] = str_replace(".", "", $_POST['vlcomercial']);
                $_POST['vlcomercial'] = str_replace(",", ".", $_POST['vlcomercial']);
            }
            if (!empty($_POST['vlcusto'])) {
                $_POST['vlcusto'] = str_replace(".", "", $_POST['vlcusto']);
                $_POST['vlcusto'] = str_replace(",", ".", $_POST['vlcusto']);
            }
            if (!empty($_POST['vlcustomedio'])) {
                $_POST['vlcustomedio'] = str_replace(".", "", $_POST['vlcustomedio']);
                $_POST['vlcustomedio'] = str_replace(",", ".", $_POST['vlcustomedio']);
            }


            $_POST['origem'] = Config::get('origem');//salvando a origem
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_artigo->usuario_id = $user; // para salvar o usuario que está fazendo

            $d = new Criteria();
            $d->addSqlConditions("nome = 'controla_lote'");
            $d->addCondition("status", '=', 1);

            if (Estq_configuracao::getFirst($d)) {
                $_POST['controla_lote'] = 0;
                $Estq_artigo->usuario_dt = date('Y-m-d H:i:s'); //salva a hora que está fazendo
    
                if($Estq_artigo->save($_POST)){
                    if (!empty($_FILES['foto']['name'])) {
                        $img = $_FILES['foto'];
                        $foto = new ImageUploader($img, 150);
                        $path = "/fotos/" . $Estq_artigo->id;
                        $nome = uniqid() . $img['name'];
                        $foto->save($nome, $path);
                        $Estq_artigoImg = new Estq_artigo($Estq_artigo->id);
                        $Estq_artigoImg->foto = SITE_PATH . '/uploads' . $path . "/" . $nome;
                        $Estq_artigoImg->save();
                    }
                }
                $Estq_artigo_lotes = new Estq_artigo_lote();
                $Estq_artigo_lotes->nome = "Unico";
                $Estq_artigo_lotes->artigo = $Estq_artigo->id;
                $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
                $Estq_artigo_lotes->usuario_id = $user; // para salvar o usuario que está fazendo
                //$Estq_artigo_lotes->validade = 2999 - 10 - 13;
                $Estq_artigo_lotes->validade = date('2999-10-13');
                $Estq_artigo_lotes->situacao = 1;
                $Estq_artigo_lotes->origem = Config::get('origem');//salvando a origem
                $Estq_artigo_lotes->sem_lote = 1;
                $Estq_artigo_lotes->save();


                $Estq_subestoques = Estq_subestoque::getList();
                foreach ($Estq_subestoques as $e) {
                    $Estq_lote_substoques = new Estq_lote_substoque();
                    $Estq_lote_substoques->lote = $Estq_artigo_lotes->id;
                    $Estq_lote_substoques->artigo = $Estq_artigo->id;
                    $Estq_lote_substoques->quantidade = 0;
                    $Estq_lote_substoques->subestoque = $e->id;
                    $Estq_lote_substoques->estoque_minimo = 0;
                    $Estq_lote_substoques->usuario_id = $user;
                    $Estq_lote_substoques->situacao = 1;
                    $Estq_lote_substoques->origem = Config::get('origem');//salvando a origem
                    $Estq_lote_substoques->save();

                }

            } else {
                $_POST['controla_lote'] = 1;
                if($Estq_artigo->save($_POST)){
                    if (!empty($_FILES['foto']['name'])) {
                        $img = $_FILES['foto'];
                        $foto = new ImageUploader($img, 150);
                        $path = "/fotos/" . $Estq_artigo->id;
                        $nome = uniqid() . $img['name'];
                        $foto->save($nome, $path);
                        $Estq_artigoImg = new Estq_artigo($Estq_artigo->id);
                        $Estq_artigoImg->foto = SITE_PATH . '/uploads' . $path . "/" . $nome;
                        $Estq_artigoImg->save();
                    }
                }
            }

            /**
             * Vincular as concessionarias
             */

            if (!empty($_POST['concessionaria'])) {
                $concessionarias = $_POST['concessionaria'];
                for ($i = 0; $i < count($_POST['concessionaria']); $i++) {
                    $estq_artigo_concessionaria = new Estq_artigo_concessionaria();
                    $estq_artigo_concessionaria->id_artigo = $Estq_artigo->id;
                    $estq_artigo_concessionaria->id_concessionaria = $concessionarias[$i];
                    $estq_artigo_concessionaria->save();
                }

            }

            /**
             * Vincular as Estq_artigo_subestoque
           
            if (!empty($_POST['substoques'])) {
                $substoque = $_POST['substoques'];
                for ($i = 0; $i < count($_POST['substoques']); $i++) {
                    $Estq_artigo_subestoque = new Estq_artigo_subestoque();
                    $Estq_artigo_subestoque->artigo_id = $Estq_artigo->id;
                    $Estq_artigo_subestoque->substoque_id = $substoque[$i];
                    $Estq_artigo_subestoque->dt_cadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
                    $Estq_artigo_subestoque->situacao_id  =$_POST['situacao_id'] = 1;
                  //  $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
                    $Estq_artigo_subestoque->cadastradopor = @Session::get("user_epi")->id; // para salvar o usuario que está fazendo
                    $Estq_artigo_subestoque->save();
                }
            } */
           
        
            
            new Msg(__('Artigos cadastrado com sucesso'));
            if($_POST['ensaio'] == 1){       
                $this->go('Estq_artigo_lote', 'all', array('id' => $Estq_artigo->id));                    
            }else{
                $this->go('Estq_artigo', 'all');
            }
           // 
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_subgrupos', Estq_subgrupo::getList());
        $this->set('Estq_unidade', Estq_unidade::getList());
    }
    # formulário de edição de Estq_artigo
    # renderiza a visão /view/Estq_artigo/edit.php
    function edit()
    {
        $this->setTitle('Edição de Artigos');
        try {
            $this->set('Estq_artigo', new Estq_artigo((int)$this->getParam('id')));
            $this->set('Estq_situacaos', Estq_situacao::getList());
            $c = new Criteria();
            $c->addCondition('origem', '=', Config::get('origem')); //filtro
            $this->set('Estq_subgrupos', Estq_subgrupo::getList($c));
            $this->set('Estq_unidade', Estq_unidade::getList());

            $d = new Criteria();
            $d->addCondition('id_artigo', '=', (int)$this->getParam('id'));

            $artigo_concessionarias = [];
            foreach (Estq_artigo_concessionaria::getList($d) as $ac) {
                array_push($artigo_concessionarias, $ac->id_concessionaria);
            }

            $this->set('Estq_artigo_concessionarias', $artigo_concessionarias);
            $this->set('Estq_concessionarias', Estq_concessionaria::getList());



            $g = new Criteria();
            $g->addCondition('artigo_id', '=', (int)$this->getParam('id'));
            $Estq_subestoques = [];
            foreach (Estq_artigo_subestoque::getList($g) as $ac) {
                array_push($Estq_subestoques, $ac->substoque_id);
            }
            $this->set('Estq_artigo_subestoques', $Estq_subestoques);
            
            $g = new Criteria();
            $g->addCondition('id', '=', Session::get('subestoque'));
            $this->set('Estq_subestoque', Estq_subestoque::getList($g));

        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_artigo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_artigo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Artigos');
        try {
            $Estq_artigo = new Estq_artigo((int)$_POST['id']);
            
            $_POST['usuario_dt'] = date('Y-m-d H:i:s');

            if (!empty($_POST['vlcomercial'])) {
                $_POST['vlcomercial'] = str_replace(".", "", $_POST['vlcomercial']);
                $_POST['vlcomercial'] = str_replace(",", ".", $_POST['vlcomercial']);
            }

            if (!empty($_POST['vlcusto'])) {
                $_POST['vlcusto'] = str_replace(".", "", $_POST['vlcusto']);
                $_POST['vlcusto'] = str_replace(",", ".", $_POST['vlcusto']);
            }

            if (!empty($_POST['vlcustomedio'])) {
                $_POST['vlcustomedio'] = str_replace(".", "", $_POST['vlcustomedio']);
                $_POST['vlcustomedio'] = str_replace(",", ".", $_POST['vlcustomedio']);
            }

            $this->set('Estq_artigo', $Estq_artigo);
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_artigo->usuario_id = $user; // para salvar o usuario que está fazendo

            if (!empty($_FILES['foto']['name'])) {
                $img = $_FILES['foto'];
                $foto = new ImageUploader($img, 150);
                $path = "/fotos/" . $Estq_artigo->id;
                $nome = uniqid() . $img['name'];
                $foto->save($nome, $path);
                $Estq_artigoImg = new Estq_artigo($Estq_artigo->id);
                $Estq_artigoImg->foto = SITE_PATH . '/uploads' . $path . "/" . $nome;
                $Estq_artigoImg->save();
            }
            /**
             * Apaga os dados da tabela estq_artigo_concessionaria para esse artigo
             *
             * E insere novamente conforme o usuario editou
             */
            $this->query("DELETE FROM estq_artigo_concessionaria WHERE id_artigo = " . (int)$_POST['id']);
            if (!empty($_POST['concessionaria'])) {
                $concessionarias = $_POST['concessionaria'];
                for ($i = 0; $i < count($_POST['concessionaria']); $i++) {
                    $estq_artigo_concessionaria = new Estq_artigo_concessionaria();
                    $estq_artigo_concessionaria->id_artigo = $Estq_artigo->id;
                    $estq_artigo_concessionaria->id_concessionaria = $concessionarias[$i];
                    $estq_artigo_concessionaria->save();
                }

            }

            new Msg(__('Artigo atualizado com sucesso'));
            $this->go('Estq_artigo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_subgrupos', Estq_subgrupo::getList());
        $this->set('Estq_unidade', Estq_unidade::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_artigo
    # renderiza a /view/Estq_artigo/delete.php
    function delete()
    {
        $this->setTitle('Apagar Artigo');
        try {
            $this->set('Estq_artigo', new Estq_artigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_artigo
    # redireciona para Estq_artigo/all
    function post_delete()
    {
        try {
            $Estq_artigo = new Estq_artigo((int)$_POST['id']);
            $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_artigo->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_artigo->usuario_dt = date('Y-m-d H:i:s');
            $Estq_artigo->situacao = 3;
            $a = new Criteria();
            $a->addCondition('artigo', '=', $_POST['id']);
            $a->addCondition('situacao', '=', 1);
            $Estq_artigo_lotes = Estq_artigo_lote::getList($a);
            $a = new Criteria();
            $a->addCondition('artigo', '=', $_POST['id']);
            $a->addCondition('situacao', '=', 1);
            $Estq_lote_substoques = Estq_lote_substoque::getList($a);
            foreach ($Estq_artigo_lotes as $a) { //desativar os lotes tb
                if ($a->artigo == $Estq_artigo->id) {
                    $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
                    $a->usuario_id = $user; // para salvar o usuario que está fazendo
                    $a->usuario_dt = date('Y-m-d H:i:s');
                    $a->situacao = 3;
                    foreach ($Estq_lote_substoques as $b) { //desativar os lotes tb
                        if ($b->artigo == $Estq_artigo->id) {
                            $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
                            $b->usuario_id = $user; // para salvar o usuario que está fazendo
                            $b->usuario_dt = date('Y-m-d H:i:s');
                            $b->situacao = 3;
                            $b->save();
                            $a->save();
                            $Estq_artigo->save();

                        } else {

                            $a->save();
                            $Estq_artigo->save();
                        }
                    }
                } else {
                    $Estq_artigo->save();

                }
            }
            new Msg(__('Artigo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_artigo', 'all');
    }


    function artigos()
    {
        $this->setTitle('Artigos');
        $a = new Criteria();
        $a->addCondition('situacao', '<>', 3);
        $a->addCondition('origem', '=', 2);
        $a->addCondition('substoque_id', '=', Session::get('subestoque'));
        $a->setOrder('nome asc');
        $this->set('Artigos', Estq_artigo::getList($a));
    }

    function a_comprar()
    {
        $this->setTitle('Artigos a comprar');

        if(@Session::get("user_epi")->gestor_estoque == 2) {

            $artigosSubestoque = $this->query('SELECT DISTINCT(S.artigo), A.nome, S.estoque_minimo  AS minimo,S.quantidade AS disponivel
                                FROM estq_lote_substoque S
                                LEFT JOIN estq_artigo A
                                ON A.id = S.artigo
                                WHERE S.estoque_minimo > S.quantidade AND S.situacao <> 3 and S.origem = ' . Config::get('origem') . ' AND S.subestoque = ' . Session::get('subestoque') . ' ORDER BY A.nome ASC ');
            $artigos = $this->query('SELECT DISTINCT(S.id), S.nome, S.est_minimo  AS minimo,S.quantidade AS disponivel
            FROM estq_artigo S
            WHERE S.est_minimo > S.quantidade AND situacao <> 3
             and S.origem = ' . Config::get('origem') . ' ORDER BY S.nome ASC ');
        } else {
            $artigosSubestoque = $this->query('SELECT DISTINCT(S.artigo), A.nome, S.estoque_minimo  AS minimo,S.quantidade AS disponivel
                                FROM estq_lote_substoque S
                                LEFT JOIN estq_artigo A
                                ON A.id = S.artigo
                                WHERE S.estoque_minimo > S.quantidade AND S.situacao <> 3 and S.origem = ' . Config::get('origem') . ' ORDER BY A.nome ASC ');
            $artigos = $this->query('SELECT DISTINCT(S.id), S.nome, S.est_minimo  AS minimo,S.quantidade AS disponivel
                                 FROM estq_artigo S
                                 WHERE S.est_minimo > S.quantidade AND situacao <> 3
                                  and S.origem = ' . Config::get('origem') . ' ORDER BY S.nome ASC ');
        }
        $this->set('artigos', $artigosSubestoque);
    }

    function artigos_vencidos(){
        
        $queryArtigosVencidos = $this->query(' SELECT DISTINCT(al.id), artigo.nome, al.`validade`
        FROM `estq_artigo_lote` al
        LEFT JOIN `estq_artigo` artigo ON artigo.id = al.artigo
        WHERE al.situacao = 1
        AND artigo.situacao = 1
        AND (al.validade >= CURDATE() 
        AND al.validade <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)) ORDER BY artigo.nome ASC ');
        $this->set('artigosVencidos', $queryArtigosVencidos);
    }

    function concessionarias_habilitadas()
    {
        $c = new Criteria();
        $c->addCondition('id_artigo', '=', (int)$this->getParam('id'));
        $artigo_concessionarias = Estq_artigo_concessionaria::getList($c);

        $this->set('artigo_concessionarias', $artigo_concessionarias);
    }

    function artigoUsaLote()
    {

        $b = new Criteria();
        $b->addCondition('situacao', '=', 1);
        $b->addCondition('id', '=', $_POST['artigo']);
        $b->addCondition('substoque_id', '=', Session::get('subestoque'));
        $artigo = Estq_artigo::getFirst($b);
        echo $artigo->controla_lote;
        exit;
    }
}