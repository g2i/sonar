<?php
final class Estq_artigo_subestoqueController extends AppController{ 

    # página inicial do módulo Estq_artigo_subestoque
    function index(){
        $this->setTitle('Visualização de Estq_artigo_subestoque');
    }

    # lista de Estq_artigo_subestoques
    # renderiza a visão /view/Estq_artigo_subestoque/all.php
    function all(){
        $this->setTitle('Listagem de Estq_artigo_subestoque');
        $p = new Paginate('Estq_artigo_subestoque', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Estq_artigo_subestoques', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Estq_artigo_subestoque
    # renderiza a visão /view/Estq_artigo_subestoque/view.php
    function view(){
        $this->setTitle('Visualização de Estq_artigo_subestoque');
        try {
            $this->set('Estq_artigo_subestoque', new Estq_artigo_subestoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo_subestoque', 'all');
        }
    }

    # formulário de cadastro de Estq_artigo_subestoque
    # renderiza a visão /view/Estq_artigo_subestoque/add.php
    function add(){
        $this->setTitle('Cadastro de Estq_artigo_subestoque');
        $this->set('Estq_artigo_subestoque', new Estq_artigo_subestoque);
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_subestoques',  Estq_subestoque::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_artigo_subestoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo_subestoque/add.php
    function post_add(){
        $this->setTitle('Cadastro de Estq_artigo_subestoque');
        $Estq_artigo_subestoque = new Estq_artigo_subestoque();
        $this->set('Estq_artigo_subestoque', $Estq_artigo_subestoque);
        try {
            $Estq_artigo_subestoque->save($_POST);
            new Msg(__('Estq_artigo_subestoque cadastrado com sucesso'));
            $this->go('Estq_artigo_subestoque', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_subestoques',  Estq_subestoque::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Estq_artigo_subestoque
    # renderiza a visão /view/Estq_artigo_subestoque/edit.php
    function edit(){
        $this->setTitle('Edição de Estq_artigo_subestoque');
        try {
            $this->set('Estq_artigo_subestoque', new Estq_artigo_subestoque((int) $this->getParam('id')));
            $this->set('Estq_artigos',  Estq_artigo::getList());
            $this->set('Estq_subestoques',  Estq_subestoque::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_artigo_subestoque', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_artigo_subestoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo_subestoque/edit.php
    function post_edit(){
        $this->setTitle('Edição de Estq_artigo_subestoque');
        try {
            $Estq_artigo_subestoque = new Estq_artigo_subestoque((int) $_POST['id']);
            $this->set('Estq_artigo_subestoque', $Estq_artigo_subestoque);
            $Estq_artigo_subestoque->save($_POST);
            new Msg(__('Estq_artigo_subestoque atualizado com sucesso'));
            $this->go('Estq_artigo_subestoque', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_subestoques',  Estq_subestoque::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_artigo_subestoque
    # renderiza a /view/Estq_artigo_subestoque/delete.php
    function delete(){
        $this->setTitle('Apagar Estq_artigo_subestoque');
        try {
            $this->set('Estq_artigo_subestoque', new Estq_artigo_subestoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo_subestoque', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_artigo_subestoque
    # redireciona para Estq_artigo_subestoque/all
    function post_delete(){
        try {
            $Estq_artigo_subestoque = new Estq_artigo_subestoque((int) $_POST['id']);
            $Estq_artigo_subestoque->delete();
            new Msg(__('Estq_artigo_subestoque apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_artigo_subestoque', 'all');
    }

}