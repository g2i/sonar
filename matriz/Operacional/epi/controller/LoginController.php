<?php

final class LoginController extends AppController {
    
    # Nome do Model que representa a tabela que guarda os dados dos usuários:
    private $model = 'Usuario';
    # Nome do campo da tabela que armazena o LOGIN:
    private $login = 'login';
    # Nome do campo da tabela que armazena a SENHA:
    private $password = 'senha';
    # Nome do campo da tabela que armazena o E-MAIL:
    private $email = 'email';

    function index() {
        $this->go('Login', 'login');
    }

    function login() {
        if (Session::get('user_epi'))
            $this->go(Config::get('indexController'), Config::get('indexAction'));
        $this->setTitle('Login');

        $a = new Criteria();
        $a->addCondition('situacao','=',1);
        $this->set('subestoque',Estq_subestoque::getList($a));
        $this->set('user', $this->model);
    }

    function post_login() {
        $this->setTitle('Login');
        $c = new Criteria();
        $c->addCondition($this->login, '=', $_POST['login']);
        $c->addCondition($this->password, '=', md5(Config::get('salt').$_POST['password']));
        $model = $this->model;
        $this->set('user', $this->model);
        $user = $model::getFirst($c);
        if ($user) {

            //Filtra o usuario por estq_usuario_substoque
            $g = new Criteria();
            $g->addCondition('estq_usuario_substoque.usario_id','=', $user->id);
            $g->addCondition('estq_usuario_substoque.substoque_id','=',$_POST['subestoque']);
            $Estq_usuario_substoque = Estq_usuario_substoque::getFirst($g);
            if(!$Estq_usuario_substoque){
                new Msg('Usuario não autorizado neste modulo.', 2);
                $this->go('login', 'login');
            }


            $d = new Criteria();
            #Epi cod. 4
            $d->addCondition('usuario_modulo.modulo_id', '=', 4);
            $d->addCondition('usuario_modulo.usuario_id', '=', $user->id);
            $Usuario_modulo = Usuario_modulo::getFirst($d);

            if (!$Usuario_modulo) {
                new Msg('Usuario não autorizado neste modulo.', 2);
                $this->go('login', 'login');
            }

            //new Msg('Bem vindo ' . $_POST['login']);
            Session::set('user_epi', $user);

            if($user->primeiro_acesso == 1) {
                new Msg('Bem Vindo! Para continuar altere sua senha');
                $this->go('Usuario', 'resetar_senha');
            }
            Session::set('subestoque',$_POST['subestoque']);
            $subestoque = new Estq_subestoque(Session::get('subestoque'));
            new Msg('Login bem sucedido! Subestoque: ' . $subestoque->nome);
            $this->go(Config::get('indexController'), Config::get('indexAction')); # Ao autenticar, redireciona para...




        } else {
            new Msg('Login ou senha incorretos. Por favor, tente novamente.', 3);
        }
    }

    function logout() {
        Session::set('user_epi', NULL);
        Session::set('subestoque',NULL);
        $this->go('Login', 'login');
    }

    function send() {
        $this->setTitle('Recuperar senha');
    }

    function post_send() {
        $this->setTitle('Recuperar senha');
        $c = new Criteria();
        $c->addCondition($this->email, '=', $_POST['email']);
        $model = $this->model;
        $user = $model::getFirst($c);
        if ($user) {
            $d = new DateTime();
            $agora = $d->format('Ymdhi');
            # email:
            $headers = "From: nao-responder@" . $_SERVER['HTTP_HOST'] . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $subject = "Recuperação de senha em ".$_SERVER['HTTP_HOST'];
            $message = "Olá,<p>Alguém (provavelmente você) pediu para mudar a senha da sua conta em ";
            $message .= $_SERVER['HTTP_HOST'] . ".</p>";
            $message .= "<p>Para confirmar este pedido e cadastrar uma nova senha, vá ao seguinte endereço web: ";
            $message .= "<a href='" . $_SERVER['HTTP_HOST'] . "/" . SITE_PATH . "/index.php?m=Login&p=reset&recuperar=" . Cript::cript(Config::get('salt') . $user->{$model::PK}) . "&d=".urlencode(Cript::cript($agora))."'>Gerar uma nova senha</a></p>";
            mail($_POST['email'], $subject, $message, $headers);
            new Msg('Um e-mail foi enviado para ' . $_POST['email'] . ' com as instruções. <br>Caso não tenha recebido, verifique sua caixa de spam e tente novamente.');
            $this->go('Login', 'login');
        } else {
            new Msg('E-mail não cadastrado!', 3);
        }
    }

    function reset() {
        $id = (int)str_replace(Config::get('salt'), '', Cript::decript($_GET['recuperar']));
        try {
            $d = new DateTime();
            $agora = $d->format('Ymdhi');
            $data = (int)str_replace(Config::get('salt'), '', urldecode(Cript::decript($_GET['d'])));
            #meia hora de validade do email
            if(($agora - $data) > 30){
                throw new Exception('<p>Este link expirou. Solicite novamente sua senha.</p>');
                $this->go('Login', 'login');
            }     
            $model = $this->model;
            $user = new $model($id);
            $senha = $this->gerarSenha();
            $user->{$this->password} = md5(Config::get('salt').$senha);
            $user->save();
            
            #envia email com a nova senha
            $headers = "From: nao-responder@" . $_SERVER['HTTP_HOST'] . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $subject = "Nova senha em ".$_SERVER['HTTP_HOST'];
            $message = "Olá de novo,<p>você pediu para mudar a senha da sua conta em ";
            $message .= $_SERVER['HTTP_HOST'] . ".</p>";
            $message .= "<p>Sua nova senha é:<strong>$senha</strong></p>";
            mail($user->{$this->email}, $subject, $message, $headers);
            
            new Msg('Uma nova senha foi gerada e enviada para o seu email!');
            $this->go('Login', 'login');
            
        } catch (Exception $exc) {
            new Msg($exc->getMessage(), 3);
             $this->go('Login', 'login');
        }
    }

    public function gerarSenha($length = 8) {
        $salt = "abcdefghijklmnpqrstuvwxyz123456789";
        $len = strlen($salt);
        $pass = '';
        mt_srand(10000000 * (double) microtime());
        for ($i = 0; $i < $length; $i++) {
            $pass .= $salt[mt_rand(0, $len - 1)];
        }
        return $pass;
    }

}