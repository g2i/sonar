<?php

final class Estq_artigocaracteristicasController extends AppController
{

    # página inicial do módulo Estq_artigocaracteristicas
    function index()
    {
        $this->setTitle('Visualização de Característica - Artigo');
    }

    # lista de Estq_artigocaracteristicas
    # renderiza a visão /view/Estq_artigocaracteristicas/all.php
    function all()
    {
        $this->setTitle('Listagem de Artigo Caracteristicas');
        $p = new Paginate('Estq_artigocaracteristicas', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('origem', '=', Config::get('origem')); //filtro
        $c->addCondition('artigo', "=", $this->getParam('id'));


        $this->set('Estq_artigocaracteristicas', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList());
        $this->set('Stq_caracteristicas', Stq_caracteristica::getList());


    }

    # visualiza um(a) Estq_artigocaracteristicas
    # renderiza a visão /view/Estq_artigocaracteristicas/view.php
    function view()
    {
        $this->setTitle('Visualização de Característica - Artigo');
        try {
            $this->set('Estq_artigocaracteristicas', new Estq_artigocaracteristicas((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigocaracteristicas', 'all');
        }
    }

    # formulário de cadastro de Estq_artigocaracteristicas
    # renderiza a visão /view/Estq_artigocaracteristicas/add.php
    function add()
    {
        $this->setTitle('Cadastro de Característica - Artigo');
        $this->set('Estq_artigocaracteristicas', new Estq_artigocaracteristicas);
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList());
        $c = new Criteria();
        $c->addCondition('origem','=',Config::get('origem')); //filtro
        $this->set('Stq_caracteristicas', Stq_caracteristica::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Estq_artigocaracteristicas
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigocaracteristicas/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Característica - Artigo');
        $Estq_artigocaracteristicas = new Estq_artigocaracteristicas();
        $this->set('Estq_artigocaracteristicas', $Estq_artigocaracteristicas);
        $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_artigocaracteristicas->usuario_id=$user; // para salvar o usuario que está fazendo
        $Estq_artigocaracteristicas->usuario_dt = date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['origem']=Config::get('origem');//salvando a origem
        try {
            $Estq_artigocaracteristicas->save($_POST);
            new Msg(__('Característica - Artigo cadastrado com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            //TERMINA AQUI

            $this->go('Estq_artigocaracteristicas', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList());
        $this->set('Stq_caracteristicas', Stq_caracteristica::getList());
    }

    # formulário de edição de Estq_artigocaracteristicas
    # renderiza a visão /view/Estq_artigocaracteristicas/edit.php
    function edit()
    {
        $this->setTitle('Edição de Característica - Artigo');
        try {
            $this->set('Estq_artigocaracteristicas', new Estq_artigocaracteristicas((int)$this->getParam('id')));
            $this->set('Estq_situacaos', Estq_situacao::getList());
            $this->set('Estq_artigos', Estq_artigo::getList());
            $c = new Criteria();
            $c->addCondition('origem','=',Config::get('origem')); //filtro
            $this->set('Stq_caracteristicas', Stq_caracteristica::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_artigocaracteristicas', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_artigocaracteristicas
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigocaracteristicas/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Característica - Artigo');
        try {
            $Estq_artigocaracteristicas = new Estq_artigocaracteristicas((int)$_POST['id']);
            $this->set('Estq_artigocaracteristicas', $Estq_artigocaracteristicas);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_artigocaracteristicas->usuario_id=$user; // para salvar o usuario que está fazendo
            $_POST['usuario_dt'] = date('Y-m-d H:i:s');
            $Estq_artigocaracteristicas->save($_POST);
            new Msg(__('Característica - Artigo atualizado com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Estq_artigocaracteristicas', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList());
        $this->set('Stq_caracteristicas', Stq_caracteristica::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_artigocaracteristicas
    # renderiza a /view/Estq_artigocaracteristicas/delete.php
    function delete()
    {
        $this->setTitle('Apagar Característica - Artigo');
        try {
            $this->set('Estq_artigocaracteristicas', new Estq_artigocaracteristicas((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigocaracteristicas', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_artigocaracteristicas
    # redireciona para Estq_artigocaracteristicas/all
    function post_delete()
    {
        try {
            $Estq_artigocaracteristicas = new Estq_artigocaracteristicas((int)$_POST['id']);
            $Estq_artigocaracteristicas->usuario_dt = date('Y-m-d H:i:s');
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_artigocaracteristicas->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_artigocaracteristicas->situacao = 3;
            $Estq_artigocaracteristicas->save();
            new Msg(__('Característica - Artigo apagado com sucesso'), 1);
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_artigocaracteristicas', 'all');
    }

}