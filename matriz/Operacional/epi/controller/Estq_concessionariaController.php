<?php

final class Estq_concessionariaController extends AppController
{

    # página inicial do módulo Estq_concessionaria
    function index()
    {
        $this->setTitle('Visualização de Estq_concessionaria');
    }

    # lista de Estq_concessionarias
    # renderiza a visão /view/Estq_concessionaria/all.php
    function all()
    {
        $this->setTitle('Listagem de Estq_concessionaria');
        $p = new Paginate('Estq_concessionaria', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('status', '=', 1);
        $this->set('Estq_concessionarias', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Estq_concessionaria
    # renderiza a visão /view/Estq_concessionaria/view.php
    function view()
    {
        $this->setTitle('Visualização de Estq_concessionaria');
        try {
            $this->set('Estq_concessionaria', new Estq_concessionaria((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_concessionaria', 'all');
        }
    }

    # formulário de cadastro de Estq_concessionaria
    # renderiza a visão /view/Estq_concessionaria/add.php
    function add()
    {
        $this->setTitle('Cadastro de Estq_concessionaria');
        $this->set('Estq_concessionaria', new Estq_concessionaria);
    }

    # recebe os dados enviados via post do cadastro de Estq_concessionaria
    # (true)redireciona ou (false) renderiza a visão /view/Estq_concessionaria/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Estq_concessionaria');
        $Estq_concessionaria = new Estq_concessionaria();
        $this->set('Estq_concessionaria', $Estq_concessionaria);
        $Estq_concessionaria->status = 1;
        try {
            $Estq_concessionaria->save($_POST);
            new Msg(__('Estq_concessionaria cadastrado com sucesso'));
            $this->go('Estq_concessionaria', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # formulário de edição de Estq_concessionaria
    # renderiza a visão /view/Estq_concessionaria/edit.php
    function edit()
    {
        $this->setTitle('Edição de Estq_concessionaria');
        try {
            $this->set('Estq_concessionaria', new Estq_concessionaria((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_concessionaria', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_concessionaria
    # (true)redireciona ou (false) renderiza a visão /view/Estq_concessionaria/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Estq_concessionaria');
        try {
            $Estq_concessionaria = new Estq_concessionaria((int)$_POST['id']);
            $this->set('Estq_concessionaria', $Estq_concessionaria);
            $Estq_concessionaria->save($_POST);
            new Msg(__('Estq_concessionaria atualizado com sucesso'));
            $this->go('Estq_concessionaria', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Estq_concessionaria
    # renderiza a /view/Estq_concessionaria/delete.php
    function delete()
    {
        $this->setTitle('Apagar Estq_concessionaria');
        try {
            $this->set('Estq_concessionaria', new Estq_concessionaria((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_concessionaria', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_concessionaria
    # redireciona para Estq_concessionaria/all
    function post_delete()
    {
        try {
            $Estq_concessionaria = new Estq_concessionaria((int)$_POST['id']);
            $Estq_concessionaria->status = 3;
            $Estq_concessionaria->save();
            new Msg(__('Estq_concessionaria apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_concessionaria', 'all');
    }

}