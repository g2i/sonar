<?php

final class Estq_movdetalhesController extends AppController
{

    # página inicial do módulo Estq_movdetalhes
    function index()
    {
        $this->setTitle('Visualização de Detalhes Movimento');
    }

    # lista de Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/all.php
    function all()
    {
        $this->setTitle('Listagem de Detalhes Movimento');
        $p = new Paginate('Estq_movdetalhes', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->setOrder('artigo');


        //pega o id do profissional para listar
        if ($this->getParam('id')) {
            $c->addCondition('movimento', '=', $this->getParam('id'));
            $this->set('Estq_mov', new Estq_mov($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        } else {
            $this->set('Estq_movs', Estq_mov::getList());
        }

        $c->addCondition('situacao', '<>', '3'); //filtro Origem
        $c->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $this->set('Estq_movdetalhes', $p->getPage($c));
        $this->set('nav', $p->getNav());

    }

    # visualiza um(a) Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/view.php
    function view()
    {
        $this->setTitle('Visualização de Detalhes Movimento');
        try {
            $this->set('Estq_movdetalhes', new Estq_movdetalhes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_movdetalhes', 'all');
        }
    }

    # formulário de cadastro de Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/add.php
    function add()
    {
        $this->setTitle('Cadastro de Detalhes Movimento');
        $this->set('Estq_movdetalhes', new Estq_movdetalhes);
        $b = new Criteria();
        $b->addCondition('origem', '=', Config::get('origem'));
        $b->addCondition('situacao', '<>', 3);
        $b->setOrder('nome');
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList($b));
        $this->set('Estq_movs', Estq_mov::getList());
        $this->set('Estq_lote_substoques', Estq_lote_substoque::getList());

        $this->set('operacao', null);
        if ($this->getParam('id')) {
            $movimento = new Estq_mov((int)$this->getParam('id'));
            $tipo = $movimento->getEstq_tipomov();
            $this->set('operacao', $tipo->operacao);
            $this->set('movimento', $movimento);
        }
    }

    function GetLoteSubestoque()
    {

        $movimento = new Estq_mov((int)$_POST['movimento']);
        $tipo = $movimento->getEstq_tipomov();
        $a = new Criteria();
        $a->addCondition('artigo', '=', $_POST['artigo']);
        $a->addCondition('origem', '=', Config::get('origem'));
        $a->addCondition('situacao', '<>', 3);
        $a->addCondition('estq_subestoque.situacao', '<>', 3);
        $a->addCondition('estq_subestoque.origem', '=', Config::get('origem'));
        /* if($tipo->operacao=='-')
             $a->addCondition('quantidade','>',0);*/
        $lote_substoque = Estq_lote_substoque::getList($a);
        $html = "";
        $html .= "<option value=''>Selecione</option>";
        foreach ($lote_substoque as $u) {
            $html .= "<option value='" . $u->id . "'>" . $u->getEstq_artigo_lote()->nome . " - " . $u->getEstq_subestoque()->nome . "</option>";
        }

        echo $html;
        exit;
    }

    function GetLoteSubestoque2()
    {

        $artigo = new Estq_artigo($_POST['artigo']);

        $a = new Criteria();
        $a->addCondition('artigo', '=', $_POST['artigo']);
        $a->addCondition('origem', '=', Config::get('origem'));
        $a->addCondition('situacao', '<>', 3);
        $a->addCondition('estq_subestoque.situacao', '<>', 3);
        $a->addCondition('estq_subestoque.origem', '=', Config::get('origem'));
        $lote_substoque = Estq_lote_substoque::getList($a);
        $html = "";

        $html .= "<option value=''>Selecione</option>";
        foreach ($lote_substoque as $u) {
            $html .= "<option value='" . $u->id . "'>" . $u->getEstq_artigo_lote()->nome . " - " . $u->getEstq_subestoque()->nome . "</option>";
        }

//        if (count($lote_substoque) <= 1 && $artigo->controla_lote == 0) {
//            foreach ($lote_substoque as $u) {
//                $html .= "<option value='" . $u->id . "'>" . $u->getEstq_artigo_lote()->nome . " - " . $u->getEstq_subestoque()->nome . "</option>";
//            }
//        } else {
//            $html .= "<option value=''>Selecione</option>";
//            foreach ($lote_substoque as $u) {
//                $html .= "<option value='" . $u->id . "'>" . $u->getEstq_artigo_lote()->nome . " - " . $u->getEstq_subestoque()->nome . "</option>";
//            }
//        }

        echo $html;

        exit;
    }

    function getLoteSubestoqueId()
    {
        $a = new Criteria();
        $a->addCondition('artigo', '=', $_POST['artigo']);
        $a->addCondition('origem', '=', Config::get('origem'));
        $a->addCondition('situacao', '<>', 3);
        $a->addCondition('estq_subestoque.situacao', '<>', 3);
        $a->addCondition('estq_subestoque.origem', '=', Config::get('origem'));
        $lote_substoque = Estq_lote_substoque::getList($a);

        echo $lote_substoque[0]->id;
        exit;
    }

    function GetLoteSubestoqueCautela()
    {

        $a = new Criteria();
        $a->addCondition('artigo', '=', $_POST['artigo']);
        $a->addCondition('origem', '=', Config::get('origem'));
        $a->addCondition('situacao', '<>', 3);
        $a->addCondition('estq_subestoque.situacao', '<>', 3);
        $a->addCondition('estq_subestoque.origem', '=', Config::get('origem'));
        $lote_substoque = Estq_lote_substoque::getList($a);
        $html = "";
        $html .= "<option value=''>Selecione</option>";
        foreach ($lote_substoque as $u) {
            $html .= "<option value='" . $u->id . "'>" . $u->getEstq_artigo_lote()->nome . " - " . $u->getEstq_subestoque()->nome . "</option>";
        }

        //echo $html;

        echo $lote_substoque[0]->id;
        exit;
    }


    function GetSubestoque_padrao()
    {

        $a = new Criteria();
        $a->addCondition('artigo', '=', $_POST['artigo']);
        $a->addCondition('origem', '=', Config::get('origem'));
        $a->addCondition('subestoque', '=', $_POST['sub_padrao']);
        $a->addCondition('situacao', '<>', 3);

        $lote_substoque = Estq_lote_substoque::getFirst($a);
        if ($lote_substoque) {
            echo json_encode($lote_substoque);
        } else {
            echo 2;
        }
        exit;
    }

    function getQuantidade()
    {
        $lote_subestoque = new Estq_lote_substoque((int)$_POST['id']);
        echo json_encode($lote_subestoque);
        exit;
    }

    function GetCodigo()
    {
        $b = new Criteria();
        $b->addCondition('situacao', '=', 1);
        $b->addCondition('id', '=', $_POST['artigo']);
        $artigo = Estq_artigo::getList($b);
        $html = "";
        $html .= "<option value=''>Selecione</option>";
        foreach ($artigo as $t) {
            $html .= "<option selected value='" . $t->id . "'>" . $t->codigo_livre . "</option>";
        }
        echo $html;
        exit;
    }

    function GetArtigo()
    {
        $b = new Criteria();
        $b->addCondition('situacao', '=', 1);
        $b->addCondition('origem', '=', Config::get('origem'));
        $b->addCondition('id', '=', $_POST['artigo']);
        $artigo = Estq_artigo::getList($b);
        $html = "";
        $html .= "<option value=''>Selecione</option>";
        foreach ($artigo as $t) {
            $html .= "<option selected value='" . $t->id . "'>" . $t->nome . "</option>";
        }
        echo $html;
        exit;
    }

    function listar()
    {
        $aux = Config::get('origem');
        if ($_POST['data']['q'] != "") {
            $arr = $this->query("SELECT `id`,`codigo_livre` FROM `estq_artigo` WHERE (codigo_livre LIKE '" . utf8_decode(addslashes($_POST['data']['q'])) . "%') AND situacao <> 3 AND origem = " . $aux . "; ");
            $results = array();
            foreach ($arr as $cli) {
                if (!empty($cli->id) && !empty($cli->codigo_livre))
                    $results[] = array(
                        'id' => $cli->id,
                        'text' => $this->removerAcentos(utf8_encode($cli->codigo_livre))
                    );
            }
            echo json_encode(array('q' => $_POST['data']['q'], 'results' => $results));
            exit;
        }
        echo json_encode(array('q' => "", 'results' => array()));
        exit;

    }

    function listar2()
    {
        $aux = Config::get('origem');
        if ($_POST['data']['q'] != "") {
            $arr = $this->query("SELECT `id`,`nome` FROM `estq_artigo`
             WHERE (nome LIKE '" . utf8_decode(addslashes($_POST['data']['q'])) . "%') 
             AND situacao <> 3 AND origem = " . $aux . "
             AND substoque_id = " . Session::get('subestoque') . "; ");

            $results = array();
            foreach ($arr as $cli) {
                if (!empty($cli->id) && !empty($cli->nome))
                    $results[] = array(
                        'id' => $cli->id,
                        'text' => $this->removerAcentos(utf8_encode($cli->nome))
                    );
            }
            echo json_encode(array('q' => $_POST['data']['q'], 'results' => $results));
            exit;
        }
        echo json_encode(array('q' => "", 'results' => array()));
        exit;

    }

    function removerAcentos($texto)
    {
        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
        , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç");
        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
        , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C");
        return str_replace($array1, $array2, $texto);
    }

    # recebe os dados enviados via post do cadastro de Estq_movdetalhes
    # (true)redireciona ou (false) renderiza a visão /view/Estq_movdetalhes/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Detalhes Movimento');
        $Estq_movdetalhes = new Estq_movdetalhes();
        $this->set('Estq_movdetalhes', $Estq_movdetalhes);
        $_POST['usuario_dt'] = date('Y-m-d H:i:s');
        $_POST['origem'] = Config::get('origem');//salvando a origem
        try {
            if ($_POST['validade'] != NULL) {
                $_POST['validade'] = DataSQL($_POST['validade']); //converte para o formato americano
            }
            if (!empty($_POST['vlnf'])) {
                $_POST['vlnf'] = str_replace(".", "", $_POST['vlnf']);
                $_POST['vlnf'] = str_replace(",", ".", $_POST['vlnf']);
            }
            if (!empty($_POST['vlcusto'])) {
                $_POST['vlcusto'] = str_replace(".", "", $_POST['vlcusto']);
                $_POST['vlcusto'] = str_replace(",", ".", $_POST['vlcusto']);
            }
            if (!empty($_POST['vlcustomedio'])) {
                $_POST['vlcustomedio'] = str_replace(".", "", $_POST['vlcustomedio']);
                $_POST['vlcustomedio'] = str_replace(",", ".", $_POST['vlcustomedio']);
            }
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_movdetalhes->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_movdetalhes->save($_POST);
            new Msg(__('Detalhes-Movimento cadastrado com sucesso'));
            //NAVEGAÇÃO ENTRE MODAl
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Estq_movdetalhes', 'all', array('id' => $Estq_movdetalhes->movimento));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $c = new Criteria();
        $c->addCondition('origem', '=', Config::get('origem'));
        $this->set('Estq_artigos', Estq_artigo::getList($c));
        $this->set('Estq_movs', Estq_mov::getList());
        $this->set('Estq_lote_substoques', Estq_lote_substoque::getList());
    }

    # formulário de edição de Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/edit.php
    function edit()
    {
        $this->setTitle('Edição de Detalhes Movimento');
        try {
            $movimento_detalhes = new Estq_movdetalhes((int)$this->getParam('id'));
            $this->set('Estq_movdetalhes', $movimento_detalhes);
            $this->set('Estq_situacaos', Estq_situacao::getList());
            $artigo = new Estq_artigo($movimento_detalhes->artigo);
            $this->set('artigo', $artigo);
            $movimento = new Estq_mov($movimento_detalhes->movimento);
            $this->set('movimento', $movimento);

            $lote_substoque = new Estq_lote_substoque($movimento_detalhes->lote_substoque);
            $this->set('lote_substoque', $lote_substoque);

        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_movdetalhes', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_movdetalhes
    # (true)redireciona ou (false) renderiza a visão /view/Estq_movdetalhes/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Detalhes Movimento');
        try {
            $Estq_movdetalhes = new Estq_movdetalhes((int)$_POST['id']);
            if ($_POST['validade'] != NULL) {
                $_POST['validade'] = DataSQL($_POST['validade']); //converte para o formato americano
            }

            if (!empty($_POST['vlnf'])) {
                $_POST['vlnf'] = str_replace(".", "", $_POST['vlnf']);
                $_POST['vlnf'] = str_replace(",", ".", $_POST['vlnf']);
            }
            if (!empty($_POST['vlcusto'])) {
                $_POST['vlcusto'] = str_replace(".", "", $_POST['vlcusto']);
                $_POST['vlcusto'] = str_replace(",", ".", $_POST['vlcusto']);
            }
            if (!empty($_POST['vlcustomedio'])) {
                $_POST['vlcustomedio'] = str_replace(".", "", $_POST['vlcustomedio']);
                $_POST['vlcustomedio'] = str_replace(",", ".", $_POST['vlcustomedio']);
            }

            $this->set('Estq_movdetalhes', $Estq_movdetalhes);
            $Estq_movdetalhes->usuario_dt = date('Y-m-d H:i:s');
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_movdetalhes->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_movdetalhes->save($_POST);
            new Msg(__('Detalhes-Movimento atualizado com sucesso'));
            //NAVEGAÇÃO ENTRE MODAl
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Estq_movdetalhes', 'all', array('id' => $Estq_movdetalhes->movimento));
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }

    }

    # Confirma a exclusão ou não de um(a) Estq_movdetalhes
    # renderiza a /view/Estq_movdetalhes/delete.php
    function delete()
    {
        $this->setTitle('Apagar Detalhes-Movimento');
        try {
            $this->set('Estq_movdetalhes', new Estq_movdetalhes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_movdetalhes', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_movdetalhes
    # redireciona para Estq_movdetalhes/all
    function post_delete()
    {
        try {
            $Estq_movdetalhes = new Estq_movdetalhes((int)$_POST['id']);
            $Estq_movdetalhes->usuario_dt = date('Y-m-d H:i:s');
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_movdetalhes->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_movdetalhes->situacao = 3;
            $Estq_movdetalhes->save();
            new Msg(__('Detalhes-Movimento apagado com sucesso'), 1);
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_movdetalhes', 'all', array('id' => $Estq_movdetalhes->movimento));
    }


    function list_art()
    {
        if (!isset($_SESSION['itens'])) $_SESSION['itens'] = array();
        $add = false;
        $quant = 0;
        $id = 0;
        if (!empty($_GET['artigo']) && !empty($_GET['lote_id'])) {
            foreach ($_SESSION['itens'] as $i => $item) {
                if ($_GET['artigo'] == $item['artigo_id']) {
                    $add = true;
                    $quant = $item['quantidade'];
                    $id = $i;
                }
            }
            if (!$add) {
                $artigo = new Estq_artigo($_GET['artigo']);
                $lote_sub = new Estq_lote_substoque($_GET['lote_id']);


                $_SESSION['itens'][] = array(
                    'artigo_id' => $artigo->id,
                    'artigo' => $artigo->nome,
                    'lote_id' => $lote_sub->id,
                    'lote_subestoque' => $lote_sub->getEstq_artigo_lote()->nome . " - " . $lote_sub->getEstq_subestoque()->nome,
                    'codigo' => $artigo->codigo_livre,
                    'quantidade' => $_GET['quantidade']
                );
            } else {
                if ($quant > 0 && $_GET['quantidade'] > $quant) {
                    $_SESSION['itens'][$id]['quantidade'] = $_GET['quantidade'];
                }
            }
        }
        $this->set('ret', $_SESSION['itens']);
    }

    function list_art_cautela()
    {
        if (!isset($_SESSION['itens'])) $_SESSION['itens'] = array();
        $add = false;
        $quant = 0;
        $id = 0;
        if (!empty($_GET['artigo']) && !empty($_GET['lote_id'])) {
            foreach ($_SESSION['itens'] as $i => $item) {
                if ($_GET['artigo'] == $item['artigo_id']) {
                    $add = true;
                    $quant = $item['quantidade'];
                    $id = $i;
                }
            }
            if (!$add) {
                $artigo = new Estq_artigo($_GET['artigo']);
                $lote_sub = new Estq_lote_substoque($_GET['lote_id']);


                $_SESSION['itens'][] = array(
                    'artigo_id' => $artigo->id,
                    'artigo' => $artigo->nome,
                    'lote_id' => $lote_sub->id,
                    'lote_subestoque' => $lote_sub->getEstq_artigo_lote()->nome . " - " . $lote_sub->getEstq_subestoque()->nome,
                    'codigo' => $artigo->codigo_livre,
                    'marca' => $_GET['marca'],
                    'validade' => $_GET['validade'],
                    'novo_usado' => $_GET['novo_usado'],
                    'quantidade' => $_GET['quantidade']
                );
            } else {
                if ($quant > 0 && $_GET['quantidade'] > $quant) {
                    $_SESSION['itens'][$id]['quantidade'] = $_GET['quantidade'];
                }
            }
        }
        $this->set('ret', $_SESSION['itens']);
    }

    function remove_item()
    {
        unset($_SESSION['itens'][$_GET['pos']]);
        exit;
    }

    function count_itens()
    {
        echo count($_SESSION['itens']);
        exit;
    }

}