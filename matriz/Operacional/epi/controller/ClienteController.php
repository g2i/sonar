<?php
final class ClienteController extends AppController{ 

    # página inicial do módulo Cliente
    function index(){
        $this->setTitle('Visualização de Cliente');
    }

    # lista de Clientes
    # renderiza a visão /view/Cliente/all.php
    function all(){
        $this->setTitle('Listagem de Clientes');
        $p = new Paginate('Cliente', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Clientes', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Status',  Status::getList());
    

    }

    # visualiza um(a) Cliente
    # renderiza a visão /view/Cliente/view.php
    function view(){
        $this->setTitle('Visualização de Cliente');
        try {
            $this->set('Cliente', new Cliente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cliente', 'all');
        }
    }

    # formulário de cadastro de Cliente
    # renderiza a visão /view/Cliente/add.php
    function add(){
        $this->setTitle('Cadastro de Cliente');
        $this->set('Cliente', new Cliente);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Cliente
    # (true)redireciona ou (false) renderiza a visão /view/Cliente/add.php
    function post_add(){
        $this->setTitle('Cadastro de Cliente');
        $Cliente = new Cliente();
        $this->set('Cliente', $Cliente);
        try {
            $Cliente->save($_POST);
            new Msg(__('Cliente cadastrado com sucesso'));
            $this->go('Cliente', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Cliente
    # renderiza a visão /view/Cliente/edit.php
    function edit(){
        $this->setTitle('Edição de Cliente');
        try {
            $this->set('Cliente', new Cliente((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Cliente', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Cliente
    # (true)redireciona ou (false) renderiza a visão /view/Cliente/edit.php
    function post_edit(){
        $this->setTitle('Edição de Cliente');
        try {
            $Cliente = new Cliente((int) $_POST['id']);
            $this->set('Cliente', $Cliente);
            $Cliente->save($_POST);
            new Msg(__('Cliente atualizado com sucesso'));
            $this->go('Cliente', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Confirma a exclusão ou não de um(a) Cliente
    # renderiza a /view/Cliente/delete.php
    function delete(){
        $this->setTitle('Apagar Cliente');
        try {
            $this->set('Cliente', new Cliente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cliente', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Cliente
    # redireciona para Cliente/all
    function post_delete(){
        try {
            $Cliente = new Cliente((int) $_POST['id']);
            $Cliente->delete();
            new Msg(__('Cliente apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Cliente', 'all');
    }

}