<?php

final class Estq_lote_substoqueController extends AppController
{

    # página inicial do módulo Estq_lote_substoque
    function index()
    {
        $this->setTitle('Visualização de Lote Sub-Estoque');
    }

    # lista de Estq_lote_substoques
    # renderiza a visão /view/Estq_lote_substoque/all.php
    function all()
    {
        $this->setTitle('Listagem do Lote Subestoque');
        $p = new Paginate('Estq_lote_substoque', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('artigo', "=", $this->getParam('id'));
        $c->addCondition('origem', '=', Config::get('origem')); //filtro ORIGEM
        $this->set('Estq_lote_substoques', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Estq_mov', Estq_mov::getList());


    }

    # visualiza um(a) Estq_lote_substoque
    # renderiza a visão /view/Estq_lote_substoque/view.php
    function view()
    {
        $this->setTitle('Visualização de Lote Subestoque');
        try {
            $this->set('Estq_lote_substoque', new Estq_lote_substoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_lote_substoque', 'all');
        }
    }

    # formulário de cadastro de Estq_lote_substoque
    # renderiza a visão /view/Estq_lote_substoque/add.php
    function add()
    {
        $this->setTitle('Cadastro de Lote Subestoque');
        $this->set('Estq_lote_substoque', new Estq_lote_substoque);
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList());
        if ($this->getParam('id')) {
            $c = new Criteria();
            $c->addCondition('artigo', '=', $this->getParam('id'));
            $c->addCondition('origem', '=', Config::get('origem'));
            $this->set('Estq_artigo_lotes', Estq_artigo_lote::getList($c));
        } else {
            $this->set('Estq_artigo_lotes', Estq_artigo_lote::getList());
        }
        $c = new Criteria();
        $c->addCondition('origem', '=', Config::get('origem')); //filtro
        $this->set('Estq_subestoques', Estq_subestoque::getList($c));


    }

    # recebe os dados enviados via post do cadastro de Estq_lote_substoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_lote_substoque/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Lote Subestoque');
        $Estq_lote_substoque = new Estq_lote_substoque();
        $this->set('Estq_lote_substoque', $Estq_lote_substoque);
        $_POST['origem'] = Config::get('origem');//salvando a origem
        $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
        $Estq_lote_substoque->usuario_id = $user; // para salvar o usuario que está fazendo
        try {
            $Estq_lote_substoque->save($_POST);
            new Msg(__('Lote Subestoque cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Estq_lote_substoque', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList());
        $this->set('Estq_subestoques', Estq_subestoque::getList());
        if ($this->getParam('id')) {
            $c = new Criteria();
            $c->addCondition('artigo', '=', $this->getParam('id'));
            $this->set('Estq_artigo_lotes', Estq_artigo_lote::getList($c));
        } else {
            $this->set('Estq_artigo_lotes', Estq_artigo_lote::getList());
        }
        $this->set('Estq_subestoques', Estq_subestoque::getList());
    }

    # formulário de edição de Estq_lote_substoque
    # renderiza a visão /view/Estq_lote_substoque/edit.php
    function edit()
    {
        $this->setTitle('Edição de Lote Subestoque');
        try {
            $this->set('Estq_lote_substoque', new Estq_lote_substoque((int)$this->getParam('id')));
            $this->set('Estq_situacaos', Estq_situacao::getList());
            $this->set('Estq_artigos', Estq_artigo::getList());
            $this->set('Estq_artigo_lotes', Estq_artigo_lote::getList());
            $c = new Criteria();
            $c->addCondition('origem', '=', Config::get('origem')); //filtro
            $this->set('Estq_subestoques', Estq_subestoque::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_lote_substoque', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_lote_substoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_lote_substoque/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Lote Subestoque');
        try {
            $Estq_lote_substoque = new Estq_lote_substoque((int)$_POST['id']);
            $this->set('Estq_lote_substoque', $Estq_lote_substoque);
            $Estq_lote_substoque->usuario_dt = date('Y-m-d H:i:s');
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_lote_substoque->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_lote_substoque->save($_POST);
            new Msg(__('Lote Subestoque atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Estq_lote_substoque', 'all');


        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_artigos', Estq_artigo::getList());
        $this->set('Estq_artigo_lotes', Estq_artigo_lote::getList());
        $this->set('Estq_subestoques', Estq_subestoque::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_lote_substoque
    # renderiza a /view/Estq_lote_substoque/delete.php
    function delete()
    {
        $this->setTitle('Apagar Lote Subestoque');
        try {
            $this->set('Estq_lote_substoque', new Estq_lote_substoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_lote_substoque', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_lote_substoque
    # redireciona para Estq_lote_substoque/all
    function post_delete()
    {
        try {
            $Estq_lote_substoque = new Estq_lote_substoque((int)$_POST['id']);
            $Estq_lote_substoque->usuario_dt = date('Y-m-d H:i:s');
            $user = @Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Estq_lote_substoque->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_lote_substoque->situacao = 3;
            $Estq_lote_substoque->save();
            new Msg(__('Lote Subestoque apagado com sucesso'), 1);
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_lote_substoque', 'all');
    }

    function get_dados()
    {


        $c = new Criteria();
        $c->addCondition('artigo', "=", $_POST['id']);
        $c->addCondition('origem', '=', Config::get('origem')); //filtro
        $Estq = Estq_lote_substoque::getList($c);

        $html = "";
        $html .= '<li>';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="col-md-3">';
        $html .= '<strong>Sub-estoque</strong>';
        $html .= '</div>';
        $html .= '<div class="col-md-3">';
        $html .= '<strong>Lote</strong>';
        $html .= '</div>';
        $html .= '<div class="col-md-3">';
        $html .= '<strong>Estoque Mínimo</strong>';
        $html .= '</div>';
        $html .= '<div class="col-md-3">';
        $html .= '<strong>Quantidade</strong>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</li>';

        foreach ($Estq as $i) {
            $es = new Estq_lote_substoque($i->id);
            $html .= '<li>';
            $html .= '<div class="col-md-12">';
            $html .= '<div class="col-md-3">';
            $html .= $i->getEstq_subestoque()->nome;
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .= $i->getEstq_artigo_lote()->nome;
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .= $i->estoque_minimo;
            $html .= '</div>';
            $html .= '<div class="col-md-3">';
            $html .= $i->quantidade;
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</li>';
        }

        echo $html;
        exit;

    }


    function update_estoque()
    {


        $estoque_outros = $this->query("SELECT LS.id,LS.lote,LS.estoque_minimo,LS.usuario_id,LS.usuario_dt,LS.situacao,LS.descricao, LS.`artigo`, LS.`subestoque`, LS.`lote`, SUM(LS.`quantidade`) AS quantidade
                                FROM estq_lote_substoque LS
                                INNER JOIN estq_subestoque S
                                ON S.id = LS.`subestoque`
                                WHERE S.origem  = 2 AND LS.`origem`=2 AND LS.`subestoque` <> 2 AND LS.`situacao`<>3 AND S.`situacao` <> 3
                                GROUP BY LS.`artigo`
                                ");

        foreach ($estoque_outros as $eo) {

            $estq_del = new Estq_lote_substoque($eo->id);
            $estq_del->situacao=3;
            $estq_del->data_del=date('Y-m-d H:i:s');
            $estq_del->save();


          
        }

    }
}