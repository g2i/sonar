<?php
final class AnexoprofissionalController extends AppController{ 

    # página inicial do módulo Anexoprofissional
    function index(){
        $this->setTitle('Visualização de Anexo - Profissional');
    }

    # lista de Anexoprofissionais
    # renderiza a visão /view/Anexoprofissional/all.php
    function all(){
        $this->setTitle('Listagem de Anexos ');
        $p = new Paginate('Anexoprofissional', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if($this->getParam('id')){
            $c->addCondition('codigo_profissional','=',$this->getParam('id'));
            $this->set('Profissional', new Rhprofissional($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        }

            $c->setOrder('nome');

        $c->addCondition('status','=',1);

        $this->set('Anexoprofissionais', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $this->set('Rhstatus',  Rhstatus::getList());

        $this->set('Rhprofissionais',  Rhprofissional::getList());
    

    }

    # visualiza um(a) Anexoprofissional
    # renderiza a visão /view/Anexoprofissional/view.php
    function view(){
        $this->setTitle('Visualização de Anexo - Profissional');
        try {
            $this->set('Anexoprofissional', new Anexoprofissional((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexoprofissional', 'all');
        }
        $this->set('Rhstatus',  Rhstatus::getList());

    }

    # formulário de cadastro de Anexoprofissional
    # renderiza a visão /view/Anexoprofissional/add.php
    function add(){
        $this->setTitle('Anexo - Profissional');
        $this->set('Anexoprofissional', new Anexoprofissional);
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # recebe os dados enviados via post do cadastro de Anexoprofissional
    # (true)redireciona ou (false) renderiza a visão /view/Anexoprofissional/add.php
    function post_add(){
        $this->setTitle('Anexo - Profissional');
        $Anexoprofissional = new Anexoprofissional();

        if(!empty($_FILES['anexo']['name'])){
                $file = $_FILES['anexo'];
                $extencoes = array('jpg', 'pdf', 'gif', 'mp3', 'mp4', 'odf', 'docx', 'doc', 'txt', 'ppd', 'ppx', 'xlsx', 'pptx', 'xls', 'png', 'zip', 'rar');
                $anexo = new FileUploader($file, NULL, $extencoes);
                $path = "/anexosprofi/" . $_POST['codigo_profissional'];
                $nome = uniqid() . $anexo->TratarName($file['name']);
                $anexo->save($nome, $path);
                $Anexoprofissional->anexo = SITE_PATH . '/uploads' . $path . "/" . $nome;
        }
        $user=Session::get('user');
        $Anexoprofissional->cadastradopor=$user->id;
        $Anexoprofissional->dtCadastro=date('Y-m-d H:i:s');
        $_POST['status']=1;
        try {
            if(!empty($_FILES['anexo']['name'])) {
                $Anexoprofissional->save($_POST);
                new Msg(__('Anexo cadastrado com sucesso'));
                if (!empty($_POST['modal'])) {
                    echo 1;
                    exit;
                }
                $this->go('Anexoprofissional', 'all');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }

        $this->set('Anexoprofissional', $Anexoprofissional);
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # Confirma a exclusão ou não de um(a) Anexoprofissional
    # renderiza a /view/Anexoprofissional/delete.php
    function delete(){
        $this->setTitle('Apagar Anexoprofissional');
        try {
            $this->set('Anexoprofissional', new Anexoprofissional((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexoprofissional', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Anexoprofissional
    # redireciona para Anexoprofissional/all
    function post_delete(){
        try {
            $Anexoprofissional = new Anexoprofissional((int) $_POST['id']);
            $Anexoprofissional->status=3;
            $Anexoprofissional->save();
            new Msg(__('Anexo apagado com sucesso'), 1);
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Anexoprofissional', 'all');
    }

}