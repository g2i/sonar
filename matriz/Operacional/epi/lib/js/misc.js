prevent_ajax_view = true;

$(document).ready(function () {
    $('button[data-dismiss="modal"]').click(function () {
        //history.go(-1);
    });

    $(document).on('hidden.bs.modal', function (e) {
        $(e.target).removeData('bs.modal');
    });

    $('body').on('click', 'a[data-toggle=modal]', function (event) {
        event.preventDefault();
        modalharef = $(this).attr('href');
        if (modalharef.indexOf("?") == -1)
            modalharef += '?'
        modalharef += '&ajax=true';
        $(this).attr('href', modalharef);
        if (modalharef.indexOf("first") != -1) {
            Acrescentar(modalharef);
        }
        if (!$(this).attr('data-target')) {
            $(this).attr('data-target', '#modal');
        }
    });
});

function str_replace(search, replace, subject, count) {
    var i = 0, j = 0, temp = '', repl = '', sl = 0, fl = 0,
        f = [].concat(search),
        r = [].concat(replace), s = subject,
        ra = Object.prototype.toString.call(r) === '[object Array]',
        sa = Object.prototype.toString.call(s) === '[object Array]';
    s = [].concat(s);
    if (count) this.window[count] = 0;
    for (i = 0, sl = s.length; i < sl; i++) {
        if (s[i] === '') continue;
        for (j = 0, fl = f.length; j < fl; j++) {
            temp = s[i] + '';
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) this.window[count] += (temp.length - s[i].length) / f[j].length;
        }
    }
    return sa ? s : s[0];
}

$(document).ready(function () {
    $("[data-toggle='tooltip']").tooltip();

    $.fn.extend({
        openSelect: function () {
            return this.each(function (idx, domEl) {
                if (document.createEvent) {
                    var event = new MouseEvent("mousedown");
                    domEl.dispatchEvent(event);
                } else if (element.fireEvent) {
                    domEl.fireEvent("onmousedown");
                }
            });
        },
        loadGrid: function (url, args, selector, callback) {

            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';
            $.each(args, function(key, value) {
                url += '&'+ key + '=' + value;
            });

            url +=' '+ selector;
           
            return this.each(function () {
                if ($.isFunction(callback)) {
                    $(this).load(url, callback);
                }else {

                    $(this).load(url);

                }
            });
        },
        loadPostGrid: function (url, form, selector, callback) {
            var ths = this
            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';
            var n = url.indexOf("?");
            if (n > -1) {
                url += '&';
            } else {
                url += '/?';
            }
            url += '' + selector;

            this.each(function () {
                if ($.isFunction(callback)) {
                    $.post(url, form, function (dados) {
                        var htm = $(dados).find(selector).html();
                        ths.html(htm)
                        carregar()
                    });
                } else {
                    return $.post(url, form, function (dados) {
                        var htm = $(dados).find(selector).html();
                        ths.html(htm)
                        carregar()
                    });
                }
            });

        }


    });

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $.fn.extend({
        serializeJSON: function (exclude) {
            exclude || (exclude = []);
            return _.reduce(this.serializeArray(), function (hash, pair) {
                pair.value && !(pair.name in exclude) && (hash[pair.name] = pair.value);
                return hash;
            }, {});
        }
    });

});




