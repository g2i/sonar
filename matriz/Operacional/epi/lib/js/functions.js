/**

 * Created by Desenvolvimento on 29/06/2015.

 */

function dublicidade(tipo,cod,id){
    var codigo = document.getElementById("codigo_livre");
    $.ajax({
        type:'POST',
        url:root+'/Estq_artigo/duplicidade',
        data:{
            tipo:tipo,
            codigo:cod,
            id:id
        },
        dataType:'JSON',
        success:function(txt){
            if(txt.result>0) {
                codigo.setCustomValidity("Código já cadastrado!");
            }else{
                codigo.setCustomValidity('');
            }
        }
    })
}


//validação de cpf

function validarCPF(cpf) {

    cpf = cpf.replace(/[^\d]+/g,'');

    if(cpf == '') return false;

    // Elimina CPFs invalidos conhecidos

    if (cpf.length != 11 ||

        cpf == "00000000000" ||

        cpf == "11111111111" ||

        cpf == "22222222222" ||

        cpf == "33333333333" ||

        cpf == "44444444444" ||

        cpf == "55555555555" ||

        cpf == "66666666666" ||

        cpf == "77777777777" ||

        cpf == "88888888888" ||

        cpf == "99999999999")

        return false;

    // Valida 1o digito

    add = 0;

    for (i=0; i < 9; i ++)

        add += parseInt(cpf.charAt(i)) * (10 - i);

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11)

        rev = 0;

    if (rev != parseInt(cpf.charAt(9)))

        return false;

    // Valida 2o digito

    add = 0;

    for (i = 0; i < 10; i ++)

        add += parseInt(cpf.charAt(i)) * (11 - i);

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11)

        rev = 0;

    if (rev != parseInt(cpf.charAt(10)))

        return false;

    return true;

}

// / fim validação de cpf

function OpenDialog(titulo, mensagem) {

    var div = document.createElement("div");

    div.setAttribute('id', 'dialog-message');

    document.body.appendChild(div);



    function fech(){

        $("#dialog-message").remove();

    }

    $(function() {

        $("#dialog-message").dialog({

            modal: true,

            moveToTop:true,

            buttons: {

                Ok: fech

            }

        });

        $("#dialog-message").dialog({title: titulo});

        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});

        $("#dialog-message").text(mensagem);

    });

}

//Função para confirmação antes de enviar o formulario

function DialogConfirm(titulo, mensagem,funcao,parametros) {

    var div = document.createElement("div");

    div.setAttribute('id', 'dialog-message');

    document.body.appendChild(div);

    function sim(){

        $("#dialog-message").remove();

        $("#validForm").click();

    }

    function fech(){

        $("#dialog-message").remove();

    }

    $(function() {

        $("#dialog-message").dialog({

            modal: true,

            moveToTop:true,

            buttons: {

                Não: fech,

                Sim:sim

            }

        });

        $("#dialog-message").dialog({title: titulo});

        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});

        $("#dialog-message").text(mensagem);

    });

}


const myJSONObject = {"navegacao": [{"p": 1, "u": root}]};

function Navegar(url,param){

    var pos = myJSONObject.navegacao.length;

    if(param=='go'){

        pos = pos+1;

        var url = {"p": pos, "u": url};

        myJSONObject.navegacao.push(url);

    }else{

        pos = pos-1;

        myJSONObject.navegacao.pop();

    }



    $.each(myJSONObject,function(key,data){

        $.each(data,function(id,valor){

            if(pos==1){

                $('.modal').modal('hide');

            }else

            if(valor.p==pos) {

                if(param=='go')

                    $('.modal').find('.modal-content').load(valor.u);

                else

                if(valor.u!="")

                    $('.modal').find('.modal-content').load(valor.u);



            }

        });

    });

}

function Acrescentar(url){

    var pos = myJSONObject.navegacao.length;

    pos = pos+1;

    var url = {"p": pos, "u": url};

    myJSONObject.navegacao.push(url);

}

function openOnModal(url, title, size, buttons, validation){
    LoadGif();
    var dialogSize = BootstrapDialog.SIZE_NORMAL;
    var dialogButtons = [];

    if(size)
        dialogSize = size;

    if(buttons)
        dialogButtons = buttons;

    $('<div></div>').load(url, function(){
        BootstrapDialog.show({
            size: dialogSize,
            title: title,
            message: this,
            type: BootstrapDialog.TYPE_DEFAULT,
            buttons: dialogButtons,
            onshown: function(dialog){
                dialog.getModal().removeAttr('tabindex');
                CloseGif();
            },
            onhide: function(dialog){
                if($.isFunction(validation))
                    return !validation.call();
                else
                    return true;
            }
        });
    });
}

function showOnModal(url, title, size, buttons, validation){
    LoadGif();
    Acrescentar(url);
    var dialogSize = BootstrapDialog.SIZE_NORMAL;
    var dialogButtons = [];

    if(size)
        dialogSize = size;

    if(buttons)
        dialogButtons = buttons;

    $('<div></div>').load(url, function(){
        var dialog = BootstrapDialog.show({
            size: dialogSize,
            title: title,
            message: this,
            type: BootstrapDialog.TYPE_DEFAULT,
            buttons: dialogButtons,
            onshown: function(dialog){
                dialog.getModal().removeAttr('tabindex');
                CloseGif();
            },
            onhide: function(dialog){
                if($.isFunction(validation))
                    return validation.call(dialog);
                else
                    return true;
            }
        });
    });
}

function initeComponentes(element){

    $(element).find("#bt-gerar").each(function () {
        $(this).click(function () {
            var modal = $('#modal');
            var documento = modal.find('.modal-content');

            if (documento.find("#subestoque").val() == "") {
                BootstrapDialog.alert('Selecione um Sub-estoque');
                return false;
            }
            var dados = {
                subestoque: documento.find("#subestoque").val(),
                tipo: documento.find("#tipo").val(),
                t: documento.find("#t").val()
            }

            $.ReportPost('pos_estoque_epi', dados);
        })
    })

    
    $(element).find('.selectPicker').each(function() {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            minimumResultsForSearch: 6,
            placeholder: 'Selecione:'
        });
    });

    $(element).find('.money2').each(function(){
        $(this).priceFormat({
            prefix: '',
            centsLimit: 2,
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
    });

    $(element).find('.currency').each(function(){
        $(this).priceFormat({
            prefix: '',
            centsLimit: 2,
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
    });

    $(element).find('.date').each(function(){
        $(this).datetimepicker({
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            locale: 'pt-Br',
            format: 'L',
            extraFormats: [ 'YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss' ],
            tooltips: {
                today: 'Ir para Hoje',
                clear: 'Limpar sele\u00e7\u00e3o',
                close: 'Fechar',
                selectMonth: 'Selecione o M\u00eas',
                prevMonth: 'M\u00eas Anterior',
                nextMonth: 'Pr\u00f3ximo M\u00eas',
                selectYear: 'Selecione o Ano',
                prevYear: 'Ano Anterior',
                nextYear: 'Pr\u00f3ximo Ano',
                selectDecade: 'Selecione a D\u00e9cada',
                prevDecade: 'D\u00e9cada Anterior',
                nextDecade: 'Pr\u00f3xima D\u00e9cada',
                prevCentury: 'S\u00e9culo Anterior',
                nextCentury: 'Pr\u00f3ximo S\u00e9culo'
            }
        });
    });

    $(element).find('.dateFormat').each(function(){
        $(this).inputmask("d/m/y",{ "placeholder": "dd/mm/yyyy" });
    });

    $(element).find('.percent').each(function(){
        $(this).priceFormat({
            prefix: '',
            limit: 5,
            centsLimit: 2,
            centsSeparator: ',',
            thousandsSeparator: '',
            allowNegative: false
        });
    });

    $(element).find('select.clientes-ajax').each(function(){
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um cliente'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Cliente/findClientes/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    $(element).find('select.credor-ajax').each(function(){

        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um credor'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Fornecedor/findCredor/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    $(element).find('select.plano-ajax').each(function(){

        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um credor'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Planocontas/findPlanos/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    var inicio = $(element).find('.dateInicio').first();

    var fim = $(element).find('.dateFim').first();

    if(inicio && fim) {
        inicio.datetimepicker({
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            maxDate: moment().endOf('month').format('YYYY-MM-DD'),
            locale: 'pt-Br',
            format: 'L',
            tooltips: {
                today: 'Ir para Hoje',
                clear: 'Limpar sele\u00e7\u00e3o',
                close: 'Fechar',
                selectMonth: 'Selecione o M\u00eas',
                prevMonth: 'M\u00eas Anterior',
                nextMonth: 'Pr\u00f3ximo M\u00eas',
                selectYear: 'Selecione o Ano',
                prevYear: 'Ano Anterior',
                nextYear: 'Pr\u00f3ximo Ano',
                selectDecade: 'Selecione a D\u00e9cada',
                prevDecade: 'D\u00e9cada Anterior',
                nextDecade: 'Pr\u00f3xima D\u00e9cada',
                prevCentury: 'S\u00e9culo Anterior',
                nextCentury: 'Pr\u00f3ximo S\u00e9culo'
            }
        });
        fim.datetimepicker({
            useCurrent: false,
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            minDate: moment().date(1).format('YYYY-MM-DD'),
            locale: 'pt-Br',
            format: 'L',
            tooltips: {
                today: 'Ir para Hoje',
                clear: 'Limpar sele\u00e7\u00e3o',
                close: 'Fechar',
                selectMonth: 'Selecione o M\u00eas',
                prevMonth: 'M\u00eas Anterior',
                nextMonth: 'Pr\u00f3ximo M\u00eas',
                selectYear: 'Selecione o Ano',
                prevYear: 'Ano Anterior',
                nextYear: 'Pr\u00f3ximo Ano',
                selectDecade: 'Selecione a D\u00e9cada',
                prevDecade: 'D\u00e9cada Anterior',
                nextDecade: 'Pr\u00f3xima D\u00e9cada',
                prevCentury: 'S\u00e9culo Anterior',
                nextCentury: 'Pr\u00f3ximo S\u00e9culo'
            }
        });

        inicio.on("dp.change", function (e) {
            fim.data("DateTimePicker").minDate(e.date);
        });
        fim.on("dp.change", function (e) {
            inicio.data("DateTimePicker").maxDate(e.date);
        });
    }
}

function EnviarFormulario(form){



    $(form).ajaxForm({

        beforeSubmit: LoadGif(),

        success: function(d){

            CloseGif();

            Navegar('','back');



        }

    });

}

//cep

function isset() {

    var a = arguments,

        l = a.length,

        i = 0,

        undef;



    if (l === 0) {

        throw new Error('Empty isset');

    }



    while (i !== l) {

        if (a[i] === undef || a[i] === null) {

            return false;

        }

        i++;

    }

    return true;

}

function empty(mixed_var) {

    var undef, key, i, len;

    var emptyValues = [undef, null, false, 0, '', '0'];



    for (i = 0, len = emptyValues.length; i < len; i++) {

        if (mixed_var === emptyValues[i]) {

            return true;

        }

    }



    if (typeof mixed_var === 'object') {

        for (key in mixed_var) {

            // TODO: should we check for own properties only?

            //if (mixed_var.hasOwnProperty(key)) {

            return false;

            //}

        }

        return true;

    }



    return false;

}

$( "[data-list='collapse']" ).click(function () {
    if(empty($( this ).parent().attr('class'))){
        $(this).parent().attr('class','active');
        $(this).parent().css({"border-left":"none","border-right":"4px solid #19AA8D"});
        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360deg)","transform":" rotate(360deg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
    }else{
        $(this).parent().removeAttr('class');
        $(this).children(".fa-th-large").removeAttr('style');
        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360reg)","transform":" rotate(360reg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
        $(this).parent().css({"border-right":"none"});
    }
});

$("[role='open-adm']").click(function () {
    $("[role='adm']").css({"display":"block"});
});

var $image = $('.img-container > img');

$(".img-container > img").cropper({

    aspectRatio: 16 / 9,

    preview: ".img-preview",

    crop: function(data) {

        $("#dataX").val(Math.round(data.x));

        $("#dataY").val(Math.round(data.y));

        $("#dataHeight").val(Math.round(data.height));

        $("#dataWidth").val(Math.round(data.width));

        $("#dataRotate").val(Math.round(data.rotate));



    }

});

// Import image
var $inputImage = $('#inputImage'),
    URL = window.URL || window.webkitURL,
    blobURL;

if (URL) {

    $inputImage.change(function () {

        LoadGif();

            var files = this.files,

                file;



            if (!$image.data('cropper')) {

                return;

            }



            if (files && files.length) {

                file = files[0];



                if (/^image\/\w+$/.test(file.type)) {

                    blobURL = URL.createObjectURL(file);

                    $image.one('built.cropper', function () {

                        URL.revokeObjectURL(blobURL); // Revoke when load complete

                    }).cropper('reset').cropper('replace', blobURL);

                    $inputImage.val('');

                } else {

                    showMessage('Please choose an image file.');

                }

            }



    });

} else {
    $inputImage.parent().remove();
}

/*var cro = $(".img-container > img").cropper("getDataURL", "image/jpeg");
$.each(cro,function(key,value){
    console.log(value);
});*/

$("#inputImage").change(function () {
    $("#cropper-modal").modal("show");
});

function PaginarPost(pagina){
    $("#form").attr("action", root + "/"+$("#m").val()+"/"+$("#p").val()+"/?&page="+pagina);
    $("#form").submit();
}

function relatorios() {
    BootstrapDialog.show({
        title: 'Selecione um artigo',
        message: $('<div></div>').load(root + '/Estq_artigo/artigos/ajax:1/'),
        buttons: [{
            label: 'OK',
            cssClass: 'btn-primary',
            action: function(){
                //alert(id);
                window.open(root + "/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=ficha_estoque&artigo="+$('#artigos').val()+"", "_blank");
            }
        }, {
            label: 'Fechar',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
}

function encerrar_movimento(id) {
    $('<div></div>').load(root + '/Estq_mov/confirmacao/ajax:1', function () {
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_DANGER,
            title: 'Finalizar Movimento',
            message: this,
            buttons: [{
                label: 'Executar',
                cssClass: 'btn-danger',
                action: function (dialog) {
                    var body = dialog.getModalBody();
                    var baixa = body.find('#chkBaixar').is(":checked");
                    var url = root + '/Estq_mov/processamento/id:' + id + '/baixar:' + baixa;
                    location.href = url;
                }
            }, {
                label: 'Fechar',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            }]
        });
    });
}

function getTipo(tipo,id){
    $.ajax({
        type:'POST',
        url:root+'/Cautela/get_tipo',
        data:'tipo='+tipo+'&id='+id,
        success: function (txt) {
            $("#charger_tipo").html(txt);
        }
    });
}

$(function () {
    $("[data-tool='tooltip']").tooltip();

    $('.cpf').mask('000.000.000-00', {reverse: true});

    $('.cep').mask('00000-000');

    $('.phone').mask('(00) 0000-00000');

    $('.date').mask('00/00/0000');

    $('.datetime').mask('00/00/0000 00:00');

    $('.cnpj').mask('00.000.000/0000-00');

    $('.money').mask('000.000.000.000.000,00', {reverse: true});

    moment.locale('pt-BR');
    $('.datePicker').datetimepicker({
        format: 'L',
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true
    });

    $('.datetimepicker').datetimepicker({
        locale:'pt-br',
        sideBySide: true,
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        showClear: true,
        allowInputToggle: true
    });
});

function dell_iten(id){
    BootstrapDialog.confirm('Tem certeza que deseja excluir este registro?', function(result){
        if(result) {
            var filtro = {
                artigo: ''
            }
            $.ajax({
                type:'GET',
                url:root+'/Estq_movdetalhes/remove_item',
                data:{
                    pos:id
                },success:function (txt) {
                    $("div.list-dep").loadGrid(root+'/Estq_movdetalhes/list_art', filtro, null, null);
                }
            })
        }
    });
}

function initComponente(document){

    $(document).find('#btn-filtro').click(function(){
        var caminho = $(document).find('form').attr('action')
        var form = $(document).find('form').serializeObject()
        $("div.table-responsive").loadPostGrid(caminho, form, null,carregar);
        return false;
    })

    $(document).find('.pagination li a').click(function(){
        var caminho = $(this).attr('href');
        var form = $(document).find('form').serializeObject()
        $("div.table-responsive").loadPostGrid(caminho, form, null,carregar);
        return false;
    })

}

function carregar(){
    $(document).find('body').each(function(){
        initComponente(document)
    })
}


