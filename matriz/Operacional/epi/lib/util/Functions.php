<?php


/**
 * @param $string_date
 * @return false|string
 * Converte uma string de data no formato d/m/Y (BR) para um date no formato Y-m-d (SQL)
 */
function stringDateBRToSQL($string_date)
{
    $date = date_create_from_format("d/m/Y", $string_date);
    return date_format($date, "Y-m-d");
}

function DataSQL($data)
{
    if ($data != "") {
        list($d, $m, $y) = preg_split('/\//', $data);
        return sprintf('%04d-%02d-%02d', $y, $m, $d);
    }
}

function DataBR($data)
{
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

function DataTimeBr($data)
{
    if ($data != "") {
        return date("d-m-Y H:i", strtotime($data));//exibe no formato d/m/a
    }
}

function diferenca_datas($inicio, $fim)
{
    $diferenca = strtotime($fim) - strtotime($inicio);
    $dias = floor($diferenca / (60 * 60 * 24));
    return $dias;

}

function ConvertDateTime($data)
{
    $dat = explode('/', $data);
    $dia = $dat[0];
    $mes = $dat[1];
    $ano_hora = explode(' ', $dat[2]);
    $ano = $ano_hora[0];
    $hora = $ano_hora[1];
    return $ano . '-' . $mes . '-' . $dia . ' ' . $hora;
}

function ConvertData($data)
{
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

function LimiteTexto($texto, $limite, $quebra = true)
{
    $tamanho = strlen($texto);
    if ($tamanho <= $limite) { //Verifica se o tamanho do texto é menor ou igual ao limite
        $novo_texto = $texto;
    } else { // Se o tamanho do texto for maior que o limite
        if ($quebra == true) { // Verifica a opção de quebrar o texto
            $novo_texto = trim(substr($texto, 0, $limite)) . "...";
        } else { // Se não, corta $texto na última palavra antes do limite
            $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
            $novo_texto = trim(substr($texto, 0, $ultimo_espaco)) . "..."; // Corta o $texto até a posição localizada
        }
    }
    return $novo_texto; // Retorna o valor formatado
}

function utf8_converter($array)
{
    array_walk_recursive($array, function (&$item, $key) {
        if (!mb_detect_encoding($item, 'utf-8', true)) {
            $item = utf8_encode($item);
        }
    });

    return $array;
}


//funcao para buscar localidades apenas do usuario logado 
function getEstq_usuario_substoque(){
    $d = new Criteria();
    $d->addCondition('estq_subestoque.usuario_id', '=', Session::get("user")->id);
    $rhUsuario = Estqusuario_substoque::getList($d);
  
    $listIds = '(';
    foreach($rhUsuario as $r){// 2
        $listIds .= $r->estq_subestoque_id.',';
    }
    $ids = substr($listIds,0,-1);
    $ids .= ')';
    
   return $ids;
}