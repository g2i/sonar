prevent_ajax_view = true;

$(document).ready(function() {
    $('button[data-dismiss="modal"]').click(function() {
        //history.go(-1);
    });
    
    $(document).on('hidden.bs.modal', function(e) {
        $(e.target).removeData('bs.modal');
    });

    $('body').on('click', 'a[data-toggle=modal]', function(event){
        event.preventDefault();
        modalharef = $(this).attr('href');
        if (modalharef.indexOf("?") == -1)
            modalharef += '?'
        modalharef += '&ajax=true';
        $(this).attr('href', modalharef);

        if(modalharef.indexOf("first")!=-1){
            Acrescentar(modalharef);
        }
        if (!$(this).attr('data-target')) {
            $(this).attr('data-target', '#modal');
        }
    });

   //function para o btn de pesquisa em form 
    $('.filtros').find('#buscar-filtro').click(function(){
        let form_action = $(this).closest('form').attr('action');
        let form_serialize = $(this).closest('form').serialize();
        let url_completa = form_action + '?' + form_serialize;
        console.log(url_completa);
        carregaTabelaResponsiva(url_completa);
    });
    
//function para paginar por ajax
    $(document).on('click','.pagination a, .table thead a', function(e){
        e.preventDefault();
        let url = $(this).attr('href');

        if(url != "")
        {            
            carregaTabelaResponsiva(url);
        }
        return false;
    });

    $(document).find('.date').each(function(){
        $(this).datetimepicker({
            showTodayButton: true,
            showClear: true,
            showClose: true,
            allowInputToggle: true,
            locale: 'pt-Br',
            format: 'L',
            extraFormats: [ 'YYYY-MM-DD', 'YYYY-MM-DD HH:mm:ss' ],
            tooltips: {
                today: 'Ir para Hoje',
                clear: 'Limpar sele\u00e7\u00e3o',
                close: 'Fechar',
                selectMonth: 'Selecione o M\u00eas',
                prevMonth: 'M\u00eas Anterior',
                nextMonth: 'Pr\u00f3ximo M\u00eas',
                selectYear: 'Selecione o Ano',
                prevYear: 'Ano Anterior',
                nextYear: 'Pr\u00f3ximo Ano',
                selectDecade: 'Selecione a D\u00e9cada',
                prevDecade: 'D\u00e9cada Anterior',
                nextDecade: 'Pr\u00f3xima D\u00e9cada',
                prevCentury: 'S\u00e9culo Anterior',
                nextCentury: 'Pr\u00f3ximo S\u00e9culo'
            }
        });
    });

});
//function para pegar as URLs 
function carregaTabelaResponsiva(url) {
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: () => {
        },
        success: (data) => {
            let conteudo = $('<div>').append(data).find('.table-responsive');
            $(".table-responsive").html(conteudo);
        },
        complete: () => {
        },
        error: () => {
        },
    });
}
