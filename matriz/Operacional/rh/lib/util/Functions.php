<?php

function DataSQL($data)
{
    if ($data != "") {
        list($d, $m, $y) = preg_split('/\//', $data);
        return sprintf('%04d-%02d-%02d', $y, $m, $d);
    }
}

function DataBR($data)
{
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

function ConvertData($data)
{
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

function LimiteTexto($texto, $limite, $quebra = true)
{
    $tamanho = strlen($texto);
    if ($tamanho <= $limite) { //Verifica se o tamanho do texto é menor ou igual ao limite
        $novo_texto = $texto;
    } else { // Se o tamanho do texto for maior que o limite
        if ($quebra == true) { // Verifica a opção de quebrar o texto
            $novo_texto = trim(substr($texto, 0, $limite)) . "...";
        } else { // Se não, corta $texto na última palavra antes do limite
            $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite
            $novo_texto = trim(substr($texto, 0, $ultimo_espaco)) . "..."; // Corta o $texto até a posição localizada
        }
    }
    return $novo_texto; // Retorna o valor formatado
}

function utf8_converter($array)
{
    array_walk_recursive($array, function (&$item, $key) {
        if (!mb_detect_encoding($item, 'utf-8', true)) {
            $item = utf8_encode($item);
        }
    });

    return $array;
}


function diferenca_datas($inicio, $fim)
{
    $diferenca = strtotime($fim) - strtotime($inicio);
    $dias = floor($diferenca / (60 * 60 * 24));
    return $dias;

}

//funcao para buscar localidades apenas do usuario logado 
function getRhusuarioLocalidade()
{
    $d = new Criteria();
    $d->addCondition('rhusuario_localidade.usuario_id', '=', Session::get("user")->id);
    $rhUsuario = Rhusuario_localidade::getList($d);

    $listIds = '(';
    foreach ($rhUsuario as $r) {// 2
        $listIds .= $r->rhlocalidade_id . ',';
    }
    $ids = substr($listIds, 0, -1);
    $ids .= ')';

    // var_dump($ids);exit;
    return $ids;
}

function print_admitidos($inicio = null, $fim = null, $localidade = null)
{
    $db = new MysqlDB();
    $admissao = "SELECT P.codigo,
    P.nome,
    C.`dtInicial`
    AS datas,
    rhl.`cidade` AS local_trabalho,
    F.nome AS funcao,
    C.observacao
    FROM rhprofissional P
    LEFT JOIN rhprofissional_contratacao C ON C.codigo_profissional = P.codigo
    LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
    LEFT JOIN `rhlocalidade` rhl ON rhl.`id` = P.`rhlocalidade_id`
    WHERE C.tipo = 'Admissão' 
    AND   C.dtInicial BETWEEN ('$inicio') AND ('$fim')
    AND P.`rhlocalidade_id` = $localidade";

    $db->query($admissao);
    return $db->getResults();
}

function print_demitidos($inicio = null, $fim = null, $localidade = null)
{
    $db = new MysqlDB();
    $demissao = "SELECT p.codigo,
    p.nome,
    c.`dtFinal` AS datas,
    rhl.`cidade` AS local_trabalho,
    f.nome AS funcao,
    c.observacao,
    c.motivo
    FROM rhprofissional_contratacao c
    LEFT JOIN rhprofissional p ON p.`codigo` = c.`codigo_profissional`
    LEFT JOIN rhprofissional_funcao f ON f.codigo = c.funcao
    LEFT JOIN `rhlocalidade` rhl ON rhl.`id` = p.`rhlocalidade_id`
    WHERE   c.tipo = 'Demissão'
    AND c.dtFinal BETWEEN ('$inicio') AND ('$fim')
    AND P.`rhlocalidade_id` =  $localidade";
    $db->query($demissao);
    return $db->getResults();
}

function print_ferias($inicio = null, $fim = null, $localidade = null)
{
    $db = new MysqlDB();
    $ferias = "SELECT p.codigo,p.nome,
    o.`data_especifica` AS datas,
    rhl.`cidade` AS local_trabalho,
    F.nome AS funcao,
    o.descricao
    FROM rhocorrencia o
    LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
    LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
    LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
    LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
    LEFT JOIN `rhlocalidade` rhl ON rhl.`id` = p.`rhlocalidade_id`
    WHERE o.`status` != 3
    AND o.historico = 29
    AND o.`data_especifica` BETWEEN ('$inicio') AND ('$fim')
    AND p.`rhlocalidade_id` = $localidade
    AND C.`tipo` != 'Demissão'
    ORDER BY UNIX_TIMESTAMP(o.`data_especifica` ) DESC";

    $db->query($ferias);
    return $db->getResults();
}

function print_aso($inicio = null, $fim = null, $historico = null, $localidade = null)
{
    $db = new MysqlDB();
    $aso = "SELECT  p.codigo,
    p.nome,
    o.`data_especifica` AS datas,
    rhl.`cidade` AS local_trabalho,
    F.nome AS funcao,
    o.descricao
    FROM rhocorrencia o
    LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
    LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
    LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
    LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
    LEFT JOIN `rhlocalidade` rhl ON rhl.`id` = P.`rhlocalidade_id`
    WHERE o.`status` != 3
    AND o.`data_especifica` BETWEEN ('$inicio') AND ('$fim')
    AND p.`rhlocalidade_id` = $localidade
    AND C.`tipo` != 'Demissão'
    ";

    if(!empty($historico)){
        $aso .= " AND o.historico = $historico";
    }

    $aso .= " ORDER BY UNIX_TIMESTAMP(o.`data_especifica` ) DESC";

    $db->query($aso);
    return $db->getResults();
}

function print_inss($inicio = null, $fim = null, $localidade = null)
{
    $db = new MysqlDB();
    $inss = "SELECT  p.codigo,
    p.nome,
    o.`data_especifica` AS datas,
    rhl.`cidade` AS local_trabalho,
    F.nome AS funcao,
    o.descricao
    FROM rhocorrencia o
    LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
    LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
    LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
    LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
    LEFT JOIN `rhlocalidade` rhl ON rhl.`id` = P.`rhlocalidade_id`
    WHERE o.`status` != 3
    AND oh.codigo = 28
    AND o.`data_especifica` BETWEEN ('$inicio') AND ('$fim')
    AND p.`rhlocalidade_id` = $localidade
    AND C.`tipo` != 'Demissão'
    ORDER BY UNIX_TIMESTAMP(o.`data_especifica` ) DESC";

    $db->query($inss);
    return $db->getResults();
}

function print_atestado($inicio = null, $fim = null, $localidade = null)
{
    $db = new MysqlDB();
    $inss = "SELECT  p.codigo,
    p.nome,
    F.nome AS funcao,
    p.cidade,
    c.dtInicial,
    p.endereco,
    p.bairro,
    p.numero,
    p.cep,
    p.dtnascimento,
    p.rg,
    p.dtemissao,
    p.cpf,
    p.pis,
    p.carteiratrabalho,
    p.dtcarteiratrabalho,
    c.valorremuneracao,
    p.celular01,
    o.`data` AS datas,
    rhl.`cidade` AS local_trabalho,
    o.descricao
    FROM rhocorrencia o
    LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
    LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
    LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
    LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
    LEFT JOIN `rhlocalidade` rhl ON rhl.`id` = P.`rhlocalidade_id`
    WHERE o.`status` != 3
    AND oh.codigo = 30
    AND o.`data` BETWEEN ('$inicio') AND ('$fim')
    AND p.`rhlocalidade_id` = $localidade
    AND C.`tipo` != 'Demissão'
    ORDER BY UNIX_TIMESTAMP(o.`data`) DESC";

    $db->query($inss);

    return $db->getResults();
}

function print_projetos($inicio = null, $fim = null, $localidade = null, $projetos = null)
{
    $db = new MysqlDB();
    $ferias = "SELECT DISTINCT  P.codigo,
    P.nome,
    p.`rhlocalidade_id`,
    p.`rhprojetos_id`,
    P.dtCadastro AS datas,
    rhl.`cidade` AS local_trabalho,
    F.nome AS funcao,
    C.observacao
    FROM rhprofissional P
    LEFT JOIN rhprofissional_contratacao C ON C.codigo_profissional = P.codigo
    LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
    LEFT JOIN `rhlocalidade` rhl ON rhl.`id` = P.`rhlocalidade_id`
    WHERE  P.dtCadastro BETWEEN ('$inicio') AND ('$fim')
    AND p.`rhlocalidade_id` = $localidade
    AND C.`tipo` != 'Demissão'
    ";

    if(!empty($projetos)){
        $ferias .=" AND p.`rhprojetos_id` = $projetos";
    }

    $ferias .=" ORDER BY P.codigo";
    $db->query($ferias);
    return $db->getResults();
}

//busca as ocorrencias com vencimento com 45 dias apartir da data atual
function sesmetAlertOcorrencias()
{
    $db = new MysqlDB();
    $ocorrencias = "SELECT o.codigo AS ocorrencia_id , p.`nome`, oh.`nome` AS descricao, o.data,DATEDIFF (o.data, o.`data_especifica`) AS dias, 
    o.`data_especifica` AS vencimento, p.`codigo` AS id_prof, oh.`historico_complementar` AS comp,
    p.`codigo`, oh.`periodicidade`
    FROM rhocorrencia o
    LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
    LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
    LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
    WHERE o.`status` != 3
    AND o.`status` != 4
    AND p.`status` != 3
    AND oh.codigo != 27
    AND oh.codigo != 28  
    AND oh.`periodicidade` != 'Nenhuma'
    AND C.`tipo` != 'Demissão'
     AND o.`data_especifica` >= CURDATE() AND o.`data_especifica` <=  DATE_ADD(CURDATE(),INTERVAL 45 DAY)
    ORDER BY vencimento ASC";

    $db->query($ocorrencias);
    return $db->getResults();
}
