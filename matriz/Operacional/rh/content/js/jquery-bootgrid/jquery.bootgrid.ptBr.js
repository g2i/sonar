(function ($, window, undefined)
{
    /*jshint validthis: true */
    "use strict";

    $.extend($.fn.bootgrid.Constructor.defaults.labels, {
        all: "Todos os registros",
        loading: "Aguarde carregando...",
        refresh: "Atualizar",
        search: "Procurar",
        noResults: "Sem registros",
        infos: "Exibindo {{ctx.start}} - {{ctx.end}} de {{ctx.total}} registros"
    });
})(jQuery, window);
