(function ($, window, undefined)
{
    /*jshint validthis: true */
    "use strict";

    $.extend($.fn.bootgrid.Constructor.defaults.templates, {
        loading: "<tr><td colspan=\"{{ctx.columns}}\" class=\"loading\"><i class=\"fa fa-spinner fa-pulse\"></i> {{lbl.loading}}</td></tr>"
    });
})(jQuery, window);
