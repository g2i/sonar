<?php
final class Rhsetor extends Record{ 

    const TABLE = 'rhsetor';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Rhsetor possui Rhprofissionais
    * @return array de Rhprofissionais
    */
    function getRhprofissionais($criteria=NULL) {
        return $this->hasMany('Rhprofissional','setor',$criteria);
    }
    
    /**
    * Rhsetor pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    
    /**
    * Rhsetor pertence a Rhsecao
    * @return Rhsecao $Rhsecao
    */
    function getRhsecao() {
        return $this->belongsTo('Rhsecao','codigo_secao');
    }
}