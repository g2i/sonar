<?php
final class Rhocorrencia_historico extends Record{ 

    const TABLE = 'rhocorrencia_historico';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Rhocorrencia_historico possui Rhocorrencias
    * @return array de Rhocorrencias
    */
    function getRhocorrencias($criteria=NULL) {
        return $this->hasMany('Rhocorrencia','historico',$criteria);
    }
    
    /**
    * Rhocorrencia_historico pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    
    /**
    * Rhocorrencia_historico pertence a Rhgrupo_histocorr
    * @return Rhgrupo_histocorr $Rhgrupo_histocorr
    */
    function getRhgrupo_histocorr() {
        return $this->belongsTo('Rhgrupo_histocorr','grupo');
    }
}