<?php
final class Anexo extends Record{ 

    const TABLE = 'anexo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Anexo pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao');
    }
    function getTipoAnexo() {
        return $this->belongsTo('Tipo_anexos','tipo');
    }
}