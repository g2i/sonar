<?php
final class Anexocontratacao extends Record{ 

    const TABLE = 'anexocontratacao';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
        return $criteria;
    }
    
    /**
    * Anexocontratacao pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    
    /**
    * Anexocontratacao pertence a Rhprofissional_contratacao
    * @return Rhprofissional_contratacao $Rhprofissional_contratacao
    */
    function getRhprofissional_contratacao() {
        return $this->belongsTo('Rhprofissional_contratacao','codigo_contratacao');
    }
}