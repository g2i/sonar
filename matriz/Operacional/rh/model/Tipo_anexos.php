<?php
final class Tipo_anexos extends Record{ 

    const TABLE = 'tipo_anexos';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Tipo_anexos possui Anexos
    * @return array de Anexos
    */
    function getAnexos($criteria=NULL) {
        return $this->hasMany('Anexos','tipo_id',$criteria);
    }
    
    /**
    * Tipo_anexos pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Tipo_anexos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','user_id');
    }
}