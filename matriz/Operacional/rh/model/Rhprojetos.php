<?php
final class Rhprojetos extends Record{ 

    const TABLE = 'rhprojetos';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
       /**
    * Rhprojetos pertence a Usuario
    * @return Usuario $Usuario
    */

    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Rhprojetos pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Rhprojetos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
}