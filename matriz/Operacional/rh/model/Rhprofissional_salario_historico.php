<?php
final class Rhprofissional_salario_historico extends Record{ 

    const TABLE = 'rhprofissional_salario_historico';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rhprofissional_salario_historico pertence a Rhprofissional_contratacao
    * @return Rhprofissional_contratacao $Rhprofissional_contratacao
    */
    function getRhprofissional_contratacao() {
        return $this->belongsTo('Rhprofissional_contratacao','rhprofissional_contratacao_id');
    }
    
    /**
    * Rhprofissional_salario_historico pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}