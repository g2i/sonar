<?php
date_default_timezone_set("America/Campo_Grande");
##################################
# CONFIGURAÇÕES DO SGBD 
##################################
/*
Config::set('db_host', 'localhost');
Config::set('db_user', 'root');
Config::set('db_password', '');
Config::set('db_name', 'js_matriz_local');*/

Config::set('db_host', 'localhost');
Config::set('db_user', 'root');
Config::set('db_password', 'root');
Config::set('db_name', 'js');


# MODO DE DEPURAÇÃO
# desenvolvimento: true
# produção: false
Config::set('debug', 0);


# TEMPLATE PADRÃO
# Ex: para o diretório /template/default:
# Config::set('template', 'default');
Config::set('template', 'inspinia');
//Config::set('template', 'sidebar');
// Config::set('template', 'topbar');
// Config::set('template', 'default');

Config::set('origem', '1'); //origem do estoque


Config::set('key', 'r528qy2014161d5jqtjeo3h8875gfFDGfqqqk3djghkH&*');
Config::set('salt', 'TjcqT8jgR8H6v6vhJhBcdZmCvnZs4Ghs9JcvW48gCqUTtVEkcK5hYLpsw6As8AbU');

//# Chave da aplicação, para controle de sessões e criptografia
//# Utilize uma cadeia alfanumérica aleatória única
//Config::set('key', 'f345kh20120141130pjhygjjft875gfFDGfqqqk3djghkH&*');
//
//# SALT - Utilizada na criptografia
//# Utiliza uma chave alfa-numéria complexa de no mínimo 16 dígitos
//Config::set('salt', 'kkghgghk00141130pjhygjjfthg6jjgBGt94gd3fC6bj36gk1os');


Config::set('lang', 'pt_br');
Config::set('rewriteURL', true);
Config::set('indexController', 'Index');
Config::set('indexAction', 'Index');
Config::set('criptedGetParamns', array());