<?php
final class AnexocontratacaoController extends AppController{ 

    # página inicial do módulo Anexocontratacao
    function index(){
        $this->setTitle('Visualização de Anexos');
    }

    # lista de Anexocontratacaos
    # renderiza a visão /view/Anexocontratacao/all.php
    function all(){
        $this->setTitle('Listagem de Anexos');
        $p = new Paginate('Anexocontratacao', 10);
        $c = new Criteria();
        if($this->getParam('id')) {
            $c->addCondition('codigo_contratacao', '=', $this->getParam('id'));
            $this->set('Contratacao', new Rhprofissional_contratacao($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        }

            $c->setOrder('nome');

        $this->set('Anexocontratacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Anexocontratacao
    # renderiza a visão /view/Anexocontratacao/view.php
    function view(){
        $this->setTitle('Visualização de Anexos');
        try {
            $this->set('Anexocontratacao', new Anexocontratacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexocontratacao', 'all');
        }
    }

    # formulário de cadastro de Anexocontratacao
    # renderiza a visão /view/Anexocontratacao/add.php
    function add(){
        $this->setTitle('Cadastro de Anexos');
        $this->set('Anexocontratacao', new Anexocontratacao);
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhprofissional_contratacaos',  Rhprofissional_contratacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Anexocontratacao
    # (true)redireciona ou (false) renderiza a visão /view/Anexocontratacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Anexos');
        $Anexocontratacao = new Anexocontratacao();
        if(!empty($_FILES['anexo']['name'])){
            $file = $_FILES['anexo'];
            $extencoes = array('jpg', 'pdf', 'gif', 'mp3', 'mp4', 'odf', 'docx', 'doc', 'txt', 'ppd', 'ppx', 'xlsx', 'pptx', 'xls', 'png', 'zip', 'rar');
            $anexo = new FileUploader($file, NULL, $extencoes);
            $path = "/anexocontrata/" . $_POST['codigo_contratacao'];
            $nome = uniqid() . $anexo->TratarName($file['name']);
            $anexo->save($nome, $path);
            $Anexocontratacao->anexo = SITE_PATH . '/uploads' . $path . "/" . $nome;
        }
        $user=Session::get('user');
        $Anexocontratacao->cadastradopor=$user->id;
        $Anexocontratacao->dtCadastro=date('Y-m-d H:i:s');
        $Anexocontratacao->status=1;
        $this->set('Anexocontratacao', $Anexocontratacao);
        try {
            if(!empty($_FILES['anexo']['name'])) {
                $Anexocontratacao->save($_POST);
                new Msg(__('Anexocontratacao cadastrado com sucesso'));
                if (!empty($_POST['modal'])) {
                    echo 1;
                    exit;
                }
                $this->go('Anexocontratacao', 'all');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhprofissional_contratacaos',  Rhprofissional_contratacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Anexocontratacao
    # renderiza a /view/Anexocontratacao/delete.php
    function delete(){
        $this->setTitle('Apagar Anexocontratacao');
        try {
            $this->set('Anexocontratacao', new Anexocontratacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexocontratacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Anexocontratacao
    # redireciona para Anexocontratacao/all
    function post_delete(){
        try {
            $Anexocontratacao = new Anexocontratacao((int) $_POST['id']);
            $Anexocontratacao->status=3;
            $Anexocontratacao->save();
            new Msg(__('Anexo Contratação deletada com sucesso'), 1);
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Anexocontratacao', 'all');
    }

}