<?php
final class Rhprofissional_dependenteController extends AppController{ 

    # página inicial do módulo Rhprofissional_dependente
    function index(){
        $this->setTitle('Visualização de Dependente');
    }

    # lista de Rhprofissional_dependentes
    # renderiza a visão /view/Rhprofissional_dependente/all.php
    function all(){
        $this->setTitle('Listagem de Dependentes');
        $p = new Paginate('Rhprofissional_dependente', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if($this->getParam('id')){
            $c->addCondition('codigo_profissional',"=",$this->getParam('id'));
            $this->set('Profissional', new Rhprofissional($this->getParam('id')));
        }

            $c->setOrder('nome');
            $c->addCondition('status','!=', 3);
        $this->set('Rhprofissional_dependentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    

    }

    # visualiza um(a) Rhprofissional_dependente
    # renderiza a visão /view/Rhprofissional_dependente/view.php
    function view(){
        $this->setTitle('Dependente');
        try {
            $this->set('Rhprofissional_dependente', new Rhprofissional_dependente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_dependente', 'all');
        }
    }

    # formulário de cadastro de Rhprofissional_dependente
    # renderiza a visão /view/Rhprofissional_dependente/add.php
    function add(){
        $this->setTitle('Cadastro de Dependente');
        $this->set('Rhprofissional_dependente', new Rhprofissional_dependente);
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhprofissional_dependente
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_dependente/add.php
    function post_add(){
        $this->setTitle('Cadastro de Dependente');
        $Rhprofissional_dependente = new Rhprofissional_dependente();
        $this->set('Rhprofissional_dependente', $Rhprofissional_dependente);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhprofissional_dependente->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhprofissional_dependente->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhprofissional_dependente->save($_POST);
            new Msg(__('Dependente cadastrado com sucesso!'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhprofissional_dependente', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # formulário de edição de Rhprofissional_dependente
    # renderiza a visão /view/Rhprofissional_dependente/edit.php
    function edit(){
        $this->setTitle('Edição de Dependente');
        try {
            $this->set('Rhprofissional_dependente', new Rhprofissional_dependente((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
            $this->set('Rhprofissionais',  Rhprofissional::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhprofissional_dependente', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhprofissional_dependente
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_dependente/edit.php
    function post_edit(){
        $this->setTitle('Edição de Dependente');
        try {
            $Rhprofissional_dependente = new Rhprofissional_dependente((int) $_POST['codigo']);
            $this->set('Rhprofissional_dependente', $Rhprofissional_dependente);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhprofissional_dependente->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhprofissional_dependente->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhprofissional_dependente->save($_POST);
            new Msg(__('Dados do Dependente atualizados com sucesso!'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhprofissional_dependente', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhprofissional_dependente
    # renderiza a /view/Rhprofissional_dependente/delete.php
    function delete(){
        $this->setTitle('Apagar Dependente');
        try {
            $this->set('Rhprofissional_dependente', new Rhprofissional_dependente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_dependente', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhprofissional_dependente
    # redireciona para Rhprofissional_dependente/all
    function post_delete(){
        try {
            $Rhprofissional_dependente = new Rhprofissional_dependente((int) $_POST['id']);
            $Rhprofissional_dependente->dtAtualizacao=date('Y-m-d H:i:s');
            $user=Session::get('user');
            $Rhprofissional_dependente->atualizadopor=$user->id;
            $Rhprofissional_dependente->status=3;
            $Rhprofissional_dependente->save();
            new Msg(__('Dependente deletado com sucesso!'), 1);
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprofissional_dependente', 'all');
    }

}