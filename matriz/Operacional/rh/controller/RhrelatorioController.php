<?php

final class RhrelatorioController extends AppController
{

    # página inicial do módulo Rhrelatorio
    function index()
    {
        $this->setTitle('Visualização de Rhrelatorio');
    }

    # lista de Rhrelatorios
    # renderiza a visão /view/Rhrelatorio/all.php
    function all()
    {
        $this->setTitle('Listagem de Rhrelatorio');
        $p = new Paginate('Rhrelatorio', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        //busca ocorrencias apenas aso DISTINCT
        $query = $this->query("SELECT DISTINCT * FROM `rhocorrencia_historico` oh
       WHERE  oh.`codigo` = 3
       OR oh.`codigo` = 14
       OR oh.`codigo` = 24
       OR oh.`codigo` = 27
       OR oh.`codigo` = 23");

        $this->set('Rhprojetos', Rhprojetos::getList());
        $this->set('Rhocorrencia_historicos', $query);
        $this->set('Rhlocalidade', Rhlocalidade::getList());
        $this->set('Rhrelatorios', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/view.php
    function view()
    {
        $this->setTitle('Visualização de Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # formulário de cadastro de Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/add.php
    function add()
    {
        $this->setTitle('Cadastro de Rhrelatorio');
        $this->set('Rhrelatorio', new Rhrelatorio);
    }

    # recebe os dados enviados via post do cadastro de Rhrelatorio
    # (true)redireciona ou (false) renderiza a visão /view/Rhrelatorio/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Rhrelatorio');
        $Rhrelatorio = new Rhrelatorio();
        $this->set('Rhrelatorio', $Rhrelatorio);
        try {
            $Rhrelatorio->save($_POST);
            new Msg(__('Rhrelatorio cadastrado com sucesso'));
            $this->go('Rhrelatorio', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # formulário de edição de Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/edit.php
    function edit()
    {
        $this->setTitle('Edição de Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhrelatorio
    # (true)redireciona ou (false) renderiza a visão /view/Rhrelatorio/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Rhrelatorio');
        try {
            $Rhrelatorio = new Rhrelatorio((int)$_POST['inicio']);
            $this->set('Rhrelatorio', $Rhrelatorio);
            $Rhrelatorio->save($_POST);
            new Msg(__('Rhrelatorio atualizado com sucesso'));
            $this->go('Rhrelatorio', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhrelatorio
    # renderiza a /view/Rhrelatorio/delete.php
    function delete()
    {
        $this->setTitle('Apagar Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhrelatorio
    # redireciona para Rhrelatorio/all
    function post_delete()
    {
        try {
            $Rhrelatorio = new Rhrelatorio((int)$_POST['id']);
            $Rhrelatorio->delete();
            new Msg(__('Rhrelatorio apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Rhrelatorio', 'all');
    }


    function print_distribuicao()
    {
        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_POST['inicio'];
        $fim = $_POST['fim'];
        $tipo = $_POST['tipo'];
        $historico = $_POST['historico'];
        $localidade = $_POST['localidade'];
        $projetos = $_POST['Rhprojetos'];

        $titulo = ' ';
        if ($tipo == 1) {
            $dados = print_admitidos($inicio, $fim, $localidade);
            $titulo = "Listagens Contratação (Por Período)";
        } else if ($tipo == 2) {
            $dados = print_demitidos($inicio, $fim, $localidade);
            $titulo = "Listagens Demissão (Por Período)";
        } else if ($tipo == 3) {
            $dados = print_ferias($inicio, $fim, $localidade);
            $titulo = "Listagem - Férias a Conceder";
        } else if ($tipo == 4) {
            $dados = print_aso($inicio, $fim, $historico, $localidade);
            $titulo = "Listagens - ASO (Período Vencimento - Ativos)";
        } else if ($tipo == 5) {
            $dados = print_inss($inicio, $fim, $localidade);
            $titulo = "Listagens INSS - Afastamento";
        } else if ($tipo == 6) {
            $dados = print_atestado($inicio, $fim, $localidade);
            $titulo = "Listagens Atestados(Por Período) ";
        } else if ($tipo == 7) {
            $dados = print_projetos($inicio, $fim, $localidade, $projetos);
            $titulo = "Listagens Colaboradores ativos (Por Localidade vs Projetos) ";
            $c = new Criteria();
            $c->addCondition('id', '=', $projetos);
            $Rhprojetos = Rhprojetos::getList($c);

            $json[] = null;
            $data[] = null;
            foreach ($Rhprojetos as $rhprojeto) {
                $data[] = [
                    "nome" => $rhprojeto->nome
                ];
            }
            $projetosJson = $json;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = $inicio;
        $json['relatorio']['fim'] = $fim;
        $json['relatorio']['dados'] = $dados;

    }


//------------RELATORIOS DE LISTAGEM ------------------//
    function relatorio_contratacao()
    {
        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_admitidos($inicio, $fim, $localidade);
        $titulo = "Listagens Contratação (Por Período)";


        $json = array();
        $itemData = array();
        $data = array();
        foreach ($dados as $item) {
            $itemData['codigo'] = (int)trim($item->codigo);
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['data'] = DataBR($item->datas);
            $itemData['local'] = $item->local_trabalho;
            $itemData['funcao'] = html_entity_decode($item->funcao);
            $itemData['descricao'] = html_entity_decode($item->observacao, ENT_QUOTES);
            $data[] = $itemData;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);
        $json['relatorio']['dados'] = $data;

        $reportCreate = new Json();
        $reportCreate->create('reportRelatorioContratacao.json', json_encode($json));
    }

    function relatorio_demissao()
    {
        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_demitidos($inicio, $fim, $localidade);
        $titulo = "Listagens Demissão (Por Período)";

        $json = array();
        $itemData = array();
        $data = array();
        foreach ($dados as $item) {
            $itemData['codigo'] = (int)trim($item->codigo);
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['data'] = DataBR($item->datas);
            $itemData['local'] = $item->local_trabalho;
            $itemData['funcao'] = html_entity_decode($item->funcao);
            $itemData['motivo'] = html_entity_decode($item->motivo, ENT_QUOTES);
            $data[] = $itemData;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);
        $json['relatorio']['dados'] = $data;

        $reportCreate = new Json();

        $reportCreate->create('reportRelatorioDemissao.json', json_encode($json));
    }

    function relatorio_ferias()
    {
        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_ferias($inicio, $fim, $localidade);
        $titulo = "Listagem - Férias a Conceder";

        $json = array();
        $itemData = array();
        $data = array();
        foreach ($dados as $item) {
            $itemData['codigo'] = (int)trim($item->codigo);
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['data'] = DataBR($item->datas);
            $itemData['local'] = $item->local_trabalho;
            $itemData['funcao'] = html_entity_decode($item->funcao);
            $itemData['descricao'] = html_entity_decode($item->descricao, ENT_QUOTES);
            $data[] = $itemData;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);
        $json['relatorio']['dados'] = $data;


        $reportCreate = new Json();

        $reportCreate->create('reportRelatorioFerias.json', json_encode($json));

    }

    function relatorio_aso()
    {

        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_aso($inicio, $fim, $historico, $localidade);
        $titulo = "Listagens - ASO (Período Vencimento - Ativos)";

        $json = array();
        $itemData = array();
        $data = array();
        foreach ($dados as $item) {
            $itemData['codigo'] = (int)trim($item->codigo);
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['data'] = DataBR($item->datas);
            $itemData['local'] = $item->local_trabalho;
            $itemData['funcao'] = html_entity_decode($item->funcao);
            $itemData['descricao'] = html_entity_decode($item->descricao, ENT_QUOTES);
            $data[] = $itemData;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);
        $json['relatorio']['dados'] = $data;

        $reportCreate = new Json();

        $reportCreate->create('reportRelatorioASO.json', json_encode($json));


    }

    function relatorio_inss()
    {

        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_inss($inicio, $fim, $localidade);
        $titulo = "Listagens INSS - Afastamento";

        $json = array();
        $itemData = array();
        $data = array();
        foreach ($dados as $item) {
            $itemData['codigo'] = (int)trim($item->codigo);
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['data'] = DataBR($item->datas);
            $itemData['local'] = $item->local_trabalho;
            $itemData['funcao'] = html_entity_decode($item->funcao);
            $itemData['descricao'] = html_entity_decode($item->descricao, ENT_QUOTES);
            $data[] = $itemData;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);
        $json['relatorio']['dados'] = $data;
        $reportCreate = new Json();

        $reportCreate->create('reportRelatorioINSS.json', json_encode($json));


    }

    function relatorio_atestados()
    {
        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_atestado($inicio, $fim, $localidade);
        $titulo = "Listagens Atestados(Por Período) ";

        $json = array();
        $itemData = array();
        $data = array();
        foreach ($dados as $item) {
            $itemData['codigo'] = (int)trim($item->codigo);
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['data'] = DataBR($item->datas);
            $itemData['local'] = $item->local_trabalho;
            $itemData['funcao'] = html_entity_decode($item->funcao);
            $itemData['descricao'] = html_entity_decode($item->descricao, ENT_QUOTES);
            $data[] = $itemData;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);
        $json['relatorio']['dados'] = $data;

        $reportCreate = new Json();

        $reportCreate->create('reportRelatorioAtestados.json', json_encode($json));
    }

    function relatorio_projetos()
    {
        $this->setTemplate('reports');
        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_projetos($inicio, $fim, $localidade, $projetos);
        $titulo = "Listagens Colaboradores ativos (Por Localidade vs Projetos) ";

        $c = new Criteria();
        $c->addCondition('id', '=', $projetos);
        $Rhprojetos = Rhprojetos::getFirst($c);

        $json = array();
        $itemData = array();
        $data = array();

        foreach ($dados as $item) {
            $itemData['codigo'] = (int)trim($item->codigo);
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['data'] = DataBR($item->datas);
            $itemData['local'] = $item->local_trabalho;
            $itemData['funcao'] = html_entity_decode($item->funcao);
            $itemData['descricao'] = html_entity_decode($item->observacao, ENT_QUOTES);
            $data[] = $itemData;
        }

        $json['relatorio']['titulo'] = $titulo;
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);
        $json['relatorio']['dados'] = $data;
        $json['relatorio']['projeto'] = $Rhprojetos->nome;

        $reportCreate = new Json();

        $reportCreate->create('reportRelatorioProjetos.json', json_encode($json));

    }

    function exportar_atestados(){

        $this->setTitle('Relatórios');
        $inicio = $_GET['inicio'];
        $fim = $_GET['fim'];
        $tipo = $_GET['tipo'];
        $historico = $_GET['historico'];
        $localidade = $_GET['localidade'];
        $projetos = $_GET['Rhprojetos'];

        $dados = print_atestado($inicio, $fim, $localidade);
        $titulo = "Listagens Atestados(Por Período) ";

        $json = array();
        $itemData = array();
        $data = array();


        $header = array(
            'Funcionarios',
            'Cargo',
            'Cidade',
            'Admissao',
            'Endereço',
            'Cep',
            'Nascimento',
            'RG',
            'Data Emissao RG',
            'CPF',
            'PIS',
            'CTPS',
            'Data Emissão CTPS',
            'Salario',
            'Telefone Particular',
        );

        foreach ($dados as $item) {
            $itemData['nome'] = html_entity_decode($item->nome, ENT_QUOTES);
            $itemData['cargo'] = $item->funcao;
            $itemData['cidade'] = $item->cidade;
            $itemData['admissao'] = DataBR($item->dtInicial);
            $itemData['endereco'] = $item->endereco.','.$item->numero.','.$item->bairro;
            $itemData['cep'] = $item->cep;
            $itemData['nascimento'] = DataBR($item->dtnascimento);
            $itemData['rg'] = $item->rg;
            $itemData['data_emissao_rg'] = DataBR($item->dtemissao);
            $itemData['cpf'] = $item->cpf;
            $itemData['pis'] = $item->pis;
            $itemData['ctps'] = $item->carteiratrabalho;
            $itemData['data_emissao_ctps'] = DataBR($item->dtcarteiratrabalho);
            $itemData['salario'] = 'R$ '.$item->valorremuneracao;
            $itemData['telefone'] = $item->celular01;
            $data[] = $itemData;
        }

        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="relatorio_atestados.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');

        $csvFile = fopen('php://output', 'w+');

        //força o excel usar a codificaçao utf8 NÃO APAGAR
        fputs( $csvFile, "\xEF\xBB\xBF");

        fputcsv($csvFile,$header,';');
        $total = count($data);
        if($total > 0)
            foreach ($data as $key => $value) {
                fputcsv($csvFile, $value, ';');
            }
        fclose($csvFile);

        exit();
    }

//------------FIM RELATORIOS DE LISTAGEM ------------------//


    function dss_cursos()
    {
        $this->setTemplate('empty');
        $this->setTitle('Relatórios');

        $c = new Criteria();
        $c->addCondition('id', '=', $this->getParam('id'));
        $cursos = Dss_curso::getList($c);

        $json[] = null;
        foreach ($cursos as $curso) {
            $json = [
                "id" => $curso->id,
                "tema" => $curso->tema,
                "descricao" => $curso->descricao,
                "palestrante" => $curso->palestrante,
                "localidade" => $curso->localidade,
                "empresa" => $curso->empresa,
                "divisaoProjeto" => $curso->divisao_projeto,
                "data_palestra" => $curso->data_palestra
            ];
        }
        $dados = (object)$json;
        $this->set('dados', $dados);
    }

    function sesmet_inspecao()
    {
        $this->setTemplate('empty');
        $this->setTitle('Relatórios');
        $c = new Criteria();
        $c->addCondition('id', '=', $this->getParam('id'));
        $inspecao = Sesmet_inspecao::getList($c);

        $jsonInpecao[] = null;
        foreach ($inspecao as $ins) {
            $jsonInpecao = [
                'id' => $ins->id,
                'nome' => $ins->nome,
                'empresa' => $ins->empresa,
                'localidade' => $ins->localidade,
                'equipe' => $ins->equipe,
                'servico' => $ins->servico,
                'veiculo' => $ins->veiculo,
                'encarregado' => $ins->encarregado,
                'responsavelVistoria' => $ins->responsavel_vistoria,
                'dataCadastro' => $ins->dt_cadastro,
                'observacao' => $ins->observacao
            ];
        }
        $inpessaoDados = (object)$jsonInpecao;
        //$this->set('inpessaoDados', $inpessaoDados); 

        $g = new Criteria();
        $g->addCondition('sesmet_inspecao_id', '=', $this->getParam('id'));
        //$g->addCondition('situacao_id','=', 1);
        $jsonFunc = [];
        $func = Sesmet_inspecao_funcionario::getList($g);
        foreach ($func as $f) {
            $jsonFunc[] = [
                'funCod' => $f->getRhprofissional()->codigo,
                'funcNome' => $f->getRhprofissional()->nome
            ];
        }
        $funcDados = (object)$jsonFunc;


        $tg = new Criteria();
        $tg->addCondition('sesmet_inspecao_id', '=', $this->getParam('id'));
        //$g->addCondition('situacao_id','=', 1);
        $jsonResp = [];
        $pergResp = Sesmet_inspecao_pergunta::getList($tg);


        foreach ($pergResp as $cg) {
            $grupo = $cg->getSesmet_pergunta()->getSesmet_grupo()->nome;
            $jsonResp[] = [
                'pergunta' => $cg->getSesmet_pergunta()->nome,
                'resposta' => $cg->getSesmet_resposta()->nome
            ];
        }
        $pergDados = (object)$jsonResp;

        $this->set('grupos', $grupo);
        $this->set('inpessaoDados', $inpessaoDados);
        $this->set('funcDados', $funcDados);
        $this->set('pergDados', $pergDados);
    }

    function reports_inspecao()
    {
        $this->setTemplate('reports');
        $inspecaoQueryId = (int)$this->getParam('id');
        $inspecaoQuery = new Criteria();
        $inspecaoQuery->addCondition('id', '=', (int)$this->getParam('id'));
        $inspecaoQuery->setOrder('id DESC');
        $inspecao = Sesmet_inspecao::getList($inspecaoQuery);

        foreach ($inspecao as $ins) {
            $json['inspecao'][] = [
                'id' => $ins->id,
                'nome' => $ins->nome,
                'empresa' => $ins->empresa,
                'localidade' => $ins->localidade,
                'equipe' => $ins->equipe,
                'servico' => $ins->servico,
                'veiculo' => $ins->veiculo,
                'encarregado' => $ins->encarregado,
                'responsavelVistoria' => $ins->responsavel_vistoria,
                'dataCadastro' => $ins->dt_cadastro,
                'observacao' => $ins->observacao
            ];
        }
        $func = Sesmet_inspecao_funcionario::getList();
        foreach ($func as $f) {
            $json['funcionario'][] = [
                'funCod' => $f->getRhprofissional()->codigo,
                'funcNome' => $f->getRhprofissional()->nome
            ];
        }

        $tg = new Criteria();
        $tg->addCondition('sesmet_inspecao_id', '=', $this->getParam('id'));
        $pergResp = Sesmet_inspecao_pergunta::getList($tg);
        foreach ($pergResp as $cg) {
            $grupo = $cg->getSesmet_pergunta()->getSesmet_grupo()->nome;
            $json['perguntas'][] = [
                'pergunta' => $cg->getSesmet_pergunta()->nome,
                'resposta' => $cg->getSesmet_resposta()->nome,
                'grupo' => $grupo
            ];
        }

        $reportCreate = new Json();
        $reportCreate->create('reportInspecao.json', json_encode($json));
        //$this->set('report', 'inspecao.mrt');   
    }

    function reports_rhprossional()
    { //Rhprofissional
        $this->setTemplate('reports');
        $profQueryId = (int)$this->getParam('id');
        $profQuery = new Criteria();
        $profQuery->addCondition('codigo', '=', (int)$this->getParam('id'));
        $profQuery->setOrder('codigo DESC');
        $rhprofissional = Rhprofissional::getList($profQuery);
        //Rhprofissional_contratacao
        $controtacaoQuery = new Criteria();
        $controtacaoQuery->addCondition('codigo_profissional', '=', (int)$this->getParam('id'));
        $controtacaoQuery->setOrder('codigo DESC');
        $contratacao = Rhprofissional_contratacao::getList($controtacaoQuery);
        //Rhprofissional_dependente status
        $dependententeQuery = new Criteria();
        $dependententeQuery->addCondition('codigo_profissional', '=', (int)$this->getParam('id'));
        $dependententeQuery->addCondition('status', '=', 1);
        $dependente = Rhprofissional_dependente::getList($dependententeQuery);


        foreach ($rhprofissional as $p) {
            $json['profissional'][] = [
                'nome' => $p->nome,
                'data_nas' => !empty($p->dtnascimento) ? DataBR($p->dtnascimento, "d/m/Y") : '-',
                'escolaridade' => $p->grauinstrucao,
                'naturalidade' => $p->naturalidade,
                'mae' => $p->mae,
                'pai' => $p->pai,
                'matricula' => $p->matricula,
                'sexo' => $p->sexo,
                'estadoCivil' => $p->estadocivil,
                'nacionalidade' => $p->nacionalidade,
                'setor' => $p->getRhsetor()->nome,
                'status' => $p->getStatus()->descricao,
                'endereco' => $p->endereco,
                'numeroCasa' => $p->numero,
                'bairro' => $p->bairro,
                'estado' => $p->estado,
                'cep' => $p->cep,
                'complemento' => $p->complemento,
                'cidade' => $p->cidade,
                'telRes' => $p->residencial,
                'telCorporativo' => $p->corporativo,
                'telRecado' => $p->recado,
                'celular' => $p->celular01,
                'celularCorporativo' => $p->celular02,
                'email' => $p->email,
                'rg' => $p->rg,
                'orgaoExpeditorRG' => $p->orgaoexped,
                'dataEmissaoRG' => !empty($p->dtemissao) ? DataBR($p->dtemissao, 'd/m/Y') : '-',
                'habilitacaoCategoria' => $p->habilitacao_categoria,
                'habilitacaoUf' => $p->habilitacao_uf,
                'habilitacaoOrgaoExped' => $p->habilitacao_orgao_exped,
                'habilitacaoEmissao' => !empty($p->habilitacao_emissao) ? DataBR($p->habilitacao_emissao) : '-',
                'habilitacaoVencimento' => !empty($p->habilitacao_vencimento) ? DataBR($p->habilitacao_vencimento, 'd/m/Y') : '-',
                'habilitacaoCarteiria' => $p->carteiriahabilitacao,
                'carteiraTrabalho' => $p->carteiratrabalho,
                'cpf' => $p->cpf,
                'pis' => $p->pis,
                'dtcarteiratrabalho' => !empty($p->dtcarteiratrabalho) ? DataBR($p->dtcarteiratrabalho, 'd/m/Y') : '-',
                'expeditorctps' => $p->expeditorctps,
                'reservistaNumero' => $p->reservista,
                'reservistaEmissao' => !empty($p->reservista_emissao) ? DataBR($p->reservista_emissao, 'd/m/Y') : '-',
                'reservistaCategoria' => $p->reservista_categoria,
                'tituloeleitor' => $p->tituloeleitor,
                'tituloeleitorEmissao' => !empty($p->tituloeleitor_emissao) ? DataBR($p->tituloeleitor_emissao, 'd/m/Y') : '-',
                'zona' => $p->zona,
                'secao' => $p->secao,
                'localidadeCidade' => $p->getLocalidade()->cidade,
                'localidadeEstado' => $p->getLocalidade()->estado
            ];
        }
        foreach ($contratacao as $c) {
            $json['contratacao'][] = [
                'conTipo' => $c->tipo,
                'conDtInicial' => !empty($c->dtInicial) ? DataBR($c->dtInicial, 'd/m/Y') : '-',
                'conDtFinal' => !empty($c->dtFinal) ? DataBR($c->dtFinal, 'd/m/Y') : '-',
                'conValetransporte' => $c->valetransporte,
                'conHorario' => $c->horario,
                'conFuncao' => $c->getRhprofissional_funcao()->nome,
                'conValetransporteqtdia' => $c->valetransporteqtdia,
                'conTiporemuneracao' => $c->tiporemuneracao,
                'convVlorremuneracao' => $c->valorremuneracao,
                'conJornada' => $c->jornada,
                'conAdicional' => $c->adicional,
                'conObservacao' => $c->observacao,
                'conSalarioBanco' => $c->banco,
                'conSalarioAgencia' => $c->bancoagencia,
                'conSalarioConta' => $c->bancoconta,
                'conSalarioOperacao' => $c->bancooperacao,
                'conPessoalBanco' => $c->banco2,
                'conPessoalAgencia' => $c->bancoagencia2,
                'conPessoalConta' => $c->bancoconta2,
                'conPessoalOperacao' => $c->bancooperacao2
            ];

        }
        foreach ($dependente as $d) {
            $json['dependentes'][] = [
                'dependenteNome' => $d->nome,
                'dependenteRegistro' => $d->registro,
                'dependenteCartaoVacinacao' => $d->cartaovacinacao,
                'dependenteTipo' => $d->tipo,
                'dependenteDataNascimento' => !empty($d->dtnascimento) ? DataBR($d->dtnascimento, 'd/m/Y') : '-',
                'dependenteDeclaracaoescolhar' => $d->declaracaoescolhar,
                'dependenteCPF' => $d->cpf,
                'dependenteNaturalidade' => $d->naturalidade,
                'dependenteImposteRenda' => $d->dependente_declaracao_imposto_renda
            ];

        }

        $reportCreate = new Json();
        $reportCreate->create('reportRhProfissional.json', json_encode($json));
    }
}