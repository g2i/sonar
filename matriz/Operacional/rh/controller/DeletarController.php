<?php
final class DeletarController extends AppController{ 

    # página inicial do módulo Deletar
    function index(){
        $this->setTitle('Visualização de Deletar');
    }

    # lista de Deletares
    # renderiza a visão /view/Deletar/all.php
    function all(){
        $this->setTitle('Listagem de Deletar');
        $p = new Paginate('Deletar', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Deletares', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Deletar
    # renderiza a visão /view/Deletar/view.php
    function view(){
        $this->setTitle('Visualização de Deletar');
        try {
            $this->set('Deletar', new Deletar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Deletar', 'all');
        }
    }

    # formulário de cadastro de Deletar
    # renderiza a visão /view/Deletar/add.php
    function add(){
        $this->setTitle('Cadastro de Deletar');
        $this->set('Deletar', new Deletar);
    }

    # recebe os dados enviados via post do cadastro de Deletar
    # (true)redireciona ou (false) renderiza a visão /view/Deletar/add.php
    function post_add(){
        $this->setTitle('Cadastro de Deletar');
        $Deletar = new Deletar();
        $this->set('Deletar', $Deletar);
        try {
            $Deletar->save($_POST);
            new Msg(__('Deletar cadastrado com sucesso'));
            $this->go('Deletar', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Deletar
    # renderiza a visão /view/Deletar/edit.php
    function edit(){
        $this->setTitle('Edição de Deletar');
        try {
            $this->set('Deletar', new Deletar((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Deletar', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Deletar
    # (true)redireciona ou (false) renderiza a visão /view/Deletar/edit.php
    function post_edit(){
        $this->setTitle('Edição de Deletar');
        try {
            $Deletar = new Deletar((int) $_POST['id']);
            $this->set('Deletar', $Deletar);
            $Deletar->save($_POST);
            new Msg(__('Deletar atualizado com sucesso'));
            $this->go('Deletar', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Deletar
    # renderiza a /view/Deletar/delete.php
    function delete(){
        $this->setTitle('Apagar Deletar');
        try {
            $this->set('Deletar', new Deletar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Deletar', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Deletar
    # redireciona para Deletar/all
    function post_delete(){
        try {
            $Deletar = new Deletar((int) $_POST['id']);
            $Deletar->delete();
            new Msg(__('Deletar apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Deletar', 'all');
    }

}