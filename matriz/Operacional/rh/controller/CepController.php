<?php
final class CepController extends AppController{

    function busca(){
        $cep = $_GET['cep'];
        //$reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
        $reg = simplexml_load_file("https://viacep.com.br/ws/". $cep . "/xml/");

//        $dados['resultado'] = (int) $reg->resultado;
//        $dados['endereco']     = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
//        $dados['logradouro']     =(string) $reg->logradouro;
//        $dados['tipo_logradouro']  =   (string) $reg->tipo_logradouro;
//        $dados['bairro']  = (string) $reg->bairro;
//        $dados['cidade']  = (string) $reg->cidade;
//        $dados['uf']  = (string) $reg->uf;

        $dados['endereco']     = (string) $reg->logradouro;
        $dados['bairro']  = (string) $reg->bairro;
        $dados['cidade']  = (string) $reg->localidade;
        $dados['uf']  = (string) $reg->uf;

        echo json_encode($dados);
        exit();
    }
}