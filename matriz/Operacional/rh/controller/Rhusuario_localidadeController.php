<?php
final class Rhusuario_localidadeController extends AppController{ 

    # página inicial do módulo husuario_localidade
    function index(){
        $this->setTitle('Visualização de Uhusuario_localidade');
    }

    # lista de Rhusuario_localidade
    # renderiza a visão /view/Rhusuario_localidade/all.php
    function all(){
        $this->setTitle('Listagem de Rhusuario_localidade');
        $p = new Paginate('Rhusuario_localidade', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhusuario_localidade', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhusuario_localidade
    # renderiza a visão /view/Rhusuario_localidade/view.php
    function view(){
        $this->setTitle('Visualização de Rhusuario_localidade');
        try {
            $this->set('Rhusuario_localidade', new Rhusuario_localidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhusuario_localidade', 'all');
        }
    }

    # formulário de cadastro de Rhusuario_localidade
    # renderiza a visão /view/Rhusuario_localidade/add.php
    function add(){
        $this->setTitle('Cadastro de Rhusuario_localidade');
        $this->set('Rhusuario_localidade', new Rhusuario_localidade);
        $this->set('Rhlocalidade',  Rhlocalidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhusuario_localidade
    # (true)redireciona ou (false) renderiza a visão /view/Rhusuario_localidade/add.php
    function post_add(){
        $this->setTitle('Cadastro de UsuaRhusuario_localidaderio_modulo');
        $Rhusuario_localidade = new Rhusuario_localidade();
        $this->set('Rhusuario_localidade', $Rhusuario_localidade);
        try {
            $Rhusuario_localidade->save($_POST);
            new Msg(__('Rhusuario_localidade cadastrado com sucesso'));
            $this->go('Rhusuario_localidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhlocalidade',  Rhlocalidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Rhusuario_localidade
    # renderiza a visão /view/Rhusuario_localidade/edit.php
    function edit(){
        $this->setTitle('Edição de Rhusuario_localidade');
        try {
            $this->set('Rhusuario_localidade', new Rhusuario_localidade((int) $this->getParam('id')));
            $this->set('Rhlocalidade',  Rhlocalidade::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhusuario_localidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhusuario_localidade
    # (true)redireciona ou (false) renderiza a visão /view/Rhusuario_localidade/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhusuario_localidade');
        try {
            $Rhusuario_localidade = new Rhusuario_localidade((int) $_POST['id']);
            $this->set('Rhusuario_localidade', $Rhusuario_localidade);
            $Rhusuario_localidade->save($_POST);
            new Msg(__('Rhusuario_localidade atualizado com sucesso'));
            $this->go('Rhusuario_localidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhlocalidade',  Rhlocalidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhusuario_localidade
    # renderiza a /view/Rhusuario_localidade/delete.php
    function delete(){
        $this->setTitle('Apagar Rhusuario_localidade');
        try {
            $this->set('Rhusuario_localidade', new Rhusuario_localidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhusuario_localidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhusuario_localidade
    # redireciona para Rhusuario_localidade/all
    function post_delete(){
        try {
            $Rhusuario_localidade = new Rhusuario_localidade((int) $_POST['id']);
            $Rhusuario_localidade->delete();
            new Msg(__('Rhusuario_localidade apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhusuario_localidade', 'all');
    }

}