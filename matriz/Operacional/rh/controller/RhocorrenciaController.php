<?php
final class RhocorrenciaController extends AppController{ 

    # página inicial do módulo Rhocorrencia
    function index(){
        $this->setTitle('Visualização de Ocorrência');
    }

    # lista de Rhocorrencias
    # renderiza a visão /view/Rhocorrencia/all.php
    function all(){
        $this->setTitle('Listagem de Ocorrência');
        $p = new Paginate('Rhocorrencia', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        if(!empty($_GET['historico'])){
            $c->addCondition('rhocorrencia_historico.codigo','=',$_GET['historico']);

        }
       
      

        //pega o id do profissional para listar
        if($this->getParam('id')){
            $c->addCondition('codigo_profissional','=',$this->getParam('id'));
            $this->set('Profissional', new Rhprofissional($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        }

        $c->setOrder('data DESC');
        $c->addCondition('status','!=', 3);
        $this->set('Rhocorrencias', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Usuario',  Usuario::getList());
        $this->set('Rhstatus',  Rhstatus::getList());

        $ocorrencias = new Rhocorrencia();
        $this->set('ocorrencias',$ocorrencias->getTabela_ocorrencia());



    }

    # visualiza um(a) Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/view.php
    function view(){
        $this->setTitle('Visualização de Ocorrência');
        try {
            $this->set('Rhocorrencia', new Rhocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhocorrencia', 'all');
        }
    }

    # formulário de cadastro de Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/add.php
    function add(){
        $c = new Criteria();
        $c->setOrder('nome');
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $g = new Criteria();
        $g->setOrder('nome');
        $g->addCondition('status','=', 1);
     
        $this->setTitle('Cadastro de Ocorrência');
        $this->set('Rhocorrencia', new Rhocorrencia);
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList($c));
        $this->set('Rhprofissionais',  Rhprofissional::getList($g));
    }

    # recebe os dados enviados via post do cadastro de Rhocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Rhocorrencia/add.php
    function post_add(){
        $this->setTitle('Cadastro de Ocorrência');
        $Rhocorrencia = new Rhocorrencia();
        $this->set('Rhocorrencia', $Rhocorrencia);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhocorrencia->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhocorrencia->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $Rhocorrencia->data_especifica=date('Y-m-d H:i:s');//salva a data especifica
        $_POST['status']=1;    
           //grava o valor para data especifica ja com a regra por periocidade
           $periodicidade = $_POST['tipoPeriodicidade'];
           //Anaual
           if($periodicidade == "Anual"){
               $dataEsp = $_POST['data']; 
               $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 years'));
           }
           if($periodicidade == "BiAnual"){
               $dataEsp = $_POST['data'];  
               $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+2 years'));
           }
           if($periodicidade == "Mensal"){ 
               $dataEsp = $_POST['data'];  
               $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 month'));
           }
           if($periodicidade == "Semestral"){
               $dataEsp = $_POST['data'];  
               $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+6 month'));
           }
           if($periodicidade == "Semanal"){
               $dataEsp = $_POST['data'];  
               $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 week'));
           }
           if($periodicidade == "Diaria"){
               $dataEsp = $_POST['data']; 
               $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 day'));
           }
   
        try {
            $Rhocorrencia->save($_POST);
            new Msg(__('Ocorrência cadastrada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            if(Session::get('dss')){
                $this->go('Sesmet', 'all');
            }else{
               $this->go('Rhocorrencia', 'all'); 
            }
            
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # formulário de edição de Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/edit.php
    function edit(){
        $this->setTitle('Edição de Ocorrência');
        $c = new Criteria();
        $c->setOrder('nome');
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        try {            
            $this->set('Rhocorrencia', new Rhocorrencia((int) $this->getParam('id')));
            $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList($c));
            $this->set('Rhprofissionais',  Rhprofissional::getList());
            $this->set('Rhstatus',  Rhstatus::getList());

        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhocorrencia', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Rhocorrencia/edit.php
    function post_edit(){
        $this->setTitle('Edição de Ocorrência');
        try {
            $user = Session::get('user');
            $Rhocorrencia = new Rhocorrencia((int) $_POST['codigo']);
            $this->set('Rhocorrencia', $Rhocorrencia);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhocorrencia->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhocorrencia->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhocorrencia->data_especifica=date('Y-m-d H:i:s');//salva a data especifica

        //grava o valor para data especifica ja com a regra por periocidade
        $periodicidade = $_POST['tipoPeriodicidade'];
        //Anaual
        if($periodicidade == "Anual"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 years'));
        }
        if($periodicidade == "BiAnual"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+2 years'));
        }
        if($periodicidade == "Mensal"){ 
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 month'));
        }
        if($periodicidade == "Semestral"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+6 month'));
        }
        if($periodicidade == "Semanal"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 week'));
        }
        if($periodicidade == "Diaria"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 day'));
        }
        if($periodicidade == "Nenhuma"){
            
            $_POST['data_especifica'] = (NULL);
        }

            $Rhocorrencia->save($_POST);
            new Msg(__('Ocorrência atualizada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhocorrencia', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhocorrencia
    # renderiza a /view/Rhocorrencia/delete.php
    function delete(){
        $this->setTitle('Apagar Rhocorrencia');
        try {
            $this->set('Rhocorrencia', new Rhocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhocorrencia', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhocorrencia
    # redireciona para Rhocorrencia/all
    function post_delete(){
        try {
            $Rhocorrencia = new Rhocorrencia((int) $_POST['id']);
            $Rhocorrencia->status=3;
            $Rhocorrencia->save();
            new Msg(__('Ocorrência apagada com sucesso'), 1);
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprofissional', 'all');
    }

     # formulário de edição de Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/renovar.php
    function renovar(){
        $this->setTitle('Renovar de Ocorrência');
        $c = new Criteria();
        $c->setOrder('nome');
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        try {            
            $this->set('Rhocorrencia', new Rhocorrencia((int) $this->getParam('id')));
            $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList($c));
            $this->set('Rhprofissionais',  Rhprofissional::getList());
            $this->set('Rhstatus',  Rhstatus::getList());

        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhocorrencia', 'all');
        }
    }
     # recebe os dados enviados via post da edição de Rhocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Rhocorrencia/renovar.php
    function post_renovar(){ 

        $this->setTitle('Renovar de Ocorrência');
        try {
            //altera o registro antigo setando para renovado
            $ocorrenciaID = $_POST['codigo'];
            $this->query("UPDATE rhocorrencia r 
            SET r.status = 4
            WHERE r.`codigo` = $ocorrenciaID");


            $Rhocorrencia = new Rhocorrencia();
            $this->set('Rhocorrencia', $Rhocorrencia);
            $Rhocorrencia->status = 1;
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhocorrencia->cadastradopor=$user->id; // para salvar o usuario que está fazendo
            $Rhocorrencia->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhocorrencia->data_especifica=date('Y-m-d H:i:s');//salva a data especifica
         
        //grava o valor para data especifica ja com a regra por periocidade
        $periodicidade = $_POST['tipoPeriodicidade'];
        //Anaual
        
        if($periodicidade == "Anual"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 years'));
        }
        if($periodicidade == "BiAnual"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+2 years'));
        }
        if($periodicidade == "Mensal"){ 
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 month'));
        }
        if($periodicidade == "Semestral"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+6 month'));
        }
        if($periodicidade == "Semanal"){
            $dataEsp = $_POST['data'];  
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 week'));
        }
        if($periodicidade == "Diaria"){
            $dataEsp = $_POST['data']; 
            $_POST['data_especifica'] = date('Y-m-d', strtotime($dataEsp . '+1 day'));
        }
        if($periodicidade == "Nenhuma"){
            
            $_POST['data_especifica'] = (NULL);
        }
            $_POST['codigo'] = null;
            $Rhocorrencia->save($_POST);
            new Msg(__('Ocorrência atualizada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhocorrencia', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

}