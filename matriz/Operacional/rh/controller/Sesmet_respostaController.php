<?php
final class Sesmet_respostaController extends AppController{ 

    # página inicial do módulo Sesmet_resposta
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Sesmet_respostas
    # renderiza a visão /view/Sesmet_resposta/all.php
    function all(){
        $this->setTitle('Lista');
        $p = new Paginate('Sesmet_resposta', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Sesmet_respostas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Sesmet_resposta
    # renderiza a visão /view/Sesmet_resposta/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Sesmet_resposta', new Sesmet_resposta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_resposta', 'all');
        }
    }

    # formulário de cadastro de Sesmet_resposta
    # renderiza a visão /view/Sesmet_resposta/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Sesmet_resposta', new Sesmet_resposta);
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Sesmet_resposta
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_resposta/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Sesmet_resposta = new Sesmet_resposta();
        $this->set('Sesmet_resposta', $Sesmet_resposta);
        try {
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_resposta->situacao_id = 1;
            $Sesmet_resposta->cadastrado_por = $user->id;
            $Sesmet_resposta->dt_cadastro = date('Y-m-d H:i:s');
            $Sesmet_resposta->save($_POST);
            new Msg(__('Sesmet_resposta cadastrado com sucesso'));
            $this->go('Sesmet_resposta', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Sesmet_resposta
    # renderiza a visão /view/Sesmet_resposta/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Sesmet_resposta', new Sesmet_resposta((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Sesmet_resposta', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Sesmet_resposta
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_resposta/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Sesmet_resposta = new Sesmet_resposta((int) $_POST['id']);
            $this->set('Sesmet_resposta', $Sesmet_resposta);
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_resposta->modificado_por = $user->id;
            $Sesmet_resposta->dt_modificado = date('Y-m-d H:i:s');
            $Sesmet_resposta->save($_POST);
            new Msg(__('Sesmet_resposta atualizado com sucesso'));
            $this->go('Sesmet_resposta', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Sesmet_resposta
    # renderiza a /view/Sesmet_resposta/delete.php
    function delete(){
        $this->setTitle('Apagar Sesmet_resposta');
        try {
            $this->set('Sesmet_resposta', new Sesmet_resposta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_resposta', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Sesmet_resposta
    # redireciona para Sesmet_resposta/all
    function post_delete(){
        try {
            $Sesmet_resposta = new Sesmet_resposta((int) $_POST['id']);
            $Sesmet_resposta->delete();
            new Msg(__('Sesmet_resposta apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Sesmet_resposta', 'all');
    }

}