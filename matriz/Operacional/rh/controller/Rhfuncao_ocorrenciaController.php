<?php
final class Rhfuncao_ocorrenciaController extends AppController{ 

    # página inicial do módulo Rhfuncao_ocorrencia
    function index(){
        $this->setTitle('Visualização de Ocorrência');
    }

    # lista de Rhfuncao_ocorrencia
    # renderiza a visão /view/Rhfuncao_ocorrencia/all.php
    function all(){
        $this->setTitle('Listagem de OcorrêncRhfuncao_ocorrenciaia');
        $p = new Paginate('Rhfuncao_ocorrencia', 10);
        $c = new Criteria();       

        if(!empty($_GET['historico'])){
            $c->addCondition('rhocorrencia_historico.codigo','=',$_GET['historico']);
        }

        //pega o id do rhprofissional_funcao para listar
        if($this->getParam('id')){
            $c->addCondition('funcao_id','=',$this->getParam('id'));
            $this->set('rhprofissional_funcao', new Rhprofissional_funcao($this->getParam('id'))); //passa o parametro id para pegar apenas o objeto com o id selecionado(passado)
        }
        $c->addCondition('situacao_id','=', 1);
        $c->setOrder('funcao_id');
       

        $this->set('Rhfuncao_ocorrencia', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissional_funcao',  Rhprofissional_funcao::getList());

    }

    # visualiza um(a) Rhocorrencia
    # renderiza a visão /view/Rhocorrencia/view.php
    function view(){
        $this->setTitle('Visualização de Ocorrência');
        try {
            $this->set('Rhocorrencia', new Rhocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhocorrencia', 'all');
        }
    }

    # formulário de cadastro de Rhfuncao_ocorrencia
    # renderiza a visão /view/Rhfuncao_ocorrencia/add.php
    function add(){
        $c = new Criteria();
        $c->setOrder('nome');
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('status','=', 1);


        $g = new Criteria();
        $g->setOrder('nome');
        if ($this->getParam('orderBy')) {
            $g->setOrder($this->getParam('orderBy'));
        }
        $g->addCondition('status', '=', 1);
        $this->setTitle('Cadastro de Rhfuncao_ocorrencia');
        $this->set('Rhfuncao_ocorrencia', new Rhfuncao_ocorrencia);
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList($g));
        $this->set('Rhprofissional_funcao',  Rhprofissional_funcao::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Rhfuncao_ocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Rhfuncao_ocorrencia/add.php
     function post_add(){
        $this->setTitle('Cadastro de Ocorrência');
        $Rhfuncao_ocorrencia = new Rhfuncao_ocorrencia();
        $this->set('Rhfuncao_ocorrencia', $Rhfuncao_ocorrencia);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhfuncao_ocorrencia->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhfuncao_ocorrencia->dt_cadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['situacao_id']=1;
        try {
            $Rhfuncao_ocorrencia->save($_POST);
            new Msg(__('Ocorrência cadastrada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhfuncao_ocorrencia', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissional_funcao',  Rhprofissional_funcao::getList());
    }
    # formulário de edição de Rhfuncao_ocorrencia
    # renderiza a visão /view/Rhfuncao_ocorrencia/edit.php
    function edit(){
        $this->setTitle('Edição de Rhfuncao Ocorrencia');
        try {        
            $this->set('Rhfuncao_ocorrencia', new Rhfuncao_ocorrencia((int) $this->getParam('id')));
            $g = new Criteria();
            $g->setOrder('nome');
            if ($this->getParam('orderBy')) {
                $g->setOrder($this->getParam('orderBy'));
            }
            $g->addCondition('status', '=', 1);
            $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList($g));
            $c = new Criteria();
            $c->setOrder('nome');
            if ($this->getParam('orderBy')) {
                $c->setOrder($this->getParam('orderBy'));
            }
            $c->addCondition('status','=', 1);
            $this->set('Rhprofissional_funcao',  Rhprofissional_funcao::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhfuncao_ocorrencia', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhfuncao_ocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Rhfuncao_ocorrencia/edit.php
    function post_edit(){
        $this->setTitle('Edição de Ocorrência');
            try {
        
            $Rhfuncao_ocorrencia = new Rhfuncao_ocorrencia((int) $_POST['id']);
            $this->set('Rhfuncao_ocorrencia', $Rhfuncao_ocorrencia);
            $Rhfuncao_ocorrencia->dt_modificacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhfuncao_ocorrencia->save($_POST);
            new Msg(__('Atualizada com sucesso'));
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Rhfuncao_ocorrencia', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $this->set('Rhprofissional_funcao',  Rhprofissional_funcao::getList());
    }
    # Confirma a exclusão ou não de um(a) Rhfuncao_ocorrencia
    # renderiza a /view/Rhfuncao_ocorrencia/delete.php
    function delete(){
        $this->setTitle('Apagar Rhocorrencia');
        try {
            $this->set('Rhfuncao_ocorrencia', new Rhfuncao_ocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhfuncao_ocorrencia', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhfuncao_ocorrencia
    # redireciona para Rhfuncao_ocorrencia/all
    function post_delete(){
        try {
            $Rhfuncao_ocorrencia = new Rhfuncao_ocorrencia((int) $_POST['id']);
            
            $Rhfuncao_ocorrencia->situacao_id=3;
            
            $Rhfuncao_ocorrencia->save();
            new Msg(__('Ocorrência apagada com sucesso'), 1);
            //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprofissional_funcao', 'all');
    }

}