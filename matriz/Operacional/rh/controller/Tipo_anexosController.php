<?php
final class Tipo_anexosController extends AppController{ 

    # página inicial do módulo Tipo_anexos
    function index(){
        $this->setTitle('Visualização de Tipo_anexos');
    }

    # lista de Tipo_anexos
    # renderiza a visão /view/Tipo_anexos/all.php
    function all(){
        $this->setTitle('Listagem de Tipo_anexos');
        $p = new Paginate('Tipo_anexos', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Tipo_anexos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Tipo_anexos
    # renderiza a visão /view/Tipo_anexos/view.php
    function view(){
        $this->setTitle('Visualização de Tipo_anexos');
        try {
            $this->set('Tipo_anexos', new Tipo_anexos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tipo_anexos', 'all');
        }
    }

    # formulário de cadastro de Tipo_anexos
    # renderiza a visão /view/Tipo_anexos/add.php
    function add(){
        $this->setTitle('Cadastro de Tipo_anexos');
        $this->set('Tipo_anexos', new Tipo_anexos);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Tipo_anexos
    # (true)redireciona ou (false) renderiza a visão /view/Tipo_anexos/add.php
    function post_add(){
        $this->setTitle('Cadastro de Tipo_anexos');
        $Tipo_anexos = new Tipo_anexos();
        $this->set('Tipo_anexos', $Tipo_anexos);
        try {
            $Tipo_anexos->save($_POST);
            new Msg(__('Tipo_anexos cadastrado com sucesso'));
            $this->go('Tipo_anexos', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Tipo_anexos
    # renderiza a visão /view/Tipo_anexos/edit.php
    function edit(){
        $this->setTitle('Edição de Tipo_anexos');
        try {
            $this->set('Tipo_anexos', new Tipo_anexos((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Tipo_anexos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Tipo_anexos
    # (true)redireciona ou (false) renderiza a visão /view/Tipo_anexos/edit.php
    function post_edit(){
        $this->setTitle('Edição de Tipo_anexos');
        try {
            $Tipo_anexos = new Tipo_anexos((int) $_POST['id']);
            $this->set('Tipo_anexos', $Tipo_anexos);
            $Tipo_anexos->save($_POST);
            new Msg(__('Tipo_anexos atualizado com sucesso'));
            $this->go('Tipo_anexos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Tipo_anexos
    # renderiza a /view/Tipo_anexos/delete.php
    function delete(){
        $this->setTitle('Apagar Tipo_anexos');
        try {
            $this->set('Tipo_anexos', new Tipo_anexos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tipo_anexos', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Tipo_anexos
    # redireciona para Tipo_anexos/all
    function post_delete(){
        try {
            $Tipo_anexos = new Tipo_anexos((int) $_POST['id']);
            $Tipo_anexos->delete();
            new Msg(__('Tipo_anexos apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Tipo_anexos', 'all');
    }

}