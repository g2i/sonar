<?php
final class Sesmet_grupoController extends AppController{ 

    # página inicial do módulo Sesmet_grupo
    function index(){
        $this->setTitle('Visualização de Sesmet_grupo');
    }

    # lista de Sesmet_grupos
    # renderiza a visão /view/Sesmet_grupo/all.php
    function all(){
        $this->setTitle('Listagem de Sesmet_grupo');
        $p = new Paginate('Sesmet_grupo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
    
        $c->setOrder($this->getParam('id'));
    
        $this->set('Sesmet_grupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Sesmet_grupo
    # renderiza a visão /view/Sesmet_grupo/view.php
    function view(){
        $this->setTitle('Visualização de Sesmet_grupo');
        try {
            $this->set('Sesmet_grupo', new Sesmet_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_grupo', 'all');
        }
    }

    # formulário de cadastro de Sesmet_grupo
    # renderiza a visão /view/Sesmet_grupo/add.php
    function add(){
        $this->setTitle('Cadastro de Sesmet_grupo');
        $this->set('Sesmet_grupo', new Sesmet_grupo);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Sesmet_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_grupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Sesmet_grupo');
        $Sesmet_grupo = new Sesmet_grupo();
        $this->set('Sesmet_grupo', $Sesmet_grupo);
        try {
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_grupo->situacao_id = 1;
            $Sesmet_grupo->cadastrado_por = $user->id;
            $Sesmet_grupo->dt_cadastro = date('Y-m-d H:i:s');       
            $Sesmet_grupo->save($_POST);
            new Msg(__('Sesmet_grupo cadastrado com sucesso'));
            $this->go('Sesmet_grupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Sesmet_grupo
    # renderiza a visão /view/Sesmet_grupo/edit.php
    function edit(){
        $this->setTitle('Edição de Sesmet_grupo');
        try {
            $this->set('Sesmet_grupo', new Sesmet_grupo((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Sesmet_grupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Sesmet_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_grupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Sesmet_grupo');
        try {
            $Sesmet_grupo = new Sesmet_grupo((int) $_POST['id']);
            $this->set('Sesmet_grupo', $Sesmet_grupo);
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_grupo->modificado_por = $user->id;
            $Sesmet_grupo->dt_modificado = date('Y-m-d H:i:s');
            $Sesmet_grupo->save($_POST);
            new Msg(__('Sesmet_grupo atualizado com sucesso'));
            $this->go('Sesmet_grupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Sesmet_grupo
    # renderiza a /view/Sesmet_grupo/delete.php
    function delete(){
        $this->setTitle('Apagar Sesmet_grupo');
        try {
            $this->set('Sesmet_grupo', new Sesmet_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_grupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Sesmet_grupo
    # redireciona para Sesmet_grupo/all
    function post_delete(){
        try {
            $Sesmet_grupo = new Sesmet_grupo((int) $_POST['id']);
            $Sesmet_grupo->delete();
            new Msg(__('Sesmet_grupo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Sesmet_grupo', 'all');
    }

}