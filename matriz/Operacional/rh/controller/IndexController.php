<?php

class IndexController extends AppController
{
    public function index()
    {
   
        /**
         * redireciona para cursos quando vir do link da pagina central 
         */
        if(Session::get('dss')) {
            $this->go('Dss_curso', 'all');
        }
       
        $this->setTitle('Início');

        $prof_ativos = $this->query_count("SELECT COUNT(DISTINCT(P.codigo)) AS total FROM rhprofissional P
                        LEFT JOIN rhprofissional_contratacao C
                        ON C.codigo_profissional = P.codigo
                        WHERE C.tipo <> 'Demissão' AND P.status <> 3
                        AND IF(C.dtFinal IS NULL,C.dtFinal IS NULL,C.dtFinal >= CURDATE())");

        $this->set('prof_ativos',$prof_ativos);

        $demissoes = $this->query_count("SELECT COUNT(DISTINCT(P.codigo)) as total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Demissão' AND C.dtFinal >= DATE_SUB(CURDATE(),INTERVAL 30 DAY)");

        $this->set('demissoes',$demissoes);

        $admissao = $this->query_count("SELECT COUNT(P.codigo) AS total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Admissão' AND
                      C.dtInicial >= DATE_SUB(CURDATE(),INTERVAL 30 DAY) ");

        $this->set('admissao',$admissao);



        $prim_periodo= $this->query_count("SELECT COUNT(P.codigo) AS total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 45 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 45 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('prim_periodo',$prim_periodo);

        $seg_periodo= $this->query_count("SELECT COUNT(P.codigo) AS total FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 90 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 90 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('seg_periodo',$seg_periodo);


            $ocorencias_vencidas = $this->query_count("SELECT COUNT(o.`codigo_profissional`) AS totale
            FROM rhocorrencia o
            LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
            LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
            LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
            WHERE o.`status` != 3
            AND o.`status` != 4
            AND p.`status` != 3
            AND oh.codigo != 27
            AND oh.codigo != 28
            AND CURDATE() > o.`data_especifica`
            AND oh.`periodicidade` != 'Nenhuma'
            AND C.`tipo` != 'Demissão'");
                  
        $this->set('ocorencias_vencidas',$ocorencias_vencidas);

        $ocorrencias = new Rhocorrencia();
        $this->set('ocorrencias',$ocorrencias->getTabela_ocorrencia());
        //getRhocorrencia
        $ocor = new Rhocorrencia();
        $this->set('ocor',$ocor->getRhocorrencia());


           }


    function setor(){
            $setor = $this->query("SELECT S.nome as label, COUNT(P.codigo) as value
                        FROM rhprofissional P
                          INNER JOIN rhsetor S
                          on S.codigo = P.setor
                        WHERE P.setor IS NOT NULL GROUP BY P.setor");

            echo json_encode($setor);
        exit;
    }

 

    function seg_periodo(){
        $this->setTitle('Segundo período');
        $periodo = $this->query("SELECT P.codigo,P.nome,F.nome as funcao, C.`local_trabalho`, C.dtInicial, DATE_ADD(C.dtInicial,INTERVAL 90 DAY) as termino  FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      LEFT join rhprofissional_funcao F
                      on F.codigo = C.funcao
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 90 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 90 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('seg_periogo',$periodo);
    }
    function prim_periodo(){
        $this->setTitle('Primeiro período');
        $periodo = $this->query("SELECT P.codigo,P.nome,F.nome as funcao, C.`local_trabalho`, C.dtInicial, DATE_ADD(C.dtInicial,INTERVAL 90 DAY) as termino  FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      LEFT join rhprofissional_funcao F
                      on F.codigo = C.funcao
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 45 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 45 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('prim_periogo',$periodo);
    }

    function admissao (){
        $this->setTitle('Admissão');
        $admissao =  $this->query("SELECT  P.nome, C.`dtInicial` AS contratacao, 
        C.`local_trabalho`, F.nome AS funcao 
        FROM rhprofissional P
        LEFT JOIN rhprofissional_contratacao C ON C.codigo_profissional = P.codigo
        LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
        WHERE C.tipo = 'Admissão' AND
        C.dtInicial >= DATE_SUB(CURDATE(),INTERVAL 30 DAY)");
    
        $this->set('admissao',$admissao);
    }
    function demissao(){
        $this->setTitle('Demissão');
        $demissao = $this->query("SELECT  P.nome, C.`dtFinal` AS contratacao, 
        C.`local_trabalho`, F.nome AS funcao 
        FROM rhprofissional P
        LEFT JOIN rhprofissional_contratacao C ON C.codigo_profissional = P.codigo
        LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
        WHERE C.tipo = 'Demissão' AND C.dtFinal >= DATE_SUB(CURDATE(),INTERVAL 30 DAY)");

        $this->set('demissao', $demissao);
    }
}