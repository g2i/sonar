
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Sesmet_pergunta</h2>
    <ol class="breadcrumb">
    <li>Sesmet_pergunta</li>
    <li class="active">
    <strong>Editar Sesmet_pergunta</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Sesmet_pergunta', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Sesmet_pergunta->nome ?>" placeholder="Nome">
        </div>     
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="sesmet_grupo">Inspeções Grupo</label>
            <select name="sesmet_grupo" class="form-control" id="sesmet_grupo">
                <?php
                foreach ($Sesmet_grupos as $s) {
                    if ($s->id == $Sesmet_pergunta->sesmet_grupo)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Sesmet_pergunta->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>