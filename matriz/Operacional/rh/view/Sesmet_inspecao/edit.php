<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Inspeções</h2>
        <ol class="breadcrumb">
            <li>Inspeções</li>
            <li class="active">
                <strong>Continuar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Sesmet_inspecao', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Sesmet_inspecao->nome ?>"
                                    placeholder="Nome">
                            </div>                             
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="empresa">Empresa</label>
                                <input type="text" name="empresa" id="empresa" class="form-control" value="<?php echo $Sesmet_inspecao->empresa ?>"
                                    placeholder="Empresa">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="localidade">Localidade</label>
                                <input type="text" name="localidade" id="localidade" class="form-control" value="<?php echo $Sesmet_inspecao->localidade ?>"
                                    placeholder="Localidade">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="equipe">Equipe</label>
                                <input type="text" name="equipe" id="equipe" class="form-control" value="<?php echo $Sesmet_inspecao->equipe ?>"
                                    placeholder="Equipe">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="encarregado">Encarregado</label>
                                <input type="text" name="encarregado" id="encarregado" class="form-control" value="<?php echo $Sesmet_inspecao->encarregado ?>"
                                    placeholder="Encarregado">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="responsavel_vistoria">Responsável vistoria</label>
                                <input type="text" name="responsavel_vistoria" id="responsavel_vistoria" class="form-control"
                                    value="<?php echo $Sesmet_inspecao->responsavel_vistoria ?>" placeholder="responsável vistoria">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="servico">Serviço</label>
                                <input type="text" name="servico" id="servico" class="form-control" value="<?php echo $Sesmet_inspecao->servico ?>"
                                    placeholder="Serviço">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="veiculo">Veículo</label>
                                <input type="text" name="veiculo" id="veiculo" class="form-control" value="<?php echo $Sesmet_inspecao->veiculo ?>"
                                    placeholder="Veículo">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="observacao">Observação</label>
                                <input type="text" name="observacao" id="observacao" class="form-control" value="<?php echo $Sesmet_inspecao->observacao ?>" placeholder="observacao">
                            </div>  
                            <div class="clearfix"></div>    
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">      
                                    <label for="#"></label>   <br>                       
                                    <?php                                                
                                    echo $this->Html->getLink('<span class="fa fa-plus-square" title="Perguntas" style="color: green; font-size: 15pt"> Perguntas</span> ', 'Sesmet_inspecao_pergunta', 'all',
                                    array('id' => $Sesmet_inspecao->id,'first' => 1,'modal' => 1),
                                    array('class' => 'btn btn-sm','data-toggle' => 'modal'));  
                                
                                    echo $this->Html->getLink('<span class="fa fa-plus-square" title="Funcionario" style="color: green; font-size: 15pt">  Funcionario </span> ', 'Sesmet_inspecao_funcionario', 'all',
                                    array('id' => $Sesmet_inspecao->id,'first' => 1,'modal' => 1),
                                    array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                                    ?>
                                </div>   
                                <div class="clearfix"></div>                    
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Sesmet_inspecao->id;?>">
                        <div class="text-right">
                        <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <a href="<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all') ?>" class="btn btn-primary" data-dismiss="modal">Salvar</a>
                        <!-- <input type="submit" class="btn btn-primary" value="salvar">-->
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>