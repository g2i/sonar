
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Inspeções</h2>
    <ol class="breadcrumb">
    <li>Inspeções</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                <div class="col-md-3 form-group">
                    <label for="nome">Nome</label>
                    <input type="text" name="filtro[interno][nome]" id="nome" class="form-control" value="<?php echo $this->getParam('nome'); ?>">
                </div>
                <div class="col-md-3 form-group">
                    <label for="situacao_id">Situação</label>
                    <select name="filtro[externo][situacao_id]" class="form-control" id="situacao_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Situacaos as $s): ?>
                            <?php echo '<option value="' . $s->id . '">' . $s->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="nome">Encarregado</label>
                    <input type="text" name="filtro[interno][encarregado]" id="encarregado" class="form-control" value="<?php echo $this->getParam('encarregado'); ?>">
                </div>
                <div class="col-md-3 form-group">
                    <label for="nome">Veículo</label>
                    <input type="text" name="filtro[interno][veiculo]" id="veiculo" class="form-control" value="<?php echo $this->getParam('veiculo'); ?>">
                </div>
                <div class="col-md-12 text-right">
                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>" class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Sesmet_inspecao', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'situacao_id')); ?>'>
                        Situação
                    </a>
                </th>                
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                    Data de cadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'modificado_por')); ?>'>
                    Equipe
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'dt_modificado')); ?>'>
                    Encarregado
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'dt_modificado')); ?>'>
                    Responsável
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'dt_modificado')); ?>'>
                    Veículo 
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => 'dt_modificado')); ?>'>
                    Observação 
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Sesmet_inspecaos as $s) {
                echo '<tr>';
                echo '<td>';
                echo $s->id; 
                echo '</td>';
                echo '<td>';
                echo $s->nome; 
                echo '</td>';
                echo '<td>';
                echo $s->getSituacao()->nome; 
                echo '</td>';
                echo '<td>';
                echo DataBR($s->dt_cadastro); 
                echo '</td>';
                echo '<td>';
                echo $s->equipe; 
                echo '</td>';
                echo '<td>';
                echo $s->encarregado; 
                echo '</td>'; 
                echo '<td>';
                echo $s->responsavel_vistoria; 
                echo '</td>';       
                echo '<td>';
                echo $s->veiculo; 
                echo '</td>';   
                echo '<td>';
                echo $s->observacao; 
                echo '</td>';      
                //*** */
                echo '<td class="actions text-right">';
                echo '<div class="dropdown">';
                   echo '<button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                    <span class="caret"></span></button>';       
                    echo '<ul class="dropdown-menu">';
                    echo '<li>';
                        echo $this->Html->getLink('<span class="fa fa-print" target="_blank"> Imprimir</span> ', 'Rhrelatorio', 'reports_inspecao', 
                        array('id' => $s->id),
                        array('target' => '_blank'));
                    echo '</li>';                                             
                        /*echo '<li>';
                            echo $this->Html->getLink('<span class="fa fa-print" target="_blank"> Imprimir</span> ', 'Rhrelatorio', 'sesmet_inspecao', 
                            array('id' => $s->id),
                            array('target' => '_blank'));
                        echo '</li>';*/
                        echo '<li>';
                            echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"> Editar</span> ', 'Sesmet_inspecao', 'edit', 
                            array('id' => $s->id), 
                            array('class' => 'btn btn-sm'));
                        echo '</li>';
                        echo '<li>';
                            echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"> Delete</span> ', 'Sesmet_inspecao', 'delete', 
                            array('id' => $s->id), 
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</li>';
                    echo '</ul>';
                echo '</div>';
                echo '</td>';
                /*** */
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Sesmet_inspecao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Sesmet_inspecao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>