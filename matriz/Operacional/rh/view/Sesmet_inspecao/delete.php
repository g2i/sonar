<form class="form" method="post" action="<?php echo $this->Html->getUrl('Sesmet_inspecao', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Sesmet_inspecao->nome; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Sesmet_inspecao->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Sesmet_inspecao', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>