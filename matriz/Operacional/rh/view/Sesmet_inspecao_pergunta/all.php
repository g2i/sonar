
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Inpeção Peguntas/Respostas</h2>
    <ol class="breadcrumb">
    <li>Inpeção Peguntas/Respostas</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro 
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Sesmet_inspecao_pergunta', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>-->

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao_pergunta', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao_pergunta', 'all', array('orderBy' => 'sesmet_pergunta_id')); ?>'>
                        Pergunta
                    </a>
                </th>              
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao_pergunta', 'all', array('orderBy' => 'sesmet_resposta_id')); ?>'>
                        Resposta
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Sesmet_inspecao_perguntas as $s) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($s->id, 'Sesmet_inspecao_pergunta', 'view',
                    array('id' => $s->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';              
                echo '<td>';
                echo $this->Html->getLink($s->getSesmet_pergunta()->nome, 'Sesmet_pergunta', 'view',
                    array('id' => $s->getSesmet_pergunta()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>'; 
                //*
                echo '<td>'; ?>          
                    <a href="#" class="edit_local" data-controller="Sesmet_inspecao_pergunta" data-type="select"
                    data-name="sesmet_resposta_id" data-pk="<?php echo $s->id  ?>"
                    data-value="<?php echo $s->sesmet_resposta_id  ?>" 
                    data-source="<?php echo $Sesmet_respostaDataSource ?>">
                     <?php echo $s->getSesmet_resposta()->nome; ?>
                    </a>
                <?php
                echo '</td>';                            
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>  
    /* faz a pesquisa com ajax */
    $(document).ready(function() {

        $(document).find(".edit_local").each(function () {
            input_editable($(this));
        });
        
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Sesmet_inspecao_pergunta', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Sesmet_inspecao_pergunta', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>