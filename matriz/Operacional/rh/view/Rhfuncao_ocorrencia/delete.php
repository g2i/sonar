<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <form class="form" method="post" action="<?php echo $this->Html->getUrl('Rhfuncao_ocorrencia', 'delete') ?>">
                        <h1>Confirmação</h1>

                        <div class="well well-lg">
                            <p>Voce tem certeza que deseja excluir o registro
                                <strong><?php echo $Rhfuncao_ocorrencia->id; ?></strong>?</p>
                        </div>
                        <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                        <?php if ($this->getParam('modal')) { ?>
                            <input type="hidden" name="modal" value="1"/>
                            <input type="hidden" name="id" value="<?php echo $Rhfuncao_ocorrencia->id; ?>"
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-danger" value="Excluir">
        </div>
    <?php } else { ?>
                            <div class="text-right">
                                <input type="hidden" name="id" value="<?php echo $Rhfuncao_ocorrencia->id; ?>">
                                <a href="<?php echo $this->Html->getUrl('Rhfuncao_ocorrencia', 'all') ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-danger" value="Excluir">
                            </div>
                        <?php } ?>

                        <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                    </form>
                </div>
                <!-- /ibox content -->
            </div>
            <!-- / ibox -->
        </div>
        <!--wrapper-->
    </div>
    <!-- /col-lg 12 -->
</div><!-- /row -->s