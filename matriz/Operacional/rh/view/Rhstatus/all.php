
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="codigo">Código</label>
            <input type="number" name="filtro[interno][codigo]" id="codigo" class="form-control maskInt" value="<?php echo $this->getParam('codigo'); ?>">
        </div>
        <div class="col-md-3 form-group">
            <label for="descricao">Descrição</label>
            <input type="text" name="filtro[interno][descricao]" id="descricao" class="form-control" value="<?php echo $this->getParam('descricao'); ?>">
        </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
<div>
    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Status', 'Rhstatus', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhstatus', 'all', array('orderBy' => 'codigo')); ?>'>
                        Código
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhstatus', 'all', array('orderBy' => 'descricao')); ?>'>
                        Descricao
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Rhstatus as $r) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($r->codigo, 'Rhstatus', 'view',
                    array('id' => $r->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->descricao, 'Rhstatus', 'view',
                    array('id' => $r->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Rhstatus', 'edit', 
                    array('id' => $r->codigo), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Rhstatus', 'delete', 
                    array('id' => $r->codigo), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Rhstatus', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Rhstatus', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>