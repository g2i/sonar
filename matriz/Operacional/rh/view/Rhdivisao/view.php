<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">
                <legend>Divisão</legend>

                <p><strong>Nome</strong>: <?php echo $Rhdivisao->nome; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Rhdivisao->getRhstatus()->descricao, 'Rhstatus', 'view',
                        array('id' => $Rhdivisao->getRhstatus()->codigo), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Departamento</strong>:
                    <?php
                    echo $this->Html->getLink($Rhdivisao->getRhdepartamento()->nome, 'Rhdepartamento', 'view',
                        array('id' => $Rhdivisao->getRhdepartamento()->codigo), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

            </div>
            <!-- /ibox content -->
        </div>
        <!-- / ibox -->
    </div>
    <!-- /col-lg 12 -->
</div><!-- /row -->
