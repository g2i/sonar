<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <?php if ($this->getParam('modal')){ ?>
                        <div class="text-right" style="padding-bottom: 30px">
                            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;
                            </button>
                        </div>
                    <?php }else{ ?>
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                           value="<?php echo $this->getParam('nome'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tipo_anexo_id">Tipo Anexo</label>
                                    <select name="filtro[externo][tipo_anexo_id]" class="form-control"
                                            id="tipo_anexo_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Tipo_anexos as $r): ?>
                                            <?php echo '<option value="' . $r->id . '">' . $r->nome . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-impressao"><span
                                                class="glyphicon glyphicon-print"></span></button>
                                    <button type="button" class="btn btn-default botao-reset"><span
                                                class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span
                                                class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                    <div>
                        <?php } ?>
                        <?php if ($this->getParam('modal')) { ?>
                            <div class="text-left col-md-6 ">
                                <h3 class="text-left"><?php echo $Profissional->nome; ?></h3>
                            </div>
                            <div class="text-right col-md-6">
                                <p>
                                    <a href="Javascript:void(0)" class="btn btn-default"
                                       onclick="Navegar('<?php echo $this->Html->getUrl("Anexoprofissional", "add", array('modal' => 1, 'ajax' => true, 'id' => $this->getParam('id'))) ?>','go')">
                                        <span class="img img-add"></span> Novo Anexo</a>
                                </p>
                            </div>
                        <?php } else { ?>
                            <!-- botao de cadastro -->
                            <div class="text-right">
                                <p>
                                    <?= $this->Html->getLink('<span class="img img-add"></span>Novo Anexo', 'Anexoprofissional', 'add',
                                        array('id' => $this->getParam('id')),
                                        array('class' => 'btn btn-default')); ?>
                                </p>
                            </div>
                        <?php } ?>

                        <!-- tabela de resultados -->
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            <a>
                                                Nome
                                            </a>
                                        </th>
                                        <th>
                                            <a>
                                                Tipo Anexo
                                            </a>
                                        </th>
                                        <th>
                                            <a>
                                                Profissional
                                            </a>
                                        </th>
                                        <th>
                                            <a>
                                                Dt.Cadastro
                                            </a>
                                        </th>
                                        <th>
                                            <a>
                                                Cadastrado por
                                            </a>
                                        </th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    foreach ($Anexoprofissionais as $a) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->nome, 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->getTipoAnexo()->nome, 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->getRhprofissional()->nome, 'Rhprofissional', 'view',
                                            array('id' => $a->getRhprofissional()->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink(DataBR($a->dtCadastro), 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->getCadastradoPor()->nome, 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo $this->Html->getLink('<span class="img img-edit"></span> ', 'Anexoprofissional', 'edit',
                                            array('id' => $a->codigo),
                                            array('class' => 'btn  btn-sm'));
                                        echo '</td>';

                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';

                                        echo $this->Html->getLink('<span class="img img-remove"></span> ', 'Anexoprofissional', 'delete',
                                            array('id' => $a->codigo, "ajax" => true, "modal" => "1", 'first' => 1),
                                            array('class' => 'btn  btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';

                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo $this->Html->getLink('<span class="img img-down"></span> ', 'Download', 'baixar',
                                            array('u' => Cript::cript($a->anexo)),
                                            array('class' => 'btn btn-sm'));
                                        echo '</td>';

                                    }
                                    ?>
                                </table>

                                <!-- menu de paginação -->
                                <div style="text-align:center"><?php echo $nav; ?></div>
                            </div>
                        </div>
                    </div><!-- /ibox content -->
                </div><!-- / ibox -->
            </div><!-- wrapper -->
        </div> <!-- /col-lg 12 -->
    </div><!-- /row -->

    <script>
        /* faz a pesquisa com ajax */
        $(document).ready(function () {
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Anexoprofissional', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Anexoprofissional', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });
        });
    </script>