<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexo </h2>
        <ol class="breadcrumb">
            <li>Anexo</li>
            <li class="active">
                <strong>All</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-12" style="margin-top: 50px">
        <form role="form"
              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('id' => $this->getParam('id'))) ?>"
              method="post" enctype="application/x-www-form-urlencoded">
            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
            <input type="hidden" name="p" value="<?php echo ACTION; ?>">

            <div class="col-md-3 form-group">
                <label for="nome">Nome</label>
                <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                       value="<?php echo $this->getParam('nome'); ?>">

            </div>
            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label for="tipo_anexo_id">Tipo Anexo/Pasta</label>
                <select name="filtro[interno][tipo_anexo_id]" class="form-control"
                        id="tipo_anexo_id">
                    <option value="">Todos</option>
                    <?php
                    foreach ($Tipo_anexos as $r) {
                        if ($r->id == $this->getParam('tipo_anexo_id'))
                            echo '<option selected value="' . $r->id . '">' . $r->nome . '</option>';
                        else
                            echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-12 text-right">
                <button type="submit" class="btn btn-default"><span
                            class="glyphicon glyphicon-search"></span></button>
                <button type="reset" class="btn btn-default botao-reset"><span
                            class="glyphicon glyphicon-refresh"></span></button>

            </div>
            <div class="clearfix"></div>
        </form>
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Anexos do Profissional</h5>
                    <div class="form-group pull-right">
                        <p>
                            <?= $this->Html->getLink('Novo Anexo', 'Anexoprofissional', 'add',
                                array('id' => $this->getParam('id'), 'new' => 1),
                                array('class' => 'btn btn-default')); ?>
                        </p>
                    </div>
                    <div class="form-group pull-right">
                        <button id="visualizador" type="button" style="margin-right: 10px" class="btn btn-primary">
                            Visualizar todos
                        </button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="ibox-content">
                    <div>
                        <!-- tabela de resultados -->
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            Nome
                                        </th>
                                        <th>
                                            Tipo Anexo/Pasta
                                        </th>
                                        <th>
                                            Profissional
                                        </th>
                                        <th>
                                            Dt.Cadastro
                                        </th>
                                        <th>
                                            Cadastrado por
                                        </th>
                                        <th>&nbsp;</th>
                                        <th></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    foreach ($Anexoprofissionais as $a) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->nome, 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->getTipoAnexo()->nome, 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->getRhprofissional()->nome, 'Rhprofissional', 'view',
                                            array('id' => $a->getRhprofissional()->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink(DataBR($a->dtCadastro), 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->getCadastradoPor()->nome, 'Anexoprofissional', 'view',
                                            array('id' => $a->codigo), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo $this->Html->getLink('<span class="img img-edit"></span> ', 'Anexoprofissional', 'edit',
                                            array('id' => $a->codigo),
                                            array('class' => 'btn  btn-sm'));
                                        echo '</td>';

                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';

                                        echo $this->Html->getLink('<span class="img img-remove"></span> ', 'Anexoprofissional', 'delete',
                                            array('id' => $a->codigo, "ajax" => true, "modal" => "1", 'first' => 1),
                                            array('class' => 'btn  btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';

                                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                        echo $this->Html->getLink('<span class="img img-down"></span> ', 'Download', 'baixar',
                                            array('u' => Cript::cript($a->anexo)),
                                            array('class' => 'btn btn-sm'));
                                        echo '</td>';

                                    }
                                    ?>
                                </table>

                                <!-- menu de paginação -->
                                <div style="text-align:center"><?php echo $nav; ?></div>
                            </div>
                        </div>
                    </div><!-- /ibox content -->
                </div><!-- / ibox -->
            </div><!-- wrapper -->
        </div><!-- wrapper -->
    </div><!-- wrapper -->
</div>

<script>
    /* faz a pesquisa com ajax */


    /* faz a pesquisa com ajax */
    $(document).ready(function () {

        $('#visualizador').on('click', function () {
            var id = <?= $this->getParam('id');?>;

            var url = root + '/Anexoprofissional/galeria/modal:1/ajax:true/id:' + id + '/hide:1';

            $('<div></div>').load(url, function () {
                BootstrapDialog.show({
                    title: 'Visualizar Documentos Anexados',
                    message: this,
                    size: 'size-wide',
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function (dialog) {
                        var body = dialog.getModalBody();
                        initAnexos(body);
                        dialog.getModal().removeAttr('tabindex');
                    },
                    buttons: [{
                        label: 'Fechar',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
                });

            });
        });

        $('#search').keyup(function () {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Anexoprofissional', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    else
                        echo '"' . $this->Html->getUrl('Anexoprofissional', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });

    });
</script>