<div class="col-lg-6 col-lg-offset-4">
    <div class="ibox">
        <div class="ibox-content">
            <div class="text-right" style="padding-bottom: 20px">
                <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form class="form" id="form-delete" method="post"
                  action="<?php echo $this->Html->getUrl('Anexoprofissional', 'delete') ?>">
                <h1>Confirmação</h1>
                <div class="well well-lg">
                    <p>Voce tem certeza que deseja excluir o anexo
                        <strong><?php echo $Anexoprofissional->nome; ?></strong>?</p>
                </div>

                <input type="hidden" name="modal" value="1"/>
                <input type="hidden" name="id" value="<?php echo $Anexoprofissional->codigo; ?>"
                <div class="text-right">
                    <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                        Cancelar
                    </a>
                    <input type="submit" onclick="EnviarFormulario('#form-delete',true);"
                           class="btn btn-danger" value="Excluir">
                </div>

            </form>
        </div><!-- /ibox content -->
    </div><!-- / ibox -->
</div> <!-- /col-lg 12 -->

