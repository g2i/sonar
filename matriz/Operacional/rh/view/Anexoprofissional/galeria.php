<style>
    .file-caption-info{
        width:200px !important;
        height: 100% !important;
        overflow-wrap: break-word !important;
    }
</style>
<div class="wrapper wrapper-content">
    <div class="row">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                    <form>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <?
                                $url = array();
                                $fileObj = array();
                                $files = array();
                                foreach($Anexoprofissional as $a) {
                                    $fileObj = array();
                                    if (!empty($a->anexo)) {
                                        $extensao = pathinfo($a->anexo, PATHINFO_EXTENSION);
                                        if ($extensao == 'pdf' || $extensao == 'txt' || $extensao == 'rar'){
                                            $fileObj['type'] = $extensao;
                                        }

                                        $url[] = "http://" . $_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT']. $a->anexo;
                                        $html = new html();
                                        $fileObj['filename'] = pathinfo($a->anexo, PATHINFO_BASENAME);
                                        $fileObj['caption'] =  $a->getTipoAnexo()->nome.' - '.$a->nome;
                                        $fileObj['width'] = "120px";
                                        $fileObj['key'] = $a->id;
                                        $files[] = $fileObj;
                                    }

                                }

                                $files = json_encode($files);
                                $url = implode($url, ",");
                                ?>

                                <input type="file" class="galeria" name="anexo[]" id="anexo"
                                       link-img='<?= $url ?>'
                                       data-object='<?= $files?>'
                                       placeholder="Anexo">
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>



