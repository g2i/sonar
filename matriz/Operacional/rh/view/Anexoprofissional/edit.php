<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Profissional</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Profissionais</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            <form method="post" id="form-edit" role="form" enctype="multipart/form-data"
                  action="<?php echo $this->Html->getUrl('Anexoprofissional', 'edit') ?>">
                <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                </div>
                <input type="hidden" name="id" value="<?php echo $this->getParam('id'); ?>"/>
                <input type="hidden" name="codigo_profissional" value="<?php echo $Anexoprofissional->codigo_profissional ?>"/>

                <div class="well well-lg">
                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" id="nome" class="form-control"
                               value="<?php echo $Anexoprofissional->nome ?>" placeholder="Nome">
                    </div>
                    <input type="hidden" name="path" id="caminho" class="form-control"
                           value="<?php echo $Anexoprofissional->anexo ?>" placeholder="Tipo">
                    <!-- / passa o parametro da id do profissional -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="tipo_anexo_id">Tipo Anexo/Pasta</label>
                        <select name="tipo_anexo_id" class="form-control" id="tipo_anexo_id">
                            <?php
                            foreach ($Tipo_anexos as $r) {
                                if ($r->id == $Anexoprofissional->tipo_anexo_id)
                                    echo '<option selected value="' . $r->id . '">' . $r->nome . '</option>';
                                else
                                    echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                        <label class="required" for="anexo">Anexo <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                        <?
                        $url = '';
                        $fileObj = array();
                        if (!empty($Anexoprofissional->anexo)) {
                            $extensao = pathinfo($Anexoprofissional->anexo, PATHINFO_EXTENSION);
                            if ($extensao == 'pdf' || $extensao == 'txt' || $extensao == 'rar')
                                $fileObj['type'] = $extensao;

                            $url = 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $Anexoprofissional->anexo;
                            $html = new html();
                            $urlDel = $html->getUrl('Anexoprofissional', 'delete_anexo', array('id' => $Anexoprofissional->codigo, 'ajax' => true));
                            $fileObj['url'] = $urlDel;
                            $fileObj['caption'] = $Anexoprofissional->nome;
                            $fileObj['width'] = "120px";
                            $fileObj['key'] = $Anexoprofissional->id;
                        }
                        $fileObj = json_encode($fileObj);

                        ?>

                        <input type="file" class="input-file-edit" name="anexo[]" id="anexo"
                               link-img="<?= $url ?>"
                               data-object='<?= $fileObj ?>'
                               placeholder="Anexo">
                    </div>
                    <div class="clearfix"></div>
                </div>

                <input type="hidden" name="modal" value="1"/>
                <div class="text-right">
                    <a href="<?php echo $this->Html->getUrl('Anexoprofissional', 'all',array('id'=> $Anexoprofissional->codigo_profissional)) ?>"
                       class="btn btn-default" data-dismiss="modal">Cancelar</a>

                    <input id="salvarAnexo" type="submit" onclick="EnviarFormularioAnexo('#form-edit')"
                           class="ladda-button ladda-button-demo btn btn-primary"
                           value="salvar">
                </div>

            </form>
        </div><!-- /ibox content -->
    </div><!-- / ibox -->
</div> <!-- /col-lg 12 -->
