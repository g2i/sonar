<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Profissional</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Profissionais</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="ibox">

        <div class="ibox-content">
            <form method="post" id="form-modal" role="form" enctype="multipart/form-data"
                  action="<?php echo $this->Html->getUrl('Anexoprofissional', 'add') ?>">
                <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                </div>


                <div class="well well-lg">
                    <?php if ($this->getParam('new')) { ?>
                        <input type="hidden" name="codigo_profissional"
                               value="<?php echo $this->getParam('id'); ?>"/>
                    <?php } else { ?>
                        <!-- / passa o parametro da id do profissional -->
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <label for="codigo_profissional">Profissional</label>
                            <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                                <?php
                                foreach ($Rhprofissionais as $r) {
                                    if ($r->codigo == $Anexoprofissional->codigo_profissional)
                                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                                    else
                                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" id="nome" class="form-control"
                               value="<?php echo $Anexoprofissional->nome ?>" placeholder="Nome">
                    </div>
                    <!-- / passa o parametro da id do profissional -->
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label for="tipo_anexo_id">Tipo Anexo/Pasta</label>
                        <select name="tipo_anexo_id" class="form-control" id="tipo_anexo_id">
                            <?php
                            foreach ($Tipo_anexos as $r) {
                                if ($r->id == $Anexoprofissional->tipo_anexo_id)
                                    echo '<option selected value="' . $r->id . '">' . $r->nome . '</option>';
                                else
                                    echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>


                    <div class="form-group col-md-6 col-sm-6 col-xs-6">
                        <label class="required" for="anexo">Anexo <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                        <input type="file" name="anexo" class="fileinput" id="anexo"
                               value="<?php echo $Anexoprofissional->anexo ?>"
                               placeholder="Anexo">
                    </div>
                    <div class="clearfix"></div>
                </div>

                <input type="hidden" name="modal" value="1"/>
                <div class="text-right">
                    <? if(empty($this->getParam('id'))){?>
                    <a href="<?php echo $this->Html->getUrl('Anexoprofissional', 'index') ?>"
                       class="btn btn-default" data-dismiss="modal">Cancelar</a>
                    <?}else{?>
                    <a href="<?php echo $this->Html->getUrl('Anexoprofissional', 'all',array('id'=> $this->getParam('id'))) ?>"
                       class="btn btn-default" data-dismiss="modal">Cancelar</a>
                    <?}?>

                    <button id="salvarAnexo" type="submit" onclick="EnviarFormularioAnexo('#form-modal');"
                            class="ladda-button ladda-button-demo btn btn-primary"
                            value="salvar">Salvar
                    </button>
                </div>

            </form>
        </div><!-- /ibox content -->
    </div><!-- / ibox -->
</div><!-- / ibox -->


