
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Rhprofissional_salario_historico</h2>
    <ol class="breadcrumb">
    <li>Rhprofissional_salario_historico</li>
    <li class="active">
    <strong>Adicionar Rhprofissional_salario_historico</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhprofissional_salario_historico', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="valorremuneracao">valorremuneracao</label>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="rhprofissional_contratacao_id">rhprofissional_contratacao_id</label>
            <select name="rhprofissional_contratacao_id" class="form-control" id="rhprofissional_contratacao_id">
                <?php
                foreach ($Rhprofissional_contratacaos as $r) {
                    if ($r->codigo == $Rhprofissional_salario_historico->rhprofissional_contratacao_id)
                        echo '<option selected value="' . $r->codigo . '">' . $r->tipo . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->tipo . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="situacao_id">situacao_id</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Rhprofissional_salario_historico->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhprofissional_salario_historico', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>