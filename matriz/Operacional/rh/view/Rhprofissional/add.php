<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-sm-4">
                        <h2>Profissional</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li class="active">
                                <strong>Cadastrar Profissional</strong>
                            </li>
                        </ol>
                    </div>
                </div>
                <div class="ibox-content">
<form method="post" role="form" enctype="multipart/form-data" action="<?php echo $this->Html->getUrl('Rhprofissional', 'add') ?>">
    <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <!-- o tab para organizar melhor o código -->
    <div class="panel">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#dadospessoais" data-toggle="tab" aria-expanded="true"><i class="glyphicon glyphicon-user"></i> Dados Pessoais </a></li>
            <li class=""><a href="#documentos" data-toggle="tab" aria-expanded="false"><i class="glyphicon glyphicon-folder-open"> </i>  Documentos Pessoais</a></li>
            <li class=""><a href="#endereco" data-toggle="tab" aria-expanded="false"><i class="glyphicon glyphicon-home"> </i> Endereço </a></li>
            <li class=""><a href="#contato" data-toggle="tab" aria-expanded="false"><i class="glyphicon glyphicon-phone-alt"> </i>  Contato </a></li>
            <li class=""><a href="#dadosProfissional" data-toggle="tab" aria-expanded="false"><i class="glyphicon glyphicon-list-alt"> </i> Dados Profissional </a></li>
            <li><a href="<?= SITE_PATH . "/uploads/" ?>DocumentosGeral.pdf" target="_blank"><i  class="fa fa-file-pdf-o"></i> Comunicados</a></li>

        </ul>
        <div class="tab-content">
            <div class="tab-pane well active" id="dadospessoais">
                <div class="form-group col-md-3 pull-right">
                    <a onclick="upload()" id="uploadFoto" style="cursor:pointer">
                        <?php if (empty($Rhprofissional->foto)) { ?>
                            <img src="<?php echo SITE_PATH ?>/lib/css/images/photo23x4.png" title="Upload Foto" id="charFoto" style="width:128px;height:128px;cursor:pointer;">
                        <?php } else { ?>
                            <img src="<?php echo SITE_PATH . "/uploads".$Rhprofissional->foto;?>" title='Foto Carregada' id="charFoto" style="width:128px;height:128px;cursor:pointer;">
                        <?php } ?>
                    </a>
                    <input type="file" name="foto" id="foto" style="visibility: hidden;" >
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label class="required" for="nome">Nome <span class="glyphicon glyphicon-asterisk"></span></label>
                    <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Rhprofissional->nome ?>" placeholder="Nome" required>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="matricula">Matricula</label>
                    <input type="number" name="matricula" id="matricula" class="form-control" value="<?php echo $Rhprofissional->matricula ?>" placeholder="Matricula">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="dtnascimento">Data de Nascimento</label>
                    <input type="date" name="dtnascimento" id="dtnascimento" class="form-control" value="<?php echo $Rhprofissional->dtnascimento ?>" placeholder="Data de Nascimento">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="sexo">Sexo</label>
                    <select name="sexo" class="form-control" id="sexo">
                        <option value="">Selecione</option>
                        <option value="F">Feminino</option>
                        <option value="M">Masculino</option>
                    </select>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="estadocivil" class="required">Estado Civil<span class="glyphicon glyphicon-asterisk"></span></label>
                    <select name="estadocivil" class="form-control" id="estadocivil" required>
                        <option value="">Selecione:</option>
                        <option value="Solteiro">Solteiro(a)</option>
                        <option value="Casado">Casado(a)</option>
                        <option value="Divorciado ">Divorciado(a)</option>
                        <option value="Viúvo">Viúvo(a)</option>
                        <option value="Outros">Outros</option>
                    </select>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="grauinstrucao">Escolaridade</label>
                    <!-- tentativa de um select -->
                    <select name="grauinstrucao" class="form-control" id="grauinstrucao">
                        <option value="">Selecione:</option>
                        <option value="Fundamental Incompleto">Fundamental Incompleto</option>
                        <option value="Fundamento Completo">Fundamento Completo</option>
                        <option value="Medio incompleto">Médio incompleto</option>
                        <option value="Medio completo">Médio completo</option>
                        <option value="Superior incompleto">Superior incompleto</option>
                        <option value="Superior completo">Superior completo</option>
                        <option value="Pos-graduação incompleto">Pós-graduação incompleto</option>
                        <option value="Pos-graduação completo">Pós-graduação completo</option>
                    </select>
                    <!-- termina aqui -->
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="mae">Nome da Mãe</label>
                    <input type="text" name="mae" id="mae" class="form-control" value="<?php echo $Rhprofissional->mae ?>" placeholder="Nome da Mãe">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="nacionalidade">Nacionalidade</label>
                    <input type="text" name="nacionalidade" id="nacionalidade" class="form-control" value="<?php echo $Rhprofissional->nacionalidade ?>" placeholder="Nacionalidade">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="naturalidade">Naturalidade</label>
                    <input type="text" name="naturalidade" id="naturalidade" class="form-control" value="<?php echo $Rhprofissional->naturalidade ?>" placeholder="Naturalidade">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="pai">Nome do Pai</label>
                    <input type="text" name="pai" id="pai" class="form-control" value="<?php echo $Rhprofissional->pai ?>" placeholder="Nome do Pai">
                </div>    
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="status">Status</label>
                    <select name="status" class="form-control" id="status">
                        <option value="">Selecione:</option>
                        <?php
                        foreach ($Rhstatus as $r) {
                            if ($r->codigo == $Rhprofissional->status)
                                echo '<option selected value="' . $r->codigo . '">' . $r->descricao . '</option>';
                            else
                                echo '<option value="' . $r->codigo . '">' . $r->descricao . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="especial">Port. Necessidades Especiais</label>
                    <select name="especial" class="form-control" id="especial">
                        <option value="">Selecione:</option>
                        <option value="1">Sim</option>
                        <option value="2">Não</option>
                    </select>
                </div>

                <div class="col-md-8"></div>

                <div class="form-group col-md-4 col-sm-6 col-xs-12" id="detalhes_especial" style="display: none;">
                    <label for="detalhes_especial">Quais: </label>
                    <textarea type="text" name="detalhes_especial" id="detalhes_especial" class="form-control"></textarea>
                </div>
                
                <div class="clearfix"></div>
                </div>
            <div class="tab-pane well" id="endereco">
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <div class="clearfix"></div>
                    <label for="cep" class="required">CEP<span class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" role="cep" cep-init="LoadGif" cep-done="CloseGif" name="cep" id="cep" class="form-control cep" data-toggle="tooltip" data-placement="bottom" title="Informe o CEP - Preenchimento automático" value="<?php echo $Rhprofissional->cep ?>" placeholder="CEP" required>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="endereco">Endereço</label>
                    <input type="text" name="endereco" data-cep="endereco"id="endereco" class="form-control endereco" value="<?php echo $Rhprofissional->endereco ?>" placeholder="Endereço">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="numero">Numero</label>
                    <input type="number" name="numero" id="numero" class="form-control" value="<?php echo $Rhprofissional->numero ?>" placeholder="Numero">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="complemento">Complemento</label>
                    <input type="text" name="complemento" id="complemento" class="form-control" value="<?php echo $Rhprofissional->complemento ?>" placeholder="Complemento">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="bairro">Bairro</label>
                    <input type="text" data-cep="bairro" name="bairro" id="bairro" class="form-control bairro" value="<?php echo $Rhprofissional->bairro ?>" placeholder="Bairro">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="cidade">Cidade</label>
                    <input type="text" data-cep="cidade" name="cidade" id="cidade" class="form-control cidade" value="<?php echo $Rhprofissional->cidade ?>" placeholder="Cidade">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="estado">Estado</label>
                    <input type="text" data-cep="uf" name="estado" id="estado" class="form-control estado" value="<?php echo $Rhprofissional->estado ?>" placeholder="Estado">
                </div>               
                <div class="clearfix"></div>
            </div>
            <div class="tab-pane well" id="contato">
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="celular01" class="required">Celular<span class="glyphicon glyphicon-asterisk"></span></label>
                    <input type="text" name="celular01" id="celular01" class="form-control phone" value="<?php echo $Rhprofissional->celular01 ?>" placeholder="Celular" required>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="celular02">Celular Corporativo</label>
                    <input type="text" name="celular02" id="celular02" class="form-control phone" value="<?php echo $Rhprofissional->celular02 ?>" placeholder="Celular">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="residencial">Telefone Residencial</label>
                    <input type="text" name="residencial" id="residencial" class="form-control phone" value="<?php echo $Rhprofissional->residencial ?>" placeholder="Telefone Residencial">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="corporativo">Telefone Corporativo</label>
                    <input type="text" name="corporativo" id="corporativo" class="form-control phone" value="<?php echo $Rhprofissional->corporativo ?>" placeholder="Telefone Corporativo">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="recado">Telefone para Recado</label>
                    <input type="text" name="recado" id="recado" class="form-control phone" value="<?php echo $Rhprofissional->recado ?>" placeholder="Telefone para Recado">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="email">E-mail</label>
                    <input type="email" name="email" id="email" class="form-control email" value="<?php echo $Rhprofissional->email ?>" placeholder="E-mail">
                </div>
                <div class="clearfix"></div>
               
            </div>
            <div class="tab-pane well" id="documentos">
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="rg" class="required">RG<span class="glyphicon glyphicon-asterisk"></span></label>
                    <input type="text" name="rg" id="rg" class="form-control" value="<?php echo $Rhprofissional->rg ?>" placeholder="RG" required>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="dtemissao">Data Emissão</label>
                    <input type="date" name="dtemissao" id="dtemissao" class="form-control" value="<?php echo $Rhprofissional->dtemissao ?>" placeholder="Data Emissão">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="orgaoexped">Orgão Expeditor</label>
                    <input type="text" name="orgaoexped" id="orgaoexped" class="form-control" value="<?php echo $Rhprofissional->orgaoexped ?>" placeholder="Orgão Expeditor">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="cpf" class="required">CPF<span class="glyphicon glyphicon-asterisk"></span></label>
                    <input type="text" name="cpf" id="cpf" class="form-control cpf" value="<?php echo $Rhprofissional->cpf ?>" placeholder="CPF" required>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="carteiriahabilitacao">Nº de Registro da CNH</label>
                    <input type="text" name="carteiriahabilitacao" id="carteiriahabilitacao" class="form-control" value="<?php echo $Rhprofissional->carteiriahabilitacao ?>" placeholder="Nº CNH">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="habilitacao_categoria">Categoria CNH</label>
                    <input type="text" name="habilitacao_categoria" id="habilitacao_categoria" class="form-control" value="<?php echo $Rhprofissional->habilitacao_categoria ?>" placeholder="Categoria">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="habilitacao_emissao">Data Emissão CNH</label>
                    <input type="date" name="habilitacao_emissao" id="habilitacao_emissao" class="form-control" value="<?php echo $Rhprofissional->habilitacao_emissao ?>" placeholder="Vencimento">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="habilitacao_orgao_exped">Orgão Expeditor CNH</label>
                    <input type="text" name="habilitacao_orgao_exped" id="habilitacao_orgao_exped" class="form-control" value="<?php echo $Rhprofissional->habilitacao_orgao_exped ?>" placeholder="Orgão Expeditor CNH">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="habilitacao_uf">UF CNH</label>
                    <input type="text" name="habilitacao_uf" id="habilitacao_uf" class="form-control" value="<?php echo $Rhprofissional->habilitacao_uf ?>" placeholder="UF CNH">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="habilitacao_vencimento">Vencimento CNH</label>
                    <input type="date" name="habilitacao_vencimento" id="habilitacao_vencimento" class="form-control" value="<?php echo $Rhprofissional->habilitacao_vencimento ?>" placeholder="Vencimento">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="tituloeleitor">Nº Título do Eleitor</label>
                    <input type="text" name="tituloeleitor" id="tituloeleitor" class="form-control" value="<?php echo $Rhprofissional->tituloeleitor ?>" placeholder="Nº Titulo de Eleitor">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="tituloeleitor_emissao">Data Emissão Título do Eleitor</label>
                    <input type="date" name="tituloeleitor_emissao" id="tituloeleitor_emissao" class="form-control" value="<?php echo $Rhprofissional->tituloeleitor_emissao ?>" placeholder="Emissão">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="zona">Zona</label>
                    <input type="number" name="zona" id="zona" class="form-control" value="<?php echo $Rhprofissional->zona ?>" placeholder="Zona"">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="secao">Seção</label>
                    <input type="number" name="secao" id="secao" class="form-control" value="<?php echo $Rhprofissional->secao ?>" placeholder="Seção"">
                </div>

                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="reservista">Nº Reservista</label>
                    <input type="text" name="reservista" id="reservista" class="form-control" value="<?php echo $Rhprofissional->reservista ?>" placeholder="Nº da Reservista">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="reservista_categoria">Categoria Reservista</label>
                    <input type="text" name="reservista_categoria" id="reservista_categoria" class="form-control" value="<?php echo $Rhprofissional->reservista_categoria ?>" placeholder="Categoria Reservista">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="reservista_emissao">Data Emissão Reservista</label>
                    <input type="date" name="reservista_emissao" id="reservista_emissao" class="form-control" value="<?php echo $Rhprofissional->reservista_emissao ?>" placeholder="">
                </div>
                <div class="clearfix"></div>
            </div>
            
            <div class="tab-pane well" id="dadosProfissional">
               <!-- ****************************
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="carteiratrabalho">Nº da Carteira de Trabalho</label>
                    <input type="text" name="carteiratrabalho" id="carteiratrabalho" class="form-control" value="<?php echo $Rhprofissional->carteiratrabalho ?>" placeholder="Nº da Carteira de Trabalho">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="dtcarteiratrabalho">Data de Emissão CTPS</label>
                    <input type="date" name="dtcarteiratrabalho" id="dtcarteiratrabalho" class="form-control" value="<?php echo $Rhprofissional->dtcarteiratrabalho ?>" placeholder="Data de Emissão Carteira de Trabalho">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="expeditorctps">Orgão Expeditor CTPS</label>
                    <input type="text" name="expeditorctps" id="expeditorctps" class="form-control" value="<?php echo $Rhprofissional->expeditorctps ?>" placeholder="Orgão Expeditor CTPS">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="pis" class="required">PIS<span class="glyphicon glyphicon-asterisk"></span></label>
                    <input type="text" name="pis" id="pis" class="form-control"  data-toggle="tooltip" data-placement="auto" title="Digite 01 para primeiro emprego" value="<?php echo $Rhprofissional->pis ?>" placeholder="PIS">
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="setor">Setor</label>
                    <select name="setor" class="form-control" id="setor">
                        <option value="">Selecione:</option>
                        <?php
                        foreach ($Rhsetores as $r) {
                            if ($r->codigo == $Rhprofissional->setor)
                                echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                            else
                                echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="rhprojetos_id" style="color:red;">Projetos</label>
                    <select name="rhprojetos_id" class="form-control" id="rhprojetos_id">
                        <option value="">Selecione:</option>
                        <?php
                        foreach ($Rhprojetos as $r) {
                            if ($r->id == $Rhprofissional->rhprojetos_id)
                                echo '<option selected value="' . $r->id . '">' . $r->nome . '</option>';
                            else
                                echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                        }
                        ?>
                    </select>
                </div>
                -->
                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                    <label for="localidade" style="color:red;" >Localidade</label>
                        <select name="rhlocalidade_id" class="form-control" id="rhlocalidade_id" required>
                            <?php
                                foreach ($Rhlocalidade as $l) {
                                    if ($l->id == $Rhprofissional->rhlocalidade_id)
                                        echo '<option selected value="'. $l->id . '">' . $l->cidade . ' - ' . $l->estado .'</option>';
                                    else 
                                        echo '<option value="' . $l->id . '">' . $l->cidade . ' - ' . $l->estado .'</option>';
                                    }
                            ?>
                        </select>
                </div>
                
                <div class="clearfix"></div>
                <div class="text-right">
                    <a href="<?php echo $this->Html->getUrl('Rhprofissional', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                    <input type="submit" class="btn btn-primary" value="Continuar">
                </div>   
            </div>
                             
        </div>
    </div>
</form>
</div>
    </div>
            </div>
        </div>
    </div>
<script>


    //toltip e validação de cpf
    $(document).ready(function(){

        $('#especial').change(function() {
            if($(this).val() == 1) {
                $('#detalhes_especial').show();
            } else {
                $('#detalhes_especial').hide();
            }

        });

        $('[data-toggle="tooltip"]').tooltip();
        $('#cpf').blur(function(){
            if(!validarCPF($(this).val())) {
                OpenDialog("Alerta!", "CPF inválido !");
            $(this).val('')
            }
        })
        $('#email').blur(function() {
            if (!validacaoEmail($(this).val())) {
                OpenDialog("Alerta!", "E-mail inválido !");
                $this.val('')
            }
        })
    }   );
    //fim toltip

    function upload() {
        $('#foto').click();
    }

    $('document').ready(function() {
        $('#foto').change(function() {
            $('#uploadFoto').html('<output id="list" style="width:128px;height:128px;"></output>');
        });
    });

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }
            var reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function(theFile) {
                return function(e) {
                    // Render thumbnail.
                    var span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '" width="128" height="128"/>'].join('');
                    document.getElementById('list').insertBefore(span, null);
                };
            })(f);
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    document.getElementById('foto').addEventListener('change', handleFileSelect, false);
</script>