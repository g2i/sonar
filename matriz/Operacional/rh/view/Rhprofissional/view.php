<div class="row">
    <div class="col-lg-12">
        <div class=" animated fadeInRight ">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="right">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div id="printablediv">
                        <legend>Dados Pessoais</legend>
                        <div class="form-groupo pull-right col-md-6 col-sm-6 col-xs-12">
                            <?php if (empty($Rhprofissional->foto)) { ?>
                                <img src="<?php echo SITE_PATH ?>/lib/css/images/photo23x4.png" title="Upload Foto"
                                     id="charFoto" style="width:128px;height:128px;cursor:pointer;">
                            <?php } else { ?>
                                <img src="<?php echo $Rhprofissional->foto; ?>" title='Foto Carregada' id="charFoto"
                                     style="width:128px;height:128px;cursor:pointer;">
                            <?php } ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nome</strong>:
                            <?php echo $Rhprofissional->nome; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Matricula</strong>:
                            <?php echo $Rhprofissional->matricula; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Data de Nascimento</strong>:
                            <?php echo DataBR($Rhprofissional->dtnascimento); ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Sexo</strong>:
                            <?php echo $Rhprofissional->sexo; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Estado civil</strong>:
                            <?php echo $Rhprofissional->estadocivil; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Escolaridade</strong>:
                            <?php echo $Rhprofissional->grauinstrucao; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nacionalidade</strong>:
                            <?php echo $Rhprofissional->nacionalidade; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Naturalidade</strong>:
                            <?php echo $Rhprofissional->naturalidade; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nome da Mãe</strong>: <?php echo $Rhprofissional->mae; ?></p>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nome do Pai</strong>: <?php echo $Rhprofissional->pai; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Setor</strong>:
                            <?php
                            echo $Rhprofissional->getRhsetor()->nome;
                            ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Status</strong>:
                            <?php
                            echo $Rhprofissional->getStatus()->descricao;
                            ?>
                        </div>
                        <legend>Endereço</legend>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>CEP</strong>: <?php echo $Rhprofissional->cep; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Endereço</strong>: <?php echo $Rhprofissional->endereco; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Numero</strong>: <?php echo $Rhprofissional->numero; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Complemento</strong>: <?php echo $Rhprofissional->complemento; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Bairro</strong>: <?php echo $Rhprofissional->bairro; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Cidade</strong>: <?php echo $Rhprofissional->cidade; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Estado</strong>: <?php echo $Rhprofissional->estado; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Localidade</strong>: <?php echo $Rhprofissional->getLocalidade()->cidade; 
                            echo " - " ;
                            echo $Rhprofissional->getLocalidade()->estado;
                            
                            ?>
                        </div>

                        <legend>Contato</legend>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Celular</strong>: <?php echo $Rhprofissional->celular01; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Celular Corporativo</strong>: <?php echo $Rhprofissional->celular02; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Telefone Residencial</strong>: <?php echo $Rhprofissional->residencial; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Telefone para Recado</strong>: <?php echo $Rhprofissional->recado; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Telefone Corporativo</strong>: <?php echo $Rhprofissional->corporativo; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>E-mail</strong>: <?php echo $Rhprofissional->email; ?>
                        </div>

                        <legend>Documentos</legend>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>RG</strong>: <?php echo $Rhprofissional->rg; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Data de Emissão</strong>: <?php echo DataBR($Rhprofissional->dtemissao); ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Orgão Expeditor</strong>
                            <?php echo $Rhprofissional->orgaoexped ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>CPF</strong>: <?php echo $Rhprofissional->cpf; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nº de Registro da CNH</strong>: <?php echo $Rhprofissional->carteiriahabilitacao; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Categoria da CNH</strong>: <?php echo $Rhprofissional->habilitacao_categoria; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Vencimento CNH</strong>: <?php echo DataBR($Rhprofissional->habilitacao_vencimento); ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>PIS</strong>: <?php echo $Rhprofissional->pis; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nº da Carteira de
                                Trabalho</strong>: <?php echo $Rhprofissional->carteiratrabalho; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Data de Emisão
                                CTPS</strong>: <?php echo DataBR($Rhprofissional->dtcarteiratrabalho); ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Orgão Expeditor CTPS</strong>: <?php echo $Rhprofissional->expeditorctps; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nº da Reservista</strong>: <?php echo $Rhprofissional->reservista; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Categoria da
                                Reservista</strong>: <?php echo $Rhprofissional->reservista_categoria; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Nº Titulo do Eleitor</strong>: <?php echo $Rhprofissional->tituloeleitor; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Zona</strong>: <?php echo $Rhprofissional->zona; ?>
                        </div>
                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                            <strong>Seção</strong>: <?php echo $Rhprofissional->secao; ?>
                        </div>
                    </div>

                    <div class="text-right">
                    <?php  echo $this->Html->getLink('<span class="fa fa-print" target="_blank"> Imprimir</span> ', 'Rhrelatorio', 'reports_rhprossional', 
                        array('id' => $Rhprofissional->codigo),
                        array('target' => '_blank'));?>
					<!-- <a href="<?php echo SITE_PATH; ?>/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=ficha_cadastral&codigo=<?php echo $Rhprofissional->codigo; ?>" target="_blank">
					<span class="img img-print" data-placement="top" data-toggle="tooltip" title="Imprimir"></span>-->
					</a>
                    </div>
                </div>
                <!-- /ibox content -->
            </div>
            <!-- / ibox -->

        </div>
        <!--wrapper-->
    </div>
    <!-- /col-lg 12 -->
</div><!-- /row -->

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

