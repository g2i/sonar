<script type='text/javascript'>
    // Set full screen mode for the designer
    var options = new Stimulsoft.Designer.StiDesignerOptions();
    options.appearance.fullScreenMode = true;

    // Create the report designer with specified options
    var designer = new Stimulsoft.Designer.StiDesigner(options, 'StiDesigner', false);
    // Create a new report instance
    var report = new Stimulsoft.Report.StiReport();
    // Load report from url
    report.loadFile("<?php echo SITE_PATH.'/lib/reports_2016/template.mrt'; ?>");
    // Edit report template in the designer
    designer.report = report;
    designer.renderHtml();
</script>