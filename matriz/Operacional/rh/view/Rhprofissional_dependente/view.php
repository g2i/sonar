
<p><strong>Tipo</strong>: <?php echo $Rhprofissional_dependente->tipo;?></p>
<p><strong>Nome</strong>: <?php echo $Rhprofissional_dependente->nome;?></p>
<p><strong>Data Nascimento</strong>: <?php echo $Rhprofissional_dependente->dtnascimento;?></p>
<p><strong>Registro</strong>: <?php echo $Rhprofissional_dependente->registro;?></p>
<p><strong>Possui Cartão de Vacinação?</strong>: <?php echo $Rhprofissional_dependente->cartaovacinacao;?></p>
<p><strong>Estuda?</strong>: <?php echo $Rhprofissional_dependente->declaracaoescolhar;?></p>
<p>
    <strong>Status</strong>:
    <?php
    echo $this->Html->getLink($Rhprofissional_dependente->getRhstatus()->descricao, 'Rhstatus', 'view',
    array('id' => $Rhprofissional_dependente->getRhstatus()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Profissional</strong>:
    <?php
    echo $this->Html->getLink($Rhprofissional_dependente->getRhprofissional()->nome, 'Rhprofissional', 'view',
    array('id' => $Rhprofissional_dependente->getRhprofissional()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>