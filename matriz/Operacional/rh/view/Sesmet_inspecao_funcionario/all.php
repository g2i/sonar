
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Inspeção funcionário</h2>
    <ol class="breadcrumb">
    <li>Inspeção funcionário</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                <div class="col-md-3 form-group">
                    <label for="sesmet_inspecao_id">sesmet_inspecao_id</label>
                    <select name="filtro[externo][sesmet_inspecao_id]" class="form-control" id="sesmet_inspecao_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Sesmet_inspecaos as $s): ?>
                            <?php echo '<option value="' . $s->id . '">' . $s->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="rhfuncionario_id">rhfuncionario_id</label>
                    <select name="filtro[externo][rhfuncionario_id]" class="form-control" id="rhfuncionario_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Rhprofissionais as $r): ?>
                            <?php echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>-->

    <!-- botao de cadastro -->
    <div class="text-right">
    <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Sesmet_inspecao_funcionario","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                <span class="img img-add"></span> Novo registro </a></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao_funcionario', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao_funcionario', 'all', array('orderBy' => 'sesmet_inspecao_id')); ?>'>
                        Inspeção
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_inspecao_funcionario', 'all', array('orderBy' => 'rhfuncionario_id')); ?>'>
                        Funcionario
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Sesmet_inspecao_funcionarios as $s) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($s->id, 'Sesmet_inspecao_funcionario', 'view',
                    array('id' => $s->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($s->getSesmet_inspecao()->nome, 'Sesmet_inspecao', 'view',
                    array('id' => $s->getSesmet_inspecao()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($s->getRhprofissional()->nome, 'Rhprofissional', 'view',
                    array('id' => $s->getRhprofissional()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar" class="btn btn-sm" style="max-width:50px; max-height:50px;"
                        onclick="Navegar(\'' . $this->Html->getUrl("Sesmet_inspecao_funcionario", "delete", array("ajax" => true, "modal" => "1", 'id' => $s->id)) . '\',\'go\')">
                        <span class="img img-remove"></span></a>';
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Sesmet_inspecao_funcionario', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Sesmet_inspecao_funcionario', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>