
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Sesmet_inspecao_funcionario</h2>
    <ol class="breadcrumb">
    <li>Sesmet_inspecao_funcionario</li>
    <li class="active">
    <strong>Editar Sesmet_inspecao_funcionario</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Sesmet_inspecao_funcionario', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="sesmet_inspecao_id">Inspeção</label>
            <select name="sesmet_inspecao_id" class="form-control" id="sesmet_inspecao_id">
                <?php
                foreach ($Sesmet_inspecaos as $s) {
                    if ($s->id == $Sesmet_inspecao_funcionario->sesmet_inspecao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="rhfuncionario_id">Funcionário</label>
            <select name="rhfuncionario_id" class="form-control" id="rhfuncionario_id">
                <?php
                foreach ($Rhprofissionais as $r) {
                    if ($r->codigo == $Sesmet_inspecao_funcionario->rhfuncionario_id)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Sesmet_inspecao_funcionario->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Sesmet_inspecao_funcionario', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>