<script type='text/javascript'>
/* nunca modificar este arquivo sempre copiar igual a este pois aqui é php raiz*/
    // Create the report viewer with default options
    var viewer = new Stimulsoft.Viewer.StiViewer(null, "StiViewer", false);
    // Create a new report instance
    var report = new Stimulsoft.Report.StiReport();

    // Load report from url
    report.loadFile("<?php echo SITE_PATH.'/Relatorios/reports/ficha_cadastral_rhprofissional.mrt'; ?>");
        // Create new DataSet object
    var dataSet = new Stimulsoft.System.Data.DataSet("reportRhProfissional");
    // Load JSON data file from specified URL to the DataSet object
    dataSet.readJsonFile("<?php echo SITE_PATH.'/Relatorios/reports/json/reportRhProfissional.json'; ?>");
    // Remove all connections from the report template
    report.dictionary.databases.clear();
    // Register DataSet object
    report.regData("reportRhProfissional", "reportRhProfissional", dataSet);

    // Assign report to the viewer, the report will be built automatically after rendering the viewer
    viewer.report = report;
    viewer.renderHtml();
</script>
