
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Anexo</h2>
    <ol class="breadcrumb">
    <li>Anexo</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <div class="text-right">
        <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Anexo","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
             <span class="img img-add"></span> Novo Anexo</a></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexo', 'all', array('orderBy' => 'id')); ?>'>
                        Id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexo', 'all', array('orderBy' => 'id')); ?>'>
                        Data de cadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexo', 'all', array('orderBy' => 'tipo')); ?>'>
                        Tipo Anexo
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexo', 'all', array('orderBy' => 'titulo')); ?>'>
                        Titulo
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Anexos as $a) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($a->id, 'Anexo', 'view',
                    array('id' => $a->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>'; 
                    echo $this->Html->getLink(DataBR($a->dtCadastro), 'Anexo', 'view',
                        array('id' => $a->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    echo '</td>';

                echo '<td>';
                echo $this->Html->getLink($a->getTipoAnexo()->nome, 'Anexo', 'view',
                    array('id' => $a->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';             
                echo '<td>';
                echo $this->Html->getLink($a->titulo, 'Anexo', 'view',
                    array('id' => $a->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                            echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar Anexo" class="btn btn-sm" style="max-width:50px; max-height:50px;"
                            onclick="Navegar(\'' . $this->Html->getUrl("Anexo", "delete", array("ajax" => true, "modal" => "1", 'id' => $a->id)).'\',\'go\')">
                            <span class="img img-remove"></span></a>';
                        echo '</td>';
                        echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                        echo $this->Html->getLink('<span class="img img-down"></span> ', 'Download', 'baixar',
                            array('u' => Cript::cript($a->url)),
                            array('class' => 'btn btn-sm'));
                        echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Anexo', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Anexo', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>