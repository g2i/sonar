<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhprofissional_contratacao', 'add') ?>">
    <div class="right">
        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
    </div>
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <legend>Dados da Contratação</legend>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="tipo" class="required">Tipo<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select class="form-control" name="tipo" id="tipo" required>
                <option value="">Selecione:</option>
                <option value="Admissão">Admissão</option>
                <option value="Demissão">Demissão</option>
                <option value="Movimentação">Movimentação</option>
                <option value="Cancelamento">Cancelamento</option>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="funcao" class="required">Função<span class="glyphicon glyphicon-asterisk"></span></label>
            <select name="funcao" class="form-control" id="funcao" required>
                <option value="">Selecione:</option>
                <?php
                foreach ($Rhprofissional_funcao as $r) {
                    if ($r->codigo == $Rhprofissional_contratacao->funcao)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="adicionais">Adicional </label>
            <select class="form-control" name="adicionais" id="adicionais" >
                <option value="">Selecione:</option>
                <option value="1">Sim</option>
                <option value="2">Não</option>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="tiporemuneracao" class="required">Tipo de Remuneração<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select class="form-control" name="tiporemuneracao" id="tiporemuneracao" required>
                <option value="">Selecione:</option>
                <option value="Diaria">Diária</option>
                <option value="Semanal">Semanal</option>
                <option value="Quinzenal">Quinzenal</option>
                <option value="Mensal">Mensal</option>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="jornada" class="required">Jornada de Trabalho<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="jornada" id="jornada" class="form-control" value="<?php echo $Rhprofissional_contratacao->jornada ?>" placeholder="Jornada de Trabalho" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="valorremuneracao" class="required">Salário Atual<span class="glyphicon glyphicon-asterisk"></span> </label>
            <input type="text" name="valorremuneracao" id="valorremuneracao"  class="form-control" value="<?php echo $Rhprofissional_contratacao->valorremuneracao ?>" placeholder="Valor da Remuneração" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dtInicial" class="required">Data Inicial<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="date" class="form-control" name="dtInicial" id="dtInicial" value="<?php echo $Rhprofissional_contratacao->dtInicial ?>" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dtFinal">Data Final</label>
           <input type="date" name="dtFinal" id="dtFinal" class="form-control" value="<?php echo $Rhprofissional_contratacao->dtFinal ?>" placeholder="DtFinal">
            </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="valetransporte" class="required">Vale Transporte ?<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select class="form-control" name="valetransporte" id="valetransporte" required>
                <option value="">Selecione:</option>
                <option value="SIM">Sim</option>
                <option value="NAO">Não</option>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="valetransporteqtdia" class="required">VT por Dia<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="number" name="valetransporteqtdia" id="valetransporteqtdia" class="form-control" value="<?php echo $Rhprofissional_contratacao->valetransporteqtdia ?>" placeholder="Quantidade de VT ao dia" required>
        </div>
        <div class="form-group col-xs-12">
            <label for="horario">Horários</label>
            <input type="text" name="horario" id="horario" class="form-control" value="<?php echo $Rhprofissional_contratacao->horario ?>" placeholder="Horários">
        </div>
        <div class="form-group col-xs-12">
            <label for="local_trabalho">Local de Trabalho</label>
            <input type="text" name="local_trabalho" id="local_trabalho" class="form-control" value="<?php echo $Rhprofissional_contratacao->local_trabalho ?>" placeholder="Local de trabalho">
        </div>
        <div class="form-group col-xs-12">
            <label for="observacao">Observação</label>
            <input type="text" name="observacao" id="observacao" class="form-control" value="<?php echo $Rhprofissional_contratacao->observacao ?>" placeholder="Observação">
        </div>
        <div class="form-group col-xs-12"">
            <label for="motivo">Motivo da Demissão</label>
            <input type="text" name="motivo" id="motivo" class="form-control motivo" value="<?php echo $Rhprofissional_contratacao->motivo ?>" placeholder="Motivo da Demissão">
        </div>
        <div class="clearfix"></div>
        <legend>Conta Salário</legend>
                <div class="form-group  col-xs-3">
                    <label for="banco">Banco</label>
                    <input type="text" name="banco" id="banco" class="form-control" value="<?php echo $Rhprofissional_contratacao->banco ?>" placeholder="Banco">
                </div>
                <div class="form-group col-xs-3">
                    <label for="bancoagencia">Agência</label>
                    <input type="text" name="bancoagencia" id="bancoagencia" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoagencia ?>" placeholder="Agência">
                </div>
                <div class="form-group col-xs-3">
                    <label for="bancoconta">Conta</label>
                    <input type="text" name="bancoconta" id="bancoconta" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoconta ?>" placeholder="Conta">
                </div>
                <div class="form-group  col-xs-3">
                    <label for="bancooperacao">Operação</label>
                    <input type="text" name="bancooperacao" id="bancooperacao" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancooperacao ?>" placeholder="Operação">
                </div>
        <legend>Conta Pessoal</legend>
        <div class="form-group  col-xs-3">
            <label for="banco2" class="required">Banco<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="banco2" id="banco2" class="form-control" value="<?php echo $Rhprofissional_contratacao->banco2 ?>" placeholder="Banco" required>
        </div>
        <div class="form-group col-xs-3">
            <label for="bancoagencia2" class="required">Agência<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="bancoagencia2" id="bancoagencia2" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoagencia2 ?>" placeholder="Agência" required>
        </div>
        <div class="form-group col-xs-3">
            <label for="bancoconta2" class="required">Conta<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="bancoconta2" id="bancoconta2" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancoconta2 ?>" placeholder="Conta" required>
        </div>
        <div class="form-group  col-xs-3">
            <label for="bancooperacao2" class="required">Operação<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="bancooperacao2" id="bancooperacao2" class="form-control" value="<?php echo $Rhprofissional_contratacao->bancooperacao2 ?>" placeholder="Operação" required>
        </div>
        <!-- passa o parametro da id do profissional -->
        <?php if($this->getParam('modal')){ ?>
            <input type="hidden" name="codigo_profissional" value="<?php echo $this->getParam('id'); ?>" />
        <?php }else{ ?>
        <!-- / passa o parametro da id do profissional -->
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="codigo_profissional" class="required">Profissional<span class="glyphicon glyphicon-asterisk"></span></label>
            <select name="codigo_profissional" class="form-control" id="codigo_profissional" required>
                <?php
                foreach ($Rhprofissionais as $r) {
                    if ($r->codigo == $Rhprofissional_contratacao->codigo_profissional)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
    <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
    <?php if($this->getParam('modal')){ ?>
        <input type="hidden" name="modal" value="1" />
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary" value="salvar">
        </div>
    <?php }else{ ?>
        <div class="text-right">
            <a href="<?php echo $this->Html->getUrl('Rhprofissional_contratacao', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" onclick="pergunta()" class="btn btn-primary" value="Enviar">
        </div>
    <?php } ?>
    <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
</form>
</div><!-- /ibox content -->
</div><!-- / ibox -->
</div> <!-- /col-lg 12 -->
</div><!-- /row -->
<script>
    function pergunta(){
        if (confirm('Deseja salvar as Alterações ?')){
            document.form.submit();
        }
    }
</script>