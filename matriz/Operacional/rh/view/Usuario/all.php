<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Usuários</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Listagem de Usuários</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
<div>
    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="img img-add"></span> Novo Usuario', 'Usuario', 'add', NULL, array('class' => 'btn btn-default')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'email')); ?>'>
                        E-mail
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'login')); ?>'>
                        Login
                    </a>
                </th>

                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'status')); ?>'>
                        Status
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Usuarios as $u) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($u->nome, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->email, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->login, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink($u->getRhstatus()->descricao, 'Rhstatus', 'view',
                    array('id' => $u->getRhstatus()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                echo $this->Html->getLink('<span class="img img-edit" data-toggle="tooltip" data-placement="auto" title="Editar Usuário"></span> ', 'Usuario', 'edit',
                    array('id' => $u->id), 
                    array('class' => 'btn btn-sm'));
                echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                echo $this->Html->getLink('<span class="img img-remove" data-toggle="tooltip" data-placement="auto" title="Deletar Usuário"></span> ', 'Usuario', 'delete',
                    array('id' => $u->id), 
                    array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>
</div><!-- /ibox content -->
                </div><!-- / ibox -->
            </div><!-- /wrapper-->
        </div> <!-- /col-lg 12 -->
    </div><!-- /row -->

<script>
    //toltip
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    //fim toltip
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Usuario', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Usuario', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>