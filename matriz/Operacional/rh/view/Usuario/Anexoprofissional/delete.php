<form class="form" method="post" action="<?php echo $this->Html->getUrl('Anexoprofissional', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o anexo <strong><?php echo $Anexoprofissional->nome; ?></strong>?</p>
    </div>
    <?php if($this->getParam('modal')){ ?>
        <input type="hidden" name="modal" value="1" />
        <input type="hidden" name="id" value="<?php echo $Anexoprofissional->codigo; ?>"
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-danger" value="Excluir">
        </div>
    <?php }else{ ?>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Anexoprofissional->codigo; ?>">
        <a href="<?php echo $this->Html->getUrl('Anexoprofissional', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
    <?php } ?>
</form>