<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Usuários</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Edição de Usuário</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Usuario', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="nome">Nome <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Usuarios->nome ?>" placeholder="Nome" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="email">E-mail <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="email" id="email" class="form-control" value="<?php echo $Usuarios->email ?>" onblur="validarEmail($(this).val())" placeholder="Email" required>
        </div>

        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="login">Login<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="login" id="login" class="form-control" onblur="validarLogin($(this).val())" value="<?php echo $Usuario->login ?>" placeholder="Login" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="status">Status</label>
            <select name="status" class="form-control" id="status">
                <?php
                foreach ($Rhstatus as $r) {
                    if ($r->codigo == $Usuarios->status)
                        echo '<option selected value="' . $r->codigo . '">' . $r->descricao . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->descricao . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Usuarios->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Usuario', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <button type="button" class="btn btn-primary" data-toggle="modal" onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar</button>
        <input type="submit" id="validForm" style="display: none;" />
    </div>
</form>

                </div><!-- /ibox content -->
            </div><!-- / ibox -->
        </div><!-- /wrapper-->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->

<script>

    var login = document.getElementById("login");

    function validarLogin(r){
        $.ajax({
            type:'POST',
            url:'<?php echo SITE_PATH; ?>/Login/check_login',
            data:'login='+r+'&user=<?php echo $Usuarios->id; ?>',
            success:function(txt){
                if(txt>0){
                    login.setCustomValidity("Login já cadastrado!");
                }else{
                    login.setCustomValidity("");
                }
            }
        });
    }

    var email = document.getElementById("email");

    function validarEmail(r){
        $.ajax({
            type:'POST',
            url:'<?php echo SITE_PATH; ?>/Login/check_email',
            data:'login='+r+'&user=<?php echo $Usuarios->id; ?>',
            success:function(txt){
                if(txt>0){
                    email.setCustomValidity("Email já cadastrado!");
                }else{
                    email.setCustomValidity("");
                }
            }
        });
    }
</script>