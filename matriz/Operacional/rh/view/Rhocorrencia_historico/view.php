<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">

                    <div class="text-right" style="padding-bottom: 20px">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
<legend>Ocorrência-Histórico</legend>
                    <p><strong>Nome</strong>: <?php echo $Rhocorrencia_historico->nome;?></p>
<p><strong>Periodicidade</strong>: <?php echo $Rhocorrencia_historico->periodicidade;?></p>
<p>
    <strong>Status</strong>:
    <?php
    echo $this->Html->getLink($Rhocorrencia_historico->getRhstatus()->descricao, 'Rhstatus', 'view',
    array('id' => $Rhocorrencia_historico->getRhstatus()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Ocorrência - Histórico</strong>:
    <?php
    echo $this->Html->getLink($Rhocorrencia_historico->getRhgrupo_histocorr()->nome, 'Rhgrupo_histocorr', 'view',
    array('id' => $Rhocorrencia_historico->getRhgrupo_histocorr()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
</div><!-- /ibox content -->
</div><!-- / ibox -->
</div><!-- /wrapper-->
</div> <!-- /col-lg 12 -->
</div><!-- /row -->