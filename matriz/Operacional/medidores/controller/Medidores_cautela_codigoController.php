<?php
final class Medidores_cautela_codigoController extends AppController{ 

    # página inicial do módulo Medidores_cautela_codigo
    function index(){
        $this->setTitle('Visualização de Medidores_cautela_codigo');
    }

    # lista de Medidores_cautela_codigos
    # renderiza a visão /view/Medidores_cautela_codigo/all.php
    function all(){
        $this->setTitle('Listagem de Medidores_cautela_codigo');
        $p = new Paginate('Medidores_cautela_codigo', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_cautela_codigos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Medidores_cautela_codigo
    # renderiza a visão /view/Medidores_cautela_codigo/view.php
    function view(){
        $this->setTitle('Visualização de Medidores_cautela_codigo');
        try {
            $this->set('Medidores_cautela_codigo', new Medidores_cautela_codigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_cautela_codigo', 'all');
        }
    }

    # formulário de cadastro de Medidores_cautela_codigo
    # renderiza a visão /view/Medidores_cautela_codigo/add.php
    function add(){
        $this->setTitle('Cadastro de Medidores_cautela_codigo');
        $this->set('Medidores_cautela_codigo', new Medidores_cautela_codigo);
        $this->set('Medidores_equipes',  Medidores_equipe::getList());
        $this->set('Medidores_localidades',  Medidores_localidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Medidores_cautela_codigo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_cautela_codigo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Medidores_cautela_codigo');
        $Medidores_cautela_codigo = new Medidores_cautela_codigo();
        $this->set('Medidores_cautela_codigo', $Medidores_cautela_codigo);
        try {
            $Medidores_cautela_codigo->save($_POST);
            new Msg(__('Medidores_cautela_codigo cadastrado com sucesso'));
            $this->go('Medidores_cautela_codigo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Medidores_equipes',  Medidores_equipe::getList());
        $this->set('Medidores_localidades',  Medidores_localidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Medidores_cautela_codigo
    # renderiza a visão /view/Medidores_cautela_codigo/edit.php
    function edit(){
        $this->setTitle('Edição de Medidores_cautela_codigo');
        try {
            $this->set('Medidores_cautela_codigo', new Medidores_cautela_codigo((int) $this->getParam('id')));
            $this->set('Medidores_equipes',  Medidores_equipe::getList());
            $this->set('Medidores_localidades',  Medidores_localidade::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_cautela_codigo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_cautela_codigo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_cautela_codigo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Medidores_cautela_codigo');
        try {
            $Medidores_cautela_codigo = new Medidores_cautela_codigo((int) $_POST['id']);
            $this->set('Medidores_cautela_codigo', $Medidores_cautela_codigo);
            $Medidores_cautela_codigo->save($_POST);
            new Msg(__('Medidores_cautela_codigo atualizado com sucesso'));
            $this->go('Medidores_cautela_codigo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Medidores_equipes',  Medidores_equipe::getList());
        $this->set('Medidores_localidades',  Medidores_localidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Medidores_cautela_codigo
    # renderiza a /view/Medidores_cautela_codigo/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_cautela_codigo');
        try {
            $this->set('Medidores_cautela_codigo', new Medidores_cautela_codigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_cautela_codigo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_cautela_codigo
    # redireciona para Medidores_cautela_codigo/all
    function post_delete(){
        try {
            $Medidores_cautela_codigo = new Medidores_cautela_codigo((int) $_POST['id']);
            $Medidores_cautela_codigo->delete();
            new Msg(__('Medidores_cautela_codigo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_cautela_codigo', 'all');
    }

}