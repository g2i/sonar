<?php
final class Medidores_grupoController extends AppController{ 

    # página inicial do módulo Medidores_grupo
    function index(){
        $this->setTitle('Visualização de Medidores_grupo');
    }

    # lista de Medidores_grupos
    # renderiza a visão /view/Medidores_grupo/all.php
    function all(){
        $this->setTitle('Listagem de Medidores_grupo');
        $p = new Paginate('Medidores_grupo', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_grupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Medidores_grupo
    # renderiza a visão /view/Medidores_grupo/view.php
    function view(){
        $this->setTitle('Visualização de Medidores_grupo');
        try {
            $this->set('Medidores_grupo', new Medidores_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_grupo', 'all');
        }
    }

    # formulário de cadastro de Medidores_grupo
    # renderiza a visão /view/Medidores_grupo/add.php
    function add(){
        $this->setTitle('Cadastro de Medidores_grupo');
        $this->set('Medidores_grupo', new Medidores_grupo);
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Medidores_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_grupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Medidores_grupo');
        $Medidores_grupo = new Medidores_grupo();
        $this->set('Medidores_grupo', $Medidores_grupo);
        $_POST['cadastradopor'] = Session::get('user')->id;
        $_POST['dt_cadastro'] = date('Y-m-d H:i:s');
        $_POST['situacao_id']= 1;
        try {
            $Medidores_grupo->save($_POST);
            new Msg(__('Medidores_grupo cadastrado com sucesso'));
            $this->go('Medidores_grupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Medidores_grupo
    # renderiza a visão /view/Medidores_grupo/edit.php
    function edit(){
        $this->setTitle('Edição de Medidores_grupo');
        try {
            $this->set('Medidores_grupo', new Medidores_grupo((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_grupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_grupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Medidores_grupo');
        try {
            $Medidores_grupo = new Medidores_grupo((int) $_POST['id']);
            $this->set('Medidores_grupo', $Medidores_grupo);

            $_POST['modificadopor'] = Session::get('user')->id;
            $_POST['dt_atualizacao'] = date('Y-m-d H:i:s');
        
            $Medidores_grupo->save($_POST);
            new Msg(__('Medidores_grupo atualizado com sucesso'));
            $this->go('Medidores_grupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Medidores_grupo
    # renderiza a /view/Medidores_grupo/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_grupo');
        try {
            $this->set('Medidores_grupo', new Medidores_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_grupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_grupo
    # redireciona para Medidores_grupo/all
    function post_delete(){
        try {
            $Medidores_grupo = new Medidores_grupo((int) $_POST['id']);
            $_POST['situacao_id']= 3;
            $Medidores_grupo->save($_POST);
            new Msg(__('Medidores_grupo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_grupo', 'all');
    }

}