<?php

final class Medidores_cautelaController extends AppController
{

    # página inicial do módulo Medidores_cautela
    function index()
    {
        $this->setTitle('Visualização de Cautela');
    }

    function imprimir() {
        $codigo_cautela = $this->getParam('id');
        $c = new Criteria();
        $c->addCondition('codigo_cautela', '=', $codigo_cautela);
        $Medidores_cautelas = Medidores_cautela::getList($c);
        $this->set('codigo_cautela', $codigo_cautela);
        $this->set('Medidores_cautelas', $Medidores_cautelas);
    }

    # lista de Medidores_cautelas
    # renderiza a visão /view/Medidores_cautela/all.php
    function all()
    {
        $this->setTitle('Listagem de Cautela');
        $p = new Paginate('Medidores_cautela', 10);
        $c = new Criteria();
        $c->setOrder('medidores_cautela.dt_cadastro DESC');
        $c->addCondition('medidores_cautela.situacao_id', "=", 1);
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->addCondition('localidade_id', '=', Session::get('localidade')->id);

        if (!empty($_POST['inicio'])) {
            $c->addCondition('data', '>=', $_POST['inicio']);
        }


        if (!empty($_POST['fim'])) {
            $c->addCondition('data', '<=', $_POST['fim']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        $this->set('Medidores_cautelas', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $this->set('Usuarios', Usuario::getList());
        $this->set('Medidores_equipes', Medidores_equipe::getList());
        $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList());
        $this->set('Cadastro_lacres', Cadastro_lacre::getList());
        $this->set('Cadastro_medidores', Cadastro_medidor::getList());
        $this->set('Cadastro_equipamentos', Cadastro_equipamento::getList());


    }

    # lista de Medidores_cautelas
    # renderiza a visão /view/Medidores_cautela/all.php
    function all_saidas()
    {
        $this->setTitle('Listagem de Cautela');
        $p = new Paginate('Medidores_cautela', 10);
        $c = new Criteria();
        $c->addCondition('id_tipo_movimento_cautela', '=', '2');
        $c->setOrder('medidores_cautela.dt_cadastro DESC');
        $c->addCondition('medidores_cautela.situacao_id', "=", 1);

        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
//realiza os filtros usando SQL
        if($_POST['dt_execucao'] == 1 ){
            $c->addSqlConditions('medidores_cautela.dt_execucao IS NOT NULL');
        }else {
            $c->addSqlConditions('medidores_cautela.dt_execucao IS NULL');
        }         
        $c->addCondition('localidade_id', '=', Session::get('localidade')->id);
      if (!empty($_POST['inicio'])) {
            $c->addCondition('data', '>=', $_POST['inicio']);
        }


        if (!empty($_POST['fim'])) {
            $c->addCondition('data', '<=', $_POST['fim']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        //recebe o parametro do menu e seta para a view
        $filtroCautela = !empty($_GET['tipo_cadastro']) ? $_GET['tipo_cadastro'] : $_POST['filtro']['externo']['tipo_cadastro'];
        //faz filtro para mandar para view os dados ja filtrados conforme o tipo_cadastro
        $tituloPagina = '';
        if($filtroCautela == '1C'){
            $c->addCondition('tipo_cadastro', '=', '1C');
            $tituloPagina = 'Lacres';
        }else if($filtroCautela == '2C'){
            $c->addCondition('tipo_cadastro', '=', '2C');
            $tituloPagina = 'Evolucros';
        }else if($filtroCautela == '3C'){
            $c->addCondition('tipo_cadastro', '=', '3C');
            $tituloPagina = 'Docs/Outros';
        }else if($filtroCautela == '4C'){
            $c->addCondition('tipo_cadastro', '=', '4C');
            $tituloPagina = 'Medidores';
        }
   //seta o titulo da pagina conforme a escolha do menu
        $this->set('tituloPagina', $tituloPagina);

        $nomeLacre = '';
        if($filtroCautela == '2C'){
            $nomeLacre = 'Envolucros';
        }else{
            $nomeLacre = 'Lacre (Início/Fim)';
        }
      
        $this->set('nomeLacre', $nomeLacre);
//Medidores_envolucro
        //manda para view uma lista no modelo do data-source="{'M': 'male', 'F': 'female'}" para conseguir listar e alterar os dados 
        $Medidores_envolucro = Medidores_envolucro::getList();
        $medioresDataSourceEnvolucro = '{';
        foreach($Medidores_envolucro as $m) {            
            $medioresDataSourceEnvolucro .= "'$m->id': '$m->nome', ";
        }
        $medioresDataSourceEnvolucro = substr($medioresDataSourceEnvolucro, 0, -2);
        $medioresDataSourceEnvolucro .= '}';

        //manda para view uma lista no modelo do data-source="{'M': 'male', 'F': 'female'}" para conseguir listar e alterar os dados 
        $medidores_equipes = Medidores_equipe::getList();
        $medioresDataSource = '{';
        foreach($medidores_equipes as $m) {            
            $medioresDataSource .= "'$m->id': '$m->descricao', ";
        }
        $medioresDataSource = substr($medioresDataSource, 0, -2);
        $medioresDataSource .= '}';
        
//manda para view uma lista no modelo do data-source="{'M': 'male', 'F': 'female'}" para conseguir listar e alterar os dados 
        $medidoresSubgrupo = Medidores_subgrupo::getList();
        $SubgrupoDataSource = '{';
        foreach($medidoresSubgrupo as $m) {            
            $SubgrupoDataSource .= "'$m->id': '$m->nome', ";
        }
        $SubgrupoDataSource = substr($SubgrupoDataSource, 0, -2);
        $SubgrupoDataSource .= '}';

        $k = new Criteria();
        $k->setOrder('descricao');     
        $this->set('filtroCautela', $filtroCautela);
        $this->set('Medidores_cautelas', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Medidores_equipes', $medidores_equipes);
        $this->set('Medidores_equipes_source', $medioresDataSource);
        $this->set('medioresDataSourceEnvolucro', $medioresDataSourceEnvolucro);        
        $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList($k));
        $this->set('Cadastro_lacres', Cadastro_lacre::getList());
        $this->set('Cadastro_medidores', Cadastro_medidor::getList());
        $this->set('Cadastro_equipamentos', Cadastro_equipamento::getList());
        $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList());
        $this->set('Medidores_subgrupos',   $SubgrupoDataSource);
   
      
        

    }

    # lista de Medidores_cautelas
    # renderiza a visão /view/Medidores_cautela/all.php
    function all_entradas()
    {
        $this->setTitle('Listagem de Cautela');
        $p = new Paginate('Medidores_cautela', 10);
        $c = new Criteria();
        $c->addCondition('id_tipo_movimento_cautela', '=', '3');
        $c->setOrder('medidores_cautela.dt_cadastro DESC');
        $c->addCondition('medidores_cautela.situacao_id', "=", 1);
        $m = new Criteria();
        $m->setOrder('id ASC');  //Parametro inicial pre setado no filtro SAIDA
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->addCondition('localidade_id', '=', Session::get('localidade')->id);

        if (!empty($_POST['inicio'])) {
            $c->addCondition('data', '>=', $_POST['inicio']);
        }

        if (!empty($_POST['fim'])) {
            $c->addCondition('data', '<=', $_POST['fim']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        $this->set('Medidores_cautelas', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Medidores_equipes', Medidores_equipe::getList());
        $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList($m));
        $this->set('Cadastro_lacres', Cadastro_lacre::getList());
        $this->set('Cadastro_medidores', Cadastro_medidor::getList());
        $this->set('Cadastro_equipamentos', Cadastro_equipamento::getList());


    }

    # visualiza um(a) Medidores_cautela
    # renderiza a visão /view/Medidores_cautela/view.php
    function view()
    {
        $this->setTitle('Visualização de Cautela');
        try {
            $this->set('Medidores_cautela', new Medidores_cautela((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_cautela', 'all');
        }
    }

    # formulário de cadastro de Medidores_cautela
    # renderiza a visão /view/Medidores_cautela/add.php
    function add(){
        $this->setTitle('Cadastro de Cautela');
        $this->set('Medidores_cautela', new Medidores_cautela);
        $this->set('Usuarios', Usuario::getList());
        $this->set('Medidores_equipes', Medidores_equipe::getList());
        $c = new Criteria();
        $c->addCondition('id', '=', '2');
        $c->setOrder('id ASC');
        $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList($c));
        $this->set('Cadastro_lacres', Cadastro_lacre::getList());
        $this->set('Cadastro_medidores', Cadastro_medidor::getList());
        $this->set('Medidores_envolucro', Medidores_envolucro::getList());
        //recebe o parametro do menu e seta para a view   
        $filtroCautela = $_GET['tipo_cadastro']; 
        $this->set('filtroCautela', $filtroCautela);
        $nomeLacre = '';
        if($filtroCautela == '2C'){
            $nomeLacre = 'Envolucros';
        }else{
            $nomeLacre = 'Lacre (Início/Fim)';
        }
      
        $this->set('nomeLacre', $nomeLacre);
    }

    # formulário de cadastro de Medidores_cautela
    # renderiza a visão /view/Medidores_cautela/add.php
    function add_cautela(){
        $this->setTitle('Cadastro de Cautela');
        $c = new Criteria();
       // $c->addCondition('id', '=', '2');
        $c->setOrder('id DESC');
        $this->set('Medidores_cautela', new Medidores_cautela);
        $this->set('Usuarios', Usuario::getList());
        $this->set('Medidores_equipes', Medidores_equipe::getList());
        $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList($c));
        $this->set('Cadastro_lacres', Cadastro_lacre::getList());
        $this->set('Cadastro_medidores', Cadastro_medidor::getList());
    }

    # formulário de cadastro de Medidores_cautela
    # renderiza a visão /view/Medidores_cautela/add.php
    function add_devolucao(){ 
        $this->setTitle('Cadastro de Cautela');
        $c = new Criteria();
        $c->addCondition('id', '=', '3');
        $c->setOrder('id ASC');
        $this->set('Medidores_cautela', new Medidores_cautela);
        $this->set('Usuarios', Usuario::getList());
        $this->set('Medidores_equipes', Medidores_equipe::getList());
        $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList($c));
        $this->set('Cadastro_lacres', Cadastro_lacre::getList());
        $this->set('Cadastro_medidores', Cadastro_medidor::getList());
    }

    # recebe os dados enviados via post do cadastro de Medidores_cautela
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_cautela/add.php
//    function post_add(){
//        $this->setTitle('Cadastro de Cautela');
//        $Medidores_cautela = new Medidores_cautela();
//        $this->set('Medidores_cautela', $Medidores_cautela);
//        try {
//            $Medidores_cautela->save($_POST);
//            new Msg(__('Medidores_cautela cadastrado com sucesso'));
//            $this->go('Medidores_cautela', 'all');
//        } catch (Exception $e) {
//            new Msg($e->getMessage(),3);
//        }
//        $this->set('Usuarios',  Usuario::getList());
//        $this->set('Medidores_equipes',  Medidores_equipe::getList());
//        $this->set('Medidores_tipo_movimentos',  Medidores_tipo_movimento::getList());
//        $this->set('Cadastro_lacres',  Cadastro_lacre::getList());
//        $this->set('Cadastro_medidores',  Cadastro_medidor::getList());
//    }

    function post_add()
    {
        $tipo_cadastro = $_POST['tipo_cadastro'];
        $this->setTitle('Cadastro de Cautela');
        $id_usuario = Session::get('user')->id;
        $order = array('\n', '\s', '\r', '&#13;&#10;', ' ');
        $medidores = str_replace($order, '-', $_POST['num_medidor']);
        $medidores = explode("-", $medidores);
        $lacres = str_replace($order, '-', $_POST['num_lacre']);
        $lacres = explode("-", $lacres);
        $equipamentos = str_replace($order, '-', $_POST['num_equipamento']);
        $equipamentos = explode("-", $equipamentos);
        
        //para SET equipe_id, DATA e localidade_id
        $Medidores_cautela_codigo =  new Medidores_cautela_codigo();
        $Medidores_cautela_codigo->data =  $_POST['data'];
        $Medidores_cautela_codigo->equipe_id =  $_POST['id_equipe'];
        $Medidores_cautela_codigo->localidade_id =  Session::get('localidade')->id;
        $Medidores_cautela_codigo->usuario_id =  Session::get('user')->id;
        $Medidores_cautela_codigo->save();

        //SALVA O MEDIDOR_CAUTELA E CONFERE ERROS SOBRE MEDIDORES
        $salvos = 0;
        try {
            $errosMedidores = '';
            if ($medidores[0] != '') {
                for ($x = 0; $x < count($medidores); $x++) {
                    $cautela = new Medidores_cautela();
                    $cautela->codigo_cautela = $Medidores_cautela_codigo->id;
                    $cautela->id_usuario = $id_usuario;
                    $cautela->id_equipe = $_POST['id_equipe'];
                    $cautela->data = $_POST['data'];
                    $cautela->id_tipo_movimento_cautela = $_POST['id_tipo_movimento_cautela'];
                    $cautela->num_medidor = $medidores[$x];
                    $cautela->localidade_id = Session::get('localidade')->id;
                    $cautela->medidores_cor_lacre_id = $_POST['medidores_cor_lacre_id'];
                    $cautela->tipo_cadastro = $_POST['tipo_cadastro'];
                    $cautela->medidores_envolucro_id = $_POST['medidores_envolucro_id'];
                    $cautela->situacao_id = 1;
                    $cautela->colaborador = $_POST['colaborador'];
                    $cautela->dt_cadastro = $date = date('Y-m-d');
                    if($_POST['id_tipo_movimento_cautela'] != 2){
                        if ($cautela->validaMedidorCadastro($medidores[$x], Session::get('localidade')->id) != null AND
                            $cautela->validaMedidorCautela($medidores[$x], Session::get('localidade')->id) == null) {

                            $cautela->save();
                            $salvos++;
                        } else {
                            $errosMedidores .= $medidores[$x] . '; ';
                        }
                    }
                    if($tipo_cadastro == '1C'){
                        $cautela->medidores_subgrupo_id = 4;
                    }elseif($tipo_cadastro == '2C'){
                        $cautela->medidores_subgrupo_id = 3;
                    }elseif($tipo_cadastro == '3C'){
                        $cautela->medidores_subgrupo_id = 7;
                    }elseif($tipo_cadastro == '4C'){ 
                        $cautela->medidores_subgrupo_id = 2;
                    }elseif($tipo_cadastro == '5C'){ 
                        $cautela->medidores_subgrupo_id = 1;
                    }
                    $cautela->save();
                            $salvos++;
                }
            }
        //SALVA O MEDIDOR_CAUTELA E CONFERE ERROS SOBRE OS LACRES
            $errosLacres = '';
            if ($lacres[0] != '') {
                for ($x = 0; $x < count($lacres); $x++) {
                    $cautela = new Medidores_cautela();
                    $cautela->codigo_cautela = $Medidores_cautela_codigo->id;
                    $cautela->id_usuario = $id_usuario;
                    $cautela->id_equipe = $_POST['id_equipe'];
                    $cautela->data = $_POST['data'];
                    $cautela->id_tipo_movimento_cautela = $_POST['id_tipo_movimento_cautela'];
                    $cautela->colaborador = $_POST['colaborador'];
                    $cautela->num_lacre = $lacres[$x];
                    $cautela->localidade_id = Session::get('localidade')->id;
                    $cautela->medidores_cor_lacre_id = $_POST['medidores_cor_lacre_id'];
                    $cautela->tipo_cadastro = $_POST['tipo_cadastro'];
                    $cautela->dt_cadastro = $date = date('Y-m-d');
                    $cautela->medidores_envolucro_id = $_POST['medidores_envolucro_id'];
                    $cautela->situacao_id = 1;
                    $cautela->medidores_subgrupo_id = $cautela->getSubGrupoId($lacres[$x]);
                    if($_POST['id_tipo_movimento_cautela'] != 2){
                        if ($cautela->validaLacreCadastro($lacres[$x], Session::get('localidade')->id) != null AND
                            $cautela->validaLacreCautela($lacres[$x], Session::get('localidade')->id) == null) {
                            $cautela->save();
                            $salvos++;
                        } else {
                            $errosLacres .= $lacres[$x] . '; ';
                        }
                    }
                    if($tipo_cadastro == '1C'){
                        $cautela->medidores_subgrupo_id = 4;
                    }elseif($tipo_cadastro == '2C'){
                        $cautela->medidores_subgrupo_id = 3;
                    }elseif($tipo_cadastro == '3C'){
                        $cautela->medidores_subgrupo_id = 7;
                    }elseif($tipo_cadastro == '4C'){ 
                        $cautela->medidores_subgrupo_id = 2;
                    }elseif($tipo_cadastro == '5C'){ 
                        $cautela->medidores_subgrupo_id = 1;
                    }
                    $cautela->save();
                    $salvos++;
                }
            }
 //SALVA O MEDIDOR_CAUTELA E CONFERE ERROS SOBRE EQUIPAMENTOS
            $errosEquipamentos = '';
            if ($equipamentos[0] != '') {
                for ($x = 0; $x < count($equipamentos); $x++) {
                    $cautela = new Medidores_cautela();
                    $cautela->codigo_cautela = $Medidores_cautela_codigo->id;
                    $cautela->id_usuario = $id_usuario;
                    $cautela->id_equipe = $_POST['id_equipe'];
                    $cautela->data = $_POST['data'];
                    $cautela->id_tipo_movimento_cautela = $_POST['id_tipo_movimento_cautela'];
                    $cautela->num_equipamento = $equipamentos[$x];
                    $cautela->medidores_cor_lacre_id = $_POST['medidores_cor_lacre_id'];
                    $cautela->localidade_id = Session::get('localidade')->id;
                    $cautela->tipo_cadastro = $_POST['tipo_cadastro'];
                    $cautela->dt_cadastro = $date = date('Y-m-d');
                    $cautela->medidores_envolucro_id = $_POST['medidores_envolucro_id'];
                    $cautela->situacao_id = 1;
                    $cautela->colaborador = $_POST['colaborador'];
                    if($_POST['id_tipo_movimento_cautela'] != 2){
                        if ($cautela->validaEquipamentoCadastro($equipamentos[$x], Session::get('localidade')->id) != null AND
                            $cautela->validaEquipamentoCautela($equipamentos[$x], Session::get('localidade')->id) == null
                        ) {

                            $cautela->save();
                            $salvos++;
                        } else {
                            $errosEquipamentos .= $equipamentos[$x] . '; ';
                        }
                    }
                    if($tipo_cadastro == '1C'){
                        $cautela->medidores_subgrupo_id = 4;
                    }elseif($tipo_cadastro == '2C'){
                        $cautela->medidores_subgrupo_id = 3;
                    }elseif($tipo_cadastro == '3C'){
                        $cautela->medidores_subgrupo_id = 7;
                    }elseif($tipo_cadastro == '4C'){ 
                        $cautela->medidores_subgrupo_id = 2;
                    }elseif($tipo_cadastro == '5C'){ 
                        $cautela->medidores_subgrupo_id = 1;
                    }
                    $cautela->save();
                            $salvos++;
                }
            }

        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_cautela', 'all');
        }

        if ($salvos > 0) {
            new Msg(__('Cautela(s) cadastrada(s) com sucesso'));
        }


        if (!empty($errosLacres)) {
            new Msg('Os seguintes lacres não existem, já foram cautelados ou não pertencem a base: ' . $errosLacres, 2);
        }

        if (!empty($errosMedidores)) {
            new Msg('Os seguintes medidores não existem, já foram cautelados ou não pertencem a base: ' . $errosMedidores, 2);
        }

        if (!empty($errosEquipamentos)) {
            new Msg('Os seguintes equipamentos não existem, já foram cautelados ou não pertencem a base: ' . $errosEquipamentos, 2);
        }

        $this->go('Medidores_cautela', 'imprimir', array('id' => $Medidores_cautela_codigo->id));
    }

    # formulário de edição de Medidores_cautela
    # renderiza a visão /view/Medidores_cautela/edit.php
    function edit()
    {
        $this->setTitle('Edição de Cautela');
        try {
            $Medidores_cautela = new Medidores_cautela((int)$this->getParam('id'));
            if ($Medidores_cautela->num_lacre != null) {
                $this->set('lacre', true);
            }
            if ($Medidores_cautela->num_medidor != null) {
                $this->set('medidor', true);
            }
            $this->set('Medidores_cautela', new Medidores_cautela((int)$this->getParam('id')));
            $this->set('Usuarios', Usuario::getList());
            $this->set('Medidores_equipes', Medidores_equipe::getList());
            $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList());
            $this->set('Cadastro_lacres', Cadastro_lacre::getList());
            $this->set('Cadastro_medidores', Cadastro_medidor::getList());
            $this->set('Action', $this->getParam('from'));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Medidores_cautela', $this->getParam('from'));
        }
    }

    # recebe os dados enviados via post da edição de Medidores_cautela
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_cautela/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de Cautela');
        try {
            $Medidores_cautela = new Medidores_cautela((int)$_POST['id']);
            $this->set('Medidores_cautela', $Medidores_cautela);
            $Medidores_cautela->save($_POST);
            new Msg(__('Medidores_cautela atualizado com sucesso'));
            $this->go('Medidores_cautela', $_POST['action']);
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }

        $this->set('Usuarios', Usuario::getList());
        $this->set('Medidores_equipes', Medidores_equipe::getList());
        $this->set('Medidores_tipo_movimentos', Medidores_tipo_movimento::getList());
        $this->set('Cadastro_lacres', Cadastro_lacre::getList());
        $this->set('Cadastro_medidores', Cadastro_medidor::getList());
    }

    # Confirma a exclusão ou não de um(a) Medidores_cautela
    # renderiza a /view/Medidores_cautela/delete.php
    function delete()
    { 
        $this->setTitle('Apagar Medidores_cautela');
        try {      
            $filtroCautela = $_GET['tipo_cadastro'];
            $this->set('filtroCautela', $filtroCautela);
            $this->set('Medidores_cautela', new Medidores_cautela((int)$this->getParam('id')));
       
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_cautela', $_POST['action']);
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_cautela
    # redireciona para Medidores_cautela/all
    function post_delete()
    {       
        try {
            $tipo_cadastro = $_POST['tipo_cadastro'];
            $Medidores_cautela = new Medidores_cautela((int)$_POST['id']);
            $Medidores_cautela->situacao_id = 3;
            $Medidores_cautela->save();
            $action = $Medidores_cautela->getMedidores_tipo_movimento();
            new Msg(__('Medidores_cautela apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }

        if($tipo_cadastro == '5C'){
            $this->go('Medidores_cautela', 'all_entradas');
        }
        $this->go('Medidores_cautela', 'all_saidas',array('tipo_cadastro'=> $tipo_cadastro));
      
    }
    
    public function parcial_edit()
    {
        $this->setTitle('Edição de Cautela');

        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];      
        $Medidores_cautela = new Medidores_cautela((int)$_POST['pk']);
        $this->set('Medidores_cautela', $Medidores_cautela);
            $name = $_POST['name'];

            //verifica se é o campo dt_execucao se for faz o tratamento para salvar no banco de dados 
            if($name == 'dt_execucao') {                
                $_POST['value'] = DataSQL($_POST['value']);
            }
            //salva dados do tipo text
            $Medidores_cautela->$name = $_POST['value'];

            if ($Medidores_cautela->save($_POST)) {
                $res = ['res' => $Medidores_cautela->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        
            //retorna apenas para o console um resultado para saber se deu certo
            echo json_encode($res);
            exit;
    }
}
