<?php
final class Medidores_motivoController extends AppController{ 

    # página inicial do módulo Medidores_motivo
    function index(){
        $this->setTitle('Visualizar Motivo');
    }

    # lista de Medidores_motivos
    # renderiza a visão /view/Medidores_motivo/all.php
    function all(){
        $this->setTitle('Listagem de Motivos');
        $p = new Paginate('Medidores_motivo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_motivos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Medidores_motivo
    # renderiza a visão /view/Medidores_motivo/view.php
    function view(){
        $this->setTitle('Visualizar Motivo');
        try {
            $this->set('Medidores_motivo', new Medidores_motivo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_motivo', 'all');
        }
    }

    # formulário de cadastro de Medidores_motivo
    # renderiza a visão /view/Medidores_motivo/add.php
    function add(){
        $this->setTitle('Cadastrar Motivo');
        $this->set('Medidores_motivo', new Medidores_motivo);
    }

    # recebe os dados enviados via post do cadastro de Medidores_motivo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_motivo/add.php
    function post_add(){
        $this->setTitle('Cadastrar Motivo');
        $Medidores_motivo = new Medidores_motivo();
        $this->set('Medidores_motivo', $Medidores_motivo);
        try {
            $Medidores_motivo->save($_POST);
            new Msg(__('Medidores_motivo cadastrado com sucesso'));
            $this->go('Medidores_motivo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Medidores_motivo
    # renderiza a visão /view/Medidores_motivo/edit.php
    function edit(){
        $this->setTitle('Editar Motivo');
        try {
            $this->set('Medidores_motivo', new Medidores_motivo((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_motivo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_motivo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_motivo/edit.php
    function post_edit(){
        $this->setTitle('Editar Motivo');
        try {
            $Medidores_motivo = new Medidores_motivo((int) $_POST['id']);
            $this->set('Medidores_motivo', $Medidores_motivo);
            $Medidores_motivo->save($_POST);
            new Msg(__('Medidores_motivo atualizado com sucesso'));
            $this->go('Medidores_motivo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Medidores_motivo
    # renderiza a /view/Medidores_motivo/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_motivo');
        try {
            $this->set('Medidores_motivo', new Medidores_motivo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_motivo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_motivo
    # redireciona para Medidores_motivo/all
    function post_delete(){
        try {
            $Medidores_motivo = new Medidores_motivo((int) $_POST['id']);
            $Medidores_motivo->delete();
            new Msg(__('Medidores_motivo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_motivo', 'all');
    }

}