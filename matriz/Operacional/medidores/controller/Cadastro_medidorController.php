<?php

final class Cadastro_medidorController extends AppController
{

    # página inicial do módulo Cadastro_medidor
    function index()
    {
        $this->setTitle('Visualizar Medidor');
    }

    # lista de Cadastro_medidores
    # renderiza a visão /view/Cadastro_medidor/all.php
    function all()
    {
        $this->setTitle('Listagem de Medidores');
        $p = new Paginate('Cadastro_medidor', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->addCondition('localidade_id', '=', Session::get('localidade')->id);

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Cadastro_medidores', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList());
        $this->set('Cadastro_medidor',  Cadastro_medidor::getList());

    }

    # visualiza um(a) Cadastro_medidor
    # renderiza a visão /view/Cadastro_medidor/view.php
    function view()
    {
        $this->setTitle('Visualizar Medidor');
        try {
            $this->set('Cadastro_medidor', new Cadastro_medidor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cadastro_medidor', 'all');
        }
    }

    # formulário de cadastro de Cadastro_medidor
    # renderiza a visão /view/Cadastro_medidor/add.php
    function add()
    {
        $this->setTitle('Cadastro de Medidor');
        $this->set('Cadastro_medidor', new Cadastro_medidor);
        $c = new Criteria();
        $c->addCondition("situacao_id", "=", 1);
        $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Cadastro_medidor
    # (true)redireciona ou (false) renderiza a visão /view/Cadastro_medidor/add.php
//    function post_add(){
//        $this->setTitle('Cadastro de Medidor');
//        $Cadastro_medidor = new Cadastro_medidor();
//        $this->set('Cadastro_medidor', $Cadastro_medidor);
//        try {
//            $Cadastro_medidor->save($_POST);
//            new Msg(__('Cadastro_medidor cadastrado com sucesso'));
//            $this->go('Cadastro_medidor', 'all');
//        } catch (Exception $e) {
//            new Msg($e->getMessage(),3);
//        }
//    }

    function post_add()
    {
        $this->setTitle('Cadastro de Medidor');
        $order = array('\n', '\s', '\r', '&#13;&#10;', ' ');
        $medidores = str_replace($order, '-', $_POST['num_medidor']);
        $medidores = explode("-", $medidores);
        $errosMedidores = '';

        $salvos = 0;
        for ($x = 0; $x < count($medidores); $x++) {
            if (!empty($medidores[$x])) {
                $medidor = new Cadastro_medidor();
                $medidor->num_medidor = $medidores[$x];
                $medidor->nf = $_POST['nf'];
                $medidor->edp = $_POST['edp'];
                $medidor->data = $_POST['data'];
                $medidor->observacao = $_POST['observacao'];
                $medidor->medidores_subgrupo_id = $_POST['medidores_subgrupo_id'];
                $medidor->localidade_id = Session::get('localidade')->id;

                if (Medidores_cautela::validaMedidorCadastro($medidores[$x], Session::get('localidade')->id) == NULL) {
                    $medidor->save();
                    $salvos++;
                } else {
                    $errosMedidores .= $medidores[$x] . '; ';
                }
            }
        }
        if($salvos > 0) {
            new Msg(__('Medidor(es) cadastrado(s) com sucesso'));
        }
        if(!empty($errosMedidores)) {
            new Msg('Os seguintes medidores ja foram cadastrados: ' . $errosMedidores, 2);
        }
        $this->go('Cadastro_medidor', 'all');
    }

    # formulário de edição de Cadastro_medidor
    # renderiza a visão /view/Cadastro_medidor/edit.php
    function edit()
    {
        $this->setTitle('Editar Medidor');
        try {
            $this->set('Cadastro_medidor', new Cadastro_medidor((int)$this->getParam('id')));
            $c = new Criteria();
            $c->addCondition("situacao_id", "=", 1);
            $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Cadastro_medidor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Cadastro_medidor
    # (true)redireciona ou (false) renderiza a visão /view/Cadastro_medidor/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Medidor');
        try {
            $Cadastro_medidor = new Cadastro_medidor((int)$_POST['id']);
            $this->set('Cadastro_medidor', $Cadastro_medidor);
            $medidor->medidores_subgrupo_id = $_POST['medidores_subgrupo_id'];
            $Cadastro_medidor->save($_POST);
            new Msg(__('Cadastro_medidor atualizado com sucesso'));
            $this->go('Cadastro_medidor', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Cadastro_medidor
    # renderiza a /view/Cadastro_medidor/delete.php
    function delete()
    {
        $this->setTitle('Apagar Cadastro_medidor');
        try {
            $this->set('Cadastro_medidor', new Cadastro_medidor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cadastro_medidor', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Cadastro_medidor
    # redireciona para Cadastro_medidor/all
    function post_delete()
    {
        try {
            $Cadastro_medidor = new Cadastro_medidor((int)$_POST['id']);
            $Cadastro_medidor->delete();
            new Msg(__('Cadastro_medidor apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Cadastro_medidor', 'all');
    }

    function findMedidor()
    {
        try {
            $termo = $this->getParam('termo');
            $size = (int)$this->getParam('size');
            $page = (int)$this->getParam('page');

            if (!isset($termo))
                $termo = '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

            $db = $this::getConn();
            $sqlCount = "SELECT count(*) AS count FROM `cadastro_medidor` WHERE `num_medidor` LIKE :termo";
            $sql = "SELECT `id`,`num_medidor` FROM `cadastro_medidor`  WHERE `num_medidor` LIKE  :termo LIMIT :li OFFSET :off";

            $db->query($sqlCount);
            $db->bind(":termo", "%" . $termo . "%");
            $db->execute();

            $ret = array();
            $ret["total"] = $db->getRow()->count;
            $ret["dados"] = array();

            $db->query($sql);
            $db->bind(":termo", "%" . $termo . "%");
            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

            $dados = $db->getResults();

            foreach ($dados as $d) {
                $ret["dados"][] = array('id' => $d->id, 'text' => $d->num_medidor);
            }

            echo json_encode($ret);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }

}