<?php
final class Medidores_medidorController extends AppController{ 

    # página inicial do módulo Medidores_medidor
    function index(){
        $this->setTitle('Visualizar Medidor');
    }

    # lista de Medidores_medidores
    # renderiza a visão /view/Medidores_medidor/all.php
    function all(){
        $this->setTitle('Listagem de Medidores');
        $p = new Paginate('Medidores_medidor', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        /**
         * Filtro por localidade escolhido na hora do login
         */

        $c->addCondition('localidade_id', '=', Session::get('localidade')->id);

        if (!empty($_POST['inicio'])) {
            $c->addCondition('data', '>=', $_POST['inicio']);
        }


        if (!empty($_POST['fim'])) {
            $c->addCondition('data', '<=', $_POST['fim']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        $this->set('localidade', new  Medidores_localidade(Session::get('localidade')));
        $this->set('Medidores_medidores', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Medidores_tipos',  Medidores_tipo::getList());
        $this->set('Medidores_situacaos',  Medidores_situacao::getList());
        $this->set('Medidores_motivos',  Medidores_motivo::getList());
        $this->set('Medidores_equipes',  Medidores_equipe::getList());
        $this->set('Usuarios',  Usuario::getList());
        
    }

    # visualiza um(a) Medidores_medidor
    # renderiza a visão /view/Medidores_medidor/view.php
    function view(){
        $this->setTitle('Visualizar Medidor');
        try {
            $this->set('Medidores_medidor', new Medidores_medidor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_medidor', 'all');
        }
    }

    # formulário de cadastro de Medidores_medidor
    # renderiza a visão /view/Medidores_medidor/add.php
    function add(){
        $this->setTitle('Cadastrar Medidor');
        $this->set('Medidores_medidor', new Medidores_medidor);
        $this->set('Medidores_tipos',  Medidores_tipo::getList());
        $this->set('Medidores_situacaos',  Medidores_situacao::getList());
        $this->set('Medidores_motivos',  Medidores_motivo::getList());
        $this->set('Medidores_equipes',  Medidores_equipe::getList());
        $this->set('Medidores_localidades',  Medidores_localidade::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList());
    }

    # recebe os dados enviados via post do cadastro de Medidores_medidor 
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_medidor/add.php
    function post_add(){
        $this->setTitle('Cadastrar Medidor');
        $Medidores_medidor = new Medidores_medidor();
        $this->set('Medidores_medidor', $Medidores_medidor);
        
        try {
            $_POST['usuario_id'] = Session::get('user')->id;
            $Medidores_medidor->save($_POST);
            new Msg(__('Medidores_medidor cadastrado com sucesso'));
            $this->go('Medidores_medidor', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Medidores_tipos',  Medidores_tipo::getList());
        $this->set('Medidores_situacaos',  Medidores_situacao::getList());
        $this->set('Medidores_motivos',  Medidores_motivo::getList());
        $this->set('Medidores_equipes',  Medidores_equipe::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Medidores_medidor
    # renderiza a visão /view/Medidores_medidor/edit.php
    function edit(){
        $this->setTitle('Editar Medidor');
        try {
            $this->set('Medidores_medidor', new Medidores_medidor((int) $this->getParam('id')));
            $this->set('Medidores_tipos',  Medidores_tipo::getList());
            $this->set('Medidores_situacaos',  Medidores_situacao::getList());
            $this->set('Medidores_motivos',  Medidores_motivo::getList());
            $this->set('Medidores_equipes',  Medidores_equipe::getList());
            $this->set('Medidores_localidades',  Medidores_localidade::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_medidor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_medidor
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_medidor/edit.php
    function post_edit(){
        $this->setTitle('Editar Medidor');
        try {
            $Medidores_medidor = new Medidores_medidor((int) $_POST['id']);
            $this->set('Medidores_medidor', $Medidores_medidor);
            $Medidores_medidor->alterado_por = Session::get('user')->id;
            $Medidores_medidor->alterado_em = date_timestamp_get();
            $Medidores_medidor->save($_POST);
            new Msg(__('Medidores_medidor atualizado com sucesso'));
            $this->go('Medidores_medidor', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Medidores_tipos',  Medidores_tipo::getList());
        $this->set('Medidores_situacaos',  Medidores_situacao::getList());
        $this->set('Medidores_motivos',  Medidores_motivo::getList());
        $this->set('Medidores_equipes',  Medidores_equipe::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Medidores_medidor
    # renderiza a /view/Medidores_medidor/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_medidor');
        try {
            $this->set('Medidores_medidor', new Medidores_medidor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_medidor', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_medidor
    # redireciona para Medidores_medidor/all
    function post_delete(){
        try {
            $Medidores_medidor = new Medidores_medidor((int) $_POST['id']);
            $Medidores_medidor->delete();
            new Msg(__('Medidores_medidor apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_medidor', 'all');
    }

}