<?php
final class Medidores_envolucroController extends AppController{ 

    # página inicial do módulo Medidores_envolucro
    function index(){
        $this->setTitle('Visualização de Medidores_envolucro');
    }

    # lista de Medidores_envolucros
    # renderiza a visão /view/Medidores_envolucro/all.php
    function all(){
        $this->setTitle('Listagem de Medidores_envolucro');
        $p = new Paginate('Medidores_envolucro', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_envolucros', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Medidores_envolucro
    # renderiza a visão /view/Medidores_envolucro/view.php
    function view(){
        $this->setTitle('Visualização de Medidores_envolucro');
        try {
            $this->set('Medidores_envolucro', new Medidores_envolucro((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_envolucro', 'all');
        }
    }

    # formulário de cadastro de Medidores_envolucro
    # renderiza a visão /view/Medidores_envolucro/add.php
    function add(){
        $this->setTitle('Cadastro de Medidores_envolucro');
        $this->set('Medidores_envolucro', new Medidores_envolucro);
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Medidores_envolucro
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_envolucro/add.php
    function post_add(){
        $this->setTitle('Cadastro de Medidores_envolucro');
        $Medidores_envolucro = new Medidores_envolucro();
        $this->set('Medidores_envolucro', $Medidores_envolucro);
        try {
            $Medidores_envolucro->save($_POST);
            new Msg(__('Medidores_envolucro cadastrado com sucesso'));
            $this->go('Medidores_envolucro', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Medidores_envolucro
    # renderiza a visão /view/Medidores_envolucro/edit.php
    function edit(){
        $this->setTitle('Edição de Medidores_envolucro');
        try {
            $this->set('Medidores_envolucro', new Medidores_envolucro((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_envolucro', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_envolucro
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_envolucro/edit.php
    function post_edit(){
        $this->setTitle('Edição de Medidores_envolucro');
        try {
            $Medidores_envolucro = new Medidores_envolucro((int) $_POST['id']);
            $this->set('Medidores_envolucro', $Medidores_envolucro);
            $Medidores_envolucro->save($_POST);
            new Msg(__('Medidores_envolucro atualizado com sucesso'));
            $this->go('Medidores_envolucro', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Medidores_envolucro
    # renderiza a /view/Medidores_envolucro/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_envolucro');
        try {
            $this->set('Medidores_envolucro', new Medidores_envolucro((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_envolucro', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_envolucro
    # redireciona para Medidores_envolucro/all
    function post_delete(){
        try {
            $Medidores_envolucro = new Medidores_envolucro((int) $_POST['id']);
            $Medidores_envolucro->delete();
            new Msg(__('Medidores_envolucro apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_envolucro', 'all');
    }

}