<?php
final class Medidores_tipoController extends AppController{ 

    # página inicial do módulo Medidores_tipo
    function index(){
        $this->setTitle('Visualizar Tipo');
    }

    # lista de Medidores_tipos
    # renderiza a visão /view/Medidores_tipo/all.php
    function all(){
        $this->setTitle('Listagem de Tipos');
        $p = new Paginate('Medidores_tipo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_tipos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Medidores_tipo
    # renderiza a visão /view/Medidores_tipo/view.php
    function view(){
        $this->setTitle('Visualizar Tipo');
        try {
            $this->set('Medidores_tipo', new Medidores_tipo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_tipo', 'all');
        }
    }

    # formulário de cadastro de Medidores_tipo
    # renderiza a visão /view/Medidores_tipo/add.php
    function add(){
        $this->setTitle('Cadastrar Tipo');
        $this->set('Medidores_tipo', new Medidores_tipo);
    }

    # recebe os dados enviados via post do cadastro de Medidores_tipo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_tipo/add.php
    function post_add(){
        $this->setTitle('Cadastrar Tipo');
        $Medidores_tipo = new Medidores_tipo();
        $this->set('Medidores_tipo', $Medidores_tipo);
        try {
            $Medidores_tipo->save($_POST);
            new Msg(__('Medidores_tipo cadastrado com sucesso'));
            $this->go('Medidores_tipo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Medidores_tipo
    # renderiza a visão /view/Medidores_tipo/edit.php
    function edit(){
        $this->setTitle('Editar Tipo');
        try {
            $this->set('Medidores_tipo', new Medidores_tipo((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_tipo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_tipo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_tipo/edit.php
    function post_edit(){
        $this->setTitle('Editar Tipo');
        try {
            $Medidores_tipo = new Medidores_tipo((int) $_POST['id']);
            $this->set('Medidores_tipo', $Medidores_tipo);
            $Medidores_tipo->save($_POST);
            new Msg(__('Medidores_tipo atualizado com sucesso'));
            $this->go('Medidores_tipo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Medidores_tipo
    # renderiza a /view/Medidores_tipo/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_tipo');
        try {
            $this->set('Medidores_tipo', new Medidores_tipo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_tipo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_tipo
    # redireciona para Medidores_tipo/all
    function post_delete(){
        try {
            $Medidores_tipo = new Medidores_tipo((int) $_POST['id']);
            $Medidores_tipo->delete();
            new Msg(__('Medidores_tipo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_tipo', 'all');
    }

}