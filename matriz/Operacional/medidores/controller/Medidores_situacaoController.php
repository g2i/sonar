<?php
final class Medidores_situacaoController extends AppController{ 

    # página inicial do módulo Medidores_situacao
    function index(){
        $this->setTitle('Visualizar Situação');
    }

    # lista de Medidores_situacaos
    # renderiza a visão /view/Medidores_situacao/all.php
    function all(){
        $this->setTitle('Listagem de Situações');
        $p = new Paginate('Medidores_situacao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_situacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Medidores_situacao
    # renderiza a visão /view/Medidores_situacao/view.php
    function view(){
        $this->setTitle('Visualizar Situação');
        try {
            $this->set('Medidores_situacao', new Medidores_situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_situacao', 'all');
        }
    }

    # formulário de cadastro de Medidores_situacao
    # renderiza a visão /view/Medidores_situacao/add.php
    function add(){
        $this->setTitle('Cadastrar Situação');
        $this->set('Medidores_situacao', new Medidores_situacao);
    }

    # recebe os dados enviados via post do cadastro de Medidores_situacao
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_situacao/add.php
    function post_add(){
        $this->setTitle('Cadastrar Situação');
        $Medidores_situacao = new Medidores_situacao();
        $this->set('Medidores_situacao', $Medidores_situacao);
        try {
            $Medidores_situacao->save($_POST);
            new Msg(__('Medidores_situacao cadastrado com sucesso'));
            $this->go('Medidores_situacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Medidores_situacao
    # renderiza a visão /view/Medidores_situacao/edit.php
    function edit(){
        $this->setTitle('Editar Situação');
        try {
            $this->set('Medidores_situacao', new Medidores_situacao((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_situacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_situacao
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_situacao/edit.php
    function post_edit(){
        $this->setTitle('Editar Situação');
        try {
            $Medidores_situacao = new Medidores_situacao((int) $_POST['id']);
            $this->set('Medidores_situacao', $Medidores_situacao);
            $Medidores_situacao->save($_POST);
            new Msg(__('Medidores_situacao atualizado com sucesso'));
            $this->go('Medidores_situacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Medidores_situacao
    # renderiza a /view/Medidores_situacao/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_situacao');
        try {
            $this->set('Medidores_situacao', new Medidores_situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_situacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_situacao
    # redireciona para Medidores_situacao/all
    function post_delete(){
        try {
            $Medidores_situacao = new Medidores_situacao((int) $_POST['id']);
            $Medidores_situacao->delete();
            new Msg(__('Medidores_situacao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_situacao', 'all');
    }

}