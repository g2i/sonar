<?php

final class DownloadController extends AppController
{
    # página inicial do módulo Faltas


    function baixar()
    {
        set_time_limit(0);
        $file = $_SERVER['DOCUMENT_ROOT'].trim(Cript::decript($_GET['u']));
        if (!file_exists($file)) {
            new Msg(__('Anexo não encontrado ou não existe!'), 2);
            $this->go('Matricula','all');
        }
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        readfile($file);
        exit;

    }

    function down_anexos(){
        set_time_limit(0);
        $file = $_SERVER['DOCUMENT_ROOT'].trim(Cript::decript($_GET['u']));

      
        if (!file_exists($file)) {
            new Msg(__('Anexo não encontrado ou não existe!'), 2);
            $this->go('Mensagens','all');
        }
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        readfile($file);
        exit;
    }
}