<?php
final class Medidores_localidadeController extends AppController{ 

    # página inicial do módulo Medidores_localidade
    function index(){
        $this->setTitle('Visualizar Localidade');
    }

    # lista de Medidores_localidades
    # renderiza a visão /view/Medidores_localidade/all.php
    function all(){
        $this->setTitle('Listagem de Localidades');
        $p = new Paginate('Medidores_localidade', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->addCondition('status_id', '=', 1);

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_localidades', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Medidores_localidade
    # renderiza a visão /view/Medidores_localidade/view.php
    function view(){
        $this->setTitle('Visualizar Localidade');
        try {
            $this->set('Medidores_localidade', new Medidores_localidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_localidade', 'all');
        }
    }

    # formulário de cadastro de Medidores_localidade
    # renderiza a visão /view/Medidores_localidade/add.php
    function add(){
        $this->setTitle('Cadastrar Localidade');
        $this->set('Medidores_localidade', new Medidores_localidade);
    }

    # recebe os dados enviados via post do cadastro de Medidores_localidade
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_localidade/add.php
    function post_add(){
        $this->setTitle('Cadastrar Localidade');
        $Medidores_localidade = new Medidores_localidade();
        $this->set('Medidores_localidade', $Medidores_localidade);
        $_POST['status_id']=1;
        try {
            $Medidores_localidade->save($_POST);
            new Msg(__('Medidores_localidade cadastrado com sucesso'));
            $this->go('Medidores_localidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Medidores_localidade
    # renderiza a visão /view/Medidores_localidade/edit.php
    function edit(){
        $this->setTitle('Editar Localidade ');
        try {
            $this->set('Medidores_localidade', new Medidores_localidade((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_localidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_localidade
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_localidade/edit.php
    function post_edit(){
        $this->setTitle('Editar Localidade ');
        try {
            $Medidores_localidade = new Medidores_localidade((int) $_POST['id']);
            $this->set('Medidores_localidade', $Medidores_localidade);
            $Medidores_localidade->save($_POST);
            new Msg(__('Medidores_localidade atualizado com sucesso'));
            $this->go('Medidores_localidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Medidores_localidade
    # renderiza a /view/Medidores_localidade/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_localidade');
        try {
            $this->set('Medidores_localidade', new Medidores_localidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_localidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_localidade
    # redireciona para Medidores_localidade/all
    function post_delete(){
        try {
            $Medidores_localidade = new Medidores_localidade((int) $_POST['id']);
            $Medidores_localidade->status_id = 3;
            $Medidores_localidade->save();
            new Msg(__('Localidade deletada apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_localidade', 'all');
    }

}