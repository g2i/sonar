<?php
final class Medidores_tipo_movimentoController extends AppController{ 

    # página inicial do módulo Medidores_tipo_movimento
    function index(){
        $this->setTitle('Visualização de Tipos de Movimento');
    }

    # lista de Medidores_tipo_movimentos
    # renderiza a visão /view/Medidores_tipo_movimento/all.php
    function all(){
        $this->setTitle('Listagem de Tipos de Movimento');
        $p = new Paginate('Medidores_tipo_movimento', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_tipo_movimentos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Medidores_tipo_movimento
    # renderiza a visão /view/Medidores_tipo_movimento/view.php
    function view(){
        $this->setTitle('Visualização de Tipos de Movimento');
        try {
            $this->set('Medidores_tipo_movimento', new Medidores_tipo_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_tipo_movimento', 'all');
        }
    }

    # formulário de cadastro de Medidores_tipo_movimento
    # renderiza a visão /view/Medidores_tipo_movimento/add.php
    function add(){
        $this->setTitle('Cadastro de Tipos de Movimento');
        $this->set('Medidores_tipo_movimento', new Medidores_tipo_movimento);
    }

    # recebe os dados enviados via post do cadastro de Medidores_tipo_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_tipo_movimento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Tipos de Movimento');
        $Medidores_tipo_movimento = new Medidores_tipo_movimento();
        $this->set('Medidores_tipo_movimento', $Medidores_tipo_movimento);
        try {
            $Medidores_tipo_movimento->save($_POST);
            new Msg(__('Medidores_tipo_movimento cadastrado com sucesso'));
            $this->go('Medidores_tipo_movimento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Medidores_tipo_movimento
    # renderiza a visão /view/Medidores_tipo_movimento/edit.php
    function edit(){
        $this->setTitle('Edição de Tipos de Movimento');
        try {
            $this->set('Medidores_tipo_movimento', new Medidores_tipo_movimento((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_tipo_movimento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_tipo_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_tipo_movimento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Tipos de Movimento');
        try {
            $Medidores_tipo_movimento = new Medidores_tipo_movimento((int) $_POST['id']);
            $this->set('Medidores_tipo_movimento', $Medidores_tipo_movimento);
            $Medidores_tipo_movimento->save($_POST);
            new Msg(__('Medidores_tipo_movimento atualizado com sucesso'));
            $this->go('Medidores_tipo_movimento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Medidores_tipo_movimento
    # renderiza a /view/Medidores_tipo_movimento/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_tipo_movimento');
        try {
            $this->set('Medidores_tipo_movimento', new Medidores_tipo_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_tipo_movimento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_tipo_movimento
    # redireciona para Medidores_tipo_movimento/all
    function post_delete(){
        try {
            $Medidores_tipo_movimento = new Medidores_tipo_movimento((int) $_POST['id']);
            $Medidores_tipo_movimento->delete();
            new Msg(__('Medidores_tipo_movimento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_tipo_movimento', 'all');
    }

}