<?php
final class Medidores_subgrupoController extends AppController{ 

    # página inicial do módulo Medidores_subgrupo
    function index(){
        $this->setTitle('Visualização de Medidores_subgrupo');
    }

    # lista de Medidores_subgrupos
    # renderiza a visão /view/Medidores_subgrupo/all.php
    function all(){
        $this->setTitle('Listagem de Medidores_subgrupo');
        $p = new Paginate('Medidores_subgrupo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
      
        $c->addCondition("situacao_id", "=", 1);
        $this->set('Medidores_subgrupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Medidores_grupos',  Medidores_grupo::getList());
    

    }

    # visualiza um(a) Medidores_subgrupo
    # renderiza a visão /view/Medidores_subgrupo/view.php
    function view(){
        $this->setTitle('Visualização de Medidores_subgrupo');
        try {
            $this->set('Medidores_subgrupo', new Medidores_subgrupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_subgrupo', 'all');
        }
    }

    # formulário de cadastro de Medidores_subgrupo
    # renderiza a visão /view/Medidores_subgrupo/add.php
    function add(){
        $this->setTitle('Cadastro de Medidores_subgrupo');
        $this->set('Medidores_subgrupo', new Medidores_subgrupo);
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $c = new Criteria();
        $c->addCondition("situacao_id", "=", 1);
        $this->set('Medidores_grupos',  Medidores_grupo::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Medidores_subgrupo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_subgrupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Medidores_subgrupo');
        $Medidores_subgrupo = new Medidores_subgrupo();
        $this->set('Medidores_subgrupo', $Medidores_subgrupo);
        $_POST['cadastradopor'] = Session::get('user')->id;
        $_POST['dt_cadastro'] = date('Y-m-d H:i:s');
        $_POST['situacao_id']= 1;
        try {
            $Medidores_subgrupo->save($_POST);
            new Msg(__('Medidores_subgrupo cadastrado com sucesso'));
            $this->go('Medidores_subgrupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Medidores_grupos',  Medidores_grupo::getList());
    }

    # formulário de edição de Medidores_subgrupo
    # renderiza a visão /view/Medidores_subgrupo/edit.php
    function edit(){
        $this->setTitle('Edição de Medidores_subgrupo');
        try {
            $this->set('Medidores_subgrupo', new Medidores_subgrupo((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Medidores_grupos',  Medidores_grupo::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_subgrupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_subgrupo
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_subgrupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Medidores_subgrupo');
        try {
            $Medidores_subgrupo = new Medidores_subgrupo((int) $_POST['id']);
            $this->set('Medidores_subgrupo', $Medidores_subgrupo);

            $_POST['modificadopor'] = Session::get('user')->id;
            $_POST['dt_atualizacao'] = date('Y-m-d H:i:s');

            $Medidores_subgrupo->save($_POST);
            new Msg(__('Medidores_subgrupo atualizado com sucesso'));
            $this->go('Medidores_subgrupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Medidores_grupos',  Medidores_grupo::getList());
    }

    # Confirma a exclusão ou não de um(a) Medidores_subgrupo
    # renderiza a /view/Medidores_subgrupo/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_subgrupo');
        try {
            $this->set('Medidores_subgrupo', new Medidores_subgrupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_subgrupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_subgrupo
    # redireciona para Medidores_subgrupo/all
    function post_delete(){
        try {
            $Medidores_subgrupo = new Medidores_subgrupo((int) $_POST['id']);
            $_POST['situacao_id']= 3;
            $Medidores_subgrupo->save($_POST);
            new Msg(__('Medidores_subgrupo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_subgrupo', 'all');
    }

}