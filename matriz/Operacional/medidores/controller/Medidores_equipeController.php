<?php
final class Medidores_equipeController extends AppController{ 

    # página inicial do módulo Medidores_equipe
    function index(){
        $this->setTitle('Visualizar Equipe');
    }

    # lista de Medidores_equipes
    # renderiza a visão /view/Medidores_equipe/all.php
    function all(){
        $this->setTitle('Listagem de Equipes');
        $p = new Paginate('Medidores_equipe', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_equipes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Medidores_equipe
    # renderiza a visão /view/Medidores_equipe/view.php
    function view(){
        $this->setTitle('Visualizar Equipe');
        try {
            $this->set('Medidores_equipe', new Medidores_equipe((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_equipe', 'all');
        }
    }

    # formulário de cadastro de Medidores_equipe
    # renderiza a visão /view/Medidores_equipe/add.php
    function add(){
        $this->setTitle('Cadastrar Equipe');
        $this->set('Medidores_equipe', new Medidores_equipe);
    }

    # recebe os dados enviados via post do cadastro de Medidores_equipe
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_equipe/add.php
    function post_add(){
        $this->setTitle('Cadastrar Equipe');
        $Medidores_equipe = new Medidores_equipe();
        $this->set('Medidores_equipe', $Medidores_equipe);
        try {
            $Medidores_equipe->save($_POST);
            new Msg(__('Medidores_equipe cadastrado com sucesso'));
            $this->go('Medidores_equipe', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Medidores_equipe
    # renderiza a visão /view/Medidores_equipe/edit.php
    function edit(){
        $this->setTitle('Editar Equipe');
        try {
            $this->set('Medidores_equipe', new Medidores_equipe((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Medidores_equipe', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Medidores_equipe
    # (true)redireciona ou (false) renderiza a visão /view/Medidores_equipe/edit.php
    function post_edit(){
        $this->setTitle('Editar Equipe');
        try {
            $Medidores_equipe = new Medidores_equipe((int) $_POST['id']);
            $this->set('Medidores_equipe', $Medidores_equipe);
            $Medidores_equipe->save($_POST);
            new Msg(__('Medidores_equipe atualizado com sucesso'));
            $this->go('Medidores_equipe', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Medidores_equipe
    # renderiza a /view/Medidores_equipe/delete.php
    function delete(){
        $this->setTitle('Apagar Medidores_equipe');
        try {
            $this->set('Medidores_equipe', new Medidores_equipe((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Medidores_equipe', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Medidores_equipe
    # redireciona para Medidores_equipe/all
    function post_delete(){
        try {
            $Medidores_equipe = new Medidores_equipe((int) $_POST['id']);
            $Medidores_equipe->delete();
            new Msg(__('Medidores_equipe apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Medidores_equipe', 'all');
    }

}