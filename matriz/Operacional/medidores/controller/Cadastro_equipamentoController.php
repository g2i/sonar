<?php

final class Cadastro_equipamentoController extends AppController
{

    # página inicial do módulo Cadastro_equipamento
    function index()
    {
        $this->setTitle('Visualizar Equipamento');
    }

    # lista de Cadastro_equipamentos
    # renderiza a visão /view/Cadastro_equipamento/all.php
    function all()
    {
        $this->setTitle('Listagem de Equipamentos');
        $p = new Paginate('Cadastro_equipamento', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->addCondition('localidade_id', '=', Session::get('localidade')->id);

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList());
        $this->set('Cadastro_equipamentos', $p->getPage($c));
        $this->set('nav', $p->getNav());


    }

    # visualiza um(a) Cadastro_equipamento
    # renderiza a visão /view/Cadastro_equipamento/view.php
    function view()
    {
        $this->setTitle('Visualizar Equipamento');
        try {
            $this->set('Cadastro_equipamento', new Cadastro_equipamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cadastro_equipamento', 'all');
        }
    }

    # formulário de cadastro de Cadastro_equipamento
    # renderiza a visão /view/Cadastro_equipamento/add.php
    function add()
    {
        $this->setTitle('Cadastro de Equipamento');
        $this->set('Cadastro_equipamento', new Cadastro_equipamento);
        $c = new Criteria();
        $c->addCondition("situacao_id", "=", 1);
        $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList($c));
    }

//    # recebe os dados enviados via post do cadastro de Cadastro_equipamento
//    # (true)redireciona ou (false) renderiza a visão /view/Cadastro_equipamento/add.php
//    function post_add(){
//        $this->setTitle('Cadastro de Equipamento');
//        $Cadastro_equipamento = new Cadastro_equipamento();
//        $this->set('Cadastro_equipamento', $Cadastro_equipamento);
//        try {
//            $Cadastro_equipamento->save($_POST);
//            new Msg(__('Cadastro_equipamento cadastrado com sucesso'));
//            $this->go('Cadastro_equipamento', 'all');
//        } catch (Exception $e) {
//            new Msg($e->getMessage(),3);
//        }
//    }

    function post_add()
    {
        $this->setTitle('Cadastro de Equipamento');
        $order = array('\n', '\s', '\r', '&#13;&#10;', ' ');
        $equipamentos = str_replace($order, '-', $_POST['num_equipamento']);
        $equipamentos = explode("-", $equipamentos);
        $errosEquipamentos = '';
        $salvos = 0;
        for ($x = 0; $x < count($equipamentos); $x++) {
            if (!empty($equipamentos[$x])) {
                $equipamento = new Cadastro_equipamento();
                $equipamento->num_equipamento = $equipamentos[$x];
                $equipamento->nf = $_POST['nf'];
                $equipamento->data = $_POST['data'];
                $equipamento->observacao = $_POST['observacao'];
                $equipamento->localidade_id = Session::get('localidade')->id;
                $equipamento->medidores_subgrupo_id = $_POST['medidores_subgrupo_id'];
                if (Medidores_cautela::validaEquipamentoCadastro($equipamentos[$x], Session::get('localidade')->id) == NULL) {
                    $equipamento->save();
                    $salvos++;
                } else {
                    $errosEquipamentos .= $equipamentos[$x] . '; ';
                }
            }
        }
        if($salvos > 0) {
            new Msg(__('Equipamento(s) cadastrado(s) com sucesso'));
        }


        if(!empty($errosLacres)) {
            new Msg('Os seguintes lacres ja foram cadastrados: ' . $errosLacres, 2);
        }

        $this->go('Cadastro_equipamento', 'all');
    }

    # formulário de edição de Cadastro_equipamento
    # renderiza a visão /view/Cadastro_equipamento/edit.php
    function edit()
    {
        $this->setTitle('Editar Equipamento');
        try {
            $this->set('Cadastro_equipamento', new Cadastro_equipamento((int)$this->getParam('id')));
            $c = new Criteria();
            $c->addCondition("situacao_id", "=", 1);
            $this->set('Medidores_subgrupo',  Medidores_subgrupo::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Cadastro_equipamento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Cadastro_equipamento
    # (true)redireciona ou (false) renderiza a visão /view/Cadastro_equipamento/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Equipamento');
        try {
            $Cadastro_equipamento = new Cadastro_equipamento((int)$_POST['id']);
            $this->set('Cadastro_equipamento', $Cadastro_equipamento);
            $Cadastro_equipamento->save($_POST);
            new Msg(__('Cadastro_equipamento atualizado com sucesso'));
            $this->go('Cadastro_equipamento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Cadastro_equipamento
    # renderiza a /view/Cadastro_equipamento/delete.php
    function delete()
    {
        $this->setTitle('Apagar Cadastro_equipamento');
        try {
            $this->set('Cadastro_equipamento', new Cadastro_equipamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cadastro_equipamento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Cadastro_equipamento
    # redireciona para Cadastro_equipamento/all
    function post_delete()
    {
        try {
            $Cadastro_equipamento = new Cadastro_equipamento((int)$_POST['id']);
            $Cadastro_equipamento->delete();
            new Msg(__('Cadastro_equipamento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Cadastro_equipamento', 'all');
    }

    function findEquipamento()
    {
        try {
            $termo = $this->getParam('termo');
            $size = (int)$this->getParam('size');
            $page = (int)$this->getParam('page');

            if (!isset($termo))
                $termo = '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

            $db = $this::getConn();
            $sqlCount = "SELECT count(*) AS count FROM `cadastro_equipamento` WHERE `num_equipamento` LIKE :termo";
            $sql = "SELECT `id`,`num_equipamento` FROM `cadastro_equipamento`  WHERE `num_equipamento` LIKE  :termo LIMIT :li OFFSET :off";

            $db->query($sqlCount);
            $db->bind(":termo", "%" . $termo . "%");
            $db->execute();

            $ret = array();
            $ret["total"] = $db->getRow()->count;
            $ret["dados"] = array();

            $db->query($sql);
            $db->bind(":termo", "%" . $termo . "%");
            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

            $dados = $db->getResults();

            foreach ($dados as $d) {
                $ret["dados"][] = array('id' => $d->id, 'text' => $d->num_equipamento);
            }

            echo json_encode($ret);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }

}