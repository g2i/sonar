<?php
final class Medidores_cautela_codigo extends Record{ 

    const TABLE = 'medidores_cautela_codigo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_cautela_codigo pertence a Medidores_equipe
    * @return Medidores_equipe $Medidores_equipe
    */
    function getMedidores_equipe() {
        return $this->belongsTo('Medidores_equipe','equipe_id');
    }
    
    /**
    * Medidores_cautela_codigo pertence a Medidores_localidade
    * @return Medidores_localidade $Medidores_localidade
    */
    function getMedidores_localidade() {
        return $this->belongsTo('Medidores_localidade','localidade_id');
    }
    
    /**
    * Medidores_cautela_codigo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
}