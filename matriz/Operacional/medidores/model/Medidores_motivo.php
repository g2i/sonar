<?php
final class Medidores_motivo extends Record{ 

    const TABLE = 'medidores_motivo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_motivo possui Medidores_medidores
    * @return array de Medidores_medidores
    */
    function getMedidores_medidores($criteria=NULL) {
        return $this->hasMany('Medidores_medidor','motivo_id',$criteria);
    }
}