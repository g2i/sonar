<?php
final class Medidores_situacao extends Record{ 

    const TABLE = 'medidores_situacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_situacao possui Medidores_medidores
    * @return array de Medidores_medidores
    */
    function getMedidores_medidores($criteria=NULL) {
        return $this->hasMany('Medidores_medidor','situacao_id',$criteria);
    }
}