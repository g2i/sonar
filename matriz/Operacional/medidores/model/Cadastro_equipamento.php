<?php
final class Cadastro_equipamento extends Record{ 

    const TABLE = 'cadastro_equipamento';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    function getMedidores_subgrupo() {
        return $this->belongsTo('Medidores_subgrupo','medidores_subgrupo_id');
    }
}