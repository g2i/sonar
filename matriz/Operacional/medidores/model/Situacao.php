<?php
final class Situacao extends Record{ 

    const TABLE = 'situacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Situacao possui Alunos
    * @return array de Alunos
    */
    function getAlunos($criteria=NULL) {
        return $this->hasMany('Alunos','situacao',$criteria);
    }
    
    /**
    * Situacao possui Anexos
    * @return array de Anexos
    */
    function getAnexos($criteria=NULL) {
        return $this->hasMany('Anexo','situacao',$criteria);
    }
    
    /**
    * Situacao possui Anexos
    * @return array de Anexos
    */
    function getAnexos2($criteria=NULL) {
        return $this->hasMany('Anexos','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Arquivos
    * @return array de Arquivos
    */
    function getArquivos($criteria=NULL) {
        return $this->hasMany('Arquivos','situacao',$criteria);
    }
    
    /**
    * Situacao possui Concessionaria_andamentos
    * @return array de Concessionaria_andamentos
    */
    function getConcessionaria_andamentos($criteria=NULL) {
        return $this->hasMany('Concessionaria_andamento','situacao',$criteria);
    }
    
    /**
    * Situacao possui Concessionaria_situacaos
    * @return array de Concessionaria_situacaos
    */
    function getConcessionaria_situacaos($criteria=NULL) {
        return $this->hasMany('Concessionaria_situacao','situacao',$criteria);
    }
    
    /**
    * Situacao possui Concessionaria_tipo_servicos
    * @return array de Concessionaria_tipo_servicos
    */
    function getConcessionaria_tipo_servicos($criteria=NULL) {
        return $this->hasMany('Concessionaria_tipo_servico','situacao',$criteria);
    }
    
    /**
    * Situacao possui Equipes
    * @return array de Equipes
    */
    function getEquipes($criteria=NULL) {
        return $this->hasMany('Equipe','situacao',$criteria);
    }
    
    /**
    * Situacao possui Equipe_movimentos
    * @return array de Equipe_movimentos
    */
    function getEquipe_movimentos($criteria=NULL) {
        return $this->hasMany('Equipe_movimento','situacao',$criteria);
    }
    
    /**
    * Situacao possui Frota_abastecimentos
    * @return array de Frota_abastecimentos
    */
    function getFrota_abastecimentos($criteria=NULL) {
        return $this->hasMany('Frota_abastecimentos','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_acidentes
    * @return array de Frota_acidentes
    */
    function getFrota_acidentes($criteria=NULL) {
        return $this->hasMany('Frota_acidente','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_equipamentos_veiculos
    * @return array de Frota_equipamentos_veiculos
    */
    function getFrota_equipamentos_veiculos($criteria=NULL) {
        return $this->hasMany('Frota_equipamentos_veiculo','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_localizacaos
    * @return array de Frota_localizacaos
    */
    function getFrota_localizacaos($criteria=NULL) {
        return $this->hasMany('Frota_localizacao','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_manutencoes
    * @return array de Frota_manutencoes
    */
    function getFrota_manutencoes($criteria=NULL) {
        return $this->hasMany('Frota_manutencoes','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_ocorrencias
    * @return array de Frota_ocorrencias
    */
    function getFrota_ocorrencias($criteria=NULL) {
        return $this->hasMany('Frota_ocorrencias','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_origens
    * @return array de Frota_origens
    */
    function getFrota_origens($criteria=NULL) {
        return $this->hasMany('Frota_origem','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_situacao_manutencaos
    * @return array de Frota_situacao_manutencaos
    */
    function getFrota_situacao_manutencaos($criteria=NULL) {
        return $this->hasMany('Frota_situacao_manutencao','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_tipo_acidentes
    * @return array de Frota_tipo_acidentes
    */
    function getFrota_tipo_acidentes($criteria=NULL) {
        return $this->hasMany('Frota_tipo_acidente','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_tipo_manutencaos
    * @return array de Frota_tipo_manutencaos
    */
    function getFrota_tipo_manutencaos($criteria=NULL) {
        return $this->hasMany('Frota_tipo_manutencao','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_tipo_ocorrencias
    * @return array de Frota_tipo_ocorrencias
    */
    function getFrota_tipo_ocorrencias($criteria=NULL) {
        return $this->hasMany('Frota_tipo_ocorrencia','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Frota_veiculos
    * @return array de Frota_veiculos
    */
    function getFrota_veiculos($criteria=NULL) {
        return $this->hasMany('Frota_veiculos','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Medidores_grupos
    * @return array de Medidores_grupos
    */
    function getMedidores_grupos($criteria=NULL) {
        return $this->hasMany('Medidores_grupo','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Rhfuncao_ocorrencias
    * @return array de Rhfuncao_ocorrencias
    */
    function getRhfuncao_ocorrencias($criteria=NULL) {
        return $this->hasMany('Rhfuncao_ocorrencia','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Rhlocalidades
    * @return array de Rhlocalidades
    */
    function getRhlocalidades($criteria=NULL) {
        return $this->hasMany('Rhlocalidade','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Rhprojetos
    * @return array de Rhprojetos
    */
    function getRhprojetos($criteria=NULL) {
        return $this->hasMany('Rhprojetos','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Rhusuario_localidades
    * @return array de Rhusuario_localidades
    */
    function getRhusuario_localidades($criteria=NULL) {
        return $this->hasMany('Rhusuario_localidade','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Stc_metas
    * @return array de Stc_metas
    */
    function getStc_metas($criteria=NULL) {
        return $this->hasMany('Stc_metas','situacao',$criteria);
    }
    
    /**
    * Situacao possui Tipo_anexos
    * @return array de Tipo_anexos
    */
    function getTipo_anexos($criteria=NULL) {
        return $this->hasMany('Tipo_anexos','situacao_id',$criteria);
    }
    
    /**
    * Situacao possui Unidades
    * @return array de Unidades
    */
    function getUnidades($criteria=NULL) {
        return $this->hasMany('Unidade','situacao',$criteria);
    }
}