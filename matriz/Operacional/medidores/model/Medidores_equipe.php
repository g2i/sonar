<?php
final class Medidores_equipe extends Record{ 

    const TABLE = 'medidores_equipe';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_equipe possui Medidores_medidores
    * @return array de Medidores_medidores
    */
    function getMedidores_medidores($criteria=NULL) {
        return $this->hasMany('Medidores_medidor','equipe_id',$criteria);
    }
}