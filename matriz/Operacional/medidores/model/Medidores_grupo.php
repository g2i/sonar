<?php
final class Medidores_grupo extends Record{ 

    const TABLE = 'medidores_grupo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_grupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Medidores_grupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
    
    /**
    * Medidores_grupo pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Medidores_grupo possui Medidores_subgrupos
    * @return array de Medidores_subgrupos
    */
    function getMedidores_subgrupos($criteria=NULL) {
        return $this->hasMany('Medidores_subgrupo','medidores_grupo_id',$criteria);
    }
}