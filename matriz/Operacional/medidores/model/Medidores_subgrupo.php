<?php
final class Medidores_subgrupo extends Record{ 

    const TABLE = 'medidores_subgrupo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_subgrupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Medidores_subgrupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
    
    /**
    * Medidores_subgrupo pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Medidores_subgrupo pertence a Medidores_grupo
    * @return Medidores_grupo $Medidores_grupo
    */
    function getMedidores_grupo() {
        return $this->belongsTo('Medidores_grupo','medidores_grupo_id');
    }
}