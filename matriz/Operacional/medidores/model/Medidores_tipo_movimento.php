<?php
final class Medidores_tipo_movimento extends Record{ 

    const TABLE = 'medidores_tipo_movimento';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_tipo_movimento possui Medidores_cautelas
    * @return array de Medidores_cautelas
    */
    function getMedidores_cautelas($criteria=NULL) {
        return $this->hasMany('Medidores_cautela','id_tipo_movimento_cautela',$criteria);
    }
}