<?php
final class Medidores_cautela extends Record{ 

    const TABLE = 'medidores_cautela';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_cautela pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','id_usuario');
    }
    
    /**
    * Medidores_cautela pertence a Medidores_equipe
    * @return Medidores_equipe $Medidores_equipe
    */
    function getMedidores_equipe() {
        return $this->belongsTo('Medidores_equipe','id_equipe');
    }

     /**
    * Medidores_cautela pertence a Medidores_envolucro
    * @return Medidores_envolucro $Medidores_envolucro
    */
    function getMedidores_envolucro() {
        return $this->belongsTo('Medidores_envolucro','medidores_envolucro_id');
    }
    /**
    * Medidores_cautela pertence a Medidores_tipo_movimento
    * @return Medidores_tipo_movimento $Medidores_tipo_movimento
    */
    function getMedidores_tipo_movimento() {
        return $this->belongsTo('Medidores_tipo_movimento','id_tipo_movimento_cautela');
    }
    
    /**
    * Medidores_cautela pertence a Cadastro_lacre
    * @return Cadastro_lacre $Cadastro_lacre
    */
    function getCadastro_lacre() {
        return $this->belongsTo('Cadastro_lacre','id_lacre');
    }
    
    /**
    * Medidores_cautela pertence a Cadastro_medidor
    * @return Cadastro_medidor $Cadastro_medidor
    */
    function getCadastro_medidor() {
        return $this->belongsTo('Cadastro_medidor','id_medidor');
    }

    function getCadastro_equipamento() {
        return $this->belongsTo('Cadastro_equipamento','id_equipamento');
    }

    function getSubGrupo() {
        return $this->belongsTo('medidores_subgrupo','medidores_subgrupo_id');
    }



    function validaLacreCadastro($numLacre, $localidade_id){
        $query = "SELECT * FROM cadastro_lacre cl WHERE cl.`num_lacre` = :numLacre AND cl.localidade_id = :localidade_id";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':numLacre', $numLacre);
        $db->bind(':localidade_id', $localidade_id);
        return $db->getResults();
    }

    function validaLacreCautela($numLacre, $localidade_id){
        $query = "SELECT * FROM medidores_cautela mc WHERE mc.`num_lacre` = :numLacre AND mc.localidade_id = :localidade_id";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':numLacre', $numLacre);
        $db->bind(':localidade_id', $localidade_id);
        return $db->getResults();
    }

    function validaMedidorCadastro($numMedidor, $localidade_id){
        $query = "SELECT * FROM cadastro_medidor cm WHERE cm.`num_medidor` = :numMedidor AND cm.localidade_id = :localidade_id";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':numMedidor', $numMedidor);
        $db->bind(':localidade_id', $localidade_id);
        return $db->getResults();
    }

    function validaMedidorCautela($numMedidor, $localidade_id){
        $query = "SELECT * FROM medidores_cautela mc WHERE mc.`num_medidor` = :numMedidor AND mc.localidade_id = :localidade_id";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':numMedidor', $numMedidor);
        $db->bind(':localidade_id', $localidade_id);
        return $db->getResults();
    }


    function validaEquipamentoCadastro($numEquipamento, $localidade_id){
        $query = "SELECT * FROM cadastro_equipamento ce WHERE ce.`num_equipamento` = :numEquipamento AND ce.localidade_id = :localidade_id";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':numEquipamento', $numEquipamento);
        $db->bind(':localidade_id', $localidade_id);
        return $db->getResults();
    }

    function validaEquipamentoCautela($numEquipamento, $localidade_id){
        $query = "SELECT * FROM medidores_cautela mc WHERE mc.`num_equipamento` = :numEquipamento AND mc.localidade_id = :localidade_id";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':numEquipamento', $numEquipamento);
        $db->bind(':localidade_id', $localidade_id);
        return $db->getResults();
    }

    function getSubGrupoId ($num_lacre) {        
        $query = "SELECT medidores_subgrupo_id FROM cadastro_lacre cl WHERE cl.num_lacre = :numlacre";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':numlacre', $num_lacre);
        return $db->getResults()[0]->medidores_subgrupo_id;
    }
}