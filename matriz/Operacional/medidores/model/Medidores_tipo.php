<?php
final class Medidores_tipo extends Record{ 

    const TABLE = 'medidores_tipo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_tipo possui Medidores_medidores
    * @return array de Medidores_medidores
    */
    function getMedidores_medidores($criteria=NULL) {
        return $this->hasMany('Medidores_medidor','tipo_id',$criteria);
    }
}