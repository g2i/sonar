<?php
final class Medidores_medidor extends Record{ 

    const TABLE = 'medidores_medidor';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Medidores_medidor pertence a Medidores_tipo
    * @return Medidores_tipo $Medidores_tipo
    */
    function getMedidores_tipo() {
        return $this->belongsTo('Medidores_tipo','tipo_id');
    }
    
    /**
    * Medidores_medidor pertence a Medidores_situacao
    * @return Medidores_situacao $Medidores_situacao
    */
    function getMedidores_situacao() {
        return $this->belongsTo('Medidores_situacao','situacao_id');
    }
    
    /**
    * Medidores_medidor pertence a Medidores_motivo
    * @return Medidores_motivo $Medidores_motivo
    */
    function getMedidores_motivo() {
        return $this->belongsTo('Medidores_motivo','motivo_id');
    }
    
    /**
    * Medidores_medidor pertence a Medidores_equipe
    * @return Medidores_equipe $Medidores_equipe
    */
    function getMedidores_equipe() {
        return $this->belongsTo('Medidores_equipe','equipe_id');
    }

    /**
    * Medidores_medidor pertence a Medidores_localidade
    * @return Medidores_localidade $Medidores_localidade
    */
    function getMedidores_localidade() {
        return $this->belongsTo('Medidores_localidade','localidade_id');
    }
    
    /**
    * Medidores_medidor pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }

    function getMedidores_subgrupo() {
        return $this->belongsTo('Medidores_subgrupo','medidores_subgrupo_id');
    }
}