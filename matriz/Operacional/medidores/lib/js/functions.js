/**
 * Created by Desenvolvimento on 25/01/2016.
 */

//Mascaras para os campos
$(document).ready(function () {
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
    $('.cep').mask('00000-000');
    $('.telefone').mask('(00) 0000-0000');
    $('.celular').mask('(00) 00000-0000');

});

function dell_iten(id){
    BootstrapDialog.confirm('Tem certeza que deseja excluir este dependente?', function(result){    
        if(result) {
            select_benef(id);
            var filtro = {
                benef: ''
            }
            $.ajax({
                type:'GET',
                url:root+'/Contratos/remove_item',
                data:{
                    pos:id
                },success:function (txt) {
                    filtro.benefi = getBenf();
                    $("div.list-dep").loadGrid(root+'/Contratos/add_benef', filtro, null, null);
                }
            })
        }
    });
}

function number_format(number, decimals, dec_point, thousands_sep) {

    number = (number + '')
        .replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
            var k = Math.pow(10, prec);
            return '' + (Math.round(n * k) / k)
                    .toFixed(prec);
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
        .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
            .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
            .join('0');
    }
    return s.join(dec);
}


function empty(mixed_var) {
    //  discuss at: http://phpjs.org/functions/empty/
    // original by: Philippe Baumann
    //    input by: Onno Marsman
    //    input by: LH
    //    input by: Stoyan Kyosev (http://www.svest.org/)
    // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Onno Marsman
    // improved by: Francesco
    // improved by: Marc Jansen
    // improved by: Rafal Kukawski
    //   example 1: empty(null);
    //   returns 1: true
    //   example 2: empty(undefined);
    //   returns 2: true
    //   example 3: empty([]);
    //   returns 3: true
    //   example 4: empty({});
    //   returns 4: true
    //   example 5: empty({'aFunc' : function () { alert('humpty'); } });
    //   returns 5: false

    var undef, key, i, len;
    var emptyValues = [undef, null, false, 0, '', '0'];

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }

    if (typeof mixed_var === 'object') {
        for (key in mixed_var) {
            // TODO: should we check for own properties only?
            //if (mixed_var.hasOwnProperty(key)) {
            return false;
            //}
        }
        return true;
    }

    return false;
}
$(function(){

    init();

    function empty(mixed_var) {
        var undef, key, i, len;
        var emptyValues = [undef, null, false, 0, '', '0'];

        for (i = 0, len = emptyValues.length; i < len; i++) {
            if (mixed_var === emptyValues[i]) {
                return true;
            }
        }

        if (typeof mixed_var === 'object') {
            for (key in mixed_var) {
                // TODO: should we check for own properties only?
                //if (mixed_var.hasOwnProperty(key)) {
                return false;
                //}
            }
            return true;
        }

        return false;
    }

    $( "[data-list='collapse']" ).click(function () {
        if(empty($( this ).parent().attr('class'))){
            $(this).parent().attr('class','active');
            $(this).parent().css({"border-left":"none","border-right":"4px solid #19AA8D"});
            $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360deg)","transform":" rotate(360deg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
        }else{
            $(this).parent().removeAttr('class');
            $(this).children(".fa-th-large").removeAttr('style');
            $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360reg)","transform":" rotate(360reg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
            $(this).parent().css({"border-right":"none"});
        }
    })

    $("[role='open-adm']").click(function () {
        $("[role='adm']").css({"display":"block"});
        $(".bounceInRight").css({"opacity":"10"});
    });

})

function EnviarFormulario(form){
    $(form).ajaxForm({
        success: function(d){
            /*console.log(d);*/
            Navegar('','back');
        }
    });
}

function EnviarJson(form){
    $(form).ajaxForm({
        success: function(ret){
            if(ret.result) {
                Navegar('', 'back');
            }else{
                BootstrapDialog.warning(ret.msg);
            }
        }
    });
}

function CancelFormulario(form){
    Navegar('','back');
}

function initComponente(document){

    init();

    $(document).find(".edit_local").each(function () {
        input_editable($(this), document)
    });

    $(document).find('select.medidor-ajax').each(function(){
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um medidor'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Cadastro_medidor/findMedidor/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    $(document).find('select.lacre-ajax').each(function(){
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um lacre'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Cadastro_lacre/findLacre/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });

    $(document).find('select.equipamento-ajax').each(function(){
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um equipamento'
            },
            minimumInputLength : 1,
            ajax: {
                url: root + "/Cadastro_equipamento/findEquipamento/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return { results: data.dados, more: more };
                }
            }
        });
    });
    
    $(document).find('.selectPicker').each(function() {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            minimumResultsForSearch: 6,
            placeholder: 'Selecione:'
        });
    });

    $(document).find('.pagination li a').click(function(){
        var caminho = $(this).attr('href');
        var form = $(document).find('form').serializeObject()
        $("div.table-responsive").loadPostGrid(caminho, form, null,carregar);
        return false;
    })

    $(document).find('#btn-filtro').click(function(){
        var caminho = $(document).find('form').attr('action')
        var form = $(document).find('form').serializeObject()
        $("div.table-responsive").loadPostGrid(caminho, form, null,carregar);
        return false;
    })

}

function carregar(){
    $(document).find('body').each(function(){
        initComponente(document)
    })

}
function init(){
 
    $("[data-tool='tooltip']").tooltip();

    $('[data-picker="agenda_inicio"]').datetimepicker({
        format: 'L',
        minDate:moment(),
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true,
        widgetPositioning:{
            horizontal:'auto',
            vertical:'bottom'
        }
    });

    $('[data-picker="agenda_fim"]').datetimepicker({
        format: 'L',
        minDate:moment().add(1, 'day'),
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true,
        widgetPositioning:{
            horizontal:'auto',
            vertical:'bottom'
        }
    });

    $('[data-picker="bottom"]').datetimepicker({
        format: 'L',
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true,
        widgetPositioning:{
            horizontal:'auto',
            vertical:'bottom'
        }
    });
    $('[data-picker="top"]').datetimepicker({
        format: 'L',
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true,
        widgetPositioning:{
            horizontal:'auto',
            vertical:'top'
        }
    });

    $('[data-timepicker="bottom"]').datetimepicker({
        locale:'pt-br',
        sideBySide: true,
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        showClear: true,
        allowInputToggle: true,
        widgetPositioning:{
            horizontal:'auto',
            vertical:'bottom'
        }
    });
    $('[data-timepicker="top"]').datetimepicker({
        locale:'pt-br',
        sideBySide: true,
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        showClear: true,
        allowInputToggle: true,
        widgetPositioning:{
            horizontal:'auto',
            vertical:'top'
        }
    });

    $('[data-mask="cpf"]').mask('000.000.000-00', {reverse: true});

    $('[data-mask="cep"]').mask('00000-000');

    $('[data-mask="fone"]').mask('(00)0000-00000');

    $('[data-mask="date"]').mask('00/00/0000');

    $('[data-mask="datetime"]').mask('00/00/0000 00:00');

    $('[data-mask="cnpj"]').mask('00.000.000/0000-00');

    $('[data-mask="money"]').mask('000.000.000.000.000,00', {reverse: true});

}
const benef = [];

function select_benef(id) {
    var tam = benef.length;
    if(tam<=0){
        insert(0,id);
    }else{
        var aux =1;
        var pos=0;
        $.each(benef,function(key,data){
            if(data==id){
                remove(pos,1);
                aux=2;
            }
            pos++;
        });
        if(aux==1) {
            insert(parseInt(pos),id);
        }
    }
    console.log(benef);
}

function remove(start, end) {
    benef.splice(start, end);
    return this;
}

function insert(pos, item) {
    benef.splice(pos, 0, item);
    return this;
}

function getBenf(){
    return benef;
}


function initDocument(element) {
    $(element).find('select.medicos').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            allowClear: true,
            placeholder: 'Selecione um médico'
            ,
            minimumInputLength: 1,
            ajax: {
                url: root + "/Find/findMedicos/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return {results: data.dados, more: more};
                }
            }
        });


    });
    $(element).find('select.vendedor').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            allowClear: true,
            placeholder: 'Selecione um vendedor'
            ,
            minimumInputLength: 1,
            ajax: {
                url: root + "/Vendas/findVendedores/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return {results: data.dados, more: more};
                }
            }
        });


    });
    $(element).find('select.estados').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            allowClear: true,
            placeholder: {
                id: '',
                text: 'Selecione uma Estado'
            },
            minimumInputLength: 1,
            ajax: {
                url: root + "/Index/findEstados/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return {results: data.dados, more: more};
                }
            }
        });

        $(this).change(function () {
            if ($(this).val()) {
                $(element).find('select.cidades').each(function () {
                    $(this).prop('disabled', false);
                });
            } else {
                $(element).find('select.cidades').each(function () {
                    $(this).prop('disabled', true);
                    $(this).val('').trigger('change');
                });
            }
        });
    });

    $(element).find('select.cidades').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            allowClear: true,
            placeholder: {
                id: '',
                text: 'Selecione uma Cidade'
            },
            minimumInputLength: 1,
            ajax: {
                url: root + "/Index/findCidades/",
                dataType: 'json',
                cache: true,
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page,
                        estado: $(element).find('select.estados').val()
                    };
                },
                processResults: function (data, page) {
                    var more = (page * 10) < data.total; // whether or not there are more results available
                    return {results: data.dados, more: more};
                }
            }
        });


    });
    $(element).find('select#plano_id').each(function () {
        $(this).change(function () {
            if ($(this).val() && ($('select.odonto').val()!="" && $('select.odonto').val()!=0)) {
                $(element).find('select.tabela_odonto').each(function () {
                    $(this).prop('disabled', false);
                    $.ajax({
                        type:'POST',
                        url:root+'/Beneficiarios/findTabelaodonto',
                        data:{
                            plano:$('#plano_id').val()
                        },
                        success:function(txt){
                            $('select.tabela_odonto').html(txt);
                        }
                    })
                });
            } else {
                $(element).find('select.tabela_odonto').each(function () {
                    $(this).prop('disabled', true);
                    $(this).val('').trigger('change');
                });
            }
        });
    });

    $(element).find('select.odonto').each(function () {
        $(this).change(function () {
            if (($(this).val()!="" && $(this).val()!=0)  && ($('#plano_id').val()!="")) {
                $(element).find('select.tabela_odonto').each(function () {
                    $(this).prop('disabled', false);
                    $.ajax({
                        type:'POST',
                        url:root+'/Beneficiarios/findTabelaodonto',
                        data:{
                            plano:$('#plano_id').val()
                        },
                        success:function(txt){
                            $('select.tabela_odonto').html(txt);
                        }
                    })
                });
            } else {
                $(element).find('select.tabela_odonto').each(function () {
                    $(this).prop('disabled', true);
                    $(this).val('').trigger('change');
                });
            }
        });
    });

    $(element).find('select.promais').each(function () {
        $(this).change(function () {
            if ($(this).val()!="" && $(this).val()!=0) {
                $(element).find('select.plano_promais').each(function () {
                    $(this).prop('disabled', false);
                    $.ajax({
                        type:'POST',
                        url:root+'/Beneficiarios/findPromais',
                        success:function(txt){
                            $('select.plano_promais').html(txt);
                        }
                    })
                });
            } else {
                $(element).find('select.plano_promais').each(function () {
                    $(this).prop('disabled', true);
                    $(this).val('').trigger('change');
                });
            }
        });
    });



}
const myJSONObject = {"navegacao": [{"p": 1, "u": root}]};

function Acrescentar(url){
    var pos = myJSONObject.navegacao.length;
    pos = pos+1;
    var url2 = {"p": pos, "u": url};
    myJSONObject.navegacao.push(url2);
}

function Navegar(url,param){
    var pos = myJSONObject.navegacao.length;
    if(param=='go'){
        pos = pos+1;
        var url2 = {"p": pos, "u": url};
        myJSONObject.navegacao.push(url2);
    }else{
        pos = pos-1;
        myJSONObject.navegacao.pop();
    }

    $.each(myJSONObject,function(key, data){
        $.each(data,function(id,valor){
            var modal = $('.modal');
            if(pos==1){
                modal.modal('hide');
            }else
            if(valor.p==pos) {
                if(param=='go') {
                    if(modal.find('.modal-content') != null){
                        modal.find('.modal-content').load(valor.u, function(){
                            initDocument(this);
                            init()
                        });
                    }else {
                        modal.find('.modal-body').load(valor.u, function(){
                            initDocument(this);
                            init()
                        });
                    }
                }
                else {
                    if (valor.u == "")
                        return;

                    if(modal.find('.modal-content') != null){
                        modal.find('.modal-content').load(valor.u, function(){
                            initDocument(this);
                            init()
                        });
                    }else {
                        modal.find('.modal-body').load(valor.u, function(){
                            initDocument(this);
                            init()
                        });
                    }
                }
            }
        });
    });
}


function Decrementar(url,param){
    var pos = myJSONObject.navegacao.length;
    pos = pos-1;
    myJSONObject.navegacao.pop();

    Navegar(url,param);
}

function showOnModal(url, title, size, buttons, validation,args){
    if(!empty(args)) {
        $.each(args, function (key, value) {
            if(value.indexOf("/")<0) {
                url += '&' + key + "=" + encodeURIComponent(value);
            }else{
                url += '&' + key + "=" + value;
            }
        });
    }

    LoadGif();
    Acrescentar(url);
    var dialogSize = BootstrapDialog.SIZE_NORMAL;
    var dialogButtons = [];

    if(size)
        dialogSize = size;

    if(buttons)
        dialogButtons = buttons;

    console.log(url);
    $('<div></div>').load(url, function(){
        BootstrapDialog.show({
            size: dialogSize,
            title: title,
            message: this,
            closable: false,
            type: BootstrapDialog.TYPE_DEFAULT,
            buttons: dialogButtons,
            onshown: function(dialog){
                init();
                initDocument(dialog.getModalBody());
                if(title == null){
                    dialog.getModalHeader().remove();
                }
                dialog.getModal().removeAttr('tabindex');
                CloseGif();
            },
            onhide: function(dialog){
                if($.isFunction(validation))
                    return validation.call(dialog);
                else
                    return true;
            }
        });
    });
}


function clear_form(form) {
    $(form).trigger('reset');
    $(form).find('input, textarea,select').val('');
    $(form).find('select').select2('val', '');
}
function array_reverse (array, preserve_keys) {
    //  discuss at: http://phpjs.org/functions/array_reverse/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Karol Kowalski
    //   example 1: array_reverse( [ 'php', '4.0', ['green', 'red'] ], true);
    //   returns 1: { 2: ['green', 'red'], 1: 4, 0: 'php'}

    var isArray = Object.prototype.toString.call(array) === '[object Array]',
        tmp_arr = preserve_keys ? {} : [],
        key

    if (isArray && !preserve_keys) {
        return array.slice(0)
            .reverse()
    }

    if (preserve_keys) {
        var keys = []
        for (key in array) {
            // if (array.hasOwnProperty(key)) {
            keys.push(key)
            // }
        }

        var i = keys.length
        while (i--) {
            key = keys[i]
            // FIXME: don't rely on browsers keeping keys in insertion order
            // it's implementation specific
            // eg. the result will differ from expected in Google Chrome
            tmp_arr[key] = array[key]
        }
    } else {
        for (key in array) {
            // if (array.hasOwnProperty(key)) {
            tmp_arr.unshift(array[key])
            // }
        }
    }

    return tmp_arr
}

function con_data(data){
    var reverter = array_reverse(data.split('/'),false);
    var result = reverter.join('/');
    return result;
}

function diferenca_dias(data){
    d = new Date();
    date1 = new Date(d.getFullYear()+'/'+ (parseInt(d.getMonth())+1)+'/'+ d.getDate());
    date2 = new Date(con_data(data));
    var diferenca = Math.abs(date1 - date2); //diferença em milésimos e positivo
    var dia = 1000*60*60*24; // milésimos de segundo correspondente a um dia
    var total = Math.round(diferenca/dia); //valor total de dias arredondado
   return total;
}


