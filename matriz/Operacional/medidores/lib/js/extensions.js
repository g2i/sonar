//para chamar a funcao na view, já o data-picker="bottom" é usado para formartar o campo de data, lembrando que deve estaar estar inst na index.php geral do sistema
function input_editable(campo, element) {
    var action = !empty($(campo).attr('data-action')) ? $(campo).attr('data-action') : 'parcial_edit';
    switch ($(campo).attr('data-param')) {
        case 'data':
            return $(campo).editable({
                url: root + '/' + $(campo).attr('data-controller') + '/' + action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                emptytext: 'Adicionar',
                title: $(campo).attr('data-title'),
                value: $(campo).attr('data-value'),
                tpl: '<input type="text" data-picker="bottom" class="form-control input-sm ">',
                success: function (response, newValue) {
                    if (response.res == 'erro') {
                    }

                }
            }).on('shown', function () {
                init();
            });

            break;

        case 'select':
            return $(campo).editable({
                url: root + '/' + $(campo).attr('data-controller') + '/' + action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                title: $(campo).attr('data-title'),
                display: function (value, sourceData) {
                    
                }
            }).on('shown', function () {
                
            });
            break;

        case 'decimal':
            $(campo).editable({
                url: root + '/' + $(campo).attr('data-controller') + '/' + action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                title: $(campo).attr('data-title'),
                value: $(this).attr('data-value'),
                tpl: '<input type="text" class="form-control input-sm" mask="money">',
                success: function (response, newValue) {
                    if (response.res == 'erro') {
                        BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                    }
                },
                display: function (value, source) {
                    $(campo).html('R$ ' + value);
                }

            }).on('shown', function () {
                
            });
            break

        default:
            $(campo).editable({
                url: root + '/' + $(campo).attr('data-controller') + '/' + action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                emptytext: 'Adicionar',
                title: $(campo).attr('data-title'),
                value: $(this).attr('data-value'),
                success: function (response, newValue) {
                    if (response.res == 'erro') {
                        BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                    }
                }
            }).on('shown', function () {
                
            });
            break;
    }
}