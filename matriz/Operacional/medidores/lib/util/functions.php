<?php
function convertData($data) {
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

function DataTimeBr($data)
{
    if ($data != "") {
        return  date("d/m/Y H:i", strtotime($data));//exibe no formato d/m/a
    }
}

function DataSQL($data) {
    if ($data != "") {
        list($d, $m, $y) = preg_split('/\//', $data);
        return sprintf('%04d-%02d-%02d', $y, $m, $d);
    }
}
function DataBR($data) {
    if (!empty($data)) {
        return implode('/',array_reverse(explode('-',$data)));
    }
}

function diferenca_datas($inicio,$fim){
    $diferenca = strtotime($fim) - strtotime($inicio);
    $dias = floor($diferenca / (60 * 60 * 24));
    return $dias;

}

function LimiteTexto($texto, $limite, $quebra = true){

    $tamanho = strlen($texto);

    if($tamanho <= $limite){ //Verifica se o tamanho do texto é menor ou igual ao limite

        $novo_texto = $texto;

    }else{ // Se o tamanho do texto for maior que o limite

        if($quebra == true){ // Verifica a opção de quebrar o texto

            $novo_texto = trim(substr($texto, 0, $limite))."...";

        }else{ // Se não, corta $texto na última palavra antes do limite

            $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite

            $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."..."; // Corta o $texto até a posição localizada

        }

    }

    return $novo_texto; // Retorna o valor formatado

}

function ConvertDateTime_Sql($data){
    $dat = explode('/',$data);
    $dia = $dat[0];
    $mes = $dat[1];
    $ano_hora = explode(' ',$dat[2]);
    $ano = $ano_hora[0];
    $hora = $ano_hora[1];
    return $ano.'-'.$mes.'-'.$dia.' '.$hora;
}

function getAmount($money)
{
    try{
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }catch (Exception $e){
        return null;
    }
}