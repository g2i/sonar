<?php if(!empty($manutencoes)) { ?>
    <div class="modal fade" id="banner" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header badge-danger">
                    <h3 class="modal-title">Atenção</h3>
                </div>
                <div class="modal-body">
                    <h3>Manutenções proximas do vencimento</h3>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>Veículo</th>
                                <th>Manutenção</th>
                                <th>Data da Manutenção</th>
                            </tr>

                            <?php

                            foreach ($manutencoes as $a) {
                                echo '<tr>';
                                echo '<td>' . $a->placa . '</td>';
                                echo '<td>' . $a->manutencao . '</td>';
                                echo '<td>' . DataBR($a->dt_manutencao) . '</td>';
                                echo '</tr>';
                            }
                            ?>

                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php } ?>


$(function ($) {
        $(window).load(function () {

            $("#banner").modal({
                show: true,
                keyboard: false,
                backdrop: 'static'
            });
        })
    });