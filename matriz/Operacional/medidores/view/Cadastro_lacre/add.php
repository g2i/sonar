<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lacre</h2>
        <ol class="breadcrumb">
            <li>Lacre</li>
            <li class="active">
                <strong>Cadastrar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Cadastro_lacre', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="num_lacre">Num. Lacre <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input name="num_lacre" id="num_lacre"
                                          class="form-control" required>
                            </div>
                            <!--<div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <label for="nf">NF <span class="glyphicon glyphicon-asterisk"></label>
                                <input type="text" name="nf" id="nf" class="form-control"
                                       value="<?php echo $Cadastro_lacre->nf ?>" required placeholder="NF">
                            </div>
                            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <label for="edp">EDP <span class="glyphicon glyphicon-asterisk"></label>
                                <input type="text" name="edp" id="edp" class="form-control"
                                       value="<?php echo $Cadastro_lacre->edp ?>" placeholder="EDP">
                            </div>-->
                            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <label for="data">Data <span class="glyphicon glyphicon-asterisk"></label>
                                <input type="date" name="data" id="data" class="form-control"
                                       value="<?php echo $Cadastro_lacre->data ?>" required placeholder="Data">
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="medidores_subgrupo_id"> SubCategoria<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="medidores_subgrupo_id" class="form-control" required id="medidores_subgrupo_id">
                                <option value="">Selecione:</option>
                                    <?php
                                    foreach ($Medidores_subgrupo as $m) {
                                        if ($m->id == $Medidores_subgrupo->medidores_subgrupo_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->nome . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group col-md-4 col-sm-8 col-xs-12">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao"
                                          class="form-control"><?php echo $Cadastro_lacre->observacao ?></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Cadastro_lacre', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>