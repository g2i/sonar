<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Lacre</h2>
                <ol class="breadcrumb">
                    <li>lacre</li>
                    <li class="active">
                        <strong>Editar Lacre</strong>
                    </li>
                </ol>
                <br>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Cadastro_lacre', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="num_lacre">Num. Lacre</label>
                                <input type="text" name="num_lacre" id="num_lacre"
                                       class="form-control" value="<?php echo $Cadastro_lacre->num_lacre ?>" />
                            </div>
                            <!--
                            <div class="form-group">
                                <label for="nf">NF</label>
                                <input type="text" name="nf" id="nf" class="form-control"
                                       value="<?php echo $Cadastro_lacre->nf ?>" placeholder="NF">
                            </div>
                            <div class="form-group">
                                <label for="edp">EDP</label>
                                <input type="text" name="edp" id="edp" class="form-control"
                                       value="<?php echo $Cadastro_lacre->edp ?>" placeholder="EDP">
                            </div>-->
                            <div class="form-group">
                                <label for="data">Data</label>
                                <input type="date" name="data" id="data" class="form-control"
                                       value="<?php echo $Cadastro_lacre->data ?>" placeholder="Data">
                            </div>

                            <div class="form-group">
                                <label for="medidores_subgrupo_id">SubCategoria </label>
                                <select name="medidores_subgrupo_id" class="form-control" id="medidores_subgrupo_id">
                                 <option value="">Selecione:</option>
                                    <?php
                                    foreach ($Medidores_subgrupo as $m) {
                                        if ($m->id == $Cadastro_lacre->medidores_subgrupo_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->nome . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao"
                                          class="form-control"><?php echo $Cadastro_lacre->observacao ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Cadastro_lacre->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Cadastro_lacre', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>