<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">

            <div class="col-lg-9">
                <h2>Medidor</h2>
                <ol class="breadcrumb">
                    <li>Medidor</li>
                    <li class="active">
                        <strong>Editar Medidor</strong>
                    </li>
                </ol>
                <br>
            </div>
            <br>
            <div class="ibox float-e-margins">
                <div class="">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Cadastro_medidor', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="num_medidor">Num. Medidor</label>
                                <input name="num_medidor" id="num_medidor"
                                       class="form-control" value="<?php echo $Cadastro_medidor->num_medidor ?>" />
                            </div>
                            <!--<div class="form-group">
                                <label for="nf">NF</label>
                                <input type="text" name="nf" id="nf" class="form-control"
                                       value="<?php echo $Cadastro_medidor->nf ?>" placeholder="NF">
                            </div>
                            <div class="form-group">
                                <label for="edp">EDP</label>
                                <input type="text" name="edp" id="edp" class="form-control"
                                       value="<?php echo $Cadastro_medidor->edp ?>" placeholder="EDP">
                            </div>-->
                            <div class="form-group">
                                <label for="data">Data</label>
                                <input type="date" name="data" id="data" class="form-control"
                                       value="<?php echo $Cadastro_medidor->data ?>" placeholder="Data">
                            </div>

                            <div class="form-group">
                                <label for="medidores_subgrupo_id">SubCategoria</label>
                                <select name="medidores_subgrupo_id" class="form-control" id="medidores_subgrupo_id">
                                    <option value="">Selecione:</option>
                                    <?php
                                    foreach ($Medidores_subgrupo as $m) {
                                        if ($m->id == $Medidores_subgrupo->medidores_subgrupo_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->nome . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao"
                                          class="form-control"><?php echo $Cadastro_medidor->observacao ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Cadastro_medidor->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Cadastro_medidor', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>