
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Equipamentos e Outros</h2>
    <ol class="breadcrumb">
    <li>Equipamentos e Outros</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="num_equipamento">Num. Equipamento</label>
            <input type="text" name="filtro[interno][num_equipamento]" id="num_equipamento" class="form-control" value="<?php echo $this->getParam('num_equipamento'); ?>">
        </div>
        <div class="col-md-3 form-group">
            <label for="nf">NF</label>
            <input type="text" name="filtro[interno][nf]" id="nf" class="form-control" value="<?php echo $this->getParam('nf'); ?>">
        </div>
        <div class="col-md-3 form-group">
            <label for="data">Data</label>
            <input type="date" name="filtro[interno][data]" id="data" class="form-control maskData" value="<?php echo $this->getParam('data'); ?>">
        </div>
        <div class="col-md-3 form-group">
            <label for="medidores_subgrupo_id">SubCategoria</label>
            <select name="filtro[interno][medidores_subgrupo_id]" class="form-control selectPicker" id="medidores_subgrupo_id">
            <option value="">Selecione:</option>
                <?php
                foreach ($Medidores_subgrupo as $m) {
                    if ($m->id == $Cadastro_lacre->medidores_subgrupo_id)
                        echo '<option selected value="' . $m->id . '">' . $m->nome . '</option>';
                    else
                        echo '<option value="' . $m->id . '">' . $m->nome . '</option>';
                }
                ?>
            </select>
        </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Cadastrar', 'Cadastro_equipamento', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Cadastro_equipamento', 'all', array('orderBy' => 'id')); ?>'>
                        Codigo
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Cadastro_equipamento', 'all', array('orderBy' => 'num_equipamento')); ?>'>
                        Num. Equipamento
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Cadastro_equipamento', 'all', array('orderBy' => 'nf')); ?>'>
                        NF
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Cadastro_equipamento', 'all', array('orderBy' => 'data')); ?>'>
                        Data
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Cadastro_lacre', 'all', array('orderBy' => 'id')); ?>'>
                    SubCategoria item
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Cadastro_equipamento', 'all', array('orderBy' => 'observacao')); ?>'>
                        Observação
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Cadastro_equipamentos as $c) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($c->id, 'Cadastro_equipamento', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->num_equipamento, 'Cadastro_equipamento', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->nf, 'Cadastro_equipamento', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->data, 'Cadastro_equipamento', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->getMedidores_subgrupo()->nome, 'Cadastro_lacre', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->observacao, 'Cadastro_equipamento', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Cadastro_equipamento', 'edit', 
                    array('id' => $c->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Cadastro_equipamento', 'delete', 
                    array('id' => $c->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Cadastro_equipamento', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Cadastro_equipamento', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>