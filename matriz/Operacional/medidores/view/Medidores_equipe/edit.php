<!--<div class="row wrapper border-bottom white-bg page-heading">-->
<!--    <div class="col-lg-9">-->
<!--        <h2>Equipe</h2>-->
<!--        <ol class="breadcrumb">-->
<!--            <li>Equipe</li>-->
<!--            <li class="active">-->
<!--                <strong>Editar Equipe</strong>-->
<!--            </li>-->
<!--        </ol>-->
<!--    </div>-->
<!--</div>-->
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Equipe / Colaborador</h2>
                <ol class="breadcrumb">
                    <li>Equipe / Colaborador</li>
                    <li class="active">
                        <strong>Editar</strong>
                    </li>
                </ol>
                <br>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Medidores_equipe', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="descricao">Descrição</label>
                                <input type="text" name="descricao" id="descricao" class="form-control"
                                       value="<?php echo $Medidores_equipe->descricao ?>" placeholder="Descrição">
                            </div>

                            <div class="form-group">
                                <label class="required" for="codigo">Código </label>
                                <input type="text" name="codigo" id="codigo" class="form-control"
                                       value="<?php echo $Medidores_equipe->codigo ?>" required placeholder="Código">
                            </div>
                        </div>

                        <input type="hidden" name="id" value="<?php echo $Medidores_equipe->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_equipe', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>