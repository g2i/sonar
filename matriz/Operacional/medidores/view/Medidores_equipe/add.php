<!--<div class="row wrapper border-bottom white-bg page-heading">-->
<!--    <div class="col-lg-9">-->
<!--        <h2>Equipe</h2>-->
<!--        <ol class="breadcrumb">-->
<!--            <li>Equipe</li>-->
<!--            <li class="active">-->
<!--                <strong>Adicionar Equipe</strong>-->
<!--            </li>-->
<!--        </ol>-->
<!--    </div>-->
<!--</div>-->
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Equipe / Colaborador</h2>
                <ol class="breadcrumb">
                    <li>Equipe / Colaborador</li>
                    <li class="active">
                        <strong>Cadastro</strong>
                    </li>
                </ol>
                <br>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Medidores_equipe', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label class="required" for="descricao">Descrição <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="descricao" id="descricao" class="form-control"
                                       value="<?php echo $Medidores_equipe->descricao ?>" required placeholder="Descricao">
                            </div>

                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label class="required" for="codigo">Código <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="codigo" id="codigo" class="form-control"
                                       value="<?php echo $Medidores_equipe->codigo ?>" required placeholder="Código">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_equipe', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>