<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimentação</h2>
        <ol class="breadcrumb">
            <li>Movimentação</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="cod_medidor">Cod. Medidor</label>
                                    <input type="text" name="filtro[interno][cod_medidor]" id="cod_medidor"
                                           class="form-control" value="<?php echo $this->getParam('cod_medidor'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="num_lacre">Lacre</label>
                                    <input type="text" name="filtro[interno][num_lacre]" id="num_lacre"
                                           class="form-control" value="<?php echo $this->getParam('num_lacre'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="num_equipamento">Equipamentos e Outros</label>
                                    <input type="text" name="filtro[interno][num_equipamento]" id="num_equipamento"
                                           class="form-control" value="<?php echo $this->getParam('num_equipamento'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="equipe_id">Equipe</label>
                                    <select name="filtro[externo][equipe_id]" class="form-control" id="equipe_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Medidores_equipes as $m): ?>
                                            <?php echo '<option value="' . $m->id . '">' . $m->descricao . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="uc">UC</label>
                                    <input type="text" name="filtro[interno][uc]" id="uc" class="form-control"
                                           value="<?php echo $this->getParam('uc'); ?>">
                                </div>

                                <div class="col-md-3 form-group">
                                    <label for="data_inicial">Inicio</label>
                                    <input type="date" name="inicio" id="inicio"
                                           class="form-control maskData" value="">
                                </div>

                                <div class="col-md-3 form-group">
                                    <label for="data_final">Fim</label>
                                    <input type="date" name="fim" id="fim"
                                           class="form-control maskData" value="">
                                </div>

                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Cadastrar', 'Medidores_medidor', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'cod_medidor')); ?>'>
                                            Cod. Medidor
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'num_lacre')); ?>'>
                                            Lacre
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'tipo_id')); ?>'>
                                            Tipo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'situacao_id')); ?>'>
                                            Situação
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'motivo_id')); ?>'>
                                            Motivo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'equipe_id')); ?>'>
                                            Equipe
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'los')); ?>'>
                                            LOS
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'uc')); ?>'>
                                            UC
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'localidade_id')); ?>'>
                                            Localidade
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'observacao')); ?>'>
                                            Observação
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                            Usuario
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => 'data')); ?>'>
                                            Data
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Medidores_medidores as $m) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->cod_medidor, 'Medidores_medidor', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->num_lacre, 'Medidores_medidor', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_tipo()->descricao, 'Medidores_tipo', 'view',
                                        array('id' => $m->getMedidores_tipo()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_situacao()->descricao, 'Medidores_situacao', 'view',
                                        array('id' => $m->getMedidores_situacao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_motivo()->descricao, 'Medidores_motivo', 'view',
                                        array('id' => $m->getMedidores_motivo()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_equipe()->descricao, 'Medidores_equipe', 'view',
                                        array('id' => $m->getMedidores_equipe()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->los, 'Medidores_medidor', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->uc, 'Medidores_medidor', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_localidade()->cidade, 'Medidores_localidade', 'view',
                                        array('id' => $m->getMedidores_localidade()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->observacao, 'Medidores_medidor', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($m->getUsuario()->nome, 'Usuario', 'view',
                                        array('id' => $m->getUsuario()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($m->data), 'Medidores_medidor', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';   
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Medidores_medidor', 'edit',
                                        array('id' => $m->id),
                                        array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Medidores_medidor', 'delete',
                                        array('id' => $m->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Medidores_medidor', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Medidores_medidor', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>