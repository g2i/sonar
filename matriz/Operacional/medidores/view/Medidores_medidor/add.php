<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimentação</h2>
        <ol class="breadcrumb">
            <li>Movimentação</li>
            <li class="active">
                <strong>Cadastro</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Medidores_medidor', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="id_medidor" class="required">Cod. Medidor<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="id_medidor" name="id_medidor" required class="form-control medidor-ajax"></select>
                                </div>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="id_lacre">Lacre</label>
                                    <select id="id_lacre" name="id_lacre"  class="form-control lacre-ajax"></select>
                                </div>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="id_equipamento">Docs,Envoltos e Outros</label>
                                    <select id="id_equipamento" name="id_equipamento"  class="form-control equipamento-ajax"></select>
                                </div>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="los">LOS <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="los" id="los" class="form-control"
                                       value="<?php echo $Medidores_medidor->los ?>" required placeholder="LOS">
                            </div>


                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="uc">UC <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="uc" id="uc" class="form-control"
                                       value="<?php echo $Medidores_medidor->uc ?>" required placeholder="UC">
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="data">Data <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="date" name="data" id="data" class="form-control"
                                       value="<?php echo $Medidores_medidor->data ?>" required placeholder="00/00/00">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="tipo_id">Tipo <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="tipo_id" class="form-control" required id="tipo_id">
                                    <?php
                                    foreach ($Medidores_tipos as $m) {
                                        if ($m->id == $Medidores_medidor->tipo_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="situacao_id">Situação <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="situacao_id" class="form-control" required id="situacao_id">
                                    <?php
                                    foreach ($Medidores_situacaos as $m) {
                                        if ($m->id == $Medidores_medidor->situacao_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="motivo_id">Motivo <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="motivo_id" class="form-control" required id="motivo_id">
                                    <?php
                                    foreach ($Medidores_motivos as $m) {
                                        if ($m->id == $Medidores_medidor->motivo_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="equipe_id">Equipe <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="equipe_id" class="form-control" required id="equipe_id">
                                    <?php
                                    foreach ($Medidores_equipes as $m) {
                                        if ($m->id == $Medidores_medidor->equipe_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="localidade_id">Localidade <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="localidade_id" class="form-control" required id="localidade_id">
                                    <?php
                                    foreach ($Medidores_localidades as $m) {
                                        if ($m->id == $Medidores_medidor->localidade_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->cidade . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->cidade . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao"
                                          class="form-control"><?php echo $Medidores_medidor->observacao ?></textarea>
                            </div>
                            
                            <input type="hidden" id="cod_medidor" name="cod_medidor"/>
                            <input type="hidden" id="num_lacre" name="num_lacre"/>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_medidor', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    
    $('#id_medidor').change(function() {

        $('#cod_medidor').val($("#id_medidor :selected").text());
        console.log($('#cod_medidor').val());

    });

    $('#id_lacre').change(function() {

        $('#num_lacre').val($("#id_lacre :selected").text());
        console.log($('#num_lacre').val());

    });
    
</script>