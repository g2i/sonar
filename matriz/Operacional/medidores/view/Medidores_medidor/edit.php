<!--<div class="row wrapper border-bottom white-bg page-heading">-->
<!--    <div class="col-lg-9">-->
<!--        <h2>Medidor</h2>-->
<!--        <ol class="breadcrumb">-->
<!--            <li>Medidor</li>-->
<!--            <li class="active">-->
<!--                <strong>Editar Medidor</strong>-->
<!--            </li>-->
<!--        </ol>-->
<!--    </div>-->
<!--</div>-->
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Movimentação</h2>
                <ol class="breadcrumb">
                    <li>Movimentação</li>
                    <li class="active">
                        <strong>Editar </strong>
                    </li>
                </ol>
                <br>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Medidores_medidor', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="cod_medidor">Cod. Medidor</label>
                                <input type="text" name="cod_medidor" id="cod_medidor" class="form-control"
                                       value="<?php echo $Medidores_medidor->cod_medidor ?>" placeholder="Cod. Medidor">
                            </div>
                            <div class="form-group">
                                <label for="num_lacre">Lacre</label>
                                <input type="text" name="num_lacre" id="num_lacre" class="form-control"
                                       value="<?php echo $Medidores_medidor->num_lacre ?>" placeholder="Num. Lacre">
                            </div>
                            <div class="form-group">
                                <label for="los">LOS</label>
                                <input type="text" name="los" id="los" class="form-control"
                                       value="<?php echo $Medidores_medidor->los ?>" placeholder="LOS">
                            </div>
                            <div class="form-group">
                                <label for="uc">UC</label>
                                <input type="text" name="uc" id="uc" class="form-control"
                                       value="<?php echo $Medidores_medidor->uc ?>" placeholder="UC">
                            </div>
                            <div class="form-group">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao"
                                          class="form-control"><?php echo $Medidores_medidor->observacao ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="data">Data</label>
                                <input type="date" name="data" id="data" class="form-control"
                                       value="<?php echo $Medidores_medidor->data ?>" placeholder="00/00/0000">
                            </div>
                            <div class="form-group">
                                <label for="tipo_id">Tipo</label>
                                <select name="tipo_id" class="form-control" id="tipo_id">
                                    <?php
                                    foreach ($Medidores_tipos as $m) {
                                        if ($m->id == $Medidores_medidor->tipo_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="situacao_id">Situação</label>
                                <select name="situacao_id" class="form-control" id="situacao_id">
                                    <?php
                                    foreach ($Medidores_situacaos as $m) {
                                        if ($m->id == $Medidores_medidor->situacao_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="motivo_id">Motivo</label>
                                <select name="motivo_id" class="form-control" id="motivo_id">
                                    <?php
                                    foreach ($Medidores_motivos as $m) {
                                        if ($m->id == $Medidores_medidor->motivo_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="equipe_id">Equipe</label>
                                <select name="equipe_id" class="form-control" id="equipe_id">
                                    <?php
                                    foreach ($Medidores_equipes as $m) {
                                        if ($m->id == $Medidores_medidor->equipe_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="localidade_id">Localidade</label>
                                <select name="localidade_id" class="form-control" id="localidade_id">
                                    <?php
                                    foreach ($Medidores_localidades as $m) {
                                        if ($m->id == $Medidores_medidor->localidade_id)
                                            echo '<option selected value="' . $m->id . '">' . $m->cidade . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->cidade . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Medidores_medidor->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_medidor', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>