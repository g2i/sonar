
<h1 class="text-center" style="padding-top: 100px; padding-bottom: 20px">Login</h1>
<div class="row" style="max-width: 350px; margin: auto; paddding-top: 250px;">
    <form class="form" method="post">
        <div class="form-group">
            <input type="text" name="login" class="form-control" placeholder="Login" autofocus="" required>
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Senha" required>
        </div>

        <div class="form-group">
            <label for="localidade">Localidade</label>
            <select name="localidade" class="form-control" id="localidade" required>
                <option value="">Selecione</option>
                <?php
                foreach ($localidades as $l) {
                    echo '<option value="' . $l->id . '">' . $l->cidade . ' - ' . $l->estado .'</option>';
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Acessar</button>
            <span class="pull-right">
                </span>
            <!--            <span> -->
            <?php //echo $this->Html->getLink('Esqueceu sua senha?','Login', 'send');?><!--</span>-->
        </div>
    </form>


</div>

