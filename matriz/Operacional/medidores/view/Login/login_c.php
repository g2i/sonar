<h1 class="text-center">Acessar agendamentos</h1>
<div class="row" style="max-width: 350px; margin: auto">
    <form class="form" method="post">
        <div class="form-group">
            <input type="text" name="login" class="form-control" placeholder="Login" autofocus="" required>
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Senha" required>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit">Acessar</button>
            <span class="pull-right">
                <span> <?php echo $this->Html->getLink('Registrar','Cliente', 'add',array('a'=>1));?></span>
            </span>
            <span> <?php echo $this->Html->getLink('Esqueceu a senha?','Login', 'send');?></span>

        </div>
    </form>
</div>