
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>SubCategoria</h2>
    <ol class="breadcrumb">
    <li>SubCategoria</li>
    <li class="active">
    <strong>Cadastro </strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Medidores_subgrupo', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Medidores_subgrupo->nome ?>" placeholder="Nome">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="medidores_grupo_id">Categoria</label>
            <select name="medidores_grupo_id" class="form-control" id="medidores_grupo_id">
                <?php
                foreach ($Medidores_grupos as $m) {
                    if ($m->id == $Medidores_subgrupo->medidores_grupo_id)
                        echo '<option selected value="' . $m->id . '">' . $m->nome . '</option>';
                    else
                        echo '<option value="' . $m->id . '">' . $m->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Medidores_subgrupo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>