
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Medidores_envolucro</h2>
    <ol class="breadcrumb">
    <li>Medidores_envolucro</li>
    <li class="active">
    <strong>Editar Medidores_envolucro</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Medidores_envolucro', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Medidores_envolucro->nome ?>" placeholder="Nome">
        </div>
        <div class="form-group">
            <label for="dt_cadastro">Dt_cadastro</label>
            <input type="date" name="dt_cadastro" id="dt_cadastro" class="form-control" value="<?php echo $Medidores_envolucro->dt_cadastro ?>" placeholder="Dt_cadastro">
        </div>
        <div class="form-group">
            <label for="dt_modificacao">Dt_modificacao</label>
            <input type="date" name="dt_modificacao" id="dt_modificacao" class="form-control" value="<?php echo $Medidores_envolucro->dt_modificacao ?>" placeholder="Dt_modificacao">
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Medidores_envolucro->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Medidores_envolucro->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Medidores_envolucro', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>