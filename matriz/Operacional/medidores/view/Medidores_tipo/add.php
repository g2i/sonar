<!--<div class="row wrapper border-bottom white-bg page-heading">-->
<!--    <div class="col-lg-9">-->
<!--        <h2>Tipo</h2>-->
<!--        <ol class="breadcrumb">-->
<!--            <li>Tipo</li>-->
<!--            <li class="active">-->
<!--                <strong>Adicionar Tipo</strong>-->
<!--            </li>-->
<!--        </ol>-->
<!--    </div>-->
<!--</div>-->
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">

            <div class="col-lg-9">
                <h2>Tipo</h2>
                <ol class="breadcrumb">
                    <li>Tipo</li>
                    <li class="active">
                        <strong>Cadastro</strong>
                    </li>
                </ol>
                <br>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Medidores_tipo', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label class="required" for="descricao">Descrição<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="descricao" id="descricao" class="form-control"
                                       value="<?php echo $Medidores_tipo->descricao ?>" required
                                       placeholder="Descrição">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_tipo', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>