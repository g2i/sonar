
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Usuario</h2>
    <ol class="breadcrumb">
    <li>Usuario</li>
    <li class="active">
    <strong>Editar Usuario</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Usuario', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label class="required" for="senha">Senha <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="password" name="senha" id="senha" class="form-control" value="<?php echo $Usuario->senha ?>" placeholder="Senha" required>
        </div>
        <div class="form-group">
            <label class="required" for="email">Email <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="email" id="email" class="form-control" value="<?php echo $Usuario->email ?>" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="dtCadastro">DtCadastro</label>
        </div>
        <div class="form-group">
            <label for="cadastradopor">Cadastradopor</label>
            <input type="number" name="cadastradopor" id="cadastradopor" class="form-control" value="<?php echo $Usuario->cadastradopor ?>" placeholder="Cadastradopor">
        </div>
        <div class="form-group">
            <label for="dtAtualizacao">DtAtualizacao</label>
        </div>
        <div class="form-group">
            <label for="atualizadopor">Atualizadopor</label>
            <input type="number" name="atualizadopor" id="atualizadopor" class="form-control" value="<?php echo $Usuario->atualizadopor ?>" placeholder="Atualizadopor">
        </div>
        <div class="form-group">
            <label class="required" for="nome">Nome <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Usuario->nome ?>" placeholder="Nome" required>
        </div>
        <div class="form-group">
            <label for="login">Login</label>
            <input type="text" name="login" id="login" class="form-control" value="<?php echo $Usuario->login ?>" placeholder="Login">
        </div>
        <div class="form-group">
            <label for="status">Rhstatus</label>
            <select name="status" class="form-control" id="status">
                <?php
                foreach ($Rhstatus as $r) {
                    if ($r->codigo == $Usuario->status)
                        echo '<option selected value="' . $r->codigo . '">' . $r->descricao . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->descricao . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Usuario->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Usuario', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>