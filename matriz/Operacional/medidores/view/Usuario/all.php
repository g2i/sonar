
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Usuario</h2>
    <ol class="breadcrumb">
    <li>Usuario</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Usuario', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'senha')); ?>'>
                        senha
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'email')); ?>'>
                        email
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'status')); ?>'>
                        status
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'dtCadastro')); ?>'>
                        dtCadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'cadastradopor')); ?>'>
                        cadastradopor
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'dtAtualizacao')); ?>'>
                        dtAtualizacao
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'atualizadopor')); ?>'>
                        atualizadopor
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'nome')); ?>'>
                        nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Usuario', 'all', array('orderBy' => 'login')); ?>'>
                        login
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Usuarios as $u) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($u->id, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->senha, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->email, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->dtCadastro, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->cadastradopor, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->dtAtualizacao, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->atualizadopor, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->nome, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->login, 'Usuario', 'view',
                    array('id' => $u->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($u->getRhstatus()->descricao, 'Rhstatus', 'view',
                    array('id' => $u->getRhstatus()->descricao), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Usuario', 'edit', 
                    array('id' => $u->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Usuario', 'delete', 
                    array('id' => $u->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Usuario', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Usuario', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>