<style>
select.form-control option.preto
{
    background-color: #000000;
}
select.form-control option.vermelho
{
    background-color: #ff0000;
}
select.form-control option.rosa
{
    background-color: #ff00ff;
}
.greenColor{
    background-color: #33CC33;
}
.redColor{
    background-color: #E60000;
}
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cautela</h2>
        <ol class="breadcrumb">
            <li>Cautela</li>
            <li class="active">
                <strong>Cadastro</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Medidores_cautela', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">                           
                            
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <input type="hidden" value="<?=  $filtroCautela; ?>" name="tipo_cadastro">
                                <label class="required" for="data">Data <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="date" name="data" id="data" class="form-control" value="<?php echo date('Y-m-d') ?>"
                                    placeholder="Data" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="id_equipe">Equipe/Colaborador</label>
                                <select name="id_equipe" class="form-control selectPicker" id="id_equipe">
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Medidores_equipes as $m) {
                                        if ($m->id == $Medidores_cautela->id_equipe)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="colaborador">Colaborador </label>
                                <input type="text" name="colaborador" id="colaborador" class="form-control">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="id_tipo_movimento_cautela">Tipo de Movimento</label>
                                <select name="id_tipo_movimento_cautela" class="form-control" id="id_tipo_movimento_cautela">
                                    <?php
                                    foreach ($Medidores_tipo_movimentos as $m) {
                                        if ($m->id == $Medidores_cautela->id_tipo_movimento_cautela)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php if($filtroCautela == '4C' ):?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="num_medidor">Num. Medidor</label>
                                <textarea name="num_medidor" id="num_medidor" class="form-control"></textarea>
                            </div> <?php endif; ?>

                            <?php if($filtroCautela == '1C' || $filtroCautela == '2C'):?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="num_lacre">
                                    <?php echo $nomeLacre; ?></label>
                                <textarea name="num_lacre" id="num_lacre" class="form-control"></textarea>
                            </div> <?php endif; ?>
                            <?php if($filtroCautela == '1C'):?>                            
                            <div class="form-group col-md-2 col-sm-6 col-xs-12">
                                <label for="medidores_cor_lacre_id">Cor Lacre</label>            
                                <select name="medidores_cor_lacre_id" id="medidores_cor_lacre_id" name="medidores_cor_lacre_id" class="form-control">     
                                    <option value="Preto" style="color: #1C1C1C;">Preto</option>                               
                                    <option value="Vermelho" style="color: #FF0000;">Vermelho</option>
                                    <option value="Rosa" style="color: #FF00FF;">Rosa</option>
                                   </select>       
                            </div>
                            
                            <?php endif; ?>
                            <?php if($filtroCautela == '2C'):?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="medidores_envolucro_id">Tipo Envolucro</label>
                                <select name="medidores_envolucro_id" class="form-control" id="medidores_envolucro_id">
                                    <?php
                                        foreach ($Medidores_envolucro as $m) {
                                            if ($m->id == $Medidores_cautela->medidores_envolucro_id)
                                                echo '<option selected value="' . $m->id . '">' . $m->nome . '</option>';
                                            else
                                                echo '<option value="' . $m->id . '">' . $m->nome . '</option>';
                                        }
                                        ?>
                                </select>
                                <br>
                            </div>
                            <?php endif; ?>
                            <?php if($filtroCautela == '3C'):?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="num_equipamento">Equipamento/outros</label>
                                <textarea name="num_equipamento" id="num_equipamento" class="form-control"></textarea>
                            </div>
                            <?php endif; ?>




                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_cautela', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>