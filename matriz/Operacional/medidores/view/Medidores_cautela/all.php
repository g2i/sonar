
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Cautela</h2>
    <ol class="breadcrumb">
    <li>Cautela</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                <div class="col-md-3 form-group">
                    <label for="id_equipe">Equipe/Colaborador</label>
                    <select name="filtro[externo][id_equipe]" class="form-control selectPicker" id="id_equipe">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Medidores_equipes as $m): ?>
                            <?php echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="id_tipo_movimento_cautela">Tipo de Movimento</label>
                    <select name="filtro[externo][id_tipo_movimento_cautela]" class="form-control selectPicker" id="id_tipo_movimento_cautela">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Medidores_tipo_movimentos as $m): ?>
                            <?php echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="num_lacre">Num. Lacre</label>
                    <select name="filtro[interno][num_lacre]" class="form-control selectPicker" id="num_lacre">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Cadastro_lacres as $c): ?>
                            <?php echo '<option value="' . $c->num_lacre . '">' . $c->num_lacre . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-md-3 form-group">
                    <label for="num_medidor">Num. Medidor</label>
                    <select name="filtro[interno][num_medidor]" class="form-control selectPicker" id="num_medidor">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Cadastro_medidores as $c): ?>
                            <?php echo '<option value="' . $c->num_medidor . '">' . $c->num_medidor . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-md-3 form-group">
                    <label for="num_equipamento">Num. Equipamento</label>
                    <select name="filtro[interno][num_equipamento]" class="form-control selectPicker" id="num_equipamento">
                        <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Cadastro_equipamentos as $c): ?>
                            <?php echo '<option value="' . $c->num_equipamento . '">' . $c->num_equipamento . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-md-3 form-group">
                    <label for="data_inicial">Inicio</label>
                    <input type="date" name="inicio" id="inicio"
                           class="form-control maskData" value="">
                </div>

                <div class="col-md-3 form-group">
                    <label for="data_final">Fim</label>
                    <input type="date" name="fim" id="fim"
                           class="form-control maskData" value="">
                </div>

                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Devolução', 'Medidores_cautela', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id')); ?>'>
                        Código
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id_equipe')); ?>'>
                        Equipe/Colaborador
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'data')); ?>'>
                        Data
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id_tipo_movimento_cautela')); ?>'>
                        Tipo de Movimento
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_lacre')); ?>'>
                        Num. Lacre
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_medidor')); ?>'>
                        Num. Medidor
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_equipamento')); ?>'>
                        Num. Equipamento
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'codigo_cautela')); ?>'>
                        Num. Processamento
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Medidores_cautelas as $m) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($m->id, 'Medidores_cautela', 'view',
                    array('id' => $m->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($m->getMedidores_equipe()->descricao, 'Medidores_cautela', 'view',
                    array('id' => $m->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink(DataBR($m->data), 'Medidores_equipe', 'view',
                    array('id' => $m->getMedidores_equipe()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($m->getMedidores_tipo_movimento()->descricao, 'Medidores_tipo_movimento', 'view',
                    array('id' => $m->getMedidores_tipo_movimento()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink($m->num_lacre, 'Cadastro_lacre', 'view',
                    array('id' => $m->getCadastro_lacre()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink($m->num_medidor, 'Cadastro_medidor', 'view',
                    array('id' => $m->getCadastro_medidor()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($m->num_equipamento, 'Cadastro_equipamento', 'view',
                    array('id' => $m->getCadastro_equipamento()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($m->codigo_cautela, 'Cadastro_equipamento', 'view',
                    array('id' => $m->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Medidores_cautela', 'edit', 
                    array('id' => $m->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Medidores_cautela', 'delete', 
                    array('id' => $m->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-print"></span> ', 'Medidores_cautela', 'imprimir',
                    array('id' => $m->codigo_cautela),
                    array('class' => 'btn btn-success btn-sm', 'target' => '_blank'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Medidores_cautela', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>



