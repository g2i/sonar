<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cautela</h2>
        <ol class="breadcrumb">
            <li>Cautela</li>
            <li class="active">
                <strong>Editar </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Medidores_cautela', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">

                            <div class="form-group col-md-4">
                                <label class="required" for="data">Data <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="date" name="data" id="data" class="form-control" value="<?php echo $Medidores_cautela->data ?>"
                                    placeholder="Data" required>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="id_equipe">Equipe/Colaboradores</label>
                                <select name="id_equipe" class="form-control" id="id_equipe">
                                    <?php
                                    foreach ($Medidores_equipes as $m) {
                                        if ($m->id == $Medidores_cautela->id_equipe)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="id_tipo_movimento_cautela">Tipo de Movimento</label>
                                <select name="id_tipo_movimento_cautela" class="form-control" id="id_tipo_movimento_cautela">
                                    <?php
                                    foreach ($Medidores_tipo_movimentos as $m) {
                                        if ($m->id == $Medidores_cautela->id_tipo_movimento_cautela)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php if ($lacre): ?>
                            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <label for="id_lacre">Número Lacre</label>
                                <select name="id_lacre" class="form-control" id="id_lacre" disabled>
                                    <?php
                                        foreach ($Cadastro_lacres as $c) {
                                            if ($c->num_lacre == $Medidores_cautela->num_lacre)
                                                echo '<option selected value="' . $c->num_lacre . '">' . $c->num_lacre . '</option>';
                                            else
                                                echo '<option value="' . $c->num_lacre . '">' . $c->num_lacre . '</option>';
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <label>Cor Lacre</label>
                                <input type='color' name='cor_lacre' id="cor_lacre" class="form-control" value="<?php echo $Medidores_cautela->cor_lacre ?>" />

                            </div>
                            <?php endif; ?>

                            <?php if ($medidor): ?>
                            <div class="form-group col-md-4">
                                <label for="id_medidor">Número Medidor</label>
                                <select name="id_medidor" class="form-control" id="id_medidor" disabled>
                                    <?php
                                        foreach ($Cadastro_medidores as $c) {
                                            if ($c->num_medidor == $Medidores_cautela->num_medidor)
                                                echo '<option selected value="' . $c->num_medidor . '">' . $c->num_medidor . '</option>';
                                            else
                                                echo '<option value="' . $c->num_medidor . '">' . $c->num_medidor . '</option>';
                                        }
                                        ?>
                                </select>
                            </div>
                            <?php endif; ?>
                            <div class="clearfix"></div>
                        </div>

                        <input type="hidden" name="id" value="<?php echo $Medidores_cautela->id; ?>">
                        <input type="hidden" name="action" value="<?php echo $Action ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_cautela', $Action) ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>