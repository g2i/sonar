<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cautela</h2>
        <ol class="breadcrumb">
            <li>Cautela</li>
            <li class="active">
                <strong>Imprimir Cautela</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="">
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id')); ?>'>
                                            Código
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id_equipe')); ?>'>
                                            Equipe/Colaborador
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'data')); ?>'>
                                            Data
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id_tipo_movimento_cautela')); ?>'>
                                            Tipo de Movimento
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_lacre')); ?>'>
                                            Num. Lacre
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_medidor')); ?>'>
                                            Num. Medidor
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_equipamento')); ?>'>
                                            Num. Equipamento
                                        </a>
                                    </th>
                                </tr>
                                <?php
                                foreach ($Medidores_cautelas as $m) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->id, 'Medidores_cautela', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_equipe()->descricao, 'Medidores_cautela', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($m->data), 'Medidores_equipe', 'view',
                                        array('id' => $m->getMedidores_equipe()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_tipo_movimento()->descricao, 'Medidores_tipo_movimento', 'view',
                                        array('id' => $m->getMedidores_tipo_movimento()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($m->num_lacre, 'Cadastro_lacre', 'view',
                                        array('id' => $m->getCadastro_lacre()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($m->num_medidor, 'Cadastro_medidor', 'view',
                                        array('id' => $m->getCadastro_medidor()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($m->num_equipamento, 'Cadastro_equipamento', 'view',
                                        array('id' => $m->getCadastro_equipamento()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>
                            <input type="hidden" id="codigo_cautela" name="codigo_cautela" value="<?= $codigo_cautela; ?>">
                            <div class="text-right">
                                <button id="btnTermoCautela" data-role="tooltip" data-placement="bottom"
                                        type="button" class="btn btn-success">
                                    <span class="fa fa-print"></span> Imprimir Cautela
                                </button>
                            </div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {

                        });

                        $('#btnTermoCautela').click(function(){

                            var data ={
                                codigo_cautela: $("#codigo_cautela").val()
                            }

                            $.Report('termo_cautela', data);

                        });

                    </script>
                </div>
            </div>
        </div>
    </div>
</div>