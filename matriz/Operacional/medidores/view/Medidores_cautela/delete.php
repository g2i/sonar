<form class="form" method="post" action="<?php echo $this->Html->getUrl('Medidores_cautela', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Medidores_cautela->id; ?></strong>?</p>
    </div>
    <div class="text-right">
    <input type="hidden"  value="<?= $filtroCautela ?>" name="tipo_cadastro">
        <input type="hidden" name="id" value="<?php echo $Medidores_cautela->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Medidores_cautela', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>