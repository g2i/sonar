<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2><?php echo $tituloPagina; ?></h2>
        <ol class="breadcrumb">
            <li><?php echo $tituloPagina; ?></li>
            <li class="active">
                <strong>Lista</strong>
            </li></ol></div></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="id_equipe">Equipe/Colaborador</label>
                                    <select name="filtro[externo][id_equipe]" class="form-control selectPicker" id="id_equipe">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Medidores_equipes as $m): ?>
                                            <?php echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                  
                                </div>
                                <!---usado para fazer a consulta pelo tipo de menu--> 
                                <input type="hidden" value="<?php echo $filtroCautela ?>" name="filtro[externo][tipo_cadastro]" class="form-control" id="tipo_cadastro">
                      
                                <!-- Retirado pois só lista tipos saidas
                                <div class="col-md-3 form-group">
                                    <label for="id_tipo_movimento_cautela">Tipo de Movimento</label>
                                    <select name="filtro[externo][id_tipo_movimento_cautela]" class="form-control selectPicker" id="id_tipo_movimento_cautela">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Medidores_tipo_movimentos as $m): ?>
                                            <?php echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>-->
                                <?php if($filtroCautela == '1C' || $filtroCautela == '2C' ): ?>
                                <div class="col-md-3 form-group">
                                    <label for="num_lacre"> <?php echo $nomeLacre; ?></label>
                                    <select name="filtro[interno][num_lacre]" class="form-control selectPicker" id="num_lacre">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Cadastro_lacres as $c): ?>
                                            <?php echo '<option value="' . $c->num_lacre . '">' . $c->num_lacre . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <!--IF serve para montar a tela conforme o btn escolhido no menu-->
                                <?php if($filtroCautela == '4C'): ?>
                                <div class="col-md-3 form-group">
                                    <label for="num_medidor">Num. Medidor</label>
                                    <select name="filtro[interno][num_medidor]" class="form-control selectPicker" id="num_medidor">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Medidores_cautelas as $c): ?>
                                            <?php echo '<option value="' . $c->num_medidor . '">' . $c->num_medidor . '</option>';  ?>
                                        <?php endforeach; ?>
                                     
                                    </select>
                                </div> <?php endif; ?>
                                <?php if($filtroCautela == '3C'): ?>
                                <div class="col-md-3 form-group">
                                    <label for="num_equipamento">Docs/Outros</label>
                                    <select name="filtro[interno][num_equipamento]" class="form-control selectPicker" id="num_equipamento">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Cadastro_equipamentos as $c): ?>
                                            <?php echo '<option value="' . $c->num_equipamento . '">' . $c->num_equipamento . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <?php endif; ?>
                                <div class="col-md-3 form-group">
                                    <label for="data_inicial">Inicio</label>
                                    <input type="date" name="inicio" id="inicio"
                                           class="form-control maskData" value="">
                                </div>

                                <div class="col-md-3 form-group">
                                    <label for="data_final">Fim</label>
                                    <input type="date" name="fim" id="fim"
                                           class="form-control maskData" value="">
                                </div>
                                <!-- Retirado a pedido do cliente
                                <div class="col-md-3 form-group">
                                    <label for="medidores_subgrupo_id">SubCategoria</label>
                                    <select name="filtro[interno][medidores_subgrupo_id]" class="form-control selectPicker" id="medidores_subgrupo_id">
                                    <option value="">Selecione:</option>
                                        <?php
                                        foreach ($Medidores_subgrupo as $m) {
                                            if ($m->id == $Cadastro_lacre->medidores_subgrupo_id)
                                                echo '<option selected value="' . $m->id . '">' . $m->nome . '</option>';
                                            else
                                                echo '<option value="' . $m->id . '">' . $m->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                -->
                            <?php if($filtroCautela == '4C'): ?>
                                <div class="col-md-3 form-group">
                                    <label for="dt_execucao">Cautelas</label>
                                    <select name="dt_execucao" class="form-control" id="dt_execucao">
                                    <option value="">Selecione:</option>
                                    <option value="1">Cautelas Executadas </option>
                                    <option value="2">Cautelas Não Executadas </option>  
                                    </select>
                                </div>     
                            <?php endif;   ?>                  
                                <div class="col-md-12 text-right">
                                  
                                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Cautela ', 'Medidores_cautela', 'add', 
                        array('tipo_cadastro' => $filtroCautela),
                        array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id')); ?>'>
                                            Código
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id_equipe')); ?>'>
                                            Equipe/Colab
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'data')); ?>'>
                                            Colaborador
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'data')); ?>'>
                                            Data
                                        </a>
                                    </th>
                                    <!--<th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'id_tipo_movimento_cautela')); ?>'>
                                            Tipo Movimento
                                        </a>
                                    </th>-->
                                    <?php if($filtroCautela == '1C' || $filtroCautela == '2C'): ?>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_lacre')); ?>'>
                                            <?php echo $nomeLacre; ?>
                                            </a>
                                        </th>                                       
                                    <?php endif; ?>
                                    <?php if($filtroCautela == '1C'): ?>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_lacre')); ?>'>
                                                Cor Lacre
                                            </a>
                                        </th>   
                                    <?php endif; ?>    
                                    <?php if($filtroCautela == '2C'): ?>     
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_lacre')); ?>'>
                                            Tipo Envolucro
                                            </a>
                                        </th>                            
                                    <?php endif; ?>
                                    <?php if($filtroCautela == '4C'): ?>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_medidor')); ?>'>
                                        N°.Medidor
                                        </a>
                                    </th>
                                    <?php endif; ?>
                                    <?php if($filtroCautela == '3C'): ?>
                                        <th>
                                            <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'num_equipamento')); ?>'>
                                            N°.Equipamento
                                            </a>
                                        </th>
                                    <?php endif; ?>
                                    <!-- <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'codigo_cautela')); ?>'>
                                        N°.Processamento
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'codigo_cautela')); ?>'>
                                        SubCategoria
                                        </a>
                                    </th>-->
                                    <?php if($filtroCautela == '4C'): ?>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'codigo_cautela')); ?>'>
                                        Nota aplicada
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => 'codigo_cautela')); ?>'>
                                            Dt.Execução
                                        </a>
                                    </th>
                                    <?php endif; ?>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php                            
                                foreach ($Medidores_cautelas as $m) { 
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $m->id;
                                    echo '</td>';
                                    echo '<td>'; ?>
                                        <a href="#" class="edit_local" data-controller="Medidores_cautela" data-type="select" data-name="id_equipe" data-pk="<?= $m->id ?>" data-value="<?= $m->id_equipe ?>" data-source="<?= $Medidores_equipes_source ?>" data-original-title="Select group">
                                            <?php $m->getMedidores_equipe()->descricao; ?>
                                        </a>
                                    <?php echo '</td>';
                                    echo '<td>';                            
                                        echo '<a class="edit_local" data-controller="Medidores_cautela" 
                                        data-param="text" data-title="colaborador" data-pk="'. $m->id .'" data-value="'. $m->colaborador .'" 
                                        listen="f">';
                                        echo $m->colaborador;
                                        echo '</a>';
                                    echo '</td>';
                                    echo '<td>';
                                    echo DataBR($m->data);
                                        
                                    echo '</td>';
                                    /*echo '<td>';
                                    echo $this->Html->getLink($m->getMedidores_tipo_movimento()->descricao, 'Medidores_tipo_movimento', 'view',
                                        array('id' => $m->getMedidores_tipo_movimento()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';*/
                                    if($filtroCautela == '1C' || $filtroCautela == '2C'){
                                        echo '<td>';
                                            echo '<a class="edit_local" data-controller="Medidores_cautela" 
                                            data-param="text" data-title="num_lacre" data-pk="'. $m->id .'" data-value="'. $m->num_lacre .'" 
                                            listen="f">';
                                            echo $m->num_lacre;
                                            echo '</a>';
                                        echo '</td>';
                                    }
                                    if($filtroCautela == '1C'){
                                        echo '<td>'; 
                                        echo '<a class="edit_local" data-controller="Medidores_cautela" 
                                        data-param="text" data-title="medidores_cor_lacre_id" data-pk="'. $m->id .'" data-value="'. $m->medidores_cor_lacre_id .'" 
                                        listen="f">';
                                        echo $m->medidores_cor_lacre_id;
                                      
                                     echo '</td>';
                                    }elseif($filtroCautela == '2C'){
                                        echo '<td>';?>
                                        <a href="#" class="edit_local" data-controller="Medidores_cautela" data-type="select" 
                                        data-name="medidores_envolucro_id" data-pk="<?= $m->id ?>" 
                                        data-value="<?= $m->medidores_envolucro_id ?>" 
                                        data-source="<?= $medioresDataSourceEnvolucro ?>" data-original-title="Select group">
                                        <?php $m->getMedidores_envolucro()->nome;
                                        echo '</td>';   
                                    } elseif($filtroCautela == '4C'){
                                    echo '<td>';
                                        echo '<a class="edit_local" data-controller="Medidores_cautela" 
                                        data-param="text" data-title="num_medidor" data-pk="'. $m->id .'" data-value="'. $m->num_medidor .'" 
                                        listen="f">';
                                        echo $m->num_medidor;
                                        echo '</a>';
                                    echo '</td>';
                                    }
                                    if($filtroCautela == '3C'){
                                        echo '<td>';
                                            echo '<a class="edit_local" data-controller="Medidores_cautela" 
                                            data-param="text" data-title="num_equipamento" data-pk="'. $m->id .'" data-value="'. $m->num_equipamento .'" 
                                            listen="f">';
                                            echo $m->num_equipamento;
                                            echo '</a>';
                                        echo '</td>';
                                    }
                                   /* echo '<td>';
                                    echo $this->Html->getLink($m->codigo_cautela, 'Cadastro_equipamento', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';*/

                                    //echo '<td>';?>

                                        <!--<a href="#" class="edit_local" data-controller="Medidores_cautela" data-type="select" data-name="medidores_subgrupo_id" data-pk="<?= $m->id ?>" data-value="<?= $m->medidores_subgrupo_id ?>" data-source="<?= $Medidores_subgrupos ?>" data-original-title="Select group">
                                        <?php $m->getSubGrupo()->nome;?>
                                        </a>-->
                                        
                                  <?php //echo '</td>';
                                    /*ABRE MODELO DE EDITAR A TABELA NO LOCAL
                                    É IMPORTANTE VERIFICAR O TIPO DA VARIAVEL 
                                    */
                                   if($filtroCautela == '4C'){
                                        echo '<td>';
                                        echo '<a class="edit_local" data-controller="Medidores_cautela" 
                                            data-param="text" data-title="nota_aplicada" data-pk="'. $m->id .'" data-value="'. $m->nota_aplicada .'" 
                                            listen="f">';
                                            echo $m->nota_aplicada;
                                        echo '</a>';
                                        echo '</td>'; 
                                        
                                        echo '<td>';
                                        echo '<a class="edit_local" data-controller="Medidores_cautela" 
                                            data-param="data" data-title="dt_execucao" data-pk="'. $m->id .'" data-value="'. $m->dt_execucao .'" 
                                            listen="f">';
                                            echo DataBR($m->dt_execucao);
                                        echo '</a>';
                                        echo '</td>'; 
                                   }
                                    echo '<td>';
                                    echo $this->Html->getLink($m->valor, 'Cadastro_equipamento', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        
                                    echo '</td>';   
                                    echo '<td>';
                                    echo $this->Html->getLink($m->valor, 'Cadastro_equipamento', 'view',
                                        array('id' => $m->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';                                  
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-print"></span> ', 'Medidores_cautela', 'imprimir',
                                        array('id' => $m->codigo_cautela),
                                        array('class' => 'btn btn-success btn-sm', 'target' => '_blank'));
                                    echo '</td>';
                                      echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Medidores_cautela', 'delete',
                                        array('id' => $m->id, 'tipo_cadastro' => $filtroCautela),
                                        array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                            
                                ?>
                            </table>
                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function() {
                            $('#search').keyup(function() {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Medidores_cautela', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Medidores_cautela', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function() {
                                            r = true;
                                        });
                                }
                            });
                        
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>