<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cautela</h2>
        <ol class="breadcrumb">
            <li>Cautela</li>
            <li class="active">
                <strong>Cadastro</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Medidores_cautela', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="data">Data <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="date" name="data" id="data" class="form-control"
                                       value="<?php echo date('Y-m-d') ?>" placeholder="Data" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="id_equipe">Equipe/Colaborador</label>
                                <select name="id_equipe" class="form-control selectPicker" id="id_equipe">
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Medidores_equipes as $m) {
                                        if ($m->id == $Medidores_cautela->id_equipe)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="id_tipo_movimento_cautela">Tipo de Movimento</label>
                                <select name="id_tipo_movimento_cautela" class="form-control"
                                        id="id_tipo_movimento_cautela">
                                    <?php
                                    foreach ($Medidores_tipo_movimentos as $m) {
                                        if ($m->id == $Medidores_cautela->id_tipo_movimento_cautela)
                                            echo '<option selected value="' . $m->id . '">' . $m->descricao . '</option>';
                                        else
                                            echo '<option value="' . $m->id . '">' . $m->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="num_medidor">Num. Medidor</label>
            <textarea name="num_medidor" id="num_medidor"
                      class="form-control"></textarea>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="num_lacre">Num. Lacre</label>
            <textarea name="num_lacre" id="num_lacre"
                      class="form-control"></textarea>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="num_equipamento">Num. Equipamento</label>
            <textarea name="num_equipamento" id="num_equipamento"
                      class="form-control"></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Medidores_cautela', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
