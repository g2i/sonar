<?php
/**
 * Created by PhpStorm.
 * User: Desenvolvimento
 * Date: 14/06/2016
 * Time: 10:45
 */

function convertDataSQL4BR($data) {
    if ($data != "") {
        list($y, $m, $d) = preg_split('/-/', $data);
        return sprintf('%02d/%02d/%04d', $d, $m, $y);
    }
}

?>