<li class="nav-header">
    <div class="dropdown profile-element">
    <span>
    <?php echo(file_exists(SITE_PATH . "/content/img/" . sprintf("%06d", @Session::get("user")->id) . ".png") ? '<img class="img-circle" src="' . SITE_PATH . '/content/img/' . sprintf("%06d", @Session::get("user")->id) . '.png" alt="Foto"/>' : '<img class="img-circle" src="' . SITE_PATH . '/content/img/empty-avatar.png" alt="Foto"/>') ?>
    </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
    <span class="clear"> <span class="block m-t-xs small"> <strong
                class="font-bold"><?php echo @Session::get("user")->nome .'<br>'. @Session::get("localidade")->cidade . ' - ' . @Session::get("localidade")->estado?></strong>
    </span> <!--<span class="text-muted text-xs block">Opções <b class="caret"></b></span> --></span> </a>
        <!--<ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><?php /*echo $this->Html->getLink('Sair', 'Login', 'logout'); */?></li>
        </ul>-->
    </div>
    <div class="logo-element">
        G2i
    </div>
</li>
<!--<li <?php echo(CONTROLLER == "Medidores_medidor" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-exchange"></i><span class="nav-label">Movimentação</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Medidores_medidor" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Listar', 'Medidores_medidor', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER ==    "Medidores_medidor" && ACTION == "add" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Medidores_medidor', 'add'); ?>
        </li>
    </ul>
</li>-->
<!--
    no sub menu da cautela foi add um id para ser usado na view, que sera montada conforme o id do menu
-->

<li <?php echo(CONTROLLER == "Medidores_cautela" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-exchange"></i><span class="nav-label">Cautela</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
    <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Medidores', 'Medidores_cautela', 'all_saidas',
         array('tipo_cadastro' => '4C')); ?>
    </li>
    <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
        <?php echo $this->Html->getLink('<i class="fa fa-link"></i> Lacres', 'Medidores_cautela', 'all_saidas',
         array('tipo_cadastro' => '1C')); ?>
    </li> 
    <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
        <?php echo $this->Html->getLink('<i class="fa fa-magic"></i> Envolucros', 'Medidores_cautela', 'all_saidas',
         array('tipo_cadastro' => '2C')); ?>
    </li>
    <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
        <?php echo $this->Html->getLink('<i class="fa fa-files-o"></i> Docs/Outros', 'Medidores_cautela', 'all_saidas',
         array('tipo_cadastro' => '3C')); ?>
    </li>
    <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-files-o"></i> Artigos(Estoque)', '#', '#',
            array('tipo_cadastro' => '5C')); ?>
    </li>
 </ul>
</li>

<li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-magic"></i> Devoluções/Usados', 'Medidores_cautela', 'all_entradas'); ?>
</li>

<li <?php echo(CONTROLLER == "Cadastro_medidor" || CONTROLLER == "Cadastro_lacre" || CONTROLLER == "Cadastro_equipamento") ;?>>
    <a href="#"><i class="fa fa-clock-o"></i><span class="nav-label">Entrada</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Cadastro_medidor" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Medidor', 'Cadastro_medidor', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
        <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Medidores', 'Medidores_cautela', 'all_entradas',
         array('tipo_cadastro' => '4C')); ?>
        </li>
        <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-link"></i> Lacres', 'Medidores_cautela', 'all_entradas',
            array('tipo_cadastro' => '1C')); ?>
        </li> 
        <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-magic"></i> Envolucros', 'Medidores_cautela', 'all_entradas',
            array('tipo_cadastro' => '2C')); ?>
        </li>
        <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-files-o"></i> Docs/Outros', 'Medidores_cautela', 'all_entradas',
            array('tipo_cadastro' => '3C')); ?>
        </li>
        <li <?php echo(CONTROLLER == "Medidores_cautela" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-files-o"></i> Artigos(Estoque)', '#', '#',
            array('tipo_cadastro' => '5C')); ?>
        </li>
       <!-- <li <?php echo(CONTROLLER == "Cadastro_lacre" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Lacre', 'Cadastro_lacre', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Cadastro_equipamento" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Equipamentos e Outros', 'Cadastro_equipamento', 'all'); ?>
        </li>-->
    </ul>
</li>
<!--<li <?php echo(CONTROLLER == "Cadastro_medidor" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-clock-o"></i><span class="nav-label">Medidor</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Cadastro_medidor" && ACTION == "add" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Cadastro_medidor', 'add'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Cadastro_medidor" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Listar', 'Cadastro_medidor', 'all'); ?>
        </li>
    </ul>
</li>
<li <?php echo(CONTROLLER == "Cadastro_lacre" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-lock"></i><span class="nav-label">Lacre</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Cadastro_lacre" && ACTION == "add" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Cadastro_lacre', 'add'); ?>
        </li
        <li <?php echo(CONTROLLER == "Cadastro_lacre" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Listar', 'Cadastro_lacre', 'all'); ?>
        </li>
    </ul>
</li>
<li <?php echo(CONTROLLER == "Cadastro_equipamento" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-wrench"></i><span class="nav-label">Equipamento</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Cadastro_equipamento" && ACTION == "add" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Cadastro_equipamento', 'add'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Cadastro_equipamento" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Listar', 'Cadastro_equipamento', 'all'); ?>
        </li>
    </ul>
</li>-->