
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Dss </h2>
    <ol class="breadcrumb">
    <li>Dss </li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Dss_curso', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'tema')); ?>'>
                        Tema
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'palestrante')); ?>'>
                        palestrante
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'data_palestra')); ?>'>
                        Data
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'localidade')); ?>'>
                        Localidade
                    </a>
                </th>            
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'divisao_projeto')); ?>'>
                        Divisao de projeto
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'descricao')); ?>'>
                        Descricao
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => 'observacao')); ?>'>
                        Observação
                    </a>
                </th>           
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Dss_cursos as $d) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($d->id, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($d->tema, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($d->palestrante, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($d->data_palestra, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($d->localidade, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';             
                echo '<td>';
                echo $this->Html->getLink($d->divisao_projeto, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
            
                echo '<td>';
                echo $this->Html->getLink($d->descricao, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($d->observacao, 'Dss_curso', 'view',
                    array('id' => $d->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td class="actions text-right">';
                echo '<div class="dropdown">';
                   echo '<button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                    <span class="caret"></span></button>';       
                    echo '<ul class="dropdown-menu">';
                       
                        echo '<li>';
                            echo $this->Html->getLink('<span class="fa fa-paperclip" title="Anexos">  Anexos</span> ', 'Anexo', 'all',
                            array('id' => $d->id,'first' => 1,'modal' => 1),
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</li>';
                        echo '<li>';
                            echo $this->Html->getLink('<span class="fa fa-print"> Imprimir</span> ', 'Rhrelatorio', 'dss_cursos', 
                            array('id' => $d->id));
                        echo '</li>';
                        echo '<li>';
                            echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"> Editar</span> ', 'Dss_curso', 'edit',
                            array('id' => $d->id),
                            array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                        echo '</li>';
                        echo '<li>';
                            echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"> Deletar</span> ', 'Dss_curso', 'delete', 
                            array('id' => $d->id), 
                            array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                        echo '</li>';
                             echo '</ul>';
                echo '</div>';
                echo '</td>';
                
                echo '<td width="50">';
               
                echo '</td>';
                echo '<td width="50">';
               
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Dss_curso', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Dss_curso', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>