<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <div class="right">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhocorrencia', 'renovar') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-sm-6">
                                <label for="data" class="required">Data<span class="glyphicon glyphicon-asterisk"></span>
                                </label>
                                <input type="date" name="data" id="data" class="form-control" value="<?php echo $Rhocorrencia->data ?>"
                                    placeholder="Data" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="historico" class="required">Histórico<span class="glyphicon glyphicon-asterisk"></span>
                                </label>
                                <select name="historico" class="form-control" id="historico" required>
                                    <?php
                                foreach ($Rhocorrencia_historicos as $r): 
                                    if ($r->codigo == $Rhocorrencia->historico): ?>
                                    <option selected value="<?= $r->codigo; ?>" data-p="<?= $r->periodicidade;?>">
                                        <?= $r->nome; ?>
                                    </option>;
                                    <?php else: ?>
                                    <option value="<?= $r->codigo; ?>" data-p="<?= $r->periodicidade; ?>">
                                        <?= $r->nome; ?>
                                    </option>
                                    <?php endif;endforeach; ?>
                                </select>                             
                            </div>
                            <!-- pega o data-p=" $r->periodicidade -->
                            <?php  foreach ($Rhocorrencia_historicos as $r): 
                                    if ($r->codigo == $Rhocorrencia->historico): ?>
                                        <input type="hidden" id="tipoPeriodicidade" name="tipoPeriodicidade" value="<?= $r->periodicidade; ?>">
  
                            <?php endif; endforeach; ?>
                            <input type="hidden" name="codigo" value="<?php echo $Rhocorrencia->codigo;?>">
                            <div class="form-group col-xs-12">
                                <label for="descricao">Descrição</label>
                                <textarea name="descricao" id="descricao" class="form-control" rows="3"><?php echo $Rhocorrencia->descricao ?></textarea>
                            </div>
                            <!-- passa o parametro da id do profissional -->
                            <?php if($this->getParam('modal')){ ?>
                            <input type="hidden" name="codigo_profissional" value="<?php echo $this->getParam('profissional'); ?>" />
                            <?php }else{ ?>
                            <!-- / passa o parametro da id do profissional -->

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="codigo_profissional">Profissional</label>
                                <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                                <?php
                                foreach ($Rhprofissionais as $r) {
                                    if ($r->codigo == $Rhocorrencia->codigo_profissional)
                                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                                    else
                                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                                }
                                ?>
                                </select>
                            </div>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                        <?php if($this->getParam('modal')){ ?>
                        <input type="hidden" name="modal" value="1" />
                        <div class="text-right">
                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                                Cancelar
                            </a>
                            <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="salvar">
                        </div>
                        <?php }else{ ?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Rhocorrencia', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                        <script>
                            $(document).ready(function () {
                                $('#codigo_profissional option[value="<?php echo $Rhocorrencia->codigo ?>"]').prop('selected', true);
                            });
                        </script>
                        <?php } ?>
                        <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                    </form>
                </div><!-- /ibox content -->
            </div><!-- / ibox -->
        </div>
        <!--wrapper-->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->
<script>
    $(document).ready(function () {
        $('#historico option[value="<?php echo $Rhocorrencia->getRhocorrencia_historico()->codigo ?>"]').prop('selected', true);
        $('#status option[value="<?php echo $Rhocorrencia->status ?>"]').prop('selected', true);

        $('#historico').change(function () {
            if ($(this).find(':selected').attr('data-p') == "Data Especifica") {
                $('#data_especifica').show();
            } else {
                $('#data_especifica').hide();
                var dataEsp = $(this).find(':selected').attr('data-p');
                $("input[name='tipoPeriodicidade']").val(dataEsp);
            }
        });
    });
</script>