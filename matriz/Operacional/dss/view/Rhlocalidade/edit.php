<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">

            <div class="col-lg-9">
                <h2>Localidade</h2>
                <ol class="breadcrumb">
                    <li>Localidade</li>
                    <li class="active">
                        <strong>Editar Localidade</strong>
                    </li>
                </ol>
                <br>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Rhlocalidade', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="cidade">Cidade</label>
                                <input type="text" name="cidade" id="cidade" class="form-control"
                                       value="<?php echo $Rhlocalidade->cidade ?>" placeholder="Cidade">
                            </div>
                            <div class="form-group">
                                <label for="estado">Estado</label>
                                <input type="text" name="estado" id="estado" class="form-control"
                                       value="<?php echo $Rhlocalidade->estado ?>" placeholder="Estado">
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Rhlocalidade->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Rhlocalidade', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>