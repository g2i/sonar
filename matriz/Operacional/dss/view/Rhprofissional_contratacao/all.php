<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <!--  todos os if's e else's em php são necessários para navegação entre MODAIS -->
                    <?php if($this->getParam('modal')){ ?>
                    <div class="text-right" style="padding-bottom: 30px">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <?php }else{ ?>
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="tipo">Tipo</label>
                                    <input type="text" name="tipo" id="tipo" class="form-control" value="<?php echo $Rhprofissional_contratacao->tipo ?>"
                                        placeholder="Tipo">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tiporemuneracao">Tipo de Remuneração</label>
                                    <input type="text" name="tiporemuneracao" id="tiporemuneracao" class="form-control"
                                        value="<?php echo $Rhprofissional_contratacao->tiporemuneracao ?>" placeholder="Tipo de Remuneração">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="jornada">Jornada de Trabalho</label>
                                    <input type="text" name="filtro[interno][jornada]" id="jornada" class="form-control"
                                        value="<?php echo $this->getParam('jornada'); ?>">
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <?php } ?>

                    <div>
                        <!-- botão de cadastro dentro da modal -->
                        <?php if($this->getParam('modal')){ ?>
                        <div class="text-left col-md-6 ">
                            <h3 class="text-left">
                                <?php echo $Profissional->nome; ?>
                            </h3>
                        </div>
                        <div class="text-right col-md-6 ">
                        <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Rhprofissional_contratacao","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                            <span class="img img-add"></span> Nova Contratação
                            </a>
                        </p>
                        </div>

                        <?php }else{ ?>
                        <!-- / botão de cadastro dentro da modal  e senão for modal , rola esse botão definido a baixo-->

                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p>
                                <?php echo $this->Html->getLink('<span class="img img-add"></span> Nova Contratação', 'Rhprofissional_contratacao', 'add', NULL, array('class' => 'btn btn-primary')); ?>
                            </p>
                        </div>
                        <?php } ?>

                        <!-- tabela de resultados -->
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            <a>
                                                Tipo
                                            </a>
                                        </th>
                                        <th>
                                            <a>
                                                Data Inicial
                                            </a>
                                        </th>
                                        <th>
                                            <a>
                                                Data Final
                                            </a>
                                        </th>
                                        <th>
                                            <a>
                                                Função
                                            </a>
                                        </th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
            foreach ($Rhprofissional_contratacaos as $r) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($r->tipo, 'Rhprofissional_contratacao', 'view',
                    array('id' => $r->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink(DataBR($r->dtInicial), 'Rhprofissional_contratacao', 'view',
                    array('id' => $r ->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink(DataBR($r->dtFinal), 'Rhprofissional_contratacao', 'view',
                    array('id' => $r ->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->getRhprofissional_funcao()->nome, 'Rhprofissional_funcao', 'view',
                    array('id' => $r->getRhprofissional_funcao()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                if($this->getParam('modal')==1) {               
                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Hitórico de Salários" class="btn btn-sm" style="max-width:50px; max-height:50px;"
                        onclick="Navegar(\'' . $this->Html->getUrl("Rhprofissional_salario_historico", "all", array("ajax" => true, "modal" => "1",'id' => $r->codigo)).'\',\'go\')">
                            <span class="glyphicon glyphicon-log-out btn btn-info btn-sm"> </span></a>';
                    echo '</td>';

                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar Contratação" class="btn btn-sm" style="max-width:50px; max-height:50px;"
                        onclick="Navegar(\'' . $this->Html->getUrl("Rhprofissional_contratacao", "edit", array("ajax" => true, "modal" => "1", 'id' => $r->codigo,'profissional'=>$r->getRhprofissional()->codigo)).'\',\'go\')">
                            <span class="img img-edit"></span></a>';
                    echo '</td>';
                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Anexos" class="btn btn-sm" style="max-width:50px; max-height:50px;"
                        onclick="Navegar(\'' . $this->Html->getUrl("Anexocontratacao", "all", array("ajax" => true, "modal" => "1", 'id' => $r->codigo)).'\',\'go\')">
                        <span class="img img-anexo"></span></a>';
                    echo '</td>';
                    
                }else {
                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Rhprofissional_contratacao', 'edit',
                        array('id' => $r->codigo),
                        array('class' => 'btn btn-warning btn-sm'));
                    echo '</td>';
                }
                echo '</tr>';
            }
            ?>
                                </table>

                                <!-- menu de paginação -->
                                <div style="text-align:center">
                                    <?php echo $nav; ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- /ibox content -->
                </div><!-- / ibox -->
            </div>
            <!--/ wrapper -->
        </div> <!-- /col-lg 12 -->
    </div><!-- /row -->

<script>
    //toltip
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
    //fim toltip
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Rhprofissional_contratacao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Rhprofissional_contratacao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>