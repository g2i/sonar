<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">ativos</span>
                        <h5>Profissionais</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $prof_ativos->total; ?>
                        </h1>
                        <div class="stat-percent font-bold text-success"><i class="fa fa-bolt"></i></div>
                        <small>Profissionais</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-danger pull-right">vencidas</span>
                            <h5>Ocorrências</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                <?php echo $ocorencias_vencidas->totale; ?>
                            </h1>
                            <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                            <small>ocorrências</small>
                        </div>
                    </div>
                </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-info pull-right">últimos 30 dias</span>
                        <a href="<?php echo SITE_PATH . '/Index/demissao/ajax:1'; ?>" data-toggle="modal">
                            <h5>Demissões </h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $demissoes->total; ?>
                        </h1>

                        <div class="stat-percent font-bold text-info"><i class="fa fa-level-up"></i></div>
                        <small>
                            <?php echo ($demissoes->total > 1) ? "Profissionais" : "Profissional"; ?></small>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <a href="<?php echo SITE_PATH . '/Index/admissao/ajax:1'; ?>" data-toggle="modal">
                        <div class="ibox-title">
                            <span class="label label-primary pull-right">últimos 30 dias</span>
                            <h5>Admissões</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                <?php echo $admissao->total; ?>
                            </h1>
                            <div class="stat-percent font-bold text-navy"><i class="fa fa-level-up"></i></div>
                            <small>
                                <?php echo ($admissao->total > 1) ? "Profissionais" : "Profissional"; ?></small>
                        </div>
                </div>
            </div>
       
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <a href="<?php echo SITE_PATH . '/Index/prim_periodo/ajax:1'; ?>" data-toggle="modal">
                        <div class="ibox-title">
                            <h5>1º Período de Experiência Vencido</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                <?php echo $prim_periodo->total; ?>
                            </h1>
                            <div class="stat-percent font-bold text-danger"></div>
                            <small>experiência</small>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <a href="<?php echo SITE_PATH . '/Index/seg_periodo/ajax:1'; ?>" data-toggle="modal">

                        <div class="ibox-title">
                            <h5>2º Período de Experiência Vencido</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                <?php echo $seg_periodo->total; ?>
                            </h1>

                            <div class="stat-percent font-bold text-danger"></div>
                            <small>experiência</small>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Gráfico por Setor</h5>

                </div>
                <div class="ibox-content">
                    <div id="morris-donut-chart"></div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ocorrências</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Colaborador</th>
                                    <th>Ocorrência</th>
                                    <th>Última Data</th>
                                    <th>Dias</th>
                                    <th>Vencimento</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                            foreach ($ocor as $o) {

                                if (!empty(Rhocorrencia::getOcorrencia_individual($o->id_prof, $o->comp)) && strtotime($o->data_out) < strtotime(date('Y-m-d'))) {

                                } else {
                                    $clas = "";
                                    if (strtotime($o->vencimento) < strtotime(date('Y-m-d'))) {
                                        $clas = 'class="text-danger"';
                                    }
                                  
                                    echo '<tr ' . $clas . '>';
                                    echo '<td>' . $o->nome . '</td>';
                                    echo '<td>' . $o->descricao . '</td>';
                                    echo '<td>' . ConvertData($o->data) . '</td>';
                                    //vencimento
                                    echo '<td>' . diferenca_datas($o->data, $o->vencimento) . '</td>';
                                    echo '<td>' . ConvertData($o->vencimento) . '</td>';
                               
                                    echo '<td  style="padding: 2px;">';
                                    echo $this->Html->getLink('<span class="img img-edit" data-toggle="tooltip" data-placement="auto" title="Editar Histórico de Ocorrência"></span> ', 'Rhocorrencia', 'all',
                                        array('id' => $o->id_prof, 'modal' => 1, 'first' => 1, 'ajax' => true),
                                        array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
   
    $(function () {
        $.ajax({
            type: 'POST',
            url: root + '/Index/setor',
            success: function (txt) {
                Morris.Donut({
                    element: 'morris-donut-chart',
                    data: jQuery.parseJSON(txt),
                    resize: true,
                    colors: ['#87d6c6', '#54cdb4', '#1ab394', '#9de8d8', '#b7eee2']
                });
            }
        });


    });
</script>