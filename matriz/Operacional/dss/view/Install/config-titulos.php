<div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#configs-acordion"
         href="#collapseTitulos_<?php echo $t->name; ?>">
        <h4 class="panel-title">Configurações de Titulos</h4>
    </div>
    <div id="collapseTitulos_<?php echo $t->name; ?>" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="col-md-3">
                <label>Nome do módulo</label>
                <input disabled type="text" class="form-control"
                       name="modName_<?php echo $t->name; ?>"
                       value="<?php echo ucfirst($t->name); ?>"/>
            </div>

            <div class="col-md-3">
                <label>Nome no menu</label>
                <input type="text" class="form-control"
                       name="modTitle_menu_<?php echo $t->name; ?>"
                       value="<?php echo ucfirst($t->name); ?>"/>
            </div>

            <div class="col-md-3">
                <label>Botao de cadastro</label>
                <input type="text" class="form-control"
                       name="btn_add_<?php echo $t->name; ?>"
                       value="Novo registro"/>
            </div>
            <div class="clearfix"></div>
            <hr/>
            <div class="clearfix"></div>

            <div class="col-md-3">
                <label>Titulo da listagem</label>
                <input type="text" class="form-control"
                       name="modTitle_all_<?php echo $t->name; ?>"
                       value="<?php echo "Listagem de " . ucfirst($t->name); ?>"/>
            </div>

            <div class="col-md-3">
                <label>Titulo da cadastro</label>
                <input type="text" class="form-control"
                       name="modTitle_add_<?php echo $t->name; ?>"
                       value="<?php echo "Cadastro de " . ucfirst($t->name); ?>"/>
            </div>

            <div class="col-md-3">
                <label>Titulo da edição</label>
                <input type="text" class="form-control"
                       name="modTitle_edit_<?php echo $t->name; ?>"
                       value="<?php echo "Edição de " . ucfirst($t->name); ?>"/>
            </div>

            <div class="col-md-3">
                <label>Titulo da visualização e inicio</label>
                <input type="text" class="form-control"
                       name="modTitle_view_<?php echo $t->name; ?>"
                       value="<?php echo "Visualização de " . ucfirst($t->name); ?>"/>
            </div>


            <div class="clearfix"></div>
            <br/>
            <div class="clearfix"></div>
        </div>
    </div>
</div>