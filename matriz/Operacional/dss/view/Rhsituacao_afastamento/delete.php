<form class="form" method="post" action="<?php echo $this->Html->getUrl('Rhsituacao_afastamento', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Rhsituacao_afastamento->nome; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Rhsituacao_afastamento->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Rhsituacao_afastamento', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>