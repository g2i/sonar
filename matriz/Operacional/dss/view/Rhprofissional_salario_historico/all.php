
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Histórico de Salários</h2>
    <ol class="breadcrumb">
    <li>Histórico de Salários</li>
    </ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <div class="text-left col-md-6">
            <a href="Javascript:void(0)" class="btn btn-sm" onclick="Navegar('','back')" title='voltar'>
                <span class="img img-return"></span></a>
        </div>
   

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>     
                <th>                    
                </th>            
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional_salario_historico', 'all', array('orderBy' => 'valorremuneracao')); ?>'>
                        Valor Remuneração
                    </a>
                </th>    
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhprofissional_salario_historico', 'all', array('orderBy' => 'valorremuneracao')); ?>'>
                        Data Cadastro
                    </a>
                </th>               
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Rhprofissional_salario_historicos as $r) {
                echo '<tr>';  
                echo '<td>';
                echo '</td>';                         
                echo '<td>';
                echo "R$ $r->valorremuneracao";
                echo '</td>';  
                echo '<td>';
                echo $this->Html->getLink(DataBR($r->dt_cadastro), 'Rhprofissional_salario_historico', 'view',
                    array('id' => $r->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';             
                echo '<td width="50">';
           
                echo '</td>';
                echo '<td width="50">';            
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Rhprofissional_salario_historico', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Rhprofissional_salario_historico', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>