//validação de cpf
function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g,'');
    if(cpf == '') return false;
    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
        return false;
    // Valida 1o digito
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return false;
    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;
    return true;
}
// / fim validação de cpf
	function OpenDialog(titulo, mensagem) {
	var div = document.createElement("div");
    div.setAttribute('id', 'dialog-message');
    document.body.appendChild(div);

    function fech(){
        $("#dialog-message").remove();
    }
    $(function() {
        $("#dialog-message").dialog({
            modal: true,
            moveToTop:true,
            buttons: {
                Ok: fech
            }
        });
        $("#dialog-message").dialog({title: titulo});
        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});
        $("#dialog-message").text(mensagem);
    });
}
//Função para confirmação antes de enviar o formulario
function DialogConfirm(titulo, mensagem,funcao,parametros) {
	var div = document.createElement("div");
    div.setAttribute('id', 'dialog-message');
    document.body.appendChild(div);
    function sim(){
       $("#validForm").click();
        $("#dialog-message").remove();
    }
    function fech(){
        $("#dialog-message").remove();
    }
    $(function() {
        $("#dialog-message").dialog({
            modal: true,
            moveToTop:true,
            buttons: {
                Não: fech,
                Sim:sim
            }
        });
        $("#dialog-message").dialog({title: titulo});
        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});
        $("#dialog-message").text(mensagem);
    });
}


const myJSONObject = {"navegacao": [{"p": 1, "u": root}]};

function Navegar(url,param){
    var pos = myJSONObject.navegacao.length;
    if(param=='go'){
        pos = pos+1;
        var url = {"p": pos, "u": url};
        myJSONObject.navegacao.push(url);
    }else{
        pos = pos-1;
        myJSONObject.navegacao.pop();
    }

    $.each(myJSONObject,function(key,data){
        $.each(data,function(id,valor){
            if(pos==1){
                $('.modal').modal('hide');
            }else
            if(valor.p==pos) {
                if(param=='go')
                $('.modal').find('.modal-content').load(valor.u);
                else
                    if(valor.u!="")
                        $('.modal').find('.modal-content').load(valor.u);

            }
        });
    });
}
function Acrescentar(url){
    var pos = myJSONObject.navegacao.length;
    pos = pos+1;
    var url = {"p": pos, "u": url};
    myJSONObject.navegacao.push(url);
}
//PARA VALIDAR FORMULARIO E SALVAR A ALTERACAO, SE OFR PASSAR MGS COLOCAR COMO PARAMENTRO E PASSAR NA VIEW
function submitFormModal(id) {
    var div = document.createElement("div");
    div.setAttribute('id', 'dialog-message');
    document.body.appendChild(div);
    function sim(){
        //ENVIA FORMULARIO
        $("form#"+id).ajaxForm({
            success: function(d){
                if(d!=1){
                    OpenDialog('Alerta',d);
                }else{
                    Navegar('','back');
                }
            }
        });
        $("form#"+id).submit();
        //REMOVE MSG MOSTRADA NA TELA
        $("#dialog-message").remove();
    }
    function fech(){
        $("#dialog-message").remove();
    }
    $(function() {
        $("#dialog-message").dialog({
            modal: true,
            moveToTop:true,
            buttons: {
                Não: fech,
                Sim:sim
            }
        });
        $("#dialog-message").dialog({title: 'Atenção'});
        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});
        $("#dialog-message").text('Deseja salvar as Alterações?');
    });
}

function DialogConfirm(titulo, mensagem,funcao,parametros) {
	var div = document.createElement("div");
    div.setAttribute('id', 'dialog-message');
    document.body.appendChild(div);
    function sim(){
       $("#validForm").click();
        $("#dialog-message").remove();
    }
    function fech(){
        $("#dialog-message").remove();
    }
    $(function() {
        $("#dialog-message").dialog({
            modal: true,
            moveToTop:true,
            buttons: {
                Não: fech,
                Sim:sim
            }
        });
        $("#dialog-message").dialog({title: titulo});
        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});
        $("#dialog-message").text(mensagem);
    });
}

function EnviarFormulario(form){
    $(form).ajaxForm({
        success: function(d){
            if(d!=1){
                OpenDialog('Alerta',d);
            }else{
                Navegar('','back');
            }
        }
    });
}
//marcara cpf cep telefone
  $(document).ready(function() {
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cep').mask('00000-000');
        $('.phone').mask('(00) 0000-00000');
    });

//cep
function LoadGif(){
    var div = document.createElement("div");
    div.setAttribute('id','dialog-cep');
    var img = document.createElement("img");
    img.setAttribute("src",root+"/lib/css/images/WindowsPhoneProgressbar.gif");
    div.appendChild(img);
    document.body.appendChild(div);
    $("#dialog-cep").css({
        "position": "fixed",
        "top":"0",
        "right":"0",
        "bottom":"0",
        "left":"0",
        "background-color": "#000000",
        "opacity":"0.8",
        "filter": "alpha(opacity=80)",
        "z-index":" 3050"
    });

    $("#dialog-cep img").css({
        "position": "fixed",
        "top":"40%",
        "left":"30%"
    });
}

function CloseGif(){
    $("#dialog-cep").remove();
    $(".numero").focus();
}

$(function () {
    $("[data-tool='tooltip']").tooltip();
})


$( "[data-list='collapse']" ).click(function () {
    if(empty($( this ).parent().attr('class'))){
        $(this).parent().attr('class','active');
        $(this).parent().css({"border-left":"none","border-right":"4px solid #19AA8D"});
        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360deg)","transform":" rotate(360deg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
    }else{
        $(this).parent().removeAttr('class');
        $(this).children(".fa-th-large").removeAttr('style');
        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360reg)","transform":" rotate(360reg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});
        $(this).parent().css({"border-right":"none"});
    }
})

$("[role='open-adm']").click(function () {
    $("[role='adm']").css({"display":"block"});
    $(".bounceInRight").css({"opacity":"10"});
});

function empty(mixed_var) {
    var undef, key, i, len;
    var emptyValues = [undef, null, false, 0, '', '0'];

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }

    if (typeof mixed_var === 'object') {
        for (key in mixed_var) {
            // TODO: should we check for own properties only?
            //if (mixed_var.hasOwnProperty(key)) {
            return false;
            //}
        }
        return true;
    }

    return false;
}