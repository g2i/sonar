<?php
final class Anexoocorrencia extends Record{ 

    const TABLE = 'anexoocorrencia';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Anexoocorrencia pertence a Rhocorrencia
    * @return Rhocorrencia $Rhocorrencia
    */
    function getRhocorrencia() {
        return $this->belongsTo('Rhocorrencia','codigo_ocorrencia');
    }
    
    /**
    * Anexoocorrencia pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
}