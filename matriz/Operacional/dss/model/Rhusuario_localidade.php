<?php
final class Rhusuario_localidade extends Record{ 

    const TABLE = 'rhusuario_localidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * rhusuario_localidade pertence a Modulo
    * @return Modulo $Modulo
    */
    function getLocalidadeRh() {
        return $this->belongsTo('Rhlocalidade','rhlocalidade_id');
    }
    
    /**
    * rhusuario_localidade pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
}