<?php
final class Rhlocalidade extends Record{ 

    const TABLE = 'rhlocalidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }

    /**
    * Modulo possui rhusuario_localidade
    * @return array de rhusuario_localidade
    */
    function getRhusuarioLocalidade($criteria=NULL) {
        return $this->hasMany('Rhusuario_localidade','rhlocalidade_id',$criteria);
    }
}


    
    
