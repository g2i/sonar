<?php
final class Rhgrupo_histocorr extends Record{ 

    const TABLE = 'rhgrupo_histocorr';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Rhgrupo_histocorr pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    
    /**
    * Rhgrupo_histocorr possui Rhocorrencia_historicos
    * @return array de Rhocorrencia_historicos
    */
    function getRhocorrencia_historico($criteria=NULL) {
        return $this->hasMany('Rhocorrencia_historico','grupo',$criteria);
    }
}