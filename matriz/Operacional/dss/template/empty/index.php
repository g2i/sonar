<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php //$this->getHeaders();?>
</head>
<style>
@page {
  /*size: A4;*/
  margin: 0mm 17mm 0mm 17mm;
}
body {
  font-family: Arial, Helvetica, sans-serif !important;
}
.tabela-medidas {
  margin-top: -90px;
  font-size: 12px;
}
.tabela-medidas th {
  text-align: left !important;
}

.tabela-medidas .title{
  /*background-color:  lightblue;*/
  -webkit-print-color-adjust: exact;
  padding: 7px;

}
.header {
  padding-top: 130px;
  font-size: 12px;
}
footer {
  height: 100px;
}
/* css padrão - não deletar */
table.report-container {
  page-break-after:always;
}
thead.report-header {
  display:table-header-group;
}
tfoot.report-footer {
  display:table-footer-group;
} 
.td-nome {
  min-width: 300px;
}
.td-localidade {
  max-width: 150px;
}
div p{
text-align: right;

}

.cabecalho{
  text-align: right;
}

td {
    text-align: left;
    padding: 12px; 
    height: 10px;
    border-bottom: 1px solid #D5D5D5;

}

tr:nth-child(even) {background-color: #f2f2f2;}

.footer {
    position: fixed;

    bottom: 0;
  
    text-align: center;
}

.titulo-t{
  text-align: center;
}
.grid-container{
  width: 100%; 
  /* largura padrão para folha a4 */
  max-width: 21cm;      
}
/*-- our cleafix hack -- */ 
.row:before, 
.row:after {
  content:"";
  display: table ;
  clear:both;
}
[class*='col-'] {
  float: left; 
  min-height: 1px; 
  width: 16.66%; 
}
.col-1{ width: 16.66%; }
.col-2{ width: 33.33%; }
.col-3{ width: 50%;    }
.col-4{ width: 66.66%; }
.col-5{ width: 83.33%; }
.col-6{ width: 100%;   }

.clearfix{
  clear: both;
}
.text-right {
  text-align: right;
}
.text-left {
  text-align: left;
}
.text-center {
  text-align: center;
}
.pull-right {
  float: right !important;
}
.pull-left {
  float: left !important;
}
.table {
  width: 100%;
}
</style>
<body>
<style type="text/css">
    .container {
        max-width: 1000px;
        margin: auto;
    }
</style>

    <div class="container">
    <img src="<?php echo SITE_PATH ?>/lib/css/images/print_cabecalho.png" title="Upload Foto" id="charFoto" 
    style="width:720px;cursor:pointer; position: fixed;">

        <?php $this->getContents(); ?>
       <footer class="footer"> <img src="<?php echo SITE_PATH ?>/lib/css/images/print_footer.png" title="Upload Foto" id="charFoto" 
    style="width:800px;cursor:pointer;"></footer> 
    </div>

</body>

</html>
