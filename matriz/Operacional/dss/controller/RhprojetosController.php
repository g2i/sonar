<?php
final class RhprojetosController extends AppController{ 

    # página inicial do módulo Rhprojetos
    function index(){
        $this->setTitle('Visualização de Rhprojetos');
    }

    # lista de Rhprojetos
    # renderiza a visão /view/Rhprojetos/all.php
    function all(){
        $this->setTitle('Listagem de Rhprojetos');
        $p = new Paginate('Rhprojetos', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhprojetos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Rhprojetos
    # renderiza a visão /view/Rhprojetos/view.php
    function view(){
        $this->setTitle('Visualização de Rhprojetos');
        try {
            $this->set('Rhprojetos', new Rhprojetos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprojetos', 'all');
        }
    }

    # formulário de cadastro de Rhprojetos
    # renderiza a visão /view/Rhprojetos/add.php
    function add(){
        $this->setTitle('Cadastro de Rhprojetos');
        $this->set('Rhprojetos', new Rhprojetos);
    }

    # recebe os dados enviados via post do cadastro de Rhprojetos
    # (true)redireciona ou (false) renderiza a visão /view/Rhprojetos/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rhprojetos');
        $Rhprojetos = new Rhprojetos();
        $this->set('Rhprojetos', $Rhprojetos);
        
        $_POST['cadastradopor'] = Session::get('user')->id;
        $_POST['dt_cadastro'] = date('Y-m-d H:i:s');
        $_POST['situacao_id']= 1;
     
        try {
            $Rhprojetos->save($_POST);
            new Msg(__('Rhprojetos cadastrado com sucesso'));
            $this->go('Rhprojetos', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Rhprojetos
    # renderiza a visão /view/Rhprojetos/edit.php
    function edit(){
        $this->setTitle('Edição de Rhprojetos');
        try {
            $this->set('Rhprojetos', new Rhprojetos((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhprojetos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhprojetos
    # (true)redireciona ou (false) renderiza a visão /view/Rhprojetos/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhprojetos');
        try {
            $Rhprojetos = new Rhprojetos((int) $_POST['id']);
            $this->set('Rhprojetos', $Rhprojetos);
            $_POST['modificadopor'] = Session::get('user')->id;
            $_POST['dt_atualizacao'] = date('Y-m-d H:i:s');
            $_POST['situacao_id']= 1;

            $Rhprojetos->save($_POST);
            new Msg(__('Rhprojetos atualizado com sucesso'));
            $this->go('Rhprojetos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhprojetos
    # renderiza a /view/Rhprojetos/delete.php
    function delete(){
        $this->setTitle('Apagar Rhprojetos');
        try {
            $this->set('Rhprojetos', new Rhprojetos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprojetos', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhprojetos
    # redireciona para Rhprojetos/all
    function post_delete(){
        try {
            $Rhprojetos = new Rhprojetos((int) $_POST['id']);
            $_POST['situacao_id']= 3;
            $Rhprojetos->salve();
            new Msg(__('Rhprojetos apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprojetos', 'all');
    }

}