<?php
final class AnexoController extends AppController{ 

    # página inicial do módulo Anexo
    function index(){
        $this->setTitle('Visualização de Anexo');
    }

    # lista de Anexos
    # renderiza a visão /view/Anexo/all.php situacao
    function all(){
        $this->setTitle('Listagem de Anexo');
     
        $p = new Paginate('Anexo', 10);
        $c = new Criteria();
        $c->addCondition('id_externo','=',$this->getParam('id'));
        $c->addCondition('situacao','=', 1);
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Anexos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Anexo
    # renderiza a visão /view/Anexo/view.php
    function view(){
        $this->setTitle('Visualização de Anexo');
        try {
            $this->set('Anexo', new Anexo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexo', 'all');
        }
    }

    # formulário de cadastro de Anexo
    # renderiza a visão /view/Anexo/add.php
    function add(){
        $this->setTitle('Cadastro de Anexo');
        $this->set('Anexo', new Anexo);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Tipo_anexos',  Tipo_anexos::getList());
        $this->set('DssCursos', Dss_curso::getList());
    }

    # recebe os dados enviados via post do cadastro de Anexo
    # (true)redireciona ou (false) renderiza a visão /view/Anexo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Anexo');
        $anexos = new Anexo();
        if(!empty($_FILES['url']['name'])){
            $file = $_FILES['url'];
            $extencoes = array('jpg', 'pdf', 'gif', 'mp3', 'mp4', 'odf', 'docx', 'doc', 'txt', 'ppd', 'ppx', 'xlsx', 'pptx', 'xls', 'png', 'zip', 'rar');
            $anexo = new FileUploader($file, NULL, $extencoes);
            $path = "/anexosdss/" . $_POST['dss_id'];
            $nome = uniqid() . $anexo->TratarName($file['name']);
            $anexo->save($nome, $path);
            $anexos->url = SITE_PATH . '/uploads' . $path . "/" . $nome;
        
            $user=Session::get('user');
            $anexos->cadastradopor = $user->id;
            $anexos->dtCadastro = date('Y-m-d H:i:s');
            $anexos->situacao = 1;
            $anexos->id_externo = $_POST['dss_id'];
            try {
                $anexos->save($_POST);
                new Msg(__('Anexo cadastrado com sucesso'));
                if (!empty($_POST['modal'])) {
                    echo 1;
                     exit;
                }
                $this->go('Anexos', 'all');
                
            } catch (Exception $e) {
                new Msg($e->getMessage(),3);
            }
        }
        $this->set('Anexo', $anexos);
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Anexo
    # renderiza a visão /view/Anexo/edit.php
    function edit(){
        $this->setTitle('Edição de Anexo');
        try {
            $this->set('Anexo', new Anexo((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Anexo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Anexo
    # (true)redireciona ou (false) renderiza a visão /view/Anexo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Anexo');
        try {
            $Anexo = new Anexo((int) $_POST['id']);
            $this->set('Anexo', $Anexo);
            $Anexo->save($_POST);
            new Msg(__('Anexo atualizado com sucesso'));
            $this->go('Anexo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Anexo
    # renderiza a /view/Anexo/delete.php
    function delete(){
        $this->setTitle('Apagar Anexo');
        try {
            $this->set('Anexo', new Anexo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Anexo
    # redireciona para Anexo/all
    function post_delete(){
        try {
            $Anexo = new Anexo((int) $_POST['id']);
            $Anexo->situacao = 3;
            $Anexo->save();
            new Msg(__('Anexo apagado com sucesso'), 1);
           
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Dss_curso', 'all');
    }
}