<?php
final class RhstatusController extends AppController{ 

    # página inicial do módulo Rhstatus
    function index(){
        $this->setTitle('Visualização de Status');
    }

    # lista de Rhstatus
    # renderiza a visão /view/Rhstatus/all.php
    function all(){
        $this->setTitle('Listagem de Status');
        $p = new Paginate('Rhstatus', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhstatus', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Rhstatus
    # renderiza a visão /view/Rhstatus/view.php
    function view(){
        $this->setTitle('Visualização de Status');
        try {
            $this->set('Rhstatus', new Rhstatus((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhstatus', 'all');
        }
    }

    # formulário de cadastro de Rhstatus
    # renderiza a visão /view/Rhstatus/add.php
    function add(){
        $this->setTitle('Cadastro de Status');
        $this->set('Rhstatus', new Rhstatus);
    }

    # recebe os dados enviados via post do cadastro de Rhstatus
    # (true)redireciona ou (false) renderiza a visão /view/Rhstatus/add.php
    function post_add(){
        $this->setTitle('Cadastro de Status');
        $Rhstatus = new Rhstatus();
        $this->set('Rhstatus', $Rhstatus);
        try {
            $Rhstatus->save($_POST);
            new Msg(__('Rhstatus cadastrado com sucesso'));
            $this->go('Rhstatus', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Rhstatus
    # renderiza a visão /view/Rhstatus/edit.php
    function edit(){
        $this->setTitle('Edição de Status');
        try {
            $this->set('Rhstatus', new Rhstatus((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhstatus', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhstatus
    # (true)redireciona ou (false) renderiza a visão /view/Rhstatus/edit.php
    function post_edit(){
        $this->setTitle('Edição de Status');
        try {
            $Rhstatus = new Rhstatus((int) $_POST['codigo']);
            $this->set('Rhstatus', $Rhstatus);
            $Rhstatus->save($_POST);
            new Msg(__('Rhstatus atualizado com sucesso'));
            $this->go('Rhstatus', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhstatus
    # renderiza a /view/Rhstatus/delete.php
    function delete(){
        $this->setTitle('Apagar Rhstatus');
        try {
            $this->set('Rhstatus', new Rhstatus((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhstatus', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhstatus
    # redireciona para Rhstatus/all
    function post_delete(){
        try {
            $Rhstatus = new Rhstatus((int) $_POST['id']);
            $Rhstatus->delete();
            new Msg(__('Rhstatus apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhstatus', 'all');
    }

}