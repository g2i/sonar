<?php
date_default_timezone_set("America/Campo_Grande");
##################################
# CONFIGURAÇÕES DO SGBD 
##################################

# Tipo do SGBD
# 0 = mysql
# 1 = postgresql
//Config::set('db_type', 0);
//# servidor do SGBD:
//Config::set('db_host', '192.175.99.74');
//# usuario do SGBD:
//Config::set('db_user', 'g2ioper_js');
//# senha do SGBD:
//Config::set('db_password', 'js6990');
//# nome do banco de dados:
//Config::set('db_name', 'g2ioper_js');

Config::set('db_host', 'localhost');
Config::set('db_user', 'root');
Config::set('db_password', '');
Config::set('db_name', 'g2ioper_js_local');

/*
Config::set('db_host', '192.175.99.74');
Config::set('db_user', 'g2ioper_js');
Config::set('db_password', 'js6990');
Config::set('db_name', 'g2ioper_js');*/


# MODO DE DEPURAÇÃO
# desenvolvimento: true
# produção: false
Config::set('debug', false);

# TEMPLATE PADRÃO
Config::set('template', 'inspinia');


# Chave da aplicação, para controle de sessões e criptografia
# Utilize uma cadeia alfanumérica aleatória única
Config::set('key', 'r528qy2014161d5jqtjeo3h8875gfFDGfqqqk3djghkH&*');

# SALT - Utilizada na criptografia
# Utiliza uma chave alfa-numéria complexa de no mínimo 16 dígitos
Config::set('salt', 'TjcqT8jgR8H6v6vhJhBcdZmCvnZs4Ghs9JcvW48gCqUTtVEkcK5hYLpsw6As8AbU');

Config::set('lang', 'pt_br');
Config::set('rewriteURL', true);
Config::set('indexController', 'Medidores_medidor');
Config::set('indexAction', 'all');
Config::set('criptedGetParamns', array());