<?php
final class Estq_artigo_loteController extends AppController{ 

    # página inicial do módulo Estq_artigo_lote
    function index(){
        $this->setTitle('Visualização de Artigo-Lote');
    }

    # lista de Estq_artigo_lotes
    # renderiza a visão /view/Estq_artigo_lote/all.php
    function all(){
        $this->setTitle('Listagem de Artigos-Lotes');
        $p = new Paginate('Estq_artigo_lote', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('artigo',"=",$this->getParam('id'));
        $c->addCondition('origem','=',Config::get('origem')); //filtro
        $this->set('Estq_artigo_lotes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Estq_artigo_lote
    # renderiza a visão /view/Estq_artigo_lote/view.php
    function view(){
        $this->setTitle('Visualização de Artigo-Lote');
        try {
            $this->set('Estq_artigo_lote', new Estq_artigo_lote((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo_lote', 'all');
        }
    }

    # formulário de cadastro de Estq_artigo_lote
    # renderiza a visão /view/Estq_artigo_lote/add.php
    function add(){
        $this->setTitle('Cadastro de Artigo-Lote');
        $this->set('Estq_artigo_lote', new Estq_artigo_lote);
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_artigo_lote
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo_lote/add.php
    function post_add(){
        $this->setTitle('Cadastro de Artigo-Lote');
        $Estq_artigo_lote = new Estq_artigo_lote();
        $this->set('Estq_artigo_lote', $Estq_artigo_lote);
       // $user=Session::get('user');
       // $Estq_artigo_lote->usuario_id=$user->id;
        $_POST['usuario_dt'] = date('Y-m-d H:i:s');
        $_POST['validade']=DataSQL($_POST['validade']); //converte para o formato americano
        $_POST['origem']=Config::get('origem');//salvando a origem
        $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
        $Estq_artigo_lote->usuario_id=$user; // para salvar o usuario que está fazendo
        try {
            $Estq_artigo_lote->save($_POST);
            new Msg(__('Artigo-Lote cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }

            $this->go('Estq_artigo_lote', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição  de Estq_artigo_lote
    # renderiza a visão /view/Estq_artigo_lote/edit.php
    function edit(){
        $this->setTitle('Edição de Artigo-Lote');
        try {
            $this->set('Estq_artigo_lote', new Estq_artigo_lote((int) $this->getParam('id')));
            $this->set('Estq_artigos',  Estq_artigo::getList());
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_artigo_lote', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_artigo_lote
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo_lote/edit.php
    function post_edit(){
        $this->setTitle('Edição de Artigo-Lote');
        try {
            $Estq_artigo_lote = new Estq_artigo_lote((int) $_POST['id']);
            $this->set('Estq_artigo_lote', $Estq_artigo_lote);
            $_POST['validade']=DataSQL($_POST['validade']);
            $_POST['usuario_dt'] = date('Y-m-d H:i:s');
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_artigo_lote->usuario_id=$user; // para salvar o usuario que está fazendo
                $Estq_artigo_lote->save($_POST);
            new Msg(__('Artigo-Lote atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Estq_artigo_lote', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_artigo_lote
    # renderiza a /view/Estq_artigo_lote/delete.php
    function delete(){
        $this->setTitle('Apagar Artigo-Lote');
        try {
            $this->set('Estq_artigo_lote', new Estq_artigo_lote((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo_lote', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_artigo_lote
    # redireciona para Estq_artigo_lote/all
    function post_delete(){
        try {
            $Estq_artigo_lote = new Estq_artigo_lote((int) $_POST['id']);
            $Estq_artigo_lote->usuario_dt = date('Y-m-d H:i:s');
            $Estq_artigo_lote->situacao=3;
            $Estq_artigo_lote->save();
            new Msg(__('Artigo-Lote apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if (!empty($_POST['modal'])) {
            echo 1;
            exit;
        }
        $this->go('Estq_artigo_lote', 'all');
    }

}