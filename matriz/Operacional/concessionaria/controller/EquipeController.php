<?php
final class EquipeController extends AppController{ 

    # página inicial do módulo Equipe
    function index(){
        $this->setTitle('Visualização de Equipe');
    }

    # lista de Equipes
    # renderiza a visão /view/Equipe/all.php
    function all(){
        $this->setTitle('Listagem de Equipe');
        $p = new Paginate('Equipe', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){

            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }

            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        $c->addCondition('situacao',"=", 1);

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        $this->set('Equipes', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Situacaos',  Situacao::getList());

    }

    # visualiza um(a) Equipe
    # renderiza a visão /view/Equipe/view.php
    function view(){
        $this->setTitle('Visualização de Equipe');
        try {
            $this->set('Equipe', new Equipe((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Equipe', 'all');
        }
    }

    # formulário de cadastro de Equipe
    # renderiza a visão /view/Equipe/add.php
    function add(){
        $this->setTitle('Cadastro de Equipe');
        $this->set('Equipe', new Equipe);
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Equipe
    # (true)redireciona ou (false) renderiza a visão /view/Equipe/add.php
    function post_add(){
        $this->setTitle('Cadastro de Equipe');
        $Equipe = new Equipe();
        $this->set('Equipe', $Equipe);
        try {
            $Equipe->save($_POST);
            new Msg(__('Equipe cadastrado com sucesso'));
            $this->go('Equipe', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Equipe
    # renderiza a visão /view/Equipe/edit.php
    function edit(){
        $this->setTitle('Edição de Equipe');
        try {
            $this->set('Equipe', new Equipe((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Equipe', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Equipe
    # (true)redireciona ou (false) renderiza a visão /view/Equipe/edit.php
    function post_edit(){
        $this->setTitle('Edição de Equipe');
        try {
            $Equipe = new Equipe((int) $_POST['id']);
            $this->set('Equipe', $Equipe);
            $Equipe->save($_POST);
            new Msg(__('Equipe atualizado com sucesso'));
            $this->go('Equipe', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Equipe
    # renderiza a /view/Equipe/delete.php
    function delete(){
        $this->setTitle('Apagar Equipe');
        try {
            $this->set('Equipe', new Equipe((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Equipe', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Equipe
    # redireciona para Equipe/all
    function post_delete(){
        try {
            $Equipe = new Equipe((int) $_POST['id']);
            $Equipe->situacao = 3;
            $Equipe->save();
            new Msg(__('Equipe apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Equipe', 'all');
    }

}