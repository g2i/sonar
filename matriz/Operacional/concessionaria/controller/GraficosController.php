<?php
final class GraficosController extends AppController
{

    # página inicial do módulo Fornecedor
    function index()
    {
        $this->setTitle('Análise gráfica');

        $dados = $this->query("SELECT estq_mov.`data_medicao` AS mes_ano,
                        EXTRACT(MONTH FROM estq_mov.`data_medicao`) AS mes, SUM(
                            IF(
                              estq_mov.`situacao_mov` >= 4,
                              estq_mov.`valor`,
                              IF((estq_mov.`valor_recebimento`+ estq_mov.`valor_recebimento2` + estq_mov.`valor_recebimento3` + estq_mov.`valor_recebimento4`)>0,
					(estq_mov.`valor_recebimento`+ estq_mov.`valor_recebimento2` + estq_mov.`valor_recebimento3` + estq_mov.`valor_recebimento4`),estq_mov.`valor`)
                            )
                          ) AS total
                          FROM estq_mov
                          WHERE estq_mov.`data_medicao`>= DATE_SUB(CURDATE(),INTERVAL 6 MONTH) AND  estq_mov.`data_medicao`<= CURDATE() and estq_mov.situacao = 1
                          GROUP BY EXTRACT(MONTH FROM estq_mov.`data_medicao`)
                          ORDER BY mes_ano ASC");

                        $array['data']= array();
                        $labels= array();
                        $meses = $this->meses();

                        foreach ($dados as $d){
                            $array['data'][] = $d->total;
                            $array['backgroundColor'][] = '#36A2EB';
                            $labels[] = $meses[$d->mes];
                        }
                        $array['label'] = "Análise Gráfica últimos 6 meses";
                    $this->set('dados',json_encode($array));
                    $this->set('labels',json_encode($labels));


    }

    function meses(){
        $meses = array(
            1 => "Janeiro",
            2 => "Fevereiro",
            3 => "Março",
            4 => "Abril",
            5 => "Maio",
            6 => "Junho",
            7 => "Julho",
            8 => "Agosto",
            9 => "Setembro",
            10 => "Outubro",
            11 => "Novembro",
            12 => "Dezembro"
        );
        return $meses;
    }

}