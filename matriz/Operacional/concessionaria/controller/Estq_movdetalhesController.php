
<?php
final class Estq_movdetalhesController extends AppController{

    # página inicial do módulo Estq_movdetalhes
    function index(){
        $this->setTitle('Visualização de Detalhes Movimento');
    }

    # lista de Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/all.php
    function all(){
        $this->setTitle('Listagem de Detalhes Movimento');
        $this->setStyles(SITE_PATH . '/content/css/bootstrap-editable.css');
        $this->setStyles(SITE_PATH . '/content/css/jquery.bootgrid.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2-bootstrap.min.css');
        $this->setStyles(SITE_PATH . '/content/css/bootstrap-dialog.min.css');

        $this->setScript(SITE_PATH . '/content/js/jquery.json.min.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-editable.min.js');
        $this->setScript(SITE_PATH . '/content/js/select2.full.min.js');
        $this->setScript(SITE_PATH . '/content/js/i18n/pt-BR.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.bootgrid.min.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.bootgrid.fa.min.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.number.min.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-dialog.min.js');

        $this->set('mov', $this->getParam('id'));
    }

    function getDetalhes(){

        $mov = (int)$this->getParam('mov');
        $page = (int)$this->getParam('current');
        $size = (int)$this->getParam('rowCount');
        $artigo = (int)$this->getParam('searchPhrase');
        $artigo_two = $this->getParam('artigo');

        $sort = json_decode($this->getParam('sort'));


        if(!isset($artigo) || $artigo < 1)
            $artigo = 0;

        if(!isset($size) || $size < 1)
            $size = 10;

        if(!isset($page) || $page < 1)
            $page = 1;

        $db = $this::getConn();
        $sql = "SELECT d.id, a.id as atrId,d.vlnf, a.nome as artigo, a.codigo_livre as codigo, d.quantidade, d.quantidade, d.qtrecebido,
                d.qtseparado, d.qtcautela_encarregado, d.qtdevolucao_encarregado, d.qtinventario_campo, d.qtzmdv, d.movimento, m.numdoc,d.adicional FROM
                estq_movdetalhes as d INNER JOIN estq_artigo as a ON (a.id = d.artigo)
                INNER JOIN estq_mov m ON (d.movimento = m.id)";

                if($this->getParam('colab') || $this->getParam('datacautela')){
                    $sql.="  LEFT JOIN c_complemento_mov cc ON cc.mov_detalhes = d.id ";
                }
        $sql.="WHERE d.situacao != 3 AND d.movimento = :mov";

        $db->query($sql);
        $db->bind(":mov", $mov, PDO::PARAM_INT);
        $db->execute();
        $total = $db->rowCount();

        if($artigo > 0){
            $sql = $sql." AND a.id = :cod";
        }
        if(!empty($artigo_two)){
            $sql = $sql." AND a.nome LIKE :art";
        }
        if($this->getParam('colab')){
            $sql.= " AND cc.colaborador = :colab";
        }
        if($this->getParam('datacautela')){
            $sql.= " AND DATE_FORMAT(cc.`data`,'%Y-%m-%d') = :datc";
        }
        if($sort){
            $sql .=" ORDER BY ";
            $ords = array();
            foreach ($sort as $s =>$key) {
                $ords[] =$key[0]." ".$key[1];
            }
            $sql.= implode(',',$ords);
        }else{
            $sql = $sql." ORDER BY a.codigo_livre";
        }
        $sql = $sql." LIMIT :li OFFSET :off";
        $db->query($sql);
        $db->bind(":mov", $mov, PDO::PARAM_INT);
        if($artigo > 0) {
            $db->bind(":cod", $artigo);
        }
        if(!empty($artigo_two)){
            $db->bind(":art", $artigo_two.'%');
        }

        if($this->getParam('colab')){
            $db->bind(":colab", $this->getParam('colab'));
        }

        if($this->getParam('datacautela')){
            $db->bind(":datc", $this->getParam('datacautela'));
        }

        $db->bind(":li", $size, PDO::PARAM_INT);
        $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
        $dados = $db->getResults();

        $ret = array();
        $ret["current"] = $page;
        $ret["rowCount"] = count($dados);
        $ret["rows"] = $dados;
        $ret["total"] = $total;

        echo json_encode($ret);
        exit;
    }

    function post_updateDetalhe(){

        $campo = $_POST['name'];
        $pk = $_POST['pk'];
        $value = $_POST['value'];

        switch($campo){
            case "Artigo":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET artigo = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;

            case "Planejado":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET quantidade = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;

            case "Recebido":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET qtrecebido = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;

            case "Separado":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET qtseparado = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;
            
            case "Cautela":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET qtcautela_encarregado = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;

            case "Devolucao":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET qtdevolucao_encarregado = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;

            case "Fiscal":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET qtinventario_campo = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;

            case "Zmdv":
                $db = $this::getConn();
                $sql = "UPDATE estq_movdetalhes SET qtzmdv = :campo WHERE id = :cod";
                $db->query($sql);
                $db->bind(":campo", $value);
                $db->bind(":cod", $pk);
                $db->execute();
                break;
        }

        $stq = new Estq_movdetalhes($pk);
        echo json_encode($stq);
        exit;
    }

    # visualiza um(a) Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/view.php
    function view(){
        $this->setTitle('Visualização de Detalhes Movimento');

        try {
            $this->set('Estq_movdetalhes', new Estq_movdetalhes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_movdetalhes', 'all');
        }
    }

    # formulário de cadastro de Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/add.php
    function add(){
        $this->setTitle('Cadastro de Detalhes Movimento');
        $this->set('Estq_movdetalhes', new Estq_movdetalhes);

        $b = new Criteria();
        $b->addCondition('origem','=',Config::get('origem'));
        $b->addCondition('situacao','<>', 3);
        $b->setOrder('nome');
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_artigos',  Estq_artigo::getList($b));
        $this->set('Estq_movs',  Estq_mov::getList());
        $this->set('Estq_lote_substoques',  Estq_lote_substoque::getList());
    }

    function GetLoteSubestoque(){
        $a= new Criteria();
        $a->addCondition('artigo','=',$_POST['artigo']);
        $a->addCondition('origem','=',Config::get('origem'));
        $a->addCondition('situacao','<>',3);
        $lote_substoque = Estq_lote_substoque::getList($a);
        $html="";
        $html.="<option value=''>Selecione</option>";
        foreach ($lote_substoque as $u) {
            $html.="<option value='".$u->id."'>".$u->getEstq_artigo_lote()->nome ." - ".  $u->getEstq_subestoque()->nome  ."</option>";
        }
        echo $html;
        exit;
    }

    function GetCodigo(){
        $b= new Criteria();
        $b->addCondition('situacao','=',1);
        $b->addCondition('id','=',$_POST['artigo']);
        $artigo = Estq_artigo::getList($b);
        $html="";
        $html.="<option value=''>Selecione</option>";
        foreach ($artigo as $t) {
            $html.="<option selected value='".$t->id."'>".$t->codigo_livre ."</option>";
        }
        echo $html;
        exit;
    }

    function GetArtigo(){
        $b= new Criteria();
        $b->addCondition('situacao','=',1);
        $b->addCondition('origem','=',Config::get('origem'));
        $b->addCondition('id','=',$_POST['artigo']);
        $artigo = Estq_artigo::getList($b);
        $html="";
        $html.="<option value=''>Selecione</option>";
        foreach ($artigo as $t) {
            $html.="<option selected value='".$t->id."'>".$t->nome ."</option>";
        }
        echo $html;
        exit;
    }

    function listar(){
        $aux = Config::get('origem');
        if( $_POST['data']['q'] != "" ){
            $arr = $this->query("SELECT `id`,`codigo_livre` FROM `estq_artigo` WHERE (codigo_livre LIKE '".$_POST['data']['q']."%') AND situacao <> 3 AND origem = ".$aux."; ");
            $results = array();
            foreach( $arr as $cli ){
                if(!empty($cli->id) && !empty($cli->codigo_livre))
                    $results[] = array(
                        'id' => $cli->id,
                        'text' => $cli->codigo_livre
                    );
            }
            echo json_encode(array('q' => $_POST['data']['q'], 'results' => $results));
            exit;
        }
        echo json_encode(array('q' => "", 'results' => array()));
        exit;

    }

    function listar2(){
        $aux = Config::get('origem');
        if( $_POST['data']['q'] != "" ){
            $arr = $this->query("SELECT `id`,`nome` FROM `estq_artigo` WHERE (nome LIKE '".$_POST['data']['q']."%') AND situacao = 1 AND origem = ".$aux."; ");
            $results = array();
            foreach( $arr as $cli ){
                if(!empty($cli->id) && !empty($cli->nome))
                    $results[] = array(
                        'id' => $cli->id,
                        'text' => $cli->nome
                    );
            }
            echo json_encode(array('q' => $_POST['data']['q'], 'results' => $results));
            exit;
        }
        echo json_encode(array('q' => "", 'results' => array()));
        exit;

    }

    function removerAcentos( $texto ){
        $array1 = array(   "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
        , "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );
        $array2 = array(   "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
        , "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" );
        return str_replace( $array1, $array2, $texto );
    }

    # recebe os dados enviados via post do cadastro de Estq_movdetalhes
    # (true)redireciona ou (false) renderiza a visão /view/Estq_movdetalhes/add.php
    function post_add(){
        $this->setTitle('Cadastro de Detalhes Movimento');
        $Estq_movdetalhes = new Estq_movdetalhes();
        $this->set('Estq_movdetalhes', $Estq_movdetalhes);
        $_POST['usuario_dt']=date('Y-m-d H:i:s');
        $_POST['origem']= Config::get('origem');//salvando a origem
        try {

            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_movdetalhes->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_movdetalhes->qtrecebido= 0.0000;
            $Estq_movdetalhes->qtseparado= 0.0000;
            $Estq_movdetalhes->qtcautela_encarregado= 0.0000;
            $Estq_movdetalhes->qtdevolucao_encarregado= 0.0000;
            $Estq_movdetalhes->qtinventario_campo= 0.0000;
            $Estq_movdetalhes->adicional= 0.0000;
            $Estq_movdetalhes->qtzmdv= 0;
            $Estq_movdetalhes->save($_POST);

            //NAVEGAÇÃO ENTRE MODAl
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }

            new Msg(__('Detalhes-Movimento cadastrado com sucesso'));
            //TERMINA AQUI
            $this->go('Estq_movdetalhes', 'all');
        } catch (Exception $e) {
            //erro modal
            if(!empty($_POST['modal'])){
                echo $e->getMessage();
                exit;
            }

            new Msg($e->getMessage(),3);
        }

        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $c = new Criteria();
        $c->addCondition('origem','=',Config::get('origem'));
        $this->set('Estq_artigos',  Estq_artigo::getList($c));
        $this->set('Estq_movs',  Estq_mov::getList());
        $this->set('Estq_lote_substoques',  Estq_lote_substoque::getList());
    }

    # formulário de edição de Estq_movdetalhes
    # renderiza a visão /view/Estq_movdetalhes/edit.php
    function edit(){
        $this->setTitle('Edição de Detalhes Movimento');
        try {
            $Estq_movdetalhes = new Estq_movdetalhes((int) $this->getParam('id'));
            $this->set('Estq_movdetalhes', $Estq_movdetalhes);
            $this->set('Estq_situacaos',  Estq_situacao::getList());
            $this->set('Estq_artigos', new Estq_artigo((int) $Estq_movdetalhes->artigo));
            $this->set('Estq_lote_substoques', new Estq_lote_substoque((int) $Estq_movdetalhes->lote_substoque));
            $this->set('Estq_movs',  Estq_mov::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_movdetalhes', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_movdetalhes
    # (true)redireciona ou (false) renderiza a visão /view/Estq_movdetalhes/edit.php
    function post_edit(){
        $this->setTitle('Edição de Detalhes Movimento');
        try {
            $Estq_movdetalhes = new Estq_movdetalhes((int) $_POST['id']);
            /*  if($_POST['validade']!= NULL) {
                 $_POST['validade']=DataSQL($_POST['validade']); //converte para o formato americano
             }

            if (!empty($_POST['vlnf'])) {
                 $_POST['vlnf'] = str_replace(".", "", $_POST['vlnf']);
                 $_POST['vlnf'] = str_replace(",", ".", $_POST['vlnf']);
             }
             if (!empty($_POST['vlcusto'])) {
                 $_POST['vlcusto'] = str_replace(".", "", $_POST['vlcusto']);
                 $_POST['vlcusto'] = str_replace(",", ".", $_POST['vlcusto']);
             }
             if (!empty($_POST['vlcustomedio'])) {
                 $_POST['vlcustomedio'] = str_replace(".", "", $_POST['vlcustomedio']);
                 $_POST['vlcustomedio'] = str_replace(",", ".", $_POST['vlcustomedio']);
             }*/


            $Estq_movdetalhes->usuario_dt=date('Y-m-d H:i:s');
            $user=Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_movdetalhes->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_movdetalhes->save($_POST);
            $this->set('Estq_movdetalhes', $Estq_movdetalhes);
            new Msg(__('Detalhes-Movimento atualizado com sucesso'));
            //NAVEGAÇÃO ENTRE MODAl
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Estq_movdetalhes', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_artigos',  Estq_artigo::getList());
        $this->set('Estq_movs',  Estq_mov::getList());
        $this->set('Estq_lote_substoques',  Estq_lote_substoque::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_movdetalhes
    # renderiza a /view/Estq_movdetalhes/delete.php
    function delete(){
        $this->setTitle('Apagar Detalhes-Movimento');
        try {
            $this->set('Estq_movdetalhes', new Estq_movdetalhes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_movdetalhes', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_movdetalhes
    # redireciona para Estq_movdetalhes/all
    function post_delete(){

        try {

            $Estq_movdetalhes = new Estq_movdetalhes((int)$_POST['id']);
            $Estq_movdetalhes->usuario_dt=date('Y-m-d H:i:s');
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_movdetalhes->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_movdetalhes->situacao=3;
            $Estq_movdetalhes->save();

            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }

            if(!empty($_POST['json'])){
                $ret = array();
                $ret['result'] = true;
                $ret['msg'] = utf8_encode('Detalhe apagado com sucesso.');
                echo json_encode($ret);
                exit;
            }

            new Msg(__('Detalhes-Movimento apagado com sucesso'), 1);
            //TERMINA AQUI
        } catch (Exception $e) {
            if(!empty($_POST['json'])){
                $ret = array();
                $ret['result'] = false;
                $ret['msg'] = utf8_encode('Erro ao processar solicitação.');
                $ret['erro'] = utf8_encode($e->getMessage());
                echo json_encode($ret);
                exit;
            }

            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_movdetalhes', 'all');
    }

    function save_oper(){
        $colaborador = ($_POST['colab']!='-1') ? $_POST['colab'] : null;
        $Cadicional = new C_complemento_mov();
        $Cadicional->mov_detalhes = $_POST['id'];
        $Cadicional->qtde= $_POST['valor'];
        $Cadicional->tipo= $_POST['tipo'];
        $Cadicional->data= date('Y-m-d H:i:s');
        $Cadicional->user= Session::get('user')->id;
        $Cadicional->situacao= 1;
        $Cadicional->movimento= $_POST['mov'];
        $Cadicional->colaborador= $colaborador;
        $Cadicional->numdoc= $_POST['numdoc'];

        Session::set('ultimo_setado',$colaborador);
        try {
            $Cadicional->save();
            $mov_det = new Estq_movdetalhes((int)$_POST['id']);
            $operacoes =array(
                1 => 'quantidade',
                2 => 'qtrecebido',
                3 => 'qtseparado',
                4 => 'qtcautela_encarregado',
                5 => 'qtdevolucao_encarregado',
                6 => 'qtinventario_campo',
                7 => 'qtzmdv',
                8 => 'adicional',
                9 => 'vlnf'
            );
            $mov_det->$operacoes[$_POST['tipo']] = $mov_det->$operacoes[$_POST['tipo']]+floatval($_POST['valor']);
            if($mov_det->save()){
                if($_POST['tipo']==2 || $_POST['tipo']==4 || $_POST['tipo']==5 ){
                    $mov_detalhes = new Estq_movdetalhes($_POST['id']);
                    $movimento = new Estq_mov($_POST['mov']);
                    $a = new Criteria();
                    $a->addCondition('artigo','=',$mov_detalhes->artigo);
                    $a->addCondition('origem','=',Config::get('origem'));
                    $a->addCondition('situacao','=',1);
                    if(!empty($movimento->getEstq_mov()->sub_padrao)) {
                        $a->addCondition('subestoque', '=', $movimento->getEstq_mov()->sub_padrao);
                    }else{
                        $a->addCondition('subestoque', '=', 18);
                    }
                    $Lote_substoques = Estq_lote_substoque::getFirst($a);
                    $Lote_sub = new Estq_lote_substoque($Lote_substoques->id);
                    if($_POST['tipo']==4){
                        $Lote_sub->quantidade = $Lote_substoques->quantidade - $_POST['valor'];
                    }else{
                        $Lote_sub->quantidade = $Lote_substoques->quantidade + $_POST['valor'];
                    }
                    $Lote_sub->save();

                }
            }
            echo number_format($mov_det->$operacoes[$_POST['tipo']],2,'.','');
            exit;

        }catch (Exception $e){
            new Msg($e->getMessage(), 2);
            exit;
        }
    }

    function list_oper(){
        $a = new Criteria();
        $a->addCondition('mov_detalhes','=',$_POST['id']);
        $a->addCondition('tipo','=',$_POST['tipo']);
        $a->addCondition('situacao','=',1);
        $a->setOrder('id DESC');
        $list_adicional = C_complemento_mov::getList($a);
        $html ="";
        if($_POST['tipo']==4 || $_POST['tipo']==5){
            $html .= '<div class="col-md-3"><strong>Quantidade</strong></div>';
            $html .= '<div class="col-md-3"><strong>Data</strong></div>';
            //$html .= '<div class="col-md-3"><strong>Quem cadastrou</strong></div>';
            $html .= '<div class="col-md-3"><strong>Colaborador</strong></div>';
        }else if($_POST['tipo']==2 || $_POST['tipo']==8){
            $html .= '<div class="col-md-3"><strong>Quantidade</strong></div>';
            $html .= '<div class="col-md-3"><strong>Num Documento</strong></div>';
            $html .= '<div class="col-md-3"><strong>Data</strong></div>';
            //$html .= '<div class="col-md-3"><strong>Quem cadastrou</strong></div>';
        }else {
            $html .= '<div class="col-md-3"><strong>Quantidade</strong></div>';
            $html .= '<div class="col-md-5"><strong>Data</strong></div>';
            //$html .= '<div class="col-md-4"><strong>Quem cadastrou</strong></div>';
        }
        $html.='<div class="clearfix"></div>';
        $total =0;
        foreach ($list_adicional as $i) {
            $total+=$i->qtde;
            if($_POST['tipo']==4 || $_POST['tipo']==4){
                $html .= '<div class="col-md-3">';
                $html .= $i->qtde;
                $html .= '</div>';
                $html .= '<div class="col-md-3">';
                $html .= date('d/m/Y H:i',strtotime($i->data));
                $html .= '</div>';

//                $html .= '<div class="col-md-3">';
//                $html .= $i->getUser()->nome;
//                $html .= '</div>';

                $html .= '<div class="col-md-3">';
                $html .= $i->getColaborador()->nome;
                $html .= '</div>';
            }else if($_POST['tipo']==2 || $_POST['tipo']==8){
                $html .= '<div class="col-md-3">';
                $html .= $i->qtde;
                $html .= '</div>';
                $html .= '<div class="col-md-3">';
                $html .= $i->numdoc;
                $html .= '</div>';
                $html .= '<div class="col-md-3">';
                $html .= date('d/m/Y H:i',strtotime($i->data));
                $html .= '</div>';

//                $html .= '<div class="col-md-3">';
//                $html .= $i->getUser()->nome;
//                $html .= '</div>';
            }else {
                $html .= '<div class="col-md-3">';
                $html .= $i->qtde;
                $html .= '</div>';
                $html .= '<div class="col-md-5">';
                $html .= date('d/m/Y H:i',strtotime($i->data));
                $html .= '</div>';

//                $html .= '<div class="col-md-4">';
//                $html .= $i->getUser()->nome;
//                $html .= '</div>';
            }
            $html.='<div class="clearfix"></div>';
        }
        $html.='<hr>';
        $html.='<div class="col-md-3">';
        $html.='<strong>'.number_format($total,4,',','.').'</strong>';
        $html.='</div>';
        echo $html;
        exit;
    }

    function findColaboradorMov(){
        $termo = $this->getParam('termo');
        $size = (int)$this->getParam('size');
        $page = (int)$this->getParam('page');


        if(!isset($termo))
            $termo = '';

        if(!isset($size) || $size < 1)
            $size = 10;

        if(!isset($page) || $page < 1)
            $page = 1;


        $db = $this::getConn();
        $sql = "SELECT p.codigo as id, p.nome FROM rhprofissional as p
                WHERE p.status != 3
                AND (p.nome LIKE :nome)";

        $db->query($sql);
        $db->bind(":nome", $termo."%", PDO::PARAM_STR);
        $db->execute();

        $ret = array();
        $ret["total"] = $db->rowCount();
        $ret["dados"] = array();

        $sql = $sql." LIMIT :li OFFSET :off";
        $db->query($sql);

        $db->bind(":nome", $termo."%", PDO::PARAM_STR);
        $db->bind(":li", $size, PDO::PARAM_INT);
        $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

        $dados = $db->getResults();

        foreach($dados as $d){
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }

        echo json_encode($ret);
        exit;
    }

    function getColag(){
        $a = new Criteria();
        $a->addCondition('status','<>',3);
        $ultimo_setado =Session::get('ultimo_setado');
        $colab = $this->query("SELECT P.codigo,P.nome FROM rhprofissional P
                        LEFT JOIN rhprofissional_contratacao C
                        ON C.codigo_profissional = P.codigo
                        WHERE P.status <> 3
                        ORDER BY P.nome ASC ");
        $html="<option value=''>Selecione</option>";
        foreach ($colab as $item) {
            if($item->codigo==$ultimo_setado)
                $html.="<option value='".$item->codigo."' selected>".$item->nome."</option>";
            else
                $html.="<option value='".$item->codigo."'>".$item->nome."</option>";
        }


        echo $html;
        exit;

    }

    function impressao(){
        $this->setTitle('Impressão de cautelas');


        $this->setStyles(SITE_PATH . '/content/css/select2.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2-bootstrap.css');
        $this->setStyles(SITE_PATH . '/content/css/bootstrap-dialog.min.css');


        $this->setScript(SITE_PATH . '/content/js/select2.full.min.js');
        $this->setScript(SITE_PATH . '/content/js/i18n/pt-BR.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.number.min.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-dialog.min.js');

        $tipo = array(
            4=>'Cautelas',
            5=>'Devoluções'
        );

        $this->set('Tipos',$tipo);

        $a = new Criteria();
        $a->addCondition('movimento','=',$_GET['mov']);
        $a->addCondition('tipo','=',$this->getParam('tipo'));
        $a->addCondition('situacao','=',1);

        if(!empty($_GET['colaborador'])){
            $a->addCondition('colaborador','=',$_GET['colaborador']);
        }

        if(!empty($_GET['data'])){

            $a->addCondition('data','LIKE',DataSQL($_GET['data']).'%');
        }

        $a->setOrder('id DESC');
        $list = C_complemento_mov::getList($a);
        $this->set('Itens',$list);

        $a = new Criteria();
        $a->addCondition('status','<>',3);
        $this->set('profissionais',Rhprofissional::getList($a));
    }

    function upd_complemento(){
        $c = new Criteria();
        $c->addCondition('origem','=',Config::get('origem')); //filtro Origem
        $c->addCondition('situacao','<>','3'); //filtro

        $mov = Estq_mov::getList($c);
        foreach ($mov as $m) {
            $a = new Criteria();
            $a->addCondition('movimento','=',$m->id);
            $a->addCondition('origem','=',Config::get('origem')); //filtro Origem
            $mov_detalhes = Estq_movdetalhes::getList($a);
            foreach ($mov_detalhes as $d) {
                $b = new Criteria();
                $b->addCondition('mov_detalhes','=',$d->id);
                $comple = C_complemento_mov::getList($b);
                foreach ($comple as $item) {
                    $c = new C_complemento_mov((int)$item->id);
                    $c->movimento = $m->id;
                    $c->save();
                }
            }
        }
    }

    function getquantidade(){
        $mov_detalhes = new Estq_movdetalhes($_POST['detalhes']);
        $movimento = new Estq_mov($_POST['movimento']);
        $a = new Criteria();
        $a->addCondition('artigo','=',$mov_detalhes->artigo);
        if(!empty($movimento->getEstq_mov()->sub_padrao)) {
            $a->addCondition('subestoque', '=', $movimento->getEstq_mov()->sub_padrao);
        }else{
            $a->addCondition('subestoque', '=', 1);
        }
        $Lote_substoques = Estq_lote_substoque::getFirst($a);
        if($_POST['quantidade']>$Lote_substoques->quantidade){
            echo $Lote_substoques->quantidade;
        }else{
            echo 1;
        }

        exit;
    }
}

