<?php
final class Concessionaria_tipo_servicoController extends AppController{ 

    # página inicial do módulo Concessionaria_tipo_servico
    function index(){
        $this->setTitle('Visualização de Cliente');
    }

    # lista de Concessionaria_tipo_servicos
    # renderiza a visão /view/Concessionaria_tipo_servico/all.php
    function all(){
        $this->setTitle('Listagem de Clientes ');
        $p = new Paginate('Concessionaria_tipo_servico', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        $c->addCondition('origem','=',Config::get('origem'));
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Concessionaria_tipo_servicos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Concessionaria_tipo_servico
    # renderiza a visão /view/Concessionaria_tipo_servico/view.php
    function view(){
        $this->setTitle('Visualização de Cliente');
        try {
            $this->set('Concessionaria_tipo_servico', new Concessionaria_tipo_servico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_tipo_servico', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_tipo_servico
    # renderiza a visão /view/Concessionaria_tipo_servico/add.php
    function add(){
        $this->setTitle('Cadastro de Cliente');
        $this->set('Concessionaria_tipo_servico', new Concessionaria_tipo_servico);
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_tipo_servico
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_tipo_servico/add.php
    function post_add(){
        $this->setTitle('Cadastro de Cliente');
        $Concessionaria_tipo_servico = new Concessionaria_tipo_servico();
        $this->set('Concessionaria_tipo_servico', $Concessionaria_tipo_servico);
        try {
            $Concessionaria_tipo_servico->save($_POST);
            if(!empty($_POST['boot'])){
                echo $Concessionaria_tipo_servico->id;
                exit;
            }
            new Msg(__('cliente cadastrado com sucesso'));
            $this->go('Concessionaria_tipo_servico', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    function load_tipo(){
        $c = new Criteria();
        $c->addCondition('origem','=',Config::get('origem'));
        $tipos = Concessionaria_tipo_servico::getList($c);
        $option = "<option value=''>Selecione</option>";
        foreach ($tipos as $t) {
            if($t->id==$_POST['id'])
                $option .= "<option value='".$t->id."' selected>".$t->nome."</option>";
            else
                $option .= "<option value='".$t->id."' >".$t->nome."</option>";
        }
        echo $option;
        exit;
    }
    # formulário de edição de Concessionaria_tipo_servico
    # renderiza a visão /view/Concessionaria_tipo_servico/edit.php
    function edit(){
        $this->setTitle('Edição de Cliente');
        try {
            $this->set('Concessionaria_tipo_servico', new Concessionaria_tipo_servico((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_tipo_servico', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_tipo_servico
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_tipo_servico/edit.php
    function post_edit(){
        $this->setTitle('Edição de Cliente');
        try {
            $Concessionaria_tipo_servico = new Concessionaria_tipo_servico((int) $_POST['id']);
            $this->set('Concessionaria_tipo_servico', $Concessionaria_tipo_servico);
            $Concessionaria_tipo_servico->save($_POST);
            new Msg(__('cliente atualizado com sucesso'));
            $this->go('Concessionaria_tipo_servico', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_tipo_servico
    # renderiza a /view/Concessionaria_tipo_servico/delete.php
    function delete(){
        $this->setTitle('Apagar Cliente');
        try {
            $this->set('Concessionaria_tipo_servico', new Concessionaria_tipo_servico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_tipo_servico', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_tipo_servico
    # redireciona para Concessionaria_tipo_servico/all
    function post_delete(){
        try {
            $Concessionaria_tipo_servico = new Concessionaria_tipo_servico((int) $_POST['id']);
            $Concessionaria_tipo_servico->situacao = 2;
            $Concessionaria_tipo_servico->save();
            new Msg(__('cliente apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_tipo_servico', 'all');
    }

}