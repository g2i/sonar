<?php
final class ModuloController extends AppController{ 

    # página inicial do módulo Modulo
    function index(){
        $this->setTitle('Visualização de Modulo');
    }

    # lista de Modulos
    # renderiza a visão /view/Modulo/all.php
    function all(){
        $this->setTitle('Listagem de Modulo');
        $p = new Paginate('Modulo', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Modulos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Modulo
    # renderiza a visão /view/Modulo/view.php
    function view(){
        $this->setTitle('Visualização de Modulo');
        try {
            $this->set('Modulo', new Modulo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Modulo', 'all');
        }
    }

    # formulário de cadastro de Modulo
    # renderiza a visão /view/Modulo/add.php
    function add(){
        $this->setTitle('Cadastro de Modulo');
        $this->set('Modulo', new Modulo);
    }

    # recebe os dados enviados via post do cadastro de Modulo
    # (true)redireciona ou (false) renderiza a visão /view/Modulo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Modulo');
        $Modulo = new Modulo();
        $this->set('Modulo', $Modulo);
        try {
            $Modulo->save($_POST);
            new Msg(__('Modulo cadastrado com sucesso'));
            $this->go('Modulo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Modulo
    # renderiza a visão /view/Modulo/edit.php
    function edit(){
        $this->setTitle('Edição de Modulo');
        try {
            $this->set('Modulo', new Modulo((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Modulo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Modulo
    # (true)redireciona ou (false) renderiza a visão /view/Modulo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Modulo');
        try {
            $Modulo = new Modulo((int) $_POST['id']);
            $this->set('Modulo', $Modulo);
            $Modulo->save($_POST);
            new Msg(__('Modulo atualizado com sucesso'));
            $this->go('Modulo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Modulo
    # renderiza a /view/Modulo/delete.php
    function delete(){
        $this->setTitle('Apagar Modulo');
        try {
            $this->set('Modulo', new Modulo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Modulo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Modulo
    # redireciona para Modulo/all
    function post_delete(){
        try {
            $Modulo = new Modulo((int) $_POST['id']);
            $Modulo->delete();
            new Msg(__('Modulo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Modulo', 'all');
    }

}