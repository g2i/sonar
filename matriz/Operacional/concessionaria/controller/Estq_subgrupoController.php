<?php
final class Estq_subgrupoController extends AppController{ 

    # página inicial do módulo Estq_subgrupo
    function index(){
        $this->setTitle('Visualização de Subgrupo');
    }

    # lista de Estq_subgrupos
    # renderiza a visão /view/Estq_subgrupo/all.php
    function all(){
        $this->setTitle('Listagem de Sub-Grupo');
        $p = new Paginate('Estq_subgrupo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('origem','=',Config::get('origem')); //filtro Origem
        $this->set('Estq_subgrupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_grupos',  Estq_grupo::getList());
    

    }

    # visualiza um(a) Estq_subgrupo
    # renderiza a visão /view/Estq_subgrupo/view.php
    function view(){
        $this->setTitle('Visualização de Subgrupo');
        try {
            $this->set('Estq_subgrupo', new Estq_subgrupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_subgrupo', 'all');
        }
    }

    # formulário de cadastro de Estq_subgrupo
    # renderiza a visão /view/Estq_subgrupo/add.php
    function add(){
        $this->setTitle('Cadastro de Sub-Grupo');
        $this->set('Estq_subgrupo', new Estq_subgrupo);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $c = new Criteria();
        $c->addCondition('origem','=',Config::get('origem')); //filtro
        $this->set('Estq_grupos',  Estq_grupo::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Estq_subgrupo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_subgrupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Subgrupo');
        $Estq_subgrupo = new Estq_subgrupo();
        $this->set('Estq_subgrupo', $Estq_subgrupo);
        $_POST['origem']=Config::get('origem');//salvando a Origem
        $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
        $Estq_subgrupo->usuario_id=$user; // para salvar o usuario que está fazendo
        try {
            $Estq_subgrupo->save($_POST);
            new Msg(__('Subgrupo cadastrado com sucesso'));
            $this->go('Estq_subgrupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_grupos',  Estq_grupo::getList());
    }

    # formulário de edição de Estq_subgrupo
    # renderiza a visão /view/Estq_subgrupo/edit.php
    function edit(){
        $this->setTitle('Subgrupo de Sub-Grupo');
        try {
            $this->set('Estq_subgrupo', new Estq_subgrupo((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
            $c = new Criteria();
            $c->addCondition('origem','=',Config::get('origem')); //filtro
            $this->set('Estq_grupos',  Estq_grupo::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_subgrupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_subgrupo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_subgrupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Subgrupo');
        try {
            $Estq_subgrupo = new Estq_subgrupo((int) $_POST['id']);
            $Estq_subgrupo->usuario_dt=date('Y-m-d H:i:s');
            $this->set('Estq_subgrupo', $Estq_subgrupo);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_subgrupo->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_subgrupo->save($_POST);
            new Msg(__('Subgrupo atualizado com sucesso'));
            $this->go('Estq_subgrupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_grupos',  Estq_grupo::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_subgrupo
    # renderiza a /view/Estq_subgrupo/delete.php
    function delete(){
        $this->setTitle('Apagar Subgrupo');
        try {
            $this->set('Estq_subgrupo', new Estq_subgrupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_subgrupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_subgrupo
    # redireciona para Estq_subgrupo/all
    function post_delete(){
        try {
            $Estq_subgrupo = new Estq_subgrupo((int) $_POST['id']);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_subgrupo->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_subgrupo->usuario_dt=date('Y-m-d H:i:s');
            $Estq_subgrupo->situacao=3;
            $Estq_subgrupo->save();
            new Msg(__('Subgrupo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_subgrupo', 'all');
    }

}