<?php
final class Concessionaria_situacaoController extends AppController{ 

    # página inicial do módulo Concessionaria_situacao
    function index(){
        $this->setTitle('Visualização de Situação Movimento');
    }

    # lista de Concessionaria_situacaos
    # renderiza a visão /view/Concessionaria_situacao/all.php
    function all(){
        $this->setTitle('Situação Movimento');
        $p = new Paginate('Concessionaria_situacao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        $c->addCondition('origem','=',Config::get('origem'));
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Concessionaria_situacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Concessionaria_situacao
    # renderiza a visão /view/Concessionaria_situacao/view.php
    function view(){
        $this->setTitle('Visualização de Situação Movimento');
        try {
            $this->set('Concessionaria_situacao', new Concessionaria_situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_situacao', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_situacao
    # renderiza a visão /view/Concessionaria_situacao/add.php
    function add(){
        $this->setTitle('Cadastro de Situação Movimento');
        $this->set('Concessionaria_situacao', new Concessionaria_situacao);
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_situacao
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_situacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Situação Movimento');
        $Concessionaria_situacao = new Concessionaria_situacao();
        $this->set('Concessionaria_situacao', $Concessionaria_situacao);
        try {
            $Concessionaria_situacao->save($_POST);
            new Msg(__('Concessionaria_situacao cadastrado com sucesso'));
            $this->go('Concessionaria_situacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Concessionaria_situacao
    # renderiza a visão /view/Concessionaria_situacao/edit.php
    function edit(){
        $this->setTitle('Edição de Situação Movimento');
        try {
            $this->set('Concessionaria_situacao', new Concessionaria_situacao((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_situacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_situacao
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_situacao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Situação Movimento');
        try {
            $Concessionaria_situacao = new Concessionaria_situacao((int) $_POST['id']);
            $this->set('Concessionaria_situacao', $Concessionaria_situacao);
            $Concessionaria_situacao->save($_POST);
            new Msg(__('Concessionaria_situacao atualizado com sucesso'));
            $this->go('Concessionaria_situacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_situacao
    # renderiza a /view/Concessionaria_situacao/delete.php
    function delete(){
        $this->setTitle('Apagar Concessionaria_situacao');
        try {
            $this->set('Concessionaria_situacao', new Concessionaria_situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_situacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_situacao
    # redireciona para Concessionaria_situacao/all
    function post_delete(){
        try {
            $Concessionaria_situacao = new Concessionaria_situacao((int) $_POST['id']);
            $Concessionaria_situacao->delete();
            new Msg(__('Concessionaria_situacao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_situacao', 'all');
    }

}