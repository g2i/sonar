<?php
final class Concessionaria_componentesController extends AppController{ 

    # página inicial do módulo Concessionaria_componentes
    function index(){
        $this->setTitle('Visualização de Tipos de Componentes');
    }

    # lista de Concessionaria_componentes
    # renderiza a visão /view/Concessionaria_componentes/all.php
    function all(){
        $this->setTitle('Listagem de Tipos de Componentes');
        $p = new Paginate('Concessionaria_componentes', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id','=',1);
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Concessionaria_componentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Concessionaria_componentes
    # renderiza a visão /view/Concessionaria_componentes/view.php
    function view(){
        $this->setTitle('Visualização de Tipos de Componentes');
        try {
            $this->set('Concessionaria_componentes', new Concessionaria_componentes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_componentes', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_componentes
    # renderiza a visão /view/Concessionaria_componentes/add.php
    function add(){
        $this->setTitle('Cadastro de Tipos de Componentes');
        $this->set('Concessionaria_componentes', new Concessionaria_componentes);

    }

    # recebe os dados enviados via post do cadastro de Concessionaria_componentes
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_componentes/add.php
    function post_add(){
        $this->setTitle('Cadastro de Tipos de Componentes');
        $Concessionaria_componentes = new Concessionaria_componentes();
        $this->set('Concessionaria_componentes', $Concessionaria_componentes);
        $Concessionaria_componentes->situacao_id = 1;
        try {
            $Concessionaria_componentes->save($_POST);
            new Msg(__('Concessionaria_componentes cadastrado com sucesso'));
            $this->go('Concessionaria_componentes', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }

    }

    # formulário de edição de Concessionaria_componentes
    # renderiza a visão /view/Concessionaria_componentes/edit.php
    function edit(){
        $this->setTitle('Edição de Concessionaria_componentes');
        try {
            $this->set('Concessionaria_componentes', new Concessionaria_componentes((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_componentes', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_componentes
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_componentes/edit.php
    function post_edit(){
        $this->setTitle('Edição de Concessionaria_componentes');
        try {
            $Concessionaria_componentes = new Concessionaria_componentes((int) $_POST['id']);
            $this->set('Concessionaria_componentes', $Concessionaria_componentes);
            $Concessionaria_componentes->save($_POST);
            new Msg(__('Concessionaria_componentes atualizado com sucesso'));
            $this->go('Concessionaria_componentes', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }

    }

    # Confirma a exclusão ou não de um(a) Concessionaria_componentes
    # renderiza a /view/Concessionaria_componentes/delete.php
    function delete(){
        $this->setTitle('Apagar Concessionaria_componentes');
        try {
            $this->set('Concessionaria_componentes', new Concessionaria_componentes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_componentes', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_componentes
    # redireciona para Concessionaria_componentes/all
    function post_delete(){
        try {
            $Concessionaria_componentes = new Concessionaria_componentes((int) $_POST['id']);
            $Concessionaria_componentes->situacao_id = 3;
            $Concessionaria_componentes->save();
            new Msg(__('Concessionaria_componentes apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_componentes', 'all');
    }

}