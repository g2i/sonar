<?php
final class Concessionaria_andamentoController extends AppController{ 

    # página inicial do módulo Concessionaria_andamento
    function index(){
        $this->setTitle('Visualização de Concessionaria andamento');
    }

    # lista de Concessionaria_andamentos
    # renderiza a visão /view/Concessionaria_andamento/all.php
    function all(){
        $this->setTitle('Listagem de Concessionaria andamento');
        $p = new Paginate('Concessionaria_andamento', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        $c->addCondition('origem','=',Config::get('origem'));
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Concessionaria_andamentos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Concessionaria_andamento
    # renderiza a visão /view/Concessionaria_andamento/view.php
    function view(){
        $this->setTitle('Visualização de Concessionaria andamento');
        try {
            $this->set('Concessionaria_andamento', new Concessionaria_andamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_andamento', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_andamento
    # renderiza a visão /view/Concessionaria_andamento/add.php
    function add(){
        $this->setTitle('Cadastro de Concessionaria andamento');
        $this->set('Concessionaria_andamento', new Concessionaria_andamento);
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_andamento
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_andamento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Concessionaria andamento');
        $Concessionaria_andamento = new Concessionaria_andamento();
        $this->set('Concessionaria_andamento', $Concessionaria_andamento);
        try {
            $Concessionaria_andamento->save($_POST);
            new Msg(__('Concessionaria_andamento cadastrado com sucesso'));
            $this->go('Concessionaria_andamento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Concessionaria_andamento
    # renderiza a visão /view/Concessionaria_andamento/edit.php
    function edit(){
        $this->setTitle('Edição de Concessionaria andamento');
        try {
            $this->set('Concessionaria_andamento', new Concessionaria_andamento((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_andamento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_andamento
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_andamento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Concessionaria andamento');
        try {
            $Concessionaria_andamento = new Concessionaria_andamento((int) $_POST['id']);
            $this->set('Concessionaria_andamento', $Concessionaria_andamento);
            $Concessionaria_andamento->save($_POST);
            new Msg(__('Concessionaria_andamento atualizado com sucesso'));
            $this->go('Concessionaria_andamento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_andamento
    # renderiza a /view/Concessionaria_andamento/delete.php
    function delete(){
        $this->setTitle('Apagar Concessionaria_andamento');
        try {
            $this->set('Concessionaria_andamento', new Concessionaria_andamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_andamento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_andamento
    # redireciona para Concessionaria_andamento/all
    function post_delete(){
        try {
            $Concessionaria_andamento = new Concessionaria_andamento((int) $_POST['id']);
            $Concessionaria_andamento->delete();
            new Msg(__('Concessionaria_andamento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_andamento', 'all');
    }

}