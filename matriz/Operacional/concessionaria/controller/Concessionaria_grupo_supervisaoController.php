<?php

final class Concessionaria_grupo_supervisaoController extends AppController
{

    # página inicial do módulo Concessionaria_grupo_supervisao
    function index()
    {
        $this->setTitle('Visualização do Origem');
    }

    # lista de Concessionaria_grupo_supervisaos
    # renderiza a visão /view/Concessionaria_grupo_supervisao/all.php
    function all()
    {
        $this->setTitle('Listagem dos grupos de supervisão');
        $p = new Paginate('Concessionaria_grupo_supervisao', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id', '=', 1);
        $this->set('Concessionaria_grupo_supervisaos', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Situacaos', Situacao::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Usuarios', Usuario::getList());


    }

    # visualiza um(a) Concessionaria_grupo_supervisao
    # renderiza a visão /view/Concessionaria_grupo_supervisao/view.php
    function view()
    {
        $this->setTitle('Visualização do Origem');
        try {
            $this->set('Concessionaria_grupo_supervisao', new Concessionaria_grupo_supervisao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_grupo_supervisao', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_grupo_supervisao
    # renderiza a visão /view/Concessionaria_grupo_supervisao/add.php
    function add()
    {
        $this->setTitle('Cadastro do Origem');
        $this->set('Concessionaria_grupo_supervisao', new Concessionaria_grupo_supervisao);
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_grupo_supervisao
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_grupo_supervisao/add.php
    function post_add()
    {
        $this->setTitle('Cadastro do Origem');
        $user = Session::get('user');
        $modal = $_POST['modal'];
        $Concessionaria_grupo_supervisao = new Concessionaria_grupo_supervisao();
        $this->set('Concessionaria_grupo_supervisao', $Concessionaria_grupo_supervisao);
        $Concessionaria_grupo_supervisao->criado_por = $user->id;
        $Concessionaria_grupo_supervisao->dt_criacao = date("Y-m-d H:i:s");
        $Concessionaria_grupo_supervisao->situacao_id = 1;

        try {
            $numFile = count(array_filter($_FILES['url']['name']));
            $file = $_FILES['url'];
            for ($i = 0; $i < $numFile; $i++) {
                $dados = array(
                    "name" => $file['name'][$i],
                    "type" => $file['type'][$i],
                    "tmp_name" => $file['tmp_name'][$i],
                    "error" => $file['error'][$i],
                    "size" => $file['size'][$i]
                );

                $extensions = array('jpg', 'png');

                $file_uploader = new FileUploader($dados, null, $extensions);
                $name = time() . tratar_arquivos($dados['name']);
                $path = "origem";
                if (empty($file_uploader->erro)) {
                    if ($file_uploader->save($name, $path)) {
                        $_POST['url'] = $file_uploader->path . '/' . $file_uploader->name;
                    }
                }
            }

            $Concessionaria_grupo_supervisao->save($_POST);
            new Msg(__('Origem cadastrado com sucesso'));
            $this->go('Concessionaria_grupo_supervisao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    function delete_anexo()
    {
        $Concessionaria = new Concessionaria_grupo_supervisao((int)$this->getParam('id'));
        unlink(SITE_PATH.'/' . $Concessionaria->url);
        $file['url'] = '';
        $Concessionaria->save($file);
        echo json_encode(1);
        exit();
    }

    # formulário de edição de Concessionaria_grupo_supervisao
    # renderiza a visão /view/Concessionaria_grupo_supervisao/edit.php
    function edit()
    {
        $this->setTitle('Edição da Origem');
        try {

            $this->set('Concessionaria_grupo_supervisao', new Concessionaria_grupo_supervisao((int)$this->getParam('id')));


        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Concessionaria_grupo_supervisao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_grupo_supervisao
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_grupo_supervisao/edit.php
    function post_edit()
    {
        $this->setTitle('Edição do Origem');
        try {
            $Concessionaria_grupo_supervisao = new Concessionaria_grupo_supervisao((int)$_POST['id']);
            $user = Session::get('user');
            $Concessionaria_grupo_supervisao->modificado_por = $user->id;
            $Concessionaria_grupo_supervisao->dt_modificacao = date("Y-m-d H:i:s");

            if (!empty($_FILES['caminho']['name'])) {

                if (!empty($Concessionaria_grupo_supervisao->url)) {
                    unlink(SITE_PATH .'/'.$Concessionaria_grupo_supervisao->url);
                }

                $numFile = count(array_filter($_FILES['caminho']['name']));
                $file = $_FILES['caminho'];


                for ($i = 0; $i < $numFile; $i++) {
                    $dados = array(
                        "name" => $file['name'][$i],
                        "type" => $file['type'][$i],
                        "tmp_name" => $file['tmp_name'][$i],
                        "error" => $file['error'][$i],
                        "size" => $file['size'][$i]
                    );
                    $extensions = explode(',', 'jpg,jpeg,png,gif');

                    $file_uploader = new FileUploader($dados, null, $extensions);
                    $name = time() . tratar_arquivos($dados['name']);
                    $path = "origem";
                    if (empty($file_uploader->erro)) {
                        if ($file_uploader->save($name, $path)) {
                            $_POST['url'] = $file_uploader->path . '/' . $file_uploader->name;
                            $Concessionaria_grupo_supervisao->save($_POST);
                        }
                    }
                }
            } else {
                $_POST['url'] = $Concessionaria_grupo_supervisao->url;
                $Concessionaria_grupo_supervisao->save($_POST);
            }

            $this->set('Concessionaria_grupo_supervisao', $Concessionaria_grupo_supervisao);
            new Msg(__('Grupo supervisao atualizado com sucesso'));
            $this->go('Concessionaria_grupo_supervisao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_grupo_supervisao
    # renderiza a /view/Concessionaria_grupo_supervisao/delete.php
    function delete()
    {
        $this->setTitle('Apagar Origem');
        try {
            $this->set('Concessionaria_grupo_supervisao', new Concessionaria_grupo_supervisao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_grupo_supervisao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_grupo_supervisao
    # redireciona para Concessionaria_grupo_supervisao/all
    function post_delete()
    {
        try {
            $Concessionaria_grupo_supervisao = new Concessionaria_grupo_supervisao((int)$_POST['id']);
            $Concessionaria_grupo_supervisao->situacao_id = 3;
            $Concessionaria_grupo_supervisao->save();
            new Msg(__('Concessionaria_grupo_supervisao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Concessionaria_grupo_supervisao', 'all');
    }

}