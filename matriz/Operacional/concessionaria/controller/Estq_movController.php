<?php

final class Estq_movController extends AppController
{

    # página inicial do módulo Estq_mov
    function index()
    {
        $this->setTitle('Visualização de O.S');
    }

    # lista de Estq_movs
    # renderiza a visão /view/Estq_mov/all.php
    function all()
    {
        $this->setTitle('Listagem de O.S');
        $p = new Paginate('Estq_mov', 20);
        $Estq_movsCriteria = new Criteria();
        $this->set('sort', 'ASC');
        $Estq_movsCriteria->addJoin('estq_movdetalhes', 'estq_movdetalhes.movimento', 'estq_mov.id', 'LEFT');
        $Estq_movsCriteria->addJoin('estq_artigo', 'estq_movdetalhes.artigo', 'estq_artigo.id', 'LEFT');
        $Estq_movsCriteria->addJoin('c_complemento_mov', 'c_complemento_mov.mov_detalhes', 'estq_movdetalhes.id', 'LEFT');
        $Estq_movsCriteria->setCont("DISTINCT(estq_mov.`id`)");

        if (!empty($_GET["filtro"])) {
            if (!empty($_GET["filtro"]["interno"])) {
                foreach ($_GET["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $Estq_movsCriteria->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }

            if (!empty($_GET["filtro"]["externo"])) {
                foreach ($_GET["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $Estq_movsCriteria->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        if (!empty($_GET['data_cautela'])) {
            $this->setParam('data_cautela', $_GET['data_cautela']);
            $Estq_movsCriteria->addSqlConditions(" DATE_FORMAT(c_complemento_mov.`data`,'%d/%m/%Y') = '" . $_GET['data_cautela'] . "'");
        }

        if (!empty($_GET['inicio']) && !empty($_GET['fim'])) {
            $tipoData = $_GET['tipo_data'];
            $inicio = DataSQL($_GET['inicio']);
            $fim = DataSQL($_GET['fim']);
            $Estq_movsCriteria->addCondition($tipoData, ">=", $inicio);
            $Estq_movsCriteria->addCondition($tipoData, "<=", $fim);
        }

        $Estq_movsCriteria->setGroup('estq_mov.id');

        if ($this->getParam('orderBy')) {
            if ($this->getParam('sort') == 'ASC') {
                $this->set('sort', 'DESC');
            } else {
                $this->set('sort', 'ASC');
            }
            $Estq_movsCriteria->setOrder($this->getParam('orderBy') . ' ' . $this->getParam('sort'));
        } else {
            $Estq_movsCriteria->setOrder('id desc');
        }

        $Estq_movsCriteria->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $Estq_movsCriteria->addCondition('situacao', '=', 1); //filtro

        $ClientesCriteria = new Criteria();
        $ClientesCriteria->addCondition('status', '=', 1);
        $ClientesCriteria->setOrder('nome');

        $FornecedorsCriteria = new Criteria();
        $FornecedorsCriteria->addCondition('situacao', '=', 1);
        $FornecedorsCriteria->setOrder('nome');

        $Estq_tipomovsCriteria = new Criteria();
        $Estq_tipomovsCriteria->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $Estq_tipomovsCriteria->addCondition('situacao', '=', 1); //filtro
        $Estq_tipomovsCriteria->setOrder('nome');

        $RhprofissionalCriteria = new Criteria();
        $RhprofissionalCriteria->addCondition('status', '=', 1);
        $RhprofissionalCriteria->setOrder('nome');

        $queryAndamento = new Criteria();
        $queryAndamento->addCondition('situacao', '=', 1);
        $queryAndamento->addCondition('origem', '=', Config::get('origem')); //filtro Origem

        $queryConcessionariaSituacao = new Criteria();
        $queryConcessionariaSituacao->addCondition('situacao', '=', 1);
        $queryConcessionariaSituacao->addCondition('origem', '=', Config::get('origem')); //filtro Origem

        $equipeCondition = new Criteria();
        $equipeCondition->addCondition('situacao', '=', 1);
        $tipoServicoCondition = new Criteria();
        $tipoServicoCondition->addCondition('situacao', '=', 1);
        $tipoServicoCondition->setOrder('nome');

        $this->set('Equipes', Equipe::getList($equipeCondition));
        $this->set('Clientes', Cliente::getList($ClientesCriteria));
        $this->set('TiposServicos', Concessionaria_tipo_servico::getList($tipoServicoCondition));
        $this->set('Fornecedors', Fornecedor::getList($FornecedorsCriteria));
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList($Estq_tipomovsCriteria));
        $this->set('Rhprofissionals', Rhprofissional::getList($RhprofissionalCriteria));
        $this->set('ConcessionariAandamento', Concessionaria_andamento::getList($queryAndamento));
        $this->set('ConcessionariaSituacao', Concessionaria_situacao::getList($queryConcessionariaSituacao));
        $this->set('Estq_movs', $p->getPage($Estq_movsCriteria));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Estq_mov
    # renderiza a visão /view/Estq_mov/view.php
    function view()
    {
        $this->setTitle('Visualização de O.S');

        try {
            $this->set('Estq_mov', new Estq_mov((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_mov', 'all');
        }
    }

    # formulário de cadastro de Estq_mov
    # renderiza a visão /view/Estq_mov/add.php
    function add()
    {
        $this->setTitle('Cadastro de O.S');


        $a = new Criteria();
        $a->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $a->setOrder('nome');
        $a->addCondition('situacao', '<>', 3);

        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $b->setOrder('nome');

        $c = new Criteria();
        $c->setOrder('nome');
        $c->addCondition('status', '<>', 3);

        $d = new Criteria();
        $d->addCondition('status', '<>', 3);
        $d->setOrder('nome');

        $queryVeiculo = new Criteria();
        $queryVeiculo->addCondition('situacao_id', '=', 1);
        $queryVeiculo->addSqlConditions('frota_veiculos.localizacao_id NOT IN (12,13,14)');
        $queryVeiculo->setOrder('placa');
        $this->set('Frota_veiculo', Frota_veiculos::getList($queryVeiculo));

        $eletricista = $this->query("SELECT DISTINCT (rhprofissional_contratacao.codigo_profissional),rhprofissional.nome
                                        FROM rhprofissional_contratacao
                                        INNER JOIN rhprofissional
                                        ON rhprofissional_contratacao.codigo_profissional = rhprofissional.codigo
                                        WHERE  rhprofissional_contratacao.tipo = 'Admissão'
                                        AND rhprofissional.rhlocalidade_id = 1
                                        ORDER BY rhprofissional.nome ASC");

        $this->set('eletricista', $eletricista);

        $equipeCondition = new Criteria();
        $equipeCondition->addCondition('situacao', '=', 1);

        $queryMotivoRetirada = new Criteria();
        $queryMotivoRetirada->addCondition('situacao_id', '=', 1);
        $queryComponente = new Criteria();
        $queryComponente->addCondition('situacao_id', '=', 1);

        $queryGrupoSupervisao = new Criteria();
        $queryGrupoSupervisao->addCondition('situacao_id', '=', 1);
        $queryGrupoSupervisao->setOrder('nome ASC');

        $this->set('GrupoSupervisao', Concessionaria_grupo_supervisao::getList($queryGrupoSupervisao));
        $this->set('componentes', Concessionaria_componentes::getList($queryComponente));
        $this->set('MotivoRetirada', Concessionaria_motivo_retirada::getList($queryMotivoRetirada));
        $this->set('Equipes', Equipe::getList($equipeCondition));
        $this->set('Estq_mov', new Estq_mov);
        $this->set('Clientes', Cliente::getList($c));
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList($a));
        $this->set('Fornecedors', Fornecedor::getList($b));
        $this->set('Rhprofissionals', Rhprofissional::getList($d));

        $m = new Criteria();
        $m->addCondition('situacao', '<>', 3);
        $m->setOrder('cidade ASC');
        $this->set('Municipios', Concessionaria_cidade::getList($m));

        $d = new Criteria();
        $d->addCondition('situacao', '<>', 3);
        $d->addCondition('origem', '=', Config::get('origem'));
        $d->setOrder('nome');

        $this->set('Tipo_servico', Concessionaria_tipo_servico::getList($d));

        $d = new Criteria();
        $d->addCondition('situacao', '<>', 3);
        $d->addCondition('origem', '=', Config::get('origem'));
        $d->setOrder('nome');

        $this->set('Andamento', Concessionaria_andamento::getList($d));

        $d = new Criteria();
        $d->addCondition('situacao', '<>', 3);
        $d->addCondition('origem', '=', Config::get('origem'));
        $d->setOrder('nome');

        $this->set('Situacao_movimento', Concessionaria_situacao::getList($d));

        $d = new Criteria();
        $d->addCondition('situacao', '<>', 3);
        $d->addCondition('origem', '=', Config::get('origem'));
        $d->setOrder('nome');

        $queryCidades = new Criteria();
        $queryCidades->addCondition('situacao', '=', 1);
        $this->set('cidades', Concessionaria_cidade::getList($queryCidades));
        $this->set('Equipes', Equipe::getList($d));

    }

    # recebe os dados enviados via post do cadastro de Estq_mov
    # (true)redireciona ou (false) renderiza a visão /view/Estq_mov/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de O.S');

        $Estq_mov = new Estq_mov();
        $frota = new Frota_veiculos((int)$_POST['placa_veiculo_id']);
        $this->set('Estq_mov', $Estq_mov);
        $Estq_mov->usuario_dt = date('Y-m-d H:i:s');

        $_POST['origem'] = Config::get('origem');//salvando a origem

        if ($_POST['datadoc'] != NULL) {
            $_POST['datadoc'] = DataSQL($_POST['datadoc']); //converte para o formato americano
        }

        if ($_POST['data'] != NULL) {
            $_POST['data'] = DataSQL($_POST['data']);
        }

        if (!empty($_POST['placa_veiculo_id'])) {
            $_POST['placa_veiculo'] = $frota->placa;
        }

        if (!empty($_POST['desl_inicial'])) {
            $_POST['desl_inicial'] = ConvertDateTime($_POST['desl_inicial']);
        }

        if (!empty($_POST['desl_final'])) {
            $_POST['desl_final'] = ConvertDateTime($_POST['desl_final']);
        }

        if (!empty($_POST['data_carta'])) {
            $_POST['data_carta'] = DataSQL($_POST['data_carta']);
        }

        if (!empty($_POST['data_medicao'])) {
            $_POST['data_medicao'] = DataSQL($_POST['data_medicao']);
            $Estq_mov->prazo_inventario = somar_dias_uteis($_POST['data_medicao'], 5);
        }
        //***** */
        if (!empty($_POST['data_execucao01'])) {
            $_POST['data_execucao01'] = ConvertDateTime($_POST['data_execucao01']);
        }
        if (!empty($_POST['data_execucao02'])) {
            $_POST['data_execucao02'] = ConvertDateTime($_POST['data_execucao02']);
        }
        if (!empty($_POST['data_execucao03'])) {
            $_POST['data_execucao03'] = ConvertDateTime($_POST['data_execucao03']);
        }
        if (!empty($_POST['data_tci'])) {
            $_POST['data_tci'] = ConvertDateTime($_POST['data_tci']);
        }


        if (!empty($_POST['data_recebimento'])) {
            $_POST['data_recebimento'] = DataSQL($_POST['data_recebimento']);
        }

        if (!empty($_POST['valor_recebimento'])) {
            $_POST['valor_recebimento'] = str_replace(',', '', $_POST['valor_recebimento']);
        }

        if (!empty($_POST['valor_os_final'])) {
            $_POST['valor_os_final'] = str_replace(".", "", $_POST['valor_os_final']);
            $_POST['valor_os_final'] = str_replace(',', '.', $_POST['valor_os_final']);
        }

        if (!empty($_POST['data_recebimento2'])) {
            $_POST['data_recebimento2'] = DataSQL($_POST['data_recebimento2']);
        }

        if (!empty($_POST['valor_recebimento2'])) {
            $_POST['valor_recebimento2'] = str_replace('.', '', $_POST['valor_recebimento2']);
            $_POST['valor_recebimento2'] = str_replace(',', '.', $_POST['valor_recebimento2']);
        }

        if (!empty($_POST['data_recebimento3'])) {
            $_POST['data_recebimento3'] = DataSQL($_POST['data_recebimento3']);
        }

        if (!empty($_POST['valor_recebimento3'])) {
            $_POST['valor_recebimento3'] = str_replace(',', '', $_POST['valor_recebimento3']);
        }

        if (!empty($_POST['data_recebimento4'])) {
            $_POST['data_recebimento4'] = DataSQL($_POST['data_recebimento4']);
        }

        if (!empty($_POST['valor_recebimento4'])) {
            $_POST['valor_recebimento4'] = str_replace(',', '', $_POST['valor_recebimento4']);
        }
        if (!empty($_POST['viabilizado'])) {
            $_POST['viabilizado'] = ConvertDateTime($_POST['viabilizado']);
        }
        if (!empty($_POST['hora_inicial'])) {
            $_POST['hora_inicial'] = $_POST['hora_inicial'];
        }
        if (!empty($_POST['hora_final'])) {
            $_POST['hora_final'] = $_POST['hora_final'];
        }
        if ($_POST['requer'] != NULL) {
            if ($_POST['requer'] == "Fornecedor") {
                $_POST['colaborador'] = null;
                $_POST['cliente'] = null;
            } elseif ($_POST['requer'] == "Colaborador") {
                $_POST['fornecedor'] = null;
                $_POST['cliente'] = null;
            } elseif ($_POST['requer'] == "Cliente") {
                $_POST['fornecedor'] = null;
                $_POST['colaborador'] = null;
            }
        }

        $_POST['status'] = 1;
        $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
        $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo

        try {
            if ($_POST['cliente'] != null) {
                if ($Estq_mov->save($_POST)) {
                    $id = $Estq_mov->id;
                    if (!empty($_POST['equipe'])) {
                        foreach ($_POST['equipe'] as $item) {
                            $movimento_equipe = new Equipe_movimento();
                            $movimento_equipe->equipe_id = $item;
                            $movimento_equipe->movimento_id = $id;
                            $movimento_equipe->origem = Config::get('origem');
                            $movimento_equipe->situacao = 1;
                            $movimento_equipe->save();
                        }
                    }
                }

                new Msg(__('O.S cadastrado com sucesso'));
                $this->go('Estq_mov', 'edit', array('id' => $Estq_mov->id));
            } else {
                new Msg('Informe um Cliente , Fornecedor ou Colaborador !', 3);
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }

        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList());
        $this->set('Fornecedors', Fornecedor::getList($b));
    }

    function addAjax()
    {
        try {
            $ret = array();
            $ret['result'] = false;

            $Estq_mov = new Estq_mov();
            $Estq_mov->usuario_dt = date('Y-m-d H:i:s');
            $_POST['origem'] = Config::get('origem');//salvando a origem

            if ($_POST['datadoc'] != NULL) {
                $_POST['datadoc'] = DataSQL($_POST['datadoc']); //converte para o formato americano
                $Estq_mov->prazo_viabilizado = somar_dias_uteis($_POST['datadoc'], 5);
                //add 5 dias para  prazo_viabilizado

            }

            if ($_POST['data'] != NULL) {
                $_POST['data'] = DataSQL($_POST['data']);
            }

            if (!empty($_POST['desl_inicial'])) {
                $_POST['desl_inicial'] = ConvertDateTime($_POST['desl_inicial']);
            }

            if (!empty($_POST['desl_final'])) {
                $_POST['desl_final'] = ConvertDateTime($_POST['desl_final']);
            }

            if (!empty($_POST['data_execucao01'])) {
                $_POST['data_execucao01'] = DataSQL($_POST['data_execucao01']);
            }
            if (!empty($_POST['data_execucao02'])) {
                $_POST['data_execucao02'] = DataSQL($_POST['data_execucao02']);
            }
            if (!empty($_POST['data_execucao03'])) {
                $_POST['data_execucao03'] = DataSQL($_POST['data_execucao03']);
            }
            if (!empty($_POST['data_tci'])) {
                $_POST['data_tci'] = DataSQL($_POST['data_tci']);
            }

            if (!empty($_POST['data_carta'])) {
                $_POST['data_carta'] = DataSQL($_POST['data_carta']);
            }

            if (!empty($_POST['data_medicao'])) {
                $_POST['data_medicao'] = DataSQL($_POST['data_medicao']);
                $Estq_mov->prazo_inventario = somar_dias_uteis($_POST['data_medicao'], 5);
                $Estq_mov->envio_inventario = somar_dias_uteis($_POST['data_medicao'], 10);
                $Estq_mov->medicao_concessionaria = somar_dias_uteis($_POST['data_medicao'], 15);
            }

            if (!empty($_POST['valor_os_final'])) {
                $_POST['valor_os_final'] = str_replace(".", "", $_POST['valor_os_final']);
                $_POST['valor_os_final'] = str_replace(',', '.', $_POST['valor_os_final']);
            }

            if (!empty($_POST['data_recebimento'])) {
                $_POST['data_recebimento'] = DataSQL($_POST['data_recebimento']);
            }

            if (!empty($_POST['valor_recebimento'])) {
                $_POST['valor_recebimento'] = str_replace('.', '', $_POST['valor_recebimento']);
                $_POST['valor_recebimento'] = str_replace(',', '.', $_POST['valor_recebimento']);
            }

            if (!empty($_POST['data_recebimento2'])) {
                $_POST['data_recebimento2'] = DataSQL($_POST['data_recebimento2']);
            }

            if (!empty($_POST['valor_recebimento2'])) {
                $_POST['valor_recebimento2'] = str_replace('.', '', $_POST['valor_recebimento2']);
                $_POST['valor_recebimento2'] = str_replace(',', '.', $_POST['valor_recebimento2']);
            }

            if (!empty($_POST['data_recebimento3'])) {
                $_POST['data_recebimento3'] = DataSQL($_POST['data_recebimento3']);
            }

            if (!empty($_POST['valor_recebimento3'])) {
                $_POST['valor_recebimento3'] = str_replace('.', '', $_POST['valor_recebimento3']);
                $_POST['valor_recebimento3'] = str_replace(',', '.', $_POST['valor_recebimento3']);
            }

            if (!empty($_POST['data_recebimento4'])) {
                $_POST['data_recebimento4'] = DataSQL($_POST['data_recebimento4']);
            }

            if (!empty($_POST['valor_recebimento4'])) {
                $_POST['valor_recebimento4'] = str_replace('.', '', $_POST['valor_recebimento4']);
                $_POST['valor_recebimento4'] = str_replace(',', '.', $_POST['valor_recebimento4']);
            }
            if (!empty($_POST['valor_os_contratada'])) {
                $_POST['valor_os_contratada'] = str_replace(".", "", $_POST['valor_os_contratada']);
                $_POST['valor_os_contratada'] = str_replace(',', '.', $_POST['valor_os_contratada']);
            }
            if (!empty($_POST['viabilizado'])) {
                $_POST['viabilizado'] = ConvertDateTime($_POST['viabilizado']);
            }


            $_POST['status'] = 1;
            $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo


            if ($_POST['cliente'] != null) {
                if ($Estq_mov->save($_POST)) {
                    $id = $Estq_mov->id;
                    if (!empty($_POST['equipe'])) {
                        foreach ($_POST['equipe'] as $item) {
                            $movimento_equipe = new Equipe_movimento();
                            $movimento_equipe->equipe_id = $item;
                            $movimento_equipe->movimento_id = $id;
                            $movimento_equipe->origem = Config::get('origem');
                            $movimento_equipe->situacao = 1;
                            $movimento_equipe->save();
                        }
                    }
                }

                $ret['result'] = true;
                $ret['msg'] = 'Salvo com sucesso';
                $ret['id'] = $Estq_mov->id;
            } else {
                $ret['result'] = false;
                $ret['msg'] = 'Informe um Cliente , Fornecedor ou Colaborador !';
                throw new Exception('Informe um Cliente , Fornecedor ou Colaborador !');
            }
        } catch (Exception $e) {
            $ret['result'] = false;
            $ret['msg'] = $e->getMessage();
            $ret['error'] = $e->getMessage();
            $ret['trace'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    # formulário de edição de Estq_mov
    # renderiza a visão /view/Estq_mov/edit.php
    function edit()
    {
        $this->setTitle('Edição de O.S');


        try {
            $estq_mov = new Estq_mov((int)$this->getParam('id'));
            $this->set('Estq_mov', $estq_mov);

            $a = new Criteria();
            $a->addCondition('origem', '=', Config::get('origem')); //filtro Origem

            $b = new Criteria();
            $b->setOrder('nome');
            $b->addCondition('situacao', '<>', 3);

            $c = new Criteria();
            $c->addCondition('status', '<>', 3);
            $c->setOrder('nome');

            $d = new Criteria();
            $d->addCondition('status', '<>', 3);
            $d->setOrder('nome');

            $queryVeiculo = new Criteria();
            $queryVeiculo->addCondition('situacao_id', '=', 1);
            $queryVeiculo->addSqlConditions('frota_veiculos.localizacao_id NOT IN (12,13,14)');
            $queryVeiculo->setOrder('placa');
            $this->set('Frota_veiculo', Frota_veiculos::getList($queryVeiculo));


            $equipeCondition = new Criteria();
            $equipeCondition->addCondition('situacao', '=', 1);

            $queryMotivoRetirada = new Criteria();
            $queryMotivoRetirada->addCondition('situacao_id', '=', 1);

            $queryComponente = new Criteria();
            $queryComponente->addCondition('situacao_id', '=', 1);
            $queryGrupoSupervisao = new Criteria();
            $queryGrupoSupervisao->addCondition('situacao_id', '=', 1);
            $queryGrupoSupervisao->setOrder('nome ASC');

            $this->set('GrupoSupervisao', Concessionaria_grupo_supervisao::getList($queryGrupoSupervisao));
            $this->set('componentes', Concessionaria_componentes::getList($queryComponente));
            $this->set('MotivoRetirada', Concessionaria_motivo_retirada::getList($queryMotivoRetirada));
            $this->set('Equipes', Equipe::getList($equipeCondition));
            $this->set('Clientes', Cliente::getList($c));
            $this->set('Fornecedors', Fornecedor::getList($b));
            $this->set('Estq_situacaos', Estq_situacao::getList());
            $this->set('Estq_tipomovs', Estq_tipomov::getList($a));
            $this->set('Rhprofissionals', Rhprofissional::getList($d));

            //localidade futuramente sera dinamica dependendo do login
//            $queryContraFunc = new Criteria();
//            $queryContraFunc->addSqlConditions('rhprofissional_contratacao.funcao IN (99938,99912,9994,9992,31,30,29,17,12,11,10,3,2)');
//            $queryContraFunc->addCondition('tipo','=','Admissão');
            //1 - igual matriz campo grande ms


            $eletricista = $this->query("SELECT DISTINCT (rhprofissional_contratacao.codigo_profissional),rhprofissional.nome
                                        FROM rhprofissional_contratacao
                                        INNER JOIN rhprofissional
                                        ON rhprofissional_contratacao.codigo_profissional = rhprofissional.codigo
                                        WHERE  rhprofissional_contratacao.tipo = 'Admissão'
                                        AND rhprofissional.rhlocalidade_id = 1
                                        ORDER BY rhprofissional.nome ASC");

            $this->set('eletricista', $eletricista);

            $d = new Criteria();
            $d->addCondition('situacao', '<>', 3);
            $d->addCondition('origem', '=', Config::get('origem'));
            $d->setOrder('nome');

            $m = new Criteria();
            $m->addCondition('situacao', '<>', 3);
            $m->setOrder('cidade ASC');
            $this->set('Municipios', Concessionaria_cidade::getList($m));

            $this->set('Tipo_servico', Concessionaria_tipo_servico::getList($d));

            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$this->getParam('id'));


            $item_serviço = Concessionaria_item_servico::getList($queryFatorFunc);
            $total = 0;
            foreach ($item_serviço AS $se) {
                $total += $se->valor_total;
            }
            $this->set('valor_total_OS', $total);


            $d = new Criteria();
            $d->addCondition('situacao', '<>', 3);
            $d->addCondition('origem', '=', Config::get('origem'));
            $d->setOrder('nome');

            $this->set('Andamento', Concessionaria_andamento::getList($d));

            $d = new Criteria();
            $d->addCondition('situacao', '<>', 3);
            $d->addCondition('origem', '=', Config::get('origem'));
            $d->setOrder('nome');

            $this->set('Situacao_movimento', Concessionaria_situacao::getList($d));

            $queryCidades = new Criteria();
            $queryCidades->addCondition('supervisao_id', '=', $estq_mov->grupo_supervisao_id);
            $queryCidades->addCondition('situacao', '=', 1);
            $this->set('cidades', Concessionaria_cidade::getList($queryCidades));

            $enderecoCriteria =  new Criteria();
            $enderecoCriteria->addCondition('cliente_id','=',$estq_mov->cliente);
            $this->set('endereco', Cliente_endereco::getList($enderecoCriteria));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Estq_mov', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_mov
    # (true)redireciona ou (false) renderiza a visão /view/Estq_mov/edit.php
    function post_edit()
    {
        $this->setTitle('Edição de O.S');

        try {
            $Estq_mov = new Estq_mov((int)$_POST['id']);
            $this->set('Estq_mov', $Estq_mov);

            if ($_POST['datadoc'] != NULL) {
                $_POST['datadoc'] = DataSQL($_POST['datadoc']); //converte para o formato americano
                $Estq_mov->prazo_viabilizado = somar_dias_uteis($_POST['datadoc'], 5);
            }

            if ($_POST['data'] != NULL) {
                $_POST['data'] = ConvertDateTime($_POST['data']);
            }

            if (!empty($_POST['desl_inicial'])) {
                $_POST['desl_inicial'] = ConvertDateTime($_POST['desl_inicial']);
            }

            if (!empty($_POST['desl_final'])) {
                $_POST['desl_final'] = ConvertDateTime($_POST['desl_final']);
            }

            if (!empty($_POST['data_carta'])) {
                $_POST['data_carta'] = DataSQL($_POST['data_carta']);
            }

            if (!empty($_POST['data_medicao'])) {
                $_POST['data_medicao'] = DataSQL($_POST['data_medicao']);
                $Estq_mov->prazo_inventario = somar_dias_uteis($_POST['data_medicao'], 5);
                $Estq_mov->envio_inventario = somar_dias_uteis($_POST['data_medicao'], 10);
                $Estq_mov->medicao_concessionaria = somar_dias_uteis($_POST['data_medicao'], 15);
            }

            //valor os final

            if (!empty($_POST['data_recebimento'])) {
                $_POST['data_recebimento'] = DataSQL($_POST['data_recebimento']);
            }

            if (!empty($_POST['valor_recebimento'])) {
                $_POST['valor_recebimento'] = str_replace('.', '', $_POST['valor_recebimento']);
                $_POST['valor_recebimento'] = str_replace(',', '.', $_POST['valor_recebimento']);
            }

            if (!empty($_POST['data_recebimento2'])) {
                $_POST['data_recebimento2'] = DataSQL($_POST['data_recebimento2']);
            }

            if (!empty($_POST['valor_recebimento2'])) {
                $_POST['valor_recebimento2'] = str_replace('.', '', $_POST['valor_recebimento2']);
                $_POST['valor_recebimento2'] = str_replace(',', '.', $_POST['valor_recebimento2']);
            }

            if (!empty($_POST['data_recebimento3'])) {
                $_POST['data_recebimento3'] = DataSQL($_POST['data_recebimento3']);
            }

            if (!empty($_POST['valor_recebimento3'])) {
                $_POST['valor_recebimento3'] = str_replace(".", "", $_POST['valor_recebimento3']);
                $_POST['valor_recebimento3'] = str_replace(',', '.', $_POST['valor_recebimento3']);
            }
            if (!empty($_POST['valor_os_contratada'])) {
                $_POST['valor_os_contratada'] = str_replace(".", "", $_POST['valor_os_contratada']);
                $_POST['valor_os_contratada'] = str_replace(',', '.', $_POST['valor_os_contratada']);
            }
            if (!empty($_POST['valor_os_final'])) {
                $_POST['valor_os_final'] = str_replace(".", "", $_POST['valor_os_final']);
                $_POST['valor_os_final'] = str_replace(',', '.', $_POST['valor_os_final']);
            }
            if (!empty($_POST['data_recebimento4'])) {
                $_POST['data_recebimento4'] = DataSQL($_POST['data_recebimento4']);
            }

            if (!empty($_POST['valor_recebimento4'])) {
                $_POST['valor_recebimento4'] = str_replace(".", "", $_POST['valor_recebimento4']);
                $_POST['valor_recebimento4'] = str_replace(',', '.', $_POST['valor_recebimento4']);
            }

            if (!empty($_POST['data_execucao01'])) {
                $_POST['data_execucao01'] = DataSQL($_POST['data_execucao01']);
            }
            if (!empty($_POST['data_execucao02'])) {
                $_POST['data_execucao02'] = DataSQL($_POST['data_execucao02']);
            }
            if (!empty($_POST['data_execucao03'])) {
                $_POST['data_execucao03'] = DataSQL($_POST['data_execucao03']);
            }
            if (!empty($_POST['data_tci'])) {
                $_POST['data_tci'] = DataSQL($_POST['data_tci']);
            }
            if (!empty($_POST['viabilizado'])) {
                $_POST['viabilizado'] = ConvertDateTime($_POST['viabilizado']);
            }

            $Estq_mov->usuario_dt = date('Y-m-d H:i:s');


            $_POST['origem'] = Config::get('origem');//salvando a origem
            $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo
            if ($_POST['cliente'] != null) {
                $Estq_mov->save($_POST);
                new Msg(__('O.S atualizado com sucesso'));
                $this->go('Estq_mov', 'all');
            } else {
                new Msg('Informe um Cliente , Fornecedor ou Colaborador !', 3);
            }

        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }

        $b = new Criteria();
        $b->addCondition('situacao', '<>', 3);

        $this->set('Fornecedors', Fornecedor::getList($b));
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList());
        $this->set('Rhprofissionals', Rhprofissional::getList());
    }

    function editAjax()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Estq_mov = new Estq_mov((int)$_POST['id']);
            $this->set('Estq_mov', $Estq_mov);
            if ($_POST['datadoc'] != NULL) {
                $_POST['datadoc'] = DataSQL($_POST['datadoc']); //converte para o formato americano
                $Estq_mov->prazo_viabilizado = somar_dias_uteis($_POST['datadoc'], 5);
            }

            if ($_POST['data'] != NULL) {
                $_POST['data'] = ConvertDateTime($_POST['data']);
            }

            if (!empty($_POST['desl_inicial'])) {
                $_POST['desl_inicial'] = ConvertDateTime($_POST['desl_inicial']);
            }

            if (!empty($_POST['desl_final'])) {
                $_POST['desl_final'] = ConvertDateTime($_POST['desl_final']);
            }

            if (!empty($_POST['data_carta'])) {
                $_POST['data_carta'] = DataSQL($_POST['data_carta']);
            }

            if (!empty($_POST['data_medicao'])) {
                $_POST['data_medicao'] = DataSQL($_POST['data_medicao']);
                $Estq_mov->prazo_inventario = somar_dias_uteis($_POST['data_medicao'], 5);
                $Estq_mov->envio_inventario = somar_dias_uteis($_POST['data_medicao'], 10);
                $Estq_mov->medicao_concessionaria = somar_dias_uteis($_POST['data_medicao'], 15);
            }

            if (!empty($_POST['valor_os_final'])) {
                $_POST['valor_os_final'] = str_replace(".", "", $_POST['valor_os_final']);
                $_POST['valor_os_final'] = str_replace(',', '.', $_POST['valor_os_final']);
            }

            if (!empty($_POST['data_recebimento'])) {
                $_POST['data_recebimento'] = DataSQL($_POST['data_recebimento']);
            }

            if (!empty($_POST['valor_recebimento'])) {
                $_POST['valor_recebimento'] = str_replace(',', '', $_POST['valor_recebimento']);
            }

            if (!empty($_POST['data_recebimento2'])) {
                $_POST['data_recebimento2'] = DataSQL($_POST['data_recebimento2']);
            }

            if (!empty($_POST['valor_recebimento2'])) {
                $_POST['valor_recebimento2'] = str_replace('.', '', $_POST['valor_recebimento2']);
                $_POST['valor_recebimento2'] = str_replace(',', '.', $_POST['valor_recebimento2']);
            }

            if (!empty($_POST['data_recebimento3'])) {
                $_POST['data_recebimento3'] = DataSQL($_POST['data_recebimento3']);
            }

            if (!empty($_POST['valor_recebimento3'])) {
                $_POST['valor_recebimento3'] = str_replace(',', '', $_POST['valor_recebimento3']);
            }

            if (!empty($_POST['data_recebimento4'])) {
                $_POST['data_recebimento4'] = DataSQL($_POST['data_recebimento4']);
            }

            if (!empty($_POST['data_execucao01'])) {
                $_POST['data_execucao01'] = DataSQL($_POST['data_execucao01']);
            }
            if (!empty($_POST['data_execucao02'])) {
                $_POST['data_execucao02'] = DataSQL($_POST['data_execucao02']);
            }
            if (!empty($_POST['data_execucao03'])) {
                $_POST['data_execucao03'] = DataSQL($_POST['data_execucao03']);
            }
            if (!empty($_POST['data_tci'])) {
                $_POST['data_tci'] = DataSQL($_POST['data_tci']);
            }
            if (!empty($_POST['viabilizado'])) {
                $_POST['viabilizado'] = ConvertDateTime($_POST['viabilizado']);
            }
            if (!empty($_POST['valor_recebimento4'])) {
                $_POST['valor_recebimento4'] = str_replace(',', '', $_POST['valor_recebimento4']);
            }
            if (!empty($_POST['hora_inicial'])) {
                $_POST['hora_inicial'] = $_POST['hora_inicial'];
            }
            if (!empty($_POST['hora_final'])) {
                $_POST['hora_final'] = $_POST['hora_final'];
            }
            $Estq_mov->usuario_dt = date('Y-m-d H:i:s');
            if ($_POST['requer'] != NULL) {
                if ($_POST['requer'] == "Fornecedor") {
                    $_POST['colaborador'] = null;
                    $_POST['cliente'] = null;
                } elseif ($_POST['requer'] == "Colaborador") {
                    $_POST['fornecedor'] = null;
                    $_POST['cliente'] = null;
                } elseif ($_POST['requer'] == "Cliente") {
                    $_POST['fornecedor'] = null;
                    $_POST['colaborador'] = null;
                }
            }

            $_POST['origem'] = Config::get('origem');//salvando a origem
            $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo
            if ($_POST['fornecedor'] != null || $_POST['cliente'] != null || $_POST['colaborador'] != null) {
                $Estq_mov->save($_POST);
                $ret['result'] = true;
                $ret['msg'] = 'Salvo com sucesso';
            } else {
                $ret['result'] = false;
                $ret['msg'] = 'Informe um Cliente , Fornecedor ou Colaborador !';
                throw new Exception('Informe um Cliente , Fornecedor ou Colaborador !');
            }

        } catch (Exception $e) {
            $ret['result'] = false;
            $ret['msg'] = $e->getMessage();
            $ret['error'] = $e->getMessage();
            $ret['trace'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    # Confirma a exclusão ou não de um(a) Estq_mov
    # renderiza a /view/Estq_mov/delete.php
    function delete()
    {
        $this->setTitle('Apagar O.S');
        try {
            $this->set('Estq_mov', new Estq_mov((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_mov', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_mov
    # redireciona para Estq_mov/all
    function post_delete()
    {
        try {
            $Estq_mov = new Estq_mov((int)$_POST['id']);
            $Estq_mov->usuario_dt = date('Y-m-d H:i:s');
            $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_mov->usuario_id = $user; // para salvar o usuario que está fazendo
            $Estq_mov->situacao = 3;
            $Estq_mov->save();
            new Msg(__('O.S deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }

        $this->go('Estq_mov', 'all');
    }

    function processamento()
    {
        //$Estq_mov->getEstq_tipomov()->operacao;
        $_POST['status'] = 2;

        try {
            $Estq_mov = new Estq_mov((int)$this->getParam('id'));
            $Estq_movdetalhe = Estq_movdetalhes::getList();
            $Estq_tipomovs = Estq_tipomov::getList();

            if (empty($Estq_movdetalhe)) {
                echo 'Cadastre os detalhes do O.S';
            } else {
                foreach ($Estq_movdetalhe as $e) {
                    if ($e->movimento == $Estq_mov->id) {
                        foreach ($Estq_tipomovs as $b) {
                            if ($b->id == $Estq_mov->tipo) {
                                $Estq_lote_substoques = new Estq_lote_substoque($e->lote_substoque);
                                if ($b->operacao == '-') {
                                    $Estq_lote_substoques->quantidade = $Estq_lote_substoques->quantidade - $e->quantidade;
                                } else {
                                    $Estq_lote_substoques->quantidade = $Estq_lote_substoques->quantidade + $e->quantidade;
                                }

                                $Estq_lote_substoques->save();
                            }
                        }
                    }
                }
            }

            $Estq_mov->save($_POST);
            new Msg(__('O.S processado com sucesso'));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
        }

        $this->go('Estq_mov', 'all');
    }

    function relatorios()
    {
        $this->setTitle('Relatórios');
    }

    function importacao()
    {
        $this->setTitle('Importação de artigos');
    }

    function importacao_r()
    {
        $this->setTitle('Importação de artigos');
    }

    function post_importacao()
    {
        $this->setTitle('Importando de artigos');

        $this->query('TRUNCATE temp_import');
        $order = array('\n', '\s', '\r', '&#13;&#10;', ' ');

        $artigos = str_replace($order, '-', $_POST['artigos']);
        $quantidade = str_replace($order, '-', $_POST['quantidade']);
        $artigos = explode("-", $artigos);
        $quantidade = explode("-", $quantidade);

        for ($x = 0; $x < count($artigos); $x++) {
            $tmp_import = new Temp_import();
            $tmp_import->codigo = $artigos[$x];
            $quant = str_replace(",", ".", $quantidade[$x]);
            $tmp_import->qtde = $quant;
            $tmp_import->save();
        }

        $this->go('Estq_mov', 'I_import', array('mov' => $this->getParam('mov'), 'tipo' => $_POST['tipo']));
    }

    function post_importacao_r()
    {
        $this->setTitle('Importando de recebidos');

        $this->query('TRUNCATE temp_import');
        $order = array('\n', '\s', '\r', '&#13;&#10;', ' ');
        $artigos = str_replace($order, '-', $_POST['artigos']);
        $quantidade = str_replace($order, '-', $_POST['quantidade']);
        $diagrama = str_replace($order, '-', $_POST['diagrama']);
        $artigos = explode("-", $artigos);
        $quantidade = explode("-", $quantidade);
        $diagrama = explode("-", $diagrama);

        for ($x = 0; $x < count($artigos); $x++) {
            $tmp_import = new Temp_import();
            $tmp_import->codigo = $artigos[$x];
            $quant = str_replace(",", ".", $quantidade[$x]);
            $tmp_import->qtde = $quant;
            $tmp_import->diagrama = $diagrama[$x];
            $tmp_import->save();
        }

        $this->go('Estq_mov', 'I_recebidos');
    }

    function I_import()
    {
        $this->setTitle('Detalhes da importação');

        $this->setStyles(SITE_PATH . '/content/css/bootstrap-editable.css');
        $this->setStyles(SITE_PATH . '/content/css/jquery.bootgrid.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2-bootstrap.min.css');
        $this->setStyles(SITE_PATH . '/content/css/bootstrap-dialog.min.css');

        $this->setScript(SITE_PATH . '/content/js/jquery.json.min.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-editable.min.js');
        $this->setScript(SITE_PATH . '/content/js/select2.full.min.js');
        $this->setScript(SITE_PATH . '/content/js/i18n/pt-BR.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.bootgrid.min.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.bootgrid.fa.min.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.number.min.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-dialog.min.js');

        $this->set('itens', Temp_import::getList());
    }

    function I_recebidos()
    {
        $this->setTitle('Detalhes da importação');

        $this->setStyles(SITE_PATH . '/content/css/bootstrap-editable.css');
        $this->setStyles(SITE_PATH . '/content/css/jquery.bootgrid.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2-bootstrap.min.css');
        $this->setStyles(SITE_PATH . '/content/css/bootstrap-dialog.min.css');

        $this->setScript(SITE_PATH . '/content/js/jquery.json.min.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-editable.min.js');
        $this->setScript(SITE_PATH . '/content/js/select2.full.min.js');
        $this->setScript(SITE_PATH . '/content/js/i18n/pt-BR.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.bootgrid.min.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.bootgrid.fa.min.js');
        $this->setScript(SITE_PATH . '/content/js/jquery.number.min.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-dialog.min.js');

        $this->set('itens', Temp_import::getList());
    }

    function update_codigo()
    {
        $pk = $_POST['pk'];
        $value = $_POST['value'];

        $im = new Temp_import((int)$pk);
        $im->codigo = $value;
        $im->save();

        $a = new Criteria();
        $a->addCondition('codigo_livre', '=', $value);
        $a->addCondition('situacao', '=', 1);
        $a->addCondition('origem', '=', Config::get('origem'));

        $artigo = Estq_artigo::getFirst($a);
        echo json_encode($artigo);
        exit;
    }

    function update_diagrama()
    {
        $pk = $_POST['pk'];
        $value = $_POST['value'];

        $im = new Temp_import((int)$pk);
        $im->diagrama = $value;
        $im->save();

        $a = new Criteria();
        $a->addCondition('numdoc', '=', $value);
        $a->addCondition('situacao', '=', 1);
        $a->addCondition('origem', '=', Config::get('origem'));
        $artigo = Estq_mov::getFirst($a);

        echo json_encode($artigo);
        exit;
    }

    function update_qtde()
    {
        $pk = $_POST['pk'];
        $value = $_POST['value'];

        $im = new Temp_import((int)$pk);
        $im->qtde = $value;
        $im->save();

        echo 1;
        exit;
    }

    # Confirma a exclusão ou não de um(a) Estq_mov
    # renderiza a /view/Estq_mov/delete.php
    function del_imp()
    {
        $this->setTitle('Apagar Detalhes de Importação');

        try {
            $this->set('Temp_import', new Temp_import((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_mov', 'I_import', array('mov' => $this->getParam('mov')));
        }
    }

    # Recebe o id via post e exclui um(a) Estq_mov
    # redireciona para Estq_mov/all
    function post_del_imp()
    {
        try {
            $Estq_mov = new Temp_import((int)$_POST['id']);
            $Estq_mov->delete();

            new Msg(__('Registro deletado com sucesso!'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }

        $this->go('Estq_mov', 'I_import', array('mov' => $this->getParam('mov')));
    }

    function f_importacao()
    {
        $this->setTitle('Finalizando Importação');
        $itens = Temp_import::getList();

        $b = new Criteria();
        $b->addCondition('movimento', '=', $this->getParam('mov'));
        $b->addCondition('situacao', '=', 1);
        $b->addCondition('origem', '=', Config::get('origem'));

        $mov_det = Estq_movdetalhes::getList($b);

        $aux_art = 1;

        foreach ($itens as $i) {
            $a = new Criteria();
            $a->addCondition('codigo_livre', '=', $i->codigo);
            $a->addCondition('situacao', '=', 1);
            $a->addCondition('origem', '=', Config::get('origem'));

            $artigo = Estq_artigo::getFirst($a);
            $aux = 1;

            if (!empty($artigo->id)) {
                $aux_art = 2;

                if (!empty($mov_det)) {
                    foreach ($mov_det as $m) {
                        if ($artigo->id == $m->artigo && $m->situacao <> 3) {
                            $complemento_movimento = new C_complemento_mov();
                            $complemento_movimento->tipo = $this->getParam('tipo');
                            $complemento_movimento->mov_detalhes = $m->id;
                            $complemento_movimento->situacao = 1;
                            $complemento_movimento->qtde = $i->qtde;
                            $complemento_movimento->user = Session::get('user')->id;
                            $complemento_movimento->data = date('Y-m-d H:i:s');
                            $complemento_movimento->complemento = 'Importação';
                            $complemento_movimento->movimento = $this->getParam('mov');
                            $complemento_movimento->save();

                            $movimento_detalhes = new Estq_movdetalhes((int)$m->id);

                            if ($this->getParam('tipo') == 1) {
                                $movimento_detalhes->quantidade = $movimento_detalhes->quantidade + floatval($i->qtde);
                            } else if ($this->getParam('tipo') == 2) {
                                $movimento_detalhes->qtrecebido = $movimento_detalhes->qtrecebido + floatval($i->qtde);
                            }

                            $movimento_detalhes->save();
                            $aux = 2;
                        }
                    }

                    if ($aux == 1) {
                        $c = new Criteria();
                        $c->addCondition('artigo', '=', $artigo->id);
                        $c->addCondition('situacao', '=', 1);
                        $c->addCondition('origem', '=', Config::get('origem'));

                        $lote_subestoque = Estq_lote_substoque::getFirst($c);

                        $movimento_detalhes = new Estq_movdetalhes();
                        $movimento_detalhes->artigo = $artigo->id;

                        if ($this->getParam('tipo') == 1) {
                            $movimento_detalhes->quantidade = $i->qtde;
                            $movimento_detalhes->qtrecebido = 0.0000;
                        } else if ($this->getParam('tipo') == 2) {
                            $movimento_detalhes->qtrecebido = $i->qtde;
                            $movimento_detalhes->quantidade = 0.0000;
                        }

                        $movimento_detalhes->lote_substoque = $lote_subestoque->id;
                        $movimento_detalhes->movimento = $this->getParam('mov');
                        $movimento_detalhes->usuario_dt = date('Y-m-d H:i:s');
                        $movimento_detalhes->usuario_id = Session::get('user')->id;
                        $movimento_detalhes->situacao = 1;
                        $movimento_detalhes->origem = Config::get('origem');
                        $movimento_detalhes->qtseparado = 0.0000;
                        $movimento_detalhes->qtcautela_encarregado = 0.0000;
                        $movimento_detalhes->qtdevolucao_encarregado = 0.0000;
                        $movimento_detalhes->qtinventario_campo = 0.0000;
                        $movimento_detalhes->adicional = 0.0000;
                        $movimento_detalhes->qtzmdv = 0;

                        if ($movimento_detalhes->save()) {
                            $complemento_movimento = new C_complemento_mov();
                            $complemento_movimento->tipo = 1;
                            $complemento_movimento->mov_detalhes = $movimento_detalhes->id;
                            $complemento_movimento->situacao = 1;
                            $complemento_movimento->qtde = $i->qtde;
                            $complemento_movimento->user = Session::get('user')->id;
                            $complemento_movimento->data = date('Y-m-d H:i:s');
                            $complemento_movimento->complemento = 'Importação';
                            $complemento_movimento->movimento = $this->getParam('mov');
                            $complemento_movimento->save();
                        }
                    }

                    $aux = 1;
                } else {
                    $c = new Criteria();
                    $c->addCondition('artigo', '=', $artigo->id);
                    $c->addCondition('situacao', '=', 1);
                    $c->addCondition('origem', '=', Config::get('origem'));

                    $lote_subestoque = Estq_lote_substoque::getFirst($c);

                    $movimento_detalhes = new Estq_movdetalhes();
                    $movimento_detalhes->artigo = $artigo->id;
                    if ($this->getParam('tipo') == 1) {
                        $movimento_detalhes->quantidade = $i->qtde;
                        $movimento_detalhes->qtrecebido = 0.0000;
                    } else if ($this->getParam('tipo') == 2) {
                        $movimento_detalhes->qtrecebido = $i->qtde;
                        $movimento_detalhes->quantidade = 0.0000;
                    }

                    $movimento_detalhes->lote_substoque = $lote_subestoque->id;
                    $movimento_detalhes->movimento = $this->getParam('mov');
                    $movimento_detalhes->usuario_dt = date('Y-m-d H:i:s');
                    $movimento_detalhes->usuario_id = Session::get('user')->id;
                    $movimento_detalhes->situacao = 1;
                    $movimento_detalhes->origem = Config::get('origem');
                    $movimento_detalhes->qtseparado = 0.0000;
                    $movimento_detalhes->qtcautela_encarregado = 0.0000;
                    $movimento_detalhes->qtdevolucao_encarregado = 0.0000;
                    $movimento_detalhes->qtinventario_campo = 0.0000;
                    $movimento_detalhes->qtzmdv = 0.0000;
                    $movimento_detalhes->adicional = 0.0000;

                    if ($movimento_detalhes->save()) {
                        $complemento_movimento = new C_complemento_mov();
                        $complemento_movimento->tipo = 1;
                        $complemento_movimento->mov_detalhes = $movimento_detalhes->id;
                        $complemento_movimento->situacao = 1;
                        $complemento_movimento->qtde = $i->qtde;
                        $complemento_movimento->user = Session::get('user')->id;
                        $complemento_movimento->data = date('Y-m-d H:i:s');
                        $complemento_movimento->complemento = 'Importação';
                        $complemento_movimento->movimento = $this->getParam('mov');
                        $complemento_movimento->save();
                    }
                }
            }

            $artigo = null;
        }

        if ($aux_art == 1) {
            new Msg(__('Nenhum artigo localizado!'), 2);
        } else {
            new Msg(__('Importados com sucesso!'), 1);
        }

        $this->go('Estq_movdetalhes', 'all', array('id' => $this->getParam('mov')));
    }

    function f_recebimento()
    {
        $this->setTitle('Finalizando Importação');
        $itens = Temp_import::getList();

        $aux_art = 1;

        foreach ($itens as $i) {
            $a = new Criteria();
            $a->addCondition('numdoc', '=', $i->diagrama);
            $a->addCondition('situacao', '=', 1);
            $a->addCondition('origem', '=', Config::get('origem'));

            $movimento = Estq_mov::getFirst($a);

            if (!empty($movimento)) {
                $b = new Criteria();
                $b->addCondition('movimento', '=', $movimento->id);
                $b->addCondition('situacao', '=', 1);
                $b->addCondition('origem', '=', Config::get('origem'));

                $mov_det = Estq_movdetalhes::getList($b);

                $a = new Criteria();
                $a->addCondition('codigo_livre', '=', $i->codigo);
                $a->addCondition('situacao', '=', 1);
                $a->addCondition('origem', '=', Config::get('origem'));
                $artigo = Estq_artigo::getFirst($a);

                $aux = 1;
                if (!empty($artigo->id)) {
                    $aux_art = 2;
                    if (!empty($mov_det)) {
                        foreach ($mov_det as $m) {
                            if ($artigo->id == $m->artigo && $m->situacao <> 3) {
                                $complemento_movimento = new C_complemento_mov();
                                $complemento_movimento->tipo = 2;
                                $complemento_movimento->mov_detalhes = $m->id;
                                $complemento_movimento->situacao = 1;
                                $complemento_movimento->qtde = $i->qtde;
                                $complemento_movimento->user = Session::get('user')->id;
                                $complemento_movimento->data = date('Y-m-d H:i:s');
                                $complemento_movimento->complemento = 'Importação';
                                $complemento_movimento->movimento = $movimento->id;
                                $complemento_movimento->save();

                                $movimento_detalhes = new Estq_movdetalhes((int)$m->id);
                                $movimento_detalhes->qtrecebido = $movimento_detalhes->qtrecebido + floatval($i->qtde);
                                $movimento_detalhes->save();
                                $aux = 2;
                            }
                        }
                        if ($aux == 1) {
                            $c = new Criteria();
                            $c->addCondition('artigo', '=', $artigo->id);
                            $c->addCondition('situacao', '=', 1);
                            $c->addCondition('origem', '=', Config::get('origem'));

                            $lote_subestoque = Estq_lote_substoque::getFirst($c);

                            $movimento_detalhes = new Estq_movdetalhes();
                            $movimento_detalhes->artigo = $artigo->id;
                            $movimento_detalhes->qtrecebido = $i->qtde;
                            $movimento_detalhes->quantidade = 0.0000;
                            $movimento_detalhes->lote_substoque = $lote_subestoque->id;
                            $movimento_detalhes->movimento = $movimento->id;
                            $movimento_detalhes->usuario_dt = date('Y-m-d H:i:s');
                            $movimento_detalhes->usuario_id = Session::get('user')->id;
                            $movimento_detalhes->situacao = 1;
                            $movimento_detalhes->origem = Config::get('origem');
                            $movimento_detalhes->qtseparado = 0.0000;
                            $movimento_detalhes->qtcautela_encarregado = 0.0000;
                            $movimento_detalhes->qtdevolucao_encarregado = 0.0000;
                            $movimento_detalhes->qtinventario_campo = 0.0000;
                            $movimento_detalhes->adicional = 0.0000;
                            $movimento_detalhes->qtzmdv = 0;

                            if ($movimento_detalhes->save()) {
                                $complemento_movimento = new C_complemento_mov();
                                $complemento_movimento->tipo = 1;
                                $complemento_movimento->mov_detalhes = $movimento_detalhes->id;
                                $complemento_movimento->situacao = 1;
                                $complemento_movimento->qtde = $i->qtde;
                                $complemento_movimento->user = Session::get('user')->id;
                                $complemento_movimento->data = date('Y-m-d H:i:s');
                                $complemento_movimento->complemento = 'Importação';
                                $complemento_movimento->movimento = $movimento->id;
                                $complemento_movimento->save();
                            }
                        }

                        $aux = 1;
                    } else {
                        $c = new Criteria();
                        $c->addCondition('artigo', '=', $artigo->id);
                        $c->addCondition('situacao', '=', 1);
                        $c->addCondition('origem', '=', Config::get('origem'));

                        $lote_subestoque = Estq_lote_substoque::getFirst($c);

                        $movimento_detalhes = new Estq_movdetalhes();
                        $movimento_detalhes->artigo = $artigo->id;
                        $movimento_detalhes->qtrecebido = $i->qtde;
                        $movimento_detalhes->quantidade = 0.0000;
                        $movimento_detalhes->lote_substoque = $lote_subestoque->id;
                        $movimento_detalhes->movimento = $movimento->id;
                        $movimento_detalhes->usuario_dt = date('Y-m-d H:i:s');
                        $movimento_detalhes->usuario_id = Session::get('user')->id;
                        $movimento_detalhes->situacao = 1;
                        $movimento_detalhes->origem = Config::get('origem');
                        $movimento_detalhes->qtseparado = 0.0000;
                        $movimento_detalhes->qtcautela_encarregado = 0.0000;
                        $movimento_detalhes->qtdevolucao_encarregado = 0.0000;
                        $movimento_detalhes->qtinventario_campo = 0.0000;
                        $movimento_detalhes->qtzmdv = 0.0000;
                        $movimento_detalhes->adicional = 0.0000;
                        if ($movimento_detalhes->save()) {
                            $complemento_movimento = new C_complemento_mov();
                            $complemento_movimento->tipo = 1;
                            $complemento_movimento->mov_detalhes = $movimento_detalhes->id;
                            $complemento_movimento->situacao = 1;
                            $complemento_movimento->qtde = $i->qtde;
                            $complemento_movimento->user = Session::get('user')->id;
                            $complemento_movimento->data = date('Y-m-d H:i:s');
                            $complemento_movimento->complemento = 'Importação';
                            $complemento_movimento->movimento = $movimento->id;
                            $complemento_movimento->save();
                        }
                    }
                }

                $artigo = null;
            }
        }

        if ($aux_art == 1) {
            new Msg(__('Nenhum artigo localizado!'), 2);
        } else {
            new Msg(__('Importados com sucesso!'), 1);
        }

        $this->go('Estq_mov', 'importacao_r');
    }

    /**
     * @return Estq_mov Diagrama.
     * @author Iohan
     * metodo criado para pequisar diagrama via ajax
     */
    function listarDiagramas()
    {
        $aux = Config::get('origem');
        if ($_POST['data']['q'] != "") {
            $arr = $this->query("SELECT `numdoc` FROM `estq_mov` WHERE (numdoc LIKE '" . utf8_decode(addslashes($_POST['data']['q'])) . "%') AND situacao <> 3 AND origem = " . $aux . " GROUP BY numdoc; ");
            $results = array();
            foreach ($arr as $mov) {
                if (!empty($mov->numdoc))
                    $results[] = array(
                        'text' => $mov->numdoc,
                        'id' => $mov->numdoc

                    );
            }
            echo json_encode(array('q' => $_POST['data']['q'], 'results' => $results));
            exit;
        }
        echo json_encode(array('q' => "", 'results' => array()));
        exit;

    }

//**********************************
    public function fotos()
    {
        $this->setTitle('Listagem de Fotos');
        $idMovimentacao = $this->getParam('id');
        $c = new Criteria();
        $c->addCondition('estq_mov_id', '=', $idMovimentacao);
        $c->addCondition('situacao_id', '=', 1);
        $c->setOrder('ordem ASC');
        $Concessionaria_imagem = Concessionaria_imagem::getList($c);

        $this->set('idMovimentacao', $idMovimentacao);
        $this->set('concessionariaImagens', $Concessionaria_imagem);
    }

    function add_fotos()
    {
        $this->setTitle('Cadastro de Fotos');
        $idMovimentacao = $this->getParam('id');
//
        $this->set('idMovimentacao', $idMovimentacao);
        $this->set('Concessionaria_imagem', new Concessionaria_imagem);
    }

    function post_add_fotos()
    {
        $ConcessionariaImagem = new Concessionaria_imagem();
        $idMovimentacao = $_POST['movimentacao_id'];
        $modal = $_POST['modal'];
        try {
            $numFile = count(array_filter($_FILES['caminho']['name']));
            $file = $_FILES['caminho'];
            for ($i = 0; $i < $numFile; $i++) {
                $dados = array(
                    "name" => $file['name'][$i],
                    "type" => $file['type'][$i],
                    "tmp_name" => $file['tmp_name'][$i],
                    "error" => $file['error'][$i],
                    "size" => $file['size'][$i]
                );
                $extensions = explode(',', 'jpg,jpeg,png,gif');

                $file_uploader = new FileUploader($dados, null, $extensions);
                $name = time() . tratar_arquivos($dados['name']);
                $path = "book";
                if (empty($file_uploader->erro)) {
                    if ($file_uploader->save($name, $path)) {
                        $_POST['caminho_imagem'] = $file_uploader->path . '/' . $file_uploader->name;
                        $_POST['cadastrado_em'] = date('Y-m-d H:i:s');
                        $_POST['usuario_id'] = Session::get('user')->id;
                        $_POST['situacao_id'] = 1;
                        $_POST['estq_mov_id'] = $idMovimentacao;
                        $ConcessionariaImagem->save($_POST);
                        if ($modal == 1) {
                            $res = ['res' => 1, 'redirect' => "/Estq_mov/edit/id:$idMovimentacao/"];
                        } else {
                            $res = ['res' => 1, 'redirect' => "/Estq_mov/fotos/id:$idMovimentacao/"];
                        }

                    } else {
                        $res = ['res' => 2, 'msg' => $file_uploader->erro];
                    }
                } else {
                    $res = ['res' => 2, 'msg' => $file_uploader->erro];
                }

                echo json_encode($res);
                exit;

            }
            new Msg(__('A foto foi adicionada com sucesso'));
            $this->go('Estq_mov', 'fotos', array('id' => $idMovimentacao));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    function edit_foto()
    {
        $this->setTitle('Editar Imagem');
        $concessionariaImagem = new Concessionaria_imagem((int)$this->getParam('id'));

        $this->set('concessionariaImagem', $concessionariaImagem);
    }

    function post_edit_foto()
    {

        $id = $_POST['id'];
        try {
            $Concessionaria_imagem = new Concessionaria_imagem((int)$id);

            if (!empty($_FILES['caminho']['name'])) {

                if (!empty($Concessionaria_imagem->caminho_imagem)) {
                    unlink(SITE_PATH .'/'.$Concessionaria_imagem->caminho_imagem);
                }

                $numFile = count(array_filter($_FILES['caminho']['name']));
                $file = $_FILES['caminho'];

                for ($i = 0; $i < $numFile; $i++) {
                    $dados = array(
                        "name" => $file['name'][$i],
                        "type" => $file['type'][$i],
                        "tmp_name" => $file['tmp_name'][$i],
                        "error" => $file['error'][$i],
                        "size" => $file['size'][$i]
                    );
                    $extensions = explode(',', 'jpg,jpeg,png,gif');

                    $file_uploader = new FileUploader($dados, null, $extensions);
                    $name = time() . tratar_arquivos($dados['name']);
                    $path = "book";
                    if (empty($file_uploader->erro)) {
                        if ($file_uploader->save($name, $path)) {
                            $_POST['caminho_imagem'] = $file_uploader->path . '/' . $file_uploader->name;
                            $_POST['alterado_em'] = date('Y-m-d H:i:s');
                            $_POST['alterado_por'] = Session::get('user')->id;
                            $Concessionaria_imagem->save($_POST);
                            $res = ['res' => 1, 'redirect' => "/Estq_mov/fotos/id:$Concessionaria_imagem->estq_mov_id/"];
                        } else {
                            $res = ['res' => 2, 'msg' => $file_uploader->erro];
                        }
                    } else {
                        $res = ['res' => 2, 'msg' => $file_uploader->erro];
                    }

                    echo json_encode($res);
                    exit;

                }
            } else {

                $_POST['caminho_imagem'] = $Concessionaria_imagem->caminho_imagem;
                $_POST['alterado_em'] = date('Y-m-d H:i:s');
                $_POST['alterado_por'] = Session::get('user')->id;
                $Concessionaria_imagem->save($_POST);
                $res = ['res' => 1, 'redirect' => "/Estq_mov/fotos/id:$Concessionaria_imagem->estq_mov_id/"];

                echo json_encode($res);
                exit;
            }

            new Msg(__('Concessionária imagem atualizada com sucesso'));
            // $this->go('Anexos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    function delete_foto()
    {
        $this->setTitle('Apagar Imagem');
        try {
            $this->set('imagem', new Concessionaria_imagem((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_mov', 'fotos');
        }
    }

    function delete_anexo()
    {
        $Concessionaria_imagem = new Concessionaria_imagem((int)$this->getParam('id'));
        unlink(SITE_PATH.'/' . $Concessionaria_imagem->caminho_imagem);
        $file['caminho_imagem'] = '';
        $Concessionaria_imagem->save($file);
        echo json_encode(1);
        exit();
    }

    function post_delete_foto()
    {
        try {
            $imagem = new Concessionaria_imagem((int)$_POST['id']);
            $imagem->situacao_id = 3;
            $imagem->save();
            new Msg(__('Imagem apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Estq_mov', 'fotos', array('id' => $_POST['estq_mov_id']));
    }

    function book_report()
    {
        $this->setTitle('Acervo de Fotos');
        $this->setTemplate('reports');
        $idMovimento = $this->getParam('id');
        $c = new Criteria();
        $c->addCondition('estq_mov_id', '=', $idMovimento);
        $c->addCondition('situacao_id', '=', 1);
        $c->addCondition('tipo_imagem', '=', 1);
        $c->setOrder('ordem ASC');
        $concessionaria_imagens = Concessionaria_imagem::getList($c);
        $movimentacao = new Estq_mov($idMovimento);
        $json['prestadora'] = array(
            "ordem_servico" => $movimentacao->tablet,
            "nr" => $movimentacao->coi,
            "codigo_equipe" => $movimentacao->codigo_equipe,
            "prestador" => 'JRSM',
            "data_conclusao" => DataBR($movimentacao->data_medicao),
            "municipio" => $movimentacao->municipio,
            "motivo" => $movimentacao->getMotivoRetirada()->nome,
            'tipo_servico' => $movimentacao->getTipo_servico()->nome
        );

        $json['rodape'] = array(
            'distribuidora' => 'Energisa Mato Grosso do Sul - Destribuidora De Energia S.A. ',
            'endereco' => 'Avenida Gury Marques,8000. Campo grande - MS | 79072-900 Tel.:(67)3398-4000',
            'site' => 'www.energisa.com.br'
        );

        foreach ($concessionaria_imagens as $img) {

            $json['servico'][$img->tamanho][] = array(
                "estq_mov_id" => $img->estq_mov_id,
                "observacao" => html_entity_decode($img->observacao),
                "estrutura" => $img->estrutura,
                "caminho_imagem" => SITE_PATH . '/' . $img->caminho_imagem,
                'ordem' => (int)$img->ordem
            );
        }
        $reportCreate = new Json();

        $reportCreate->create('reportFotos.json', json_encode($json));
    }

    function download()
    {
        $Imagem = new Concessionaria_imagem((int)$_GET['id']);
        set_time_limit(0);
        $file = $_SERVER['DOCUMENT_ROOT'] . trim(SITE_PATH . '/' . $Imagem->caminho_imagem);

        if (!file_exists($file)) {
            exit;
        }
        $filename = basename($file);
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));
        $types = [
            'gif' => 'image/gif',
            'png' => 'image/png',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
        ];
        header('Content-type:' . $types[$file_extension]);
        header("Cache-Control: no-store, no-cache");
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        exit;

    }

    function exportarDadosBi()
    {

        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="movimentos.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');

        $csvFile = fopen('php://output', 'w+');

        $sql = "Select * From relatorio_movimentacao_powerbi_2 ";
        $sqlWhere = '';
        $date_inicio = '';
        $date_fim = '';

        $tipoData = $this->getParam('tipo_data');
        if(!empty($this->getParam('tipo_data'))) {
            $sqlWhere = "WHERE ";
            if (!empty($this->getParam('data_inicio'))) {
                $dateFormat = new DateTime();
                $date_inicio = $dateFormat->createFromFormat('d/m/Y', $this->getParam('data_inicio'));
                $sqlWhere .= "{$tipoData} >= :data_inicio ";
            }

            if (!empty($this->getParam('data_fim'))) {
                $dateFormat = new DateTime();
                $date_fim = $dateFormat->createFromFormat('d/m/Y', $this->getParam('data_fim'));
                $sqlWhere .= (!empty($this->getParam('data_inicio'))?'AND':'')." {$tipoData} <= :data_fim ";
            }
        }

        $db = $this::getConn();
        $sql .= $sqlWhere;
        $db->query($sql);
        if(!empty($this->getParam('tipo_data'))) {
            if (!empty($this->getParam('data_inicio'))) {
                $db->bind(":data_inicio", $date_inicio->format('Y-m-d'),PDO::PARAM_STR);
            }

            if (!empty($this->getParam('data_fim'))) {
                $db->bind(":data_fim", $date_fim->format('Y-m-d'),PDO::PARAM_STR);
            }
        }


        $dados = $db->fetchAll(PDO::FETCH_ASSOC);


//        força o excel usar a codificaçao utf8 NÃO APAGAR
        fputs($csvFile, "\xEF\xBB\xBF");
        if (!empty($dados)) {
            unset($dados[0]['data_medicao']);
            $header = array_keys($dados[0]);
            fputcsv($csvFile, $header, ';');
            foreach ($dados as $key => $values) {
                unset($values['data_medicao']);
                fputcsv($csvFile, $values, ';');
            }
        }

        fclose($csvFile);
        exit();
    }

}