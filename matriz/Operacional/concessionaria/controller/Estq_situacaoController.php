<?php
final class Estq_situacaoController extends AppController{ 

    # página inicial do módulo Estq_situacao
    function index(){
        $this->setTitle('Visualização de Situação');
    }

    # lista de Estq_situacaos
    # renderiza a visão /view/Estq_situacao/all.php
    function all(){
        $this->setTitle('Listagem de Situação');
        $p = new Paginate('Estq_situacao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Estq_situacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Estq_situacao
    # renderiza a visão /view/Estq_situacao/view.php
    function view(){
        $this->setTitle('Visualização de Situação');
        try {
            $this->set('Estq_situacao', new Estq_situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_situacao', 'all');
        }
    }

    # formulário de cadastro de Estq_situacao
    # renderiza a visão /view/Estq_situacao/add.php
    function add(){
        $this->setTitle('Cadastro de Situação');
        $this->set('Estq_situacao', new Estq_situacao);
    }

    # recebe os dados enviados via post do cadastro de Estq_situacao
    # (true)redireciona ou (false) renderiza a visão /view/Estq_situacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Situação');
        $Estq_situacao = new Estq_situacao();
        $this->set('Estq_situacao', $Estq_situacao);
        try {
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_situacao->usuario_id=$user; // para salvar o usuario que está fazendo
            $_POST['data_cadastro'] = date('Y-m-d H:i:s');
            $Estq_situacao->save($_POST);
            new Msg(__('Situação cadastrada com sucesso'));
            $this->go('Estq_situacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição de Estq_situacao
    # renderiza a visão /view/Estq_situacao/edit.php
    function edit(){
        $this->setTitle('Edição de Situação');
        try {
            $this->set('Estq_situacao', new Estq_situacao((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_situacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_situacao
    # (true)redireciona ou (false) renderiza a visão /view/Estq_situacao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Situação');
        try {
            $Estq_situacao = new Estq_situacao((int) $_POST['id']);
            $this->set('Estq_situacao', $Estq_situacao);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_situacao->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_situacao->save($_POST);
            new Msg(__('Situação atualizada com sucesso'));
            $this->go('Estq_situacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_situacao
    # renderiza a /view/Estq_situacao/delete.php
    function delete(){
        $this->setTitle('Apagar Estq_situacao');
        try {
            $this->set('Estq_situacao', new Estq_situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_situacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_situacao
    # redireciona para Estq_situacao/all
    function post_delete(){
        try {
            $Estq_situacao = new Estq_situacao((int) $_POST['id']);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_situacao->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_situacao->cascata("estp_situacao","id",$_POST['id']);
            new Msg(__('Situação deletada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_situacao', 'all');
    }

}