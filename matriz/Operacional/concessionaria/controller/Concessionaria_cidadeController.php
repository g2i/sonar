<?php
final class Concessionaria_cidadeController extends AppController{ 

    # página inicial do módulo Concessionaria_cidade
    function index(){
        $this->setTitle('Visualização de Concessionaria_cidade');
    }

    # lista de Concessionaria_cidades
    # renderiza a visão /view/Concessionaria_cidade/all.php
    function all(){
        $this->setTitle('Listagem de Concessionaria_cidade');
        $p = new Paginate('Concessionaria_cidade', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Concessionaria_cidades', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Concessionaria_cidade
    # renderiza a visão /view/Concessionaria_cidade/view.php
    function view(){
        $this->setTitle('Visualização de Concessionaria_cidade');
        try {
            $this->set('Concessionaria_cidade', new Concessionaria_cidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_cidade', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_cidade
    # renderiza a visão /view/Concessionaria_cidade/add.php
    function add(){
        $this->setTitle('Cadastro de Concessionaria_cidade');
        $this->set('Concessionaria_cidade', new Concessionaria_cidade);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Concessionaria_grupo_supervisaos',  Concessionaria_grupo_supervisao::getList());
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_cidade
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_cidade/add.php
    function post_add(){
        $this->setTitle('Cadastro de Concessionaria_cidade');
        $Concessionaria_cidade = new Concessionaria_cidade();
        $this->set('Concessionaria_cidade', $Concessionaria_cidade);
        try {
            $Concessionaria_cidade->save($_POST);
            new Msg(__('Concessionaria_cidade cadastrado com sucesso'));
            $this->go('Concessionaria_cidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Concessionaria_grupo_supervisaos',  Concessionaria_grupo_supervisao::getList());
    }

    # formulário de edição de Concessionaria_cidade
    # renderiza a visão /view/Concessionaria_cidade/edit.php
    function edit(){
        $this->setTitle('Edição de Concessionaria_cidade');
        try {
            $this->set('Concessionaria_cidade', new Concessionaria_cidade((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Concessionaria_grupo_supervisaos',  Concessionaria_grupo_supervisao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_cidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_cidade
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_cidade/edit.php
    function post_edit(){
        $this->setTitle('Edição de Concessionaria_cidade');
        try {
            $Concessionaria_cidade = new Concessionaria_cidade((int) $_POST['id']);
            $this->set('Concessionaria_cidade', $Concessionaria_cidade);
            $Concessionaria_cidade->save($_POST);
            new Msg(__('Concessionaria_cidade atualizado com sucesso'));
            $this->go('Concessionaria_cidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Concessionaria_grupo_supervisaos',  Concessionaria_grupo_supervisao::getList());
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_cidade
    # renderiza a /view/Concessionaria_cidade/delete.php
    function delete(){
        $this->setTitle('Apagar Concessionaria_cidade');
        try {
            $this->set('Concessionaria_cidade', new Concessionaria_cidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_cidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_cidade
    # redireciona para Concessionaria_cidade/all
    function post_delete(){
        try {
            $Concessionaria_cidade = new Concessionaria_cidade((int) $_POST['id']);
            $Concessionaria_cidade->delete();
            new Msg(__('Concessionaria_cidade apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_cidade', 'all');
    }

    function getAllCidades(){
        try {
            $queryCidades = new Criteria();
            $queryCidades->setOrder('cidade ASC');
            $cidadesObj = Concessionaria_cidade::getList($queryCidades);
            $cidades =array();
            foreach($cidadesObj AS $value){
                $cidade = array();
                $cidade['id'] = $value->id;
                $cidade['cidade'] = htmlentities($value->cidade);
                $cidade['sigla'] = $value->sigla;
                $cidade['estado'] = $value->estado;
                $cidades[] = $cidade;
            }
            http_response_code(200);
            echo json_encode($cidades);
            exit();
        }catch(PDOException $e){
            http_response_code(500);
            echo json_encode($e->getMessage());
            exit();
        }


    }
    function getCidades(){
        try {
            if(empty($this->getParam('supervisao_id'))){
                throw new PDOException('Houve perda do id da supervisao ');
            }
            $supervisao_id = $this->getParam('supervisao_id');
            $queryCidades = new Criteria();
            $queryCidades->addCondition('supervisao_id','=', $supervisao_id);
            $queryCidades->setOrder('cidade ASC');
            $cidadesObj = Concessionaria_cidade::getList($queryCidades);
            $cidades =array();
            foreach($cidadesObj AS $value){
                $cidade = array();
                $cidade['id'] = $value->id;
                $cidade['cidade'] = htmlentities($value->cidade);
                $cidade['sigla'] = $value->sigla;
                $cidade['estado'] = $value->estado;
                $cidades[] = $cidade;
            }
            http_response_code(200);
            echo json_encode($cidades);
            exit();
        }catch(PDOException $e){
            http_response_code(500);
            echo json_encode($e->getMessage());
            exit();
        }


    }


}