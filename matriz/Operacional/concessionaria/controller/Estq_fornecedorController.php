<?php
final class Estq_fornecedorController extends AppController{ 

    # página inicial do módulo Estq_fornecedor
    function index(){
        $this->setTitle('Visualização de Fornecedor');
    }

    # lista de Estq_fornecedores
    # renderiza a visão /view/Estq_fornecedor/all.php
    function all(){
        $this->setTitle('Listagem de Fornecedores');
        $p = new Paginate('Estq_fornecedor', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Estq_fornecedores', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Estq_fornecedor
    # renderiza a visão /view/Estq_fornecedor/view.php
    function view(){
        $this->setTitle('Visualização de Fornecedor');
        try {
            $this->set('Estq_fornecedor', new Estq_fornecedor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_fornecedor', 'all');
        }
    }

    # formulário de cadastro de Estq_fornecedor
    # renderiza a visão /view/Estq_fornecedor/add.php
    function add(){
        $this->setTitle('Cadastro de Fornecedor');
        $this->set('Estq_fornecedor', new Estq_fornecedor);
    }

    # recebe os dados enviados via post do cadastro de Estq_fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Estq_fornecedor/add.php
    function post_add(){
        $this->setTitle('Cadastro de Fornecedor');
        $Estq_fornecedor = new Estq_fornecedor();
        $this->set('Estq_fornecedor', $Estq_fornecedor);
        $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
        $Estq_fornecedor->usuario_id=$user; // para salvar o usuario que está fazendo
        try {
            $Estq_fornecedor->save($_POST);
            new Msg(__('Fornecedor cadastrado com sucesso'));
            $this->go('Estq_fornecedor', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Estq_fornecedor
    # renderiza a visão /view/Estq_fornecedor/edit.php
    function edit(){
        $this->setTitle('Edição de Fornecedor');
        try {
            $this->set('Estq_fornecedor', new Estq_fornecedor((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_fornecedor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Estq_fornecedor/edit.php
    function post_edit(){
        $this->setTitle('Edição de Fornecedor');
        try {
            $Estq_fornecedor = new Estq_fornecedor((int) $_POST['id']);
            $this->set('Estq_fornecedor', $Estq_fornecedor);
            $Estq_fornecedor->usuario_dt=date('Y-m-d H:i:s');
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_fornecedor->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_fornecedor->save($_POST);
            new Msg(__('Fornecedor atualizado com sucesso'));
            $this->go('Estq_fornecedor', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Estq_fornecedor
    # renderiza a /view/Estq_fornecedor/delete.php
    function delete(){
        $this->setTitle('Apagar Fornecedor');
        try {
            $this->set('Estq_fornecedor', new Estq_fornecedor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_fornecedor', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_fornecedor
    # redireciona para Estq_fornecedor/all
    function post_delete(){
        try {
            $Estq_fornecedor = new Estq_fornecedor((int) $_POST['id']);
            $Estq_fornecedor->usuario_dt=date('Y-m-d H:i:s');
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_fornecedor->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_fornecedor->situacao=3;
            $Estq_fornecedor->save();
            new Msg(__('Fornecedor deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_fornecedor', 'all');
    }

}