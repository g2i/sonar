<?php
final class FornecedorController extends AppController{ 

    # página inicial do módulo Fornecedor
    function index(){
        $this->setTitle('Visualização de Fornecedor');
    }

    # lista de Fornecedores
    # renderiza a visão /view/Fornecedor/all.php
    function all(){
        $this->setTitle('Listagem de Fornecedor');
        $p = new Paginate('Fornecedor', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Fornecedores', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Fornecedor
    # renderiza a visão /view/Fornecedor/view.php
    function view(){
        $this->setTitle('Visualização de Fornecedor');
        try {
            $this->set('Fornecedor', new Fornecedor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fornecedor', 'all');
        }
    }

    # formulário de cadastro de Fornecedor
    # renderiza a visão /view/Fornecedor/add.php
    function add(){
        $this->setTitle('Cadastro de Fornecedor');
        $this->set('Fornecedor', new Fornecedor);
    }

    # recebe os dados enviados via post do cadastro de Fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor/add.php
    function post_add(){
        $this->setTitle('Cadastro de Fornecedor');
        $Fornecedor = new Fornecedor();
        $this->set('Fornecedor', $Fornecedor);
        try {
            $Fornecedor->save($_POST);
            new Msg(__('Fornecedor cadastrado com sucesso'));
            $this->go('Fornecedor', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Fornecedor
    # renderiza a visão /view/Fornecedor/edit.php
    function edit(){
        $this->setTitle('Edição de Fornecedor');
        try {
            $this->set('Fornecedor', new Fornecedor((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Fornecedor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor/edit.php
    function post_edit(){
        $this->setTitle('Edição de Fornecedor');
        try {
            $Fornecedor = new Fornecedor((int) $_POST['id']);
            $this->set('Fornecedor', $Fornecedor);
            $Fornecedor->save($_POST);
            new Msg(__('Fornecedor atualizado com sucesso'));
            $this->go('Fornecedor', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Fornecedor
    # renderiza a /view/Fornecedor/delete.php
    function delete(){
        $this->setTitle('Apagar Fornecedor');
        try {
            $this->set('Fornecedor', new Fornecedor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fornecedor', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Fornecedor
    # redireciona para Fornecedor/all
    function post_delete(){
        try {
            $Fornecedor = new Fornecedor((int) $_POST['id']);
            $Fornecedor->delete();
            new Msg(__('Fornecedor apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Fornecedor', 'all');
    }

}