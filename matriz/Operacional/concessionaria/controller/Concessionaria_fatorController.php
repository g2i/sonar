<?php
final class Concessionaria_fatorController extends AppController{ 

    # página inicial do módulo Concessionaria_fator
    function index(){
        $this->setTitle('Visualização de Concessionaria_fator');
    }

    # lista de Concessionaria_fatores
    # renderiza a visão /view/Concessionaria_fator/all.php
    function all(){
        $this->setTitle('Listagem de Concessionaria_fator');
        $p = new Paginate('Concessionaria_fator', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Concessionaria_fatores', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Concessionaria_fator
    # renderiza a visão /view/Concessionaria_fator/view.php
    function view(){
        $this->setTitle('Visualização de Concessionaria_fator');
        try {
            $this->set('Concessionaria_fator', new Concessionaria_fator((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_fator', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_fator
    # renderiza a visão /view/Concessionaria_fator/add.php
    function add(){
        $this->setTitle('Cadastro de Concessionaria_fator');
        $this->set('Concessionaria_fator', new Concessionaria_fator);
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_fator
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_fator/add.php
    function post_add(){
        $this->setTitle('Cadastro de Concessionaria_fator');
        $Concessionaria_fator = new Concessionaria_fator();
        $this->set('Concessionaria_fator', $Concessionaria_fator);
        try {
            $Concessionaria_fator->save($_POST);
            new Msg(__('Concessionaria_fator cadastrado com sucesso'));
            $this->go('Concessionaria_fator', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Concessionaria_fator
    # renderiza a visão /view/Concessionaria_fator/edit.php
    function edit(){
        $this->setTitle('Edição de Concessionaria_fator');
        try {
            $this->set('Concessionaria_fator', new Concessionaria_fator((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_fator', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_fator
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_fator/edit.php
    function post_edit(){
        $this->setTitle('Edição de Concessionaria_fator');
        try {
            $Concessionaria_fator = new Concessionaria_fator((int) $_POST['id']);
            $this->set('Concessionaria_fator', $Concessionaria_fator);
            $Concessionaria_fator->save($_POST);
            new Msg(__('Concessionaria_fator atualizado com sucesso'));
            $this->go('Concessionaria_fator', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_fator
    # renderiza a /view/Concessionaria_fator/delete.php
    function delete(){
        $this->setTitle('Apagar Concessionaria_fator');
        try {
            $this->set('Concessionaria_fator', new Concessionaria_fator((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_fator', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_fator
    # redireciona para Concessionaria_fator/all
    function post_delete(){
        try {
            $Concessionaria_fator = new Concessionaria_fator((int) $_POST['id']);
            $Concessionaria_fator->delete();
            new Msg(__('Concessionaria_fator apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_fator', 'all');
    }

}