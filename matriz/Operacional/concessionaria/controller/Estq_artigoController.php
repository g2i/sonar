<?php
final class Estq_artigoController extends AppController{ 

    # página inicial do módulo Estq_artigo
    function index(){
        $this->setTitle('Visualização de Artigos');
    }

    # lista de Estq_artigos
    # renderiza a visão /view/Estq_artigo/all.php
    function all(){
        $this->setTitle('Listagem de Artigos');
        $p = new Paginate('Estq_artigo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

            $c->setOrder('nome');

        $c->addCondition('origem','=',Config::get('origem')); //filtro
        $b = new Criteria();
        $b->addCondition('situacao','<>',3);
        $b->addCondition('origem','=',Config::get('origem'));
        $this->set('Estq_artigos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_subgrupos',  Estq_subgrupo::getList($b));
        $this->set('Estq_unidade',  Estq_unidade::getList());
    

    }

    function artigos(){
        $this->setTitle('Artigos');
        $a = new Criteria();
        $a->addCondition('situacao','<>',3);
        $a->addCondition('origem','=',2);
        $a->setOrder('nome asc');
        $this->set('Artigos',Estq_artigo::getList($a));
    }

    # visualiza um(a) Estq_artigo
    # renderiza a visão /view/Estq_artigo/view.php
    function view(){
        $this->setTitle('Visualização de Artigos');
        try {
            $this->set('Estq_artigo', new Estq_artigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo', 'all');
        }
    }

    # formulário de cadastro de Estq_artigo
    # renderiza a visão /view/Estq_artigo/add.php
    function add(){
        $this->setTitle('Cadastro de Artigos');
        $this->set('Estq_artigo', new Estq_artigo);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $c = new Criteria();
        $c->addCondition('origem','=',Config::get('origem'));
        $c->addCondition('situacao','<>',3);
        $this->set('Estq_subgrupos',  Estq_subgrupo::getList($c));
        $this->set('Estq_unidade',  Estq_unidade::getList());

    }

    # recebe os dados enviados via post do cadastro de Estq_artigo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Artigos');
        $Estq_artigo = new Estq_artigo();
        $this->set('Estq_artigo', $Estq_artigo);
        try {
            if (!empty($_POST['vlcomercial'])) {
                $_POST['vlcomercial'] = str_replace(".", "", $_POST['vlcomercial']);
                $_POST['vlcomercial'] = str_replace(",", ".", $_POST['vlcomercial']);
            }
            if (!empty($_POST['vlcusto'])) {
                $_POST['vlcusto'] = str_replace(".", "", $_POST['vlcusto']);
                $_POST['vlcusto'] = str_replace(",", ".", $_POST['vlcusto']);
            }
            if (!empty($_POST['vlcustomedio'])) {
                $_POST['vlcustomedio'] = str_replace(".", "", $_POST['vlcustomedio']);
                $_POST['vlcustomedio'] = str_replace(",", ".", $_POST['vlcustomedio']);
            }


            $_POST['origem']=Config::get('origem');//salvando a origem
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_artigo->usuario_id=$user; // para salvar o usuario que está fazendo

            if($_POST['controla_lote']==0) {
                $Estq_artigo->save($_POST);
                $Estq_artigo_lotes = new Estq_artigo_lote();
                $Estq_artigo_lotes->nome = "Unico";
                $Estq_artigo_lotes->artigo = $Estq_artigo->id;
                $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
                $Estq_artigo_lotes->usuario_id=$user; // para salvar o usuario que está fazendo
                $Estq_artigo_lotes->validade=2999-10-13;
                $Estq_artigo_lotes->situacao=1;
                $Estq_artigo_lotes->origem=Config::get('origem');//salvando a origem
                $Estq_artigo_lotes->sem_lote=1;
                $Estq_artigo_lotes->save();

                $a = new Criteria();
                $a->addCondition('origem','=',Config::get('origem'));
                $Estq_subestoques = Estq_subestoque::getList($a);
                foreach($Estq_subestoques as $e){
                    $Estq_lote_substoques = new Estq_lote_substoque();
                    $Estq_lote_substoques->lote=$Estq_artigo_lotes->id;
                    $Estq_lote_substoques->artigo=$Estq_artigo->id;
                    $Estq_lote_substoques->quantidade=0;
                    $Estq_lote_substoques->subestoque=$e->id;
                    $Estq_lote_substoques->estoque_minimo=0;
                    $Estq_lote_substoques->usuario_id=$user;
                    $Estq_lote_substoques->situacao=1;
                    $Estq_lote_substoques->origem=Config::get('origem');//salvando a origem
                    $Estq_lote_substoques->save();

                }

            }

            new Msg(__('Artigos cadastrado com sucesso'));
            $this->go('Estq_artigo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_subgrupos',  Estq_subgrupo::getList());
        $this->set('Estq_unidade',  Estq_unidade::getList());
    }

    # formulário de edição de Estq_artigo
    # renderiza a visão /view/Estq_artigo/edit.php
    function edit(){
        $this->setTitle('Edição de Artigos');
        try {
            $this->set('Estq_artigo', new Estq_artigo((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
            $c = new Criteria();
            $c->addCondition('origem','=',Config::get('origem')); //filtro
            $this->set('Estq_subgrupos',  Estq_subgrupo::getList($c));
            $this->set('Estq_unidade',  Estq_unidade::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_artigo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_artigo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_artigo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Artigos');
        try {
            $Estq_artigo = new Estq_artigo((int) $_POST['id']);
            $_POST['usuario_dt'] = date('Y-m-d H:i:s');

            if (!empty($_POST['vlcomercial'])) {
                $_POST['vlcomercial'] = str_replace(".", "", $_POST['vlcomercial']);
                $_POST['vlcomercial'] = str_replace(",", ".", $_POST['vlcomercial']);
            }

            if (!empty($_POST['vlcusto'])) {
                $_POST['vlcusto'] = str_replace(".", "", $_POST['vlcusto']);
                $_POST['vlcusto'] = str_replace(",", ".", $_POST['vlcusto']);
            }

            if (!empty($_POST['vlcustomedio'])) {
                $_POST['vlcustomedio'] = str_replace(".", "", $_POST['vlcustomedio']);
                $_POST['vlcustomedio'] = str_replace(",", ".", $_POST['vlcustomedio']);
            }

            $this->set('Estq_artigo', $Estq_artigo);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_artigo->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_artigo->save($_POST);
            new Msg(__('Artigo atualizado com sucesso'));
            $this->go('Estq_artigo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
        $this->set('Estq_subgrupos',  Estq_subgrupo::getList());
        $this->set('Estq_unidade',  Estq_unidade::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_artigo
    # renderiza a /view/Estq_artigo/delete.php
    function delete(){
            $this->setTitle('Apagar Artigo');
        try {
            $this->set('Estq_artigo', new Estq_artigo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_artigo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_artigo
    # redireciona para Estq_artigo/all
    function post_delete(){
        try {
            $Estq_artigo = new Estq_artigo((int) $_POST['id']);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_artigo->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_artigo->usuario_dt=date('Y-m-d H:i:s');
            $Estq_artigo->situacao=3;
            $a = new Criteria();
            $a->addCondition('artigo','=',$_POST['id']);
            $a->addCondition('situacao','=',1);
            $Estq_artigo_lotes = Estq_artigo_lote::getList($a);
            $a = new Criteria();
            $a->addCondition('artigo','=',$_POST['id']);
            $a->addCondition('situacao','=',1);
            $Estq_lote_substoques = Estq_lote_substoque::getList($a);
            foreach($Estq_artigo_lotes as $a) { //desativar os lotes tb
                if ( $a->artigo == $Estq_artigo->id) {
                    $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
                    $a->usuario_id = $user; // para salvar o usuario que está fazendo
                    $a->usuario_dt = date('Y-m-d H:i:s');
                    $a->situacao = 3;
                    foreach($Estq_lote_substoques as $b) { //desativar os lotes tb
                        if ( $b->artigo == $Estq_artigo->id) {
                            $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
                            $b->usuario_id = $user; // para salvar o usuario que está fazendo
                            $b->usuario_dt = date('Y-m-d H:i:s');
                            $b->situacao = 3;
                            $b->save();
                            $a->save();
                            $Estq_artigo->save();

                        }else{

                            $a->save();
                            $Estq_artigo->save();
                        }
                    }
                } else {
                    $Estq_artigo->save();

                }
            }
            new Msg(__('Artigo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_artigo', 'all');
    }

    function findArtigo(){
        $termo = $this->getParam('termo');
        $size = (int)$this->getParam('size');
        $page = (int)$this->getParam('page');

        if(!isset($termo))
            $termo = '';

        if(!isset($size) || $size < 1)
            $size = 10;

        if(!isset($page) || $page < 1)
            $page = 1;

        $origem = (int)Config::get('origem');

        $db = $this::getConn();
        $sql = "SELECT id, nome, codigo_livre as codigo FROM estq_artigo WHERE situacao != 3 AND origem = :ori AND (nome LIKE :nome OR codigo_livre = :cod)";
        $db->query($sql);
        $db->bind(":ori", $origem, PDO::PARAM_INT);
        $db->bind(":nome", $termo."%", PDO::PARAM_STR);
        $db->bind(":cod", $termo, PDO::PARAM_STR);
        $db->execute();

        $ret = array();
        $ret["total"] = $db->rowCount();
        $ret["dados"] = array();

        $sql = $sql." LIMIT :li OFFSET :off";
        $db->query($sql);
        $db->bind(":ori", $origem, PDO::PARAM_INT);
        $db->bind(":nome", $termo."%", PDO::PARAM_STR);
        $db->bind(":cod", $termo, PDO::PARAM_STR);
        $db->bind(":li", $size, PDO::PARAM_INT);
        $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

        $dados = $db->getResults();

        foreach($dados as $d){
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome, 'codigo' => $d->codigo);
        }

        echo json_encode($ret);
        exit;
    }

    function findArtigoMov(){
        $termo = $this->getParam('termo');
        $size = (int)$this->getParam('size');
        $page = (int)$this->getParam('page');
        $mov = (int)$this->getParam('mov');

        if(!isset($termo))
            $termo = '';

        if(!isset($size) || $size < 1)
            $size = 10;

        if(!isset($page) || $page < 1)
            $page = 1;

        $origem = (int)Config::get('origem');

        $db = $this::getConn();
        $sql = "SELECT a.id, a.nome FROM estq_artigo as a
                INNER JOIN estq_movdetalhes as m ON (m.artigo = a.id)
                WHERE m.movimento = :mov AND a.situacao != 3 AND a.origem = :ori
                AND (a.nome LIKE :nome OR a.codigo_livre = :cod)";

        $db->query($sql);
        $db->bind(":mov", $mov, PDO::PARAM_INT);
        $db->bind(":ori", $origem, PDO::PARAM_INT);
        $db->bind(":nome", $termo."%", PDO::PARAM_STR);
        $db->bind(":cod", $termo, PDO::PARAM_STR);
        $db->execute();

        $ret = array();
        $ret["total"] = $db->rowCount();
        $ret["dados"] = array();

        $sql = $sql." LIMIT :li OFFSET :off";
        $db->query($sql);
        $db->bind(":mov", $mov, PDO::PARAM_INT);
        $db->bind(":ori", $origem, PDO::PARAM_INT);
        $db->bind(":nome", $termo."%", PDO::PARAM_STR);
        $db->bind(":cod", $termo, PDO::PARAM_STR);
        $db->bind(":li", $size, PDO::PARAM_INT);
        $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

        $dados = $db->getResults();

        foreach($dados as $d){
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }

        echo json_encode($ret);
        exit;
    }

    function getArtigo(){
        $origem = (int)Config::get('origem');
        $id = (int)$this->getParam('id');

        $db = $this::getConn();
        $sql = "SELECT id, nome, codigo_livre as codigo FROM estq_artigo WHERE situacao != 3 AND origem = :ori AND id = :cod";
        $db->query($sql);
        $db->bind(":ori", $origem, PDO::PARAM_INT);
        $db->bind(":cod", $id, PDO::PARAM_INT);
        $db->execute();

        $dados = $db->getResults();
        echo json_encode($dados);
        exit;
    }


    function preencher(){
        exit;
        $a = new Criteria();
        $a->addCondition('situacao','=',1);
        $artigos = Estq_artigo::getList($a);
        foreach ($artigos as $artigo) {
            $Estq_artigo_lotes = new Estq_artigo_lote();
            $Estq_artigo_lotes->nome = "Unico";
            $Estq_artigo_lotes->artigo = $artigo->id;
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_artigo_lotes->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_artigo_lotes->validade=2200-12-31;
            $Estq_artigo_lotes->situacao=1;
            $Estq_artigo_lotes->origem=Config::get('origem');//salvando a origem
            $Estq_artigo_lotes->sem_lote=1;
            $Estq_artigo_lotes->save();

        }
    exit;

    }

    function insertsubestoque(){
        exit;
        $Estq_artigo_lotes = Estq_artigo_lote::getList();
            foreach ($Estq_artigo_lotes as $artigolote) {
                $Estq_lote_substoques = new Estq_lote_substoque();
                $Estq_lote_substoques->lote = $artigolote->id;
                $Estq_lote_substoques->artigo = $artigolote->artigo;
                $Estq_lote_substoques->quantidade = 0;
                $Estq_lote_substoques->subestoque = 3;
                $Estq_lote_substoques->estoque_minimo = 0;
                $Estq_lote_substoques->usuario_id = 1;
                $Estq_lote_substoques->situacao = 1;
                $Estq_lote_substoques->origem = Config::get('origem');//salvando a origem
                $Estq_lote_substoques->save();
            }
    }

    function duplicidade(){
        $tipo = $_POST['tipo'];
        $id = $_POST['id'];
        $codigo= $_POST['codigo'];
        $a = new Criteria();
        $a->addCondition('codigo_livre','=',$codigo);

        if($tipo=='edit'){
            $a->addCondition('id','<>',$id);
        }

        $a->addCondition('situacao','=',1);
        $artigos = Estq_artigo::getList($a);

        header('Content-Type: application/json');
        echo json_encode(array('result'=>count($artigos)));
        exit;
    }


    function criar_lote(){

        $artigos = $this->query("SELECT a.id FROM estq_artigo a WHERE NOT a.id IN(SELECT l.`artigo` FROM estq_lote_substoque l
WHERE l.`origem` = 1 AND l.`situacao` = 1 GROUP BY l.`artigo`) AND a.`origem` = 1 AND a.`situacao`=1");

        foreach ($artigos as $a){
            $b = new Criteria();
            $b->addCondition('situacao','=',1);
            $b->addCondition('origem','=',Config::get('origem'));
            $b->addCondition('artigo','=',$a->id);
            $Estq_artigo_lotes = new Estq_artigo_lote($b);
                $user = @Session::get("user")->id;// para salvar o usuario que está fazendo
            if(empty($Estq_artigo_lotes)) {
                $Estq_artigo_lotes->nome = "Unico";
                $Estq_artigo_lotes->artigo = $a->id;
                $Estq_artigo_lotes->usuario_id = $user; // para salvar o usuario que está fazendo
                $Estq_artigo_lotes->validade = '2200-10-13';
                $Estq_artigo_lotes->situacao = 1;
                $Estq_artigo_lotes->origem = Config::get('origem');//salvando a origem
                $Estq_artigo_lotes->sem_lote = 1;
                $Estq_artigo_lotes->save();
            }
            $b = new Criteria();
            $b->addCondition('origem','=',Config::get('origem'));
            $Estq_subestoques = Estq_subestoque::getList($b);
            foreach($Estq_subestoques as $e){
                $Estq_lote_substoques = new Estq_lote_substoque();
                $Estq_lote_substoques->lote=$Estq_artigo_lotes->id;
                $Estq_lote_substoques->artigo=$a->id;
                $Estq_lote_substoques->quantidade=0;
                $Estq_lote_substoques->subestoque=$e->id;
                $Estq_lote_substoques->estoque_minimo=0;
                $Estq_lote_substoques->usuario_id=$user;
                $Estq_lote_substoques->situacao=1;
                $Estq_lote_substoques->origem=Config::get('origem');//salvando a origem
                $Estq_lote_substoques->save();

            }
        }
        exit;
    }
}
