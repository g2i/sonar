<?php

final class ClienteController extends AppController
{

    public function listaEnderecos()
    {
        $cliente_id = $_GET['cliente_id'];
        $enderecoCriteria = new Criteria();
        $enderecoCriteria->addCondition('cliente_id', '=', $cliente_id);
        $enderecoCriteria->addCondition('status', '=', 1);

        $enderecos = Cliente_endereco::getList($enderecoCriteria);
        $dados = array();
        foreach ($enderecos AS $value) {
            $dados[] = array(
                'id' => $value->id,
                'endereco' => $value->endereco . ', n°' . $value->numero . ',' . $value->bairro . '/' . $value->cidade . ' - ' . $value->uf,
            );
        }
        echo json_encode($dados);
        exit;
    }

    public function buscaEndereco()
    {
        $id = $_GET['cliente_endereco_id'];
        $enderecoCriteria = new Criteria();
        $enderecoCriteria->addCondition('id', '=', $id);
        $enderecos = Cliente_endereco::getFirst($enderecoCriteria);
        $dados = array(
            'id' => $enderecos->id,
            'cep' => $enderecos->cep,
            'endereco' => $enderecos->endereco,
            'numero' =>  $enderecos->numero,
            'bairro' => $enderecos->bairro ,
            'cidade' => $enderecos->cidade ,
            'uf' => $enderecos->uf,
            'complemento' => $enderecos->complemento
        );

        echo json_encode($dados);
        exit;
    }

}