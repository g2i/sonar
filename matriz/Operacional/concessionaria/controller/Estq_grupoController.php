<?php
final class Estq_grupoController extends AppController{ 

    # página inicial do módulo Estq_grupo
    function index(){
        $this->setTitle('Visualização de Grupos');
    }

    # lista de Estq_grupos
    # renderiza a visão /view/Estq_grupo/all.php
    function all(){
        $this->setTitle('Listagem de Grupos');
        $p = new Paginate('Estq_grupo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('origem','=',Config::get('origem')); //filtro
        $this->set('Estq_grupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Estq_grupo
    # renderiza a visão /view/Estq_grupo/view.php
    function view(){
        $this->setTitle('Visualização de Grupos');
        try {
            $this->set('Estq_grupo', new Estq_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_grupo', 'all');
        }
    }

    # formulário de cadastro de Estq_grupo
    # renderiza a visão /view/Estq_grupo/add.php
    function add(){
        $this->setTitle('Cadastro de Grupos');
        $this->set('Estq_grupo', new Estq_grupo);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_grupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Grupos');
        $Estq_grupo = new Estq_grupo();
        $this->set('Estq_grupo', $Estq_grupo);
        try {
            $_POST['origem']=Config::get('origem');//salvando a origem
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_grupo->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_grupo->usuario_dt=date('Y-m-d H:i:s');
            $Estq_grupo->save($_POST);
            new Msg(__('Grupo cadastrado com sucesso'));
            $this->go('Estq_grupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição de Estq_grupo
    # renderiza a visão /view/Estq_grupo/edit.php
    function edit(){
        $this->setTitle('Edição de Grupos');
        try {
            $this->set('Estq_grupo', new Estq_grupo((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_grupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Estq_grupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Grupos');
        try {
            $Estq_grupo = new Estq_grupo((int) $_POST['id']);
            $this->set('Estq_grupo', $Estq_grupo);
            $Estq_grupo->usuario_dt=date('Y-m-d H:i:s');
            $Estq_grupo->save($_POST);
            new Msg(__('Grupo atualizado com sucesso'));
            $this->go('Estq_grupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_grupo
    # renderiza a /view/Estq_grupo/delete.php
    function delete(){
        $this->setTitle('Apagar Grupo');
        try {
            $this->set('Estq_grupo', new Estq_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_grupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_grupo
    # redireciona para Estq_grupo/all
    function post_delete(){
        try {
            $Estq_grupo = new Estq_grupo((int) $_POST['id']);
            $Estq_grupo->usuario_dt=date('Y-m-d H:i:s');
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_grupo->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_grupo->situacao=3;
            $Estq_grupo->save();
            new Msg(__('Grupo deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_grupo', 'all');
    }

}