<?php
final class Equipe_movimentoController extends AppController{ 

    # página inicial do módulo Equipe_movimento
    function index(){
        $this->setTitle('Visualização de Movimento Equipe');
    }

    # lista de Equipe_movimentos
    # renderiza a visão /view/Equipe_movimento/all.php
    function all(){
        $this->setTitle('Listagem de Movimento Equipe');
        $p = new Paginate('Equipe_movimento', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('movimento')) {
            $c->addCondition('movimento_id','=',$this->getParam('movimento'));
        }
        $c->addCondition('situacao','<>',3);

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Equipe_movimentos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Equipes',  Equipe::getList());
        $this->set('Estq_movs',  Estq_mov::getList());
        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Equipe_movimento
    # renderiza a visão /view/Equipe_movimento/view.php
    function view(){
        $this->setTitle('Visualização de Movimento Equipe');
        try {
            $this->set('Equipe_movimento', new Equipe_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Equipe_movimento', 'all');
        }
    }

    # formulário de cadastro de Equipe_movimento
    # renderiza a visão /view/Equipe_movimento/add.php
    function add(){
        $this->setTitle('Cadastro de Movimento Equipe');
        $this->set('Equipe_movimento', new Equipe_movimento);
        $this->set('Equipes',  Equipe::getList());
        $this->set('Estq_movs',  Estq_mov::getList());
        $this->set('Situacaos',  Situacao::getList());
       
    }

    # recebe os dados enviados via post do cadastro de Equipe_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Equipe_movimento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Movimento Equipe');
        $Equipe_movimento = new Equipe_movimento();
        $this->set('Equipe_movimento', $Equipe_movimento);
        try {
            $Equipe_movimento->save($_POST);
            new Msg(__('Equipe_movimento cadastrado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('Equipe_movimento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Equipes',  Equipe::getList());
        $this->set('Estq_movs',  Estq_mov::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Equipe_movimento
    # renderiza a visão /view/Equipe_movimento/edit.php
    function edit(){
        $this->setTitle('Edição de Movimento Equipe');
        try {
            $this->set('Equipe_movimento', new Equipe_movimento((int) $this->getParam('id')));
            $this->set('Equipes',  Equipe::getList());
            $this->set('Estq_movs',  Estq_mov::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Equipe_movimento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Equipe_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Equipe_movimento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Movimento Equipe');
        try {
            $Equipe_movimento = new Equipe_movimento((int) $_POST['id']);
            $this->set('Equipe_movimento', $Equipe_movimento);
            $Equipe_movimento->save($_POST);
            new Msg(__('Equipe_movimento atualizado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('Equipe_movimento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Equipes',  Equipe::getList());
        $this->set('Estq_movs',  Estq_mov::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Equipe_movimento
    # renderiza a /view/Equipe_movimento/delete.php
    function delete(){
        $this->setTitle('Apagar Equipe_movimento');
        try {
            $this->set('Equipe_movimento', new Equipe_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Equipe_movimento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Equipe_movimento
    # redireciona para Equipe_movimento/all
    function post_delete(){
        try {
            $Equipe_movimento = new Equipe_movimento((int) $_POST['id']);
            $Equipe_movimento->situacao=3;
            $Equipe_movimento->save();
            new Msg(__('Equipe_movimento apagado com sucesso'), 1);
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Equipe_movimento', 'all');
    }

}