<?php

final class Concessionaria_item_servicoController extends AppController
{

    # página inicial do módulo Concessionaria_item_servico
    function index()
    {
        $this->setTitle('Visualização de Concessionaria_item_servico');
    }

    # lista de Concessionaria_item_servicos
    # renderiza a visão /view/Concessionaria_item_servico/all.php
    function all()
    {

        $this->setTitle('Listagem de Concessionaria_item_servico');

        $this->setStyles(SITE_PATH . '/content/css/select2.min.css');
        $this->setStyles(SITE_PATH . '/content/css/select2-bootstrap.min.css');
        $this->setScript(SITE_PATH . '/content/js/select2.min.js');
        $this->setScript(SITE_PATH . '/content/js/i18n/pt-BR.js');
        $this->setScript(SITE_PATH . '/content/js/bootstrap-dialog.min.js');

        $p = new Paginate('Concessionaria_item_servico', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c = new Criteria();
        $c->addCondition('estq_mov_id', '=', $this->getParam('movimento'));
        $c->addCondition('situacao_id', '=', 1);
        $this->set('Concessionaria_item_servicos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Concessionaria_item_servico
    # renderiza a visão /view/Concessionaria_item_servico/view.php
    function view()
    {
        $this->setTitle('Visualização de Concessionaria_item_servico');
        try {
            $this->set('Concessionaria_item_servico', new Concessionaria_item_servico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_item_servico', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_item_servico
    # renderiza a visão /view/Concessionaria_item_servico/add.php
    function add()
    {
        $this->setTitle('Cadastro de Concessionaria_item_servicos');

        $this->set('Concessionaria_item_servico', new Concessionaria_item_servico);
        $this->set('Estq_movs', Estq_mov::getList());
        $queryFatores = new Criteria();
        $queryFatores->addCondition("situacao_id", "=", 1);
        $queryServico = new Criteria();
        $queryServico->addCondition("situacao_id", "=", 1);
        $queryServico->addSqlConditions('(concessionaria_servico.uso_servico = 1 OR concessionaria_servico.uso_servico = 3)');

        $this->set('Concessionaria_fatores', Concessionaria_fator::getList($queryFatores));
        $queryServico->setOrder('codigo_livre');
        $this->set('Concessionaria_servicosCodigo', Concessionaria_servico::getList($queryServico));
        $this->set('Situacaos', Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_item_servico
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_item_servico/add.php
    function post_add()
    {

        $this->beginTransaction();
        $this->setTitle('Cadastro de Concessionaria_item_servico');
        $Concessionaria_item_servico = new Concessionaria_item_servico();
        $this->set('Concessionaria_item_servico', $Concessionaria_item_servico);
        $Concessionaria_item_servico->valor_total = $_POST['valor_atual_conversao'] * (float)$_POST['quantidade'];
        $user = Session::get('user');// para salvar o usuario que está fazendo
        $Concessionaria_item_servico->situacao_id = 1;
        $Concessionaria_item_servico->cadastrado_por = $user->id;
        $Concessionaria_item_servico->cadastrado_em = date('Y-m-d H:i:s');

        try {
            $Concessionaria_item_servico->save($_POST);

            //calcula o valor de cis_valor_ost_final
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$_POST['estq_mov_id']);

            $item_servico = Concessionaria_item_servico::getList($queryFatorFunc);
            $total = 0;
            foreach ($item_servico AS $se) {
                $total += $se->valor_total;
            }

            if ($total > 0) {
                $estq_mov = new Estq_mov($_POST['estq_mov_id']);
                $estq_mov->cis_valor_os_final = $total;
                $estq_mov->save();
            }
            $this->endTransaction();
            new Msg(__('item servico cadastrado com sucesso'));
        } catch (Exception $e) {
            $this->cancelTransaction();
            new Msg($e->getMessage(), 3);
        }
        echo 1;
        exit;

    }

    # formulário de edição de Concessionaria_item_servico
    # renderiza a visão /view/Concessionaria_item_servico/edit.php
    function edit()
    {
        $this->setTitle('Edição de Concessionaria_item_servicos');

        try {
            $this->set('Concessionaria_item_servico', new Concessionaria_item_servico((int)$this->getParam('id')));
            $this->set('Estq_movs', Estq_mov::getList());
            $queryFatores = new Criteria();
            $queryFatores->addCondition("situacao_id", "=", 1);
            $queryServico = new Criteria();
            $queryServico->addCondition("situacao_id", "=", 1);
            $this->set('Concessionaria_fatores', Concessionaria_fator::getList($queryFatores));
            $queryServico->addSqlConditions('(concessionaria_servico.uso_servico = 1 OR concessionaria_servico.uso_servico = 3)');
            $queryServico->setOrder('codigo_livre');
            $this->set('Concessionaria_servicosCodigo', Concessionaria_servico::getList($queryServico));
            $this->set('Situacaos', Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Concessionaria_item_servicos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_item_servico
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_item_servico/edit.php
    function post_edit()
    {
        $this->beginTransaction();
        $this->setTitle('Edição de Concessionaria_item_servico');
        try {

            $Concessionaria_item_servico = new Concessionaria_item_servico((int)$_POST['id']);
            $this->set('Concessionaria_item_servico', $Concessionaria_item_servico);

            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Concessionaria_item_servico->alterado_por = $user->id;
            $Concessionaria_item_servico->alterado_em = date('Y-m-d H:i:s');
            $Concessionaria_item_servico->valor_total = $_POST['valor_atual_conversao'] * (float)$_POST['quantidade'];
            $Concessionaria_item_servico->save($_POST);

            //calcula o valor de cis_valor_ost_final
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', $Concessionaria_item_servico->estq_mov_id);
            $item_servico = Concessionaria_item_servico::getList($queryFatorFunc);
            $total = 0;
            foreach ($item_servico AS $se) {
                $total += $se->valor_total;
            }


            $estq_mov = new Estq_mov($Concessionaria_item_servico->estq_mov_id);
            $estq_mov->cis_valor_os_final = $total;
            $estq_mov->save();


            $this->endTransaction();
            new Msg(__('Item Servico atualizado com sucesso'));
        } catch (Exception $e) {
            $this->cancelTransaction();
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        echo 1;
        exit;

    }

    # Confirma a exclusão ou não de um(a) Concessionaria_item_servico
    # renderiza a /view/Concessionaria_item_servico/delete.php
    function delete()
    {
        $this->setTitle('Apagar Concessionaria_item_servico');
        try {
            $this->set('Concessionaria_item_servico', new Concessionaria_item_servico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_item_servico', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_item_servico
    # redireciona para Concessionaria_item_servico/all
    function post_delete()
    {
        $this->beginTransaction();
        try {
            $Concessionaria_item_servico = new Concessionaria_item_servico((int)$_POST['id']);
            $Concessionaria_item_servico->situacao_id = 3;
            $Concessionaria_item_servico->save();

            //calcula o valor de cis_valor_ost_final
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$Concessionaria_item_servico->estq_mov_id);
            $item_servico = Concessionaria_item_servico::getList($queryFatorFunc);
            $total = 0;
            foreach ($item_servico AS $se) {
                $total += $se->valor_total;
            }


            $estq_mov = new Estq_mov($Concessionaria_item_servico->estq_mov_id);
            $estq_mov->cis_valor_os_final = $total;
            $estq_mov->save();


            $this->endTransaction();
            new Msg(__('Item servico apagado com sucesso'), 1);
        } catch (Exception $e) {
            $this->cancelTransaction();
            new Msg($e->getMessage(), 3);
        }
        echo 1;
        exit;
    }


    function add_servico_adicional()
    {
        $queryFatores = new Criteria();
        $queryFatores->addCondition("situacao_id", "=", 1);
        $queryServico = new Criteria();
        $queryServico->addCondition("situacao_id", "=", 1);
        $this->set('Concessionaria_fatores', Concessionaria_fator::getList($queryFatores));
        $queryServico->setOrder('codigo_livre');
        $queryServico->addSqlConditions('(concessionaria_servico.uso_servico = 2 OR concessionaria_servico.uso_servico = 3)');

        $this->set('Concessionaria_servicosCodigo', Concessionaria_servico::getList($queryServico));
        $queryItemServico = new Criteria();
        $queryItemServico->addCondition('situacao_id', '=', 1);
        $queryItemServico->addCondition('item_pai', '=', $this->getParam('id'));
        $this->set('Concessionaria_item_servicos', Concessionaria_item_servico::getList($queryItemServico));
    }

    function post_add_servico_adicional()
    {
        try {
            $data = array();
            $this->beginTransaction();
            $this->setTitle('Cadastro de Concessionaria_item_servico');
            $Concessionaria_item_servico = new Concessionaria_item_servico();
            $this->set('Concessionaria_item_servico', $Concessionaria_item_servico);
            $servico_id = $_POST['servico_id'];
            //salva a US no item servico que foi cadastrada no servico

            $servico = new Concessionaria_servico((int)$servico_id);
            $Concessionaria_item_servico->us = $servico->us;
            $Concessionaria_item_servico->fator_us = $servico->fator_us;

            $fator = new Concessionaria_fator($servico->us);
            $Concessionaria_item_servico->valor_atual_conversao =
            $Concessionaria_item_servico->valor_total_item_filho = $fator->valor * (float)$_POST['quantidade'];
            $Concessionaria_item_servico->valor_total_sem_adicional = $fator->valor * (float)$_POST['quantidade'];
            $data['fator_us'] = '----';
            if ($servico->us == 1) {
                $Concessionaria_item_servico->valor_total_item_filho *= $servico->fator_us;
                $Concessionaria_item_servico->valor_total_sem_adicional *= $servico->fator_us;
                $data['fator_us'] = number_format($servico->fator_us, 2, ',', '.');
            }

            $Concessionaria_item_servico->valor_total = 0;
            $Concessionaria_item_servico->valor_total_item_filho *= $_POST['adicional_servico'];
            $Concessionaria_item_servico->valor_total_diferenca_item_filho = $Concessionaria_item_servico->valor_total_item_filho - $Concessionaria_item_servico->valor_total_sem_adicional;

            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Concessionaria_item_servico->situacao_id = 1;
            $Concessionaria_item_servico->cadastrado_por = $user->id;
            $Concessionaria_item_servico->cadastrado_em = date('Y-m-d H:i:s');


            $Concessionaria_item_servico->save($_POST);

//            //calcula o valor de valor total item pai
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$_POST['estq_mov_id']);
            $queryFatorFunc->addCondition('item_pai', '=', (int)$_POST['item_pai']);

            $item_serviço = Concessionaria_item_servico::getList($queryFatorFunc);
            $total = 0;
            foreach ($item_serviço AS $se) {
                $total += $se->valor_total_diferenca_item_filho;
            }


            $Concessionaria_item_pai = new Concessionaria_item_servico($_POST['item_pai']);
            $Concessionaria_item_pai->valor_total = $total;
            $Concessionaria_item_pai->save();


            //calcula o valor cis_valor_os_final
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$_POST['estq_mov_id']);

            $item_serviço = Concessionaria_item_servico::getList($queryFatorFunc);
            $totalMov = 0;
            foreach ($item_serviço AS $se) {
                $totalMov += $se->valor_total;
            }


            $estq_mov = new Estq_mov($_POST['estq_mov_id']);
            $estq_mov->cis_valor_os_final = $totalMov;
            $estq_mov->save();


            //retorno do json
            $data['nome'] = $servico->nome;
            $data['quantidade'] = $_POST['quantidade'];
            $data['valor_atual_conversao'] = number_format($fator->valor, 2, ',', '.');
            $data['valor_total_sem_adicional'] = number_format($Concessionaria_item_servico->valor_total_sem_adicional, 2, ',', '.');
            $data['valor_total_item_filho'] = number_format($Concessionaria_item_servico->valor_total_item_filho, 2, ',', '.');
            $data['valor_total_diferenca_item_filho'] = number_format($Concessionaria_item_servico->valor_total_diferenca_item_filho, 2, ',', '.');
            $data['adicional_servico'] = (($_POST['adicional_servico'] * 100) - 100) . '%';
            $data['id'] = $Concessionaria_item_servico->id;
            $data['total'] = number_format($total, 2, ',', '.');
            $data['msg'] = 'Servico adicional salvo com sucesso';
            http_response_code(200);
            echo json_encode($data);
            $this->endTransaction();
            exit();
        } catch (Exception $e) {
            $this->cancelTransaction();
            http_response_code(500);
            $data = array(
                'msg' => 'Houve uma falha ao tentar salvar!',
            );
            echo json_encode($data);
            exit();
        }
    }

    function post_edit_servico_adicional()
    {
        try {
            $data = array();
            $this->beginTransaction();
            $this->setTitle('Editar Concessionária item de servico');
            $Concessionaria_item_servico = new Concessionaria_item_servico();
            $this->set('Concessionaria_item_servico', $Concessionaria_item_servico);
            $servico_id = $_POST['servico_id'];
            //salva a US no item servico que foi cadastrada no servico

            $servico = new Concessionaria_servico((int)$servico_id);
            $Concessionaria_item_servico->us = $servico->us;
            $Concessionaria_item_servico->fator_us = $servico->fator_us;

            $fator = new Concessionaria_fator($servico->us);
            $Concessionaria_item_servico->valor_atual_conversao =
            $Concessionaria_item_servico->valor_total_item_filho = $fator->valor * (float)$_POST['quantidade'];
            $Concessionaria_item_servico->valor_total_sem_adicional = $fator->valor * (float)$_POST['quantidade'];
            $data['fator_us'] = '----';
            if ($servico->us == 1) {
                $Concessionaria_item_servico->valor_total_item_filho *= $servico->fator_us;
                $Concessionaria_item_servico->valor_total_sem_adicional *= $servico->fator_us;
                $data['fator_us'] = number_format($servico->fator_us, 2, ',', '.');
            }

            $Concessionaria_item_servico->valor_total = 0;
            $Concessionaria_item_servico->valor_total_item_filho *= $_POST['adicional_servico'];
            $Concessionaria_item_servico->valor_total_diferenca_item_filho = $Concessionaria_item_servico->valor_total_item_filho - $Concessionaria_item_servico->valor_total_sem_adicional;

            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Concessionaria_item_servico->situacao_id = 1;
            $Concessionaria_item_servico->cadastrado_por = $user->id;
            $Concessionaria_item_servico->cadastrado_em = date('Y-m-d H:i:s');


            $Concessionaria_item_servico->save($_POST);

//            //calcula o valor de valor total item pai
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$_POST['estq_mov_id']);
            $queryFatorFunc->addCondition('item_pai', '=', (int)$_POST['item_pai']);

            $item_serviço = Concessionaria_item_servico::getList($queryFatorFunc);
            $total = 0;
            foreach ($item_serviço AS $se) {
                $total += $se->valor_total_diferenca_item_filho;
            }


            $Concessionaria_item_pai = new Concessionaria_item_servico($_POST['item_pai']);
            $Concessionaria_item_pai->valor_total = $total;
            $Concessionaria_item_pai->save();


            //calcula o valor cis_valor_os_final
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$_POST['estq_mov_id']);

            $item_serviço = Concessionaria_item_servico::getList($queryFatorFunc);
            $totalMov = 0;
            foreach ($item_serviço AS $se) {
                $totalMov += $se->valor_total;
            }


            $estq_mov = new Estq_mov($_POST['estq_mov_id']);
            $estq_mov->cis_valor_os_final = $totalMov;
            $estq_mov->save();


            //retorno do json
            $data['nome'] = $servico->nome;
            $data['quantidade'] = $_POST['quantidade'];
            $data['valor_atual_conversao'] = number_format($fator->valor, 2, ',', '.');
            $data['valor_total_sem_adicional'] = number_format($Concessionaria_item_servico->valor_total_sem_adicional, 2, ',', '.');
            $data['valor_total_item_filho'] = number_format($Concessionaria_item_servico->valor_total_item_filho, 2, ',', '.');
            $data['valor_total_diferenca_item_filho'] = number_format($Concessionaria_item_servico->valor_total_diferenca_item_filho, 2, ',', '.');
            $data['adicional_servico'] = (($_POST['adicional_servico'] * 100) - 100) . '%';
            $data['id'] = $Concessionaria_item_servico->id;
            $data['total'] = number_format($total, 2, ',', '.');
            $data['msg'] = 'Serviço adicional alterado com sucesso';
            http_response_code(200);
            echo json_encode($data);
            $this->endTransaction();
            exit();
        } catch (Exception $e) {
            $this->cancelTransaction();
            http_response_code(500);
            $data = array(
                'msg' => 'Houve uma falha ao tentar editar!',
            );
            echo json_encode($data);
            exit();
        }
    }

    function post_delete_adicional()
    {
        try {
            $this->beginTransaction();
            $Concessionaria_item_servico = new Concessionaria_item_servico((int)$_POST['id']);
            $Concessionaria_item_servico->situacao_id = 3;
            $Concessionaria_item_servico->save();

            //            //calcula o valor de valor total do pai
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$_POST['estq_mov_id']);
            $queryFatorFunc->addCondition('item_pai', '=', (int)$_POST['item_pai']);

            $item_serviço = Concessionaria_item_servico::getList($queryFatorFunc);
            $total = 0;
            foreach ($item_serviço AS $se) {
                $total += $se->valor_total_diferenca_item_filho;
            }

            $Concessionaria_item_pai = new Concessionaria_item_servico($_POST['item_pai']);
            $Concessionaria_item_pai->valor_total = $total;
            $Concessionaria_item_pai->save();


            //calcula o valor de cis_valor_ost_final
            $queryFatorFunc = new Criteria();
            $queryFatorFunc->addCondition('situacao_id', '=', 1);
            $queryFatorFunc->addCondition('estq_mov_id', '=', (int)$Concessionaria_item_servico->estq_mov_id);
            $item_serviço = Concessionaria_item_servico::getList($queryFatorFunc);
            $totalmov = 0;
            foreach ($item_serviço AS $se) {
                $totalmov += $se->valor_total;
            }


            $estq_mov = new Estq_mov($Concessionaria_item_servico->estq_mov_id);
            $estq_mov->cis_valor_os_final = $totalmov;
            $estq_mov->save();


            $this->endTransaction();
            http_response_code(200);
            $data = array(
                'msg' => 'Servico adicional apagado com sucesso!',
            );
            $data['total'] = number_format($total, 2, ',', '.');
            echo json_encode($data);
            exit();

        } catch (Exception $e) {
            $this->cancelTransaction();
            http_response_code(500);
            $data = array(
                'msg' => 'Houve uma falha ao tentar apagar!',
            );
            echo json_encode($data);
            exit();
        }

    }
}