<?php

final class RelatoriosController extends AppController
{

    # página inicial do módulo Estq_users
    function all()
    {
        /*$this->setTitle('Lista de relatórios');*/

        $this->setTitle('Listagem de O.S');
        $p = new Paginate('Estq_mov', 20);
        $Estq_movsCriteria = new Criteria();
        $this->set('sort', 'ASC');
        $Estq_movsCriteria->addJoin('estq_movdetalhes', 'estq_movdetalhes.movimento', 'estq_mov.id', 'LEFT');
        $Estq_movsCriteria->addJoin('estq_artigo', 'estq_movdetalhes.artigo', 'estq_artigo.id', 'LEFT');
        $Estq_movsCriteria->addJoin('c_complemento_mov', 'c_complemento_mov.mov_detalhes', 'estq_movdetalhes.id', 'LEFT');
        $Estq_movsCriteria->setCont("DISTINCT(estq_mov.`id`)");

        if (!empty($_GET["filtro"])) {
            if (!empty($_GET["filtro"]["interno"])) {
                foreach ($_GET["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $Estq_movsCriteria->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }

            if (!empty($_GET["filtro"]["externo"])) {
                foreach ($_GET["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $Estq_movsCriteria->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        if (!empty($_GET['data_cautela'])) {
            $this->setParam('data_cautela', $_GET['data_cautela']);
            $Estq_movsCriteria->addSqlConditions(" DATE_FORMAT(c_complemento_mov.`data`,'%d/%m/%Y') = '" . $_GET['data_cautela'] . "'");
        }

        if (!empty($_GET['inicio']) && !empty($_GET['fim'])) {
            $tipoData = $_GET['tipo_data'];
            $inicio = DataSQL($_GET['inicio']);
            $fim = DataSQL($_GET['fim']);
            $Estq_movsCriteria->addCondition($tipoData, ">=", $inicio);
            $Estq_movsCriteria->addCondition($tipoData, "<=", $fim);
        }

        $Estq_movsCriteria->setGroup('estq_mov.id');

        if ($this->getParam('orderBy')) {
            if ($this->getParam('sort') == 'ASC') {
                $this->set('sort', 'DESC');
            } else {
                $this->set('sort', 'ASC');
            }
            $Estq_movsCriteria->setOrder($this->getParam('orderBy') . ' ' . $this->getParam('sort'));
        } else {
            $Estq_movsCriteria->setOrder('id desc');
        }
        $Estq_movsCriteria->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $Estq_movsCriteria->addCondition('situacao', '=', 1); //filtro


        $ClientesCriteria = new Criteria();
        $ClientesCriteria->addCondition('status', '=', 1);
        $ClientesCriteria->setOrder('nome');

        $FornecedorsCriteria = new Criteria();
        $FornecedorsCriteria->addCondition('situacao', '=', 1);
        $FornecedorsCriteria->setOrder('nome');

        $Estq_tipomovsCriteria = new Criteria();
        $Estq_tipomovsCriteria->addCondition('origem', '=', Config::get('origem')); //filtro Origem
        $Estq_tipomovsCriteria->addCondition('situacao', '=', 1); //filtro
        $Estq_tipomovsCriteria->setOrder('nome');

        $RhprofissionalCriteria = new Criteria();
        $RhprofissionalCriteria->addCondition('status', '=', 1);
        $RhprofissionalCriteria->setOrder('nome');

        $queryAndamento = new Criteria();
        $queryAndamento->addCondition('situacao', '=', 1);
        $queryAndamento->addCondition('origem', '=', Config::get('origem')); //filtro Origem

        $queryConcessionariaSituacao = new Criteria();
        $queryConcessionariaSituacao->addCondition('situacao', '=', 1);
        $queryConcessionariaSituacao->addCondition('origem', '=', Config::get('origem')); //filtro Origem

        $equipeCondition = new Criteria();
        $equipeCondition->addCondition('situacao', '=', 1);
        $tipoServicoCondition = new Criteria();
        $tipoServicoCondition->addCondition('situacao', '=', 1);
        $tipoServicoCondition->setOrder('nome');


        $this->set('Equipes', Equipe::getList($equipeCondition));
        $this->set('Clientes', Cliente::getList($ClientesCriteria));
        $this->set('TiposServicos', Concessionaria_tipo_servico::getList($tipoServicoCondition));
        $this->set('Fornecedors', Fornecedor::getList($FornecedorsCriteria));
        $this->set('Estq_situacaos', Estq_situacao::getList());
        $this->set('Estq_tipomovs', Estq_tipomov::getList($Estq_tipomovsCriteria));
        $this->set('Rhprofissionals', Rhprofissional::getList($RhprofissionalCriteria));
        $this->set('ConcessionariAandamento', Concessionaria_andamento::getList($queryAndamento));
        $this->set('ConcessionariaSituacao', Concessionaria_situacao::getList($queryConcessionariaSituacao));
        $this->set('Estq_movs', $p->getPage($Estq_movsCriteria));
        $this->set('nav', $p->getNav());
    }

    function ficha()
    {
        $this->setTitle('Ficha de Estoque');
    }

    function posicao()
    {
        $this->setTitle('Posição do estoque');

        $a = new Criteria();
        $a->addCondition('situacao', '=', 1);
        $a->addCondition('origem', '=', Config::get('origem'));
        $this->set('subestoque', Estq_subestoque::getList($a));
        $tipo = ($this->getParam('a') == 1) ? 'Sucata' : 'Novos';
        $this->set('tipo', $tipo);
        $this->set('t', $this->getParam('a'));
    }

    function relatorio_movimento_transformadores()
    {
        $this->setTemplate('reports');
        $idMovimento = $this->getParam('id');
        $movimentacao = new Estq_mov($idMovimento);
        $json['nota'] = array(
            "nr" => $movimentacao->coi,
            "alimentador" => strtoupper($movimentacao->alimentador),
            "manutencao" => strtoupper($movimentacao->manutencao),
            "supervicao" => strtoupper($movimentacao->getGrupoSupervisao()->nome),
            "endereco" => strtoupper($movimentacao->endereco),
            "municipio" => strtoupper($movimentacao->municipio),
            "sigla" => strtoupper($movimentacao->sigla),
            "referencia" => strtoupper($movimentacao->referencia),
            "codigo_instalacao" => strtoupper($movimentacao->codigo_instalacao),
            "urbano" => (strcmp($movimentacao->localizacao, 'URBANA') == 0) ? 'x' : '',
            "rural" => (strcmp($movimentacao->localizacao, 'RURAL') == 0) ? 'x' : '',
        );
        $json['transformador_retirado'] = array(
            'n_patrimonio' => $movimentacao->num_patrimonio_transf_retirado,
            'n_fabrica' => $movimentacao->num_fabricacao_transf_retirado,
            'marca' => $movimentacao->marca_transf_retirado,
            'ano' => $movimentacao->ano_transf_retirado,
            'potencia' => $movimentacao->potencia_transf_retirado,
            'n_fases' => $movimentacao->num_fases_transf_retirado,
            'tensao_prim' => $movimentacao->tensao_prima_transf_retirado,
            'tensao_secund' => $movimentacao->tensao_secun_transf_retirado,
        );

        $json['transformador_instalado'] = array(
            'n_patrimonio' => $movimentacao->num_patrimonio_transf_instalado,
            'n_fabrica' => $movimentacao->num_fabricacao_transf_instalado,
            'marca' => $movimentacao->marca_transf_instalado,
            'ano' => $movimentacao->ano_transf_instalado,
            'potencia' => $movimentacao->potencia_transf_instalado,
            'n_fases' => $movimentacao->num_fases_transf_instalado,
            'tensao_prim' => $movimentacao->tensao_prima_transf_instalado,
            'tensao_secund' => $movimentacao->tensao_secun_transf_instalado,
        );

        $json['motivo'] = array(
            'preventivo' => (strcmp($movimentacao->manutencao, 'Preventiva') == 0) ? 'x' : '',
            'corretivo' => (strcmp($movimentacao->manutencao, 'Corretiva') == 0) ? 'x' : '',
            'motivo' => $movimentacao->getMotivoRetirada()->nome
        );

        $json['pararaios'] = array(
            'reaplicados' => (strcmp($movimentacao->manutencao_pararaio, 'reaplicados') == 0) ? 'x' : '',
            'substituidos' => (strcmp($movimentacao->manutencao_pararaio, 'substituidos') == 0) ? 'x' : '',
            'transferidos' => (strcmp($movimentacao->manutencao_pararaio, 'transferids p/ carcaças') == 0) ? 'x' : '',
        );

        $json['equipe'] = array(
            'executante' => 'JRSM CONS. PREST. SERV. ELET. LTDA',
            'encarregado' => '',
            'eletricista1' => $movimentacao->eletrecista01,
            'eletricista2' => $movimentacao->eletrecista02,
            'data' => DataBR($movimentacao->data_medicao),
            'hora_inicial' => $movimentacao->hora_inicial,
            'hora_final' => $movimentacao->hora_final,
            'equipe' => strtoupper($movimentacao->tipo_carro),
            'camionete' => (strcmp($movimentacao->tipo_carro, 'Camionete') == 0) ? 'x' : '',
            'equipeb1' => (strcmp($movimentacao->tipo_carro, 'Equipe B1') == 0) ? 'x' : '',
            'equipeb2' => (strcmp($movimentacao->tipo_carro, 'Equipe B2') == 0) ? 'x' : '',
        );
        $json['aterramento'] = array(
            'medicao' => $movimentacao->medicao_aterramento,
        );

        $concessionariaItemCriteria = new Criteria();
        $concessionariaItemCriteria->addCondition('situacao_id', '=', 1);
        $concessionariaItemCriteria->addCondition('estq_mov_id', '=', $idMovimento);
        $concessionariaItemCriteria->addCondition('item_pai', 'IS', NULL);

        $concessionaria_item_servico = Concessionaria_item_servico::getList($concessionariaItemCriteria);


        foreach ($concessionaria_item_servico as $item) {

            $json['servico'][] = array(
                'codigo' => $item->getServico()->codigo_livre,
                'descricao' => $item->getServico()->nome,
                'quantidade' => number_format($item->quantidade, 2, ',', '.'),
                'centro' => $movimentacao->getGrupoSupervisao()->centro,
                'it' => '',
                'recebedor' => '',
                'UM' => ''
            );
        }


        $json['rodape'] = array(
            'distribuidora' => 'Energisa Mato Grosso do Sul - Destribuidora De Energia S.A. ',
            'endereco' => 'Avenida Gury Marques,8000. Campo grande - MS | 79072-900 Tel.:(67)3398-4000',
            'site' => 'www.energisa.com.br'
        );


        $reportCreate = new Json();

        $reportCreate->create('reportMovimentoTransformadores.json', json_encode($json));
    }

    function relatorio_mao_de_obra()
    {
        $this->setTemplate('reports');
        $idMovimento = $this->getParam('id');

        $movimentacao = new Estq_mov($idMovimento);
        $json['prestadora'] = array(
            "ordem_servico" => $movimentacao->tablet,
            "nr" => $movimentacao->coi,
            "prestador" => 'JRSM',
            "data_conclusao" => DataBR($movimentacao->data_medicao),
            "municipio" => $movimentacao->municipio,
            "codigo_equipe" => $movimentacao->codigo_equipe
        );

        $concessionariaItemCriteria = new Criteria();
        $concessionariaItemCriteria->addCondition('situacao_id', '=', 1);
        $concessionariaItemCriteria->addCondition('estq_mov_id', '=', $idMovimento);
        $concessionariaItemCriteria->addCondition('item_pai', 'IS', NULL);

        $concessionaria_item_servico = Concessionaria_item_servico::getList($concessionariaItemCriteria);


        foreach ($concessionaria_item_servico as $item) {

            $mao = array(
                'codigo' => $item->getServico()->codigo_livre,
                'descricao' => $item->getServico()->nome,
                'quantidade' => number_format($item->quantidade, 2, ',', '.'),
            );

            $mao['valor_m'] = $item->valor_atual_conversao;
            if ($item->us == 1) {
                $mao['valor_m'] *= $item->fator_us;

            }

            if ($item->usar_adicional) {
                $mao['valor_m'] = $item->valor_total;
            }

            $mao['valor_total'] = $item->valor_total;
            $json['servico'][] = $mao;
        }

        $json['rodape'] = array(
            'distribuidora' => 'Energisa Mato Grosso do Sul - Destribuidora De Energia S.A. ',
            'endereco' => 'Avenida Gury Marques,8000. Campo grande - MS | 79072-900 Tel.:(67)3398-4000',
            'site' => 'www.energisa.com.br'
        );


        $reportCreate = new Json();

        $reportCreate->create('reportMaoDeObra.json', json_encode($json));
    }

    function relatorio_materiais()
    {
        $this->setTemplate('reports');
        $idMovimento = $this->getParam('id');

        $movimentacao = new Estq_mov($idMovimento);
        $json['prestadora'] = array(
            "ordem_servico" => $movimentacao->id,
            "cliente" => $movimentacao->getCliente()->nome,
            "endereco_cliente"=>$movimentacao->getCliente()->endereco,
            "bairro_cliente"=>$movimentacao->getCliente()->bairro,
            "cidade_cliente"=>$movimentacao->getCliente()->cidade,
            "numero_cliente"=>$movimentacao->getCliente()->numero,
            "complemento_cliente"=>$movimentacao->getCliente()->complemento,
            "cep_cliente"=>$movimentacao->getCliente()->cep,
            "prestador" => 'Sonar',
            "data_conclusao" => DataBR($movimentacao->data_medicao),
            "municipio" => $movimentacao->municipio,
            "codigo_equipe" => $movimentacao->codigo_equipe,
            "valor_geral" => 0
        );


        $concessionariaItemCriteria = new Criteria();
        $concessionariaItemCriteria->addCondition('situacao_id', '=', 1);
        $concessionariaItemCriteria->addCondition('estq_mov_id', '=', $idMovimento);
        $concessionariaItemCriteria->addCondition('item_pai', 'IS', NULL);

        $concessionaria_item_servico = Concessionaria_item_servico::getList($concessionariaItemCriteria);


        foreach ($concessionaria_item_servico as $item) {

            $mao = array(
                'codigo' => $item->getServico()->codigo_livre,
                'descricao' => $item->getServico()->nome,
                'quantidade' => number_format($item->quantidade, 2, ',', '.'),
            );

            $mao['valor_m'] = $item->valor_atual_conversao;
            if ($item->us == 1) {
                $mao['valor_m'] *= $item->fator_us;

            }

            if ($item->usar_adicional) {
                $mao['valor_m'] = $item->valor_total;
            }

            $mao['valor_total'] = $item->valor_total;
            $json['servico'][] = $mao;
            $json['prestadora']['valor_geral'] +=$item->valor_total;
        }

        $criteriaDetalhes = new Criteria();
        $criteriaDetalhes->addCondition('situacao', '=', 1);
        $criteriaDetalhes->addCondition('movimento', '=', $idMovimento);

        $movimentoDetalhes = Estq_movdetalhes::getList($criteriaDetalhes);
        foreach ($movimentoDetalhes as $item) {

            $json['material'][] = array(
                'codigo' => $item->getEstq_artigo()->codigo_livre,
                'descricao' => $item->getEstq_artigo()->nome,
                'quantidade' => number_format($item->quantidade, 2, ',', '.'),
                'vlnf' => number_format($item->vlnf, 2, ',', '.'),
                'valor_total' => number_format($item->vlnf * $item->quantidade, 2, ',', '.')
            );

            $json['prestadora']['valor_geral'] += $item->vlnf * $item->quantidade;
        }

        $json['rodape'] = array(
            'distribuidora' => ' ',
            'endereco' => '',
            'site' => ''
        );


        $reportCreate = new Json();

        $reportCreate->create('reportMaterial.json', json_encode($json));
    }

    function relatorio_manutencao_preventiva_corretiva()
    {
        $this->setTemplate('reports');
        $idMovimento = $this->getParam('id');
        $movimentacao = new Estq_mov($idMovimento);
        $json['nota'] = array(
            "nr" => $movimentacao->coi,
            "os" => $movimentacao->tablet,
            "tipo_servico" => strtoupper($movimentacao->getTipo_servico()->nome),
            "alimentador" => $movimentacao->alimentador,
            "manutencao" => $movimentacao->manutencao,
            "supervicao" => strtoupper($movimentacao->getGrupoSupervisao()->nome),
            "endereco" => strtoupper($movimentacao->endereco),
            "municipio" => strtoupper($movimentacao->municipio),
            "referencia" => strtoupper($movimentacao->referencia),
            "codigo_instalacao" => $movimentacao->codigo_instalacao,
            "urbano" => (strcmp($movimentacao->localizacao, 'URBANA') == 0) ? 'x' : '',
            "rural" => (strcmp($movimentacao->localizacao, 'RURAL') == 0) ? 'x' : '',
            "componente" => strtoupper($movimentacao->getComponentes()->nome)
        );

        $json['motivo'] = array(
            'preventivo' => (strcmp($movimentacao->manutencao, 'Preventiva') == 0) ? 'x' : '',
            'corretivo' => (strcmp($movimentacao->manutencao, 'Corretiva') == 0) ? 'x' : '',
            'motivo' => $movimentacao->getMotivoRetirada()->nome
        );

        $json['pararaios'] = array(
            'reaplicados' => (strcmp($movimentacao->manutencao_pararaio, 'reaplicados') == 0) ? 'x' : '',
            'substituidos' => (strcmp($movimentacao->manutencao_pararaio, 'substituidos') == 0) ? 'x' : '',
            'transferidos' => (strcmp($movimentacao->manutencao_pararaio, 'transferids p/ carcaças') == 0) ? 'x' : '',
        );

        $json['equipe'] = array(
            'executante' => $movimentacao->codigo_equipe,
            'encarregado' => '',
            'eletricista1' => $movimentacao->eletrecista01,
            'eletricista2' => $movimentacao->eletrecista02,
            'data' => DataBR($movimentacao->data_medicao),
            'hora_inicial' => $movimentacao->hora_inicial,
            'hora_final' => $movimentacao->hora_final,
            'equipe' => strtoupper($movimentacao->tipo_carro),
            'camionete' => (strcmp($movimentacao->tipo_carro, 'Camionete') == 0) ? 'x' : '',
            'equipeb1' => (strcmp($movimentacao->tipo_carro, 'Equipe B1') == 0) ? 'x' : '',
            'equipeb2' => (strcmp($movimentacao->tipo_carro, 'Equipe B2') == 0) ? 'x' : '',
        );


        $c = new Criteria();
        $c->addCondition('estq_mov_id', '=', $idMovimento);
        $c->addCondition('situacao_id', '=', 1);
        $c->addCondition('tipo_imagem', '=', 2);
        $c->setOrder('ordem ASC');
        $concessionaria_imagens = Concessionaria_imagem::getList($c);

        foreach ($concessionaria_imagens as $img) {
            $json['imagem'][] = array(
                'ordem' => $img->ordem,
                "observacao" => html_entity_decode($img->observacao),
                "caminho_imagem" => SITE_PATH . '/' . $img->caminho_imagem
            );
        }

        $concessionariaItemCriteria = new Criteria();
        $concessionariaItemCriteria->addCondition('situacao_id', '=', 1);
        $concessionariaItemCriteria->addCondition('estq_mov_id', '=', $idMovimento);
        $concessionariaItemCriteria->addCondition('item_pai', 'IS', NULL);

        $concessionaria_item_servico = Concessionaria_item_servico::getList($concessionariaItemCriteria);


        foreach ($concessionaria_item_servico as $item) {

            $json['servico'][] = array(
                'codigo' => $item->getServico()->codigo_livre,
                'centro' => $movimentacao->getGrupoSupervisao()->centro,
                'descricao' => $item->getServico()->nome,
                'quantidade' => number_format($item->quantidade, 2, ',', '.')
            );
        }
        $criteriaDetalhes = new Criteria();
        $criteriaDetalhes->addCondition('situacao', '=', 1);
        $criteriaDetalhes->addCondition('movimento', '=', $idMovimento);

        $movimentoDetalhes = Estq_movdetalhes::getList($criteriaDetalhes);
        foreach ($movimentoDetalhes as $item) {

            $json['material'][] = array(
                'codigo' => $item->getEstq_artigo()->codigo_livre,
                'centro' => $movimentacao->getGrupoSupervisao()->centro,
                'descricao' => $item->getEstq_artigo()->nome,
                'quantidade' => number_format($item->quantidade, 2, ',', '.')
            );
        }


        $json['rodape'] = array(
            'distribuidora' => 'Energisa Mato Grosso do Sul - Destribuidora De Energia S.A. ',
            'endereco' => 'Avenida Gury Marques,8000. Campo grande - MS | 79072-900 Tel.:(67)3398-4000',
            'site' => 'www.energisa.com.br'
        );


        $reportCreate = new Json();
        $reportCreate->create('reportManutencaoPreventivaCorretiva.json', json_encode($json));
    }

    /*
    montamos aqui todos os relatorios gerias das movimentacoes
    */
    function relatorio_movimentacoes_geral()
    {

        $this->setTemplate('reports');
        $Estq_movsCriteria = new Criteria();

        if (!empty($_GET['situacao_mov'])) {
            $Estq_movsCriteria->addCondition('situacao_mov', '=', $_GET['situacao_mov']);
        }
        if (!empty($_GET['numdoc'])) {
            $Estq_movsCriteria->addCondition('numdoc', '=', $_GET['numdoc']);
        }
        if (!empty($_GET['coi'])) {
            $Estq_movsCriteria->addCondition('coi', '=', $_GET['coi']);
        }
        if (!empty($_GET['tablet'])) {
            $Estq_movsCriteria->addCondition('tablet', '=', $_GET['tablet']);
        }
        if (!empty($_GET['codigo_equipe_id'])) {
            $equipes = implode(',', $_GET['codigo_equipe_id']);

            $Estq_movsCriteria->addSqlConditions("codigo_equipe_id IN ($equipes)");
        }

        if (!empty($_GET['inicio']) && !empty($_GET['fim'])) {
            $inicio = DataSQL($_GET['inicio']);
            $fim = DataSQL($_GET['fim']);
            $Estq_movsCriteria->addCondition('data_medicao', '>=', $inicio);
            $Estq_movsCriteria->addCondition('data_medicao', '<=', $fim);
        }

        $movimentacao = Estq_mov::getList($Estq_movsCriteria);


        $json = array();
        foreach ($movimentacao as $mov) {

            $soma = $mov->valor_recebimento + $mov->valor_recebimento2 + $mov->valor_recebimento3 + $mov->valor_recebimento4;

            if ($mov->valor_os_contratada != $soma && $mov->valor_os_contratada > $soma) {
                $json['mov'][] = array(
                    'id' => $mov->id,
                    'codigo_equipe_id' => !empty($mov->codigo_equipe_id) ? $mov->codigo_equipe_id : '',
                    'codigo_equipe_nome' => $mov->getEquipe()->nome,
                    'no' => $mov->numdoc,
                    'coi' => $mov->coi,
                    'tablet' => $mov->tablet,
                    'data_conclusao_obra' => !empty($mov->data_medicao) ? DataBR($mov->data_medicao) : '',
                    'valor_os_contratada' => $mov->valor_os_contratada,
                    'data_recebimento' => !empty($mov->data_recebimento) ? DataBR($mov->data_recebimento) : '',
                    'valor_recebimento' => $mov->valor_recebimento,
                    'data_recebimento2' => !empty($mov->data_recebimento2) ? DataBR($mov->data_recebimento2) : '',
                    'valor_recebimento2' => $mov->valor_recebimento2,
                    'data_recebimento3' => !empty($mov->data_recebimento3) ? DataBR($mov->data_recebimento3) : '',
                    'valor_recebimento3' => $mov->valor_recebimento3,
                    'data_recebimento4' => !empty($mov->data_recebimento4) ? DataBR($mov->data_recebimento4) : '',
                    'valor_recebimento4' => $mov->valor_recebimento4,
                    'valor_Areceber' => $mov->valor_os_contratada - ($mov->valor_recebimento4 + $mov->valor_recebimento3 + $mov->valor_recebimento2 + $mov->valor_recebimento)
                );
            }
        }

        $json['aux'][] = [
            'inicio' => !empty($_GET['inicio']) ? $_GET['inicio'] : '-',
            'fim' => !empty($_GET['fim']) ? $_GET['fim'] : '-'
        ];
        $reportCreate = new Json();
        $reportCreate->create('movimentao_sem_recebimento.json', json_encode($json));
    }

    function relatorio_movimentacoes_geral_recebidos()
    {

        $this->setTemplate('reports');
        $Estq_movsCriteria = new Criteria();

        if (!empty($_GET['situacao_mov'])) {
            $Estq_movsCriteria->addCondition('situacao_mov', '=', $_GET['situacao_mov']);
        }
        if (!empty($_GET['numdoc'])) {
            $Estq_movsCriteria->addCondition('numdoc', '=', $_GET['numdoc']);
        }
        if (!empty($_GET['coi'])) {
            $Estq_movsCriteria->addCondition('coi', '=', $_GET['coi']);
        }
        if (!empty($_GET['tablet'])) {
            $Estq_movsCriteria->addCondition('tablet', '=', $_GET['tablet']);
        }
        if (!empty($_GET['codigo_equipe_id'])) {
            $equipes = implode(',', $_GET['codigo_equipe_id']);

            $Estq_movsCriteria->addSqlConditions("codigo_equipe_id IN ($equipes)");
        }

        if (!empty($_GET['inicio']) && !empty($_GET['fim'])) {
            $inicio = DataSQL($_GET['inicio']);
            $fim = DataSQL($_GET['fim']);
            $Estq_movsCriteria->addCondition('data_medicao', '>=', $inicio);
            $Estq_movsCriteria->addCondition('data_medicao', '<=', $fim);
        }

        $movimentacao = Estq_mov::getList($Estq_movsCriteria);


        $json = array();
        foreach ($movimentacao as $mov) {

            $soma = $mov->valor_recebimento + $mov->valor_recebimento2 + $mov->valor_recebimento3 + $mov->valor_recebimento4;

            if ($mov->valor_os_contratada <= $soma) {
                $json['mov'][] = array(
                    'id' => $mov->id,
                    'codigo_equipe_id' => !empty($mov->codigo_equipe_id) ? $mov->codigo_equipe_id : '',
                    'codigo_equipe_nome' => $mov->getEquipe()->nome,
                    'no' => $mov->numdoc,
                    'coi' => $mov->coi,
                    'tablet' => $mov->tablet,
                    'data_conclusao_obra' => !empty($mov->data_medicao) ? DataBR($mov->data_medicao) : '',
                    'valor_os_contratada' => $mov->valor_os_contratada,
                    'data_recebimento' => !empty($mov->data_recebimento) ? DataBR($mov->data_recebimento) : '',
                    'valor_recebimento' => $mov->valor_recebimento,
                    'data_recebimento2' => !empty($mov->data_recebimento2) ? DataBR($mov->data_recebimento2) : '',
                    'valor_recebimento2' => $mov->valor_recebimento2,
                    'data_recebimento3' => !empty($mov->data_recebimento3) ? DataBR($mov->data_recebimento3) : '',
                    'valor_recebimento3' => $mov->valor_recebimento3,
                    'data_recebimento4' => !empty($mov->data_recebimento4) ? DataBR($mov->data_recebimento4) : '',
                    'valor_recebimento4' => $mov->valor_recebimento4,
                    'valor_Areceber' => $mov->valor_os_contratada - ($mov->valor_recebimento4 + $mov->valor_recebimento3 + $mov->valor_recebimento2 + $mov->valor_recebimento)
                );
            }
        }

        $json['aux'][] = [
            'inicio' => !empty($_GET['inicio']) ? $_GET['inicio'] : '-',
            'fim' => !empty($_GET['fim']) ? $_GET['fim'] : '-'
        ];
        $reportCreate = new Json();
        $reportCreate->create('movimentacao_recebido_geral.json', json_encode($json));
    }

    function relatorio_faturamento_por_equipes()
    {

        $this->setTemplate('reports');
        $Estq_movsCriteria = new Criteria();

        if (!empty($_GET['situacao_mov'])) {
            $Estq_movsCriteria->addCondition('situacao_mov', '=', $_GET['situacao_mov']);
        }
        if (!empty($_GET['numdoc'])) {
            $Estq_movsCriteria->addCondition('numdoc', '=', $_GET['numdoc']);
        }
        if (!empty($_GET['coi'])) {
            $Estq_movsCriteria->addCondition('coi', '=', $_GET['coi']);
        }
        if (!empty($_GET['tablet'])) {
            $Estq_movsCriteria->addCondition('tablet', '=', $_GET['tablet']);
        }
        if (!empty($_GET['codigo_equipe_id'])) {
            $equipes = implode(',', $_GET['codigo_equipe_id']);

            $Estq_movsCriteria->addSqlConditions("codigo_equipe_id IN ($equipes)");
        }

        if (!empty($_GET['inicio']) && !empty($_GET['fim'])) {
            $inicio = DataSQL($_GET['inicio']);
            $fim = DataSQL($_GET['fim']);
            $Estq_movsCriteria->addCondition('data_medicao', '>=', $inicio);
            $Estq_movsCriteria->addCondition('data_medicao', '<=', $fim);
        }

        $Estq_movsCriteria->setOrder('data_medicao ASC');

        $movimentacao = Estq_mov::getList($Estq_movsCriteria);

        $somaFaturamento = array();
        // inicializa o vetor de soma
        foreach ($movimentacao as $mov) {
            if (!empty($mov->data_medicao)) {
                $date = new DateTime($mov->data_medicao);
                $somaFaturamento[$date->format('m/Y')][$mov->codigo_equipe]['valor_recebido'] = 0;
                $somaFaturamento[$date->format('m/Y')][$mov->codigo_equipe]['valor_contratado'] = 0;

            }
        }
        //calcular o valores contratados de todas movimentaçoes da equipe e os valores recebidos
        foreach ($movimentacao as $mov) {
            if (!empty($mov->data_medicao)) {
                $date = new DateTime($mov->data_medicao);
                $soma = $mov->valor_recebimento + $mov->valor_recebimento2 + $mov->valor_recebimento3 + $mov->valor_recebimento4;
                $somaFaturamento[$date->format('m/Y')][$mov->codigo_equipe]['equipe'] = $mov->codigo_equipe;
                $somaFaturamento[$date->format('m/Y')][$mov->codigo_equipe]['mes'] = $date->format('m/Y');
                $somaFaturamento[$date->format('m/Y')][$mov->codigo_equipe]['valor_recebido'] += $soma;
                $somaFaturamento[$date->format('m/Y')][$mov->codigo_equipe]['valor_contratado'] += $mov->valor_os_contratada;
            }
        }
        $json = array();
        foreach ($somaFaturamento as $mes) {
            foreach ($mes as $equipe) {
                $json['equipes'][] = $equipe;
            }
        }

        $json['aux'][] = [
            'inicio' => !empty($_GET['inicio']) ? $_GET['inicio'] : '-',
            'fim' => !empty($_GET['fim']) ? $_GET['fim'] : '-'
        ];

        $reportCreate = new Json();
        $reportCreate->create('relatorio_faturamento_por_equipes.json', json_encode($json));
    }

    function relatorio_medicoes_em_aberto()
    {

        $this->setTemplate('reports');
        $Estq_movsCriteria = new Criteria();

        if (!empty($_GET['situacao_mov'])) {
            $Estq_movsCriteria->addCondition('situacao_mov', '=', $_GET['situacao_mov']);
        }
        if (!empty($_GET['numdoc'])) {
            $Estq_movsCriteria->addCondition('numdoc', '=', $_GET['numdoc']);
        }
        if (!empty($_GET['coi'])) {
            $Estq_movsCriteria->addCondition('coi', '=', $_GET['coi']);
        }
        if (!empty($_GET['tablet'])) {
            $Estq_movsCriteria->addCondition('tablet', '=', $_GET['tablet']);
        }
        if (!empty($_GET['codigo_equipe_id'])) {
            $equipes = implode(',', $_GET['codigo_equipe_id']);

            $Estq_movsCriteria->addSqlConditions("codigo_equipe_id IN ($equipes)");
        }

        if (!empty($_GET['inicio']) && !empty($_GET['fim'])) {
            $inicio = DataSQL($_GET['inicio']);
            $fim = DataSQL($_GET['fim']);
            $Estq_movsCriteria->addCondition('data_medicao', '>=', $inicio);
            $Estq_movsCriteria->addCondition('data_medicao', '<=', $fim);
        }


        $movimentacao = Estq_mov::getList($Estq_movsCriteria);


        $json = array();
        foreach ($movimentacao as $mov) {

            $soma = $mov->valor_recebimento + $mov->valor_recebimento2 + $mov->valor_recebimento3 + $mov->valor_recebimento4;

            if ($mov->valor_os_contratada != $soma && $mov->valor_os_contratada > $soma) {
                $json['mov'][] = array(
                    'id' => $mov->id,
                    'codigo_equipe_id' => !empty($mov->codigo_equipe_id) ? $mov->codigo_equipe_id : '',
                    'codigo_equipe_nome' => $mov->getEquipe()->nome,
                    'no' => $mov->numdoc,
                    'coi' => $mov->coi,
                    'tablet' => $mov->tablet,
                    'data_conclusao_obra' => !empty($mov->data_medicao) ? DataBR($mov->data_medicao) : '',
                    'valor_os_contratada' => $mov->valor_os_contratada,
                    'data_recebimento' => !empty($mov->data_recebimento) ? DataBR($mov->data_recebimento) : '',
                    'valor_recebimento' => $mov->valor_recebimento,
                    'data_recebimento2' => !empty($mov->data_recebimento2) ? DataBR($mov->data_recebimento2) : '',
                    'valor_recebimento2' => $mov->valor_recebimento2,
                    'data_recebimento3' => !empty($mov->data_recebimento3) ? DataBR($mov->data_recebimento3) : '',
                    'valor_recebimento3' => $mov->valor_recebimento3,
                    'data_recebimento4' => !empty($mov->data_recebimento4) ? DataBR($mov->data_recebimento4) : '',
                    'valor_recebimento4' => $mov->valor_recebimento4,
                    'valor_Areceber' => $mov->valor_os_contratada - ($mov->valor_recebimento4 + $mov->valor_recebimento3 + $mov->valor_recebimento2 + $mov->valor_recebimento)
                );
            }
        }

        $json['aux'][] = [
            'inicio' => !empty($_GET['inicio']) ? $_GET['inicio'] : '-',
            'fim' => !empty($_GET['fim']) ? $_GET['fim'] : '-'
        ];

        $reportCreate = new Json();
        $reportCreate->create('relatorio_medicoes_em_aberto.json', json_encode($json));
    }


    function relatorio_medicoes_recebidas()
    {

        $this->setTemplate('reports');
        $Estq_movsCriteria = new Criteria();

        if (!empty($_GET['situacao_mov'])) {
            $Estq_movsCriteria->addCondition('situacao_mov', '=', $_GET['situacao_mov']);
        }
        if (!empty($_GET['numdoc'])) {
            $Estq_movsCriteria->addCondition('numdoc', '=', $_GET['numdoc']);
        }
        if (!empty($_GET['coi'])) {
            $Estq_movsCriteria->addCondition('coi', '=', $_GET['coi']);
        }
        if (!empty($_GET['tablet'])) {
            $Estq_movsCriteria->addCondition('tablet', '=', $_GET['tablet']);
        }
        if (!empty($_GET['codigo_equipe_id'])) {
            $equipes = implode(',', $_GET['codigo_equipe_id']);

            $Estq_movsCriteria->addSqlConditions("codigo_equipe_id IN ($equipes)");
        }

        if (!empty($_GET['inicio']) && !empty($_GET['fim'])) {
            $inicio = DataSQL($_GET['inicio']);
            $fim = DataSQL($_GET['fim']);
            $Estq_movsCriteria->addCondition('data_medicao', '>=', $inicio);
            $Estq_movsCriteria->addCondition('data_medicao', '<=', $fim);
        }

        $movimentacao = Estq_mov::getList($Estq_movsCriteria);


        $json = array();
        foreach ($movimentacao as $mov) {

            $soma = $mov->valor_recebimento + $mov->valor_recebimento2 + $mov->valor_recebimento3 + $mov->valor_recebimento4;

            if ($mov->valor_os_contratada <= $soma) {
                $json['mov'][] = array(
                    'id' => $mov->id,
                    'codigo_equipe_id' => !empty($mov->codigo_equipe_id) ? $mov->codigo_equipe_id : '',
                    'codigo_equipe_nome' => $mov->getEquipe()->nome,
                    'no' => $mov->numdoc,
                    'coi' => $mov->coi,
                    'tablet' => $mov->tablet,
                    'data_conclusao_obra' => !empty($mov->data_medicao) ? DataBR($mov->data_medicao) : '',
                    'valor_os_contratada' => $mov->valor_os_contratada,
                    'data_recebimento' => !empty($mov->data_recebimento) ? DataBR($mov->data_recebimento) : '',
                    'valor_recebimento' => $mov->valor_recebimento,
                    'data_recebimento2' => !empty($mov->data_recebimento2) ? DataBR($mov->data_recebimento2) : '',
                    'valor_recebimento2' => $mov->valor_recebimento2,
                    'data_recebimento3' => !empty($mov->data_recebimento3) ? DataBR($mov->data_recebimento3) : '',
                    'valor_recebimento3' => $mov->valor_recebimento3,
                    'data_recebimento4' => !empty($mov->data_recebimento4) ? DataBR($mov->data_recebimento4) : '',
                    'valor_recebimento4' => $mov->valor_recebimento4,
                    'valor_Areceber' => $mov->valor_os_contratada - ($mov->valor_recebimento4 + $mov->valor_recebimento3 + $mov->valor_recebimento2 + $mov->valor_recebimento)
                );
            }
        }

        $json['aux'][] = [
            'inicio' => !empty($_GET['inicio']) ? $_GET['inicio'] : '-',
            'fim' => !empty($_GET['fim']) ? $_GET['fim'] : '-'
        ];
        $reportCreate = new Json();
        $reportCreate->create('relatorio_medicoes_recebidas.json', json_encode($json));
    }

}