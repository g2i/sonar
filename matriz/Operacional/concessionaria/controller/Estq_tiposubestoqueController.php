<?php
final class Estq_tiposubestoqueController extends AppController{ 

    # página inicial do módulo Estq_tiposubestoque
    function index(){
        $this->setTitle('Visualização de Tipo Sub-Estoque');
    }

    # lista de Estq_tiposubestoques
    # renderiza a visão /view/Estq_tiposubestoque/all.php
    function all(){
        $this->setTitle('Listagem de Tipo Sub-Estoque');
        $p = new Paginate('Estq_tiposubestoque', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('origem','=',Config::get('origem')); //filtro Origem
        $this->set('Estq_tiposubestoques', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Estq_situacaos',  Estq_situacao::getList());
    

    }

    # visualiza um(a) Estq_tiposubestoque
    # renderiza a visão /view/Estq_tiposubestoque/view.php
    function view(){
        $this->setTitle('Visualização de Tipo Subestoque');
        try {
            $this->set('Estq_tiposubestoque', new Estq_tiposubestoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_tiposubestoque', 'all');
        }
    }

    # formulário de cadastro de Estq_tiposubestoque
    # renderiza a visão /view/Estq_tiposubestoque/add.php
    function add(){
        $this->setTitle('Cadastro de Tipo Subestoque');
        $this->set('Estq_tiposubestoque', new Estq_tiposubestoque);
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_tiposubestoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_tiposubestoque/add.php
    function post_add(){
        $this->setTitle('Cadastro de Tipo Sub-Estoque');
        $Estq_tiposubestoque = new Estq_tiposubestoque();
        $this->set('Estq_tiposubestoque', $Estq_tiposubestoque);
        $_POST['origem']=Config::get('origem');//salvando a Origem
        $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
        $Estq_tiposubestoque->usuario_id=$user; // para salvar o usuario que está fazendo
        try {
            $Estq_tiposubestoque->save($_POST);
            new Msg(__('Tipo Subestoque cadastrado com sucesso'));
            $this->go('Estq_tiposubestoque', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # formulário de edição de Estq_tiposubestoque
    # renderiza a visão /view/Estq_tiposubestoque/edit.php
    function edit(){
        $this->setTitle('Edição de Tipo Subestoque');
        try {
            $this->set('Estq_tiposubestoque', new Estq_tiposubestoque((int) $this->getParam('id')));
            $this->set('Estq_situacaos',  Estq_situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_tiposubestoque', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_tiposubestoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_tiposubestoque/edit.php
    function post_edit(){
        $this->setTitle('Edição de Tipo Subestoque');
        try {
            $Estq_tiposubestoque = new Estq_tiposubestoque((int) $_POST['id']);
            $this->set('Estq_tiposubestoque', $Estq_tiposubestoque);
            $Estq_tiposubestoque->usuario_dt=date('Y-m-d H:i:s');
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_tiposubestoque->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_tiposubestoque->save($_POST);
            new Msg(__('Tipo Subestoque atualizado com sucesso'));
            $this->go('Estq_tiposubestoque', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Estq_situacaos',  Estq_situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_tiposubestoque
    # renderiza a /view/Estq_tiposubestoque/delete.php
    function delete(){
        $this->setTitle('Apagar Tipo Subestoque');
        try {
            $this->set('Estq_tiposubestoque', new Estq_tiposubestoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_tiposubestoque', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_tiposubestoque
    # redireciona para Estq_tiposubestoque/all
    function post_delete(){
        try {
            $Estq_tiposubestoque = new Estq_tiposubestoque((int) $_POST['id']);
            $user=@Session::get("user")->id;// para salvar o usuario que está fazendo
            $Estq_tiposubestoque->usuario_id=$user; // para salvar o usuario que está fazendo
            $Estq_tiposubestoque->usuario_dt=date('Y-m-d H:i:s');
            $Estq_tiposubestoque->situacao=3;
            $Estq_tiposubestoque->save();
            new Msg(__('Tipo Subestoque deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_tiposubestoque', 'all');
    }

}