<?php

final class CepController extends AppController
{
    public function busca()
    {
        $cep = $_GET['cep'];
        //$reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
        $url = "http://viacep.com.br/ws/$cep/json/";
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
            $url = "https://viacep.com.br/ws/$cep/json/";

        $reg = json_decode(@file_get_contents($url));
        if (!isset($reg->erro)) {
            $dados['endereco'] = (string) $reg->logradouro;
            $dados['logradouro'] = (string) $reg->logradouro;
            $dados['bairro'] = (string) $reg->bairro;
            $dados['cidade'] = (string) $reg->localidade;
            $dados['uf'] = (string) $reg->uf;
        } else {
            throw new InternalErrorException(__('Ocorreu um problema ao tentar buscar o CEP, por favor tente mais tarde!'));
        }
        echo json_encode($dados);
        exit;
    }
}