<?php
final class Concessionaria_servicoController extends AppController{ 

    # página inicial do módulo Concessionaria_servico
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Concessionaria_servicos
    # renderiza a visão /view/Concessionaria_servico/all.php
    function all(){
        $this->setTitle('Listagem');
        $p = new Paginate('Concessionaria_servico', 50);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
       /* if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }*/
        $c->setOrder('id DESC');
        $c->addCondition("situacao_id","=",1);
        $this->set('Concessionaria_servicos', $p->getPage($c));
        $this->set('nav', $p->getNav());

    }

    # visualiza um(a) Concessionaria_servico
    # renderiza a visão /view/Concessionaria_servico/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Concessionaria_servico', new Concessionaria_servico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_servico', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_servico
    # renderiza a visão /view/Concessionaria_servico/add.php
    function add(){
        $this->setTitle('Cadastro');

        $this->set('Concessionaria_fator', Concessionaria_fator::getList());
        $this->set('Concessionaria_servico', new Concessionaria_servico);
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_servico
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_servico/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Concessionaria_servico = new Concessionaria_servico();
        $this->set('Concessionaria_servico', $Concessionaria_servico);

        try {
            if(!empty($_POST['fator_us']) && $_POST['us'] == 1){
                $_POST['fator_us'] = str_replace(".", "", $_POST['fator_us']);
                $_POST['fator_us'] = str_replace(',','.',$_POST['fator_us']);
            }else{
                $_POST['fator_us'] = 1;
            }

            $Concessionaria_servico->situacao_id = 1;
            $Concessionaria_servico->save($_POST);
            new Msg(__('Concessionaria_servico cadastrado com sucesso'));
            $this->go('Concessionaria_servico', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Concessionaria_servico
    # renderiza a visão /view/Concessionaria_servico/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Concessionaria_fator', Concessionaria_fator::getList());
            $this->set('Concessionaria_usa_tipo_cobrancas',  Concessionaria_usa_tipo_cobranca::getList());
            $this->set('Concessionaria_servico', new Concessionaria_servico((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_servico', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_servico
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_servico/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Concessionaria_servico = new Concessionaria_servico((int) $_POST['id']);
            if(!empty($_POST['fator_us']) && $_POST['us'] == 1){
                $_POST['fator_us'] = str_replace(".", "", $_POST['fator_us']);
                $_POST['fator_us'] = str_replace(',','.',$_POST['fator_us']);
            }else {
                $_POST['fator_us'] = 1;
            }

            $Concessionaria_servico->save($_POST);
            $this->set('Concessionaria_servico', $Concessionaria_servico);
            new Msg(__('Concessionaria_servico atualizado com sucesso'));
            $this->go('Concessionaria_servico', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_servico
    # renderiza a /view/Concessionaria_servico/delete.php
    function delete(){
        $this->setTitle('Apagar Concessionaria_servico');
        try {
            $this->set('Concessionaria_servico', new Concessionaria_servico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_servico', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_servico
    # redireciona para Concessionaria_servico/all
    function post_delete(){
        try {
            $Concessionaria_servico = new Concessionaria_servico((int) $_POST['id']);
            $Concessionaria_servico->situacao_id = 3;
            $Concessionaria_servico->save();
            new Msg(__('Concessionaria_servico apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_servico', 'all');
    }

    function search_servico(){

        $Concessionaria_servico = new Concessionaria_servico((int) $_GET['id']);
        $Concessionaria_fator = new Concessionaria_fator($Concessionaria_servico->us);
        $json['fator_us'] = (float)$Concessionaria_servico->fator_us;
        $json['us'] = (int)$Concessionaria_servico->us;
        $json['valor_us'] = $Concessionaria_fator->valor;
        echo json_encode($json);
        exit;
    }



}