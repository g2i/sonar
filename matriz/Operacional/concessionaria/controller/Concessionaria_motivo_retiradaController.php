<?php
final class Concessionaria_motivo_retiradaController extends AppController{ 

    # página inicial do módulo Concessionaria_motivo_retirada
    function index(){
        $this->setTitle('Visualização de Concessionaria motivo retirada');
    }

    # lista de Concessionaria_motivo_retiradas
    # renderiza a visão /view/Concessionaria_motivo_retirada/all.php
    function all(){
        $this->setTitle('Listagem de Concessionaria motivo retirada');
        $p = new Paginate('Concessionaria_motivo_retirada', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Concessionaria_motivo_retiradas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Concessionaria_motivo_retirada
    # renderiza a visão /view/Concessionaria_motivo_retirada/view.php
    function view(){
        $this->setTitle('Visualização de Concessionaria motivo retirada');
        try {
            $this->set('Concessionaria_motivo_retirada', new Concessionaria_motivo_retirada((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_motivo_retirada', 'all');
        }
    }

    # formulário de cadastro de Concessionaria_motivo_retirada
    # renderiza a visão /view/Concessionaria_motivo_retirada/add.php
    function add(){
        $this->setTitle('Cadastro de Concessionaria motivo retirada');
        $this->set('Concessionaria_motivo_retirada', new Concessionaria_motivo_retirada);
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Concessionaria_motivo_retirada
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_motivo_retirada/add.php
    function post_add(){
        $this->setTitle('Cadastro de Concessionaria motivo retirada');
        $Concessionaria_motivo_retirada = new Concessionaria_motivo_retirada();
        $this->set('Concessionaria_motivo_retirada', $Concessionaria_motivo_retirada);
        try {
            $Concessionaria_motivo_retirada->save($_POST);
            new Msg(__('Concessionaria_motivo_retirada cadastrado com sucesso'));
            $this->go('Concessionaria_motivo_retirada', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Concessionaria_motivo_retirada
    # renderiza a visão /view/Concessionaria_motivo_retirada/edit.php
    function edit(){
        $this->setTitle('Edição de Concessionaria motivo retirada');
        try {
            $this->set('Concessionaria_motivo_retirada', new Concessionaria_motivo_retirada((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Concessionaria_motivo_retirada', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Concessionaria_motivo_retirada
    # (true)redireciona ou (false) renderiza a visão /view/Concessionaria_motivo_retirada/edit.php
    function post_edit(){
        $this->setTitle('Edição de Concessionaria motivo retirada');
        try {
            $Concessionaria_motivo_retirada = new Concessionaria_motivo_retirada((int) $_POST['id']);
            $this->set('Concessionaria_motivo_retirada', $Concessionaria_motivo_retirada);
            $Concessionaria_motivo_retirada->save($_POST);
            new Msg(__('Concessionaria_motivo_retirada atualizado com sucesso'));
            $this->go('Concessionaria_motivo_retirada', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Concessionaria_motivo_retirada
    # renderiza a /view/Concessionaria_motivo_retirada/delete.php
    function delete(){
        $this->setTitle('Apagar Concessionaria_motivo_retirada');
        try {
            $this->set('Concessionaria_motivo_retirada', new Concessionaria_motivo_retirada((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Concessionaria_motivo_retirada', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Concessionaria_motivo_retirada
    # redireciona para Concessionaria_motivo_retirada/all
    function post_delete(){
        try {
            $Concessionaria_motivo_retirada = new Concessionaria_motivo_retirada((int) $_POST['id']);
            $Concessionaria_motivo_retirada->delete();
            new Msg(__('Concessionaria_motivo_retirada apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Concessionaria_motivo_retirada', 'all');
    }

}