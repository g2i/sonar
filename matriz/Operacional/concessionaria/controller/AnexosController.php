<?php
final class AnexosController extends AppController{ 

    # página inicial do módulo Anexos
    function index(){
        $this->setTitle('Visualização de Anexos');
    }

    # lista de Anexos
    # renderiza a visão /view/Anexos/all.phprr
    function all(){
        $this->setTitle('Listagem de Anexos');
        $p = new Paginate('Anexos', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        if($this->getParam('tipo_id')){
            $this->setParam('tipo_id',$this->getParam('tipo_id'));
            $c->addCondition('tipo_id','=',$this->getParam('tipo_id'));
        }
        if($this->getParam('descricao')){
            $this->setParam('descricao',$this->getParam('descricao'));
            $c->addCondition('descricao','like','%'.$this->getParam('descricao').'%');
        }

        if($this->getParam('id')){
             $c->addCondition('estq_mov_id','=',  $this->getParam('id'));
        }
       

        $c->addCondition('situacao_id','=',1);
        $this->set('Anexos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $a = new Criteria();
        $a->addCondition('situacao_id','=',1);
        $this->set('Tipo_anexos',  TipoAnexos::getList($a));
    }

    # visualiza um(a) Anexos
    # renderiza a visão /view/Anexos/view.php
    function view(){
        $this->setTitle('Visualização de Anexos');
        try {
            $this->set('Anexos', new Anexos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexos', 'all');
        }
    }

    # formulário de cadastro de Anexos
    # renderiza a visão /view/Anexos/add.php
    function add(){
        $this->setTitle('Cadastro de Anexos');
        $this->set('Anexos', new Anexos);
        $a = new Criteria();
        $a->addCondition('situacao_id','=',1);
        $this->set('Tipo_anexos',  TipoAnexos::getList($a));
    }

    # recebe os dados enviados via post do cadastro de Anexos
    # (true)redireciona ou (false) renderiza a visão /view/Anexos/add.php
    function post_add(){
        $this->setTitle('Cadastro de Anexos');
        $Anexos = new Anexos();
        $this->set('Anexos', $Anexos);
        $idMovimentacao = $_POST['estq_mov_id'];
        try {
            $numFile= count(array_filter($_FILES['caminho']['name']));
            $file = $_FILES['caminho'];
            for($i = 0; $i < $numFile; $i++){
                $dados = array(
                    "name"      => $file['name'][$i],
                    "type"      => $file['type'][$i],
                    "tmp_name"  => $file['tmp_name'][$i],
                    "error"     => $file['error'][$i],
                    "size"      => $file['size'][$i]
                );
                $tipoAnexos = new TipoAnexos($_POST['tipo_id']);
                $extensions = array('jpg', 'pdf', 'gif', 'mp3', 'mp4', 'odf', 'docx', 'doc', 'txt', 'ppd', 'ppx', 'xlsx', 'pptx', 'xls', 'png', 'zip', 'rar');

                $file_uploader = new FileUploader($dados,null,$extensions);
                $name = time().tratar_arquivos($dados['name']);
                $path = "anexos";
                if(empty($file_uploader->erro)) {
                    if ($file_uploader->save($name, $path)) {
                        $_POST['caminho'] = $file_uploader->path . '/' . $file_uploader->name;
                        $_POST['created'] = date('Y-m-d H:i:s');
                        $_POST['user_created'] = Session::get('user')->id;
                        $_POST['modified'] = date('Y-m-d H:i:s');
                        $_POST['user_modified'] = Session::get('user')->id;
                        $_POST['situacao_id'] = 1;
                        $Anexos->save($_POST);
                        $res = ['res' => 1, 'redirect' => "/Anexos/all/id:$idMovimentacao/"];
                    } else {
                        $res = ['res' => 2, 'msg' => $file_uploader->erro];
                    }
                }else{
                        $res = ['res' => 2, 'msg' => $file_uploader->erro];
                }

                echo json_encode($res);
                exit;

            }
            new Msg(__('Anexos cadastrado com sucesso'));
            $this->go('Anexos', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $a = new Criteria();
        $a->addCondition('situacao_id','=',1);
        $this->set('Tipo_anexos',  TipoAnexos::getList($a));

    }

    # formulário de edição de Anexos
    # renderiza a visão /view/Anexos/edit.php
    function edit(){
        $this->setTitle('Edição de Anexos');
        try {
            $this->set('Anexos', new Anexos((int) $this->getParam('id')));
            $a = new Criteria();
            $a->addCondition('situacao_id','=',1);
            $this->set('Tipo_anexos',  TipoAnexos::getList($a));

        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Anexos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Anexos
    # (true)redireciona ou (false) renderiza a visão /view/Anexos/edit.php
    function post_edit(){
        $this->setTitle('Edição de Anexos');
        try {
            $Anexos = new Anexos((int) $_POST['id']);
            $this->set('Anexos', $Anexos);
            if(!empty($_FILES['caminho']['name'])) {
                $numFile = count(array_filter($_FILES['caminho']['name']));
                $file = $_FILES['caminho'];

                for ($i = 0; $i < $numFile; $i++) {
                    $dados = array(
                        "name" => $file['name'][$i],
                        "type" => $file['type'][$i],
                        "tmp_name" => $file['tmp_name'][$i],
                        "error" => $file['error'][$i],
                        "size" => $file['size'][$i]
                    );
                    $tipoAnexos = new TipoAnexos($_POST['tipo_id']);
                    $extensions = explode(',', $tipoAnexos->extensao);

                    $file_uploader = new FileUploader($dados, null, $extensions);
                    $name = time() . tratar_arquivos($dados['name']);
                    $path = "anexos";
                    if (empty($file_uploader->erro)) {
                        if ($file_uploader->save($name, $path)) {
                            $_POST['caminho'] = $file_uploader->path . '/' . $file_uploader->name;
                            $_POST['modified'] = date('Y-m-d H:i:s');
                            $_POST['user_modified'] = Session::get('user')->id;
                            $Anexos->save($_POST);
                            $res = ['res' => 1];
                        } else {
                            $res = ['res' => 2, 'msg' => $file_uploader->erro];
                        }
                    } else {
                        $res = ['res' => 2, 'msg' => $file_uploader->erro];
                    }

                    echo json_encode($res);
                    exit;

                }
            }else{
                $_POST['modified'] = date('Y-m-d H:i:s');
                $_POST['user_modified'] = Session::get('user')->id;
                $_POST['caminho'] = $Anexos->caminho;
                $Anexos->save($_POST);
                $res = ['res' => 1];
                echo json_encode($res);
                exit;
            }

            new Msg(__('Anexos atualizado com sucesso'));
            $this->go('Anexos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $a = new Criteria();
        $a->addCondition('situacao_id','=',1);
        $this->set('Tipo_anexos',  TipoAnexos::getList($a));

    }

    # Confirma a exclusão ou não de um(a) Anexos
    # renderiza a /view/Anexos/delete.php
    function delete(){
        $this->setTitle('Apagar Anexos');
        try {
            $this->set('Anexos', new Anexos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Anexos', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Anexos
    # redireciona para Anexos/all
    function post_delete(){
        try {
            $Anexos = new Anexos((int) $_POST['id']);
            $Anexos->modified = date('Y-m-d H:i:s');
            $Anexos->user_modified = Session::get('user')->id;
            $Anexos->situacao_id=3;
            $Anexos->save();
            new Msg(__('Anexos apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Anexos', 'all');
    }

    function download()
    {
        $Anexos = new Anexos((int) $_GET['id']);
        set_time_limit(0);
        $file = $_SERVER['DOCUMENT_ROOT'].trim(SITE_PATH.'/'.$Anexos->caminho);

        if (!file_exists($file)) {
            exit;
        }
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.basename($file).'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        readfile($file);
        exit;

    }
}