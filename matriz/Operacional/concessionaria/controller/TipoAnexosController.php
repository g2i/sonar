<?php
final class TipoAnexosController extends AppController{

    # página inicial do módulo Anexos
    function index(){
        $this->setTitle('Visualização de Tipo de Anexos');
    }

    # lista de Anexos
    # renderiza a visão /view/Anexos/all.php
    function all(){
        $this->setTitle('Listagem de Tipo de Anexos');
        $p = new Paginate('TipoAnexos', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('TipoAnexos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Anexos
    # renderiza a visão /view/Anexos/view.php
    function view(){
        $this->setTitle('Visualização de Tipo de Anexos');
        try {
            $this->set('TipoAnexos', new Anexos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('TipoAnexos', 'all');
        }
    }

    # formulário de cadastro de Anexos
    # renderiza a visão /view/Anexos/add.php
    function add(){
        $this->setTitle('Cadastro de Anexos');
        $this->set('TipoAnexos', new TipoAnexos);
    }

    # recebe os dados enviados via post do cadastro de Anexos
    # (true)redireciona ou (false) renderiza a visão /view/Anexos/add.php
    function post_add(){
        $this->setTitle('Cadastro de Tipo de Anexos');
        $TipoAnexos = new TipoAnexos();
        $this->set('TipoAnexos', $TipoAnexos);
        try {
            $_POST['created'] = date('Y-m-d H:i:s');
            $_POST['modified'] = date('Y-m-d H:i:s');
            $_POST['user_id'] = Session::get('user')->id;
            $_POST['situacao_id'] = 1;
            $TipoAnexos->save($_POST);
            new Msg(__('Tipo de Anexo cadastrado com sucesso'));
            $this->go('TipoAnexos', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Anexos
    # renderiza a visão /view/Anexos/edit.php
    function edit(){
        $this->setTitle('Edição de Tipo de Anexos');
        try {
            $this->set('TipoAnexos', new TipoAnexos((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('TipoAnexos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de TipoAnexos
    # (true)redireciona ou (false) renderiza a visão /view/TipoAnexos/edit.php
    function post_edit(){
        $this->setTitle('Edição de Tipo de Anexos');
        try {
            $TipoAnexos = new TipoAnexos((int) $_POST['id']);
            $this->set('TipoAnexos', $TipoAnexos);
            $_POST['modified'] = date('Y-m-d H:i:s');
            $_POST['user_id'] = Session::get('user')->id;
            $TipoAnexos->save($_POST);
            new Msg(__('Tipo de Anexo atualizado com sucesso'));
            $this->go('TipoAnexos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Anexos
    # renderiza a /view/Anexos/delete.php
    function delete(){
        $this->setTitle('Apagar Tipo de Anexos');
        try {
            $this->set('TipoAnexos', new TipoAnexos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('TipoAnexos', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Anexos
    # redireciona para Anexos/all
    function post_delete(){
        try {
            $TipoAnexos = new TipoAnexos((int) $_POST['id']);
            $TipoAnexos->situacao_id = 3;
            $TipoAnexos->modified = date('Y-m-d H:i:s');
            $TipoAnexos->user_id = Session::get('user')->id;
            $TipoAnexos->save();
            new Msg(__('Tipo de Anexo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('TipoAnexos', 'all');
    }

}