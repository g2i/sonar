<?php

class Paginate {

    private $count = 1;
    private $perpage = 1;
    private $curpage = 1;
    private $model = NULL;
    private $criteria;
    private $class = 'pagination';

    /**
     * Configura a paginação.
     *
     * @param String $model nome do Modelo
     * @param int $perpage resultados por página
     */
    public function __construct($model, $perpage) {
        if (isset($_GET['page'])) {
            $this->curpage = (int) $_GET['page'];
        }
        $this->perpage = $perpage;
        $this->model = $model;
    }

    /**
     * Busca um array de objetos da página atual
     *
     * @param Criteria $criteria
     * @return array de objetos do modelo configurado
     */
    public function getPage(Criteria $criteria = NULL) {
        $this->criteria = $criteria;
        $model = $this->model;

        $this->count = $model::count($criteria);
        if (is_null($this->criteria))
            $this->criteria = new Criteria();
        $data = ($this->curpage - 1) * $this->perpage;
        $this->criteria->setLimit($data . ',' . $this->perpage);
        $m = $this->model;
        return $m::getList($this->criteria);
    }

    /**
     * Retorna o menu de navegação do sistema de paginação
     * para ser utilizado na View;
     *
     * @return null|string
     */
    public function getNav() {

        $pages = ceil($this->count / $this->perpage);
        if ($pages <= 1)
            return null;
        $pageURL = 'http';
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        if (stripos($pageURL, '?') === false)
            $pageURL .= '?';
        $removeParam = strstr($pageURL, '&page=');
        $pageURL = str_replace($removeParam, '', $pageURL);
        $r = '';
        $r .= "<ul class='$this->class'>";
        if($this->curpage ==1) {
            $r .= '<li class=""><a>&laquo;</a></li>';
        }else {
            if(!empty($_GET['modal']))
                $r .= '<li><a href="javascript:void(0)" onclick="Navegar(\''.$pageURL.'&page=1\',\'go\')">&laquo;</a></li>';
            else if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST')
                $r .= '<li><a href="javascript:void(0)" onclick="PaginarPost(1)" >&laquo;</a></li>';
            else
                $r .= '<li><a href="' . $pageURL . '&page=1">&laquo;</a></li>';
        }
        if ($this->curpage > 2 && $this->curpage <= ($pages - 2)) {
            $aux = $this->curpage - 2;
            $aux2 = $this->curpage + 3;
            for ($i = $aux; $i < $aux2; $i++){
                if(!empty($_GET['modal']))
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="Javascript:void(0)" onclick="Navegar(\''.$pageURL.'&page=' . $i . '\',\'go\')">';
                else if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST')
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="javascript:void(0)" onclick="PaginarPost('.$i.')"  >';
                else
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="' . $pageURL . '&page=' . $i . '">';
                $r .= $i;
                $r .= '</a></li>';
            }
        }
        else if ($this->curpage <= 2) {
            $aux = ceil($this->count / $this->perpage);
            if ($aux >= 5)
                $aux2 = 6;
            else {
                $aux2 = $aux + 1;
            }
            for ($i = 1; $i < $aux2; $i++) {
                if(!empty($_GET['modal']))
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="Javascript:void(0)" onclick="Navegar(\''.$pageURL.'&page=' . $i . '\',\'go\')">';
                else if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST')
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="javascript:void(0)" onclick="PaginarPost('.$i.')">';
                else
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="' . $pageURL . '&page=' . $i . '">';
                $r .= $i;
                $r .= '</a></li>';
            }
        }else if ($this->curpage >= ($pages - 2)) {
            $n = ceil($this->count / $this->perpage);
            if ($n > 5)
                $aux = $pages - 4;
            else
                $aux = $pages - $n + 1;
            for ($i = $aux; $i <= $pages; $i++) {
                if(!empty($_GET['modal']))
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="Javascript:void(0)" onclick="Navegar(\''.$pageURL.'&page=' . $i . '\',\'go\')">';
                else if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST')
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="javascript:void(0)" onclick="PaginarPost('.$i.')">';
                else
                    $r .= '<li class="' . (($this->curpage == $i) ? "active" : "") . '"><a href="' . $pageURL . '&page=' . $i . '">';

                $r .= $i;
                $r .= '</a></li>';
            }
        }if ($this->curpage >= $pages)
            $r .= '<li><a href="javascript:void(0)">&raquo;</a></li>';
        else {
            if(!empty($_GET['modal']))
                $r .= '<li><a href="javascript:void(0)" onclick="Navegar(\''.$pageURL.'&page=' . $pages . '\',\'go\')">&raquo;</a></li>';
            else if(strtoupper($_SERVER['REQUEST_METHOD']) === 'POST')
                $r .= '<li><a href="javascript:void(0)" onclick="PaginarPost('.$pages.')">&raquo;</a></li>';
            else
                $r .= '<li><a href="' . $pageURL . '&page=' . $pages . '">&raquo;</a></li>';
        }
        $r .= "</ul>";
        return $r;
    }

    public function setCssClass($class) {
        $this->class = $class;
    }

}

?>
