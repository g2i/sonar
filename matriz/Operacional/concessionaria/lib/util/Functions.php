<?php



function DataSQL($data) {

    if ($data != "") {

        list($d, $m, $y) = preg_split('/\//', $data);

        return sprintf('%04d-%02d-%02d', $y, $m, $d);

    }

}



function DataBR($data) {

    if ($data != "") {

        list($y, $m, $d) = preg_split('/-/', $data);

        return sprintf('%02d/%02d/%04d', $d, $m, $y);

    }

}

function DataTimeBr($data)

{

    if ($data != "") {

        return  date("d-m-Y H:i", strtotime($data));//exibe no formato d/m/a

    }

}

function ConvertDateTime($data){

    $dat = explode('/',$data);

    $dia = $dat[0];

    $mes = $dat[1];

    $ano_hora = explode(' ',$dat[2]);

    $ano = $ano_hora[0];

    $hora = $ano_hora[1];

    return $ano.'-'.$mes.'-'.$dia.' '.$hora;

}
function ConvertDateTimeBR($data){

    $dat = explode('-',$data);

    $ano = $dat[0];

    $mes = $dat[1];

    $dia_hora = explode(' ',$dat[2]);

    $dia = $dia_hora[0];

    $hora = $dia_hora[1];

    return $dia.'/'.$mes.'/'.$ano.' '.$hora;

}
function ConvertDTime($data){

    $dat = explode('-',$data);

    $ano = $dat[0];

    $mes = $dat[1];

    $dia_hora = explode(' ',$dat[2]);

    $dia = $dia_hora[0];

    $hora = $dia_hora[1];

    return $dia.'/'.$mes.'/'.$ano;

}

function ConvertData($data) {

    if ($data != "") {

        list($y, $m, $d) = preg_split('/-/', $data);

        return sprintf('%02d/%02d/%04d', $d, $m, $y);

    }

}

function somar_dias_uteis($str_data,$int_qtd_dias_somar = 7) {
    // Caso seja informado uma data do MySQL do tipo DATETIME - aaaa-mm-dd 00:00:00
    // Transforma para DATE - aaaa-mm-dd

    $str_data = substr($str_data,0,10);
    // Se a data estiver no formato brasileiro: dd/mm/aaaa
    // Converte-a para o padrão americano: aaaa-mm-dd
    $ano = '';
    $mes = '';
    $dia = '';
    if ( preg_match("@/@",$str_data) == 1 ) {
        $array_data = explode('/', $str_data);
        $dia = $array_data[0];
        $mes = $array_data[1];
        $ano = $array_data[2];

    }else{
        $array_data = explode('-', $str_data);
        $dia = $array_data[2];
        $mes = $array_data[1];
        $ano = $array_data[0];
    }

    $count_days = 0;
    $int_qtd_dias_uteis = 0;
    while ( $int_qtd_dias_uteis < $int_qtd_dias_somar )
    {
        $count_days++;
        if ( ( $dias_da_semana = gmdate('w', strtotime('+'.$count_days.' day', mktime(0, 0, 0, $mes, $dia, $ano))) ) != '0' && $dias_da_semana != '6' )
        {
           $int_qtd_dias_uteis++;
        }
    }
    return gmdate('Y/m/d',strtotime('+'.$count_days.' day',strtotime($str_data)));
}


function LimiteTexto($texto, $limite, $quebra = true){

    $tamanho = strlen($texto);

    if($tamanho <= $limite){ //Verifica se o tamanho do texto é menor ou igual ao limite

        $novo_texto = $texto;

    }else{ // Se o tamanho do texto for maior que o limite

        if($quebra == true){ // Verifica a opção de quebrar o texto

            $novo_texto = trim(substr($texto, 0, $limite))."...";

        }else{ // Se não, corta $texto na última palavra antes do limite

            $ultimo_espaco = strrpos(substr($texto, 0, $limite), " "); // Localiza o útlimo espaço antes de $limite

            $novo_texto = trim(substr($texto, 0, $ultimo_espaco))."..."; // Corta o $texto até a posição localizada

        }

    }

    return $novo_texto; // Retorna o valor formatado

}



function utf8_converter($array)

{

    array_walk_recursive($array, function(&$item, $key){

        if(!mb_detect_encoding($item, 'utf-8', true)){

            $item = utf8_encode($item);

        }

    });

    return $array;
}

function regiao(){
    $zona = array(
        'URBANA' => 'ÁREA URBANA',
        'RURAL' => 'ÁREA RURAL'
        );

    return $zona;
}


function tratar_arquivos($string){
    // pegando a extensao do arquivo
    $partes 	= explode(".", $string);
    $extensao 	= $partes[count($partes)-1];
    // somente o nome do arquivo
    $nome	= preg_replace('/\.[^.]*$/', '', $string);
    // removendo simbolos, acentos etc
    $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýýþÿŔŕ?';
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuuyybyRr-';
    $nome = strtr($nome, utf8_decode($a), $b);
    $nome = str_replace(".","-",$nome);
    $nome = preg_replace( "/[^0-9a-zA-Z\.]+/",'-',$nome);
    return utf8_decode(strtolower($nome.".".$extensao));
}

//pega o campo de pesquisa Tipo data da view estq_mov e retorna uma legenda 
function legendaTipoData($tipoData = null){
   // if($tipoData == '')

}
