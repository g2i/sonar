/**

 * Created by Desenvolvimento on 29/06/2015.

 */
$(function(){
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    }
});



//validação de cpf
function dublicidade(tipo,cod,id){
    var codigo = document.getElementById("codigo_livre");
    $.ajax({
        type:'POST',
        url:root+'/Estq_artigo/duplicidade',
        data:{
            tipo:tipo,
            codigo:cod,
            id:id
        },
        dataType:'JSON',
        success:function(txt){
            if(txt.result>0) {
                codigo.setCustomValidity("Código já cadastrado!");
            }else{
                codigo.setCustomValidity('');
            }
        }
    })
}
function validarCPF(cpf) {

    cpf = cpf.replace(/[^\d]+/g,'');

    if(cpf == '') return false;

    // Elimina CPFs invalidos conhecidos

    if (cpf.length != 11 ||

        cpf == "00000000000" ||

        cpf == "11111111111" ||

        cpf == "22222222222" ||

        cpf == "33333333333" ||

        cpf == "44444444444" ||

        cpf == "55555555555" ||

        cpf == "66666666666" ||

        cpf == "77777777777" ||

        cpf == "88888888888" ||

        cpf == "99999999999")

        return false;

    // Valida 1o digito

    add = 0;

    for (i=0; i < 9; i ++)

        add += parseInt(cpf.charAt(i)) * (10 - i);

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11)

        rev = 0;

    if (rev != parseInt(cpf.charAt(9)))

        return false;

    // Valida 2o digito

    add = 0;

    for (i = 0; i < 10; i ++)

        add += parseInt(cpf.charAt(i)) * (11 - i);

    rev = 11 - (add % 11);

    if (rev == 10 || rev == 11)

        rev = 0;

    if (rev != parseInt(cpf.charAt(10)))

        return false;

    return true;

}

// / fim validação de cpf

function OpenDialog(titulo, mensagem) {

    var div = document.createElement("div");

    div.setAttribute('id', 'dialog-message');

    document.body.appendChild(div);



    function fech(){

        $("#dialog-message").remove();

    }

    $(function() {

        $("#dialog-message").dialog({

            modal: true,

            moveToTop:true,

            buttons: {

                Ok: fech

            }

        });

        $("#dialog-message").dialog({title: titulo});

        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});

        $("#dialog-message").text(mensagem);

    });

}

//Função para confirmação antes de enviar o formulario

function DialogConfirm(titulo, mensagem,funcao,parametros) {

    var div = document.createElement("div");

    div.setAttribute('id', 'dialog-message');

    document.body.appendChild(div);

    function sim(){

        $("#dialog-message").remove();

        $("#validForm").click();

    }

    function fech(){

        $("#dialog-message").remove();

    }

    $(function() {

        $("#dialog-message").dialog({

            modal: true,

            moveToTop:true,

            buttons: {

                Não: fech,

                Sim:sim

            }

        });

        $("#dialog-message").dialog({title: titulo});

        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});

        $("#dialog-message").text(mensagem);

    });

}





var myJSONObject = {"navegacao": [{"p": 1, "u": root}]};



function Navegar(url,param){

    var pos = myJSONObject.navegacao.length;

    if(param=='go'){

        pos = pos+1;

        var url = {"p": pos, "u": url};

        myJSONObject.navegacao.push(url);

    }else{

        pos = pos-1;

        myJSONObject.navegacao.pop();

    }



    $.each(myJSONObject,function(key,data){

        $.each(data,function(id,valor){

            if(pos==1){

                $('.modal').modal('hide');

            }else

            if(valor.p==pos) {

                if(param=='go')

                    $('.modal').find('.modal-content').load(valor.u);

                else

                if(valor.u!="")

                    $('.modal').find('.modal-content').load(valor.u);



            }

        });

    });

}

function Acrescentar(url){

    console.log(url);
    var pos = myJSONObject.navegacao.length;

    pos = pos+1;

    var url2 = {"p": pos, "u": url};
    console.log(url2);
    myJSONObject.navegacao.push(url2);

}



function EnviarFormulario(form){
    console.log(form);
    $(form).ajaxForm({
        success: function(d){
                Navegar('','back');
        }
    });
}
function EnviarFormularioMovDetalhes(form) {
    $(form).ajaxForm({
        success: function (d) {
            Navegar(myJSONObject.navegacao[1].u, 'go');
        }
    });
}



//marcara cpf cep telefone

$(document).ready(function() {

    initComponent();



});
/*
//funcao padrao - dar uma trabalhada
function senduploads(form) {
    $('form#' + form).ajaxForm({
        uploadProgress: function (event, position, total, percentComplete) {
            $(".progress").css({"display": "block"});
            var percentVal = percentComplete + '%';
            $("[role='progressbar']").attr('style', 'width: ' + percentVal);
        },
        success: function (d) {
            d = JSON.parse(d);
            if(d.res==1){
                location.href=root+'/Anexos/all';
            }else{
                BootstrapDialog.alert({
                    type:BootstrapDialog.TYPE_DANGER,
                    title:'Atenção',
                    message:d.msg
                });
            }
        }
    });
}*/
//funcao padrao - dar uma trabalhada
function senduploads(form) {
    console.log(form);
    $('form#' + form).ajaxForm({
        uploadProgress: function (event, position, total, percentComplete) {
            $(".progress").css({"display": "block"});
            var percentVal = percentComplete + '%';
            $("[role='progressbar']").attr('style', 'width: ' + percentVal);
        },
        success: function (d) {
            d = JSON.parse(d)
            if (d.res == 1) {
                if (typeof d.redirect != 'undefined') {
                    location.href = root + d.redirect;
                } else {
                    location.href = root + '/Anexos/all'
                }
            } else {
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: 'Atenção',
                    message: d.msg
                });
            }
        }
    });
}


var click = null;

function initeComponentes(element) {

    $(element).find("[data-gallery='gal']").each(function () {
        $(this).click(function (event) {
            console.log(event)
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = {index: link, event: event},
                links = $("[data-gallery='gal']")
            blueimp.Gallery(links, options);
        })
    })

    $(element).find('.input-file').each(function () {
        $(this).fileinput({
            showUpload: false,
            maxFileCount: 20,
            layoutTemplates: {
                main1: "{preview}\n" +
                "<div class=\'input-group {class}\'>\n" +
                "   <div class=\'input-group-btn\'>\n" +
                "       {browse}\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>"
            },
        });
    });
    $(element).find("#bt-gerar").each(function () {
        $(this).click(function () {
            var modal = $('#modal');
            var documento = modal.find('.modal-content');

            if (documento.find("#subestoque").val() == "") {
                BootstrapDialog.alert('Selecione um Sub-estoque');
                return false;
            }
            var dados = {
                subestoque: documento.find("#subestoque").val(),
                tipo: documento.find("#tipo").val(),
                t: documento.find("#t").val()
            }

            $.ReportPost('pos_estoque_epi', dados);
        })
    })

}
function initComponent() {

    $('.cpf').mask('000.000.000-00', {reverse: true});

    $('.cep').mask('00000-000');

    $('.phone').mask('(00) 0000-00000');

    $('.date').mask('00/00/0000');

    $('.datetime').mask('00/00/0000 00:00');

    $('.cnpj').mask('00.000.000/0000-00');

    $('.money').mask('000.000.000.000.000,00', {reverse: true});


    $('[data="moeda"]').maskMoney();

    moment.locale('pt-BR');

    $('.datePicker').datetimepicker({
        format: 'L',
        extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true,
    });

    $('.datePicke_bottom').datetimepicker({
        format: 'L',
        extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }
    });


    $('.datetimepicker').datetimepicker({

        locale: 'pt-br',
        format: 'L',
        sideBySide: true,
        extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
        showTodayButton: true,
        useStrict: true,
        showClear: true,
        allowInputToggle: true,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }


    });
    $('.datetimepicker_bottom').datetimepicker({
        locale: 'pt-br',
        sideBySide: true,
        extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
        showTodayButton: true,
        useStrict: true,
        showClear: true,
        allowInputToggle: true,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }
    });

    $('.hours').datetimepicker({
        locale: 'pt-br',
        format: 'LT',
        allowInputToggle: true
    });

    $('.datetimepicker_top').datetimepicker({
        locale: 'pt-br',
        sideBySide: true,
        extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
        showTodayButton: true,
        useStrict: true,
        showClear: true,
        allowInputToggle: true,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'top'
        }


    });


}

//cep

function isset() {

    var a = arguments,

        l = a.length,

        i = 0,

        undef;



    if (l === 0) {

        throw new Error('Empty isset');

    }



    while (i !== l) {

        if (a[i] === undef || a[i] === null) {

            return false;

        }

        i++;

    }

    return true;

}

function empty(mixed_var) {

    var undef, key, i, len;

    var emptyValues = [undef, null, false, 0, '', '0'];



    for (i = 0, len = emptyValues.length; i < len; i++) {

        if (mixed_var === emptyValues[i]) {

            return true;

        }

    }



    if (typeof mixed_var === 'object') {

        for (key in mixed_var) {

            // TODO: should we check for own properties only?

            //if (mixed_var.hasOwnProperty(key)) {

            return false;

            //}

        }

        return true;

    }



    return false;

}
$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-tool='tooltip']").tooltip();
    $("[data-togle='tooltip']").tooltip();
})


$( "[data-list='collapse']" ).click(function () {

    if(empty($( this ).parent().attr('class'))){

        $(this).parent().attr('class','active');

        $(this).parent().css({"border-left":"none","border-right":"4px solid #19AA8D"});

        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360deg)","transform":" rotate(360deg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});

    }else{

        $(this).parent().removeAttr('class');

        $(this).children(".fa-th-large").removeAttr('style');

        $(this).children(".fa-th-large").css({"-webkit-transform":" rotate(360reg)","transform":" rotate(360reg)","-webkit-transition":"width 2s, height 2s, -webkit-transform 2s"});

        $(this).parent().css({"border-right":"none"});

    }

})

$("[role='open-adm']").click(function () {

    $("[role='adm']").css({"display":"block"});

});



var $image = $('.img-container > img');

$(".img-container > img").cropper({

    aspectRatio: 16 / 9,

    preview: ".img-preview",

    crop: function(data) {

        $("#dataX").val(Math.round(data.x));

        $("#dataY").val(Math.round(data.y));

        $("#dataHeight").val(Math.round(data.height));

        $("#dataWidth").val(Math.round(data.width));

        $("#dataRotate").val(Math.round(data.rotate));



    }

});



// Import image

var $inputImage = $('#inputImage'),

    URL = window.URL || window.webkitURL,

    blobURL;



if (URL) {

    $inputImage.change(function () {

        LoadGif();

            var files = this.files,

                file;



            if (!$image.data('cropper')) {

                return;

            }



            if (files && files.length) {

                file = files[0];



                if (/^image\/\w+$/.test(file.type)) {

                    blobURL = URL.createObjectURL(file);

                    $image.one('built.cropper', function () {

                        URL.revokeObjectURL(blobURL); // Revoke when load complete

                    }).cropper('reset').cropper('replace', blobURL);

                    $inputImage.val('');

                } else {

                    showMessage('Please choose an image file.');

                }

            }



    });

} else {

    $inputImage.parent().remove();

}



/*var cro = $(".img-container > img").cropper("getDataURL", "image/jpeg");



$.each(cro,function(key,value){

    console.log(value);

});*/





$("#inputImage").change(function () {

    $("#cropper-modal").modal("show");

});



function PaginarPost(pagina){

    $("#form").attr("action", root + "/"+$("#m").val()+"/"+$("#p").val()+"/?&page="+pagina);

    $("#form").submit();

}
function PaginarGet(pagina){
    $("#page").val(pagina);

    setTimeout(function(){
        $("#pesquisar").click();
    },1000);
}


function relatorios(id) {
    BootstrapDialog.show({
    title: 'Seleção de Relatórios',
    message: $('<div></div>').load(root + '/Estq_mov/relatorios/ajax:1/'),
        buttons: [{
            label: 'OK',
            cssClass: 'btn-primary',
            action: function(){
                //alert(id);
                window.open(root+"/Relatorios/"+$('#relatorios').val()+"/id:"+id+"/", "_blank");
            }
        }, {
            label: 'Fechar',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
});
}

function replaceAll(string, token, newtoken) {
    while (string.indexOf(token) != -1) {
        string = string.replace(token, newtoken);
    }
    return string;
}

function editarCliente(idCliente){
    if(idCliente.length > 0)
        window.open('/matriz/Financeiro/Cliente/edit?id='+idCliente,'_blank');
    else
        BootstrapDialog.alert('Selecione um Cliente!');
}

function editarEndereco(idEndereco){
    if(idEndereco.length > 0)
        window.open('/matriz/Financeiro/Cliente_endereco/edit?id='+idEndereco,'_blank');
    else
        BootstrapDialog.alert('Selecione um Endereco!');
}
