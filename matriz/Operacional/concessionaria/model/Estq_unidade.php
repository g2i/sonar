<?php
final class Estq_unidade extends Record{ 

    const TABLE = 'estq_unidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_unidade pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
}