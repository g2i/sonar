<?php
final class Rhprofissional extends Record{ 

    const TABLE = 'rhprofissional';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
         $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Rhprofissional possui Rhocorrencias
    * @return array de Rhocorrencias
    */
    function getRhocorrencias($criteria=NULL) {
        return $this->hasMany('Rhocorrencia','codigo_profissional',$criteria);
    }
    
    /**
    * Rhprofissional pertence a Rhsetor
    * @return Rhsetor $Rhsetor
    */
    function getRhsetor() {
        return $this->belongsTo('Rhsetor','setor');
    }

    function getStatus() {
        return $this->belongsTo('Rhstatus','status');
    }
    function getRhprofissional_funcao() {
        return $this->belongsTo('Rhprofissional_funcao','funcao');
    }
    /**
    * Rhprofissional possui Rhprofissional_contratacaos
    * @return array de Rhprofissional_contratacaos
    */
    function getRhprofissional_contratacaos($criteria=NULL) {
        return $this->hasMany('Rhprofissional_contratacao','codigo_profissional',$criteria);
    }
    
    /**
    * Rhprofissional possui Rhprofissional_dependentes
    * @return array de Rhprofissional_dependentes
    */
    function getRhprofissional_dependentes($criteria=NULL) {
        return $this->hasMany('Rhprofissional_dependente','codigo_profissional',$criteria);
    }
}