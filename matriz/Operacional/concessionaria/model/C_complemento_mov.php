<?php
final class C_complemento_mov extends Record
{

    const TABLE = 'c_complemento_mov';
    const PK = 'id';
    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }

    /**
     * Concessionaria_andamento pertence a Situacao
     * @return Situacao $Situacao
     */
    function getUser() {
        return $this->belongsTo('Usuario','user');
    }
    function getColaborador() {
        return $this->belongsTo('Rhprofissional','colaborador');
    }

    function getDetalhesMov() {
        return $this->belongsTo('Estq_movdetalhes','mov_detalhes');
    }
}