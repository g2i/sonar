<?php
final class Estq_subgrupo extends Record{ 

    const TABLE = 'estq_subgrupo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_subgrupo possui Estq_artigos
    * @return array de Estq_artigos
    */
    function getEstq_artigos($criteria=NULL) {
        return $this->hasMany('Estq_artigo','subgrupo',$criteria);
    }
    
    /**
    * Estq_subgrupo pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_subgrupo pertence a Estq_grupo
    * @return Estq_grupo $Estq_grupo
    */
    function getEstq_grupo() {
        return $this->belongsTo('Estq_grupo','grupo');
    }
}