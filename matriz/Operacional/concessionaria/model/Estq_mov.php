<?php
final class Estq_mov extends Record{ 

    const TABLE = 'estq_mov';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_mov pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_mov pertence a Estq_tipomov
    * @return Estq_tipomov $Estq_tipomov
    */
    function getEstq_tipomov() {
        return $this->belongsTo('Estq_tipomov','tipo');
    }

    /**
     * Estq_mov pertence a Equipe
     * @return Equipe $Equipe
     */
    function getEquipe() {
        return $this->belongsTo('Equipe','codigo_equipe_id');
    }
    /**
    * Estq_mov possui Estq_movdetalhes
    * @return array de Estq_movdetalhes
    */
    function getEstq_movdetalhes($criteria=NULL) {
        return $this->hasMany('Estq_movdetalhes','movimento',$criteria);
    }
    function getConcessionariaCidade(){
        return $this->belongsTo('Concessionaria_cidade','municipio_id');
    }
    function getFornecedors() {
        return $this->belongsTo('Fornecedor','fornecedor');
    }
    function getCliente() {
        return $this->belongsTo('Cliente','cliente');
    }
    function getRhprofissional() {
        return $this->belongsTo('Rhprofissional','colaborador');
    }
    function getTipo_servico() {
        return $this->belongsTo('Concessionaria_tipo_servico','tipo_servico');
    }
     function getAndamento() {
        return $this->belongsTo('Concessionaria_andamento','andamento');
    }
    function getSituacaoMov() {
        return $this->belongsTo('Concessionaria_situacao','situacao_mov');
    }
    function getMotivoRetirada(){
        return $this->belongsTo('Concessionaria_motivo_retirada','motivo_retirada');
    }
    function getComponentes(){
        return $this->belongsTo('Concessionaria_componentes','componente_id');
    }
    function getGrupoSupervisao(){
        return $this->belongsTo('Concessionaria_grupo_supervisao','grupo_supervisao_id');
    }
}
