<?php
final class Fornecedor extends Record{ 

    const TABLE = 'fornecedor';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('situacao','<>',3);
         return $criteria;
    }
}