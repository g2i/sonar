<?php
final class Estq_movdetalhes extends Record{ 

    const TABLE = 'estq_movdetalhes';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_movdetalhes pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_movdetalhes pertence a Estq_artigo
    * @return Estq_artigo $Estq_artigo
    */
    function getEstq_artigo() {
        return $this->belongsTo('Estq_artigo','artigo');
    }
    
    /**
    * Estq_movdetalhes pertence a Estq_mov
    * @return Estq_mov $Estq_mov
    */
    function getEstq_mov() {
        return $this->belongsTo('Estq_mov','movimento');
    }
    
    /**
    * Estq_movdetalhes pertence a Estq_lote_substoque
    * @return Estq_lote_substoque $Estq_lote_substoque
    */
    function getEstq_lote_substoque() {
        return $this->belongsTo('Estq_lote_substoque','lote_substoque');
    }
    function getEstq_artigo_lote() {
        return $this->belongsTo('Estq_artigo_lote','lote');
    }
    function getEstq_subestoque() {
        return $this->belongsTo('Estq_subestoque','substoque');
    }

}