<?php
final class Concessionaria_andamento extends Record{ 

    const TABLE = 'concessionaria_andamento';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Concessionaria_andamento pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao');
    }
    
    /**
    * Concessionaria_andamento possui Estq_movs
    * @return array de Estq_movs
    */
    function getEstq_movs($criteria=NULL) {
        return $this->hasMany('Estq_mov','andamento',$criteria);
    }
}