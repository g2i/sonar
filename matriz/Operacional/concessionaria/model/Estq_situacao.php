<?php
final class Estq_situacao extends Record{ 

    const TABLE = 'estq_situacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_situacao possui Estq_artigos
    * @return array de Estq_artigos
    */
    function getEstq_artigos($criteria=NULL) {
        return $this->hasMany('Estq_artigo','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_artigo_lotes
    * @return array de Estq_artigo_lotes
    */
    function getEstq_artigo_lotes($criteria=NULL) {
        return $this->hasMany('Estq_artigo_lote','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_artigocaracteristicas
    * @return array de Estq_artigocaracteristicas
    */
    function getEstq_artigocaracteristicas($criteria=NULL) {
        return $this->hasMany('Estq_artigocaracteristicas','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_group_useres
    * @return array de Estq_group_useres
    */
    function getEstq_group_useres($criteria=NULL) {
        return $this->hasMany('Estq_group_user','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_grupos
    * @return array de Estq_grupos
    */
    function getEstq_grupos($criteria=NULL) {
        return $this->hasMany('Estq_grupo','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_lote_substoques
    * @return array de Estq_lote_substoques
    */
    function getEstq_lote_substoques($criteria=NULL) {
        return $this->hasMany('Estq_lote_substoque','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_movs
    * @return array de Estq_movs
    */
    function getEstq_movs($criteria=NULL) {
        return $this->hasMany('Estq_mov','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_movdetalhes
    * @return array de Estq_movdetalhes
    */
    function getEstq_movdetalhes($criteria=NULL) {
        return $this->hasMany('Estq_movdetalhes','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_subestoques
    * @return array de Estq_subestoques
    */
    function getEstq_subestoques($criteria=NULL) {
        return $this->hasMany('Estq_subestoque','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_subgrupos
    * @return array de Estq_subgrupos
    */
    function getEstq_subgrupos($criteria=NULL) {
        return $this->hasMany('Estq_subgrupo','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_tipomovs
    * @return array de Estq_tipomovs
    */
    function getEstq_tipomovs($criteria=NULL) {
        return $this->hasMany('Estq_tipomov','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_tiposubestoques
    * @return array de Estq_tiposubestoques
    */
    function getEstq_tiposubestoques($criteria=NULL) {
        return $this->hasMany('Estq_tiposubestoque','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Estq_users
    * @return array de Estq_users
    */
    function getEstq_users($criteria=NULL) {
        return $this->hasMany('Estq_users','situacao',$criteria);
    }
    
    /**
    * Estq_situacao possui Stq_caracteristicas
    * @return array de Stq_caracteristicas
    */
    function getStq_caracteristicas($criteria=NULL) {
        return $this->hasMany('Stq_caracteristica','situacao',$criteria);
    }

    function getStq_fornecedores($criteria=NULL) {
        return $this->hasMany('Estq_fornecedor','situacao',$criteria);
    }
}