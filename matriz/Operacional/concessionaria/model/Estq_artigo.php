<?php
final class Estq_artigo extends Record{ 

    const TABLE = 'estq_artigo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=','3');
        return $criteria;
    }
    
    /**
    * Estq_artigo pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_artigo pertence a Estq_subgrupo
    * @return Estq_subgrupo $Estq_subgrupo
    */
    function getEstq_subgrupo() {
        return $this->belongsTo('Estq_subgrupo','subgrupo');
    }
    
    /**
    * Estq_artigo possui Estq_artigo_lotes
    * @return array de Estq_artigo_lotes
    */
    function getEstq_artigo_lotes($criteria=NULL) {
        return $this->hasMany('Estq_artigo_lote','artigo',$criteria);
    }
    
    /**
    * Estq_artigo possui Estq_artigocaracteristicas
    * @return array de Estq_artigocaracteristicas
    */
    function getEstq_artigocaracteristicas($criteria=NULL) {
        return $this->hasMany('Estq_artigocaracteristicas','artigo',$criteria);
    }
    
    /**
    * Estq_artigo possui Estq_lote_substoques
    * @return array de Estq_lote_substoques
    */
    function getEstq_lote_substoques($criteria=NULL) {
        return $this->hasMany('Estq_lote_substoque','artigo',$criteria);
    }
    
    /**
    * Estq_artigo possui Estq_movdetalhes
    * @return array de Estq_movdetalhes
    */
    function getEstq_movdetalhes($criteria=NULL) {
        return $this->hasMany('Estq_movdetalhes','artigo',$criteria);
    }

    function getUnidade($criteria=NULL) {
        return $this->belongsTo('Estq_unidade','unidade',$criteria);
    }
}