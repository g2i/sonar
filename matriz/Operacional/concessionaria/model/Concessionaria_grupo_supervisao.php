<?php
final class Concessionaria_grupo_supervisao extends Record{ 

    const TABLE = 'concessionaria_grupo_supervisao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    

    /**
    * Concessionaria_grupo_supervisao pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }

    function getCidades() {
        return $this->hasMany('Concessionaria_cidade','supervisao_id');
    }
    /**
    * Concessionaria_grupo_supervisao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','criado_por');
    }
    
    /**
    * Concessionaria_grupo_supervisao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}