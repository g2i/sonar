<?php
final class Concessionaria_imagem extends Record{

    const TABLE = 'concessionaria_imagem';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure(){
         #$criteria = new Criteria();
       # return $criteria;
    }

    /**
     * Concessionaria_imagem pertence a Usuario
     * @return Usuario $Usuario
     */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }

    /**
     * Concessionaria_imagem pertence a Situacao
     * @return Situacao $Situacao
     */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}