<?php
final class Equipe extends Record{ 

    const TABLE = 'equipe';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Equipe pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao');
    }
    
    /**
    * Equipe possui Equipe_movimentos
    * @return array de Equipe_movimentos
    */
    function getEquipe_movimentos($criteria=NULL) {
        return $this->hasMany('Equipe_movimento','equipe_id',$criteria);
    }
}