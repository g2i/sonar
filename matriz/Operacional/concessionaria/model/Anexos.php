<?php
final class Anexos extends Record{ 

    const TABLE = 'anexos';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Anexos pertence a Tipo_anexos
    * @return TipoAnexos $Tipo_anexos
    */
    function getTipo_anexos() {
        return $this->belongsTo('TipoAnexos','tipo_id');
    }
    
    /**
    * Anexos pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Anexos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','user_created');
    }
    
    /**
    * Anexos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','user_modified');
    }
}