<?php
final class Concessionaria_item_servico extends Record{ 

    const TABLE = 'concessionaria_item_servico';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    function getMovimentacao() {
        return $this->belongsTo('Estq_mov','estq_mov_id');
    }
    function getServico() {
        return $this->belongsTo('Concessionaria_servico','servico_id');
    }
    function getFator() {
        return $this->belongsTo('Concessionaria_fator','us');
    }    
    /**
    * Concessionaria_item_servicos pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}