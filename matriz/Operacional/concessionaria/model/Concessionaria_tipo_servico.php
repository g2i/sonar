<?php
final class Concessionaria_tipo_servico extends Record{ 

    const TABLE = 'concessionaria_tipo_servico';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Concessionaria_tipo_servico pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao');
    }
    
    /**
    * Concessionaria_tipo_servico possui Estq_movs
    * @return array de Estq_movs
    */
    function getEstq_movs($criteria=NULL) {
        return $this->hasMany('Estq_mov','tipo_servico',$criteria);
    }
}