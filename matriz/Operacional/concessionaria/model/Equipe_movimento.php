<?php
final class Equipe_movimento extends Record{ 

    const TABLE = 'equipe_movimento';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Equipe_movimento pertence a Equipe
    * @return Equipe $Equipe
    */
    function getEquipe() {
        return $this->belongsTo('Equipe','equipe_id');
    }
    
    /**
    * Equipe_movimento pertence a Estq_mov
    * @return Estq_mov $Estq_mov
    */
    function getEstq_mov() {
        return $this->belongsTo('Estq_mov','movimento_id');
    }
    
    /**
    * Equipe_movimento pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao');
    }
}