<?php
final class Rhprofissional_contratacao extends Record{ 

    const TABLE = 'rhprofissional_contratacao';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rhprofissional_contratacao possui Anexocontratacaos
    * @return array de Anexocontratacaos
    */
    function getAnexocontratacaos($criteria=NULL) {
        return $this->hasMany('Anexocontratacao','codigo_contratacao',$criteria);
    }
    
    /**
    * Rhprofissional_contratacao pertence a Rhcentrocusto
    * @return Rhcentrocusto $Rhcentrocusto
    */
    function getRhcentrocusto() {
        return $this->belongsTo('Rhcentrocusto','centrocusto');
    }
    
    /**
    * Rhprofissional_contratacao pertence a Rhprofissional
    * @return Rhprofissional $Rhprofissional
    */
    function getRhprofissional() {
        return $this->belongsTo('Rhprofissional','codigo_profissional');
    }
    
    /**
    * Rhprofissional_contratacao possui Rhprofissional_salario_historicos
    * @return array de Rhprofissional_salario_historicos
    */
    function getRhprofissional_salario_historicos($criteria=NULL) {
        return $this->hasMany('Rhprofissional_salario_historico','rhprofissional_contratacao_id',$criteria);
    }
}