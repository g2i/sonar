<?php
final class TipoAnexos extends Record{

    const TABLE = 'tipo_anexos';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    

    
    /**
    * Anexos pertence a Situacao
    * @return Object
     */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Anexos pertence a Usuario
    * @return Object
     */
    function getUsuario() {
        return $this->belongsTo('Usuario','user_id');
    }
    

}