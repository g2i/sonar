<?php
final class Frota_veiculos extends Record{ 

    const TABLE = 'frota_veiculos';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_veiculos possui Frota_abastecimentos
    * @return array de Frota_abastecimentos
    */
    function getFrota_abastecimentos($criteria=NULL) {
        return $this->hasMany('Frota_abastecimentos','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos possui Frota_acidentes
    * @return array de Frota_acidentes
    */
    function getFrota_acidentes($criteria=NULL) {
        return $this->hasMany('Frota_acidente','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos possui Frota_equipamentos_veiculos
    * @return array de Frota_equipamentos_veiculos
    */
    function getFrota_equipamentos_veiculos($criteria=NULL) {
        return $this->hasMany('Frota_equipamentos_veiculo','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos possui Frota_historico_motorista_veiculos
    * @return array de Frota_historico_motorista_veiculos
    */
    function getFrota_historico_motorista_veiculos($criteria=NULL) {
        return $this->hasMany('Frota_historico_motorista_veiculo','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos possui Frota_inspecaos
    * @return array de Frota_inspecaos
    */
    function getFrota_inspecaos($criteria=NULL) {
        return $this->hasMany('Frota_inspecao','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos possui Frota_manutencoes
    * @return array de Frota_manutencoes
    */
    function getFrota_manutencoes($criteria=NULL) {
        return $this->hasMany('Frota_manutencoes','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos possui Frota_motoristas
    * @return array de Frota_motoristas
    */
    function getFrota_motoristas($criteria=NULL) {
        return $this->hasMany('Frota_motorista','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos possui Frota_ocorrencias
    * @return array de Frota_ocorrencias
    */
    function getFrota_ocorrencias($criteria=NULL) {
        return $this->hasMany('Frota_ocorrencias','veiculo_id',$criteria);
    }
    
    /**
    * Frota_veiculos pertence a Frota_origem
    * @return Frota_origem $Frota_origem
    */
    function getFrota_origem() {
        return $this->belongsTo('Frota_origem','origem_id');
    }
    
    /**
    * Frota_veiculos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
    
    /**
    * Frota_veiculos pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Frota_veiculos pertence a Frota_localizacao
    * @return Frota_localizacao $Frota_localizacao
    */
    function getFrota_localizacao() {
        return $this->belongsTo('Frota_localizacao','localizacao_id');
    }
    
    /**
    * Frota_veiculos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','alterado_por');
    }
    
    /**
    * Frota_veiculos pertence a Frota_motorista
    * @return Frota_motorista $Frota_motorista
    */
    function getFrota_motorista() {
        return $this->belongsTo('Frota_motorista','motorista_id');
    }
}