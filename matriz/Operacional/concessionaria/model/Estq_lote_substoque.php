<?php
final class Estq_lote_substoque extends Record{ 

    const TABLE = 'estq_lote_substoque';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        $criteria = new Criteria();
        $criteria->addCondition('situacao','!=',3);
         return $criteria;
    }
    
    /**
    * Estq_lote_substoque pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_lote_substoque pertence a Estq_artigo
    * @return Estq_artigo $Estq_artigo
    */
    function getEstq_artigo() {
        return $this->belongsTo('Estq_artigo','artigo');
    }
    
    /**
    * Estq_lote_substoque pertence a Estq_artigo_lote
    * @return Estq_artigo_lote $Estq_artigo_lote
    */
    function getEstq_artigo_lote() {
        return $this->belongsTo('Estq_artigo_lote','lote');
    }
    
    /**
    * Estq_lote_substoque pertence a Estq_subestoque
    * @return Estq_subestoque $Estq_subestoque
    */
    function getEstq_subestoque() {
        return $this->belongsTo('Estq_subestoque','subestoque');
    }
    
    /**
    * Estq_lote_substoque possui Estq_movdetalhes
    * @return array de Estq_movdetalhes
    */
    function getEstq_movdetalhes($criteria=NULL) {
        return $this->hasMany('Estq_movdetalhes','lote_substoque',$criteria);
    }
}