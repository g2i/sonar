<?php
$usuario=Session::get('user');
if($usuario !== null){
    ?>
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>

            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <a role="open-adm" class="animatedClick" data-target='clickExample'>
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>
                <li>
                    <?php echo $this->Html->getLink('<i class="fa fa-sign-out"></i> Sair', 'Login', 'logout'); ?>
                </li>
            </ul>
        </nav>
    </div>

    <div id="right-sidebar" style="display: none" role="adm" class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
        <div class="sidebar-container" full-scroll>

            <ul class="nav nav-tabs navs-2" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Administração</a>
                </li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuários</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" style="background: #2f4050">
                <div role="tabpanel" class="tab-pane active" id="home" >
                    <div class="sidebar-title">
                        <h3><i class="fa fa-gears"></i> Ajustes </h3>
                        <small><i class="fa fa-tim"></i> Área do administrador.</small>
                    </div>
                    <nav class="navbar-default navbar-static" role="tablist">
                        <div class="sidebar-collapse">
                            <ul class="nav">
                                <?php if($usuario->id==1){ ?>
                                <li <?php echo(CONTROLLER == "Estq_group_user" ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse" href="#Estq_group_user"
                                       aria-expanded="false" aria-controls="Estq_group_user">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Grupos de Usuários</span><span
                                            class="fa arrow"></span></a>

                                    <div id="Estq_group_user" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false" style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Estq_group_user" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Estq_group_user', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_group_user" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Estq_group_user', 'add'); ?>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li <?php echo(CONTROLLER == "Estq_users" ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse" href="#Estq_users"
                                       aria-expanded="false" aria-controls="Estq_users">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span><span
                                            class="fa arrow"></span></a>

                                    <div id="Estq_users" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false" style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Estq_users" && ACTION == "all" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> All', 'Estq_users', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_users" && ACTION == "add" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Estq_users', 'add'); ?>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <?php } ?>
                                <li <?php echo((CONTROLLER == "Estq_tipomov"  || CONTROLLER == "Estq_subestoque"||CONTROLLER == "Estq_tiposubestoque"||CONTROLLER == "Concessionaria_andamento"||CONTROLLER == "Concessionaria_situacao" ||CONTROLLER == "Concessionaria_tipo_servico"  ) ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse" href="#Estq_adm"
                                       aria-expanded="false" aria-controls="Estq_adm">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Administração</span><span
                                            class="fa arrow"></span></a>

                                    <div id="Estq_adm" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false" style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Concessionaria_andamento"  ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Andamento', 'Concessionaria_andamento', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Stq_caracteristica" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom">
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Características', 'Stq_caracteristica', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Equipe"  ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Codigo Equipe', 'Equipe', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_fornecedor" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom">
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Fornecedor', 'Estq_fornecedor', 'all'); ?>
                                            </li>
<!--                                            <li --><?php //echo(CONTROLLER == "Estq_grupo" ? 'class="active"' : ''); ?><!-- data-toggle="tooltip" data-placement="bottom" >-->
<!--                                                --><?php //echo $this->Html->getLink('<i class="fa fa-th-large"></i> Grupo', 'Estq_grupo', 'all'); ?>
<!--                                            </li>-->
<!--                                            <li --><?php //echo(CONTROLLER == "Estq_subestoque" ? 'class="active"' : ''); ?><!--
                                               --><?php //echo $this->Html->getLink('<i class="fa fa-th-large"></i> Sub-Estoque', 'Estq_subestoque', 'all'); ?>
<!--                                            </li>-->
                                            <li <?php echo(CONTROLLER == "Concessionaria_situacao"  ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Situação Movimento', 'Concessionaria_situacao', 'all'); ?>
                                            </li>
<!--                                            <li --><?php //echo(CONTROLLER == "Estq_situacao" && ACTION == "all" ? 'class="active"' : ''); ?><!--
                                               --><?php //echo $this->Html->getLink('<i class="fa fa-th-large"></i> Situação', 'Estq_situacao', 'all'); ?>
<!--                                            </li>-->
                                            <li <?php echo(CONTROLLER == "Concessionaria_grupo_supervisao"  ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Origem', 'Concessionaria_grupo_supervisao', 'all'); ?>
                                            </li>
<!--                                            <li --><?php //echo(CONTROLLER == "Estq_subgrupo" ? 'class="active"' : ''); ?><!-- data-toggle="tooltip" data-placement="bottom">-->
<!--                                                --><?php //echo $this->Html->getLink('<i class="fa fa-th-large"></i> Subgrupo', 'Estq_subgrupo', 'all'); ?>
<!--                                            </li>-->
                                            <li <?php echo(CONTROLLER == "TipoAnexos"  ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Tipo de Anexos', 'TipoAnexos', 'all'); ?>
                                            </li>
<!--                                            <li --><?php //echo(CONTROLLER == "Estq_tipomov"  ? 'class="active"' : ''); ?><!--
                                                --><?php //echo $this->Html->getLink('<i class="fa fa-th-large"></i> Tipo de Movimento', 'Estq_tipomov', 'all'); ?>
<!--                                            </li>-->
                                          
<!--                                            <li --><?php //echo(CONTROLLER == "Estq_tiposubestoque" ? 'class="active"' : ''); ?><!--
                                                --><?php //echo $this->Html->getLink('<i class="fa fa-th-large"></i> Tipo Subestoque', 'Estq_tiposubestoque', 'all'); ?>
<!--                                            </li> -->
                                            <li <?php echo(CONTROLLER == "Concessionaria_tipo_servico"  ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Tipo de serviço', 'Concessionaria_tipo_servico', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Concessionaria_servico"  ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i> Serviço', 'Concessionaria_servico', 'all'); ?>
                                            </li>
                                            <li <?php echo(CONTROLLER == "Estq_unidade" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom">
                                                <?php echo $this->Html->getLink('<i class="fa fa-th-large"></i>Tipos de Unidade', 'Estq_unidade', 'all'); ?>
                                            </li>
<!--                                            <li --><?php //echo(CONTROLLER == "Concessionaria_componentes" ? 'class="active"' : ''); ?><!-- data-toggle="tooltip" data-placement="bottom">-->
<!--                                                --><?php //echo $this->Html->getLink('<i class="fa fa-th-large"></i>Tipos de Componentes', 'Concessionaria_componentes', 'all'); ?>
<!--                                            </li>-->


                                        </ul>
                                    </div>
                                </li>

                            </ul>
                        </div>
                    </nav>

                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-users"></i> Usuário</h3>
                        <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                    </div>
                    <nav class="navbar-default navbar-static" role="tablist">
                        <div class="sidebar-collapse">
                            <ul class="nav">
                                <li <?php echo(CONTROLLER == "Usuario" ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse" href="#Estq_users2"
                                       aria-expanded="false" aria-controls="Estq_users">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span><span
                                            class="fa arrow"></span></a>

                                    <div id="Estq_users2" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false" style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Usuario" && ACTION == "conta" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-user"></i>Conta', 'Usuario', 'conta'); ?>
                                            </li>
                                         
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

<?php } ?>