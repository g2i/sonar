<li class="nav-header">
    <div class="dropdown profile-element">
    <span>
    <?php echo(file_exists(SITE_PATH . "/content/img/" . sprintf("%06d", @Session::get("user")->id) . ".png") ? '<img class="img-circle" src="' . SITE_PATH . '/content/img/' . sprintf("%06d", @Session::get("user")->id) . '.png" alt="Foto"/>' : '<img class="img-circle" src="' . SITE_PATH . '/content/img/empty-avatar.png" alt="Foto"/>') ?>
    </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
    <span class="clear"> <span class="block m-t-xs"> <strong
                class="font-bold"><?php echo @Session::get("user")->nome; ?></strong>
    </span> <span class="text-muted text-xs block">Colaborador <b class="caret"></b></span> </span> </a>
        <ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><?php echo $this->Html->getLink('Sair', 'Login', 'logout'); ?></li>
        </ul>
    </div>
    <div class="logo-element">
        G2i
    </div>
</li>

<li <?php echo(CONTROLLER == "Estq_mov" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" >
    <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> O.S', 'Estq_mov', 'all'); ?>
</li>

<li <?php echo(CONTROLLER == "Clientes" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" >
    <a href="/matriz/Financeiro/Cliente/all"><i class="fa fa-users"></i> Clientes</a>
</li>
<!--<li <?php echo((CONTROLLER == "Estq_mov" || CONTROLLER == "Estq_movdetalhes" )? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-exchange"></i><span class="nav-label">Movimento</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Estq_mov" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Lista de Movimentos">
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Lista', 'Estq_mov', 'all'); ?>
        </li>
    </ul>
</li>-->

<li <?php echo(CONTROLLER == "Estq_artigo" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-cube" data-toggle="tooltip" data-placement="auto" title="Artigos" ></i><span class="nav-label">Artigos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Estq_artigo"? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Lista de Artigos">
            <?php echo $this->Html->getLink('<i class="fa fa-list-alt"></i> lista', 'Estq_artigo', 'all'); ?>
        </li>
        <!--
        <li <?php echo(CONTROLLER == "Estq_artigo"? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Posição do estoque Novos">
            <?php echo $this->Html->getLink('<i class="fa fa-file-text"></i> Posição do estoque', 'Relatorios', 'posicao',array('a'=>0),array('data-toggle'=>'modal')); ?>
        </li>
        -->
    </ul>
</li>
<!--
<li <?php echo((CONTROLLER == "Estq_grupo" || CONTROLLER == "Estq_subgrupo"||CONTROLLER == "Stq_caracteristica"||CONTROLLER == "Estq_fornecedor" || CONTROLLER =='Estq_unidade') ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-plus-square"></i><span class="nav-label">Cadastros</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Estq_grupo" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Grupos">
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Grupo', 'Estq_grupo', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Estq_subgrupo" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Subgrupos">
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Subgrupo', 'Estq_subgrupo', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Stq_caracteristica" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Características">
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Características', 'Stq_caracteristica', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Estq_fornecedor" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Forenecedores">
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Fornecedor', 'Estq_fornecedor', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Estq_unidade" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Tipos de unidades">
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i>Tipos de Unidade', 'Estq_unidade', 'all'); ?>
        </li>
    </ul>
</li>-->
<li <?php echo(CONTROLLER == "Anexos"? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Lista de Anexos">
            <?php echo $this->Html->getLink('<i class="fa fa-cloud-upload"></i> Anexos', 'Anexos', 'all'); ?>
</li>

 <!-- 
<li <?php echo((CONTROLLER == "Relatorios"  )? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-file-text"></i><span class="nav-label">Relatórios</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
      <li <?php echo(CONTROLLER == "Relatorios" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Relatorios">
            <?php echo $this->Html->getLink('<i class="fa fa-print"></i> Relatórios', 'Relatorios', 'all',null, array('data-toggle'=>'modal')); ?>
        </li>
        <li <?php echo(CONTROLLER == "Relatorios" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Ficha de Estoque">
            <?php echo $this->Html->getLink('<i class="glyphicon glyphicon-folder-open"></i> Ficha de Estoque', 'Relatorios', 'ficha',null, array('data-toggle'=>'modal')); ?>
        </li>
    </ul>
</li>

<li <?php echo(CONTROLLER == "Relatorios" ? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Ficha de Estoque">
    <?php echo $this->Html->getLink('<i class="glyphicon glyphicon-folder-open"></i> Ficha de Estoque', 'Relatorios', 'ficha',null, array('data-toggle'=>'modal')); ?>
</li>
-->
<li <?php echo(CONTROLLER == "Anexos"? 'class="active"' : ''); ?> data-toggle="tooltip" data-placement="bottom" title="Lista de Anexos">
    <?php echo $this->Html->getLink('<i class="fa fa-print"></i> <span class="nav-label">Relatórios</span>', 'Relatorios', 'all'); ?>
</li>
<!--
<li <?php echo(CONTROLLER == "Index" ? 'class="active"' : ''); ?>>
    <?php echo $this->Html->getLink('<i class="fa fa-thumb-tack"></i> <span class="nav-label">Estatísticas</span>', 'Index', 'index'); ?>
</li>
-->
<!--
<li <?php echo(CONTROLLER == "Graficos" ? 'class="active"' : ''); ?>>
    <a href="http://grupog2i.ddns.com.br:3022/" target="_blank"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Análise Gráfica</span></a>
</li>
-->
<li <?php echo(CONTROLLER == "pedidos" ? 'class="active"' : ''); ?>>
<!--    <a href="http://grupog2i.ddns.com.br:3022/" target="_blank"><i class="glyphicon glyphicon-shopping-cart"></i> <span class="nav-label">Pedidos de Compra</span></a>-->
</li>





