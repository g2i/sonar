<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_movdetalhes', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group col-md-12 col-sm-6 col-xs-12">
                            <label for="artigo" class="required">Descrição<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="artigo" class="form-control artigo artigo-ajax" id="artigo" >
                                <option value="">Selecione:</option>
                                <?php
                                    echo '<option value="' . $Estq_artigos->id . '" selected>' . $Estq_artigos->nome . '</option>';
                                ?>

                            </select>
                        </div>
                        <div class="form-group col-md-12 col-sm-6 col-xs-12">
                            <label for="codigo_livre" class="required">Código</label>
                            <select name="codigo_livre" class="form-control codigo codigo-ajax" id="codigo_livre">
                                <option value="">Selecione:</option>
                                <?php
                                    echo '<option value="' . $Estq_artigos->id . '" selected>' . $Estq_artigos->codigo_livre . '</option>';
                                ?>

                            </select>
                        </div>
                        <input type="hidden" name="lote_substoque" value="-1" />
                        <div class="form-group col-md-12 col-sm-6 col-xs-12">
                            <label for="quantidade" class="required">Quantidade:<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="number" step="0,01" name="quantidade" id="quantidade" class="form-control"
                                   value="<?php echo $Estq_movdetalhes->quantidade ?>" placeholder="Quantidade" >
                        </div>

                        <input type="hidden" name="vlnf" id="vlnf" value="0" >
                        <input type="hidden" name="vlcusto" id="vlcusto" value="0" >
                        <input type="hidden" name="vlcustomedio" id="vlcustomedio" value="0" >
                        <input type="hidden" name="marca" id="marca" value="" >
                        <input type='hidden' name="validade" id="validade" class="form-control date"  value="<?php echo $Estq_movdetalhes->validade ?>" />

                        <input type="hidden" name="situacao" value="1"/>

                        <?php if($this->getParam('modal')){ ?>

                            <?php }else{ ?>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="movimento">Movimento</label>
                            <select name="movimento" class="form-control" id="movimento">
                                <option value="">Selecione</option>
                                <?php
                                foreach ($Estq_movs as $e) {
                                    if ($e->id == $Estq_movdetalhes->movimento)
                                        echo '<option selected value="' . $e->id . '">' . $e->tipodoc . '</option>';
                                    else
                                        echo '<option value="' . $e->id . '">' . $e->tipodoc . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    <?php } ?>

                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Estq_movdetalhes->id;?>">
                    <?php if($this->getParam('modal')){ ?>
                        <input type="hidden" name="modal" value="1"/>
                        <div class="text-right">
                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                                Cancelar
                            </a>
                            <button type="button" class="btn btn-primary" onclick="return DialogLocal('Confirmação','Deseja salvar as Alterações? ')"> Salvar</button>
                            <input type="submit" onclick="EnviarFormulario('form')" id="validForm" style="display: none;" />
                        </div>
                    <?php }else{ ?>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Estq_movdetalhes', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
    </div>
</div>
<input type="hidden" id="const" class="const" value="0" />
<script>
    $(document).ready(function() {
        $(".lote_substo").val($(".lote_substoque").val());
        $(".lote_substoque").change(function () {
                $(".lote_substo").val($(this).val());
        })
        $('.money').mask('000.000.000.000.000,00', {reverse: true});

        $('select.codigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            })
        });
        $('select.artigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar2/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            })
        });

        $('.artigo').change(function () {
            if ($(".const").val() != 1) {
                $.ajax({
                    type: 'POST',
                    url: root + '/Estq_movdetalhes/GetLoteSubestoque',
                    data: 'artigo=' + $(this).val(),
                    success: function (txt) {
                        $(".lote_substoque").html(txt);
                        $(".const").val(1);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: root + '/Estq_movdetalhes/GetCodigo',
                    data: 'artigo=' + $(this).val(),
                    success: function (txt) {
                        $(".codigo").html(txt);
                        $(".const").val(1);
                    }
                });
            }
        });


        $('.codigo').change(function () {
            if ($(".const").val() != 1) {
                $.ajax({
                    type: 'POST',
                    url: root + '/Estq_movdetalhes/GetLoteSubestoque',
                    data: 'artigo=' + $(this).val(),
                    success: function (txt) {
                        $(".lote_substoque").html(txt);
                        $(".const").val(1);
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: root + '/Estq_movdetalhes/GetArtigo',
                    data: 'artigo=' + $(this).val(),
                    success: function (txt) {
                        $(".artigo").html(txt);
                        $(".const").val(1);

                    }
                });
            }
        });

        moment.locale('pt-BR');

        $('.datePicker').datetimepicker({
            format: 'L',
            extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
            showTodayButton: true,
            useStrict: true,
            locale: 'pt-BR',
            showClear: true,
            allowInputToggle: true
        });


    });


    function DialogLocal(titulo, mensagem) {
        var div = document.createElement("div");
        div.setAttribute('id', 'dialog-messag');
        document.body.appendChild(div);
        function sim(){
            $("#dialog-messag").remove();
            $("#validForm").click();
            return true;
        }
        function fech(){
            $("#dialog-messag").remove();
            return false;
        }
        $(function() {
            $("#dialog-messag").dialog({
                modal: true,
                moveToTop:true,
                buttons: {
                    Não: fech,
                    Sim:sim
                }
            });
            $("#dialog-messag").dialog({title: titulo});
            $("#dialog-messag").dialog({position: {my: "center", at: "center", of: window}});
            $("#dialog-messag").text(mensagem);
        });
    }

</script>