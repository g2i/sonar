<style type="text/css">
    .options {
        min-width: 20px;
        max-width: 20px;
    }


</style>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row table-responsive" style="overflow: auto; ">
                    <table id="tblMovimento" class="table table-condensed table-hover table-striped fixed_headers" >
                        <thead >
                        <tr class="header">
                            <th data-column-id="id" data-type="numeric" data-visible="false" data-visibleInSelection="false" data-identifier="true"></th>
                            <th data-column-id="codigo" data-sortable="true">C&oacute;digo</th>
                            <th data-column-id="artigo" data-formatter="artigo" data-sortable="true">Artigo</th>
                            <th data-column-id="quantidade" data-formatter="planejado2" data-sortable="false">Quantidade</th>
                            <th data-column-id="vlnf" data-formatter="vlnf" data-sortable="false" data-type="numeric">vlnf</th>
<!--                            <th data-column-id="adicional" data-formatter="adicional" data-sortable="false" data-type="numeric">Adicional</th>-->
<!--                            <th data-column-id="qtrecebido" data-formatter="recebido2" data-sortable="false" data-type="numeric">Recebido Físico</th>-->
<!--                            <th data-column-id="saldo1" data-formatter="saldo1" data-sortable="false" data-type="numeric">Saldo a Receber</th>-->
                            <!--<th data-column-id="saldo2" data-formatter="saldo2" data-sortable="false" data-type="numeric">Saldo</th>-->
<!--                            <th data-column-id="qtcautela_encarregado" data-formatter="cautela2" data-sortable="false" data-type="numeric">Cautela</th>-->
<!--                            <th data-column-id="saldo_cautelar" data-formatter="saldo_cautelar" data-sortable="false" data-type="numeric">Saldo a cautelar</th>-->
<!--                            <th data-column-id="qtdevolucao_encarregado" data-formatter="devolucao2" data-sortable="false" data-type="numeric">Devolu&ccedil;&atilde;o</th>-->

<!--                            <th data-column-id="saldo_devolucao" data-formatter="saldo_devolucao" data-sortable="false" data-type="numeric">Saldo Devolu&ccedil;&atilde;o</th>-->
<!--                            <th data-column-id="qtinventario_campo" data-formatter="fiscal2" data-sortable="false" data-type="numeric">Fiscal</th>-->
                            <th data-column-id="saldo3" data-formatter="saldo3" data-sortable="false" data-type="numeric">Saldo Geral</th>
                            <!--<th data-column-id="qtzmdv" data-formatter="zmdv2" data-sortable="true" data-type="numeric">ZMDV</th>-->
<!--                            <th data-column-id="saldo_zmdv" data-formatter="saldo_zmdv" data-sortable="false" data-type="numeric">Saldo ZMDV</th>-->
                            <th data-column-id="opcoes" data-header-css-class="options" data-formatter="opcoes" data-sortable="false" data-sortable="true"></th>
                        </tr>
                        </thead>
                        <tbody >
                        </tbody>
                    </table>
                    <div style="clear:both"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var grid = $('#tblMovimento').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: [10,25, 50, 100],
            ajax: true,
            url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'getDetalhes') ?>',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            converters:{
                currency: {
                    from: function (value) { return value; },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return 'R$ ' + value.replace('.', ',');
                    }
                },
                dateTime: {
                    from: function (value) {
                        if(value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L LT");
                    }
                }
            },
            formatters:{
                artigo: function(column, row) {
                    return '<a class="artigo" href="#" data-type="select2" data-pk="' + row.id + '" data-value="' + row.atrid + '">' + row.artigo +'</a>';
                },
                planejado2: function(column, row) {
                    return '<ul class="list-unstyled">'+
                           '<li><a id="1-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="Planejado" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group \'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,1)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,1)\'>Listar</a></div> </div></div>" ><p style="color: blue"> ' + $.number(row.quantidade,2) +'<span class="caret"></span></p></a></li>'+
                           '</ul>';
                },
                recebido2: function(column, row) {
                    return '<ul class="list-unstyled">'+
                            '<li><a id="2-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="Recebido" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group\'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,2)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,2)\'>Listar</a></div> </div></div>"><p style="color: green"> ' + $.number(row.qtrecebido,2) +'<span class="caret"></span></p></a></li>'+
                            '</ul>';
                },
                saldo1: function(column, row) {
                    var value = row.quantidade - row.qtrecebido;
                    return '<div class="saldo1 text-warning" id="saldo1-'+row.id+'">' + $.number(value, 2) + '</div>';
                },
                separado2: function(column, row) {
                    return '<ul class="list-unstyled">'+
                            '<li><a id="3-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="Separado" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group \'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,3)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,3)\'>Listar</a></div> </div></div>" >  ' + $.number(row.qtseparado,2) +'<span class="caret"></span></a></li>'+
                            '</ul>';
                },
                saldo2: function(column, row) {
                    var value = row.qtrecebido - row.qtseparado;
                    var css = 'text-danger';

                    if(value >= 0)
                        css = 'text-info';

                    return '<div class="saldo2 ' + css + '">' + $.number(value, 2) + '</div>';
                },
                vlnf: function(column, row) {
                    return '<ul class="list-unstyled">' +
                        '<li><a id="9-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="vlnf" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group \'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,9)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,9)\'>Listar</a></div> </div></div>" ><p style="color: blue"> ' + $.number(row.vlnf,2) +'<span class="caret"></span></p></a></li>'+
                        '</ul>';
                },
                adicional: function(column, row) {
                    return '<ul class="list-unstyled">' +
                           '<li><a id="8-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="Adicional" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group \'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,8)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,8)\'>Listar</a></div> </div></div>" ><p style="color: blue"> ' + $.number(row.adicional,2) +'<span class="caret"></span></p></a></li>'+
                           '</ul>';
                },
                cautela2: function(column, row) {
                    return '<ul class="list-unstyled">'+
                           '<li><a id="4-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="Cautela" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group\'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,4)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,4)\'>Listar</a></div> </div></div>" >  ' + $.number(row.qtcautela_encarregado,2) +'<span class="caret"></span></a></li>'+
                           '</ul>';
                },
                devolucao2: function(column, row) {
                    return '<ul class="list-unstyled">'+
                           '<li><a id="5-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="Devolucao" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group \'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,5)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,5)\'>Listar</a></div> </div></div>" >  ' + $.number(row.qtdevolucao_encarregado,2) +'<span class="caret"></span></a></li>'+
                           '</ul>';
                },
                saldo_devolucao:function(column,row){
                    var value = parseFloat(row.qtcautela_encarregado) - parseFloat(row.qtdevolucao_encarregado);
                    var css = 'text-danger';

                    if(value >= 0)
                        css = 'text-info';

                    return '<div class="saldo_devolucao ' + css + '" id="saldo_devolucao-'+row.id+'">' + $.number(value, 2) + '</div>';
                },
                fiscal2: function(column, row) {
                    return '<ul class="list-unstyled">'+
                           '<li><a id="6-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="Fiscal" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group \'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,6)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,6)\'>Listar</a></div> </div></div>" >  ' + $.number(row.qtinventario_campo,2) +'<span class="caret"></span></a></li>'+
                           '</ul>';
                },
                saldo3: function(column, row) {
                    var value = parseFloat(row.quantidade) * parseFloat(row.vlnf);
                    var css = 'text-danger';

                    if(value >= 0)
                        css = 'text-info';

                    return '<div class="saldo3 ' + css + '" id="saldo3-'+row.id+'">' + $.number(value, 2) + '</div>';
                },
                saldo_cautelar: function(column, row) {
                    var res = parseFloat(row.qtcautela_encarregado) - parseFloat(row.qtdevolucao_encarregado);
                    var value = (parseFloat(row.quantidade)+ parseFloat(row.adicional)) - parseFloat(res);
                    var css = 'text-danger';

                    if(value >= 0)
                        css = 'text-info';

                    return '<div class="saldo_cautelar ' + css + '" id="saldo_cautelar-'+row.id+'" style="font-weight: bold">' + $.number(value, 2) + '</div>';
                },
                saldo_zmdv: function(column, row) {
                    var value = parseFloat(row.quantidade) + parseFloat(row.adicional) - parseFloat(row.qtinventario_campo);
                    var css = 'text-danger';

                    if(value >= 0)
                        css = 'text-info';

                    return '<div class="saldo_zmdv ' + css + '" id="saldo_zmdv-'+row.id+'">' + $.number(value, 2) + '</div>';
                },
                zmdv2: function(column, row) {
                    return '<ul class="list-unstyled">'+
                           '<li><a id="7-'+row.id+'" data-toggle="popover" data-container="body" data-placement="bottom" title="ZMDV" type="button" data-html="true" data-content="<div id=\'ul-'+row.id+'\'><div class=\'form-group\'><div class=\'list-group\'><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-warning\' onclick=\'get_results('+row.id+',1,7)\'>Adicionar</a><a href=\'Javascript:void(0)\' class=\'list-group-item list-group-item-success\' onclick=\'get_results('+row.id+',2,7)\'>Listar</a></div> </div></div>" ><p style="color: red">  ' + row.qtzmdv +'<span class="caret"></span></p></a></li>'+
                           '</ul>';
                },
                opcoes: function(column, row){
                    return '<button type="button" class="btn btn-danger btn-sm command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                }
            },
            requestHandler: function (request) {
                request.mov = '<?php echo $mov ?>';
                request.artigo = '<?= $this->getParam('artigo') ?>';
                request.colab = '<?= $this->getParam('colab') ?>';
                request.datacautela = '<?= $this->getParam('datacautela') ?>';

                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                return request;
            },
            templates: {
                search: "<div class='col-md-2 text-left' style='padding-left:0px; margin-left:-10px'><h3><strong>MOVIMENTO: </strong>  <?php echo $mov; ?> <strong> - O.S: </strong>  <?php echo $this->getParam('numdoc'); ?></h3></div>" +
                        //"<div class='col-md-2 text-left' style='padding-left:0px; '>" +
                        //"<button class='btn btn-primary' style='height: 34px' >" +
                        //"<a href='<?php //echo $this->Html->getUrl('Estq_movdetalhes', 'impressao', array( 'mov' => $mov,"tipo"=>4)) ?>//' style='color: #FFFFFF' role='button' target='_blank'>" +
                        //"<span class='fa fa-print'> Imprimir Cautelas</span></a></button></div>" +
                        //"<div class='col-md-2 text-left' style='padding-left:0px; '>" +
                        //"<button class='btn btn-primary' style='height: 34px' >" +
                        //"<a href='<?php //echo $this->Html->getUrl('Estq_movdetalhes', 'impressao', array( 'mov' => $mov,"tipo"=>5)) ?>//' style='color: #FFFFFF' role='button' target='_blank'>" +
                        //"<span class='fa fa-print'> Imprimir Devoluções</span></a></button></div>" +
                        "<div class='col-md-2 text-left' style='padding-left:0px; '>" +
                        "<button class='btn btn-info' style='height: 34px' >" +
                        "<a href='<?php echo $this->Html->getUrl('Estq_movdetalhes', 'add', array('id' => $mov,'modal' => 1, 'first' => 1)) ?>' style='color: #FFFFFF' role='button' data-toggle='modal'>" +
                        "<span class='fa fa-plus-square'> Novo Artigo</span></a></button></div>" +
                        "<div class='form-group {{css.search}}'>" +
                        "<div class='input-group select2-bootstrap-append'>"+
                        "<select name='artigo' class='form-control' id='artigo' style='width: 100%;'></select>"+
                        "<span class='input-group-btn'>" +
                        "<button class='btn btn-default' style='width: 35px; height: 34px' type='button' data-select2-clear='artigo'>" +
                        "<i class='fa fa-times'></i>" +
                        "</button></span></div></div>"
            },
            labels:{
                all: "Todos",
                noResults: "Sem resultados",
                loading: "Carregando...",
                infos: "Mostrando {{ctx.start}} - {{ctx.end}} de {{ctx.total}}",
                refresh: "Atualizar",
                search: "Procurar"
            }
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {


            $("[data-toggle=popover]").popover();

            $("[data-toggle=popover]").on('hide.bs.popover', function (e) {
                var id = e.target.id.split('-');
                $("#ul-"+id[1]).parent().removeAttr('style')

            })

            grid.find(".command-delete").on("click", function(e){
                var conta = $(this).data("row-id");
                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function(result) {
                        if(result) {
                            var url = '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_delete') ?>';
                            $.post(url, { id: conta, json: true }, function(data){
                                var retorno = $.parseJSON(data);

                                if(retorno.result) {
                                    BootstrapDialog.success(retorno.msg);
                                    grid.bootgrid("reload");
                                }else{
                                    BootstrapDialog.warning(retorno.msg);
                                }
                            });
                        }
                    }
                });
            });

            $('#artigo').select2({
                language: 'pt-BR',
                theme: 'bootstrap',
                placeholder: {
                    id: '-1',
                    text: 'Selecione um Artigo'
                },
                minimumInputLength : 1,
                ajax: {
                    url: '<?php echo $this->Html->getUrl('Estq_artigo', 'findArtigoMov') ?>',
                    dataType: 'json',
                    cache: true,
                    data: function (params) {
                        return {
                            mov: <?php echo $mov ?>,
                            termo: params.term,
                            size: 10,
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        var more = (page * 10) < data.total; // whether or not there are more results available
                        return { results: data.dados, more: more };
                    }
                }
            }).on('change', function(e){
                var value = $(this).val();
                grid.bootgrid("search", value);
            });



            $("button[data-select2-clear]").click(function(){
                $("#" + $(this).data("select2-clear")).val('').trigger('change');
            });

            $("#tblMovimento a.artigo").editable({
                name: 'Artigo',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                title: 'Selecione um Artigo',
                select2: {
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: "100%",
                    placeholder: {
                        id: '-1',
                        text: 'Selecione um Artigo'
                    },
                    minimumInputLength : 1,
                    ajax: {
                        url: '<?php echo $this->Html->getUrl('Estq_artigo', 'findArtigo') ?>',
                        dataType: 'json',
                        cache: true,
                        data: function (params) {
                            return {
                                termo: params.term,
                                size: 10,
                                page: params.page
                            };
                        },
                        processResults: function (data, page) {
                            var more = (page * 10) < data.total; // whether or not there are more results available
                            return { results: data.dados, more: more };
                        }
                    }
                },
                display: function(value, sourceData) {
                    var element = $(this);

                    var url = '<?php echo $this->Html->getUrl('Estq_artigo', 'getArtigo') ?>';
                    $.get(url, { id: value }, function(data){
                        var dados = $.parseJSON(data);
                        element.html(dados[0].nome);
                        var parent = $($(element[0].parentElement)[0].parentElement);
                        var codigo = $(parent[0].firstChild);
                        codigo.html(dados[0].codigo);
                    });
                }
            });

            $("#tblMovimento a.planejado").editable({
                name: 'Planejado',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                display: function(value, sourceData) {
                    $(this).number(value, 2);
                }
            }).on('save', function(e, params) {
                var row = $.parseJSON(params.response);
                var parent = $($($(this)[0].parentElement)[0].parentElement);
                var saldo1 = $($(parent[0]).find('td div.saldo1')[0]);
                var saldo3 = $($(parent[0]).find('td div.saldo3')[0]);

                var value1 = row.quantidade - row.qtrecebido;
                var value2 = row.quantidade - row.qtinventario_campo;

                var css1 = 'text-danger';
                var css2 = 'text-danger';

                if(value1 >= 0)
                    css1 = 'text-info';

                if(value2 >= 0)
                    css2 = 'text-info';

                saldo1.html($.number(value1, 2));
                saldo1.removeClass('text-info').removeClass('text-danger').addClass(css1);

                saldo3.html($.number(value2, 2));
                saldo3.removeClass('text-info').removeClass('text-danger').addClass(css2);
            });

            $("#tblMovimento a.recebido").editable({
                name: 'Recebido',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                display: function(value, sourceData) {
                    $(this).number(value, 2);
                }
            }).on('save', function(e, params) {
                var row = $.parseJSON(params.response);
                var parent = $($($(this)[0].parentElement)[0].parentElement);

                var saldo1 = $($(parent[0]).find('td div.saldo1')[0]);

                var value1 = row.quantidade - row.qtrecebido;
                var value2 = row.qtrecebido - row.qtseparado;

                var css1 = 'text-danger';
                var css2 = 'text-danger';

                if(value1 >= 0)
                    css1 = 'text-info';

                if(value2 >= 0)
                    css2 = 'text-info';

                saldo1.html($.number(value1, 2));
                saldo1.removeClass('text-info').removeClass('text-danger').addClass(css1);
            });

            $("#tblMovimento a.separado").editable({
                name: 'Separado',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                display: function(value, sourceData) {
                    $(this).number(value, 2);
                }
            }).on('save', function(e, params) {
                var row = $.parseJSON(params.response);
                var parent = $($($(this)[0].parentElement)[0].parentElement);

                var value = row.qtrecebido - row.qtseparado;
                var css = 'text-danger';

                if(value >= 0)
                    css = 'text-info';
            });

            $("#tblMovimento a.cautela").editable({
                name: 'Cautela',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                display: function(value, sourceData) {
                    $(this).number(value, 2);
                }
            });

            $("#tblMovimento a.devolucao").editable({
                name: 'Devolucao',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                display: function(value, sourceData) {
                    $(this).number(value, 2);
                }
            });

            $("#tblMovimento a.fiscal").editable({
                name: 'Fiscal',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                display: function(value, sourceData) {
                    $(this).number(value, 2);
                }
            }).on('save', function(e, params) {
                var row = $.parseJSON(params.response);
                var parent = $($($(this)[0].parentElement)[0].parentElement);
                var saldo3 = $($(parent[0]).find('td div.saldo3')[0]);

                var value = row.quantidade - row.qtinventario_campo;
                var css = 'text-danger';

                if(value >= 0)
                    css = 'text-info';

                saldo3.html($.number(value, 2));
                saldo3.removeClass('text-info').removeClass('text-danger').addClass(css);
            });

            $("#tblMovimento a.zmdv").editable({
                name: 'Zmdv',
                url: '<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_updateDetalhe') ?>',
                mode: 'inline',
                display: function(value, sourceData) {
                    $(this).number(value, 2);
                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('.modal').on('hidden.bs.modal', function (e) {
            grid.bootgrid('reload');
        });
    });

    function get_results(id,tipo,oper){
        var operacoes =[];
            operacoes[1] = 'Planejado';
            operacoes[2] = 'Recebido';
            operacoes[3] = 'Separado';
            operacoes[4] = 'Cautela';
            operacoes[5] = 'Devolução';
            operacoes[6] = 'Fiscal';
            operacoes[7] = 'ZMDV';
            operacoes[8] = 'Adicional';
            operacoes[9] = 'Vlnf';

        if(tipo==1){
            //adicinar
            var html= "";
                html+='<li><div class="form-group">'+
                        '<label for="valor">'+operacoes[oper]+'</label> '+
                        '<div>'+
                            '<div class="editable-input" style="position: relative;">'+
                            '<input type="number" class="form-control input-mini" id="valor'+id+'" style="padding-right: 24px;">'+
                            '<span class="editable-clear-x"></span>'+
                        '</div></div>';
                        if(oper==2 || oper==8) {
                            html+= '<div class="form-group">'+
                                '<label for="valor">Num Documento</label> '+
                                '<div>'+
                                '<div class="editable-input" style="position: relative;">'+
                                '<input type="text" class="form-control input-mini" id="numdoc'+id+'" style="padding-right: 24px;">'+
                                '<span class="editable-clear-x"></span>'+
                                '</div></div>';
                        }
                    if(oper==4 || oper==5) {
                        html+= '<label for="valor">Colaborador</label>';
                        html+="<div style='margin-bottom: 5px'><div class=' col-md-10 input-group select2-bootstrap-append'>"+
                        "<select class='form-control oper" + id + "' name='oper'  id='oper" + id + "'></select></div></div>";
                    }

                     html+='<div><div class="editable-buttons">'+
                        '<button type="button" class="btn btn-primary btn-sm editable-submit" onclick="salvar_oper('+oper+','+id+')">'+
                        '<i class="glyphicon glyphicon-ok"></i></button>'+
                        '</div> '+
                         '</div></div></li>';

            if(oper==4 || oper==5){
                select_colaborador(id);
            }

            $("#ul-"+id).css({"min-width":"270px","padding":"10px"});
            $("#ul-"+id).html(html);
            var position= $("#ul-"+id).parent().parent().position();
            if(oper==7) {
                $("#ul-" + id).parent().parent().css({"left": position.left - 156});
                $("#ul-"+id).parent().parent().children('.arrow').remove();
            }else {
                $("#ul-" + id).parent().parent().css({"left": position.left - 96});
            }
        }
        else{
            $.ajax({
                type:'POST',
                url:root+'/Estq_movdetalhes/list_oper',
                data:'id='+id+'&tipo='+oper,
                success:function(txt){
                    $("#ul-"+id).parent().css({"min-width":"434px","padding":"10px"});
                    $(".popover").css({"max-width":"600px"});
                    $("#ul-"+id).html(txt);
                    var position= $("#ul-"+id).parent().parent().position();

                    if(oper==7) {
                        $("#ul-" + id).parent().parent().css({"left": position.left - 300});
                        $("#ul-"+id).parent().parent().children('.arrow').remove();
                    }else {
                        $("#ul-" + id).parent().parent().css({"left": position.left - 176});
                    }
                }
            })
        }
    }

    function salvar_oper(tipo,id){
        var colab='-1';
        if(tipo==4 || tipo==5){
            colab =  $('.oper'+id).val();
            if(colab==""){
                BootstrapDialog.alert('Selecione um colaborador!');
                return false;
            }
        }
        LoadGif();
        var numdoc = !empty($("#numdoc"+id).val()) ? $("#numdoc"+id).val() : "";
        $.ajax({
            type:'POST',
            url:root+'/Estq_movdetalhes/save_oper',
            data:'tipo='+tipo+'&id='+id+'&valor='+$("#valor"+id).val()+'&colab='+colab+'&numdoc='+numdoc+'&mov=<?php echo $mov; ?>',
            success:function(txt){
                CloseGif();
                $("[data-toggle=popover]").popover('hide');

                $("#"+tipo+"-"+id).text(txt);

                if(tipo==1){
                    var saldo1 = $('#saldo1-'+id);
                    var saldo3 = $('#saldo3-'+id);

                    var value1 = parseFloat($("#1-"+id).text()) - parseFloat($("#2-"+id).text());
                    var value2 = parseFloat($("#1-"+id).text()) + parseFloat($("#8-"+id).text()) - parseFloat($("#6-"+id).text());

                    var css1 = 'text-danger';
                    var css2 = 'text-danger';

                    if(value1 >= 0)
                        css1 = 'text-info';

                    if(value2 >= 0)
                        css2 = 'text-info';

                    saldo1.html($.number(value1, 2));
                    saldo1.removeClass('text-info').removeClass('text-danger').addClass(css1);

                    saldo3.html($.number(value2, 2));
                    saldo3.removeClass('text-info').removeClass('text-danger').addClass(css2);

                    var saldo_cautelar = $('#saldo_cautelar-'+id);
                    var css3 = 'text-danger';
                    var value3 = (parseFloat($("#1-"+id).text())+parseFloat($("#8-"+id).text())) - (parseFloat($("#4-"+id).text()) - parseFloat($("#5-"+id).text()));

                    if(value3 >= 0)
                        css3 = 'text-info';

                    saldo_cautelar.html($.number(value3, 2));
                    saldo_cautelar.removeClass('text-info').removeClass('text-danger').addClass(css3);

                    var saldo_zmdv= $('#saldo_zmdv-'+id);
                    var css_zmdv = 'text-danger';
        var value_zmdv = parseFloat($("#1-"+id).text()) + parseFloat($("#8-"+id).text()) -  parseFloat($("#6-"+id).text());

                    if(value_zmdv >= 0)
                        css_zmdv = 'text-info';

                    saldo_zmdv.html($.number(value_zmdv, 2));
                    saldo_zmdv.removeClass('text-info').removeClass('text-danger').addClass(css_zmdv);

                }else if(tipo == 2){

                    var saldo1 = $('#saldo1-'+id);
                    var value1 = parseFloat($("#1-"+id).text()) - parseFloat($("#2-"+id).text());
                    var value2 = parseFloat($("#2-"+id).text()) - parseFloat($("#3-"+id).text());

                    var css1 = 'text-danger';
                    var css2 = 'text-danger';

                    if(value1 >= 0)
                        css1 = 'text-info';

                    if(value2 >= 0)
                        css2 = 'text-info';

                    saldo1.html($.number(value1, 2));
                    saldo1.removeClass('text-info').removeClass('text-danger').addClass(css1);
                }else if(tipo==3){
                    var value = parseFloat($("#2-"+id).text()) - parseFloat($("#3-"+id).text());
                    var css = 'text-danger';

                    if(value >= 0)
                        css = 'text-info';


                }else if(tipo==4){
                    var saldo_cautelar = $('#saldo_cautelar-'+id);
                    var css2 = 'text-danger';

                    var value2 =  (parseFloat($("#1-"+id).text())+parseFloat($("#8-"+id).text())) - (parseFloat($("#4-"+id).text()) - parseFloat($("#5-"+id).text()));

                    if(value2 >= 0)
                        css2 = 'text-info';

                    saldo_cautelar.html($.number(value2, 2));
                    saldo_cautelar.removeClass('text-info').removeClass('text-danger').addClass(css2);
                }else if(tipo==6){
                    var saldo3 = $('#saldo3-'+id);

                    var value = parseFloat($("#1-"+id).text()) + parseFloat($("#8-"+id).text()) - parseFloat($("#6-"+id).text());
                    var css = 'text-danger';

                    if(value >= 0)
                        css = 'text-info';

                    saldo3.html($.number(value, 2));
                    saldo3.removeClass('text-info').removeClass('text-danger').addClass(css);

                    var saldo_zmdv= $('#saldo_zmdv-'+id);
                    var css_zmdv = 'text-danger';
                    var value_zmdv = parseFloat($("#1-"+id).text()) + parseFloat($("#8-"+id).text()) -  parseFloat($("#6-"+id).text());

                    if(value_zmdv >= 0)
                        css_zmdv = 'text-info';

                    saldo_zmdv.html($.number(value_zmdv, 2));
                    saldo_zmdv.removeClass('text-info').removeClass('text-danger').addClass(css_zmdv);


                    var saldo_devolucao= $('#saldo_devolucao-'+id);
                    var css_saldo_devolucao= 'text-danger';
                    var value_saldo_devolucao = parseFloat($("#4-"+id).text()) -  parseFloat($("#5-"+id).text());

                    if(value_saldo_devolucao >= 0)
                        value_saldo_devolucao = 'text-info';

                    saldo_devolucao.html($.number(value_saldo_devolucao, 2));
                    saldo_devolucao.removeClass('text-info').removeClass('text-danger').addClass(css_saldo_devolucao);


                }else if(tipo==8){
                    var saldo3 = $('#saldo3-'+id);
                    var value = parseFloat($("#1-"+id).text()) + parseFloat($("#8-"+id).text()) - parseFloat($("#6-"+id).text());

                    var css = 'text-danger';
                    if(value >= 0)
                        css = 'text-info';

                    saldo3.html($.number(value, 2));
                    saldo3.removeClass('text-info').removeClass('text-danger').addClass(css);

                    var saldo_cautelar = $('#saldo_cautelar-'+id);
                    var css2 = 'text-danger';
                    var value2 =  (parseFloat($("#1-"+id).text())+parseFloat($("#8-"+id).text())) - (parseFloat($("#4-"+id).text()) - parseFloat($("#5-"+id).text()));


                   if(value2 >= 0)
                       css2 = 'text-info';

                    saldo_cautelar.html($.number(value2, 2));
                    saldo_cautelar.removeClass('text-info').removeClass('text-danger').addClass(css2);


                    var saldo_zmdv= $('#saldo_zmdv-'+id);
                    var css_zmdv = 'text-danger';
                    var value_zmdv = parseFloat($("#1-"+id).text()) + parseFloat($("#8-"+id).text()) -  parseFloat($("#6-"+id).text());

                    if(value_zmdv >= 0)
                        css_zmdv = 'text-info';

                    saldo_zmdv.html($.number(value_zmdv, 2));
                    saldo_zmdv.removeClass('text-info').removeClass('text-danger').addClass(css_zmdv);
                }
            }
        })


    }


    function select_colaborador(id){
        $.ajax({
            type:'POST',
            url:root+'/Estq_movdetalhes/getColag',
            success:function(txt){
                $('.oper'+id).html(txt);
                $('.oper'+id).select2();
            }
        });
    }


</script>