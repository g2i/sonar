<div class="wrapper wrapper-content">

    <div class="row">

        <div class="col-lg-12">

            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>

            <div class="ibox">

                <div class="ibox-content no-borders">

                    <form method="post" role="form"

                          action="<?php echo $this->Html->getUrl('Estq_movdetalhes', 'post_add') ?>">

                        <div class="alert alert-info">Os campos marcados com <span

                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.

                        </div>

                        <div class="well well-lg">


                            <div class="form-group col-md-12 col-sm-6 col-xs-12">

                                <label for="codigo_livre" class="required">Código</label>

                                <select name="codigo_livre" class="form-control codigo codigo-ajax" id="codigo_livre">

                                    <option value="">Selecione:</option>

                                </select>

                            </div>

                            <div class="form-group col-md-12">

                                <label for="artigo" class="required">Descrição <span
                                            class="glyphicon glyphicon-asterisk"></span></label><br/>

                                <select name="artigo" id="artigo" class="form-control artigo artigo-ajax">

                                    <option value="">Selecione:</option>

                                </select>

                            </div>


                            <input type="hidden" name="lote_substoque" value="-1"/>


                            <div class="form-group col-md-12 col-sm-6 col-xs-12">

                                <label for="quantidade" class="required">Quantidade:<span

                                            class="glyphicon glyphicon-asterisk"></span></label>

                                <input type="number" step="0.0001" name="quantidade" id="quantidade"
                                       class="form-control"

                                       value="<?php echo $Estq_movdetalhes->quantidade ?>" placeholder="Quantidade"

                                       required>

                            </div>

                            <input type="hidden" name="vlnf" id="vlnf" value="0">

                            <input type="hidden" name="vlcusto" id="vlcusto" value="0">

                            <input type="hidden" name="vlcustomedio" id="vlcustomedio" value="0">

                            <input type="hidden" name="marca" id="marca" value="0">

                            <input type='hidden' name="validade" id="validade" class="form-control date"

                                   value="<?php echo $Estq_movdetalhes->validade ?>"/>

                            <input type="hidden" name="situacao" value="1"/>

                            <input type="hidden" name="movimento" value="<?php echo $this->getParam('id'); ?>"/>

                            <div class="clearfix"></div>

                        </div>

                        <input type="hidden" name="modal" value="1"/>

                        <div class="text-right">

                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">

                                Cancelar

                            </a>

                            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                                   value="salvar">

                        </div>


                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

<input type="hidden" id="const" class="const" value="0"/>


<script>


    $(document).ready(function () {

        $('.money').mask('000.000.000.000.000,00', {reverse: true});

        $('select.codigo-ajax').each(function () {

            $(this).ajaxChosen({

                dataType: "json",

                type: "POST",

                url: root + "/Estq_movdetalhes/listar/",

                allow_single_deselect: true

            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});

            $(this).change(function () {

                $(this).trigger("chosen:updated");

            })

        });

        $('select.artigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar2/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            })
        });


        moment.locale('pt-BR');


        $('.datePicker').datetimepicker({

            format: 'L',

            extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],

            showTodayButton: true,

            useStrict: true,

            locale: 'pt-BR',

            showClear: true,

            allowInputToggle: true

        });


        $('.artigo').change(function () {

            if ($(".const").val() != 1) {

                $.ajax({

                    type: 'POST',

                    url: root + '/Estq_movdetalhes/GetLoteSubestoque',

                    data: 'artigo=' + $(this).val(),

                    success: function (txt) {

                        $(".lote_substoque").html(txt);

                        $(".const").val(1);

                    }

                });

                $.ajax({

                    type: 'POST',

                    url: root + '/Estq_movdetalhes/GetCodigo',

                    data: 'artigo=' + $(this).val(),

                    success: function (txt) {

                        $(".codigo").html(txt);

                        setTimeout(function () {

                            $(".codigo").trigger("chosen:updated");

                        }, 20);

                        $(".const").val(1);

                    }

                });

            }

        });

        $('.codigo').change(function () {

            if ($(".const").val() != 1) {

                $.ajax({

                    type: 'POST',

                    url: root + '/Estq_movdetalhes/GetLoteSubestoque',

                    data: 'artigo=' + $(this).val(),

                    success: function (txt) {

                        $(".lote_substoque").html(txt);

                        $(".const").val(1);

                    }

                });

                $.ajax({

                    type: 'POST',

                    url: root + '/Estq_movdetalhes/GetArtigo',

                    data: 'artigo=' + $(this).val(),

                    success: function (txt) {

                        $(".artigo").html(txt);

                        setTimeout(function () {

                            $(".artigo").trigger("chosen:updated");

                        }, 20);

                        $(".const").val(1);


                    }

                });

            }

        });

    });


</script>



