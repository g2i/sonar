<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Detalhes </h2>
        <ol class="breadcrumb">
            <li>movimento</li>
            <li class="active">
                <strong>Impressão <?php echo $Tipos[$this->getParam('tipo')]?></strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <div class="form">
                                <form role="form" id="form"
                                      action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                      method="get">
                                    <input type="hidden" id="m" name="m" value="<?php echo CONTROLLER; ?>">
                                    <input type="hidden" name="p" id="p" value="<?php echo ACTION; ?>">

                                    <input type="hidden" name="mov" value="<?php echo $this->getParam('mov'); ?>" />
                                    <input type="hidden" name="tipo" id="tipo" value="<?php echo $this->getParam('tipo'); ?>" />
                                    <div class="col-md-4 form-group search">
                                        <label for="colaborador">Colaborador</label>
                                            <select name="colaborador" class="form-control " id="colaborador" >
                                                <?php echo '<option value="">Selecione:</option>'; ?>
                                                <?php foreach ($profissionais as $e): ?>
                                                    <?php
                                                    if ($this->getParam('colaborador') == $e->codigo) {
                                                        echo '<option selected value="' . $e->codigo . '">' . $e->nome . '</option>';
                                                    } else {
                                                        echo '<option value="' . $e->codigo . '">' . $e->nome . '</option>';
                                                    }
                                                    ?>
                                                <?php endforeach; ?>
                                            </select>

                                    </div>

                                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                        <label for="data">Data</label>

                                        <div class='input-group datePicke_bottom'>
                                            <input type='text' name="data" id="data" class="form-control data"
                                                   value="<?php echo $this->getParam('data'); ?>"/>
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                        </div>
                                    </div>


                                    <div class="col-md-4 text-right" style="margin-top: 25px;">

                                        <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('mov'=>$this->getParam('mov'))) ?>"
                                           class="btn btn-default"
                                           data-dismiss="modal" data-toggle="tooltip" data-placement="bottom"
                                           title="Recarregar a página"><span
                                                class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                                data-placement="bottom" title="Pesquisar"><span
                                                class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Cautelas
                        </h5>

                        <div class="col-md-2 pull-right" style="margin-top: -5px">
                            <button class="btn btn-white btn-sm selecionar text-right" data-toggle="tooltip"
                                    data-placement="bottom"
                                    title="Selecionar todos" id="print"><i class="fa fa-check"></i> Imprimir
                                Selecionados
                            </button>
                        </div>
                        <div class="col-md-2 pull-right" style="margin-top: -5px">
                            <button class="btn btn-white btn-sm selecionar text-right" data-toggle="tooltip"
                                    data-placement="bottom"
                                    title="Selecionar todos" id="stodos"><i class="fa fa-check"></i> Selecionar Todos
                            </button>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>
                                            <a href='Javascript:void(0)'>
                                                Artigo
                                            </a>
                                        </th>
                                        <th>
                                            <a href='Javascript:void(0)'>
                                                Colaborador
                                            </a>
                                        </th>
                                        <th>
                                            <a href='Javascript:void(0)'>
                                                Data
                                            </a>
                                        </th>
                                        <th>
                                            <a href='Javascript:void(0)'>
                                                Quantidade
                                            </a>
                                        </th>
                                    </tr>

                                    <?php
                                    foreach ($Itens as $i) {
                                        echo '<tr>';
                                        echo '<td><input type="checkbox" class="check_" value="' . $i->id . '" /></td>';
                                        echo '<td>' . $i->getDetalhesMov()->getEstq_artigo()->nome . '</td>';
                                        echo '<td>' . $i->getColaborador()->nome . '</td>';
                                        echo '<td>' . date('d/m/Y H:i',strtotime($i->data)) . '</td>';
                                        echo '<td>' . $i->qtde . '</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(function () {
            $("#colaborador").select2({
                theme: "bootstrap"
            });


        })
        $("#stodos").click(function () {
            $('.check_').each(
                function () {
                    if ($(this).is(':checked')) {
                        $(this).prop('checked', false);
                    } else {
                        $(this).prop('checked', true);
                    }
                }
            );
            if ($(this).hasClass('selecionar')) {
                $(this).removeClass('selecionar');
                $(this).html('<i class="fa fa-exclamation"></i> Desmarcar Todos');
            } else {
                $(this).addClass('selecionar');
                $(this).html('<i class="fa fa-check"></i> Selecionar Todos');
            }
        });

        $("#print").click(function () {
            var ids = new Array();
            $('.check_').each(
                function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                }
            );
            if (ids.length > 0) {
                window.open(root + "/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=print_c&ids=" + ids+"&tipo=<?=$this->getParam('tipo')?>", "_blank");
            } else {
                BootstrapDialog.alert('Selecione algum item!');
                return false;
            }
        })


    </script>