<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Andamento</h5>
                </div>
                <div class="ibox-content" style="min-height: 347px;">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <?php
                            foreach ($andamentos as $a) {
                                echo '<tr>';
                                echo '<td>' . $a->label . '</td>';
                                echo '<td>' . $a->value . '</td>';
                                echo '<td>R$ ' . number_format($a->total, 2, ',', '.') . '</td>';
                                echo '<td>';
                                echo '<a href="Javascript:void(0)" onclick="pesquisar_andamento('.$a->id_andamento.')" data-toggle="tooltip" data-placement="auto" title="Pesquisar" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-search"></span></a>';
                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="col-lg-3" style="padding: 0px">
             <div class="ibox float-e-margins">
                 <div class="ibox-title">
                     <h5>Gráfico de Andamentos</h5>
                 </div>
                 <div class="ibox-content" style="padding: 0px">
                     <div id="chart-andamento"></div>
                 </div>
             </div>
         </div>-->
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Situações</h5>
                </div>
                <div class="ibox-content" style="min-height: 347px;">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <?php
                            foreach ($situacoes as $a) {
                                echo '<tr>';
                                echo '<td>' . $a->label . '</td>';
                                echo '<td>' . $a->value . '</td>';
                                echo '<td>R$ ' . number_format($a->total, 2, ',', '.') . '</td>';
                                echo '<td>';
                                echo '<a href="Javascript:void(0)" onclick="pesquisar_situacao('.$a->id_situacao.')" data-toggle="tooltip" data-placement="auto" title="Pesquisar" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-search"></span></a>';
                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <!--<div class="col-lg-3" style="padding: 0px">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Gráfico de Situações</h5>
                </div>
                <div class="ibox-content" style="padding: 0px">
                    <div id="chart-situacao"></div>
                </div>
            </div>
        </div>-->
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form role="form" id="form"
                          action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                          method="get">
                        <input type="hidden" name="m" id="m" value="<?php echo CONTROLLER; ?>">
                        <input type="hidden" name="p" id="p" value="<?php echo ACTION; ?>">
                        <input type="hidden" name="page" id="page">

                        <div class="col-md-6 col-sm-12 col-xs-12" style="padding-left: 0">

                            <div class="col-md-12 form-group">
                                <label for="andamento">Andamento</label>
                                <select name="andamento" class="form-control" id="andamento">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Andamento as $e) {
                                        if ($e->id == $and)
                                            echo '<option value="' . $e->id . '" selected>' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    } ?>
                                </select>
                            </div>

                            <div class="col-md-12 form-group">
                                <label for="situacao">Situação</label>
                                <select name="situacao" class="form-control" id="situacao">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Situacao as $e) {
                                        if ($e->id == $sit)
                                            echo '<option value="' . $e->id . '" selected>' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    } ?>
                                </select>
                            </div>

                            <div class="col-md-12 form-group">
                                <label for="servicos">Serviços</label>
                                <select name="servicos" class="form-control" id="servicos">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Servicos as $e) {
                                        if ($e == $serv)
                                            echo '<option value="' . $e->id . '" selected>' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    } ?>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="col-md-12 form-group">
                                <label for="tipo_filtro">Origem Datas</label>
                                <select name="tipo_filtro" class="form-control" id="tipo_filtro">
                                    <option value="">Selecione:</option>
                                    <?php foreach ($filtros as $e => $f) {
                                        echo '<option value="' . $e . '">' . $f . '</option>';
                                    } ?>
                                </select>
                            </div>
                            <div class="col-md-12 form-group">
                                <label for="inicio">Início</label>

                                <div class='input-group datePicke_bottom'>
                                    <input name="inicio" class="form-control" id="inicio" placeholder="Ínício"/>
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>

                            </div>

                            <div class="col-md-12 form-group">
                                <label for="fim">Fim</label>
                                <div class='input-group datePicke_bottom'>
                                    <input name="fim" class="form-control" id="fim" placeholder="Fim"/>
                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>

                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="radio-inline">
                                <input type="radio" name="book" id="book" value="1"> Com Book Fotográfico
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="book" id="book" value="2"> Sem Book Fotográfico
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="book" id="book" value="3" checked> Ambos
                            </label>

                        </div>
                        <div class="col-md-12 text-right">
                            <button type="button" id="print" class="btn btn-default" data-toggle="tooltip"
                                    data-placement="bottom" title="Imprimir"><span
                                    class="glyphicon glyphicon-print"></span></button>

                            <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                               class="btn btn-default" data-dismiss="modal" data-toggle="tooltip"
                               data-placement="bottom" title="Recarregar a página"><span
                                    class="glyphicon glyphicon-refresh "></span></a>
                            <button type="button" id="pesquisar" class="btn btn-default" data-toggle="tooltip"
                                    data-placement="bottom" title="Pesquisar"><span
                                    class="glyphicon glyphicon-search"></span></button>

                        </div>
                    </form>
                    <hr/>
                    <div class="table_results">
                        <div class="row">
                            <div class="col-md-2 pull-left">
                                <h3>Total Filtrado: <?= $total_filtrado ?></h3>
                            </div>
                            <div class="col-md-6 pull-left">
                                <h3>Total O.S: R$ <?= number_format($total_ads, 2, ',', '.') ?> <br /> Total Rec: R$ <?= number_format($total_valor, 2, ',', '.') ?></h3>
                            </div>
                        </div>
                        <hr/>
                        <div class="table-responsive " style="overflow-x: auto; font-size: 12px">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Diagrama</th>
                                    <th>Serviço</th>
                                    <th>Andamento</th>
                                    <th>Situação</th>
                                    <th  <?= $tipo_filtro==2 ? 'class="success"' : ''?>>Ent. O.S</th>
                                    <th <?= $tipo_filtro==1 ? 'class="success"' : ''?>>Venc. O.S</th>
                                    <th>Dias a vencer</th>
                                    <th <?= $tipo_filtro==3 ? 'class="success"' : ''?>>Dt Carta</th>
                                    <th  <?= $tipo_filtro==4 ? 'class="success"' : ''?>>Dt Concl.</th>
                                    <th <?= $tipo_filtro==5 ? 'class="success"' : ''?>>Dt Receb.</th>
                                    <th  <?= $tipo_filtro==6 ? 'class="success"' : ''?>>Dt Deslig.</th>
                                    <th  <?= $tipo_filtro==7 ? 'class="success"' : ''?>>Book Fot.</th>
                                    <th>Valor O.S</th>
                                    <th>Valor Rec.</th>
                                    <th>&nbsp;</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($movimento as $e) {
                                    echo '<tr>';
                                    echo '<td>' . $e->numdoc . '</td>';
                                    echo '<td>' . $e->getTipo_servico()->nome . '</td>';
                                    echo '<td>' . $e->getAndamento()->nome . '</td>';
                                    echo '<td>' . $e->getSituacaoMov()->nome . '</td>';
                                    echo $tipo_filtro==2 ? "<td  class='success'>": "<td>" ;
                                    echo ConvertData($e->datadoc) . '</td>';
                                    echo $tipo_filtro==1 ? "<td  class='success'>": "<td>" ;
                                        echo ConvertDTime($e->data) . '</td>';
                                    echo '<td>' . $e->dias . '</td>';
                                    echo $tipo_filtro==3 ? "<td  class='success'>": "<td>" ;
                                    echo ConvertData($e->data_carta) . '</td>';
                                    echo $tipo_filtro==4 ? "<td  class='success'>": "<td>" ;
                                    echo ConvertData($e->data_medicao) . '</td>';
                                    echo $tipo_filtro==5 ? "<td  class='success'>": "<td>" ;
                                    echo  ConvertData($e->data_recebimento) . '</td>';
                                    echo $tipo_filtro==6 ? "<td  class='success'>": "<td>" ;
                                    echo ConvertData($e->desl_inicial) . '</td>';
                                    echo $tipo_filtro==7 ? "<td  class='success'>": "<td>" ;
                                    echo ConvertData($e->book_fotografico) . '</td>';

                                    echo '<td>';
                                        echo number_format($e->valor, 2, ',', '.');
                                    echo '</td>';

                                    echo '<td>';
                                        echo number_format($e->valor_recebimento + $e->valor_recebimento2 + $e->valor_recebimento3 + $e->valor_recebimento4, 2, ',', '.');
                                    echo '</td>';

                                    echo '<td width="30" >';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="auto" title="Editar Movimento"></span> ', 'Estq_mov', 'edit',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-warning btn-xs'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                                </tbody>
                            </table>
                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#pesquisar').click(function () {
            var inicio = $("#inicio").val();
            var fim = $("#fim").val();

            if (inicio != "" || fim != "") {
                if ($("#tipo_filtro").val() == "") {
                    BootstrapDialog.alert('Selecione um tipo de filtro!');
                    $("#tipo_filtro").focus();
                    return false;
                }
            }

            var dados = $("#form").serialize();
            var r = true;
            if (r) {
                r = false;
                $("div.table_results").load(
                    <?php
                    echo '"' . $this->Html->getUrl('Index', 'index') . '?"+dados+ " .table_results"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });
    });

    $(function () {
        /*  $.ajax({
         type:'POST',
         url:root+'/Index/Andamento',
         success:function(txt) {
         Morris.Donut({
         element: 'chart-andamento',
         data: jQuery.parseJSON(txt),
         resize: true,
         colors: ['#87d6c6', '#54cdb4', '#1ab394', '#9de8d8', '#b7eee2']
         });
         }
         });*/


    });
    $(function () {
        /*  $.ajax({
         type:'POST',
         url:root+'/Index/Situacao',
         success:function(txt) {
         Morris.Donut({
         element: 'chart-situacao',
         data: jQuery.parseJSON(txt),
         resize: true,
         colors: ['#87d6c6', '#54cdb4', '#1ab394', '#9de8d8', '#b7eee2']
         });
         }
         });*/


        $("#print").click(function () {
            var data = {
                servicos: $("#servicos").val(),
                andamento: $("#andamento").val(),
                situacao: $("#situacao").val(),
                tipo_filtro: "'" + $("#tipo_filtro").val() + "'",
                inicio: $("#inicio").val(),
                fim: $("#fim").val()
            };

            if (data.servicos == "") {
                data.servicos = -1;
            }
            if (data.andamento == "") {
                data.andamento = -1;
            }
            if (data.situacao == "") {
                data.situacao = -1;
            }
            if (data.tipo_filtro == "") {
                data.tipo_filtro = -1;
            }

            if (data.inicio == "") {
                data.inicio = -1;
            } else {
                data.inicio = "'" + moment(data.inicio, 'DD/MM/YYYY').format('YYYY-MM-DD') + "'"
            }
            if (data.fim == "") {
                data.fim = -1;
            } else {
                data.fim = "'" + moment(data.fim, 'DD/MM/YYYY').format('YYYY-MM-DD') + "'"
            }

            $.Report(14, data);
        });
    });

    function pesquisar_andamento(id){
        $("#andamento").val(id);
        $("#pesquisar").click();
    }

    function pesquisar_situacao(id){
        $("#situacao").val(id);
        $("#pesquisar").click();
    }
</script>