
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Concessionaria_motivo_retirada</h2>
    <ol class="breadcrumb">
    <li>Concessionaria_motivo_retirada</li>
    <li class="active">
    <strong>Editar Concessionaria_motivo_retirada</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Concessionaria_motivo_retirada', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Concessionaria_motivo_retirada->nome ?>" placeholder="Nome">
        </div>
        <div class="form-group">
            <label for="dt_cadastro">Dt_cadastro</label>
            <input type="date" name="dt_cadastro" id="dt_cadastro" class="form-control" value="<?php echo $Concessionaria_motivo_retirada->dt_cadastro ?>" placeholder="Dt_cadastro">
        </div>
        <div class="form-group">
            <label for="dt_modificado">Dt_modificado</label>
            <input type="date" name="dt_modificado" id="dt_modificado" class="form-control" value="<?php echo $Concessionaria_motivo_retirada->dt_modificado ?>" placeholder="Dt_modificado">
        </div>
        <div class="form-group">
            <label for="cadastrado_por">Usuario</label>
            <select name="cadastrado_por" class="form-control" id="cadastrado_por">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Concessionaria_motivo_retirada->cadastrado_por)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="modificado_por">Usuario</label>
            <select name="modificado_por" class="form-control" id="modificado_por">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Concessionaria_motivo_retirada->modificado_por)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Concessionaria_motivo_retirada->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Concessionaria_motivo_retirada->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Concessionaria_motivo_retirada', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>