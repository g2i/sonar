<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo de Serviço</h2>
        <ol class="breadcrumb">
            <li>Tipo de Serviço</li>
            <li class="active">
                <strong>Editar Tipo de Serviço</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Concessionaria_tipo_servico', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div >
                            <div class="form-group">
                                <label class="required" for="nome">Nome <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Concessionaria_tipo_servico->nome ?>" placeholder="Nome"
                                       required>
                            </div>

                            <input type="hidden" name="origem" value="<?php echo Config::get('origem'); ?>"  >

                            <div class="form-group">
                                <label for="situacao">Situação</label>
                                <select name="situacao" class="form-control" id="situacao">
                                    <?php
                                    foreach ($Situacaos as $s) {
                                        if ($s->id == $Concessionaria_tipo_servico->situacao)
                                            echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                                        else
                                            echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="id" value="<?php echo $Concessionaria_tipo_servico->id; ?>">

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Concessionaria_tipo_servico', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>