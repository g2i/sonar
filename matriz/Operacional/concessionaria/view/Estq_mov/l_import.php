<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimento</h2>
        <ol class="breadcrumb">
            <li>Detalhes</li>
            <li class="active">
                <strong>Importação</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover" id="import">
                            <tr>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                        Código
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                        Artigo
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                        Quantidade
                                    </a>
                                </th>
                                <th>&nbsp;</th>
                            </tr>

                            <?php
                            foreach ($itens as $i) {
                                $a = new Criteria();
                                $a->addCondition('codigo_livre','=',$i->codigo);
                                $a->addCondition('situacao','=',1);
                                $a->addCondition('origem','=',Config::get('origem'));
                                $artigo = Estq_artigo::getFirst($a);
                                echo '<tr '.(empty($artigo))? "class='danger'": "class='success'".'>';
                                echo '<td>';
                                echo '<a class="codigo" href="#" data-type="number" data-pk='.$i->id.' data-value='.$i->codigo.'>'.$i->codigo.'</a>';
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink($artigo->nome, 'Estq_artigo', 'view',
                                    array('id' => $artigo->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo '<a class="quantidade" href="#" data-type="number" data-pk='.$i->id.' data-value='.$i->qtde.'>'.$i->qtde.'</a>';
                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#import a.codigo").editable({
        name: 'Codigo',
        url: '<?php echo $this->Html->getUrl('Estq_mov', 'update_codigo') ?>',
        mode: 'inline',
        display: function(value, sourceData) {
            $(this).number(value, 4);
        }
    }).on('save', function(e, params) {
        var row = $.parseJSON(params.response);
        var parent = $($($(this)[0].parentElement)[0].parentElement);
        var saldo1 = $($(parent[0]).find('td div.saldo1')[0]);
        var saldo3 = $($(parent[0]).find('td div.saldo3')[0]);

        var value1 = row.quantidade - row.qtrecebido;
        var value2 = row.quantidade - row.qtinventario_campo;
        var css1 = 'text-danger';
        var css2 = 'text-danger';

        if(value1 >= 0)
            css1 = 'text-info';

        if(value2 >= 0)
            css2 = 'text-info';

        saldo1.html($.number(value1, 4));
        saldo1.removeClass('text-info').removeClass('text-danger').addClass(css1);

        saldo3.html($.number(value2, 4));
        saldo3.removeClass('text-info').removeClass('text-danger').addClass(css2);
    });
</script>