<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <div class="ibox">
                    <div class="ibox-content">
                        <span aria-hidden="true">&times;</span>
            </button>
            <form class="form" method="post" action="<?php echo $this->Html->getUrl('Estq_mov', 'delete_foto') ?>">
                <h1>Confirmação</h1>
                <div class="well well-lg">
                    <p>Voce tem certeza que deseja excluir o registro</p>
                </div>
                <div class="text-right">
                    <input type="hidden" name="id" value="<?php echo $imagem->id; ?>">
                    <input type="hidden" name="estq_mov_id" value="<?php echo $imagem->estq_mov_id; ?>">
                    <a href="<?php echo $this->Html->getUrl('Estq_mov', 'fotos', array('id' => $Estq_mov->id)) ?>"
                               class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                    <input type="submit" class="btn btn-danger" value="Excluir">
                </div>
            </form>
        </div>
    </div>
</div>