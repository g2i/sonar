<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimento</h2>
        <ol class="breadcrumb">
            <li>Detalhes</li>
            <li class="active">
                <strong>Importação Recebido</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Estq_mov', 'importacao_r'); ?>">
                        <div class="alert alert-info">Todos os campos abaixo devem ser preenchidos!
                        </div>
                        <div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="artigos">Artigos<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <textarea name="artigos" id="artigos" class="form-control"
                                      placeholder="Artigos" required></textarea>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="quantidade">Quantidade<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <textarea name="quantidade" id="quantidade" class="form-control"
                                      placeholder="Quantidade" required></textarea>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="diagrama">Diagramas<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <textarea name="diagrama" id="diagrama" class="form-control"
                                      placeholder="Diagramas" required></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="Importar">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>