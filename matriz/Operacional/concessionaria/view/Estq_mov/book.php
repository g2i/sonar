<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12 text-center">
        <h2><strong>Acervo de Fotos</strong></h2>
    </div>
    <div class="col-lg-12 text-right">
        <p><strong>DR: </strong><?= $movimentacao->numdoc ?></p>
        <p><strong>PRESTADOR: </strong>Sonar</p>
        <p><strong>Emitido em: </strong><?= DataBR($movimentacao->book_fotografico) ?></p>
    </div>
</div>
<div class="white-bg" style="padding: 20px">
    <div>
        <?php foreach ($concessionaria_imagens as $ci): ?>
            <table border="1" style="width: 1000px; margin: auto">
                <tr>
                    <th class="text-center">Estrutura</th>
                    <th class="text-center">Imagem</th>
                    <th class="text-center">Obs.</th>
                </tr>
                <?php for ($i = 0; $i < count($ci) - 1; $i++): ?>
                    <tr class="text-center">
                        <?php if ($i == 0): ?>
                            <td style="width:200px;" rowspan="<?= $i ?>"><?= $ci['estrutura'] ?></td>
                        <?php endif; ?>
                        <td style=" width:400px; padding: 5px 0px">
                            <img style="width:400px;" src="<?php echo SITE_PATH . '/' . $ci[$i]->caminho_imagem; ?>"
                                 alt="Fotos Book">
                        </td>
                        <td style="width:200px;"><?= $ci[$i]->observacao ?></td>
                    </tr>
                <?php endfor; ?>
            </table>
            <div style="margin-bottom: 20px"></div>
        <?php endforeach; ?>
    </div>
</div>
