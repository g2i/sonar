<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimento</h2>
        <ol class="breadcrumb">
            <li>Detalhes</li>
            <li class="active">
                <strong>Importação</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Itens da Importação</h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover" id="import">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                            Código
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                            Artigo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                            Quantidade
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                            Diagrama
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'id')); ?>'>
                                            Movimento
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                </tr>

                                <?php
                                foreach ($itens as $i) {
                                    $a = new Criteria();
                                    $a->addCondition('codigo_livre', '=', $i->codigo);
                                    $a->addCondition('situacao', '=', 1);
                                    $a->addCondition('origem', '=', Config::get('origem'));
                                    $artigo = Estq_artigo::getFirst($a);
                                    echo (empty($artigo)) ? "<tr class='danger'>" : "<tr class='success'>";
                                    echo '<td>';
                                    echo '<a class="codigo" href="#" data-type="text" data-pk=' . $i->id . ' data-value=' . $i->codigo . '>' . $i->codigo . '</a>';
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($artigo->nome, 'Estq_artigo', 'view',
                                        array('id' => $artigo->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal', 'class' => 'artigo')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo '<a class="quantidade" href="#" data-type="text" data-pk=' . $i->id . ' data-value=' . $i->qtde . '>' . $i->qtde . '</a>';
                                    echo '</td>';


                                    echo '<td>';
                                    echo '<a class="diagrama" href="#" data-type="text" data-pk=' . $i->id . ' data-value=' . $i->diagrama. '>' . $i->diagrama. '</a>';
                                    echo '</td>';
                                    $a = new Criteria();
                                    $a->addCondition('numdoc','=',$i->diagrama);
                                    $a->addCondition('situacao','=',1);
                                    $a->addCondition('origem','=',Config::get('origem'));
                                    $movimento = Estq_mov::getFirst($a);
                                    echo '<td>';
                                    echo $this->Html->getLink($movimento->id, 'Estq_mov', 'view',
                                        array('id' => $movimento->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal', 'class' => 'movimento')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_mov', 'del_imp',
                                        array('id' => $i->id,'mov'=>$this->getParam('mov')),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Deletar"));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>

                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Estq_mov', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <a href="Javascript:void(0)" id="finalizar" class="btn btn-primary" >Finalizar Importação</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#import a.codigo").editable({
            name: 'Codigo',
            url: '<?php echo $this->Html->getUrl('Estq_mov', 'update_codigo') ?>',
            mode: 'inline'
        }).on('save', function (e, params) {
            var row = $.parseJSON(params.response);
            var parent = $($($(this)[0].parentElement)[0].parentElement);
            var artigo = $($(parent[0]).find('td a.artigo')[0]);

            if (!empty(row.nome)) {
                artigo.html(row.nome);
                parent.removeClass('danger').addClass('success');
            } else {
                artigo.html('');
                parent.removeClass('success').addClass('danger');
            }

        });

        $("#import a.diagrama").editable({
            name: 'Diagrama',
            url: '<?php echo $this->Html->getUrl('Estq_mov', 'update_diagrama') ?>',
            mode: 'inline'
        }).on('save', function (e, params) {
            var row = $.parseJSON(params.response);
            var parent = $($($(this)[0].parentElement)[0].parentElement);
            var movimento = $($(parent[0]).find('td a.movimento')[0]);

            if (!empty(row.id)) {
                movimento.html(row.id);
                parent.removeClass('danger').addClass('success');
            } else {
                movimento.html('');
                movimento.removeClass('success').addClass('danger');
            }

        });
    });

    $(document).ready(function () {
        $("#import a.quantidade").editable({
            name: 'Quantidade',
            url: '<?php echo $this->Html->getUrl('Estq_mov', 'update_qtde') ?>',
            mode: 'inline'
        });
    });

    $("#finalizar").click(function () {
        var aux=1;
        $("#import tr").each(function () {
            if($(this).hasClass('danger')){
                console.log(1)
                aux=2;
            }
        });

        if(aux==2){
            BootstrapDialog.confirm({
                title: 'Aviso',
                message: 'Existem registros com artigos não localizados! Deseja continuar?',
                type: BootstrapDialog.TYPE_WARNING,
                closable: false,
                draggable: false,
                btnCancelLabel: 'Não',
                btnOKLabel: 'Sim',
                btnOKClass: 'btn-warning',
                callback: function (result) {
                    if(result) {
                        location.href = root + '/Estq_mov/f_recebimento/'
                    }
                }
            })
        }else{
            location.href = root + '/Estq_mov/f_recebimento/'

        }
    })
</script>