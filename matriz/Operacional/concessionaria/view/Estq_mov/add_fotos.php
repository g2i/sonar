<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>O.S - Acervo de Fotos</h2>
        <ol class="breadcrumb">
            <li> Foto</li>
            <li class="active">
                <strong>Adicionar Fotos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" id="form-anexos" role="form"
                          action="<?php echo $this->Html->getUrl('Estq_mov', 'post_add_fotos') ?>"
                          enctype="multipart/form-data">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <?php if ($this->getParam('modal')): ?>
                                <input type="hidden" id="modal" name="modal"
                                       value="1">
                                <input type="hidden" id="movimentacao_id" name="movimentacao_id"
                                       value="<?= $this->getParam('id') ?>">
                            <?php else: ?>
                                <input type="hidden" id="movimentacao_id" name="movimentacao_id"
                                       value="<?= $idMovimentacao ?>">
                            <?php endif; ?>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Titulo</label>
                                                <input class="form-control" id="estrutura" required type="text"
                                                       name="estrutura">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Estrutura</label>
                                                <input class="form-control" id="ordem" required type="number"
                                                       name="ordem">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Tamanho da Imagem</label>
                                                <select name="tamanho" class="form-control " id="tipo_imagem">
                                                    <option selected value="P">Pequena</option>
                                                    <option value="M">Media</option>
                                                    <option value="G">Grande</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Tipo</label>
                                                <select name="tipo_imagem" class="form-control " id="tipo_imagem">
                                                    <option selected value="1">Acervo de Fotos</option>
                                                    <option value="2">Manutenção Preventiva/Corretiva</option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                            <label for="observacao">Observação</label>
                                            <textarea rows="9" name="observacao" id="observacao"
                                                      class="form-control"></textarea>
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="files">Foto</label>
                                                <input id="files" type="file" name="caminho[]" class="input-file "
                                                       data-overwrite-initial="false" data-max-file-count="1">
                                                <span class="help-block" style="color: red">Extensões: png, jpg, jpeg e gifs.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_mov', 'fotos', array('id' => $idMovimentacao)) ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar"
                                   onclick="senduploads('form-anexos')">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        initeComponentes(document)
    })
</script>