<style>
    .bootstrap-datetimepicker-widget {
        z-index: 99999 !important;
    }

    .dropdown-menu {
        z-index: 99999 !important;
    }
</style>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>O.S</h2>
        <ol class="breadcrumb">
            <li>O.S</li>
            <li class="active">
                <strong>Adicionar O.S</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>O.S</h5>
                </div>
                <div class="ibox-content">
                    <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>

                    <form method="post" role="form" id="form-mov">
                        <input type="hidden" id="id" name="id" value="0">
                        <input type="hidden" id="codigo_equipe" name="codigo_equipe" value="">
                        <input type="hidden" id="eletrecista01" name="eletrecista01" value="">
                        <input type="hidden" id="eletrecista02" name="eletrecista02" value="">
                        <input type="hidden" id="placa_veiculo" name="placa_veiculo" value="">
                        <input type="hidden" id="sigla" name="sigla" value="">
                        <input type="hidden" id="municipio_id" name="municipio_id" value="">
                        <input type="hidden" name="situacao" value="1"/>

                        <div>
                            <h3>Dados da Ordem</h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="ibox">
                                            <div class="ibox-title" style="border-color: #0a90eb;">
                                                <h2>Dados da Ordem</h2>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="col-md-12 col-lg-12 col-xs-12">
                                                    <div class="row">
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <label for="tipo">Tipo de O.S </label>
                                                            <select name="tipo" class="form-control tipo" id="tipo">
                                                                <option value="">Selecione :</option>
                                                                <?php
                                                                foreach ($Estq_tipomovs as $e) {
                                                                    if ($e->id == $Estq_mov->tipo)
                                                                        echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                                    else
                                                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <label for="Subgrupo">Origem</label>
                                                            <select name="grupo_supervisao_id" class="form-control"
                                                                    id="grupoSupervisao">
                                                                <option value="">Selecione:</option>
                                                                <?php
                                                                foreach ($GrupoSupervisao as $r) {
                                                                    if ($r->id == $Estq_mov->grupo_supervisao_id)
                                                                        echo '<option data-url="'.$r->url.'" selected value="' . $r->id . '">' . strtoupper($r->nome) . '</option>';
                                                                    else
                                                                        echo '<option data-url="'.$r->url.'" value="' . $r->id . '">' . strtoupper($r->nome) . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12 cliente">
                                                            <label for="cliente">Cliente</label>
                                                            <select name="cliente" class="form-control" id="cliente">
                                                                <option value="">Selecione :</option>
                                                                <?php
                                                                foreach ($Clientes as $e) {
                                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <label for="viabilizado">Viabilizado</label>

                                                            <div class='input-group datetimepicker_top'>
                                                                <input type='text' name="viabilizado" id="viabilizado"
                                                                       class="form-control viabilizado"
                                                                       value="<?php echo $Estq_mov->viabilizado ?>"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar">
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <label for="datadoc">Data entrada O.S </label>

                                                            <div class='input-group datePicke_bottom'>
                                                                <input type='text' name="datadoc" id="datadoc"
                                                                       class="form-control datadoc"
                                                                       value="<?php echo $Estq_mov->datadoc ?>"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar">
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <label for="numdoc">N° Concessionaria </label>
                                                            <input type="text" name="numdoc" id="numdoc"
                                                                   class="form-control"
                                                                   value="<?php echo $Estq_mov->numdoc ?>"
                                                                   placeholder="Número Documento">
                                                        </div>
                                                        <div class="clearfix"></div>


                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <label for="valor_os_contratada">Valor O.S
                                                                Contratada</label>

                                                            <div class="input-group m-b">
                                                                <span class="input-group-addon">R$</span>
                                                                <input type="text" name="valor_os_contratada"
                                                                       id="valor_os_contratada"
                                                                       class="form-control money"
                                                                       value="<?php echo $Estq_mov->valor_os_contratada ?>"
                                                                       placeholder="Valor O.S">
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                            <label for="data">Data vencimento O.S</label>

                                                            <div class='input-group datetimepicker_bottom '>
                                                                <input type='text' name="data" id="data"
                                                                       class="form-control data"
                                                                       value="<?php echo $Estq_mov->data; ?>"/>
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-calendar">
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                            <label for="Observação">Observação
                                                            </label>
                                                            <textarea type="text" rows="10" name="observacao"
                                                                      id="observacao"
                                                                      class="form-control"
                                                                      placeholder="observacao"><?php echo $Estq_mov->observacao ?></textarea>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="ibox ">
                                            <div class="ibox-title" style="border-color: forestgreen">
                                                <h2>Endereço</h2>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12 cliente">
                                                    <label for="cliente">Endereços do Cliente</label>
                                                    <div class="input-group">
                                                        <div class="btn-group bootstrap-select input-group-btn form-control">
                                                            <select name="cliente_endereco_id"
                                                                    class="form-control select2"
                                                                    id="cliente_endereco">
                                                                <option value="">Selecione :</option>
                                                            </select>
                                                        </div>
                                                        <span class="input-group-btn">
                                                        <a href="#" title="Editar cliente"
                                                           class="btn btn-primary"
                                                           onclick="editarEndereco($('#cliente_endereco').val())"><i
                                                                    aria-hidden="true"
                                                                    class="fa fa-home"></i></a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="cep">Cep</label>
                                                    <input type="text" rel="txtTooltip" role="cep"
                                                           cep-ajax-url="<?= $this->Html->getUrl('Cep', 'busca') ?>"
                                                           cep-init="LoadGif" cep-done="CloseGif"
                                                           name="cep" id="cep" class="form-control cep"
                                                           placeholder="Cep"
                                                           title="Informe o CEP - Preenchimento Automático"
                                                           data-toggle="tooltip" data-placement="top">
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="endereco">Endereco </label>
                                                    <input type="text" name="endereco" data-cep="endereco" id="endereco"
                                                           class="form-control"
                                                           placeholder="Endereco">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="uf">Estado</label>
                                                    <select name="uf" id="uf" data-cep="uf"
                                                            class="form-control selectPicker">
                                                        <option value="">Selecione</option>
                                                        <option value="AC">AC</option>
                                                        <option value="AL">AL</option>
                                                        <option value="AM">AM</option>
                                                        <option value="AP">AP</option>
                                                        <option value="BA">BA</option>
                                                        <option value="CE">CE</option>
                                                        <option value="DF">DF</option>
                                                        <option value="ES">ES</option>
                                                        <option value="GO">GO</option>
                                                        <option value="MA">MA</option>
                                                        <option value="MG">MG</option>
                                                        <option value="MS">MS</option>
                                                        <option value="MT">MT</option>
                                                        <option value="PA">PA</option>
                                                        <option value="PB">PB</option>
                                                        <option value="PE">PE</option>
                                                        <option value="PI">PI</option>
                                                        <option value="PR">PR</option>
                                                        <option value="RJ">RJ</option>
                                                        <option value="RN">RN</option>
                                                        <option value="RS">RS</option>
                                                        <option value="RO">RO</option>
                                                        <option value="RR">RR</option>
                                                        <option value="SC">SC</option>
                                                        <option value="SE">SE</option>
                                                        <option value="SP">SP</option>
                                                        <option value="TO">TO</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="cidade">Cidade </label>
                                                    <input type="text" name="cidade" data-cep="cidade" id="cidade"
                                                           class="form-control"
                                                           placeholder="Cidade">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="bairro">Bairro</label>
                                                    <input type="text" name="bairro" data-cep="bairro" id="bairro"
                                                           class="form-control"
                                                           placeholder="Bairro">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="numero">Numero</label>
                                                    <input type="text" name="numero" id="numero" class="form-control"
                                                           placeholder="Numero">
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="complemento">Complemento</label>
                                                    <input type="text" name="complemento" id="complemento"
                                                           class="form-control"
                                                           placeholder="Complemento">
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <h3>Dados Execução </h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="ibox">
                                            <div class="ibox-title" style="border-color: #0a90eb">
                                                <h2>Dados Execução</h2>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                                    <label class="codigo_equipe" for="codigo_equipe">Código Equipe
                                                        <span class=" "></span>
                                                    </label>
                                                    <select name="codigo_equipe_id" class="form-control"
                                                            id="codigo_equipe_id">
                                                        <option value="">Selecione:</option>
                                                        <?php
                                                        foreach ($Equipes as $eq) {
                                                            if ($eq->id == $Estq_mov->codigo_equipe_id)
                                                                echo '<option selected value="' . $eq->id . '">' . $eq->nome . '</option>';
                                                            else
                                                                echo '<option value="' . $eq->id . '">' . $eq->nome . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                                    <label class="eletrecista01" for="eletrecista01">Colaborador - Lider
                                                        <span class=""></span></label>
                                                    <select name="eletrecista01_id" class="form-control"
                                                            id="eletrecista01_id">
                                                        <option value="">Selecione:</option>
                                                        <?php
                                                        foreach ($eletricista as $r) {
                                                            if ($r->codigo_profissional == $Estq_mov->eletrecista01)
                                                                echo '<option selected value="' . $r->codigo_profissional . '">' . $r->nome . '</option>';
                                                            else
                                                                echo '<option value="' . $r->codigo_profissional . '">' . $r->nome . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                    <label for="situacao">Situação O.S </label>
                                                    <select name="situacao_mov" class="form-control" id="situacao_mov">
                                                        <option value="">Selecione:</option>
                                                        <?php
                                                        foreach ($Situacao_movimento as $e) {
                                                            if ($e->id == $Estq_mov->situacao_mov)
                                                                echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                            else
                                                                echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                    <label for="data_medicao">Data Conclusão</label>

                                                    <div class='input-group datePicke_bottom'>
                                                        <input type='text' name="data_medicao" id="data_medicao"
                                                               class="form-control data_medicao"
                                                               value="<?php echo $Estq_mov->data_medicao; ?>"/>
                                                        <span class="input-group-addon"> <span
                                                                    class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                                    <label for="data_execucao01">Data Execução</label>

                                                    <div class='input-group datePicke_bottom'>
                                                        <input type='text' name="data_execucao01" id="data_execucao01"
                                                               class="form-control data_execucao01"
                                                               value="<?php echo $Estq_mov->data_execucao01; ?>"/>
                                                        <span class="input-group-addon"> <span
                                                                    class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                    <label for="Observação">Observação
                                                    </label>
                                                    <textarea type="text" rows="10" name="observacao_execucao"
                                                              id="observacao"
                                                              class="form-control"
                                                              placeholder="observacao"><?php echo $Estq_mov->observacao_execucao ?></textarea>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <h3>Faturamento</h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="ibox ">
                                            <div class="ibox-title" style="border-color: #0a90eb">
                                                <h2>Faturamento</h2>
                                            </div>
                                            <div class="ibox-content">
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="data_recebimento3">Dt Faturamento Parcial</label>
                                                    <div class='input-group datePicke_bottom'>
                                                        <input type='text' name="data_recebimento3"
                                                               id="data_recebimento3"
                                                               class="form-control data_recebimento3"
                                                               value="
                                    <?php echo $Estq_mov->data_recebimento3; ?>"/>
                                                        <span class="input-group-addon"> <span
                                                                    class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="data_recebimento4"> Dt Recebimento Parcial</label>
                                                    <div class='input-group datePicke_bottom'>
                                                        <input type='text' name="data_recebimento4"
                                                               id="data_recebimento4"
                                                               class="form-control data_recebimento4"
                                                               value="
                                    <?php echo $Estq_mov->data_recebimento4; ?>"/>
                                                        <span class="input-group-addon"> <span
                                                                    class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="valor_recebimento">Valor Recebido Parcial</label>
                                                    <div class="input-group m-b">
                                                        <span class="input-group-addon">R$</span>
                                                        <input type="text" name="valor_recebimento"
                                                               id="valor_recebimento"
                                                               class="form-control money"
                                                               value="
                                    <?php echo $Estq_mov->valor_recebimento ?>"
                                                               placeholder="Valor">
                                                    </div>
                                                </div>

                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="data_recebimento">Dt Faturamento</label>
                                                    <div class='input-group datePicke_bottom'>
                                                        <input type='text' name="data_recebimento" id="data_recebimento"
                                                               class="form-control data_recebimento"
                                                               value="<?php echo $Estq_mov->data_recebimento; ?>"/>
                                                        <span class="input-group-addon"> <span
                                                                    class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="data_recebimento2">Dt Recebimento </label>
                                                    <div class='input-group datePicke_bottom'>
                                                        <input type='text' name="data_recebimento2"
                                                               id="data_recebimento2"
                                                               class="form-control data_recebimento2"
                                                               value="<?php echo $Estq_mov->data_recebimento2; ?>"/>
                                                        <span class="input-group-addon"> <span
                                                                    class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>


                                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                                    <label for="nota_fiscal">Nota Fiscal </label>
                                                    <input type="text" name="nota_fiscal" id="nota_fiscal"
                                                           class="form-control"
                                                           value="<?php echo $Estq_mov->nota_fiscal ?>"
                                                           placeholder="Número Documento">
                                                </div>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        $.extend($.validator.methods, {
            date: function (value, element) {
                return this.optional(element) || /^\d\d?\/\d\d?\/\d\d\d?\d?$/.test(value);
            },
            number: function (value, element) {
                return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:\.\d{3})+)(?:,\d+)?$/.test(value);
            }
        });

        $.validator.setDefaults({
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'label label-danger',
            errorPlacement: function (error, element) {
                var label = null;
                if (element.parent('.input-group').length) {
                    label = element.parent().parent().find("label[for='" + $(element).attr('id') +
                        "']");
                    if (label) {
                        error.insertAfter(label);
                    } else {
                        error.insertAfter(element.parent());
                    }
                } else {
                    label = element.parent().parent().find("label[for='" + $(element).attr('id') +
                        "']");
                    if (label) {
                        error.insertAfter(label);
                    } else {
                        error.insertAfter(element);
                    }
                }
            }
        });

        var form = $("#form-mov");
        form.validate();

        form.children("div").steps({
            labels: {
                cancel: "Cancel",
                current: "current step:",
                pagination: "Pagination",
                finish: "Continuar",
                next: "Próximo",
                previous: "Anterior",
                loading: "Loading ..."
            },
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            enableCancelButton: true,
            onStepChanging: function (event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                var id = parseFloat($('#id').val());
                var url = root;
                if (id < 1)
                    url += '/Estq_mov/addAjax';
                else
                    url += '/Estq_mov/editAjax';

                var data = $(form).serialize();

                $.post(url, data, function (ret) {
                    if (!ret.result) {
                        BootstrapDialog.warning(ret.msg);
                        return;
                    }

                    if (id < 1) {
                        $('#id').val(ret.id);
                        $('#btnEquipe').show(500);
                        $('.equipe').hide(500);
                    }

                    BootstrapDialog.success(ret.msg);
                    setTimeout(function () {
                        window.location = root + '/Estq_mov/edit/id:' + ret.id;
                    }, 3000);
                });
            },
            onCanceled: function (event, currentIndex) {
                window.location = root + '/Estq_mov/all';
            }
        });


        $("#cliente").on('change',function (ev) {
            $.get('<?= $this->Html->getUrl('Cliente', 'listaEnderecos') ?>',{
                cliente_id:$(this).val()
            },function(data){
                let html = '<option value="">Selecione um Cliente</option>';
                $.each(data,function(key,value){
                    html += '<option value="'+value.id+'">'+value.endereco+'</option>'
                })
                $('#cliente_endereco').html(html).trigger('change.select2');
                $('#cep').val('');
                $('#endereco').val('');
                $('#bairro').val();
                $('#uf').val('').trigger('change');
                $('#cidade').val('');
                $('#numero').val('');
                $('#complemento').val('');
            },'json');
        });

        $('#cliente_endereco').on('change',function(ev){
            $.get('<?= $this->Html->getUrl('Cliente', 'buscaEndereco') ?>',{
                cliente_endereco_id:$(this).val()
            },function(data){
                $('#cep').val(data.cep);
                $('#endereco').val(data.endereco);
                $('#bairro').val(data.bairro);
                $('#uf').val(data.uf).trigger('change');
                $('#cidade').val(data.cidade);
                $('#numero').val(data.numero);
                $('#complemento').val(data.complemento);
            },'json');
        });

        $("#add_tiposervico").click(function () {
            BootstrapDialog.show({
                title: 'Novo Tipo de Serviço',
                message: $('<div></div>').load(root +
                    '/Concessionaria_tipo_servico/add/ajax:1/boot:1'),
                buttons: [{
                    icon: 'glyphicon glyphicon-send',
                    label: 'Salvar',
                    cssClass: 'btn-primary',
                    autospin: true,
                    action: function (dialog) {
                        dialog.enableButtons(false);
                        dialog.setClosable(false);

                        if ($("#nome").val() == "") {
                            BootstrapDialog.alert('Digite o nome!');
                            dialog.updateButtons();
                            dialog.enableButtons(true);
                            dialog.setClosable(true);
                            return false;
                        }

                        var url = root + '/Concessionaria_tipo_servico/post_add';
                        var data = 'nome=' + $("#nome").val() +
                            '&situacao=1&boot=1&origem=' + $("#origem").val();

                        $.post(url, data, function (txt) {
                            var url2 = root +
                                '/Concessionaria_tipo_servico/load_tipo';
                            var data2 = 'id=' + txt;
                            $.post(url2, data2, function (opt) {
                                $("#tipo_servico").html(opt).select2(
                                    'change.select2');
                                dialog.close();
                            });
                        });
                    }
                }, {
                    label: 'Fechar',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });
        });

        $("#valor_recebimento,#valor_recebimento2,#valor_recebimento3,#valor_recebimento4").on('keyup', function () {
            var valor_recebimento = ($("#valor_recebimento").val() != "") ? replaceAll($(
                "#valor_recebimento").val(), ',', '') : 0;
            var valor_recebimento2 = ($("#valor_recebimento2").val() != "") ? replaceAll($(
                "#valor_recebimento2").val(), ',', '') : 0;
            var valor_recebimento3 = ($("#valor_recebimento3").val() != "") ? replaceAll($(
                "#valor_recebimento3").val(), ',', '') : 0;
            var valor_recebimento4 = ($("#valor_recebimento4").val() != "") ? replaceAll($(
                "#valor_recebimento4").val(), ',', '') : 0;
            var total = parseFloat(valor_recebimento) + parseFloat(valor_recebimento2) + parseFloat(
                valor_recebimento3) + parseFloat(valor_recebimento4);
            $(".total").html($.number(total, 2));
        });

        $('#btnEquipe').click(function () {
            var url = root + '/Equipe_movimento/all/first:1/modal:1/ajax:true/movimento:' + $('#id').val();
        });

        $("#codigo_equipe_id").select2({
            placeholder: 'Selecione uma equipe',
            theme: 'bootstrap',
            width: '100%'
        }).on('change', function (ev) {
            $('#codigo_equipe').val($("#codigo_equipe_id option:selected").text());
        });

        $("#eletrecista01_id").select2({
            placeholder: 'Selecione um eletricista',
            theme: 'bootstrap',
            width: '100%'
        }).on('change', function (ev) {
            $('#eletrecista01').val($("#eletrecista01_id option:selected").text());
        });

        $("#eletrecista02_id").select2({
            placeholder: 'Selecione um eletricista',
            theme: 'bootstrap',
            width: '100%'
        }).on('change', function (ev) {
            $('#eletrecista02').val($("#eletrecista02_id option:selected").text());
        });

        function setLogo(logo) {
            if (!logo.id) { return logo.text; }
            var img = $(logo.element.attributes[0]).val();

            var baseUrl = ''
            if(img.length > 0 ){
                baseUrl = root+"/"+img;
            }

            var $logo = $(
                '<span><img src="' + baseUrl.toLowerCase() +'"  style="height: 16px;width: 16px" /> ' + logo.text + '</span>'
            );

            return $logo;
        }
        $('#grupoSupervisao').select2({
            placeholder:'Selecione uma Origem',
            theme: 'bootstrap',
            width: '100%',
            templateResult: setLogo,
            templateSelection: setLogo,
            escapeMarkup: function(markup) {
                return markup;
            },
        });

        $('#colaborador').select2({
            placeholder: 'Selecione um colaborador',
            theme: 'bootstrap',
            width: '100%'
        });

        $('#fornecedor').select2({
            placeholder: 'Selecione um fornecedor',
            theme: 'bootstrap',
            width: '100%'
        });

        $('#cliente,#cliente_endereco,#uf').select2({
            placeholder: 'Selecione um Cliente',
            theme: 'bootstrap',
            width: '100%'
        });

        $('#tipo_servico').select2({
            placeholder: 'Selecione um Tipo de Serviço',
            theme: 'bootstrap',
            width: '100%'
        });

        $('#componente_id').select2({
            placeholder: 'Selecione um Componente',
            theme: 'bootstrap',
            width: '100%'
        });

        $('#municipio').select2({
            placeholder: 'Selecione uma supervisao',
            theme: 'bootstrap',
            width: '100%'
        }).on('change', function (ev) {
            $('#municipio_id').val($('#municipio option:selected').data('id'));
            $('#sigla').val($('#municipio option:selected').data('sigla'))
        })

        $("#placa_veiculo_id").select2({
            placeholder: 'Selecione uma placa',
            theme: 'bootstrap',
            width: '100%'
        }).on('change', function (ev) {
            $('#placa_veiculo').val($("#placa_veiculo_id option:selected").text());
        });
    });
</script>

<style>
    .wizard > .content > .body {
        padding: 0;
        margin: 2.5%;
    }

    .wizard > .steps > ul > li {
        width: auto;
    }

    .select2-container--bootstrap .select2-selection {
        border-radius: 0px;
    }
</style>