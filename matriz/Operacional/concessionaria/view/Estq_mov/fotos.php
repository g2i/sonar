<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>O.S - Acervo de Fotos</h2>
        <ol class="breadcrumb">
            <li>O.S</li>
            <li class="active">
                <strong>Listagem de Imagens</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div>
                    <?php if ($this->getParam('modal')):
                        echo ' <div class="text-right">';
                        echo $this->Html->getNavegar(' <span class="glyphicon glyphicon-plus-sign"></span>Adicionar Foto', 'Estq_mov', 'add_fotos',
                            array("ajax" => true, "modal" => "1", "id" => $this->getParam('id')),
                            array('class' => 'btn btn-primary', 'data-placement' => 'bottom', 'data-togle' => 'tooltip', 'title' => 'Cadastrar Foto'));
                        echo '</div>';
                        ?>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-right">
                                  <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Adicionar Foto ', 'Estq_mov', 'add_fotos',
                                            array('id' => $idMovimentacao),
                                            array('class' => 'btn btn-primary btn-sm', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Cadastrar Foto")); ?>

                                    <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-print"></span> Imprimir Acervo de Fotos ', 'Estq_mov', 'book_report',
                                      array('id' => $idMovimentacao),
                                      array('class' => 'btn btn-default btn-sm', 'target' => '_blank')); ?>
                                </div>

                            </div>
                        </div>
                    <?php endif; ?>
                    <!-- tabela de resultados -->
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Imagens</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>
                                                Ordem
                                            </th>
                                            <th>
                                                Código
                                            </th>
                                            <th>
                                                O.S
                                            </th>
                                            <th>
                                                Observação
                                            </th>
                                            <th>
                                                Titulo
                                            </th>
                                            <th>
                                                Tipo
                                            </th>
                                            <th>
                                                Usuário
                                            </th>
                                            <th>
                                                Cadastrado Em
                                            </th>

                                            <th>Opções</th>
                                        </tr>
                                        <?php
                                        $tipo = array(
                                            1 => 'Acervo de Fotos',
                                            2 => 'Manutenção Preventiva/Corretiva'
                                        );
                                        foreach ($concessionariaImagens as $imagem): ?>
                                            <tr>
                                                <td><?= $imagem->ordem ?></td>
                                                <td><?= $imagem->id ?></td>
                                                <td><?= $imagem->estq_mov_id ?></td>
                                                <td><?= $imagem->observacao ?></td>
                                                <td><?= $imagem->estrutura ?></td>
                                                <td><?= $tipo[$imagem->tipo_imagem] ?></td>
                                                <td><?= $imagem->getUsuario()->nome ?></td>
                                                <td><?= DataTimeBr($imagem->cadastrado_em) ?></td>
                                                <td width="40" style="padding:2px;">
                                                    <?php
                                                    echo
                                                    $this->Html->getLink('<span class="fa fa-pencil" data-toggle="tooltip" data-placement="auto" title="Editar Foto"></span>', 'Estq_mov', 'edit_foto',
                                                        array('id' => $imagem->id, 'estqMovId' => $idMovimentacao),
                                                        array('class' => 'btn btn-warning btn-xs'));
                                                    echo '&nbsp;';
                                                    echo
                                                    $this->Html->getLink('<span class="fa fa-trash-o" data-toggle="tooltip" data-placement="auto" title="Excluir Foto"></span>', 'Estq_mov', 'delete_foto',
                                                        array('id' => $imagem->id, 'estqMovId' => $idMovimentacao),
                                                        array('class' => 'btn btn-danger btn-xs', 'data-toggle' => 'modal'));
                                                    echo '&nbsp;';
                                                    echo $this->Html->getLink('<span class="fa fa-cloud-download" data-toggle="tooltip" data-placement="auto" title="Download"></span>', 'Estq_mov', 'Download',
                                                        array('id' => $imagem->id, 'estqMovId' => $idMovimentacao),
                                                        array('class' => 'btn btn-info btn-xs'));
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>

                                    <!-- menu de paginação -->
                                    <div style="text-align:center"><?php echo $nav; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $("#tipo_filtro").change(function () {
            carregar_tipo();
        });

        function carregar_tipo() {
            if ($("#tipo_filtro").val() == 1)
                getTipo($("#tipo_filtro").val(), '<?php echo $this->getParam('fornecedor'); ?>');
            else if ($("#tipo_filtro").val() == 2)
                getTipo($("#tipo_filtro").val(), '<?php echo $this->getParam('cliente'); ?>');
            else if ($("#tipo_filtro").val() == 3)
                getTipo($("#tipo_filtro").val(), '<?php echo $this->getParam('colaborador'); ?>');
        }

        //toltip
        $(document).ready(function () {
            carregar_tipo();
            $('[data-toggle="tooltip"]').tooltip();
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Movimentacoes', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Movimentacoes', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });
        });

        $(function () {
            $("input[id='requer']").click(function () {
                if ($(this).val() == 'Cliente') {
                    $(".cliente").show(500);
                    $(".colaborador").hide(500);
                    $(".fornecedor").hide(500);
                }
                else if ($(this).val() == 'Colaborador') {
                    $(".cliente").hide(500);
                    $(".colaborador").show(500);
                    $(".fornecedor").hide(500);
                }
                else if ($(this).val() == 'Fornecedor') {
                    $(".cliente").hide(500);
                    $(".colaborador").hide(500);
                    $(".fornecedor").show(500);
                }

            })
        });


    </script>