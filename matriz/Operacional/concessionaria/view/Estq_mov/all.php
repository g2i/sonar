<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>O.S</h2>
        <ol class="breadcrumb">
            <li>O.S</li>
            <li class="active">
                <strong>Listagem de O.S</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <!----------FILTROS---------->
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox-content">
                    <div class="form">
                        <form role="form" id="form"
                              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                              method="get" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" name="m" id="m" value="<?php echo CONTROLLER; ?>">
                            <input type="hidden" name="p" id="p" value="<?php echo ACTION; ?>">

                            <div class="form-group col-md-4 col-sm-6 col-xs-12 ">
                                <label for="inicio">Início</label>

                                <div class='input-group datePicke_bottom'>
                                    <input type="text" name="inicio" id="inicio" class="form-control date"
                                           value="<?= $this->getParam('inicio') ?>"/>
                                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 ">
                                <label for="fim">Fim</label>

                                <div class='input-group datePicke_bottom'>
                                    <input type="text" name="fim" id="fim" class="form-control date"
                                           value="<?= $this->getParam('fim') ?>"/>
                                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="tipo_data">Tipo de Data</label>
                                <select name="tipo_data" class="form-control" id="tipo_data">
                                    <option selected value="">Selecione um Tipo</option>
                                    <!--                                    <option value="datadoc">Data de Liberação Energisa</option>-->
                                    <!--                                    <option value="prazo_viabilizado">Prazo Viabilizado(5 dias)</option>-->
                                    <!--                                    <option value="viabilizado">Viabilizado</option>-->
                                    <!--                                    <option value="data">Data vencimento O.S</option>-->
                                    <option value="data_medicao">Data Conclusão Obra</option>
                                    <!--                                    <option value="desl_inicial">Desligamento Inicial</option>-->
                                    <!--                                    <option value="desl_final">Desligamento Final</option>-->
                                    <!--                                    <option value="data_tci">Data TCI</option>-->
                                    <!--                                    <option value="book_fotografico">Book Fotográfico</option>-->

                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4 form-group">
                                <label for="id">Código</label>
                                <input type="number" name="filtro[interno][id]" id="id" class="form-control maskInt"
                                       value="<?php echo $this->getParam('id'); ?>">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="tipo">Tipo de O.S</label>
                                <select name="filtro[externo][tipo]" class="form-control" id="tipo">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Estq_tipomovs as $e): ?>
                                        <?php
                                        if ($this->getParam('tipo') == $e->id) {
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        } ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-md-4 form-group">
                                <label for="situacao_mov">Situação do O.S</label>
                                <select name="filtro[externo][situacao_mov]" class="form-control" id="situacao_mov">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($ConcessionariaSituacao as $e): ?>
                                        <?php
                                        if ($this->getParam('situacao_mov') == $e->id) {
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        } ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="id">N° Concessionaria </label>
                                <input type="text" name="filtro[interno][numdoc]" id="numdoc"
                                       class="form-control maskInt"
                                       value="<?php echo $this->getParam('numdoc'); ?>">
                            </div>
                            <!--                            <div class="col-md-4 form-group">-->
                            <!--                                <label for="id">N.R (COI)</label>-->
                            <!--                                <input type="text" name="filtro[interno][coi]" id="coi" class="form-control maskInt"-->
                            <!--                                       value="-->
                            <?php //echo $this->getParam('coi'); ?><!--">-->
                            <!--                            </div>-->
                            <!--                            <div class="col-md-4 form-group">-->
                            <!--                                <label for="numdoc">O.S (Tablet)</label>-->
                            <!--                                <input type="text" name="filtro[interno][tablet]" id="tablet"-->
                            <!--                                       class="form-control maskInt"-->
                            <!--                                       value="-->
                            <?php //echo $this->getParam('tablet'); ?><!--">-->
                            <!--                            </div>-->
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 cliente">
                                <label for="cliente">Cliente</label>
                                <select name="filtro[externo][cliente]" class="form-control" id="cliente">
                                    <option value="">Selecione:</option>
                                    <?php
                                    foreach ($Clientes as $e) {
                                        if ($this->getParam('cliente') == $e->id) {
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <!--                            <div class="form-group col-md-4 col-sm-6 col-xs-12 cliente">-->
                            <!--                                <label for="cliente">Tipo de serviço</label>-->
                            <!---->
                            <!--                                <select name="filtro[externo][tipo_servico]" class="form-control" id="tipoServico">-->
                            <!--                                    <option value="">Selecione:</option>-->
                            <!--                                    --><?php
                            //                                    foreach ($TiposServicos as $e) {
                            //                                        if ($this->getParam('tipo_servico') == $e->id) {
                            //                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                            //                                        } else {
                            //                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                            //                                        }
                            //                                    }
                            //                                    ?>
                            <!--                                </select>-->
                            <!--                            </div>-->
                            <div class="col-md-12 text-right">
                                <button id="exportarExcel" class="btn btn-warning"
                                        title="Exportar os movimentos para excel"><span
                                            class="fa fa-file-excel-o "></span></button>

                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>" class="btn btn-default"
                                   data-dismiss="modal" data-toggle="tooltip" data-placement="bottom"
                                   title="Recarregar a página"><span
                                            class="glyphicon glyphicon-refresh "></span></a>
                                <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                        data-placement="bottom"
                                        title="Pesquisar"><span class="glyphicon glyphicon-search"></span></button>
                            </div>

                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>


            <!----------RESULTADOS---------->
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox-content">
                    <div>
                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p>
                                <!--                                --><?php //echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Importação recebidos', 'Estq_mov', 'importacao_r', NULL, array('class' => 'btn btn-primary', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Importar recebidos")); ?>

                                <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Movimento', 'Estq_mov', 'add', NULL, array('class' => 'btn btn-primary', 'data-tool' => "tooltip", 'data-placement' => "bottom", 'title' => "Cadastrar Movimento")); ?>
                            </p>
                        </div>

                        <!-- tabela de resultados -->
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            <a href=''>
                                                Código
                                            </a>
                                        </th>
                                        <th>
                                            <a href=''>
                                                Origem
                                            </a>
                                        </th>
                                        <th>
                                            <a href=''>
                                                Cliente
                                            </a>
                                        </th>
<!--                                        <th>-->
<!--                                            <a href=''>-->
<!--                                                Vl. Total Serviço-->
<!--                                            </a>-->
<!--                                        </th>-->
<!--                                        <th>-->
<!--                                            <a href=''>-->
<!--                                                Vl. Total Artigos-->
<!--                                            </a>-->
<!--                                        </th>-->
                                        <th>
                                            <a href=''>
                                                Valor Total O.S
                                            </a>
                                        </th>

                                        <th>
                                            <a href=''>
                                                Equipe
                                            </a>
                                        </th>
                                        <th>
                                            <a href=''>
                                                Dt. Conclusão
                                            </a>
                                        </th>
                                        <th>
                                            <a href=''>
                                                Situação Mov.
                                            </a>
                                        </th>
                                        <!-- <th>
                                            <a href='<?php echo $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => 'status', 'sort' => $sort)); ?>'>
                                                Status
                                            </a>
                                        </th>-->
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>

                                    </tr>
                                    <?php
                                    foreach ($Estq_movs as $e) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->id, 'Estq_mov', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo "<td>{$e->getGrupoSupervisao()->nome}</td>";
                                        echo "<td>{$e->getCliente()->nome}</td>";
//                                        echo '<td></td>';
//                                        echo '<td></td>';
                                        echo '<td>';
                                        echo $this->Html->getLink(number_format(empty($e->cis_valor_os_final) ? $e->valor_os_final : $e->cis_valor_os_final, 2, ',', '.'), 'Estq_mov', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        echo $this->Html->getLink($e->getEquipe()->nome, 'Estq_mov', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink(ConvertData($e->data_medicao), 'Estq_mov', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais data_medicao
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->getSituacaoMov()->nome, 'Concessionaria_situacao', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais data_medicao
                                        echo '</td>';
                                        //btns geral
                                        echo '<td class="actions text-right">';
                                        echo '<div class="btn-group">';
                                        echo '<button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-list"></span> Opções
                                                    <span class="caret"></span></button>';
                                        echo '<ul class="dropdown-menu pull-right">';

//                                        if ($e->status == 2) {
//                                            echo '<li>';
//                                            echo '<span class="img-processado"  data-toggle="tooltip" data-placement="auto" title="Processado"> Processado</span>'; // atributos HTML opcionais
//                                            echo '</li>';
//                                            echo '<li width="30" style="padding-top: 0px; padding-bottom: 0px;">';
//                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-download-alt"> Importação</span> ', 'Estq_mov', 'importacao',
//                                                array('mov' => $e->id),
//                                                array('class' => 'btn btn-default btn-sm disabled', 'data-togle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Importação'));
//                                            echo '</li>';
//                                            echo '<li width="30" style="padding-top: 0px; padding-bottom: 0px;">';
//                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="auto" title="Editar Movimento"> Editar </span> ', 'Estq_mov', 'edit',
//                                                array('id' => $e->id),
//                                                array('class' => 'btn btn-default btn-sm disabled'));
//                                            echo '</li>';
//                                            echo '<li width="30" style="padding-top: 0px; padding-bottom: 0px;">';
//                                            echo $this->Html->getLink('<span class="fa fa-trash-o" data-toggle="tooltip" data-placement="auto" title="Excluir Movimento"> Cancelar Orçamento</span>', 'Estq_mov', 'delete',
//                                                array('id' => $e->id),
//                                                array('class' => 'btn btn-default btn-sm disabled', 'data-toggle' => 'modal'));
//                                            echo '</li>';
//
//                                        }
//                                        else {
                                        echo '<li>';

                                        echo '</li>';
//                                            echo '<li width="30" style="padding:2px;">';
//                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-download-alt"> Importação</span> ', 'Estq_mov', 'importacao',
//                                                array('mov' => $e->id),
//                                                array('class' => 'btn btn-default btn-sm', 'data-togle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Importação'));
//                                            echo '</li>';

                                        echo '<li width="30" style="padding:2px;">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit" data-toggle="tooltip" data-placement="auto" title="Editar Movimento"> Editar </span> ', 'Estq_mov', 'edit',
                                            array('id' => $e->id),
                                            array('class' => 'btn btn-default btn-sm'));
                                        echo '</li>';

                                        echo '<li width="30" style="padding:2px;">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-align-justify" data-toggle="tooltip" data-placement="auto" title="Detalhes de Movimento"> Artigos</span> ', 'Estq_movdetalhes', 'all',
                                            array('id' => $e->id, 'numdoc' => $e->numdoc, 'artigo' => $this->getParam('estq_artigo.nome'), 'colab' => $this->getParam('c_complemento_mov.colaborador'), 'datacautela' => DataSQL($this->getParam('data_cautela'))),
                                            array('class' => 'btn btn-default btn-sm', 'data-target' => '.bs-lg'));
                                        echo '</li>';

//                                            echo '<li width="30" style="padding:2px;">';
//                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-screenshot" data-toggle="tooltip" data-placement="auto" title="Imprimir Book"> Imprimir Acervo</span> ', 'Estq_mov', 'book_report',
//                                                array('id' => $e->id),
//                                                array('class' => 'btn btn-default btn-sm', 'target' => '_blank'));
//                                            echo '</li>';
//                                            echo '<li width="30" style="padding:2px;">';
//                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-camera" data-toggle="tooltip"  data-placement="auto" title="Book Fotográfico"> Imprimir Acervo Fotográfico</span> ', 'Estq_mov', 'fotos',
//                                                array('id' => $e->id),
//                                                array('class' => 'btn btn-default btn-sm', 'target' => '_blank'));
//                                            echo '</li>';
                                        echo '<li width="30" style="padding:2px;">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-barcode" data-toggle="tooltip"  data-placement="auto" title="Serviços"> Serviços</span>', 'Concessionaria_item_servico', 'all',
                                            array('movimento' => $e->id),
                                            array('class' => 'btn btn-info btn-equipe', 'target' => '_blank'));
                                        /*echo $this->Html->getLink('<span class="glyphicon glyphicon-barcode" data-toggle="tooltip"  data-placement="auto" title="Serviços"> Serviços</span> ', 'Concessionaria_item_servico', 'all',
                                            array('movimento' => $e->id, 'first' => 1, 'modal' => 1, 'ajax' => true),
                                            array('class' => 'btn btn-default btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                            */
                                        echo '</li>';

                                        echo '<li width="30" style="padding:2px;">';
                                        echo '<a href="Javascript:void(0)" onclick="relatorios(' . $e->id . ')" class="btn btn-default btn-sm ">';
                                        echo '<span class="glyphicon glyphicon-print " data-toggle="tooltip" data-placement="auto" title="Imprimir Relatorios"> Relatorios </span> ';
                                        echo '</li>';

                                        echo '<li width="30" style="padding:2px;">';
                                        echo $this->Html->getLink('<span class="fa fa-trash-o" data-toggle="tooltip" data-placement="auto" title="Cancelar Orçamento"> Cancelar Orçamento</span> ', 'Estq_mov', 'delete',
                                            array('id' => $e->id),
                                            array('class' => 'btn btn-default btn-sm', 'data-toggle' => 'modal'));
                                        echo '</li>';
//                                        }



//                                        echo '<li width="30" style="padding:2px;">';
//                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-screenshot" data-toggle="tooltip" data-placement="auto" title="Imprimir Relatorio Movimentação de Transformadores"> Relatorio de Mov/Transformadores</span> ', 'Relatorios', 'relatorio_movimento_transformadores',
//                                            array('id' => $e->id),
//                                            array('class' => 'btn btn-default btn-sm', 'target' => '_blank'));
//                                        echo '</li>';

                                        echo '<li width="30" style="padding:2px;">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-tags" data-toggle="tooltip" data-placement="auto" title="Equipe"> Equipe</span> ', 'Equipe_movimento', 'all',
                                            array('movimento' => $e->id, 'first' => 1, 'modal' => 1, 'ajax' => true),
                                            array('class' => 'btn btn-default btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                        echo '</li>';
//                                        echo '<li width="30" style="padding:2px;">';
//                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-tags" data-toggle="tooltip" data-placement="auto" title="Croqui"> Croqui</span> ', null, null,
//                                            array('movimento' => $e->id, 'first' => 1, 'modal' => 1, 'ajax' => true),
//                                            array('class' => 'btn btn-default btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
//                                        echo '</li>';
//                                        echo '<li width="30" style="padding:2px;">';
//                                        echo $this->Html->getLink('<span class="fa fa-bolt" data-toggle="tooltip " data-placement="auto" title="Transformadores"> Transformadores</span> ', 'Anexos', 'all',
//                                            array('id' => $e->id),
//                                            array('class' => 'btn btn-default btn-sm', 'data-target' => '.bs-lg'));
//                                        echo '</li>';
                                        echo '</tr>';
                                        echo '</ul>';
                                        echo '</div>';
                                        echo '</td>';
                                    }
                                    ?>
                                </table>
                                <!-- menu de paginação -->
                                <div style="text-align:center">
                                    <?php echo $nav; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {

            $('#tipoServico').select2({
                language: 'pt-BR',
                theme: 'bootstrap',
                width: '100%'
            });

            //function para o btn de pesquisa em form
            $('.filtros').find('#buscar-filtro').click(function () {
                let form_action = $(this).closest('form').attr('action');
                let form_serialize = $(this).closest('form').serialize();
                let url_completa = form_action + '?' + form_serialize;
                console.log(url_completa);
                carregaTabelaResponsiva(url_completa);
            });

            //function para paginar por ajax
            $(document).on('click', '.pagination a, .table thead a', function (e) {
                e.preventDefault();
                let url = $(this).attr('href');

                if (url != "") {
                    carregaTabelaResponsiva(url);
                }
                return false;
            });

            //function para pegar as URLs
            function carregaTabelaResponsiva(url) {
                $.ajax({
                    type: 'GET',
                    url: url,
                    beforeSend: function () {
                    },
                    success: function (data) {
                        let conteudo = $('<div>').append(data).find('.table-responsive');
                        $(".table-responsive").html(conteudo);
                    },
                    complete: function () {
                    },
                    error: function () {
                    }
                })
                ;
            }

            //toltip
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('#search').keyup(function () {
                    var r = true;
                    if (r) {
                        r = false;
                        $("div.table-responsive").load(
                            <?php
                            if (isset($_GET['orderBy']))
                                echo '"' . $this->Html->getUrl('Estq_mov', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                            else
                                echo '"' . $this->Html->getUrl('Estq_mov', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                            ?>
                            , function () {
                                r = true;
                            });
                    }
                });
            });

            $(function () {
                $("input[id='requer']").click(function () {
                    console.log(id);
                    if ($(this).val() == 'Cliente') {
                        $(".cliente").show(500);
                        $(".colaborador").hide(500);
                        $(".fornecedor").hide(500);
                    } else if ($(this).val() == 'Colaborador') {
                        $(".cliente").hide(500);
                        $(".colaborador").show(500);
                        $(".fornecedor").hide(500);
                    } else if ($(this).val() == 'Fornecedor') {
                        $(".cliente").hide(500);
                        $(".colaborador").hide(500);
                        $(".fornecedor").show(500);
                    }

                })
            });

        });


        $('#exportarExcel').on('click', function (ev) {
            window.open("<?=SITE_PATH . '/Estq_mov/exportarDadosBi?data_inicio='?>" + $('#inicio').val() + "&data_fim=" + $('#fim').val() + "&tipo_data=" + $('#tipo_data').val())
        });

    </script>