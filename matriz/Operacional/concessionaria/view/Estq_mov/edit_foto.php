<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>O.S - Acervo de Fotos</h2>
        <ol class="breadcrumb">
            <li> Foto</li>
            <li class="active">
                <strong>Editar Foto</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" id="form-anexos" role="form"
                          action="<?php echo $this->Html->getUrl('Estq_mov', 'post_edit_foto') ?>"
                          enctype="multipart/form-data">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <input type="hidden" id="movimentacao_id" name="movimentacao_id"
                                   value="<?= $concessionariaImagem->movimentacao_id ?>">

                            <input type="hidden" id="id" name="id"
                                   value="<?= $concessionariaImagem->id ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Titulo</label>
                                                <input value="<?= $concessionariaImagem->estrutura ?>" required
                                                       class="form-control"
                                                       id="estrutura" type="text" name="estrutura">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Estrutura</label>
                                                <input class="form-control" id="ordem"
                                                       value="<?= $concessionariaImagem->ordem ?>" required
                                                       type="number"
                                                       name="ordem">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Tamanho da Imagem</label>
                                                <select name="tamanho" class="form-control " id="tipo_imagem">
                                                    <option <?= $concessionariaImagem->tamanho =='P'?'selected':''?> value="P">Pequena</option>
                                                    <option <?= $concessionariaImagem->tamanho =='M'?'selected':''?> value="M">Media</option>
                                                    <option <?= $concessionariaImagem->tamanho =='G'?'selected':''?> value="G">Grande</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="estrutura">Tipo</label>
                                                <select name="tipo_imagem" class="form-control " id="tipo_imagem">
                                                    <option <?= $concessionariaImagem->tipo_imagem == 1?'selected':''?> value="1">Acervo de Fotos</option>
                                                    <option <?= $concessionariaImagem->tipo_imagem == 2?'selected':''?>value="2">Manutenção Preventiva/Corretiva</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                            <label for="observacao">Observação</label>
                                            <textarea name="observacao" id="observacao" required rows="9"
                                                      class="form-control"><?= $concessionariaImagem->observacao ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="files">Foto</label>
                                                <?php
                                                $url = '';
                                                $fileObj = array();
                                                if(!empty($concessionariaImagem->caminho_imagem)) {
                                                    $extensao = pathinfo($concessionariaImagem->caminho_imagem, PATHINFO_EXTENSION);
                                                    if ($extensao == 'pdf' || $extensao == 'txt' || $extensao == 'rar')
                                                        $fileObj['type'] = $extensao;

                                                    $url = 'http://' . $_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'].SITE_PATH.'/'. $concessionariaImagem->caminho_imagem;

                                                    $html = new html();
                                                    $urlDel = $html->getUrl('Estq_mov', 'delete_anexo', array('id' => $concessionariaImagem->id, 'ajax' => true));

                                                    $fileObj['url'] = $urlDel;
                                                    $fileObj['caption'] = $concessionariaImagem->estrutura;
                                                    $fileObj['width'] = "120px";
                                                    $fileObj['key'] = $concessionariaImagem->id;
                                                }
                                                $fileObj = json_encode($fileObj);

                                                ?>
                                                <input id="files" type="file" name="caminho[]"
                                                       class="input-file-edit"
                                                       link-img="<?= $url ?>"
                                                       data-object='<?= $fileObj?>'
                                                       >
                                                <span class="help-block" style="color: red">Extensões: png, jpg, jpeg e gifs.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_mov', 'fotos', array('id' => $concessionariaImagem->estq_mov_id)) ?>"
                               class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar"
                                   onclick="senduploads('form-anexos')">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.input-file-edit').each(function () {
            var img = [];
            if($(this).attr('link-img').length != 0){
                img = $(this).attr('link-img').split(',')
            }
            $(this).fileinput({
                language: "pt-BR",
                maxFileCount: 1,
                overwriteInitial: true,
                initialPreview: img,
                previewFileType: 'any',
                initialPreviewConfig: [JSON.parse($(this).attr('data-object'))],
                layoutTemplates: {
                    main1: "{preview}\n" +
                        "<div class=\'input-group {class}\'>\n" +
                        "   <div class=\'input-group-btn\'>\n" +
                        "       {browse}\n" +
                        "       {remove}\n" +
                        "   </div>\n" +
                        "   {caption}\n" +
                        "</div>"
                },
                initialPreviewAsData: true
            }).on('filebatchselected', function (event, files) {
                $('#caminho').val('');
            }).on("filebeforedelete", function () {
                return new Promise(function (resolve, reject) {
                    BootstrapDialog.show({
                        message: 'Certeza que deseja apagar este anexo?',
                        type: BootstrapDialog.TYPE_WARNING,
                        buttons: [
                            {
                                label: 'Confirmar',
                                action: function (dialogRef) {
                                    resolve();
                                    $('#caminho').val('');
                                    dialogRef.close();
                                }
                            },
                            {
                                label: 'cancelar',
                                action: function (dialog) {
                                    reject();
                                    dialogRef.close();
                                }
                            }
                        ]
                    });
                });
            });
        });
    })
</script>