
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Concessionaria_fator</h2>
    <ol class="breadcrumb">
    <li>Concessionaria_fator</li>
    <li class="active">
    <strong>Editar Concessionaria_fator</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Concessionaria_fator', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="descricao">Descricao</label>
            <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Concessionaria_fator->descricao ?>" placeholder="Descricao">
        </div>
        <div class="form-group">
            <label class="required" for="valor">Valor <span class="glyphicon glyphicon-asterisk"></span></label>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao_id</label>
            <input type="number" name="situacao_id" id="situacao_id" class="form-control" value="<?php echo $Concessionaria_fator->situacao_id ?>" placeholder="Situacao_id">
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Concessionaria_fator->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Concessionaria_fator', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>