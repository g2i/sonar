
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Concessionaria_fator</h2>
    <ol class="breadcrumb">
    <li>Concessionaria_fator</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Concessionaria_fator', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Concessionaria_fator', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Concessionaria_fator', 'all', array('orderBy' => 'descricao')); ?>'>
                        descricao
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Concessionaria_fator', 'all', array('orderBy' => 'valor')); ?>'>
                        valor
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Concessionaria_fator', 'all', array('orderBy' => 'situacao_id')); ?>'>
                        situacao_id
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Concessionaria_fatores as $c) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($c->id, 'Concessionaria_fator', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->descricao, 'Concessionaria_fator', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->valor, 'Concessionaria_fator', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->situacao_id, 'Concessionaria_fator', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Concessionaria_fator', 'edit', 
                    array('id' => $c->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Concessionaria_fator', 'delete', 
                    array('id' => $c->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Concessionaria_fator', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Concessionaria_fator', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>