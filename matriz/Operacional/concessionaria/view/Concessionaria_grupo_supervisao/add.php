<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Origem</h2>
        <ol class="breadcrumb">
            <li>Origem</li>
            <li class="active">
                <strong>Adicionar Origem</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" enctype="multipart/form-data"
                          action="<?php echo $this->Html->getUrl('Concessionaria_grupo_supervisao', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label class="required" for="nome">Nome <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Concessionaria_grupo_supervisao->nome ?>" placeholder="Nome"
                                       required>
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="centro">Valor de Centro</label>
                                <input type="text" name="centro" id="centro" class="form-control"
                                       value="<?php echo $Concessionaria_grupo_supervisao->centro ?>"
                                       placeholder="Centro">
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="files">Logo</label>
                                    <input id="files" type="file" name="url[]" class="input-file"
                                           data-overwrite-initial="false">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Concessionaria_grupo_supervisao', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
        initeComponentes(document)
    })
</script>