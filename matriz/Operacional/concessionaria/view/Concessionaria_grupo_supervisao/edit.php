<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Origem</h2>
        <ol class="breadcrumb">
            <li>Origem</li>
            <li class="active">
                <strong>Editar Origem</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" enctype="multipart/form-data"
                          action="<?php echo $this->Html->getUrl('Concessionaria_grupo_supervisao', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label class="required" for="nome">Nome <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Concessionaria_grupo_supervisao->nome ?>" placeholder="Nome"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="centro">Centro</label>
                                <input type="text" name="centro" id="centro" class="form-control"
                                       value="<?php echo $Concessionaria_grupo_supervisao->centro ?>"
                                       placeholder="Centro">
                            </div>

                            <div class="form-group">
                                <label for="files">Logo</label>
                                <?php
                                $url = '';
                                $fileObj = array();
                                if(!empty($Concessionaria_grupo_supervisao->url)) {
                                    $extensao = pathinfo($Concessionaria_grupo_supervisao->url, PATHINFO_EXTENSION);
                                    if ($extensao == 'pdf' || $extensao == 'txt' || $extensao == 'rar')
                                        $fileObj['type'] = $extensao;

                                    $url = 'http://' . $_SERVER['SERVER_NAME'] .':'.$_SERVER['SERVER_PORT'].SITE_PATH.'/'. $Concessionaria_grupo_supervisao->url;

                                    $html = new html();
                                    $urlDel = $html->getUrl('Concessionaria_grupo_supervisao', 'delete_anexo', array('id' => $Concessionaria_grupo_supervisao->id, 'ajax' => true));

                                    $fileObj['url'] = $urlDel;
                                    $fileObj['caption'] = $Concessionaria_grupo_supervisao->nome;
                                    $fileObj['width'] = "120px";
                                    $fileObj['key'] = $Concessionaria_grupo_supervisao->id;
                                }
                                $fileObj = json_encode($fileObj);

                                ?>
                                <input id="files" type="file" name="caminho[]"
                                       class="input-file-edit"
                                       link-img="<?= $url ?>"
                                       data-object='<?= $fileObj?>'
                                >
                                <span class="help-block" style="color: red">Extensões: png, jpg, jpeg e gifs.</span>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Concessionaria_grupo_supervisao->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Concessionaria_grupo_supervisao', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.input-file-edit').each(function () {
            var img = [];
            if ($(this).attr('link-img').length != 0) {
                img = $(this).attr('link-img').split(',')
            }
            $(this).fileinput({
                language: "pt-BR",
                maxFileCount: 1,
                overwriteInitial: true,
                initialPreview: img,
                previewFileType: 'any',
                initialPreviewConfig: [JSON.parse($(this).attr('data-object'))],
                layoutTemplates: {
                    main1: "{preview}\n" +
                        "<div class=\'input-group {class}\'>\n" +
                        "   <div class=\'input-group-btn\'>\n" +
                        "       {browse}\n" +
                        "       {remove}\n" +
                        "   </div>\n" +
                        "   {caption}\n" +
                        "</div>"
                },
                initialPreviewAsData: true
            }).on('filebatchselected', function (event, files) {
                $('#caminho').val('');
            }).on("filebeforedelete", function () {
                return new Promise(function (resolve, reject) {
                    BootstrapDialog.show({
                        message: 'Certeza que deseja apagar este anexo?',
                        type: BootstrapDialog.TYPE_WARNING,
                        buttons: [
                            {
                                label: 'Confirmar',
                                action: function (dialogRef) {
                                    resolve();
                                    $('#caminho').val('');
                                    dialogRef.close();
                                }
                            },
                            {
                                label: 'cancelar',
                                action: function (dialog) {
                                    reject();
                                    dialogRef.close();
                                }
                            }
                        ]
                    });
                });
            });
        });
    });
</script>