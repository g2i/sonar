<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Concessionaria Serviço</h2>
        <ol class="breadcrumb">
            <li>Concessionaria Serviço</li>
            <li class="active">
                <strong>Adicionar </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                        action="<?php echo $this->Html->getUrl('Concessionaria_servico', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="codigo_livre">Código Livre <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="number" name="codigo_livre" id="codigo_livre" class="form-control"
                                    value="<?php echo $Concessionaria_servico->codigo_livre ?>"
                                    placeholder="Código Livre" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome">Nome do Serviço </label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                    value="<?php echo $Concessionaria_servico->nome ?>" placeholder="Nome do Serviço">
                            </div>

                            <input value="1" name="uso_servico" type="hidden"/>


                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="us">Usa US/KM ? <span class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="us" class="form-control" id="us" required>
                                            <option value="">Selecione :</option>
                                            <?php
                                                foreach ($Concessionaria_fator as $e) {
                                                    if ($e->id == $Concessionaria_servico->us)
                                                        echo '<option selected value="' . $e->id . '">' . $e->descricao . '</option>';
                                                    else
                                                        echo '<option value="' . $e->id . '">' . $e->descricao . '</option>';
                                                }
                                                ?>
                                    </select>
                            </div>
                            <input type="hidden" name="fator_us" id="fator_us" class="form-control" value="1" placeholder="fator us">
                            <div class="clearfix"></div>

                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Concessionaria_servico', 'all') ?>"
                                class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" async>

    $(function(){

        $('#fator_us').maskMoney({thousands:'.', decimal:','});


        $('#us').on('change',function(ev){
            if($(this).val() == 1) {
                $('#div_fator_us').show(500);
            }else{
                $('#div_fator_us').hide(500);
            }
        });



    });



</script>
