<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Posições de</li>
            <li class="active">
                <strong>Diagramas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox-content">
                    <div class="formi">
                        <form id="form" method="get" role="form"
                              action="<?php echo $this->Html->getUrl('Relatorios', 'relatorio_movimentacoes_geral') ?>"
                              target="_blank">

                            <div class="form-group col-md-4 col-sm-6 col-xs-12 ">
                                <label for="inicio">Início</label>

                                <div class='input-group datePicke_bottom'>
                                    <input type="text" name="inicio" id="inicio" class="form-control date"
                                           value="<?= $this->getParam('inicio') ?>"/>
                                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 ">
                                <label for="fim">Fim</label>

                                <div class='input-group datePicke_bottom'>
                                    <input type="text" name="fim" id="fim" class="form-control date"
                                           value="<?= $this->getParam('fim') ?>"/>
                                    <span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="situacao_mov">Situação do Movimento</label>
                                <select name="situacao_mov" class="form-control" id="situacao_mov">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($ConcessionariaSituacao as $e): ?>
                                        <?php
                                        if ($this->getParam('situacao_mov') == $e->id) {
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                        } ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
<!--                            <div class="col-md-4 form-group">-->
<!--                                <label for="id">N.O (SIAGO) </label>-->
<!--                                <input type="text" name="numdoc" id="numdoc"-->
<!--                                       class="form-control maskInt"-->
<!--                                       value="--><?php //echo $this->getParam('numdoc'); ?><!--">-->
<!--                            </div>-->
<!--                            <div class="col-md-4 form-group">-->
<!--                                <label for="id">N.R (COI)</label>-->
<!--                                <input type="text" name="coi" id="coi" class="form-control maskInt"-->
<!--                                       value="--><?php //echo $this->getParam('coi'); ?><!--">-->
<!--                            </div>-->
<!--                            <div class="col-md-4 form-group">-->
<!--                                <label for="numdoc">O.S (Tablet)</label>-->
<!--                                <input type="text" name="tablet" id="tablet"-->
<!--                                       class="form-control maskInt"-->
<!--                                       value="--><?php //echo $this->getParam('tablet'); ?><!--">-->
<!--                            </div>-->
                            <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                <label class="codigo_equipe" for="codigo_equipe">Código Equipe
                                    <span class=" "></span>
                                </label>
                                <select name="codigo_equipe_id[]" multiple class="form-control" id="codigo_equipe_id">
                                    <option value="">Selecione:</option>
                                    <?php
                                    foreach ($Equipes as $eq) {
                                        if ($eq->id == $Estq_mov->codigo_equipe_id)
                                            echo '<option selected value="' . $eq->id . '">' . $eq->nome . '</option>';
                                        else
                                            echo '<option value="' . $eq->id . '">' . $eq->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-xs-12 col-md-4">
                                <label for="tipo">Relatório</label>
                                <select id="tipo" name="tipo" class="form-control selectPicker" data-dropup-auto="false" data-live-search="true">
                                        <option data-action="relatorio_movimentacoes_geral" value="1"> Relatório de
                                            Movimentações em Aberto (Sem Recebimento)
                                        </option>
                                        <option data-action="relatorio_movimentacoes_geral_recebidos" value="2">
                                            Relatório de Movimentações (Com Recebimento)
                                        </option>
                                        <option data-divider="true"></option>
                                        <option data-action="relatorio_faturamento_por_equipes" value="3"> Relatório de
                                            Faturamento por Equipe
                                        </option>
                                        <option data-divider="true"></option>
                                        <option data-action="relatorio_medicoes_em_aberto" value="4"> Relatório de
                                            Medições
                                            em Aberto
                                        </option>
                                        <option data-action="relatorio_medicoes_recebidas" value="5"> Relatório de
                                            Medições
                                            Recebidas
                                        </option>
                                </select>
                            </div>
                            <div class="col-md-12 text-right">
                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>" class="btn btn-default"
                                   data-dismiss="modal" data-toggle="tooltip" data-placement="bottom"
                                   title="Recarregar a página"><span
                                            class="glyphicon glyphicon-refresh "></span></a>
                                <button type="submit" class="btn btn-warning"
                                        title="Gerar Relatório sem Contas Receber"><span
                                            class="glyphicon glyphicon-print"></span> Imprimir
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>


            <script>
                $(document).ready(function () {
                    console.log(root);

                    $('#codigo_equipe_id').select2({
                        placeholder: 'Selecione uma equipe',
                        theme: 'bootstrap',
                        width: '100%',
                        multiple: true
                    });
                    $('.selectPicker').selectpicker()
                    $('#tipo').change(function () {
                        $('#form').attr('action', root+'/Relatorios/' + $('#tipo option:selected').attr('data-action') + "/");
                    });
                });
            </script>