<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Relatório </li>
            <li class="active">
                <strong>Ficha de Estoque</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox-content">
                    <div class="form">
                        <form role="form" id="form-relatorios" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                              method="post">
                            <input type="hidden" name="stimulsoft_client_key" value="ViewerFx" />
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <label for="codigo_livre" class="required">Código</label>
                                <select name="codigo_livre" class="form-control codigo codigo-ajax" id="codigo_livre">
                                    <option value="">Selecione:</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6 col-sm-6 col-xs-12 ">
                                <label for="artigo">Artigos</label>
                                <select name="artigo" id="artigo" class="form-control report">
                                    <option value="">Selecione</option>
                                </select>
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-default botao-impressao" data-toggle="tooltip"
                                        data-placement="bottom" title="Impressão" id="print-relatorio"><span
                                        class="glyphicon glyphicon-print"></span></button>
                            </div>

                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        var modal = $('.modal');
        var documento = modal.find('.modal-content');

        documento.find("#print-relatorio").click(function () {
        var data = {
            artigo: documento.find("#artigo").val()
        };

        if(data.artigo==""){
            BootstrapDialog.alert('Selecione um artigo');
            return false;
       }
        $.Report('ficha', data);

    })

        initComponent();
        documento.find('select.codigo-ajax').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});
            $(this).change(function () {
                $(this).trigger("chosen:updated");
            $.ajax({
                type: 'POST',
                url: root + '/Estq_movdetalhes/GetArtigo',
                data: 'artigo=' + $(this).val(),
                success: function (txt) {
                    documento.find("#artigo").html(txt);
                    setTimeout(function () {
                        documento.find("#artigo").trigger("chosen:updated");
                    },20);
                }
            });
            })
        });

        documento.find('select#artigo').each(function () {
            $(this).ajaxChosen({
                dataType: "json",
                type: "POST",
                url: root + "/Estq_movdetalhes/listar2/",
                allow_single_deselect: true
            }, {loadingImg: root + "/img/loading.gif"}, {allow_single_deselect: true});
            $(this).change(function () {
                $(this).trigger("chosen:updated");

                $.ajax({
                    type: 'POST',
                    url: root + '/Estq_movdetalhes/GetCodigo',
                    data: 'artigo=' + $(this).val(),
                    success: function (txt) {
                        documento.find('select.codigo-ajax').html(txt);
                        setTimeout(function () {
                            documento.find('select.codigo-ajax').trigger("chosen:updated");
                        },20);
                    }
                });
            })
        });


    })
</script>
