<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatório</h2>
        <ol class="breadcrumb">
            <li>Posição do estoque</li>
            <li class="active">
                <strong><?= $tipo ?></strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="form-group col-md-12">
                        <label for="tipo">Sub-estoque</label>
                        <select name="subestoque" id="subestoque" class="form-control" data-select="2">
                            <option value="">Selecione</option>
                            <?php
                            foreach ($subestoque as $tt){
                                echo '<option value=' . $tt->id . '>' . $tt->nome . '</option>';
                            }

                            ?>
                        </select>
                    </div>
                    <input type="hidden" id="tipo" value="<?= $tipo ?>" />
                    <input type="hidden" id="t" value="<?= $t ?>" />
                    <div class="clearfix"></div>

                    <div class="text-right">
                        <a href=Javascript:void(0)"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="button" class="btn btn-primary" value="Gerar" id="bt-gerar">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<script>
    $(document).ready(function(){
        initeComponentes(document)
    })
</script>