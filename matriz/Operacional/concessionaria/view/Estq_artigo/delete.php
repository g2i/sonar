<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders"></div>
<form class="form" method="post" action="<?php echo $this->Html->getUrl('Estq_artigo', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Estq_artigo->nome; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Estq_artigo->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Estq_artigo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>
                </div>
            </div>
        </div>
    </div>
</div>