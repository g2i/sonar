<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Artigos</h2>
        <ol class="breadcrumb">
            <li>Artigos</li>
            <li class="active">
                <strong>Adicionar Artigos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content  ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_artigo', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de
                            preenchimento obrigatório.
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="codigo_livre">Código</label>
                            <input type="text" name="codigo_livre" id="codigo_livre" class="form-control"
                                   value="<?php echo $Estq_artigo->codigo_livre ?>" placeholder="Código" onblur="dublicidade('add',$('#codigo_livre').val(),null)">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label class="required" for="nome">Nome <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Estq_artigo->nome ?>"
                                   placeholder="Nome" required>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="controla_lote" class="required">Controla Lote<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="controla_lote" class="form-control" id="controla_lote" required>
                                <option value="">Selecione :</option>
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="vlcomercial">Valor Comercial</label>
                            <input type="text" name="vlcomercial" id="vlcomercial"
                                   class="form-control money"
                                   value="<?php echo $Estq_artigo->vlcomercial ?>" placeholder="Valor Comercial">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="vlcusto">Valor Custo</label>
                            <input type="text" name="vlcusto" id="vlcusto" class="form-control money"
                                   value="<?php echo $Estq_artigo->vlcusto ?>" placeholder="Valor Custo">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="vlcustomedio">Valor Custo Médio</label>
                            <input type="text" name="vlcustomedio" id="vlcustomedio"
                                   class="form-control money"
                                   value="<?php echo $Estq_artigo->vlcustomedio ?>" placeholder="Valor Custo Médio">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="unidade">Unidade</label>
                            <select name="unidade" class="form-control" id="unidade">
                                <option value="">Selecione :</option>
                                <?php
                                foreach ($Estq_unidade as $e) {
                                    if ($e->id == $Estq_artigo->unidade)
                                        echo '<option selected value="' . $e->id . '">' . $e->sigla . '</option>';
                                    else
                                        echo '<option value="' . $e->id . '">' . $e->sigla . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="tamanho">Tamanho</label>
                            <input type="text" name="tamanho" id="tamanho" class="form-control"
                                   value="<?php echo $Estq_artigo->tamanho ?>" placeholder="Tamanho">
                        </div>
                        <input type="hidden" name="situacao" value="1"/>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="subgrupo" class="required">Sub-Grupo<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="subgrupo" class="form-control" id="subgrupo" required>
                                <option value="">Selecione :</option>
                                <?php
                                foreach ($Estq_subgrupos as $e) {
                                    if ($e->id == $Estq_artigo->subgrupo)
                                        echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                    else
                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="clearfix"></div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Estq_artigo', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>