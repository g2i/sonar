<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Artigos</h2>
        <ol class="breadcrumb">
            <li>Artigos</li>
            <li class="active">
                <strong>Listagem de Artigos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <!-- formulario de pesquisa -->
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <div class="form">
                                <form role="form" id="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                    method="post" enctype="application/x-www-form-urlencoded">
                                    <input type="hidden" id="m" name="m" value="<?php echo CONTROLLER; ?>">
                                    <input type="hidden" name="p" id="p" value="<?php echo ACTION; ?>">
                                    <div class="col-md-3 form-group">
                                        <label for="codigo_livre">Código</label>
                                        <input type="number" name="filtro[interno][codigo_livre]" id="codigo_livre"
                                            class="form-control maskInt" value="<?php echo $this->getParam('codigo_livre'); ?>">
                                    </div>                                                                    
                                    <div class="col-md-3 form-group">
                                        <label for="nome">Nome</label>
                                        <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                            value="<?php echo $this->getParam('nome'); ?>">
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="subgrupo">Sub-Grupo</label>
                                        <select name="filtro[externo][subgrupo]" class="form-control " id="subgrupo">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Estq_subgrupos as $e): ?>
                                            <?php
                                                if ($this->getParam('subgrupo') == $e->id) {
                                                    echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                                } else {
                                                    echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                                }
                                                ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>  
                                    <div class="col-md-12 text-right">
                                        <a href="<?php echo $this->Html->getUrl('Estq_artigo', 'all') ?>" class="btn btn-default"
                                            data-dismiss="modal" data-toggle="tooltip" data-placement="bottom" title="Recarregar a página"><span
                                                class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                            data-placement="bottom" title="Pesquisar"><span class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p>
                                <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo artigo', 'Estq_artigo', 'add', NULL, array('class' => 'btn btn-primary', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Cadastrar Artigos")); ?>
                            </p>
                        </div>
                        <!-- tabela de resultados -->
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            <a href=''>
                                                Código
                                            </a>
                                        </th>
                                        <th>
                                            <a href=''>
                                                Nome
                                            </a>
                                        </th>
                                        
                                        <th>
                                            <a href=''>
                                                Sub-Grupo
                                            </a>
                                        </th>
                                        <th>
                                            <a href=''>
                                                Valor Custo
                                            </a>
                                        </th>
                                        <th>
                                            <a href=''>
                                                Unidade
                                            </a>
                                        </th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>                                        
                                    </tr>
                                    <?php
                                    foreach ($Estq_artigos as $e) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->codigo_livre, 'Estq_artigo', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->nome, 'Estq_artigo', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';                                       
                                        echo '<td>';
                                        echo $this->Html->getLink($e->getEstq_subgrupo()->nome, 'Estq_subgrupo', 'view',
                                            array('id' => $e->getEstq_subgrupo()->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink(number_format($e->vlcusto, 2, ',', '.'), 'Estq_artigo', 'view',
                                            array('id' => $e->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($e->getUnidade()->sigla, 'Estq_artigo', 'view',
                                            array('id' => $e->getUnidade()->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        
                                        echo '<td>';
                                        echo '<div class="dropdown">';
                                            echo '<button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                                <span class="caret"></span></button>';       
                                                echo '<ul class="dropdown-menu">';
                                                echo '<li>';
                                                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit" ></span> Editar', 'Estq_artigo', 'edit',
                                                    array('id' => $e->id),
                                                    array('class' => 'btn  btn-sm', 'data-toggle' => "tooltip", 'data-placement' => "bottom", 'title' => "Editar Artigos"));
                                                echo '</li>';
                                                echo '<li>';
                                                echo $this->Html->getLink('<i class="fa fa-indent" data-toggle="tooltip" data-placement="auto" title="Artigo - Lote"> </i>  Artigo - Lote', 'Estq_artigo_lote', 'all',
                                                    array('id' => $e->id, 'first' => 1, 'modal' => 1),
                                                    array('class' => 'btn  btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                                echo '</li>';
                                                echo '<li>';
                                                echo $this->Html->getLink('<i class="fa fa-list-ol" data-toggle="tooltip" data-placement="auto" title="Artigo - Lote Subestoque"> </i>  Artigo - Lote Subestoque', 'Estq_lote_substoque', 'all',
                                                    array('id' => $e->id, 'first' => 1, 'modal' => 1),
                                                    array('class' => 'btn  btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-lg'));
                                                echo '</li>';
                                                echo '<li>';
                                                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="auto" title="Excluir artigo"> </span> Excluir artigo', 'Estq_artigo', 'delete',
                                                    array('id' => $e->id),
                                                    array('class' => 'btn  btn-sm', 'data-toggle' => 'modal'));
                                                echo '</li>';

                                                echo '</tr>';
                                                echo '</ul>';
                                        echo '</div>';
                                    echo '</td>';  
                                        echo '</tr>';
                                    }
                                    ?>
                                </table>

                                <!-- menu de paginação -->
                                <div style="text-align:center">
                                    <?php echo $nav; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function () {

        $('.search').keyup(function () {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Estq_artigo', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    else
                        echo '"' . $this->Html->getUrl('Estq_artigo', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>