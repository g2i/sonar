<form class="form" method="post" action="<?php echo $this->Html->getUrl('Concessionaria_item_servico', 'delete') ?>">
    <h1>Confirmação</h1>
    <input name="id" value="<?= $Concessionaria_item_servico->id?>" type="hidden">
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro ?</p>
    </div>
    <div class="text-right">
        <a href="Javascript:void(0)" class="btn btn-default" <?php echo (!$this->getParam('modal'))?'data-dismiss="modal"': 'onclick="Navegar(\'\',\'back\')"'; ?>>Cancelar</a>
        <input type="submit" class="btn btn-danger" <?php echo (!$this->getParam('modal'))?'': 'onclick="EnviarFormularioItemServiço(\'form\')"'; ?> value="Excluir">
    
    </div>
</form>