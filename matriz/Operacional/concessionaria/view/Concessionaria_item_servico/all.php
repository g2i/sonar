<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Item Serviço</h2>

        <h2>OS:<?= $this->getParam('movimento') ?></h2>
        <ol class="breadcrumb">
            <li>Item Serviço</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- botao de cadastro -->
                    <?php

                    echo ' <div class="text-right">';
                    echo '<a href="' . $this->Html->getUrl('Concessionaria_item_servico', 'add', array("modal" => "1", 'first' => 1, "movimento" => $this->getParam('movimento'))) . '" style="color: #FFFFFF" role="button" data-toggle="modal">';
                    echo "<button class='btn btn-info' style='height: 34px' >";
                    echo "<span class='fa fa-plus-square'></span> <label class=''> Adicionar Serviço</label> </button></a>";
                    echo '</div>';
                    ?>
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>

                                    <th>
                                        <a
                                            href='#'>
                                            Serviço
                                        </a>
                                    </th>

                                    <th>
                                        <a
                                            href='#'>
                                            Fator
                                        </a>
                                    </th>
                                    <th class="text-left">
                                        <a
                                            href='#'>
                                            Qtd.
                                        </a>
                                    </th>

                                    <th class="text-left">
                                        <a
                                            href='#'>
                                            Valor Unitario.
                                        </a>
                                    </th>

                                    <th class="text-left">
                                        <a
                                            href='#'>
                                            Total.
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>
                                <?php

                                $totalMaximo = 0;
                                foreach ($Concessionaria_item_servicos as $c) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $c->getServico()->nome;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $c->getFator()->descricao;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $c->quantidade;
                                        echo '</td>';
                                        echo '<td>';
                                        echo number_format($c->valor_atual_conversao, 2, ',', '.');
                                        echo '</td>';
                                        echo '<td>';
                                        $totalMaximo += $c->valor_total;
                                        echo number_format($c->valor_total, 2, ',', '.');
                                        echo '</td>';

                                        echo '<td width="50">';
                                        echo '<a href="' . $this->Html->getUrl('Concessionaria_item_servico', 'edit', array("modal" => "1", "movimento" => $this->getParam('movimento'), 'id' => $c->id)) . '"  style="color: #FFFFFF" role="button" data-toggle="modal">';
                                        echo '<button class="btn btn-warning btn-sm">';
                                        echo '<span class="glyphicon glyphicon-edit"></span></button></a>';
                                        echo '</td>';
                                        echo '<td width="50">';
                                        echo '<a href="' . $this->Html->getUrl('Concessionaria_item_servico', 'delete', array("modal" => "1", "movimento" => $this->getParam('movimento'), 'id' => $c->id)) . '"  style="color: #FFFFFF" role="button" data-toggle="modal">';
                                        echo '<button class="btn btn-danger btn-sm">';
                                        echo '<span class="glyphicon glyphicon-remove"></span></button></a>';
                                        echo '</td>';
                                        echo '</tr>';
                                }
                                ?>
                                <tfoot>
                                <tr>
                                    <td colspan="3"></td>
                                    <td style="font-weight: bold">Total:</td>

                                    <td><?php echo number_format($totalMaximo, 2, ',', '.'); ?></td>
                                    <td colspan="3"></td>
                                </tr>
                                </tfoot>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        function EnviarFormularioItemServiço(form) {
                            $(form).ajaxForm({
                                success: function (d) {
                                    Navegar('', 'back');
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                            });

                        }
                        $(document).ready(function () {
                            $('#modal').on('hidden.bs.modal',function(){
                                location.reload();
                            });
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Concessionaria_item_servico', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Concessionaria_item_servico', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>