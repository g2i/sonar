<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2>Serviços Adicionais</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="formAdicional">
                        <?php echo '<input type="hidden" name="estq_mov_id" value="' . $this->getParam('movimento') . '">'; ?>
                        <input type="hidden" name="id" id="id" value="">
                        <input type="hidden" name="adicional_servico" value="1.3">
                        <input type="hidden" name="item_pai" value="<?= $this->getParam('id') ?>">
                        <input type="hidden" name="usar_adicional" value="1">

                                    <?php ///var_dump($Concessionaria_servicosCodigo);exit; ?>
                        <div class="well well-lg">
                            <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                <label class="required" for="servico_id">Código <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="codigo_servico" name="servico_id_cod" class="form-control select2-cod">
                                    <option value="">Selecione um serviço</option>
                                    <?php
                                    foreach ($Concessionaria_servicosCodigo as $e) {
                                        echo '<option value="' . $e->id . '">' . $e->codigo_livre . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-6 col-sm-6 col-xs-6">
                                <label for="quantidade">Quantidade</label>
                                <input type="number" name="quantidade" id="quantidade" step="any" class="form-control"
                                       value="1"
                                       placeholder="Quantidade">
                            </div>

                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label class="required" for="servico_id"> Descrição: <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="servico_id" name="servico_id" class="form-control select2-cod">
                                    <option value="">Selecione um serviço</option>
                                    <?php
                                    foreach ($Concessionaria_servicosCodigo as $e) {
                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="text-right">
                                <input type="button" id="editarAdicional" class="btn btn-warning hidden" value="Editar">
                                <input type="button" id="salvarAdicional" class="btn btn-primary" value="Incluir">
                            </div>
                            <div class="clearfix">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                <a
                                                    href='#'>
                                                    Serviço
                                                </a>
                                            </th>

                                            <th class="text-right">
                                                <a
                                                    href='#'>
                                                    US
                                                </a>
                                            </th>
                                            <th class="text-right">
                                                <a
                                                    href='#'>
                                                    Qtd.
                                                </a>
                                            </th>

                                            <th class="text-right">
                                                <a
                                                    href='#'>
                                                    Valor US.
                                                </a>
                                            </th>
                                            <th class="text-right">
                                                <a
                                                    href='#'>
                                                    Total sem adicional.
                                                </a>
                                            </th>
                                            <th class="text-right">
                                                <a
                                                    href='#'>
                                                    Adicional %.
                                                </a>
                                            </th>
                                            <th class="text-right">
                                                <a
                                                    href='#'>
                                                    Total com adicional.
                                                </a>
                                            </th>
                                            <th class="text-right">
                                                <a
                                                    href='#'>
                                                    Diferança.
                                                </a>
                                            </th>
                                            <th>&nbsp;</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tableAdicional">
                                        <?php
                                        $totalMaximo = 0;
                                        foreach ($Concessionaria_item_servicos as $c) {
                                           // var_dump($c);
                                           // exit;
                                            echo '<tr>';
                                            echo '<td>';
                                            echo $c->getServico()->nome;
                                            echo '</td>';
                                            echo '<td class="text-right">';
                                            if ($c->us == 1)
                                                echo number_format($c->fator_us, 2, ',', '.');
                                            else
                                                echo '----';
                                            echo '</td>';
                                            echo '<td class="text-right">';
                                            echo $c->quantidade;
                                            echo '</td>';
                                            echo '<td class="text-right">';
                                            echo number_format($c->valor_atual_conversao, 2, ',', '.');
                                            echo '</td>';
                                            echo '<td class="text-right">';
                                            echo number_format($c->valor_total_sem_adicional, 2, ',', '.');
                                            echo '</td>';
                                            echo '<td class="text-right">';
                                            echo (($c->adicional_servico * 100) - 100) . '%';
                                            echo '</td>';
                                            echo '<td class="text-right">';
                                            $totalMaximo += $c->valor_total_diferenca_item_filho;
                                            echo number_format($c->valor_total_item_filho, 2, ',', '.');
                                            echo '</td>';
                                            echo '<td class="text-right">';
                                            echo number_format($c->valor_total_diferenca_item_filho, 2, ',', '.');
                                            echo '</td>';
                                            /*
                                            echo '<td width="50">';
                                            echo '<a href="' . $this->Html->getUrl('Concessionaria_item_servico', 'edit', array("modal" => "1", "movimento" => $this->getParam('movimento'), 'id' => $c->id)) . '"  style="color: #FFFFFF" role="button" data-toggle="modal">';
                                            echo '<button class="btn btn-warning btn-sm">';
                                            echo '<span class="glyphicon glyphicon-edit"></span></button></a>';
                                            echo '</td>';
                                            */
                                            echo '<td width="50">';
                                            echo '<a href="Javascript:void(0)" class="btn btn-warning btn-sm edit-adicional" data-id="'.$c->id.'" 
                                            data-servico_id="'.$c->servico_id.'" data-quantidade="'.$c->quantidade.'" data-valor_atual_conversao="'.$c->valor_atual_conversao.'" data-servico_nome="'.$c->getServico()->nome.'"  >';
                                            echo '<span class="glyphicon glyphicon-edit"></span></a>';
                                            echo '</td>';
                                            echo '<td width="50">';
                                            echo '<a href="Javascript:void(0)" class="btn btn-danger btn-sm delete-adicional" data-id="'.$c->id.'">';
                                            echo '<span class="glyphicon glyphicon-remove"></span></a>';
                                            echo '</td>';
                                            echo '</tr>';
                                        }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="6"></td>
                                            <td style="font-weight: bold">Total:</td>
                                            <td id="totalItemPai"  class="text-right"><?php echo number_format($totalMaximo, 2, ',', '.'); ?></td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                    <!-- menu de paginação -->
                                    <div style="text-align:center"><?php echo $nav; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                                <a href="Javascript:void(0)" data-dismiss="modal" class="btn btn-default">Concluir</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function deleteAdicional(id){
        var url = '<?=$this->Html->getUrl('Concessionaria_item_servico', 'post_delete_adicional');?>';
        var estq_mov = <?= $this->getParam('movimento') ?>;
        var item_pai = <?= $this->getParam('id') ?>;
        $.post(url,{
            estq_mov_id:estq_mov,
            item_pai:item_pai,
            id:id
        },function(data){
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_SUCCESS,
                message:data.msg
            });
            $('#totalItemPai').html(data.total);
        },'json').fail(function(xhr,status){
            BootstrapDialog.alert({
                type: BootstrapDialog.TYPE_DANGER,
                message:xhr.responseJSON.msg
            });
        });
    }

    $(function () {
        $('.delete-adicional').on('click',function(ev){
            $(this).parent().parent().remove();
            var id = $(this).attr('data-id');
            deleteAdicional(id);
        });
        
        $('.edit-adicional').on('click',function(ev){
            $('#editarAdicional').removeClass('hidden');
            $('#salvarAdicional').addClass('hidden');

            var estq_mov = <?= $this->getParam("movimento") ?>;
            var id = $(this).attr('data-id');
            var servico_id = $(this).attr('data-servico_id');
            var servico_nome = $(this).attr('data-servico_nome');
            var valor_atual_conversao = $(this).attr('data-valor_atual_conversao');
            var quantidade = $(this).attr('data-quantidade');
                       
            $('#id').val(id);
            $("#servico_id").select2("val", servico_id);
            $("#codigo_servico").select2("val", servico_id);
            $('#quantidade').val(quantidade);            
        });
        

        $('.select2-cod').select2({
            language: 'pt-BR',
            width: "100%",
            theme: 'bootstrap',
            placeholder: {
                id: '',
                text: 'Selecione um serviço'
            }
        }).on('select2:select', function (ev) {
            var id = ev.params.data.id;
            var url = '<?=$this->Html->getUrl('Concessionaria_servico', 'search_servico')?>';
            $.get(url,
                {
                    id: id
                },
                function (data) {
                    $('.select2-cod').val(id).trigger('change');
                    $('#valor_atual_conversao').val(data.valor_us);
                    if (data.us == 1) {
                        $('#div_fator_us').show();
                        $('#fator_us').val(data.fator_us);
                    } else {
                        $('#div_fator_us').hide();
                    }
                }, 'json');
        });

                    
        $('#salvarAdicional, #editarAdicional').on('click', function (ev) {
            var url = '<?=$this->Html->getUrl('Concessionaria_item_servico', 'post_add_servico_adicional');?>';
            var formData = $('#formAdicional').serializeObject();

            $.post(url, formData,
                function (data) {
                    $('#codigo_servico').val('').trigger('change');
                    $('#servico_id').val('').trigger('change');
                    $('#quantidade').val(1);
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_SUCCESS,
                        message:data.msg
                    });
                    setTimeout(function(){ location.reload(); }, 2000);                
                    var html= '<tr>';
                    html += "<td>"+data.nome+"</td>";
                    html += "<td>"+data.fator_us+"</td>";
                    html += "<td>"+data.quantidade+"</td>";
                    html += "<td>"+data.valor_atual_conversao+"</td>";
                    html += "<td>"+data.valor_total_sem_adicional+"</td>";
                    html += "<td>"+data.adicional_servico+"</td>";
                    html += "<td>"+data.valor_total_item_filho+"</td>";
                    html += "<td>"+data.valor_total_diferenca_item_filho+"</td>";
                    html += "<td width='50'><a class='btn btn-danger btn-sm delete-adicional' data-id='"+data.id+"'><span class='glyphicon glyphicon-remove'></span></a></td>";
                    html +='</tr>';
                    $('#tableAdicional').append(html);
                    $('#totalItemPai').html(data.total);
                    $('.delete-adicional').unbind('click').bind('click',function(ev){
                        $(this).parent().parent().remove();
                        var id = $(this).attr('data-id');
                        deleteAdicional(id);
                    });
                },'json')
                .fail(function (xhr, status) {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        message:xhr.responseJSON.msg
                    });
                });
        });
                    
    });

</script>
 
<style>
    .ibox-content {
        border-width: 0px 0;
    }

    .modal-dialog {
        height: 800px;
        width: 1000px;
    }

    .select2-container--open {
        z-index: 9999
    }

    .select2-search,
    .select2-search__field,
    .select2-container,
    .select2-dropdown {
        z-index: 1030 !important;
    }
</style>