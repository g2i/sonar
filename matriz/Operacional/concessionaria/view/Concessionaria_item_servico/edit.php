<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Item Serviço</h2>
        <ol class="breadcrumb">
            <li>Item Serviço</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Concessionaria_item_servico', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label class="required" for="servico_id">Código<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="codigo_servico" name="servico_id_cod" class="form-control select2-cod">
                                    <option value="">Selecione um serviço</option>
                                    <?php
                                    foreach ($Concessionaria_servicosCodigo as $e) {
                                        if ($e->id == $Concessionaria_item_servico->servico_id)
                                            echo '<option selected value="' . $e->id . '">' . $e->codigo_livre . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->codigo_livre . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label class="required" for="servico_id"> Descrição: <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="servico_id" name="servico_id" class="form-control select2-cod">
                                    <option value="">Selecione um serviço</option>
                                    <?php
                                    foreach ($Concessionaria_servicosCodigo as $e) {
                                        if ($e->id == $Concessionaria_item_servico->servico_id)
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="quantidade">Quantidade</label>
                                <input type="number" name="quantidade" step="any" id="quantidade" class="form-control"
                                       value="<?php echo $Concessionaria_item_servico->quantidade; ?>"
                                       placeholder="Quantidade">
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="valor_atual_conversao">Valor Unitario</label>
                                <input type="text" name="valor_atual_conversao" id="valor_atual_conversao"
                                       class="form-control"
                                       value="<?php echo number_format($Concessionaria_item_servico->valor_atual_conversao, 2, '.', ','); ?>"
                                       >
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Concessionaria_item_servico->id; ?>">


                        <div class="text-right">

                            <input id="editar" type="submit"
                                   class="btn btn-primary" <?php echo (!$this->getParam('modal')) ? '' : 'onclick="EnviarFormularioItemServiço(\'form\')"'; ?>
                                   value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


    $(function () {
        $('#usar_adicional').on('change',function(ev){
            if($(this).val() == 1){
                $('#div_adicional').show();
            }else{
                $('#div_adicional').hide();
            }
        });

        $('.select2-cod').select2({
            language: 'pt-BR',
            width: "100%",
            theme: 'bootstrap',
            placeholder: {
                id: '',
                text: 'Selecione um servico'
            }
        }).on('select2:select', function (ev) {
            var id = ev.params.data.id;
            $.get('<?=$this->Html->getUrl('Concessionaria_servico', 'search_servico')?>',
                {
                    id: id
                },
                function (data) {
                    $('.select2-cod').val(id).trigger('change');
                }, 'json');
        });

    });

</script>
<style>
    .select2-container--open {
        z-index: 9999
    }

    .select2-search,
    .select2-search__field,
    .select2-container,
    .select2-dropdown {
        z-index: 9999 !important;
    }
</style>
