
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo de Anexos</h2>
        <ol class="breadcrumb">
            <li>Tipo de Anexos</li>
            <li class="active">
                <strong>Adicionar Tipo</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('TipoAnexos', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $TipoAnexos->nome ?>" placeholder="Nome" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="extensao">Extensão <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="extensao" id="extensao" class="form-control"
                                       value="<?php echo $TipoAnexos->extensao ?>" placeholder="Extensão" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $TipoAnexos->id;?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('TipoAnexos', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
