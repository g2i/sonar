<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo de Anexos</h2>
        <ol class="breadcrumb">
            <li>Tipo de Anexos</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Tipo', 'TipoAnexos', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('TipoAnexos', 'all', array('orderBy' => 'nome')); ?>'>
                                            Nome
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('TipoAnexos', 'all', array('orderBy' => 'extensao')); ?>'>
                                            Extensão
                                        </a>
                                    </th>

                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('TipoAnexos', 'all', array('orderBy' => 'created')); ?>'>
                                            Dt Cadastro
                                        </a>
                                    </th>

                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('TipoAnexos', 'all', array('orderBy' => 'modified')); ?>'>
                                            Dt Modificação
                                        </a>
                                    </th>


                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($TipoAnexos as $a) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($a->nome, 'TipoAnexos', 'view',
                                        array('id' => $a->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($a->extensao, 'TipoAnexos', 'view',
                                        array('id' => $a->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataTimeBr($a->created), 'TipoAnexos', 'view',
                                        array('id' => $a->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataTimeBr($a->modified), 'TipoAnexos', 'view',
                                        array('id' => $a->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td class="actions">';
                                    echo $this->Html->getLink('<i class="fa fa-pencil"></i> ', 'TipoAnexos', 'edit',
                                        array('id' => $a->id),
                                        array('class' => 'btn btn-info btn-xs'));
                                    echo $this->Html->getLink('<i class="fa fa-remove"></i> ', 'TipoAnexos', 'delete',
                                        array('id' => $a->id),
                                        array('class' => 'btn btn-danger btn-xs', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('TipoAnexos', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('TipoAnexos', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>