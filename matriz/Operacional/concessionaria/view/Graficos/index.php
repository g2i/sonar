<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Gráficos</h2>
        <ol class="breadcrumb">
            <li>análise</li>
            <li class="active">
                <strong>gráfica</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Gráfico dos últimos 6 meses</h5>
                </div>
                <!-- formulario de pesquisa -->
                <div class="ibox-content">
                    <div class="col-md-12">
                        <canvas id="analise" ></canvas>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var ctx = document.getElementById("analise");
        var dados = '<?= $dados ?>';
            dados = JSON.parse(dados);
        var labels= '<?= $labels ?>';
            labels = JSON.parse(labels);
    console.log('<?= $dados ?>');
    console.log('<?= $labels ?>');
    var data = {
        datasets: [dados],
        labels: labels
    };
    var myChart = new Chart(ctx, {
        type: 'bar',
        data:data,
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>