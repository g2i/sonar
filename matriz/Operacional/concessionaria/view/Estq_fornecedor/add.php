<div class="row wrapper border-bottom white-bg page-heading" xmlns="http://www.w3.org/1999/html">
    <div class="col-lg-9">
        <h2>Fornecedor</h2>
        <ol class="breadcrumb">
            <li>Fornecedor</li>
            <li class="active">
                <strong>Adicionar Fornecedor</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Estq_fornecedor', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>

                        <div id="wizard" class="wizard-big">
                            <h1>Dados do Fornecedor</h1>
                            <section>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="cep">Selecione o Tipo de Fornecedor<span class="glyphicon glyphicon-asterisk"></span></label>
                                <div class="radio required">
                                    <label>
                                        <input type="radio" name="tipo" id="tipo" role="tipo" value="fisica" required>
                                        Pessoa Fisica
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="tipo" id="tipo" role="tipo" value="juridica">
                                        Pessoa Jurídica
                                    </label>
                                </div>
                                    </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12 fisica" style="display: none">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="nome" id="nome" class="form-control"
                                           value="<?php echo $Estq_fornecedor->nome ?>" placeholder="Nome" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12 fisica" style="display: none">
                                    <label for="cpf">CPF</label>
                                    <input type="text" name="cpf" id="cpf" class="form-control cpf"
                                           value="<?php echo $Estq_fornecedor->cpf ?>" placeholder="CPF" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12 fisica" style="display: none">
                                    <label for="rg">RG</label>
                                    <input type="text" name="rg" id="rg" class="form-control"
                                           value="<?php echo $Estq_fornecedor->rg ?>" placeholder="RG" required>
                                </div>

                                <div class="form-group col-md-4 col-sm-6 col-xs-12 juridica" style="display: none">
                                    <label for="razao_social">Razão Social</label>
                                    <input type="text" name="razao_social" id="razao_social" class="form-control"
                                           value="<?php echo $Estq_fornecedor->razao_social ?>"
                                           placeholder="Razão Social" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12 juridica" style="display: none">
                                    <label for="cnpj">CNPJ</label>
                                    <input type="text" name="cnpj" id="cnpj" class="form-control cnpj"
                                           value="<?php echo $Estq_fornecedor->cnpj ?>" placeholder="CNPJ" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12 juridica" style="display: none">
                                    <label for="insc_estadual">Inscrição Estadual</label>
                                    <input type="text" name="insc_estadual" id="insc_estadual" class="form-control"
                                           value="<?php echo $Estq_fornecedor->insc_estadual ?>"
                                           placeholder="Inscrição Estadual">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12 juridica" style="display: none">
                                    <label for="insc_municipal">Inscrição Municipal</label>
                                    <input type="text" name="insc_municipal" id="insc_municipal" class="form-control"
                                           value="<?php echo $Estq_fornecedor->insc_municipal ?>"
                                           placeholder="Inscrição Municipal">
                                </div>
                                <div class="clearfix"></div>

                                <script>
                                    $(function () {

                                        $("input[name='tipo']").click(function () {
                                            if ($(this).val() == 'fisica') {
                                                $(".fisica").show(500);
                                                $(".juridica").hide(500);
                                            }
                                            else if ($(this).val() == 'juridica') {
                                                $(".juridica").show(500);
                                                $(".fisica").hide(500);
                                            }

                                        })
                                    });
                                </script>
                            </section>
                            <h1>Endereço do Fornecedor</h1>
                            <section>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="cep">CEP<span class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" role="cep" cep-init="LoadGif" cep-done="CloseGif" name="cep"
                                           id="cep" class="form-control cep" data-toggle="tooltip"
                                           title="Informe o CEP - Preenchimento automático"
                                           value="<?php echo $Estq_fornecedor->cep ?>" placeholder="CEP" required>
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="endereco">Endereço</label>
                                    <input type="text" name="endereco" id="endereco" data-cep="endereco"
                                           class="form-control endereco"
                                           value="<?php echo $Estq_fornecedor->endereco ?>" placeholder="Endereco">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="numero">Numero</label>
                                    <input type="number" name="numero" id="numero" class="form-control"
                                           value="<?php echo $Estq_fornecedor->numero ?>" placeholder="Numero">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="complemento">Complemento</label>
                                    <input type="text" name="complemento" id="complemento" class="form-control"
                                           value="<?php echo $Estq_fornecedor->complemento ?>"
                                           placeholder="Complemento">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="bairro">Bairro</label>
                                    <input type="text" name="bairro" id="bairro" data-cep="bairro"
                                           class="form-control bairro" value="<?php echo $Estq_fornecedor->bairro ?>"
                                           placeholder="Bairro">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="cidade">Cidade</label>
                                    <input type="text" name="cidade" id="cidade" data-cep="cidade"
                                           class="form-control cidade" value="<?php echo $Estq_fornecedor->cidade ?>"
                                           placeholder="Cidade">
                                </div>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="estado">Estado</label>
                                    <input type="text" name="estado" id="estado" data-cep="uf"
                                           class="form-control estado" value="<?php echo $Estq_fornecedor->estado ?>"
                                           placeholder="Estado">
                                </div>
                                <div class="clearfix"></div>
                            </section>
                            <h1>Contato/Dados Bancários</h1>
                            <section>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="email">E-mail</label>
                                    <input type="email" name="email" id="email" class="form-control"
                                           value="<?php echo $Estq_fornecedor->email ?>" placeholder="Email">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="contato">Contato</label>
                                    <textarea name="contato" id="contato" class="form-control" rows="4"><?php echo $Estq_fornecedor->contato ?></textarea>
                                </div>
                                <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                    <label for="banco">Banco</label>
                                    <input type="text" name="banco" id="banco" class="form-control"
                                           value="<?php echo $Estq_fornecedor->banco ?>" placeholder="Banco">
                                </div>
                                <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                    <label for="bancoagencia">Agência</label>
                                    <input type="text" name="bancoagencia" id="bancoagencia" class="form-control"
                                           value="<?php echo $Estq_fornecedor->bancoagencia ?>"
                                           placeholder="Agência">
                                </div>
                                <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                    <label for="bancoconta">Conta</label>
                                    <input type="text" name="bancoconta" id="bancoconta" class="form-control"
                                           value="<?php echo $Estq_fornecedor->bancoconta ?>" placeholder="Conta">
                                </div>
                                <input type="hidden" name="situacao" value="1">
                                <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                    <label for="bancooperacao">Operação</label>
                                    <input type="text" name="bancooperacao" id="bancooperacao" class="form-control"
                                           value="<?php echo $Estq_fornecedor->bancooperacao ?>"
                                           placeholder="Operação">
                                </div>
                                <div class="clearfix"></div>
                            </section>
                        </div>


                    </form>


                </div>

            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        var form = $("form");
        form.validate({

        });
        $("#wizard").steps({
            headerTag: "h1",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {

                    $('form').submit();

            }
        });
        $('[data-toggle="tooltip"]').tooltip();
        $('#cpf').blur(function () {
            if (!validarCPF($(this).val())) {
                OpenDialog("Alerta!", "CPF inválido !");
                $(this).val('')
            }
        })
    });

</script>