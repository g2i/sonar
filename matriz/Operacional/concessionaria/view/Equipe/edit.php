<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Codigo Equipe</h2>
        <ol class="breadcrumb">
            <li>Codigo Equipe</li>
            <li class="active">
                <strong>Editar Equipe</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Equipe', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="nome">Equipe <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Equipe->nome ?>" placeholder="Equipe" required>
                            </div>

                            <input type="hidden" name="origem" value="<?php echo $Equipe->origem; ?>"  >
                            <input type="hidden" name="data_cadastro" value="<?php echo $Equipe->data_cadastro; ?>"  >
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="situacao">Situação</label>
                                <select name="situacao" class="form-control" id="situacao">
                                    <?php
                                    foreach ($Situacaos as $s) {
                                        if ($s->id == $Equipe->situacao)
                                            echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                                        else
                                            echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Equipe->id; ?>">

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Equipe', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>