<?php if($this->getParam('modal')){ ?>
    <?php }else{ ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lote Subestoque</h2>
        <ol class="breadcrumb">
            <li>Lote Subestoque</li>
            <li class="active">
                <strong>Listagem de Lote Subestoque</strong>
            </li>
        </ol>
    </div>
</div>
<?php } ?>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <?php if($this->getParam('modal')){ ?>
                       <!-- aqui vai o nome do artigo <div class="text-left col-md-6 ">
                            <h3 class="text-left"></h3>
                        </div>
                        -->
                        <div class="text-right">

                            <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Estq_lote_substoque","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                                    <span class="img img-add"></span> Novo Lote Subestoque</a></p>'
                        </div>
                    <?php }else{ ?>
                <!-- formulario de pesquisa -->
                <div class="filtros well">
                    <div class="form">
                        <form role="form"
                              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                              method="post" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                            <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                            <div class="col-md-3 form-group">
                                <label for="lote">Lote</label>
                                <select name="filtro[externo][lote]" class="form-control" id="lote">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Estq_artigo_lotes as $e): ?>
                                        <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="artigo">Artigo</label>
                                <select name="filtro[externo][artigo]" class="form-control" id="artigo">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Estq_artigos as $e): ?>
                                        <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="situacao">Situação</label>
                                <select name="filtro[externo][situacao]" class="form-control" id="situacao">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Estq_situacaos as $e): ?>
                                        <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-default botao-impressao" data-toggle="tooltip" data-placement="bottom" title="Impressão"><span
                                        class="glyphicon glyphicon-print"></span></button>
                                <a href="<?php echo $this->Html->getUrl('Estq_lote_substoque', 'all') ?>"
                                   class="btn btn-default"
                                   data-dismiss="modal" data-toggle="tooltip" data-placement="bottom" title="Recarregar a página"><span
                                        class="glyphicon glyphicon-refresh "></span></a>
                                <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pesquisar"><span
                                        class="glyphicon glyphicon-search"></span></button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Lote Subestoque', 'Estq_lote_substoque', 'add', NULL, array('class' => 'btn btn-primary','data-tool'=>"tooltip",'data-placement'=>"bottom", 'title'=>"Cadastrar Lote Subestoque")); ?></p>
                        </div>

                    <?php } ?>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_lote_substoque', 'all', array('orderBy' => 'lote')); ?>'>
                                            Lote
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_lote_substoque', 'all', array('orderBy' => 'artigo')); ?>'>
                                            Artigo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_lote_substoque', 'all', array('orderBy' => 'estoque_minimo')); ?>'>
                                            Estoque Mínimo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_lote_substoque', 'all', array('orderBy' => 'quantidade')); ?>'>
                                            Quantidade
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_lote_substoque', 'all', array('orderBy' => 'subestoque')); ?>'>
                                            Sub-Estoque
                                        </a>
                                    </th>

                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Estq_lote_substoques as $e) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->getEstq_artigo_lote()->nome, 'Estq_artigo_lote', 'view',
                                        array('id' => $e->getEstq_artigo_lote()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->getEstq_artigo()->nome, 'Estq_artigo', 'view',
                                        array('id' => $e->getEstq_artigo()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->estoque_minimo, 'Estq_lote_substoque', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->quantidade, 'Estq_lote_substoque', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->getEstq_subestoque()->nome, 'Estq_subestoque', 'view',
                                        array('id' => $e->getEstq_subestoque()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                if($this->getParam('modal')==1) {
                                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar Lote Subestoque" class="btn  btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Estq_lote_substoque", "edit", array("ajax" => true, "modal" => "1", 'id' => $e->id, 'artigo' => $e->id)) . '\',\'go\')">
    <span class="glyphicon glyphicon-edit"></span></a>';
                                    echo '</td>';
                                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Excluir Lote Subestoque" class="btn  btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Estq_lote_substoque", "delete", array("ajax" => true, "modal" => "1", 'id' => $e->id, 'artigo' => $e->getEstq_artigo()->id)) . '\',\'go\')">
    <span class="glyphicon glyphicon-remove"></span></a>';
                                    echo '</td>';
                                }else {
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Estq_lote_substoque', 'edit',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-warning btn-sm','data-tool'=>"tooltip",'data-placement'=>"bottom", 'title'=>"Editar"));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_lote_substoque', 'delete',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal','data-tool'=>"tooltip",'data-placement'=>"bottom", 'title'=>"Deletar"));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        /* faz a pesquisa com ajax */
        $(document).ready(function () {
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Estq_lote_substoque', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Estq_lote_substoque', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });
        });
    </script>