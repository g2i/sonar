<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexos</h2>
        <ol class="breadcrumb">
            <li>Anexos</li>
            <li class="active">
                <strong>Adicionar Anexos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" id="form-anexos" role="form" action="<?php echo $this->Html->getUrl('Anexos', 'add') ?>" enctype="multipart/form-data">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                <label for="tipo_id">Tipo</label>
                                <select name="tipo_id" class="form-control" id="tipo_id">
                                    <?php
                                    foreach ($Tipo_anexos as $t) {
                                        if ($t->id == $Anexos->tipo_id)
                                            echo '<option selected value="' . $t->id . '">' . $t->nome . '</option>';
                                        else
                                            echo '<option value="' . $t->id . '">' . $t->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="files">Anexos</label>
                                    <input id="files" type="file" name="caminho[]" multiple class="input-file"
                                           data-overwrite-initial="false" data-min-file-count="1">
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="descricao">Descrição</label>
                                <textarea name="descricao" id="descricao"
                                          class="form-control"><?php echo $Anexos->descricao ?></textarea>
                            </div>
                            <div class="clearfix"></div>

                            <div class="progress" style="display: none;">
                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span class="sr-only">0% Complete</span>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" id="estq_mov_id" name="estq_mov_id"  value="<?= $this->getParam('id') ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Anexos', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar" onclick="senduploads('form-anexos')">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        initeComponentes(document)
    })
</script>