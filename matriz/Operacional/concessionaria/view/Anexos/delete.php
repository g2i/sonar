<form class="form" method="post" action="<?php echo $this->Html->getUrl('Anexos', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir este(a) <strong><?php echo $Anexos->getTipo_anexos()->nome; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Anexos->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Anexos', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>