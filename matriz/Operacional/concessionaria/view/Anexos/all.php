<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexos</h2>
        <ol class="breadcrumb">
            <li>Anexos</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="GET" >
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="tipo_id">Tipo</label>
                                    <select name="tipo_id" class="form-control" id="tipo_id">
                                        <option  value="">Selecione</option>
                                        <?php
                                        foreach ($Tipo_anexos as $t) {
                                            if ($t->id == $this->getParam('tipo_id'))
                                                echo '<option selected value="' . $t->id . '">' . $t->nome . '</option>';
                                            else
                                                echo '<option value="' . $t->id . '">' . $t->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label for="nome">Descrição</label>
                                    <input type="text" name="descricao" id="nome" class="form-control"
                                           value="<?php echo $this->getParam('descricao'); ?>">
                                </div>

                                <div class="col-md-12 text-right">
                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>" class="btn btn-default"
                                    data-dismiss="modal" data-toggle="tooltip" data-placement="bottom" title="Recarregar a página"><span
                                        class="glyphicon glyphicon-refresh "></span></a>
                                    <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                            data-placement="bottom" title="Pesquisar"><span
                                                class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Anexo', 'Anexos', 'add', array('id' =>  $this->getParam('id')), array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Anexos', 'all', array('orderBy' => 'tipo_id')); ?>'>
                                            Tipo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Anexos', 'all', array('orderBy' => 'descricao')); ?>'>
                                            Descrição
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Anexos', 'all', array('orderBy' => 'caminho')); ?>'>
                                            Arquivo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Anexos', 'all', array('orderBy' => 'created')); ?>'>
                                            Dt Cadastro
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Anexos', 'all', array('orderBy' => 'modified')); ?>'>
                                            Dt Modificação
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Anexos as $a) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($a->getTipo_anexos()->nome, 'TipoAnexos', 'view',
                                        array('id' => $a->getTipo_anexos()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($a->descricao, 'Anexos', 'view',
                                        array('id' => $a->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    $ext = explode('.', $a->caminho);
                                    if (in_array($ext[count($ext)-1], ['jpg', 'gif', 'png'])) { ?>
                                        <a href="<?= SITE_PATH.'/' . $a->caminho ?>" data-gallery="gal">
                                            <img src="<?= SITE_PATH.'/' . $a->caminho ?>"  class="img-responsive" style="max-width: 100px"/>
                                        </a>
                                    <?php
                                    }else{
                                        echo '<i class="fa fa-file"></i>';
                                    }
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(ConvertDateTimeBR($a->created), 'Anexos', 'view',
                                        array('id' => $a->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(ConvertDateTimeBR($a->modified), 'Anexos', 'view',
                                        array('id' => $a->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td class="actions">';
                                    echo $this->Html->getLink('<i class="fa fa-cloud-download"></i>', 'Anexos', 'download',
                                        array('id' => $a->id),
                                        array('class' => 'btn btn-primary btn-xs'));

                                    echo $this->Html->getLink('<i class="fa fa-pencil"></i> ', 'Anexos', 'edit',
                                        array('id' => $a->id),
                                        array('class' => 'btn btn-info btn-xs'));
                                    echo $this->Html->getLink('<i class="fa fa-remove"></i> ', 'Anexos', 'delete',
                                        array('id' => $a->id),
                                        array('class' => 'btn btn-danger btn-xs', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>
                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev" listen="f">‹</a>
                                <a class="next" listen="f">›</a>
                                <a class="close" listen="f">×</a>
                                <a class="play-pause" listen="f"></a>
                                <ol class="indicator"></ol>
                            </div>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            initeComponentes(document)
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>