<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
                <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">

                <div class="ibox-content no-borders">
                <?php if($this->getParam('modal')){ ?>
                    <div class="text-right">

                        <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Estq_artigo_lote","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')" data-toggle="tooltip" data-placement="bottom" title="Novo lote">
                                <span class="img img-add"></span> Novo Lote</a></p>'
                    </div>
                <?php }else{ ?>
                <!-- formulario de pesquisa -->
                <div class="filtros well">
                    <div class="form">
                        <form role="form"
                              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                              method="post" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                            <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                            <div class="col-md-3 form-group">
                                <label for="nome">Nome</label>
                                <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                       value="<?php echo $this->getParam('nome'); ?>">
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="artigo">Artigo</label>
                                <select name="filtro[externo][artigo]" class="form-control" id="artigo">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Estq_artigos as $e): ?>
                                        <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="validade">Validade</label>
                                <input type="date" name="filtro[interno][validade]" id="validade"
                                       class="form-control maskData" value="<?php echo $this->getParam('validade'); ?>">
                            </div>
                            <div class="col-md-3 form-group">
                                <label for="situacao">situação</label>
                                <select name="filtro[externo][situacao]" class="form-control" id="situacao">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Estq_situacaos as $e): ?>
                                        <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-default botao-impressao" data-toggle="tooltip" data-placement="bottom" title="Impressão"><span
                                        class="glyphicon glyphicon-print"></span></button>
                                <button type="button" class="btn btn-default botao-reset" data-toggle="tooltip" data-placement="bottom" title="Recarregar a página"><span
                                        class="glyphicon glyphicon-refresh"></span></button>
                                <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pesquisar"><span
                                        class="glyphicon glyphicon-search"></span></button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <?php } ?>
                <div>
                    <?php if($this->getParam('modal')){ ?>


                    <?php }else{ ?>
                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Lote', 'Estq_artigo_lote', 'add', NULL, array('class' => 'btn btn-primary','data-toggle'=>"tooltip" ,'data-placement'=>"bottom", 'title'=>"Novo Lote")); ?></p>
                    </div>
                    <?php } ?>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_artigo_lote', 'all', array('orderBy' => 'nome')); ?>'>
                                            Nome
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_artigo_lote', 'all', array('orderBy' => 'artigo')); ?>'>
                                            Artigo
                                        </a>
                                    </th>

                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_artigo_lote', 'all', array('orderBy' => 'validade')); ?>'>
                                            Validade
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Estq_artigo_lote', 'all', array('orderBy' => 'situacao')); ?>'>
                                            Situação
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Estq_artigo_lotes as $e) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->nome, 'Estq_artigo_lote', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->getEstq_artigo()->nome, 'Estq_artigo', 'view',
                                        array('id' => $e->getEstq_artigo()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($e->validade), 'Estq_artigo_lote', 'view',
                                        array('id' => $e->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($e->getEstq_situacao()->nome, 'Estq_situacao', 'view',
                                        array('id' => $e->getEstq_situacao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                if($this->getParam('modal')==1) {
                                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar Lote-Artigo" class="btn  btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Estq_artigo_lote", "edit", array("ajax" => true, "modal" => "1", 'id' => $e->id,'artigo'=>$e->getEstq_artigo()->id)).'\',\'go\')">
    <span class="glyphicon glyphicon-edit"></span></a>';
                                    echo '</td>';
                                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar Lote-Artigo" class="btn  btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Estq_artigo_lote", "delete", array("ajax" => true, "modal" => "1", 'id' => $e->id,'artigo'=>$e->getEstq_artigo()->id)).'\',\'go\')">
    <span class="glyphicon glyphicon-remove"></span></a>';
                                    echo '</td>';
                                }else{

                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Estq_artigo_lote', 'edit',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-warning btn-sm','data-toggle'=>"tooltip" ,
'data-placement'=>"bottom", 'title'=>"Editar"));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_artigo_lote', 'delete',
                                        array('id' => $e->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal','data-tool'=>"tooltip",'data-placement'=>"bottom", 'title'=>"Deletar"));
                                    echo '</td>';
                                    echo '</tr>';}
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>

    <script>
        //toltip
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
        //fim toltip
        /* faz a pesquisa com ajax */
        $(document).ready(function () {
            $('#search').keyup(function () {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                        <?php
                        if (isset($_GET['orderBy']))
                            echo '"' . $this->Html->getUrl('Estq_artigo_lote', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        else
                            echo '"' . $this->Html->getUrl('Estq_artigo_lote', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                        ?>
                        , function () {
                            r = true;
                        });
                }
            });
        });
    </script>