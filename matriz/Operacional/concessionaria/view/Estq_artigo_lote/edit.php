<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_artigo_lote', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de
                        preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label class="required" for="nome">Nome <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Estq_artigo_lote->nome ?>"
                                   placeholder="Nome" required>
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="validade">Validade</label>
                            <div class='input-group datePicker'>
                                <input type='text' name="validade" id="validade" class="form-control date"  value="<?php echo $Estq_artigo_lote->validade ?>" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                            </div>
                        </div>
                        <!-- passa o parametro da id do profissional -->
                        <?php if($this->getParam('modal')){ ?>

                        <?php }else{ ?>
                        <!-- / passa o parametro da id do profissional -->
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="artigo">Artigo</label>
                            <select name="artigo" class="form-control" id="artigo">
                                <?php
                                foreach ($Estq_artigos as $e) {
                                    if ($e->id == $Estq_artigo_lote->artigo)
                                        echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                    else
                                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <?php } ?>
                        <input type="hidden" name="situacao" value="1"/>

                        <div class="clearfix"></div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Estq_artigo_lote->id;?>">

                    <?php if($this->getParam('modal')){ ?>
                        <input type="hidden" name="modal" value="1"/>
                        <div class="text-right">
                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                                Cancelar
                            </a>
                            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar</button>
                            <input type="submit" onclick="EnviarFormulario('form')" id="validForm" style="display: none;" />
                        </div>
                    <?php }else{ ?>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Estq_artigo_lote', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#artigo option[value="<?php echo $Estq_artigo_lote->artigo ?>"]').prop('selected', true);



    moment.locale('pt-BR');

    $('.datePicker').datetimepicker({
        format: 'L',
        extraFormats: [ 'YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD' ],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true
    });
    });
</script>