
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Usuários</h2>
    <ol class="breadcrumb">
    <li>Usuários</li>
    <li class="active">
    <strong>Listagem de Usuários</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content ">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox">
    <div class="ibox-content no-borders">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="nome">Nome</label>
            <input type="text" name="filtro[interno][nome]" id="nome" class="form-control" value="<?php echo $this->getParam('nome'); ?>">
        </div>
                <div class="col-md-3 form-group">
                    <label for="situacao">Situação</label>
                    <select name="filtro[externo][situacao]" class="form-control" id="situacao">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Estq_situacaos as $e): ?>
                            <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="grupo">Grupo</label>
                    <select name="filtro[externo][grupo]" class="form-control" id="grupo">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Estq_group_useres as $e): ?>
                            <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao" data-toggle="tooltip" data-placement="bottom" title="Impressão"><span
                            class="glyphicon glyphicon-print"></span></button>
                    <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                       class="btn btn-default"
                       data-dismiss="modal" data-toggle="tooltip" data-placement="bottom" title="Recarregar a página"><span
                            class="glyphicon glyphicon-refresh "></span></a>
                    <button type="submit" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Pesquisar"><span
                            class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Usuário', 'Estq_users', 'add', NULL, array('class' => 'btn btn-primary','data-tool'=>"tooltip",'data-placement'=>"bottom", 'title'=>"Cadastrar novo Usuário")); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_users', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_users', 'all', array('orderBy' => 'imagem')); ?>'>
                        Imagem
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_users', 'all', array('orderBy' => 'login')); ?>'>
                        Login
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_users', 'all', array('orderBy' => 'email')); ?>'>
                        Email
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_users', 'all', array('orderBy' => 'situacao')); ?>'>
                        Situação
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_users', 'all', array('orderBy' => 'grupo')); ?>'>
                        Grupo
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Estq_users as $e) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($e->nome, 'Estq_users', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->imagem, 'Estq_users', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->login, 'Estq_users', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->email, 'Estq_users', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->getEstq_situacao()->nome, 'Estq_situacao', 'view',
                    array('id' => $e->getEstq_situacao()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->getEstq_group_user()->nome, 'Estq_group_user', 'view',
                    array('id' => $e->getEstq_group_user()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"  data-toggle="tooltip" data-placement="auto" title="Editar Usuário"></span> ', 'Estq_users', 'edit',
                    array('id' => $e->id), 
                    array('class' => 'btn btn-warning btn-sm','data-tool'=>"tooltip",'data-placement'=>"bottom", 'title'=>"Editar"));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"  data-toggle="tooltip" data-placement="auto" title="Excluir Usuário"></span> ', 'Estq_users', 'delete',
                    array('id' => $e->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal','data-tool'=>"tooltip",'data-placement'=>"bottom", 'title'=>"Deletar"));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Estq_users', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Estq_users', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>