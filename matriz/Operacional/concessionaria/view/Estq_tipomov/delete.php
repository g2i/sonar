<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <div class="ibox">
                <div class="ibox-content no-borders">

                    <form class="form" method="post"
                          action="<?php echo $this->Html->getUrl('Estq_tipomov', 'delete') ?>">
                        <h1>Confirmação</h1>

                        <div class="well well-lg">
                            <p>Voce tem certeza que deseja excluir o registro
                                <strong><?php echo $Estq_tipomov->nome; ?></strong>?</p>
                        </div>
                        <div class="text-right">
                            <input type="hidden" name="id" value="<?php echo $Estq_tipomov->id; ?>">
                            <a href="<?php echo $this->Html->getUrl('Estq_tipomov', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-danger" value="Excluir">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
