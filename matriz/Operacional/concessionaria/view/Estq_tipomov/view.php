<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <legend>Tipo de Movimento</legend>

        <p> <strong>Tipo de Movimento</strong>: <?php echo $Estq_tipomov->nome;?></p>
                    <p>
                    <strong>Requer</strong>:
                   <?php
                   switch($Estq_tipomov->requer){
                       case 1:
                           echo "Fornecedor";
                           break;
                       case 2:
                           echo "Cliente";
                           break;
                       case 3:
                           echo "Colaborador";
                           break;
                   }
                   ?>
                    </p>

<p><strong>Operação</strong>: <?php echo $Estq_tipomov->operacao;?></p>
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Estq_tipomov->getEstq_situacao()->nome, 'Estq_situacao', 'view',
    array('id' => $Estq_tipomov->getEstq_situacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>


                </div>
            </div>
        </div>
    </div>
</div>
