
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Andamento</h2>
    <ol class="breadcrumb">
    <li>Andamento</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated ">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"

            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"

            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="nome">Nome</label>
            <input type="text" name="filtro[interno][nome]" id="nome" class="form-control" value="<?php echo $this->getParam('nome'); ?>">
        </div>
                <div class="col-md-3 form-group">
                    <label for="situacao">Situação</label>
                    <select name="filtro[externo][situacao]" class="form-control" id="situacao">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Situacaos as $s): ?>
                            <?php echo '<option value="' . $s->id . '">' . $s->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">                    
                    <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>" class="btn btn-default"
                                    data-dismiss="modal" data-toggle="tooltip" data-placement="bottom" title="Recarregar a página"><span
                                        class="glyphicon glyphicon-refresh "></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo andamento', 'Concessionaria_andamento', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Concessionaria_andamento', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Concessionaria_andamento', 'all', array('orderBy' => 'situacao')); ?>'>
                        Situação
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Concessionaria_andamentos as $c) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($c->nome, 'Concessionaria_andamento', 'view',
                    array('id' => $c->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($c->getSituacao()->nome, 'Situacao', 'view',
                    array('id' => $c->getSituacao()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Concessionaria_andamento', 'edit', 
                    array('id' => $c->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Concessionaria_andamento', 'delete', 
                    array('id' => $c->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Concessionaria_andamento', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Concessionaria_andamento', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>