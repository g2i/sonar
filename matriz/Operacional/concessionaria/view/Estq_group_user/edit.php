
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo de Usuários</h2>
        <ol class="breadcrumb">
            <li>Grupo de Usuários</li>
            <li class="active">
                <strong>Editar Grupo</strong>
            </li></ol></div></div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content no-borders">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_group_user', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label class="required" for="nome">Nome <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Estq_group_user->nome ?>" placeholder="Nome" required>
        </div>
       <input type="hidden" value="1" name="situacao">
    </div>
    <input type="hidden" name="id" value="<?php echo $Estq_group_user->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Estq_group_user', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>