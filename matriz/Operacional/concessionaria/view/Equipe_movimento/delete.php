<form class="form" method="post" action="<?php echo $this->Html->getUrl('Equipe_movimento', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir a equipe <strong><?php echo $Equipe_movimento->getEquipe()->nome; ?></strong> deste movimento?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Equipe_movimento->id; ?>">
        <input type="hidden" name="modal" value="<?php echo $this->getParam('modal'); ?>">

        <a href="Javascript:void(0)" class="btn btn-default" <?php echo (!$this->getParam('modal'))?'data-dismiss="modal"': 'onclick="Navegar(\'\',\'back\')"'; ?>>Cancelar</a>
        <input type="submit" class="btn btn-danger" <?php echo (!$this->getParam('modal'))?'': 'onclick="EnviarFormulario(\'form\')"'; ?> value="Excluir">
    </div>
</form>