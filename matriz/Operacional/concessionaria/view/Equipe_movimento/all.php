<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Equipe</h2>
        <ol class="breadcrumb">
            <li>Equipe</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <?php if (!$this->getParam('modal')) { ?>
                        <div class="filtros well">
                            <div class="form">
                                <form role="form"
                                      action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                      method="post" enctype="application/x-www-form-urlencoded">
                                    <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                    <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                                    <div class="col-md-3 form-group">
                                        <label for="equipe_id">Equipe</label>
                                        <select name="filtro[externo][equipe_id]" class="form-control" id="equipe_id">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Equipes as $e): ?>
                                                <?php echo '<option value="' . $e->id . '">' . $e->nome . '</option>'; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 form-group">
                                        <label for="movimento_id">Movimento</label>
                                        <select name="filtro[externo][movimento_id]" class="form-control"
                                                id="movimento_id">
                                            <?php echo '<option value="">Selecione:</option>'; ?>
                                            <?php foreach ($Estq_movs as $e): ?>
                                                <?php echo '<option value="' . $e->id . '">' . $e->tipo . '</option>'; ?>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="button" class="btn btn-default botao-impressao"
                                                data-toggle="tooltip"
                                                data-placement="bottom" title="Imprimir"><span
                                                class="glyphicon glyphicon-print"></span></button>
                                        <button type="button" class="btn btn-default botao-reset" data-toggle="tooltip"
                                                data-placement="bottom" title="Limpar Filtros"><span
                                                class="glyphicon glyphicon-refresh"></span></button>
                                        <button type="submit" class="btn btn-default" data-toggle="tooltip"
                                                data-placement="bottom" title="Pesquisar"><span
                                                class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Aquicionar na Equipe', 'Equipe_movimento', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                        </div>
                    <?php } else {
                        echo ' <div class="text-right">';
                        echo $this->Html->getNavegar(' <span class="glyphicon glyphicon-plus-sign"></span>Aquicionar na Equipe', 'Equipe_movimento', 'add', array("ajax" => true, "modal" => "1", "movimento" => $this->getParam('movimento')), array('class' => 'btn btn-primary','data-placement' => 'bottom', 'data-togle' => 'tooltip', 'title' => 'Adicionar na Equipe'));
                        echo '</div>';
                    } ?>


                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Equipe_movimento', 'all', array('orderBy' => 'equipe_id')); ?>'>
                                            Equipe
                                        </a>
                                    </th>

                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Equipe_movimentos as $e) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($e->getEquipe()->nome, 'Equipe', 'view',
                                        array('id' => $e->getEquipe()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    if(!$this->getParam('modal')) {
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Equipe_movimento', 'edit',
                                            array('id' => $e->id),
                                            array('class' => 'btn btn-warning btn-sm'));
                                        echo '</td>';
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Equipe_movimento', 'delete',
                                            array('id' => $e->id),
                                            array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';
                                    }else{
                                        echo '<td width="50">';
                                        echo $this->Html->getNavegar(' <span class="glyphicon glyphicon-edit"></span>', 'Equipe_movimento', 'edit', array("ajax" => true, "modal" => "1", "movimento" => $this->getParam('movimento'),'id' => $e->id), array('class' => 'btn btn-warning btn-sm','data-placement' => 'bottom', 'data-togle' => 'tooltip', 'title' => 'Editar'));
                                        echo '</td>';
                                        echo '<td width="50">';
                                        echo $this->Html->getNavegar(' <span class="glyphicon glyphicon-remove"></span>', 'Equipe_movimento', 'delete', array("ajax" => true, "modal" => "1", "movimento" => $this->getParam('movimento'),'id' => $e->id), array('class' => 'btn btn-danger btn-sm','data-placement' => 'bottom', 'data-togle' => 'tooltip', 'title' => 'Deletar'));
                                        echo '</td>';
                                    }
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Equipe_movimento', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Equipe_movimento', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>