<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Equipe</h2>
        <ol class="breadcrumb">
            <li>Equipe</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Equipe_movimento', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div >
                            <input type="hidden" name="origem" value="<?php echo Config::get('origem'); ?>">
                            <input type="hidden" name="modal" value="<?php echo $this->getParam('modal'); ?>">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="equipe_id">Equipe</label>
                                <select name="equipe_id" class="form-control" id="equipe_id">
                                    <?php
                                    foreach ($Equipes as $e) {
                                        if ($e->id == $Equipe_movimento->equipe_id)
                                            echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                                        else
                                            echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <?php if(!$this->getParam('movimento')){ ?>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="movimento_id">Movimento</label>
                                    <select name="movimento_id" class="form-control" id="movimento_id">
                                        <?php
                                        foreach ($Estq_movs as $e) {
                                            if ($e->id == $Equipe_movimento->movimento_id)
                                                echo '<option selected value="' . $e->id . '">' . $e->tipodoc . '</option>';
                                            else
                                                echo '<option value="' . $e->id . '">' . $e->tipodoc . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            <?php }else{
                                echo '<input type="hidden" name="movimento_id" value="'.$this->getParam('movimento').'">';
                            } ?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="situacao">Situação</label>
                                <select name="situacao" class="form-control" id="situacao">
                                    <?php
                                    foreach ($Situacaos as $s) {
                                        if ($s->id == $Equipe_movimento->situacao)
                                            echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                                        else
                                            echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Equipe_movimento->id; ?>">

                        <div class="text-right">
                            <a href="Javascript:void(0)"
                               class="btn btn-default" <?php echo (!$this->getParam('modal'))?'data-dismiss="modal"': 'onclick="Navegar(\'\',\'back\')"'; ?>>Cancelar</a>
                            <input type="submit" class="btn btn-primary" <?php echo (!$this->getParam('modal'))?'': 'onclick="EnviarFormulario(\'form\')"'; ?> value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>