<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a role="open-adm" class="animatedClick" data-target='clickExample'>
                    <i class="fa fa-tasks"></i>
                </a>
            </li>
            <li>
                <?php echo $this->Html->getLink('<i class="fa fa-sign-out"></i> Sair', 'Login', 'logout'); ?>
            </li>
        </ul>
    </nav>
</div>

<div id="right-sidebar" style="display: none" role="adm"
    class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
    <div class="sidebar-container" full-scroll>
        <ul class="nav nav-tabs navs-2 " role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                    data-toggle="tab">Administração</a>
            </li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuários</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content" style="background: #2f4050">
            <!-- Administração -->
            <div role="tabpanel" class="tab-pane active" id="home">
                <div class="sidebar-title">
                    <h3><i class="fa fa-gears"></i> Ajustes </h3>
                    <small><i class="fa fa-tim"></i> Área do administrador.</small>
                </div>
                <nav class="navbar-default navbar-static" role="tablist">
                    <div class="sidebar-collapse">
                        <ul class="nav">

                            <li
                                <?php echo((CONTROLLER == "Frota_origem" || CONTROLLER == "Frota_tipo_manutencao" || CONTROLLER == "Frota_tipo_ocorrencia" ||
                        CONTROLLER == "Frota_tipo_acidente" || CONTROLLER == "Frota_acidentes" || CONTROLLER == "Frota_manutencao_classificacao" || CONTROLLER == "Frota_tipo_equipamento")); ?>>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                    href="#Frota_adm" aria-expanded="false" aria-controls="Frota_adm">
                                    <i class="fa fa-key"></i><span class="nav-label">Administração</span><span
                                        class="fa arrow"></span></a>

                                <div id="Frota_adm" class="panel-collapse collapse" role="tabpanel"
                                    aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                    style="height: 0px;">
                                    <ul class="nav nav-second-level">

                                        <li <?php echo(CONTROLLER == "Frota_origem" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Origem', 'Frota_origem', 'all'); ?>
                                        </li>

                                        <li <?php echo(CONTROLLER == "Frota_localizacao" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Localizaçao', 'Frota_localizacao', 'all'); ?>
                                        </li>

                                        <li <?php echo(CONTROLLER == "Fornecedor" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Fornecedores', 'Fornecedor', 'all'); ?>
                                        </li>

                                        <li <?php echo(CONTROLLER == "Frota_tipo_ocorrencia" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Tipo de Ocorrência', 'Frota_tipo_ocorrencia', 'all'); ?>
                                        </li>


                                        <li <?php echo(CONTROLLER == "Frota_tipo_manutencao" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Manutenção - Itens', 'Frota_tipo_manutencao', 'all'); ?>
                                        </li>
                                        <li
                                            <?php echo(CONTROLLER == "Frota_manutencao_classificacao" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Manutencao - Classificação', 'Frota_manutencao_classificacao', 'all'); ?>
                                        </li>

                                        <li
                                            <?php echo(CONTROLLER == "Frota_equipamentos_veiculo" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Equipamento Auxiliar', 'Frota_equipamentos_veiculo', 'all'); ?>
                                        </li>

                                        <li <?php echo(CONTROLLER == "Frota_tipo_acidente" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Frota tipo acidente', 'Frota_tipo_acidente', 'all'); ?>
                                        </li>
                                        <li <?php echo(CONTROLLER == "Frota_situacao_acidente" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Situação do acidente', 'Frota_situacao_acidente', 'all'); ?>
                                        </li>
                                        <li <?php echo(CONTROLLER == "Frota_tipo_equipamento" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Tipo Equipamento', 'Frota_tipo_equipamento', 'all'); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li <?php echo(CONTROLLER == "Frota_inspecao_grupo" ? 'class="active"' : ''); ?>>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                    href="#insp" aria-expanded="false" aria-controls="insp">
                                    <i class="fa fa-plus-circle"></i><span class="nav-label">Inspeção</span><span
                                        class="fa arrow"></span></a>

                                <div id="insp" class="panel-collapse collapse" role="tabpanel"
                                    aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                    style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li <?php echo(CONTROLLER == "Frota_inspecao_grupo" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Grupo', 'Frota_inspecao_grupo', 'all'); ?>
                                        </li>
                                        <li <?php echo(CONTROLLER == "Frota_inspecao_pergunta" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Pergunta', 'Frota_inspecao_pergunta', 'all'); ?>
                                        </li>
                                        <li <?php echo(CONTROLLER == "Frota_inspecao_resposta" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Resposta', 'Frota_inspecao_resposta', 'all'); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <!-- Usuário   -->

            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="sidebar-title">
                    <h3><i class="fa fa-users"></i> Usuário</h3>
                    <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                </div>
                <nav class="navbar-default navbar-static" role="tablist">
                    <div class="sidebar-collapse">
                        <ul class="nav">
                            <li <?php echo(CONTROLLER == "Usuario" ? 'class="active"' : ''); ?>>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                    href="#Usuario" aria-expanded="false" aria-controls="Usuario">
                                    <i class="fa fa-users"></i><span class="nav-label">Usuários</span><span
                                        class="fa arrow"></span></a>

                                <div id="Usuario" class="panel-collapse collapse" role="tabpanel"
                                    aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                    style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li
                                            <?php echo(CONTROLLER == "Usuario" && ACTION == "conta" ? 'class="active"' : ''); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-user"></i>Conta', 'Usuario', 'conta'); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>