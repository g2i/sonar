<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo SITE_PATH; ?>/img/g2i.png">
    <link href="<?php echo SITE_PATH; ?>/template/inspinia/css/style.css" rel="stylesheet">
      
 <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php $this->getHeaders(); ?>

</head>
<?php if(!empty($user_) && (CONTROLLER == "Login" && ACTION == "lock") && (CONTROLLER == "Usuario" && ACTION == "resetar_senha")): ?>
<body class="gray-bg">
<?php
$this->getContents();
else:
?>
<style>
    body { }
    .starter-template { padding: 0px 15px; }
    div.box-login{width: 450px;position: absolute;left: 50%;margin-left: -225px;top: 50%;height: 250px;margin-top: -125px;border: 1px solid #CCC;padding: 25px;border-radius: 10px;box-shadow: 5px 5px 9px #333;}
    div.offinput{background: #FFF;border: 1px solid #ccc;border-radius: 4px;position: relative;}
    div.offinput span img{vertical-align: top;}
    div.offinput span{position: absolute;right: 10px;top: 50%;height: 16px;width: 16px;margin-top: -8px;}
    div.offinput input{width: 100%;border-radius: 4px;border: none;padding: 10px;}
    div.offinput input:focus{outline: none;}
    div.offinput input:-webkit-autofill {-webkit-box-shadow: 0 0 0px 1000px white inset;}
</style>
<?php if(empty($user_) || (CONTROLLER == "Usuario" && ACTION == "resetar_senha")){ ?>
    <style>
        #page-wrapper{
            margin-left: 0px;
        }
        #wrapper{
            background-color: #fff !important;
        }
        .footer.fixed{
            margin-left: 0px !important;
        }
    </style>
<?php } ?>
<body>
<div id="wrapper">
    <?php if(!empty($user_) && $user_->primeiro_acesso != 1): ?>
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <?php include 'template/menu.php' ?>
                </ul>
            </div>
        </nav>
    <?php endif; ?>
    <div id="page-wrapper" class="gray-bg">
        <?php
        if(!empty($user_)&& (CONTROLLER != "Usuario" && ACTION != "resetar_senha")):
            include 'topNavbar.php';
        endif;
        $this->getContents();
        ?>
    </div>
    <div class="footer fixed">
        <div>
            <strong>Copyright</strong> G2i &copy; 2014-<?php echo date('Y'); ?>
        </div>
    </div>
</div>
</div>
<div class="clearfix"></div>

<!-- Generic Modal -->
<div class="modal fade" id="modal" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div style="text-align:center">
                <img src="<?php echo SITE_PATH; ?>/template/default/images/loading.gif" alt="LazyPHP">
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg" id="modal-lg" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div style="text-align:center"><img src="<?php echo SITE_PATH;?>/template/default/images/loading.gif" alt="LazyPHP"></div>
        </div>
    </div>
</div>

<?php endif;
$this->getScripts();  ?>

<script>
    $(window).load(function(){
    initComponente(document)
    })
</script>
</body>
</html>
