<li class="nav-header">
    <div class="dropdown profile-element">
    <span>
    <?php echo(file_exists(SITE_PATH . "/content/img/" . sprintf("%06d", @Session::get("user")->id) . ".png") ? '<img class="img-circle" src="' . SITE_PATH . '/content/img/' . sprintf("%06d", @Session::get("user")->id) . '.png" alt="Foto"/>' : '<img class="img-circle" src="' . SITE_PATH . '/content/img/empty-avatar.png" alt="Foto"/>') ?>
    </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
    <span class="clear"> <span class="block m-t-xs"> <strong
                    class="font-bold"><?php echo @Session::get("user")->nome; ?></strong>
    </span> <span class="text-muted text-xs block">Opções <b class="caret"></b></span> </span> </a>
        <ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><?php echo $this->Html->getLink('Sair', 'Login', 'logout'); ?></li>
        </ul>
    </div>
    <div class="logo-element">
        G2i
    </div>
</li>

<li <?php echo(CONTROLLER == "Frota_veiculos" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-truck"></i><span class="nav-label">Veículos</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Frota_veiculos" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Listar', 'Frota_veiculos', 'all'); ?>
        </li>
        <!--<li <?php echo(CONTROLLER == "Frota_veiculos" && ACTION == "add" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Frota_veiculos', 'add'); ?>
        </li>-->
        <li <?php echo(CONTROLLER == "Frota_veiculos" && ACTION == "total" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-money"></i> Gastos', 'Frota_veiculos', 'total'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Frota_motorista" && ACTION == "total" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-user"></i> Motorista', 'Frota_motorista', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Frota_equipamentos_veiculo" && ACTION == "all"); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus"></i> Equipamento Auxiliar', 'Frota_equipamentos_veiculo', 'all'); ?>
        </li>
    </ul>
</li>

<li <?php echo(CONTROLLER == "Frota_abastecimentos" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-tachometer"></i><span class="nav-label">Abastecimentos</span><span
                class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Frota_abastecimentos" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-list-ul"></i> Listar', 'Frota_abastecimentos', 'all'); ?>
        </li>
       <!-- <li <?php echo(CONTROLLER == "Frota_abastecimentos" && ACTION == "add" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Adicionar', 'Frota_abastecimentos', 'add'); ?>
        </li>-->
        <li>
            <a href="http://www.g2ioperacional.com.br/JS/matriz/Operacional/frota/Frota_abastecimentos/"><i
                        class="fa fa-leaf"></i>Consumo Médio</a>
        </li>
    </ul>
</li>


<li <?php echo(CONTROLLER == "Frota_manutencoes" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-wrench"></i> Manutenções', 'Frota_manutencoes', 'all'); ?>
</li>
<li <?php echo(CONTROLLER == "Frota_ocorrencias" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-warning"></i> Ocorrências', 'Frota_ocorrencias', 'all'); ?>
</li>
<li <?php echo(CONTROLLER == "Frota_acidente" ? 'class="active"' : '');?>>
<?php echo $this->Html->getLink('<span class="fa fa-ambulance"></span> Acidentes/RAV', 'Frota_acidente', 'acidentes'); ?>
</li>

<li <?php echo(CONTROLLER == "Frota_veiculos" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exclamation-triangle"></i>Equipamento Segurança', 'Frota_veiculos', 'all'); ?>
</li>
<li <?php echo(CONTROLLER == "Frota_inspecao" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-plus-circle"></i> Inspeção', 'Frota_inspecao', 'all'); ?>
</li>


