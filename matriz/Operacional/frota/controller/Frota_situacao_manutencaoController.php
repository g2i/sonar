<?php
final class Frota_situacao_manutencaoController extends AppController{ 

    # página inicial do módulo Frota_situacao_manutencao
    function index(){
        $this->setTitle('Visualização de Frota_situacao_manutencao');
    }

    # lista de Frota_situacao_manutencaos
    # renderiza a visão /view/Frota_situacao_manutencao/all.php
    function all(){
        $this->setTitle('Listagem de Frota_situacao_manutencao');
        $p = new Paginate('Frota_situacao_manutencao', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_situacao_manutencaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Frota_situacao_manutencao
    # renderiza a visão /view/Frota_situacao_manutencao/view.php
    function view(){
        $this->setTitle('Visualização de Frota_situacao_manutencao');
        try {
            $this->set('Frota_situacao_manutencao', new Frota_situacao_manutencao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_situacao_manutencao', 'all');
        }
    }

    # formulário de cadastro de Frota_situacao_manutencao
    # renderiza a visão /view/Frota_situacao_manutencao/add.php
    function add(){
        $this->setTitle('Cadastro de Frota_situacao_manutencao');
        $this->set('Frota_situacao_manutencao', new Frota_situacao_manutencao);
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_situacao_manutencao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_situacao_manutencao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Frota_situacao_manutencao');
        $Frota_situacao_manutencao = new Frota_situacao_manutencao();
        $this->set('Frota_situacao_manutencao', $Frota_situacao_manutencao);
        try {
            $Frota_situacao_manutencao->save($_POST);
            new Msg(__('Frota_situacao_manutencao cadastrado com sucesso'));
            $this->go('Frota_situacao_manutencao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_situacao_manutencao
    # renderiza a visão /view/Frota_situacao_manutencao/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_situacao_manutencao');
        try {
            $this->set('Frota_situacao_manutencao', new Frota_situacao_manutencao((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_situacao_manutencao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_situacao_manutencao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_situacao_manutencao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_situacao_manutencao');
        try {
            $Frota_situacao_manutencao = new Frota_situacao_manutencao((int) $_POST['id']);
            $this->set('Frota_situacao_manutencao', $Frota_situacao_manutencao);
            $Frota_situacao_manutencao->save($_POST);
            new Msg(__('Frota_situacao_manutencao atualizado com sucesso'));
            $this->go('Frota_situacao_manutencao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_situacao_manutencao
    # renderiza a /view/Frota_situacao_manutencao/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_situacao_manutencao');
        try {
            $this->set('Frota_situacao_manutencao', new Frota_situacao_manutencao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_situacao_manutencao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_situacao_manutencao
    # redireciona para Frota_situacao_manutencao/all
    function post_delete(){
        try {
            $Frota_situacao_manutencao = new Frota_situacao_manutencao((int) $_POST['id']);
            $Frota_situacao_manutencao->delete();
            new Msg(__('Frota_situacao_manutencao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_situacao_manutencao', 'all');
    }

}