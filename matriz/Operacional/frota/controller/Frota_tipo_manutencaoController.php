<?php

final class Frota_tipo_manutencaoController extends AppController
{

    # página inicial do módulo Frota_tipo_manutencao
    function index()
    {
        $this->setTitle('Tipos de Manutenção');
    }

    # lista de Frota_tipo_manutencaos
    # renderiza a visão /view/Frota_tipo_manutencao/all.php
    function all()
    {
        $this->setTitle('Listagem de Tipo de Manutenção');
        $p = new Paginate('Frota_tipo_manutencao', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->setOrder("dt_cadastro DESC"); 
        $this->set('Frota_tipo_manutencaos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $this->set('Usuarios', Usuario::getList());
        $this->set('Frota_manutencao_classificacao', Frota_manutencao_classificacao::getList());
    }

    # visualiza um(a) Frota_tipo_manutencao
    # renderiza a visão /view/Frota_tipo_manutencao/view.php
    function view()
    {
        $this->setTitle('Tipos de Manutenção');
        try {
            $this->set('Frota_tipo_manutencao', new Frota_tipo_manutencao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_manutencao', 'all');
        }
    }

    # formulário de cadastro de Frota_tipo_manutencao
    # renderiza a visão /view/Frota_tipo_manutencao/add.php
    function add()
    {
        $this->setTitle('Cadastro de Tipo de Manutenção');
        $this->set('Frota_tipo_manutencao', new Frota_tipo_manutencao);
        $this->set('Usuarios', Usuario::getList());
        $this->set('Frota_manutencao_classificacao', Frota_manutencao_classificacao::getList());
        $this->set('modal', $this->getParam('modal'));
       
    }

    # recebe os dados enviados via post do cadastro de Frota_tipo_manutencao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_manutencao/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Tipo de Manutenção');
        $Frota_tipo_manutencao = new Frota_tipo_manutencao();
        $this->set('Frota_tipo_manutencao', $Frota_tipo_manutencao);
        $_POST['usuario_id'] = Session::get('user')->id;
        $_POST['situacao_id'] = 1;
        $_POST['descricao'] = strtoupper($_POST['descricao']);
     
        try {
            $Frota_tipo_manutencao->save($_POST);
            new Msg(__('Tipo de Manutenção cadastrado com sucesso'));
            if(!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Frota_tipo_manutencao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }

    }

    # formulário de edição de Frota_tipo_manutencao
    # renderiza a visão /view/Frota_tipo_manutencao/edit.php
    function edit()
    {
        $this->setTitle('Editar Tipo de Manutenção');
        try {
            $this->set('Frota_tipo_manutencao', new Frota_tipo_manutencao((int)$this->getParam('id')));
            $this->set('Usuarios', Usuario::getList());
            $this->set('Frota_manutencao_classificacao', Frota_manutencao_classificacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Frota_tipo_manutencao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_tipo_manutencao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_manutencao/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Tipo de Manutenção');
        try {
            $Frota_tipo_manutencao = new Frota_tipo_manutencao((int)$_POST['id']);
            $this->set('Frota_tipo_manutencao', $Frota_tipo_manutencao);
            $_POST['usuario_id'] = Session::get('user')->id;
            $Frota_tipo_manutencao->save($_POST);
            new Msg(__('Tipo de Manutenção atualizado com sucesso'));
            $this->go('Frota_tipo_manutencao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Frota_tipo_manutencao
    # renderiza a /view/Frota_tipo_manutencao/delete.php
    function delete()
    {
        $this->setTitle('Apagar Frota_tipo_manutencao');
        try {
            $this->set('Frota_tipo_manutencao', new Frota_tipo_manutencao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_manutencao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_tipo_manutencao
    # redireciona para Frota_tipo_manutencao/all
    function post_delete()
    {
        try {
            $Frota_tipo_manutencao = new Frota_tipo_manutencao((int)$_POST['id']);
            $Frota_tipo_manutencao->situacao_id = 3;
            $Frota_tipo_manutencao->save();
            new Msg(__('Tipo de Manutenção apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Frota_tipo_manutencao', 'all');
    }

}