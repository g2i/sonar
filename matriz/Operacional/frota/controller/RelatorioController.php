<?php
final class RelatorioController extends AppController{ 

    # página inicial do módulo Rhrelatorio
    function index(){
        $this->setTitle('Visualização de Rhrelatorio');
    }

    function reports_inspecao(){
        $this->setTemplate('reports');
        $inspecaoQuery = new Criteria();
        $inspecaoQuery->addCondition('id','=', (int) $this->getParam('id'));
        $inspecaoQuery->setOrder('id DESC');
        $inspecao =  Frota_inspecao::getList($inspecaoQuery);
        foreach($inspecao as $ins){
            $json['inspecao'][] = [                
                'id' => $ins->id,
                'veiculo' => !empty($ins->getFrota_veiculos()->placa) ? $ins->getFrota_veiculos()->placa : '-',
                'motorista' => !empty($ins->getFrota_motorista()->nome) ? $ins->getFrota_motorista()->nome : '-',
                'equipe' => !empty($ins->equipe) ? $ins->equipe : '-', 
                'carona' => !empty($ins->nome_carona) ? $ins->nome_carona : '-',             
                'responsavelVistoria' => !empty($ins->responsavel_vistoria) ? $ins->responsavel_vistoria : '-',
                'dataInspecao' => !empty($ins->data_inspecao) ? DataBR($ins->data_inspecao) : '-',
                'observacao' => !empty($ins->observacao) ? $ins->observacao : '-',
                'localidade' => !empty($ins->getLocalidade()->cidade) ? $ins->getLocalidade()->cidade : '-'
            ];
        }   
        $checkQuery = new Criteria();
        $checkQuery->addCondition('frota_inspecao_id','=', (int) $this->getParam('id'));
        $checkQuery->setOrder('id DESC');
        $inspecaoCheck =  Frota_inspecao_check_pergunta_resposta::getList($checkQuery);          
        foreach($inspecaoCheck as $check){           
            $grupo = $check->getFrota_inspecao_pergunta()->getFrota_inspecao_grupo()->nome;
            $json['perguntas'][] = [                      
                'pergunta' => !empty($check->getFrota_inspecao_pergunta()->nome) ? $check->getFrota_inspecao_pergunta()->nome : '-',
                'resposta' => !empty($check->getFrota_inspecao_resposta()->nome) ? $check->getFrota_inspecao_resposta()->nome : '-',
                'grupo' =>  !empty($grupo) ? $grupo : '-'
            ];              
        }
        $reportCreate = new Json();
        $reportCreate->create('reportInspecao.json', json_encode($json));         
    }
}