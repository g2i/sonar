<?php
final class FinalizadoController extends AppController{ 

    # página inicial do módulo Finalizado
    function index(){
        $this->setTitle('Visualização de Finalizado');
    }

    # lista de Finalizados
    # renderiza a visão /view/Finalizado/all.php
    function all(){
        $this->setTitle('Listagem de Finalizado');
        $p = new Paginate('Finalizado', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Finalizados', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Finalizado
    # renderiza a visão /view/Finalizado/view.php
    function view(){
        $this->setTitle('Visualização de Finalizado');
        try {
            $this->set('Finalizado', new Finalizado((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Finalizado', 'all');
        }
    }

    # formulário de cadastro de Finalizado
    # renderiza a visão /view/Finalizado/add.php
    function add(){
        $this->setTitle('Cadastro de Finalizado');
        $this->set('Finalizado', new Finalizado);
    }

    # recebe os dados enviados via post do cadastro de Finalizado
    # (true)redireciona ou (false) renderiza a visão /view/Finalizado/add.php
    function post_add(){
        $this->setTitle('Cadastro de Finalizado');
        $Finalizado = new Finalizado();
        $this->set('Finalizado', $Finalizado);
        try {
            $Finalizado->save($_POST);
            new Msg(__('Finalizado cadastrado com sucesso'));
            $this->go('Finalizado', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Finalizado
    # renderiza a visão /view/Finalizado/edit.php
    function edit(){
        $this->setTitle('Edição de Finalizado');
        try {
            $this->set('Finalizado', new Finalizado((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Finalizado', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Finalizado
    # (true)redireciona ou (false) renderiza a visão /view/Finalizado/edit.php
    function post_edit(){
        $this->setTitle('Edição de Finalizado');
        try {
            $Finalizado = new Finalizado((int) $_POST['id']);
            $this->set('Finalizado', $Finalizado);
            $Finalizado->save($_POST);
            new Msg(__('Finalizado atualizado com sucesso'));
            $this->go('Finalizado', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Finalizado
    # renderiza a /view/Finalizado/delete.php
    function delete(){
        $this->setTitle('Apagar Finalizado');
        try {
            $this->set('Finalizado', new Finalizado((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Finalizado', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Finalizado
    # redireciona para Finalizado/all
    function post_delete(){
        try {
            $Finalizado = new Finalizado((int) $_POST['id']);
            $Finalizado->delete();
            new Msg(__('Finalizado apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Finalizado', 'all');
    }

}