<?php

final class LoginController extends AppController {
    
    # Nome do Model que representa a tabela que guarda os dados dos usuários:
    private $model = 'Usuario';
    # Nome do campo da tabela que armazena o LOGIN:
    private $login = 'login';
    # Nome do campo da tabela que armazena a SENHA:
    private $password = 'senha';
    # Nome do campo da tabela que armazena o E-MAIL:
    private $email = 'email';

    function index() {
        //$this->go('Login', 'login');
    }

    function login() {
        if (Session::get('user')) {
            $this->go(Config::get('indexController'), Config::get('indexAction'));
        }
        $this->setTitle('Login');
        $this->set('user', $this->model);

    }

    function post_login() {
        $this->setTitle('Login');
        $c = new Criteria();
        $c->addCondition($this->login, '=', $_POST['login']);
        $c->addCondition($this->password, '=', $_POST['password']);

        $model = $this->model;
        $this->set('user', $this->model);

            $d = new Criteria();
            $d->addCondition($this->login, '=', $_POST['login']);
            $d->addCondition($this->password, '=', md5(Config::get('salt').$_POST['password']));
            $user = $model::getFirst($d);
            if ($user) {

                $e = new Criteria();
                #Frota cod. 5
                $e->addCondition('usuario_modulo.modulo_id', '=', 5);
                $e->addCondition('usuario_modulo.usuario_id', '=', $user->id);
                $Usuario_modulo = Usuario_modulo::getFirst($e);

                if (!$Usuario_modulo) {
                    new Msg('Usuario não autorizado neste modulo.', 2);
                    $this->go('login', 'login');
                }

                // new Msg('Bem vindo ' . $_POST['login']);

                Session::set('user', $user);
                //Seta o id da div de notificação das manutenções, que inicialmente é setado como banner para ativar a exibição da modal
                $id = "banner";
                Session::set('id', $id);
                if($user->primeiro_acesso == 1) {
                    new Msg('Bem Vindo! Para continuar altere sua senha');
                    $this->go('Usuario', 'resetar_senha');
                }

                $this->go(Config::get('indexController'), Config::get('indexAction')); # Ao autenticar, redireciona para...
            }else {
                new Msg('Login ou senha incorretos. Por favor, tente novamente.', 3);
            }

    }

    function logout() {
        Session::set('user', NULL);
        $this->go('Login', 'login');
    }

    function send() {
        $this->setTitle('Recuperar senha');
    }

    function post_send() {
        $this->setTitle('Recuperar senha');
        $c = new Criteria();
        $c->addCondition($this->email, '=', $_POST['email']);
        $model = $this->model;
        $user = $model::getFirst($c);
        if ($user) {
            $d = new DateTime();
            $agora = $d->format('Ymdhi');
            # email:
            $headers = "From: nao-responder@" . $_SERVER['HTTP_HOST'] . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $subject = "Recuperação de senha em ".$_SERVER['HTTP_HOST'];
            $message = "Olá,<p>Alguém (provavelmente você) pediu para mudar a senha da sua conta em ";
            $message .= $_SERVER['HTTP_HOST'] . ".</p>";
            $message .= "<p>Para confirmar este pedido e cadastrar uma nova senha, vá ao seguinte endereço web: ";
            $message .= "<a href='" . $_SERVER['HTTP_HOST'] . "/" . SITE_PATH . "/index.php?m=Login&p=reset&recuperar=" . Cript::cript(Config::get('salt') . $user->{$model::PK}) . "&d=".urlencode(Cript::cript($agora))."'>Gerar uma nova senha</a></p>";
            mail($_POST['email'], $subject, $message, $headers);
            new Msg('Um e-mail foi enviado para ' . $_POST['email'] . ' com as instruções. <br>Caso não tenha recebido, verifique sua caixa de spam e tente novamente.');
            $this->go('Login', 'login');
        } else {
            new Msg('E-mail não cadastrado!', 3);
        }
    }

    function reset() {
        $id = (int)str_replace(Config::get('salt'), '', Cript::decript($_GET['recuperar']));
        try {
            $d = new DateTime();
            $agora = $d->format('Ymdhi');
            $data = (int)str_replace(Config::get('salt'), '', urldecode(Cript::decript($_GET['d'])));
            #meia hora de validade do email
            if(($agora - $data) > 30){
                throw new Exception('<p>Este link expirou. Solicite novamente sua senha.</p>');
                $this->go('Login', 'login');
            }     
            $model = $this->model;
            $user = new $model($id);
            $senha = $this->gerarSenha();
            $user->{$this->password} = md5(Config::get('salt').$senha);
            $user->save();
            
            #envia email com a nova senha
            $headers = "From: nao-responder@" . $_SERVER['HTTP_HOST'] . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $subject = "Nova senha em ".$_SERVER['HTTP_HOST'];
            $message = "Olá de novo,<p>você pediu para mudar a senha da sua conta em ";
            $message .= $_SERVER['HTTP_HOST'] . ".</p>";
            $message .= "<p>Sua nova senha é:<strong>$senha</strong></p>";
            mail($user->{$this->email}, $subject, $message, $headers);
            
            new Msg('Uma nova senha foi gerada e enviada para o seu email!');
            $this->go('Login', 'login');
            
        } catch (Exception $exc) {
            new Msg($exc->getMessage(), 3);
             $this->go('Login', 'login');
        }
    }

    public function gerarSenha($length = 8) {
        $salt = "abcdefghijklmnpqrstuvwxyz123456789";
        $len = strlen($salt);
        $pass = '';
        mt_srand(10000000 * (double) microtime());
        for ($i = 0; $i < $length; $i++) {
            $pass .= $salt[mt_rand(0, $len - 1)];
        }
        return $pass;
    }

    function check_login(){
        $this->setTitle('Checar login');
        $user = Session::get('user_rel');

        $a = new Criteria();
        $a->addCondition('login','=',$_POST['login']);
        if(!empty($user->codigo))
            $a->addCondition('codigo','!=',$user->codigo);
        $clientes = Cliente::getList($a);

        if(count($clientes)>0){
            echo 1;
        }else{
            $b = new Criteria();
            $b->addCondition('login','=',$_POST['login']);
            if(!empty($user->id))
            $a->addCondition('id','!=',$user->id);
            $login_clientes = Login_cliente::getList($b);
            if(count($login_clientes)>0){
                echo 1;
            }else{
                echo 0;
            }
        }
        exit;
    }

    function lock() {
        if (!Session::get('user'))
            $this->go('Login', 'login');

        $image = file_exists(SITE_PATH . "/content/img/" . sprintf("%06d", Session::get('user')->id) . ".png") ?
            SITE_PATH . '/content/img/' . sprintf("%06d", @Session::get('user')->id) . '.png' :
            SITE_PATH . '/content/img/empty-avatar.png';

        $nome = Session::get('user')->nome;
        $login = Session::get('user')->login;
        Session::set('user', NULL);

        $this->setTitle('Tela de Bloqueio');
        $this->set('image', $image);
        $this->set('nome', $nome);
        $this->set('login', $login);
        $this->set('redirect', $this->getParam('redirect'));
    }

    function post_lock() {
        $this->setTitle('Tela de Bloqueio');
        $c = new Criteria();
        $c->addCondition($this->login, '=', $_POST['login']);
        $c->addCondition($this->password, '=', md5(Config::get('salt').$_POST['password']));

        $model = $this->model;
        $this->set('user', $this->model);
        $user = $model::getFirst($c);
        if ($user) {
            Session::set('user', $user);
            if($this->getParam('redirect'))
                header('Location: '.Cript::decript($this->getParam('redirect'))); # Ao autenticar, redireciona para...
            else
                $this->go(Config::get('indexController'), Config::get('indexAction')); # Ao autenticar, redireciona para...
        } else {
            new Msg('Senha incorreta. Por favor, tente novamente.', 3);
        }
    }

    function login_c(){
        if (Session::get('user_cliente')) {
            $this->go('Cliente_solicitacoes','all');
        }
        $this->setTitle('Acessar Agendamentos');
        $this->set('user_cliente', $this->model);
    }

    function post_login_c() {
        $this->setTitle('Agendamentos');

            $model = $this->model;
            $this->set('user_cliente', $this->model);
            $d = new Criteria();
            $d->addCondition($this->login, '=', $_POST['login']);
            $d->addCondition($this->password, '=', md5(Config::get('salt').$_POST['password']));
            $user = $model::getFirst($d);
            if ($user) {
                new Msg('Bem vindo ' . $_POST['login']);
                Session::set('user_cliente', $user);
                $this->go('Cliente_solicitacoes','add');
            }else {
                new Msg('Login ou senha incorretos. Por favor, tente novamente.', 3);
            }
    }


}