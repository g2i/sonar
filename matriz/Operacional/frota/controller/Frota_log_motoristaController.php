<?php
final class Frota_log_motoristaController extends AppController{ 

    # página inicial do módulo Frota_log_motorista
    function index(){
        $this->setTitle('Visualização de Frota_log_motorista');
    }

    # lista de Frota_log_motoristas
    # renderiza a visão /view/Frota_log_motorista/all.php
    function all(){
        $this->setTitle('Listagem de Frota_log_motorista');
        $p = new Paginate('Frota_log_motorista', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_log_motoristas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Frota_log_motorista
    # renderiza a visão /view/Frota_log_motorista/view.php
    function view(){
        $this->setTitle('Visualização de Frota_log_motorista');
        try {
            $this->set('Frota_log_motorista', new Frota_log_motorista((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_log_motorista', 'all');
        }
    }

    # formulário de cadastro de Frota_log_motorista
    # renderiza a visão /view/Frota_log_motorista/add.php
    function add(){
        $this->setTitle('Cadastro de Frota_log_motorista');
        $this->set('Frota_log_motorista', new Frota_log_motorista);
    }

    # recebe os dados enviados via post do cadastro de Frota_log_motorista
    # (true)redireciona ou (false) renderiza a visão /view/Frota_log_motorista/add.php
    function post_add(){
        $this->setTitle('Cadastro de Frota_log_motorista');
        $Frota_log_motorista = new Frota_log_motorista();
        $this->set('Frota_log_motorista', $Frota_log_motorista);
        try {
            $Frota_log_motorista->save($_POST);
            new Msg(__('Frota_log_motorista cadastrado com sucesso'));
            $this->go('Frota_log_motorista', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Frota_log_motorista
    # renderiza a visão /view/Frota_log_motorista/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_log_motorista');
        try {
            $this->set('Frota_log_motorista', new Frota_log_motorista((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_log_motorista', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_log_motorista
    # (true)redireciona ou (false) renderiza a visão /view/Frota_log_motorista/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_log_motorista');
        try {
            $Frota_log_motorista = new Frota_log_motorista((int) $_POST['id']);
            $this->set('Frota_log_motorista', $Frota_log_motorista);
            $Frota_log_motorista->save($_POST);
            new Msg(__('Frota_log_motorista atualizado com sucesso'));
            $this->go('Frota_log_motorista', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Frota_log_motorista
    # renderiza a /view/Frota_log_motorista/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_log_motorista');
        try {
            $this->set('Frota_log_motorista', new Frota_log_motorista((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_log_motorista', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_log_motorista
    # redireciona para Frota_log_motorista/all
    function post_delete(){
        try {
            $Frota_log_motorista = new Frota_log_motorista((int) $_POST['id']);
            $Frota_log_motorista->delete();
            new Msg(__('Frota_log_motorista apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_log_motorista', 'all');
    }

}