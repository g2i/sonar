<?php
final class RhprofissionalController extends AppController{ 

    # página inicial do módulo Rhprofissional
    function index(){
        $this->setTitle('Visualização de Rhprofissional');
    }

    # lista de Rhprofissionais
    # renderiza a visão /view/Rhprofissional/all.php
    function all(){
        $this->setTitle('Listagem de Rhprofissional');
        $p = new Paginate('Rhprofissional', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhprofissionais', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhprofissional
    # renderiza a visão /view/Rhprofissional/view.php
    function view(){
        $this->setTitle('Visualização de Rhprofissional');
        try {
            $this->set('Rhprofissional', new Rhprofissional((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional', 'all');
        }
    }

    # formulário de cadastro de Rhprofissional
    # renderiza a visão /view/Rhprofissional/add.php
    function add(){
        $this->setTitle('Cadastro de Rhprofissional');
        $this->set('Rhprofissional', new Rhprofissional);
    }

    # recebe os dados enviados via post do cadastro de Rhprofissional
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rhprofissional');
        $Rhprofissional = new Rhprofissional();
        $this->set('Rhprofissional', $Rhprofissional);
        try {
            $Rhprofissional->save($_POST);
            new Msg(__('Rhprofissional cadastrado com sucesso'));
            $this->go('Rhprofissional', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Rhprofissional
    # renderiza a visão /view/Rhprofissional/edit.php
    function edit(){
        $this->setTitle('Edição de Rhprofissional');
        try {
            $this->set('Rhprofissional', new Rhprofissional((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhprofissional', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhprofissional
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhprofissional');
        try {
            $Rhprofissional = new Rhprofissional((int) $_POST['codigo']);
            $this->set('Rhprofissional', $Rhprofissional);
            $Rhprofissional->save($_POST);
            new Msg(__('Rhprofissional atualizado com sucesso'));
            $this->go('Rhprofissional', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhprofissional
    # renderiza a /view/Rhprofissional/delete.php
    function delete(){
        $this->setTitle('Apagar Rhprofissional');
        try {
            $this->set('Rhprofissional', new Rhprofissional((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhprofissional
    # redireciona para Rhprofissional/all
    function post_delete(){
        try {
            $Rhprofissional = new Rhprofissional((int) $_POST['id']);
            $Rhprofissional->delete();
            new Msg(__('Rhprofissional apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprofissional', 'all');
    }

}