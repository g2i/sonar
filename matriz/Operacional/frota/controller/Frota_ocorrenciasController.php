<?php

final class Frota_ocorrenciasController extends AppController
{

    # página inicial do módulo Frota_ocorrencias
    function index()
    {
        $this->setTitle('Ocorrências');
    }

    # lista de Frota_ocorrencias
    # renderiza a visão /view/Frota_ocorrencias/all.php
    function all()
    {
        $this->setTitle('Listagem de Ocorrências');
        $p = new Paginate('Frota_ocorrencias', 10);
        $c = new Criteria();
        //Consulta de veículos
        $c->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_ocorrencias.veiculo_id', 'INNER');
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        //Filtro por localização do veículo
        if (!empty($_POST['localizacao_id'])) {
            $c->addCondition('frota_veiculos.localizacao_id', '=', $_POST['localizacao_id']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_ocorrencias', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Frota_tipo_ocorrencias', Frota_tipo_ocorrencia::getList());
        $this->set('Frota_localizacao', Frota_localizacao::getList());


    }

    # visualiza um(a) Frota_ocorrencias
    # renderiza a visão /view/Frota_ocorrencias/view.php
    function view()
    {
        $this->setTitle('Ocorrências');
        try {
            $this->set('Frota_ocorrencias', new Frota_ocorrencias((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_ocorrencias', 'all');
        }
    }

    # formulário de cadastro de Frota_ocorrencias
    # renderiza a visão /view/Frota_ocorrencias/add.php
    function add()
    {
        $this->setTitle('Cadastro de Ocorrências');
        $this->set('Frota_ocorrencias', new Frota_ocorrencias);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        $c->setOrder("placa ASC");
        $this->set('Frota_veiculos', Frota_veiculos::getList($c));
        $this->set('Usuarios', Usuario::getList());
        $b = new Criteria();
        $b->setOrder("descricao ASC");
        $this->set('Frota_tipo_ocorrencias', Frota_tipo_ocorrencia::getList($b));
    }

    # recebe os dados enviados via post do cadastro de Frota_ocorrencias
    # (true)redireciona ou (false) renderiza a visão /view/Frota_ocorrencias/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Ocorrências');
        $Frota_ocorrencias = new Frota_ocorrencias();
        $this->set('Frota_ocorrencias', $Frota_ocorrencias);
        $_POST['usuario_id'] = Session::get('user')->id;
        $_POST['situacao_id'] = 1;
        if ($_POST['infracao_desconto_folha'] == 1) {
            $_POST['usuario_desconto_folha'] = Session::get('user')->id;
            $_POST['data_desconto_folha'] = date('Y-m-d H:i:s');
        }

        try {
            $Frota_ocorrencias->save($_POST);
            new Msg(__('Ocorrência cadastrada com sucesso'));
            $this->go('Frota_ocorrencias', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Frota_tipo_ocorrencias', Frota_tipo_ocorrencia::getList());
    }

    # formulário de edição de Frota_ocorrencias
    # renderiza a visão /view/Frota_ocorrencias/edit.php
    function edit()
    {
        $this->setTitle('Editar Ocorrências');
        try {
            $this->set('Frota_ocorrencias', new Frota_ocorrencias((int)$this->getParam('id')));
            $this->set('Frota_veiculos', Frota_veiculos::getList());
            $this->set('Usuarios', Usuario::getList());
            $this->set('Frota_tipo_ocorrencias', Frota_tipo_ocorrencia::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Frota_ocorrencias', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_ocorrencias
    # (true)redireciona ou (false) renderiza a visão /view/Frota_ocorrencias/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Ocorrências');
        try {
            $Frota_ocorrencias = new Frota_ocorrencias((int)$_POST['id']);
            $this->set('Frota_ocorrencias', $Frota_ocorrencias);
            $_POST['usuario_id'] = Session::get('user')->id;

            if ($_POST['infracao_desconto_folha'] != $Frota_ocorrencias->infracao_desconto_folha) {
                $_POST['usuario_desconto_folha'] = Session::get('user')->id;
                $_POST['data_desconto_folha'] = date('Y-m-d H:i:s');
            }

            $Frota_ocorrencias->save($_POST);
            new Msg(__('Ocorrência atualizada com sucesso'));
            $this->go('Frota_ocorrencias', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Frota_tipo_ocorrencias', Frota_tipo_ocorrencia::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_ocorrencias
    # renderiza a /view/Frota_ocorrencias/delete.php
    function delete()
    {
        $this->setTitle('Apagar Frota_ocorrencias');
        try {
            $this->set('Frota_ocorrencias', new Frota_ocorrencias((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_ocorrencias', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_ocorrencias
    # redireciona para Frota_ocorrencias/all
    function post_delete()
    {
        try {
            $Frota_ocorrencias = new Frota_ocorrencias((int)$_POST['id']);
            $Frota_ocorrencias->situacao_id = 3;
            $Frota_ocorrencias->save();
            new Msg(__('Ocorrência apagada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Frota_ocorrencias', 'all');
    }

}