<?php
final class Frota_inspecao_respostaController extends AppController{ 

    # página inicial do módulo Frota_inspecao_resposta
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Frota_inspecao_respostas
    # renderiza a visão /view/Frota_inspecao_resposta/all.php
    function all(){
        $this->setTitle('Listagem ');
        $p = new Paginate('Frota_inspecao_resposta', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Frota_inspecao_respostas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Frota_inspecao_resposta
    # renderiza a visão /view/Frota_inspecao_resposta/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Frota_inspecao_resposta', new Frota_inspecao_resposta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao_resposta', 'all');
        }
    }

    # formulário de cadastro de Frota_inspecao_resposta
    # renderiza a visão /view/Frota_inspecao_resposta/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Frota_inspecao_resposta', new Frota_inspecao_resposta);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_inspecao_resposta
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao_resposta/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Frota_inspecao_resposta = new Frota_inspecao_resposta();
        $this->set('Frota_inspecao_resposta', $Frota_inspecao_resposta);
        try {
            $Frota_inspecao_resposta->situacao_id = 1;
            $Frota_inspecao_resposta->cadastrado_por = Session::get('user')->id;
            $Frota_inspecao_resposta->dt_cadastro = date('Y-m-d H:i:d');
            $Frota_inspecao_resposta->save($_POST);
            new Msg(__('Frota_inspecao_resposta cadastrado com sucesso'));
            $this->go('Frota_inspecao_resposta', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_inspecao_resposta
    # renderiza a visão /view/Frota_inspecao_resposta/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Frota_inspecao_resposta', new Frota_inspecao_resposta((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_inspecao_resposta', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_inspecao_resposta
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao_resposta/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Frota_inspecao_resposta = new Frota_inspecao_resposta((int) $_POST['id']);
            $this->set('Frota_inspecao_resposta', $Frota_inspecao_resposta);
            $Frota_inspecao_resposta->modificado_por = Session::get('user')->id;
            $Frota_inspecao_resposta->dt_modificado = date('Y-m-d H:i:d');
            $Frota_inspecao_resposta->save($_POST);
            new Msg(__('Frota_inspecao_resposta atualizado com sucesso'));
            $this->go('Frota_inspecao_resposta', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_inspecao_resposta
    # renderiza a /view/Frota_inspecao_resposta/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_inspecao_resposta');
        try {
            $this->set('Frota_inspecao_resposta', new Frota_inspecao_resposta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao_resposta', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_inspecao_resposta
    # redireciona para Frota_inspecao_resposta/all
    function post_delete(){
        try {
            $Frota_inspecao_resposta = new Frota_inspecao_resposta((int) $_POST['id']);
            $Frota_inspecao_resposta->situacao_id = 3;
            $Frota_inspecao_resposta->save();
            new Msg(__('Frota_inspecao_resposta apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_inspecao_resposta', 'all');
    }

}