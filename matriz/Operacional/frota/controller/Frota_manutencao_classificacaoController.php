<?php
final class Frota_manutencao_classificacaoController extends AppController{ 

    # página inicial do módulo Frota_manutencao_classificacao
    function index(){
        $this->setTitle('Visualização de Frota_manutencao_classificacao');
    }

    # lista de Frota_manutencao_classificacaos
    # renderiza a visão /view/Frota_manutencao_classificacao/all.php
    function all(){
        $this->setTitle('Listagem de Frota_manutencao_classificacao');
        $p = new Paginate('Frota_manutencao_classificacao', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_manutencao_classificacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Frota_manutencao_classificacao
    # renderiza a visão /view/Frota_manutencao_classificacao/view.php
    function view(){
        $this->setTitle('Visualização de Frota_manutencao_classificacao');
        try {
            $this->set('Frota_manutencao_classificacao', new Frota_manutencao_classificacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_manutencao_classificacao', 'all');
        }
    }

    # formulário de cadastro de Frota_manutencao_classificacao
    # renderiza a visão /view/Frota_manutencao_classificacao/add.php
    function add(){
        $this->setTitle('Cadastro de Frota_manutencao_classificacao');
        $this->set('Frota_manutencao_classificacao', new Frota_manutencao_classificacao);
    }

    # recebe os dados enviados via post do cadastro de Frota_manutencao_classificacao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_manutencao_classificacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Frota_manutencao_classificacao');
        $Frota_manutencao_classificacao = new Frota_manutencao_classificacao();
        $this->set('Frota_manutencao_classificacao', $Frota_manutencao_classificacao);
        $_POST['dt_cadastro'] = date('Y-m-d H:i:s');
        $_POST['situacao_id'] = 1;
        $_POST['cadastradopor'] = Session::get('user')->id;
        try {
            $Frota_manutencao_classificacao->save($_POST);
            new Msg(__('Frota_manutencao_classificacao cadastrado com sucesso'));
            $this->go('Frota_manutencao_classificacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Frota_manutencao_classificacao
    # renderiza a visão /view/Frota_manutencao_classificacao/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_manutencao_classificacao');
        try {
            $this->set('Frota_manutencao_classificacao', new Frota_manutencao_classificacao((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_manutencao_classificacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_manutencao_classificacao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_manutencao_classificacao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_manutencao_classificacao');
        try {
            $Frota_manutencao_classificacao = new Frota_manutencao_classificacao((int) $_POST['id']);
            $this->set('Frota_manutencao_classificacao', $Frota_manutencao_classificacao);
            $_POST['dt_atualizacao'] = date('Y-m-d H:i:s');
            $_POST['modificadopor'] = Session::get('user')->id;
            $Frota_manutencao_classificacao->save($_POST);
            new Msg(__('Frota_manutencao_classificacao atualizado com sucesso'));
            $this->go('Frota_manutencao_classificacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Frota_manutencao_classificacao
    # renderiza a /view/Frota_manutencao_classificacao/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_manutencao_classificacao');
        try {
            $this->set('Frota_manutencao_classificacao', new Frota_manutencao_classificacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_manutencao_classificacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_manutencao_classificacao
    # redireciona para Frota_manutencao_classificacao/all
    function post_delete(){
        try {
            $Frota_manutencao_classificacao = new Frota_manutencao_classificacao((int) $_POST['id']);
            $Frota_manutencao_classificacao->situacao_id = 3;
            $Frota_manutencao_classificacao->save();
            new Msg(__('Frota_manutencao_classificacao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_manutencao_classificacao', 'all');
    }

}