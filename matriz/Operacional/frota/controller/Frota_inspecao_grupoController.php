<?php
final class Frota_inspecao_grupoController extends AppController{ 

    # página inicial do módulo Frota_inspecao_grupo
    function index(){
        $this->setTitle('Visualização de Frota_inspecao_grupo');
    }

    # lista de Frota_inspecao_grupos
    # renderiza a visão /view/Frota_inspecao_grupo/all.php
    function all(){
        $this->setTitle('Listagem de Frota_inspecao_grupo');
        $p = new Paginate('Frota_inspecao_grupo', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id', '=', 1);
        $this->set('Frota_inspecao_grupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Frota_inspecao_grupo
    # renderiza a visão /view/Frota_inspecao_grupo/view.php
    function view(){
        $this->setTitle('Visualização de Frota_inspecao_grupo');
        try {
            $this->set('Frota_inspecao_grupo', new Frota_inspecao_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao_grupo', 'all');
        }
    }

    # formulário de cadastro de Frota_inspecao_grupo
    # renderiza a visão /view/Frota_inspecao_grupo/add.php
    function add(){
        $this->setTitle('Cadastro de Frota_inspecao_grupo');
        $this->set('Frota_inspecao_grupo', new Frota_inspecao_grupo);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_inspecao_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao_grupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Frota_inspecao_grupo');
        $Frota_inspecao_grupo = new Frota_inspecao_grupo();
        $this->set('Frota_inspecao_grupo', $Frota_inspecao_grupo);
        try {
            $Frota_inspecao_grupo->situacao_id = 1;
            $Frota_inspecao_grupo->cadastrado_por = Session::get('user')->id;
            $Frota_inspecao_grupo->dt_cadastro = date('Y-m-d H:i:d');
            $Frota_inspecao_grupo->save($_POST);
            new Msg(__('Frota_inspecao_grupo cadastrado com sucesso'));
            $this->go('Frota_inspecao_grupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_inspecao_grupo
    # renderiza a visão /view/Frota_inspecao_grupo/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_inspecao_grupo');
        try {
            $this->set('Frota_inspecao_grupo', new Frota_inspecao_grupo((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_inspecao_grupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_inspecao_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao_grupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_inspecao_grupo');
        try {
            $Frota_inspecao_grupo = new Frota_inspecao_grupo((int) $_POST['id']);
            $this->set('Frota_inspecao_grupo', $Frota_inspecao_grupo);
            $Frota_inspecao_grupo->modificado_por = Session::get('user')->id;
            $Frota_inspecao_grupo->dt_modificado = date('Y-m-d H:i:d');
            $Frota_inspecao_grupo->save($_POST);
            new Msg(__('Frota_inspecao_grupo atualizado com sucesso'));
            $this->go('Frota_inspecao_grupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_inspecao_grupo
    # renderiza a /view/Frota_inspecao_grupo/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_inspecao_grupo');
        try {
            $this->set('Frota_inspecao_grupo', new Frota_inspecao_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao_grupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_inspecao_grupo
    # redireciona para Frota_inspecao_grupo/all
    function post_delete(){
        try {
            $Frota_inspecao_grupo = new Frota_inspecao_grupo((int) $_POST['id']);
            $Frota_inspecao_grupo->situacao_id = 3;
            $Frota_inspecao_grupo->save();
            new Msg(__('Frota_inspecao_grupo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_inspecao_grupo', 'all');
    }

}