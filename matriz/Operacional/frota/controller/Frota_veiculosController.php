<?php

final class Frota_veiculosController extends AppController
{


    function mes()
    {
        $mes = array();
        $mes[1] = "Jan";
        $mes[2] = "Fev";
        $mes[3] = "Mar";
        $mes[4] = "Abr";
        $mes[5] = "Mai";
        $mes[6] = "Jun";
        $mes[7] = "Jul";
        $mes[8] = "Ago";
        $mes[9] = "Set";
        $mes[10] = "Out";
        $mes[11] = "Nov";
        $mes[12] = "Dez";
        return $mes;
    }

    function total()
    {
        $this->setTitle('Gastos');
        $this->set('meses', $this->mes());
        $totais = array();
        if (!empty($_POST['ano'])) {
            $ano = $_POST['ano'];
        } else {
            $ano = date('Y');
        }

        for ($i = 1; $i <= 12; $i++) {
            if (Frota_abastecimentos::getTotalAbastecimento($ano, $i) != NULL) {
                $totalAbs[$i] = Frota_abastecimentos::getTotalAbastecimento($ano, $i);
            } else {
                $totalAbs[$i] = '0';
            }
        }

        for ($i = 1; $i <= 12; $i++) {
            if (Frota_manutencoes::getTotalManutencoes($ano, $i) != NULL) {
                $totalMan[$i] = Frota_manutencoes::getTotalManutencoes($ano, $i);
            } else {
                $totalMan[$i] = '0';
            }
        }

        $this->set('abastecimentos', $totalAbs);
        $this->set('manutencoes', $totalMan);
    }

    # página inicial do módulo Frota_veiculos
    function index()
    {
        $this->setTitle('Veículos');
    }

    function transferir()
    {

        $this->setTitle('Transferir Veículo');
        try {
            $this->set('Frota_veiculos', new Frota_veiculos((int)$this->getParam('id')));
            $this->set('Frota_localizacao', Frota_localizacao::getList());
            $this->set('Frota_ocorrencias', new Frota_ocorrencias());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Frota_veiculos', 'all');
        }

    }

    function post_transferir()
    {

        $this->setTitle('Transferir Veículo');
        try {
            //Editar Veículo
            $Frota_veiculos = new Frota_veiculos((int)$_POST['id']);
            $this->set('Frota_veiculos', $Frota_veiculos);
            $Frota_veiculos->localizacao_id = $_POST['localizacao_id'];
            $Frota_veiculos->save();

            //Geração de ocorrência
            $Frota_ocorrencias = new Frota_ocorrencias();
            $Frota_ocorrencias->usuario_id = Session::get('user')->id;
            $Frota_ocorrencias->veiculo_id = (int)$_POST['id'];

            //Tipo de ocorrência 2 = transferência;
            $Frota_ocorrencias->tipo_ocorrencia_id = 2;
            $Frota_ocorrencias->dt_ocorrencia = date('Y-m-d');
            if (!empty($_POST['descricao'])) {
                $Frota_ocorrencias->descricao = $_POST['descricao'];
            } else {
                $Frota_ocorrencias->descricao = "Transferência de veículo";
            }

            $Frota_ocorrencias->save();

            new Msg(__('Veículo transferido com sucesso. Registrada ocorrência de transferência.'));
            $this->go('Frota_veiculos', 'all');

        } catch (Exception $e) {
            new Msg(__('Não foi possível efetuar a transferência.'), 2);
        }

        $this->set('Frota_localizacao', Frota_localizacao::getList());

    }

    # lista de Frota_veiculos
    # renderiza a visão /view/Frota_veiculos/all.php
    function all()
    {
        $this->setTitle('Listagem de Veículos');
        $p = new Paginate('Frota_veiculos', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
       /* if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }*/

        $c->setOrder('situacao_id');
       // $c->addCondition('situacao_id', '!=', '3');
        $this->set('Frota_veiculos', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Frota_origens', Frota_origem::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Situacaos', Situacao::getList());

        $b = new Criteria();
        $b->addCondition('situacao_id', '=', '1');
        $b->setOrder("cidade ASC");
        $this->set('Frota_localizacao', Frota_localizacao::getList($b));
        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $a->setOrder("nome ASC");
        $this->set('Frota_motoristas', Frota_motorista::getList($a));

    }

    # visualiza um(a) Frota_veiculos
    # renderiza a visão /view/Frota_veiculos/view.php
    function view()
    {
        $this->setTitle('Veículos');
        try {
            $this->set('Frota_veiculos', new Frota_veiculos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_veiculos', 'all');
        }
    }

    # formulário de cadastro de Frota_veiculos
    # renderiza a visão /view/Frota_veiculos/add.php
    function add()
    {
        $this->setTitle('Cadastro de Veículos');
        $this->set('Frota_veiculos', new Frota_veiculos);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        $c->setOrder("descricao ASC");
        $this->set('Frota_origens', Frota_origem::getList($c));
        $this->set('Usuarios', Usuario::getList());
        $b = new Criteria();
        $b->addCondition('situacao_id', '=', 1);
        $b->setOrder("cidade");
        $this->set('Frota_localizacao', Frota_localizacao::getList($b));
        $this->set('Situacaos', Situacao::getList());
        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $a->setOrder("nome ASC");
        $this->set('Frota_motoristas', Frota_motorista::getList($a));

        $criteriaGrupo = new Criteria();
        $criteriaGrupo->addCondition("situacao_id", "=", 1);
        $this->set("Fin_projeto_grupo", Fin_projeto_grupo::getList($criteriaGrupo));
    }

    # recebe os dados enviados via post do cadastro de Frota_veiculos
    # (true)redireciona ou (false) renderiza a visão /view/Frota_veiculos/add.php
    function post_add()
    {
        //var_dump($_POST['vencimento_proximo_ipva']);exit;
        $this->setTitle('Cadastro de Veículos');
        $Frota_veiculos = new Frota_veiculos();
        $this->set('Frota_veiculos', $Frota_veiculos);
        //Vincula usuario logado ao cadastro
        $_POST['usuario_id'] = Session::get('user')->id;
        //Quilometragem atual inicia no cadastro de veículo, com a quilometragem
        $_POST['quilometragem_atual'] = $_POST['quilometragem'];
        //$_POST['situacao_id'] = 1;
        $Frota_veiculos->dt_cadastro = date('Y-m-d H:i:s');
        try {
             if (empty($_POST['motorista_id'])) {
                $_POST['motorista_id'] = 684;
             }
            $Frota_veiculos->save($_POST);
           
            //Verifica se foi preenchido motorista no cadastro de veiculo
            if (!empty($_POST['motorista_id'])) {
                 //salva o historico do veiculo e os motoristas
                 $historico = new Frota_historico_motorista_veiculo();
                 $historico->veiculo_id = $Frota_veiculos->id;
                 $historico->motorista_antigo_id = $Frota_veiculos->motorista_id;
                 $historico->cadastrado_por = Session::get('user')->id;
                 $historico->ultima_data = date('Y-m-d H:i:s');
                 $historico->dt_cadastro = date('Y-m-d H:i:s');
                 $historico->situacao_id = 1;
                 $historico->save();

                $Motorista = new Frota_motorista($_POST['motorista_id']);                   
                //Verifica se o motorista selecionado já possui um carro vinculado a ele
                if (!empty($Motorista->veiculo_id)) {
                    $Veiculo = new Frota_veiculos($Motorista->veiculo_id);
                    //Caso haja um veiculo vinculado ao motorista, é gerada uma ocorrência de remanejamento de motorista, no veiculo atual e de onde ele saiu
                    $Frota_ocorrencias = new Frota_ocorrencias();
                    $Frota_ocorrencias->usuario_id = Session::get('user')->id;
                    $Frota_ocorrencias->veiculo_id = $Veiculo->id;
                    //Tipo de ocorrência 3 = ajuste de motorista;
                    $Frota_ocorrencias->tipo_ocorrencia_id = 3;
                    $Frota_ocorrencias->dt_ocorrencia = date('Y-m-d');
                    if (!empty($_POST['descricao'])) {
                        $Frota_ocorrencias->descricao = $_POST['descricao'];
                    } else {
                        $Frota_ocorrencias->descricao = "Designação / Mudança de motorista. Motorista remanejado: "
                            . $Motorista->nome;
                    }
                    $Frota_ocorrencias->situacao_id = 1;
                    $Frota_ocorrencias->save();

                    //No novo veículo

                    $Frota_ocorrencias2 = new Frota_ocorrencias();
                    $Frota_ocorrencias2->usuario_id = Session::get('user')->id;
                    $Frota_ocorrencias2->veiculo_id = $Frota_veiculos->id;
                    //Tipo de ocorrência 3 = ajuste de motorista;
                    $Frota_ocorrencias2->tipo_ocorrencia_id = 3;
                    $Frota_ocorrencias2->dt_ocorrencia = date('Y-m-d');
                    if (!empty($_POST['descricao'])) {
                        $Frota_ocorrencias2->descricao = $_POST['descricao'];
                    } else {
                        $Frota_ocorrencias2->descricao = "Designação / Mudança de motorista. Motorista Atual: "
                            . $Motorista->nome . " - Veiculo Anterior: " . $Veiculo->placa;
                    }
                    $Frota_ocorrencias2->situacao_id = 1;
                    $Frota_ocorrencias2->save();

                    //O motorista é apagado do veículo a que estava vinculado antes
                    $Veiculo->motorista_id = NULL;
                    $Veiculo->save();

                    //O motorista é vinculado ao novo veiculo salvo.
                    $Motorista->veiculo_id = $Frota_veiculos->id;
                    $Motorista->save();

                } else {

                    //Caso seja um motorista sem nenhum veículo vinculado, o novo veiculo é salvo nele normalmente,

                    $Motorista->veiculo_id = $Frota_veiculos->id;
                    $Motorista->save();

                    //E também gerada uma ocorrência de designação de motorista novo ao veículo

                    $Frota_ocorrencias = new Frota_ocorrencias();
                    $Frota_ocorrencias->usuario_id = Session::get('user')->id;
                    $Frota_ocorrencias->veiculo_id = $Frota_veiculos->id;
                    //Tipo de ocorrência 3 = ajuste de motorista;
                    $Frota_ocorrencias->tipo_ocorrencia_id = 3;
                    $Frota_ocorrencias->dt_ocorrencia = date('Y-m-d');
                    if (!empty($_POST['descricao'])) {
                        $Frota_ocorrencias->descricao = $_POST['descricao'];
                    } else {
                        $Frota_ocorrencias->descricao = "Designação / Mudança de motorista. Novo Motorista: " . $Motorista->nome;
                    }
                    $Frota_ocorrencias->situacao_id = 1;
                    $Frota_ocorrencias->save();
                }
            }

            new Msg(__('Veículo cadastrado com sucesso'));
            $this->go('Frota_veiculos', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Frota_origens', Frota_origem::getList());
        $this->set('Situacaos', Situacao::getList());
    }

    # formulário de edição de Frota_veiculos
    # renderiza a visão /view/Frota_veiculos/edit.php
    function edit()
    {
        $this->setTitle('Editar Veículo');
        try {
            $this->set('Frota_veiculos', new Frota_veiculos((int)$this->getParam('id')));
            $Veiculo = new Frota_veiculos((int)$this->getParam('id'));

            $c = new Criteria();
            $c->addCondition('situacao_id', '=', 1);
            $c->setOrder("descricao ASC");
            $this->set('Frota_origens', Frota_origem::getList($c));
            $this->set('Usuarios', Usuario::getList());

            $b = new Criteria();
            $b->addCondition('situacao_id', '=', 1);
            $b->setOrder("cidade ASC");
            $this->set('Frota_localizacao', Frota_localizacao::getList($b));
            $this->set('Situacaos', Situacao::getList());

            $a = new Criteria();
            $a->addCondition('status', '=', 1);
            $a->setOrder("nome ASC");
            $this->set('Frota_motoristas', Frota_motorista::getList($a));


            $sql =  "Select * From view_grupo_empresa_unidade WHERE unidade_id = :unidade";
            $db = $this::getConn();
            $db->query($sql);
            $db->bind(":unidade",$Veiculo->fin_projeto_unidade_id);
            $dados = $db->getResults();
            $dados = $dados[0];

            $criteriaGrupo = new Criteria();
            $criteriaGrupo->addCondition("situacao_id", "=", 1);
            $this->set("Fin_projeto_grupo", Fin_projeto_grupo::getList($criteriaGrupo));

            $criteriaEmpresa = new Criteria();
            $criteriaEmpresa->addCondition("situacao_id", "=", 1);
            $criteriaEmpresa->addCondition("fin_projeto_grupo_id", "=", $dados->grupo_id);
            $this->set("Fin_projeto_empresa", Fin_projeto_empresa::getList($criteriaEmpresa));

            $criteriaUnidade = new Criteria();
            $criteriaUnidade->addCondition("situacao_id", "=", 1);
            $criteriaUnidade->addCondition("fin_projeto_empresa_id", "=", $dados->empresa_id);
            $this->set("Fin_projeto_unidade", Fin_projeto_unidade::getList($criteriaUnidade));

            $this->set("Fin_projeto", $dados);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Frota_veiculos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_veiculos
    # (true)redireciona ou (false) renderiza a visão /view/Frota_veiculos/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Veículo');
        try {
            $Frota_veiculos = new Frota_veiculos((int)$_POST['id']);
            $this->set('Frota_veiculos', $Frota_veiculos);
            $_POST['alterado_em'] = date_timestamp_get();
            $_POST['alterado_por'] = Session::get('user')->id;
            if (!empty($Frota_veiculos->motorista_id)) {
                $motorista_id = $Frota_veiculos->motorista_id;
            }
            $Frota_veiculos->save($_POST);

            if (!empty($_POST['motorista_id']) && $_POST['motorista_id'] != $motorista_id) {
                 //salva o historico do veiculo e os motoristas
                 $historico = new Frota_historico_motorista_veiculo();
                 $historico->veiculo_id = $Frota_veiculos->id;
                 $historico->motorista_antigo_id = $Frota_veiculos->motorista_id;
                 $historico->cadastrado_por = Session::get('user')->id;
                 $historico->ultima_data = date('Y-m-d H:i:s');
                 $historico->dt_cadastro = date('Y-m-d H:i:s');
                 $historico->situacao_id = 1;
                 $historico->save();
                 
                $Motorista = new Frota_motorista($_POST['motorista_id']);
                $MotoristaAntigo = new Frota_motorista($motorista_id);

                //Verifica se o motorista selecionado já possui um carro vinculado a ele
                if (!empty($Motorista->veiculo_id)) {

                    $Veiculo = new Frota_veiculos($Motorista->veiculo_id);

                    //Caso haja um veiculo vinculado ao motorista, é gerada uma ocorrência de remanejamento de motorista, no veiculo atual e de onde ele saiu
                    $Frota_ocorrencias = new Frota_ocorrencias();
                    $Frota_ocorrencias->usuario_id = Session::get('user')->id;
                    $Frota_ocorrencias->veiculo_id = $Veiculo->id;
                    //Tipo de ocorrência 3 = ajuste de motorista;
                    $Frota_ocorrencias->tipo_ocorrencia_id = 3;
                    $Frota_ocorrencias->dt_ocorrencia = date('Y-m-d');
                    if (!empty($_POST['descricao'])) {
                        $Frota_ocorrencias->descricao = $_POST['descricao'];
                    } else {
                        $Frota_ocorrencias->descricao = "Designação / Mudança de motorista. Motorista remanejado: "
                            . $Motorista->nome;
                    }
                    $Frota_ocorrencias->situacao_id = 1;
                    $Frota_ocorrencias->save();

                    //No novo veículo

                    $Frota_ocorrencias2 = new Frota_ocorrencias();
                    $Frota_ocorrencias2->usuario_id = Session::get('user')->id;
                    $Frota_ocorrencias2->veiculo_id = $Frota_veiculos->id;
                    //Tipo de ocorrência 3 = ajuste de motorista;
                    $Frota_ocorrencias2->tipo_ocorrencia_id = 3;
                    $Frota_ocorrencias2->dt_ocorrencia = date('Y-m-d');
                    if (!empty($_POST['descricao'])) {
                        $Frota_ocorrencias2->descricao = $_POST['descricao'];
                    } else {
                        $Frota_ocorrencias2->descricao = "Designação / Mudança de motorista. Motorista Atual: "
                            . $Motorista->nome . " - Veiculo Anterior: " . $Veiculo->placa . " - Motorista Anterior: " . $MotoristaAntigo->nome;
                    }
                    $Frota_ocorrencias2->situacao_id = 1;
                    $Frota_ocorrencias2->save();

                    //O motorista é apagado do veículo a que estava vinculado antes
                    $Veiculo->motorista_id = NULL;
                    $Veiculo->save();

                    //O motorista é vinculado ao novo veiculo salvo.

                    $MotoristaAntigo->veiculo_id = NULL;
                    $MotoristaAntigo->save();

                    $Motorista->veiculo_id = $Frota_veiculos->id;
                    $Motorista->save();

                } else {

                    //Caso seja um motorista sem nenhum veículo vinculado, o novo veiculo é salvo nele normalmente,

                    $Motorista->veiculo_id = $Frota_veiculos->id;
                    $Motorista->save();

                    //E também gerada uma ocorrência de designação de motorista novo ao veículo

                    $Frota_ocorrencias = new Frota_ocorrencias();
                    $Frota_ocorrencias->usuario_id = Session::get('user')->id;
                    $Frota_ocorrencias->veiculo_id = $Frota_veiculos->id;
                    //Tipo de ocorrência 3 = ajuste de motorista;
                    $Frota_ocorrencias->tipo_ocorrencia_id = 3;
                    $Frota_ocorrencias->dt_ocorrencia = date('Y-m-d');
                    if (!empty($_POST['descricao'])) {
                        $Frota_ocorrencias->descricao = $_POST['descricao'];
                    } else {
                        $Frota_ocorrencias->descricao = "Designação / Mudança de motorista. Novo Motorista: " . $Motorista->nome;
                    }
                    $Frota_ocorrencias->situacao_id = 1;
                    $Frota_ocorrencias->save();
                }

            }

            new Msg(__('Veículo atualizado com sucesso'));
            $this->go('Frota_veiculos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_origens', Frota_origem::getList());
        $this->set('Situacaos', Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_veiculos
    # renderiza a /view/Frota_veiculos/delete.php
    function delete()
    {
        $this->setTitle('Apagar Frota_veiculos');
        try {
            $this->set('Frota_veiculos', new Frota_veiculos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_veiculos', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_veiculos
    # redireciona para Frota_veiculos/all
    function post_delete()
    {
        try {
            $Frota_veiculos = new Frota_veiculos((int)$_POST['id']);
            $Frota_veiculos->situacao_id = 3;
            $Frota_veiculos->save();
            new Msg(__('Veículo excluido com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Frota_veiculos', 'all');
    }

    function getMotorista()
    {
        $Motorista = new Frota_motorista($_POST['motorista_id']);

        if (!empty($Motorista->veiculo_id)) {
            echo 1;
        } else {
            echo 2;
        }
        exit;
    }

    function getMotoristaEdit()
    {
        $Motorista = new Frota_motorista($_POST['motorista_id']);

        if (!empty($Motorista->veiculo_id) && $Motorista->veiculo_id != $_POST['veiculo_id']) {
            echo 1;
        } else {
            echo 2;
        }
        exit;
    }

    function seguranca (){
    
            $this->setTitle('Listagem de Manutenções');
            $p = new Paginate('Frota_manutencoes', 10);
            $c = new Criteria();
            $c->addCondition('frota_manutencoes.situacao_id', '=', 1);
            $c->addCondition('frota_manutencoes.situacao_manutencao_id', '=', 4);
            $c->setOrder('frota_manutencoes.dt_manutencao DESC');
            $c->addCondition('veiculo_id', '=', (int) $_GET['id']);
            //Consulta de veiculos  situacao_manutencao_id
            $c->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_manutencoes.veiculo_id', 'INNER');
            if (!empty($_POST["filtro"])) {
                if (!empty($_POST["filtro"]["interno"])) {
                    foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                        $this->setParam($fl, $fv);
                        if (!empty($fv)) {
                            $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                        }
                    }
                }
                if (!empty($_POST["filtro"]["externo"])) {
                    foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                        $this->setParam($fl, $fv);
                        if (!empty($fv)) {
                            $c->addCondition($fl, "=", $fv);
                        }
                    }
                }
            }
    
            //Filtro por localização do veículo
            if (!empty($_POST['localizacao_id'])) {
                $c->addCondition('frota_veiculos.localizacao_id', '=', $_POST['localizacao_id']);
            }
    
            if ($this->getParam('orderBy')) {
                $c->setOrder($this->getParam('orderBy'));
            }
            $this->set('Frota_manutencoes', $p->getPage($c));
            $this->set('nav', $p->getNav());
    
            $a = new Criteria();
            $a->addCondition('situacao_id', '=', 1);
            $a->setOrder("placa ASC");
            $this->set('Frota_veiculos', Frota_veiculos::getList($a));
    
            $this->set('Usuarios', Usuario::getList());
            $this->set('Fornecedores', Fornecedor::getList());
    
            $b = new Criteria();
            $b->setOrder("descricao ASC");
            $this->set('Frota_tipo_manutencaos', Frota_tipo_manutencao::getList($b));
    
            $d = new Criteria();
            $d->setOrder("cidade ASC");
            $d->addCondition("situacao_id", "=", 1);
            $this->set('Frota_localizacao', Frota_localizacao::getList($d));
           
            $this->set('Frota_situacao_manutencao', Frota_situacao_manutencao::getList());
    
        
    }
    function alert_licenciamento(){
        $this->setTitle('Listagem de Veículos');
        $p = new Paginate('Frota_veiculos', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id','=', 1);
        $c->addCondition('licenciamento','=', date('Y-m'));

        $this->set('Frota_veiculos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }
    function alert_ipva(){
        $this->setTitle('Listagem de Veículos');
        $p = new Paginate('Frota_veiculos', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id','=', 1);
        $c->addCondition('vencimento_proximo_ipva', "LIKE", "%" . date('Y-m') . "%");

        $this->set('Frota_veiculos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }
}