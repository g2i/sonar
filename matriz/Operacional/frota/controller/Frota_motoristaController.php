<?php
final class Frota_motoristaController extends AppController{ 

    # página inicial do módulo Frota_motorista
    function index(){
        $this->setTitle('Visualizar Motorista');
    }

    function cnhs_vencidas() {


        $this->setTitle("Listagem de CNH's Vencidas");
        $p = new Paginate('Frota_motorista', 10);
        $c = new Criteria();

        $c->addCondition('frota_motorista.status', '=', 1);
        $c->addSqlConditions('frota_motorista.validade_cnh < CURRENT_DATE()');

        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {

                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        if (!empty($_POST['inicio'])) {
            $c->addCondition('validade_cnh', '>=', $_POST['inicio']);
        }


        if (!empty($_POST['fim'])) {
            $c->addCondition('validade_cnh', '<=', $_POST['fim']);
        }



        $a = new Criteria();
        $a->addCondition('situacao_id', '=', 1);
        $a->setOrder('placa ASC');
        $this->set('veiculos', Frota_veiculos::getList($a));

        $b = new Criteria();
        $b->setOrder('cidade ASC');
        $this->set('Frota_localizacao', Frota_localizacao::getList($b));

        $this->set('motoristas', $p->getPage($c));
        $this->set('nav', $p->getNav());

    }

    # lista de Frota_motoristas
    # renderiza a visão /view/Frota_motorista/all.php
    function all(){
        $this->setTitle('Listagem de Motoristas');
        $p = new Paginate('Frota_motorista', 10);
        $c = new Criteria();
        $c->addCondition('status', '=', 1);
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_motoristas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Frota_veiculos',  Frota_veiculos::getList());
    

    }

    # visualiza um(a) Frota_motorista
    # renderiza a visão /view/Frota_motorista/view.php
    function view(){
        $this->setTitle('Visualizar Motorista');
        try {
            $this->set('Frota_motorista', new Frota_motorista((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_motorista', 'all');
        }
    }

    # formulário de cadastro de Frota_motorista
    # renderiza a visão /view/Frota_motorista/add.php
    function add(){
        $this->setTitle('Cadastrar Motorista');
        $this->set('Frota_motorista', new Frota_motorista);
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_localizacao',  Frota_localizacao::getList());
        $this->set('Frota_situacao_habilitacao',  Frota_situacao_habilitacao::getList());

        $queryRhprofissional = new Criteria();// Rhprofissional
        $queryRhprofissional->addCondition('status', '=', 1);
        $queryRhprofissional->setOrder('nome ASC');
        $this->set('Rhprof', Rhprofissional::getList($queryRhprofissional));
    }

    # recebe os dados enviados via post do cadastro de Frota_motorista
    # (true)redireciona ou (false) renderiza a visão /view/Frota_motorista/add.php
    function post_add(){
        $this->setTitle('Cadastrar Motorista');
    try { 
        $Frota_motorista = new Frota_motorista();
        $this->set('Frota_motorista', $Frota_motorista);
        $_POST['status'] = 1;
        $Rhprofissiona = new Rhprofissional((int) $_POST['rhprofissional_id']);
        $_POST['nome'] = $Rhprofissiona->nome;
        
     
            $Frota_motorista->save($_POST);
            new Msg(__('Motorista cadastrado com sucesso'));
            $this->go('Frota_motorista', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
    }

    # formulário de edição de Frota_motorista
    # renderiza a visão /view/Frota_motorista/edit.php
    function edit(){
        $this->setTitle('Editar Motorista');
        try {
            $this->set('Frota_motorista', new Frota_motorista((int) $this->getParam('id')));
            $this->set('Frota_veiculos',  Frota_veiculos::getList());
            $this->set('Frota_localizacao',  Frota_localizacao::getList());
            $this->set('Frota_situacao_habilitacao',  Frota_situacao_habilitacao::getList());
            
            $queryRhprofissional = new Criteria();// Rhprofissional
            $queryRhprofissional->addCondition('status', '=', 1);
            $queryRhprofissional->setOrder('nome ASC');
        $this->set('Rhprof', Rhprofissional::getList($queryRhprofissional));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_motorista', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_motorista
    # (true)redireciona ou (false) renderiza a visão /view/Frota_motorista/edit.php
    function post_edit(){
        $this->setTitle('Editar Motorista');
        try {
            $Frota_motorista = new Frota_motorista((int) $_POST['id']);
            $this->set('Frota_motorista', $Frota_motorista);
            $_POST['dt_atualizacao'] = date('Y-m-d');
            $Rhprofissiona = new Rhprofissional((int) $_POST['rhprofissional_id']);
            $_POST['nome'] = $Rhprofissiona->nome;
            
            $Frota_motorista->save($_POST);
            new Msg(__('Motorista atualizado com sucesso'));
            $this->go('Frota_motorista', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_motorista
    # renderiza a /view/Frota_motorista/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_motorista');
        try {
            $this->set('Frota_motorista', new Frota_motorista((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_motorista', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_motorista
    # redireciona para Frota_motorista/all
    function post_delete(){
        try {
            $Frota_motorista = new Frota_motorista((int) $_POST['id']);
            $Frota_motorista->status = 3;
            $Frota_motorista->save();
            new Msg(__('Motorista apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_motorista', 'all');
    }
    function alert_cnh_vencimento(){
        $this->set('motoristas', Frota_motorista::getMotoristasVencimentoCNH());
    }

}