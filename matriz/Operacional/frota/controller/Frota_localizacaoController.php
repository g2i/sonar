<?php
final class Frota_localizacaoController extends AppController{ 

    # página inicial do módulo Frota_localizacao
    function index(){
        $this->setTitle('Visualizar Localização');
    }

    # lista de Frota_localizacaos
    # renderiza a visão /view/Frota_localizacao/all.php
    function all(){
        $this->setTitle('Listagem de Localizações');
        $p = new Paginate('Frota_localizacao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_localizacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

    

    }

    # visualiza um(a) Frota_localizacao
    # renderiza a visão /view/Frota_localizacao/view.php
    function view(){
        $this->setTitle('Visualizar Localização');
        try {
            $this->set('Frota_localizacao', new Frota_localizacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_localizacao', 'all');
        }
    }

    # formulário de cadastro de Frota_localizacao
    # renderiza a visão /view/Frota_localizacao/add.php
    function add(){
        $this->setTitle('Cadastro de Localizações');
        $this->set('Frota_localizacao', new Frota_localizacao);
    }

    # recebe os dados enviados via post do cadastro de Frota_localizacao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_localizacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Localizações');
        $Frota_localizacao = new Frota_localizacao();
        $this->set('Frota_localizacao', $Frota_localizacao);
        $_POST['situacao_id'] = 1;
        try {
            $Frota_localizacao->save($_POST);
            new Msg(__('Localização cadastrada com sucesso'));
            $this->go('Frota_localizacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Frota_localizacao
    # renderiza a visão /view/Frota_localizacao/edit.php
    function edit(){
        $this->setTitle('Editar Localização');
        try {
            $this->set('Frota_localizacao', new Frota_localizacao((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_localizacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_localizacao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_localizacao/edit.php
    function post_edit(){
        $this->setTitle('Editar Localização');
        try {
            $Frota_localizacao = new Frota_localizacao((int) $_POST['id']);
            $this->set('Frota_localizacao', $Frota_localizacao);
            $Frota_localizacao->save($_POST);
            new Msg(__('Frota_localizacao atualizado com sucesso'));
            $this->go('Frota_localizacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Frota_localizacao
    # renderiza a /view/Frota_localizacao/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_localizacao');
        try {
            $this->set('Frota_localizacao', new Frota_localizacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_localizacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_localizacao
    # redireciona para Frota_localizacao/all
    function post_delete(){
        try {
            $Frota_localizacao = new Frota_localizacao((int) $_POST['id']);
            $Frota_localizacao->situacao_id = 3;
            $Frota_localizacao->save();
            new Msg(__('Frota_localizacao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_localizacao', 'all');
    }

}