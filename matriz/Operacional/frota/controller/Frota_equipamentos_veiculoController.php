<?php
final class Frota_equipamentos_veiculoController extends AppController{

    # página inicial do módulo Frota_equipamentos_veiculo
    function index(){
        $this->setTitle('Equipamento Auxiliar');
    }

    # lista de Frota_equipamentos_veiculos
    # renderiza a visão /view/Frota_equipamentos_veiculo/all.php
    function all(){
        $this->setTitle('Listagem de Equipamentos');
        $p = new Paginate('Frota_equipamentos_veiculo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_equipamentos_veiculos', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Usuarios',  Usuario::getList());
        $this->set('Frota_veiculos',  Frota_veiculos::getList());


    }

    # visualiza um(a) Frota_equipamentos_veiculo
    # renderiza a visão /view/Frota_equipamentos_veiculo/view.php
    function view(){
        $this->setTitle('Equipamento Auxiliar');
        try {
            $this->set('Frota_equipamentos_veiculo', new Frota_equipamentos_veiculo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_equipamentos_veiculo', 'all');
        }
    }

    # formulário de cadastro de Frota_equipamentos_veiculo
    # renderiza a visão /view/Frota_equipamentos_veiculo/add.php
    function add(){
        $this->setTitle('Cadastro de Equipamentos');
        $this->set('Frota_equipamentos_veiculo', new Frota_equipamentos_veiculo);
        $this->set('Usuarios',  Usuario::getList());
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        $this->set('Frota_veiculos',  Frota_veiculos::getList($c));

        $queryTipoEquipamento = new Criteria();
        $queryTipoEquipamento->addCondition('situacao_id','=',1);
        $this->set('Frota_tipo_equipamentos', Frota_tipo_equipamento::getList($queryTipoEquipamento));
    }

    # recebe os dados enviados via post do cadastro de Frota_equipamentos_veiculo
    # (true)redireciona ou (false) renderiza a visão /view/Frota_equipamentos_veiculo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Equipamentos');
        $Frota_equipamentos_veiculo = new Frota_equipamentos_veiculo();
        $this->set('Frota_equipamentos_veiculo', $Frota_equipamentos_veiculo);
        $_POST['usuario_id'] = Session::get('user')->id;
        $_POST['situacao_id'] = 1;
        try {
            $Frota_equipamentos_veiculo->save($_POST);
            new Msg(__('Equipamento cadastrado com sucesso'));
            $this->go('Frota_equipamentos_veiculo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
    }

    # formulário de edição de Frota_equipamentos_veiculo
    # renderiza a visão /view/Frota_equipamentos_veiculo/edit.php
    function edit(){
        $this->setTitle('Editar Equipamento');
        try {
            $this->set('Frota_equipamentos_veiculo', new Frota_equipamentos_veiculo((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Frota_veiculos',  Frota_veiculos::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_equipamentos_veiculo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_equipamentos_veiculo
    # (true)redireciona ou (false) renderiza a visão /view/Frota_equipamentos_veiculo/edit.php
    function post_edit(){
        $this->setTitle('Editar Equipamento');
        try {
            $Frota_equipamentos_veiculo = new Frota_equipamentos_veiculo((int) $_POST['id']);
            $this->set('Frota_equipamentos_veiculo', $Frota_equipamentos_veiculo);
            $_POST['usuario_id'] = Session::get('user')->id;
            $Frota_equipamentos_veiculo->save($_POST);
            new Msg(__('Equipamento atualizado com sucesso'));
            $this->go('Frota_equipamentos_veiculo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }

        $this->set('Frota_veiculos',  Frota_veiculos::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_equipamentos_veiculo
    # renderiza a /view/Frota_equipamentos_veiculo/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_equipamentos_veiculo');
        try {
            $this->set('Frota_equipamentos_veiculo', new Frota_equipamentos_veiculo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_equipamentos_veiculo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_equipamentos_veiculo
    # redireciona para Frota_equipamentos_veiculo/all
    function post_delete(){
        try {
            $Frota_equipamentos_veiculo = new Frota_equipamentos_veiculo((int) $_POST['id']);
            $Frota_equipamentos_veiculo->situacao_id = 3;
            $Frota_equipamentos_veiculo->save();
            new Msg(__('Equipamento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_equipamentos_veiculo', 'all');
    }
//mostra a lista de equipamentos auxis com vencimento de 45 dias 
    function alert_equipamento_vencimento(){
        $this->set('equipamentos', Frota_equipamentos_veiculo::getEquipamentoVencimento());
    }
}