<?php

final class Frota_abastecimentosController extends AppController
{
    # página inicial do módulo Frota_abastecimentos
    function index()
    {
        $this->set('manutencoes', Frota_manutencoes::getVeiculosManutencoes());
        $this->set('motoristas', Frota_motorista::getMotoristasVencimentoCNH());
        $this->setTitle('Consumo Médio');
        $p = new Paginate('Frota_abastecimentos', 10);
        $c = new Criteria();

        //Consulta de veiculos
        $c->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_abastecimentos.veiculo_id', 'INNER');
        $c->addCondition('frota_veiculos.situacao_id', '=', 1);
        $c->addCondition('frota_abastecimentos.situacao_id', '=', 1);
        $c->setOrder('frota_abastecimentos.dt_abastecimento DESC');

        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {

                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        if (!empty($_POST['veiculo_id'])) {
            $c->addCondition('veiculo_id', '=', $_POST['veiculo_id']);
        }

        if (!empty($_POST['inicio'])) {
            $c->addCondition('dt_abastecimento', '>=', $_POST['inicio']);
        }


        if (!empty($_POST['fim'])) {
            $c->addCondition('dt_abastecimento', '<=', $_POST['fim']);
        }

        if (!empty($_POST['localizacao_id'])) {
            $c->addCondition('frota_veiculos.localizacao_id', '=', $_POST['localizacao_id']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }


        $this->set('abastecimentos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $a = new Criteria();
        $a->addCondition('situacao_id', '=', 1);
        $a->setOrder('placa ASC');
        $this->set('veiculos', Frota_veiculos::getList($a));

        $b = new Criteria();
        $b->setOrder('cidade ASC');
        $this->set('Frota_localizacao', Frota_localizacao::getList($b));

        //CONTADORES DA INDEX
        $contManut = $this->query_count("SELECT COUNT(fm.id) as total FROM frota_manutencoes fm 
        INNER JOIN frota_veiculos fv ON fm.veiculo_id = fv.id 
        INNER JOIN (SELECT veiculo_id, MAX(quilometragem) AS km FROM frota_abastecimentos 
        GROUP BY veiculo_id) AS tbl ON tbl.veiculo_id = fv.`id` 
        INNER JOIN frota_tipo_manutencao ftm ON fm.`tipo_manutencao_id` = ftm.id 
        WHERE (fm.`vida_util` + fm.`quilometragem`) - tbl.km <= 100 
        AND (fm.`vida_util` + fm.`quilometragem`) - tbl.km >= 0 AND fm.`situacao_id` = 1");
        $this->set('contManutencao', $contManut);

        //CNH vencidas com 60 dias
        $contCnhVencida = $this->query_count("SELECT COUNT(fm.id) as total FROM frota_motorista fm LEFT JOIN frota_veiculos fv 
         ON fv.`id` = fm.`veiculo_id` WHERE DATEDIFF(fm.validade_cnh, CURDATE()) <= 60 
         AND DATEDIFF(fm.validade_cnh, CURDATE()) >= 0 AND fm.`status` = 1
         AND fm.status = 1");
        $this->set('contCnhVencidas', $contCnhVencida);

        //Equipamento auxiliar com vencimento com 45 dias
        $contEquipamentoAux = $this->query_count("SELECT COUNT(fev.id) AS total  FROM `frota_equipamentos_veiculo` fev
          WHERE DATEDIFF(fev.validade_laudo_mecanico, CURDATE()) <= 60 
          AND DATEDIFF(fev.validade_laudo_mecanico, CURDATE()) >= 0 
          AND fev.situacao_id = 1");
        $this->set('contEquipamentAux', $contEquipamentoAux);

        $paginAc = new Paginate('Frota_acidente', 10);
        $queryContAcidente = new Criteria();
        $queryContAcidente->addCondition('situacao_acidente_id', '=', 1);
        $queryContAcidente->addCondition('situacao_id', '=', 1);
        $this->set('contAcidentes', $paginAc->getPage($queryContAcidente));
        $this->set('nav', $paginAc->getNav());

        $paginLicen = new Paginate('Frota_veiculos', 10);
        $queryContLicen = new Criteria();
        $queryContLicen->addCondition('situacao_id', '=', 1);
        $queryContLicen->addCondition('licenciamento', '=', date('Y-m'));
        $this->set('contLicen', $paginLicen->getPage($queryContLicen));

        $paginIpva = new Paginate('Frota_veiculos', 10);
        $queryContIpva = new Criteria();
        $queryContIpva->addCondition('situacao_id', '=', 1);
        $queryContIpva->addCondition('vencimento_proximo_ipva', "LIKE", "%" . date('Y-m') . "%");
        $this->set('ContIpva', $paginIpva->getPage($queryContIpva));
    }

    # lista de Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/all.php
    function all()
    {
        $this->setTitle('Listagem de Abastecimentos');
        $p = new Paginate('Frota_abastecimentos', 10);
        $c = new Criteria();
        $c->addCondition('frota_abastecimentos.situacao_id', '=', 1);
        $c->setOrder('frota_abastecimentos.dt_abastecimento DESC');
        //Consulta de veículos
        $c->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_abastecimentos.veiculo_id', 'INNER');

        if (!empty($_POST['veiculo_id'])) {
            $c->addSqlConditions("veiculo_id IN (" . implode(',', $_POST['veiculo_id']) . ")");
        }

        if (!empty($_POST['dt_abastecimento_inicio'])) {
            $c->addCondition('dt_abastecimento', '>=', $_POST['dt_abastecimento_inicio'] . ' 00:00:00');
        }

        if (!empty($_POST['dt_abastecimento_fim'])) {
            $c->addCondition('dt_abastecimento', '<=', $_POST['dt_abastecimento_fim'] . ' 23:59:59');
        }
        //Filtro por localização do veículo
        if (!empty($_POST['localizacao_id'])) {
            $c->addCondition('frota_veiculos.localizacao_id', '=', $_POST['localizacao_id']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_abastecimentos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $p = new Criteria();
        $p->addCondition('situacao_id', '=', '1');
        $this->set('Frota_veiculos', Frota_veiculos::getList($p));
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());

        $this->set('Frota_localizacao', Frota_localizacao::getList());
    }

    # visualiza um(a) Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/view.php
    function view()
    {
        $this->setTitle('Abastecimentos');
        try {
            $this->set('Frota_abastecimentos', new Frota_abastecimentos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_abastecimentos', 'all');
        }
    }

    # formulário de cadastro de Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/add.php
    function add()
    {
        $this->setTitle('Cadastro de Abastecimentos');
        $this->set('Frota_abastecimentos', new Frota_abastecimentos);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        $this->set('Frota_veiculos', Frota_veiculos::getList($c));
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $frotaMotoristaCriteria = new Criteria();
        $frotaMotoristaCriteria->addSqlConditions('rhprofissional_id IS NOT NULL');
        $frotaMotoristaCriteria->addCondition('status', '=', 1);
        $frotaMotoristaCriteria->setOrder('nome ASC');
        $this->set('Frota_motorista', Frota_motorista::getList($frotaMotoristaCriteria));
        $this->set('Frota_combustivel', Frota_combustivel::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_abastecimentos
    # (true)redireciona ou (false) renderiza a visão /view/Frota_abastecimentos/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Abastecimentos');
        $Frota_abastecimentos = new Frota_abastecimentos();
        $this->set('Frota_abastecimentos', $Frota_abastecimentos);

        $_POST['media_consumo'] = ($_POST['quilometragem'] - $_POST['quilometragem_anterior']) / $_POST['quantidade'];
        $_POST['usuario_id'] = Session::get('user')->id;
        $_POST['dt_cadastro'] = date_timestamp_get();
        $_POST['situacao_id'] = 1;
        try {
            $Frota_abastecimentos->save($_POST);
            new Msg(__('Abastecimento cadastrado com sucesso'));
            $this->go('Frota_abastecimentos', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Frota_motorista', Frota_motorista::getList());
    }

    # formulário de edição de Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/edit.php
    function edit()
    {
        $this->setTitle('Editar Abastecimento');
        try {
            $this->set('Frota_abastecimentos', new Frota_abastecimentos((int)$this->getParam('id')));
            $this->set('Frota_veiculos', Frota_veiculos::getList());
            $this->set('Usuarios', Usuario::getList());
            $this->set('Fornecedores', Fornecedor::getList());
            $frotaMotoristaCriteria = new Criteria();
            $frotaMotoristaCriteria->addSqlConditions('rhprofissional_id IS NOT NULL');
            $frotaMotoristaCriteria->addCondition('status', '=', 1);
            $frotaMotoristaCriteria->setOrder('nome ASC');
            $this->set('Frota_motorista', Frota_motorista::getList($frotaMotoristaCriteria));
            $this->set('Frota_combustivel', Frota_combustivel::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Frota_abastecimentos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_abastecimentos
    # (true)redireciona ou (false) renderiza a visão /view/Frota_abastecimentos/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Abastecimento');
        try {
            $Frota_abastecimentos = new Frota_abastecimentos((int)$_POST['id']);
            $this->set('Frota_abastecimentos', $Frota_abastecimentos);
            $veiculo = new Frota_veiculos((int)$_POST['veiculo_id']);
            $_POST['alterado_por'] = Session::get('user')->id;
            $_POST['alterado_em'] = date_timestamp_get();

            if (!empty($_POST['quilometragem_anterior'])) {
                $Frota_abastecimentos->media_consumo = ($_POST['quilometragem'] - $_POST['quilometragem_anterior']) / $_POST['quantidade'];
            }

            $Frota_abastecimentos->save($_POST);
            new Msg(__('Abastecimento atualizado com sucesso'));
            $this->go('Frota_abastecimentos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Frota_motorista', Frota_motorista::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_abastecimentos
    # renderiza a /view/Frota_abastecimentos/delete.php
    function delete()
    {
        $this->setTitle('Apagar Frota_abastecimentos');
        try {
            $this->set('Frota_abastecimentos', new Frota_abastecimentos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_abastecimentos', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_abastecimentos
    # redireciona para Frota_abastecimentos/all
    function post_delete()
    {
        try {
            $Frota_abastecimentos = new Frota_abastecimentos((int)$_POST['id']);
            $Frota_abastecimentos->situacao_id = 3;
            $Frota_abastecimentos->save();
            new Msg(__('Abastecimento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Frota_abastecimentos', 'all');
    }

    function getQuilometragem()
    {
        $veiculo = Frota_abastecimentos::getMaxKm((int)$_POST['veiculo']);
        echo $veiculo[0]->km_max;
        exit;
    }

    function contManutencoes()
    {
        $contManut = $this->query_count("SELECT COUNT(fm.id) as total FROM frota_manutencoes fm 
        INNER JOIN frota_veiculos fv ON fm.veiculo_id = fv.id 
        INNER JOIN (SELECT veiculo_id, MAX(quilometragem) AS km FROM frota_abastecimentos 
        GROUP BY veiculo_id) AS tbl ON tbl.veiculo_id = fv.`id` 
        INNER JOIN frota_tipo_manutencao ftm ON fm.`tipo_manutencao_id` = ftm.id 
        WHERE (fm.`vida_util` + fm.`quilometragem`) - tbl.km <= 100 
        AND (fm.`vida_util` + fm.`quilometragem`) - tbl.km >= 0 AND fm.`situacao_id` = 1");
        return $contManut;
    }


    function downloadExcel()
    {

        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="abastecimentos.csv"');
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');

        $csvFile = fopen('php://output', 'w+');

        $c = new Criteria();
        $c->addCondition('frota_abastecimentos.situacao_id', '=', 1);
        //Consulta de veículos
        $c->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_abastecimentos.veiculo_id', 'INNER');
        $c->setOrder('frota_veiculos.placa ASC , frota_abastecimentos.dt_abastecimento DESC');


        if (!empty($_GET['veiculo_id'])) {
            $c->addSqlConditions("veiculo_id IN ({$_GET['veiculo_id']})");
        }

        if (!empty($_GET['dt_abastecimento_inicio'])) {
            $c->addCondition('dt_abastecimento', '>=', $_GET['dt_abastecimento_inicio'] . ' 00:00:00');
        }

        if (!empty($_GET['dt_abastecimento_fim'])) {
            $c->addCondition('dt_abastecimento', '<=', $_GET['dt_abastecimento_fim'] . ' 23:59:59');
        }
        //Filtro por localização do veículo
        if (!empty($_GET['localizacao_id'])) {
            $c->addCondition('frota_veiculos.localizacao_id', '=', $_GET['localizacao_id']);
        }
        $abastecimentos = Frota_abastecimentos::getList($c);
        $dados = array();
        $somaDados = array();
        $count = 0;
        $oldPlaca = NULL;
        foreach ($abastecimentos AS $f) {
            $queryMotorista = $f->getFrota_veiculos()->getMotorista();
            $motorista = '';
            if (!empty($queryMotorista->rhprofissional_id))
                $motorista = $queryMotorista->getRhprofissional()->nome;
            $combustivel = '';
            if (!empty($f->combustivel_id)) {
                $combustivel = $f->getCombustivel()->nome;
            }
            $valor_litro = 0;
            if (!empty($f->valor_litro)) {
                $valor_litro = $f->valor_litro;
            }
            $km_litro = 0;
            $valor_km = 0;
            if (!empty($valor_litro)) {
                $km_litro = ($f->quilometragem - $f->quilometragem_anterior) / $f->valor_litro;
                $km_rodado = $f->quilometragem - $f->quilometragem_anterior;
                if ($km_rodado > 0)
                    $valor_km = ($f->valor_litro * $f->quantidade) / ($km_rodado);
                else
                    $valor_km = 0;
            }

            if (empty($oldPlaca)) {
                $oldPlaca = $f->getFrota_veiculos()->placa;
                $somaDados[$f->getFrota_veiculos()->placa]['Total de KM'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Qtd de Litros'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Valor por litro'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Valor Total'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['KM/Litros'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['KM rodado'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Valor/KM'] = 0;
            }

            $dados[$f->getFrota_veiculos()->placa][] = array(
                'Placa' => $f->getFrota_veiculos()->placa,
                'Data do Abastecimento' => DataBR($f->dt_abastecimento),
                'Tipo de Veículo' => $f->getFrota_veiculos()->modelo,
                'Localizacao' => $f->getFrota_veiculos()->getFrota_localizacao()->cidade,
                'Motorista' => $motorista,
                'Fornecedor' => $f->getFornecedor()->nome,
                'Tipo de combustivel' => $combustivel,
                'Qtd de Litros' => $f->quantidade,
                'Valor por litro' => 'R$ ' . sprintf('%0.3f', $valor_litro),
                'Valor total' => 'R$ ' . ($f->quantidade * $valor_litro),
                'KM anterior' => $f->quilometragem_anterior,
                'KM atual' => $f->quilometragem,
                'KM Rodado' => $f->quilometragem - $f->quilometragem_anterior,
                'KM/Litros' => sprintf('%0.3f', $km_litro),
                'Valor/KM' => sprintf('%0.3f', $valor_km),
                'Cadastrado Por' => $f->getUsuario()->login,
                'Cadastrado Em' => DataTimeBr($f->dt_cadastro),
            );

            if ($oldPlaca == $f->getFrota_veiculos()->placa) {
                $count += 1;
            } else {
                $somaDados[$f->getFrota_veiculos()->placa]['Total de KM'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Qtd de Litros'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Valor por litro'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Valor Total'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['KM/Litros'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['KM rodado'] = 0;
                $somaDados[$f->getFrota_veiculos()->placa]['Valor/KM'] = 0;

                $count = 1;
                $oldPlaca = $f->getFrota_veiculos()->placa;
            }
            $somaDados[$f->getFrota_veiculos()->placa]['Total de KM'] += $f->quilometragem - $f->quilometragem_anterior;
            $somaDados[$f->getFrota_veiculos()->placa]['Qtd de Litros'] += $f->quantidade;
            $somaDados[$f->getFrota_veiculos()->placa]['Valor por litro'] += $valor_litro;
            $somaDados[$f->getFrota_veiculos()->placa]['Valor Total'] += ($f->quantidade * $valor_litro);
            $somaDados[$f->getFrota_veiculos()->placa]['KM/Litros'] += $km_litro;
            $somaDados[$f->getFrota_veiculos()->placa]['KM rodado'] += $f->quilometragem - $f->quilometragem_anterior;
            $somaDados[$f->getFrota_veiculos()->placa]['Valor/KM'] += $valor_km;

            $somaDados[$f->getFrota_veiculos()->placa]['count'] = $count;


        }

//        força o excel usar a codificaçao utf8 NÃO APAGAR
        fputs($csvFile, "\xEF\xBB\xBF");
        if (!empty($dados)) {
            $header = array(
                'Placa',
                'Data do Abastecimento',
                'Tipo de Veículo',
                'Localizacao',
                'Motorista',
                'Fornecedor',
                'Tipo de combustivel',
                'Qtd de Litros',
                'Valor por litro',
                'Valor total',
                'KM anterior',
                'KM atual',
                'KM Rodado',
                'KM/Litros',
                'Valor/KM',
                'Cadastrado Por',
                'Cadastrado Em'
            );

            fputcsv($csvFile, $header, ';');
            $oldKey = null;
            $count = 0;
            foreach ($dados as $key => $values) {
                if (empty($oldKey)) {
                    $oldKey = $key;
                    $count = 1;
                }

                foreach ($values AS $newkey => $value) {
                    fputcsv($csvFile, $value, ';');
                }
                $count = count($values);
                if ($oldKey !== $key || $somaDados[$key]['count'] == $count ) {
                    $oldKey = $key;
                    $headerSoma = array(
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        'Qtde de Lts total:'. $somaDados[$key]['Qtd de Litros'],
                        'Media valor por Lts: R$ '.($somaDados[$key]['Qtd de Litros']/$somaDados[$key]['count']),
                        'Valor Total: R$ '.$somaDados[$key]['Valor Total'],
                        '',
                        '',
                        'Total KM rodado: '.$somaDados[$key]['Valor Total'],
                        'Media valor KM/litros: '. ($somaDados[$key]['KM/Litros']/$somaDados[$key]['count']),
                        'Media valor Valor/KM: '. ($somaDados[$key]['Valor/KM']/$somaDados[$key]['count']),
                        
                    );
                    fputcsv($csvFile, $headerSoma, ';');
                    fputcsv($csvFile, array(''), ';');
                }
            }
        }

        fclose($csvFile);
        exit();
    }
}