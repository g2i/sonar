<?php
final class Fornecedor_servico_produtoController extends AppController{ 

    # página inicial do módulo Fornecedor_servico_produto
    function index(){
        $this->setTitle('Listagem de Fornecedor_servico_produto');
        $p = new Paginate('Fornecedor_servico_produto', 40);
        $c = new Criteria();
        $queryFonecedor =  new Criteria();

        if(!empty($this->getParam('fornecedor'))){
             $c->addCondition('fornecedor_id', "=", $this->getParam('fornecedor'));
        }
        if(!empty($this->getParam('produto_servico'))){
            $filtro = true;
            $c->addCondition('produto_servico', "LIKE", "%" . $this->getParam('produto_servico') . "%");
        }

          $c->setOrder('id');
          if(!empty( $this->getParam('id'))){
              $c->addCondition('fornecedor_id', '=', $this->getParam('id'));
          }
          
          $c->addCondition('situacao_id', '=', 1);
     
        $this->set('Fornecedor_servico_produtos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $queryFonecedor->setOrder('nome');
        $queryFonecedor->addCondition('status', '=', 1);
        $this->set('Fornecedores',  Fornecedor::getList($queryFonecedor));
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # lista de Fornecedor_servico_produtos
    # renderiza a visão /view/Fornecedor_servico_produto/all.php
    function all(){
        $this->setTitle('Listagem de Fornecedor_servico_produto');
        $p = new Paginate('Fornecedor_servico_produto', 10);
        $c = new Criteria();
        $queryFonecedor =  new Criteria();

        if(!empty($this->getParam('fornecedor'))){
             $c->addCondition('fornecedor_id', "=", $this->getParam('fornecedor'));
        }
        if(!empty($this->getParam('produto_servico'))){
            $filtro = true;
            $c->addCondition('produto_servico', "LIKE", "%" . $this->getParam('produto_servico') . "%");
        }

          $c->setOrder('id');
          if(!empty( $this->getParam('id'))){
              $c->addCondition('fornecedor_id', '=', $this->getParam('id'));
          }
          
          $c->addCondition('situacao_id', '=', 1);
     
        $this->set('Fornecedor_servico_produtos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $queryFonecedor->setOrder('nome');
        $queryFonecedor->addCondition('status', '=', 1);
        $this->set('Fornecedores',  Fornecedor::getList($queryFonecedor));
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        

    }

    # visualiza um(a) Fornecedor_servico_produto
    # renderiza a visão /view/Fornecedor_servico_produto/view.php
    function view(){
        $this->setTitle('Visualização de Fornecedor_servico_produto');
        try {
            $this->set('Fornecedor_servico_produto', new Fornecedor_servico_produto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fornecedor_servico_produto', 'all');
        }
    }

    # formulário de cadastro de Fornecedor_servico_produto
    # renderiza a visão /view/Fornecedor_servico_produto/add.php
    function add(){
        $this->setTitle('Cadastro de Fornecedor_servico_produto');
        $this->set('Fornecedor_servico_produto', new Fornecedor_servico_produto);
        $queryFornecedor = new Criteria();
        $queryFornecedor->addCondition('status', '=', 1);
        $this->set('Fornecedores',  Fornecedor::getList($queryFornecedor));
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        
    }

    # recebe os dados enviados via post do cadastro de Fornecedor_servico_produto
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor_servico_produto/add.php
    function post_add(){
        $this->setTitle('Cadastro de Fornecedor_servico_produto');
        $Fornecedor_servico_produto = new Fornecedor_servico_produto();
        $this->set('Fornecedor_servico_produto', $Fornecedor_servico_produto);
        try {           
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Fornecedor_servico_produto->fornecedor_id = $this->getParam('id');
            $Fornecedor_servico_produto->situacao_id = 1;
            $Fornecedor_servico_produto->cadastrado_por =$user;
            $Fornecedor_servico_produto->dt_cadastro = date('Y-m-d H:i:s');
            $Fornecedor_servico_produto->save($_POST);
            new Msg(__('Serviço/produto cadastrado com sucesso'));
            if(!empty($_POST['modal'])) {
                echo 1;
                exit;
            }           
                 $this->go('Fornecedor_servico_produto', 'index'); 
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Fornecedores',  Fornecedor::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Fornecedor_servico_produto
    # renderiza a visão /view/Fornecedor_servico_produto/edit.php
    function edit(){
        $this->setTitle('Edição de Fornecedor_servico_produto');
        try {
            $this->set('Fornecedor_servico_produto', new Fornecedor_servico_produto((int) $this->getParam('id')));
            $this->set('Fornecedores',  Fornecedor::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Fornecedor_servico_produto', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Fornecedor_servico_produto
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor_servico_produto/edit.php
    function post_edit(){
        $this->setTitle('Edição de Fornecedor_servico_produto');
        $fornecedorId = $_POST['id_fornecedor'];
        try {
            $Fornecedor_servico_produto = new Fornecedor_servico_produto((int) $fornecedorId);
            $this->set('Fornecedor_servico_produto', $Fornecedor_servico_produto);
            $user=@Session::get("user_epi")->id;// para salvar o usuario que está fazendo
            $Fornecedor_servico_produto->modificado_por = $user;
            $Fornecedor_servico_produto->dt_modificado = date('Y-m-d H:i:s');
            $Fornecedor_servico_produto->save($_POST);
            new Msg(__('Serviço/produto atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Fornecedor_servico_produto', 'index');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Fornecedores',  Fornecedor::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }
    # Confirma a exclusão ou não de um(a) Fornecedor_servico_produto
    # renderiza a /view/Fornecedor_servico_produto/delete.php
    function delete(){
        $this->setTitle('Apagar Fornecedor_servico_produto');
        try {
            $this->set('Fornecedor_servico_produto', new Fornecedor_servico_produto((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fornecedor_servico_produto', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Fornecedor_servico_produto
    # redireciona para Fornecedor_servico_produto/all
    function post_delete(){
        try {
            $Fornecedor_servico_produto = new Fornecedor_servico_produto((int) $_POST['id']);
            $Fornecedor_servico_produto->situacao_id = 3;
            $Fornecedor_servico_produto->save($Fornecedor_servico_produto);
            new Msg(__('Serviço/produto apagado com sucesso'), 1);
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Fornecedor_servico_produto', 'index');
    }

}