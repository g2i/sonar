<?php
/**
 * Classe AppController
 * 
 * @author Miguel
 * @package \controller
 */

class AppController extends Controller {


    public function beforeRun()
    {
        $profissional = Session::get('user');

            if (!$profissional && (CONTROLLER != 'Login')) {
                switch(CONTROLLER){
                    default:
                        new Msg(__('Para continuar faça login',2));
                        $this->go('Login','login');
                        break;
                }
            }

        $this->set('user_',$profissional);
        # evitando Cross-site Scripting
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
        //$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
        
        # preservando as ordenações
        if ($this->getParam('orderBy'))
            Session::set(CONTROLLER . ACTION . APPKEY . '.orderBy', $this->getParam('orderBy'));
        $or = Session::get(CONTROLLER . ACTION . APPKEY . '.orderBy');
        if (!empty($or) && !($this->getParam('orderBy'))) {
            $this->setParam('orderBy',Session::get(CONTROLLER . ACTION . APPKEY . '.orderBy'));
        }
        setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
        date_default_timezone_set('America/Sao_Paulo');
    }
}

