<?php
final class Frota_origemController extends AppController{ 

    # página inicial do módulo Frota_origem
    function index(){
        $this->setTitle('Origens');
    }

    # lista de Frota_origens
    # renderiza a visão /view/Frota_origem/all.php
    function all(){
        var_dump(Session::get('user')->id);exit;
        $this->setTitle('Listagem de Origens');
        $p = new Paginate('Frota_origem', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_origens', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Frota_origem
    # renderiza a visão /view/Frota_origem/view.php
    function view(){
        $this->setTitle('Origens');
        try {
            $this->set('Frota_origem', new Frota_origem((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_origem', 'all');
        }
    }

    # formulário de cadastro de Frota_origem
    # renderiza a visão /view/Frota_origem/add.php
    function add(){
        $this->setTitle('Cadastro de Origens');
        $this->set('Frota_origem', new Frota_origem);
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_origem
    # (true)redireciona ou (false) renderiza a visão /view/Frota_origem/add.php
    function post_add(){
        $this->setTitle('Cadastro de Origens');
        $Frota_origem = new Frota_origem();
        $this->set('Frota_origem', $Frota_origem);
        $_POST['usuario_id'] = Session::get('user')->id;
        $_POST['situacao_id'] = 1;
        try {
            $Frota_origem->save($_POST);
            new Msg(__('Origem cadastrada com sucesso'));
            $this->go('Frota_origem', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }

    }

    # formulário de edição de Frota_origem
    # renderiza a visão /view/Frota_origem/edit.php
    function edit(){
        $this->setTitle('Editar Origem');
        try {
            $this->set('Frota_origem', new Frota_origem((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_origem', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_origem
    # (true)redireciona ou (false) renderiza a visão /view/Frota_origem/edit.php
    function post_edit(){
        $this->setTitle('Editar Origem');
        try {
            $Frota_origem = new Frota_origem((int) $_POST['id']);
            $this->set('Frota_origem', $Frota_origem);
            $_POST['usuario_id'] = Session::get('user')->id;
            $Frota_origem->save($_POST);
            new Msg(__('Origem atualizada com sucesso'));
            $this->go('Frota_origem', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Frota_origem
    # renderiza a /view/Frota_origem/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_origem');
        try {
            $this->set('Frota_origem', new Frota_origem((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_origem', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_origem
    # redireciona para Frota_origem/all
    function post_delete(){
        try {
            $Frota_origem = new Frota_origem((int) $_POST['id']);
            $Frota_origem->situacao_id = 3;
            $Frota_origem->save();
            new Msg(__('Origem apagada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_origem', 'all');
    }

}