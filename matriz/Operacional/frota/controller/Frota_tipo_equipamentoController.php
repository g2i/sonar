<?php
final class Frota_tipo_equipamentoController extends AppController{ 

    # página inicial do módulo Frota_tipo_equipamento
    function index(){
        $this->setTitle('Visualização de Frota_tipo_equipamento');
    }

    # lista de Frota_tipo_equipamentos
    # renderiza a visão /view/Frota_tipo_equipamento/all.php
    function all(){
        $this->setTitle('Listagem de Frota_tipo_equipamento');
        $p = new Paginate('Frota_tipo_equipamento', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        
        $c->addCondition('situacao_id','=',1);
        $this->set('Frota_tipo_equipamentos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Frota_tipo_equipamento
    # renderiza a visão /view/Frota_tipo_equipamento/view.php
    function view(){
        $this->setTitle('Visualização de Frota_tipo_equipamento');
        try {
            $this->set('Frota_tipo_equipamento', new Frota_tipo_equipamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_equipamento', 'all');
        }
    }

    # formulário de cadastro de Frota_tipo_equipamento
    # renderiza a visão /view/Frota_tipo_equipamento/add.php
    function add(){
        $this->setTitle('Cadastro de Frota_tipo_equipamento');
        $this->set('Frota_tipo_equipamento', new Frota_tipo_equipamento);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_tipo_equipamento
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_equipamento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Frota_tipo_equipamento');
        $Frota_tipo_equipamento = new Frota_tipo_equipamento();
        $this->set('Frota_tipo_equipamento', $Frota_tipo_equipamento);
        try {
            $Frota_tipo_equipamento->situacao_id = 1;
            $Frota_tipo_equipamento->cadastrado_por = Session::get('user')->id;
            $Frota_tipo_equipamento->dt_cadastro = date('Y-m-d H:i:d');
            $Frota_tipo_equipamento->save($_POST);
            new Msg(__('Frota_tipo_equipamento cadastrado com sucesso'));
            $this->go('Frota_tipo_equipamento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_tipo_equipamento
    # renderiza a visão /view/Frota_tipo_equipamento/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_tipo_equipamento');
        try {
            $this->set('Frota_tipo_equipamento', new Frota_tipo_equipamento((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_tipo_equipamento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_tipo_equipamento
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_equipamento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_tipo_equipamento');
        try {
            $Frota_tipo_equipamento = new Frota_tipo_equipamento((int) $_POST['id']);
            $this->set('Frota_tipo_equipamento', $Frota_tipo_equipamento);
            $Frota_tipo_equipamento->modificado_por = Session::get('user')->id;
            $Frota_tipo_equipamento->dt_modificado = date('Y-m-d H:i:d');
            $Frota_tipo_equipamento->save($_POST);
            new Msg(__('Frota_tipo_equipamento atualizado com sucesso'));
            $this->go('Frota_tipo_equipamento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_tipo_equipamento
    # renderiza a /view/Frota_tipo_equipamento/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_tipo_equipamento');
        try {
            $this->set('Frota_tipo_equipamento', new Frota_tipo_equipamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_equipamento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_tipo_equipamento
    # redireciona para Frota_tipo_equipamento/all
    function post_delete(){
        try {
            $Frota_tipo_equipamento = new Frota_tipo_equipamento((int) $_POST['id']);
            $Frota_tipo_equipamento->situacao_id = 3;
            $Frota_tipo_equipamento->save();
            new Msg(__('Frota_tipo_equipamento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_tipo_equipamento', 'all');
    }

}