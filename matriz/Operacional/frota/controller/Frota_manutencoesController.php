<?php

final class Frota_manutencoesController extends AppController
{

    # página inicial do módulo Frota_manutencoes
    function index()
    {
        $this->setTitle('Manutenções');
    }

    # lista de Frota_manutencoes
    # renderiza a visão /view/Frota_manutencoes/all.php
    function all()
    {
        $this->setTitle('Listagem de Manutenções');
        $p = new Paginate('Frota_manutencoes', 10);
        $c = new Criteria();
        $c->addCondition('frota_manutencoes.situacao_id', '=', 1);
        $c->setOrder('frota_manutencoes.dt_manutencao DESC');
        //Consulta de veiculos
        $c->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_manutencoes.veiculo_id', 'INNER');
       /* if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }*/
        $filtro = false;
        if(!empty($this->getParam('veiculo_id'))){
             $filtro = true;
            $c->addCondition('veiculo_id', "=", $this->getParam('veiculo_id'));
        }
        if(!empty($this->getParam('tipo_manutencao_id'))){
            $filtro = true;
            $c->addCondition('tipo_manutencao_id', "=", $this->getParam('tipo_manutencao_id'));
        }
        if(!empty($this->getParam('situacao_manutencao_id'))){
            $filtro = true;
            $c->addCondition('situacao_manutencao_id', "=", $this->getParam('situacao_manutencao_id'));
        }
        if(!empty($this->getParam('localizacao_id'))){
            $filtro = true;
            $c->addCondition('localizacao_id', "=", $this->getParam('localizacao_id'));
        }
        
        if(!empty($this->getParam('inicio'))){
            $filtro = true;
           
            $inicio = $this->getParam('inicio');
            $fim = $this->getParam('fim');
            $c->addCondition('dt_manutencao', ">=", $inicio);
            $c->addCondition('dt_manutencao', "<=", $fim);
        }

        //Filtro por localização do veículo
        if (!empty($_POST['localizacao_id'])) {
            $c->addCondition('frota_veiculos.localizacao_id', '=', $_POST['localizacao_id']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_manutencoes', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $a = new Criteria();
        $a->addCondition('situacao_id', '=', 1);
        $a->setOrder("placa ASC");
        $this->set('Frota_veiculos', Frota_veiculos::getList($a));

        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());

        $b = new Criteria();
        $b->setOrder("descricao ASC");
        $this->set('Frota_tipo_manutencaos', Frota_tipo_manutencao::getList($b));

        $d = new Criteria();
        $d->setOrder("cidade ASC");
        $d->addCondition("situacao_id", "=", 1);
        $this->set('Frota_localizacao', Frota_localizacao::getList($d));
       
        $this->set('Frota_situacao_manutencao', Frota_situacao_manutencao::getList());

    }

    # visualiza um(a) Frota_manutencoes
    # renderiza a visão /view/Frota_manutencoes/view.php
    function view()
    {
        $this->setTitle('Manutenções');
        try {
            $this->set('Frota_manutencoes', new Frota_manutencoes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_manutencoes', 'all');
        }
    }

    # formulário de cadastro de Frota_manutencoes
    # renderiza a visão /view/Frota_manutencoes/add.php
    function add()
    {
        
        $this->setTitle('Cadastro de Manutenção');
        $this->set('Frota_manutencoes', new Frota_manutencoes);
        $this->set('Frota_situacao_manutencao', Frota_situacao_manutencao::getList());

            $b = new Criteria();
            $c = new Criteria();
        if($_GET['id'] != 0){
            //filtro para manutenção que seja por veiculo/seguranca
            $b->setOrder("descricao ASC");
              //condicao para filtro, busca apenas manutencao que for de seguranca
            $b->addCondition('manutencao_classificacao_id', '=', 4);
            //filtro para 
            $c->addCondition('id', '=', $_GET['id']);

            $seguranca = 1;
            $this->set('seguranca', $seguranca );
        } else{
            $b->setOrder("descricao ASC");
            $c->setOrder("placa ASC");
            $c->addCondition('situacao_id', '=', 1);
        }

        $this->set('Frota_veiculos', Frota_veiculos::getList($c));
        $this->set('Usuarios', Usuario::getList());

        $a = new Criteria();
        $a->setOrder("nome ASC");
        $this->set('Fornecedores', Fornecedor::getList($a));
        $this->set('Frota_tipo_manutencaos', Frota_tipo_manutencao::getList($b));
       
    }

    # recebe os dados enviados via post do cadastro de Frota_manutencoes
    # (true)redireciona ou (false) renderiza a visão /view/Frota_manutencoes/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Manutenção');
        $Frota_manutencoes = new Frota_manutencoes();
        $this->set('Frota_manutencoes', $Frota_manutencoes);
        $veiculo = new Frota_veiculos((int)$_POST['veiculo_id']);
         $_POST['usuario_id'] = Session::get('user')->id;
        $_POST['dt_cadastro'] = date('Y-m-d H:i:s');
        $_POST['situacao_id'] = 1;
        $total = $_POST['vl_unitario'] * $_POST['quantidade'];
        $_POST['valor'] = $total;
        try {
//            if($Frota_manutencoes->save($_POST)) {
//                $veiculo->quilometragem_atual = $Frota_manutencoes->quilometragem;
//                $veiculo->save();
//            }
            $Frota_manutencoes->save($_POST);
            new Msg(__('Manutenção cadastrada com sucesso'));
            $this->go('Frota_manutencoes', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Frota_tipo_manutencaos', Frota_tipo_manutencao::getList());
    }

    # formulário de edição de Frota_manutencoes
    # renderiza a visão /view/Frota_manutencoes/edit.php
    function edit()
    {
        $this->setTitle('Editar Manutenção');
        try {
            $this->set('Frota_manutencoes', new Frota_manutencoes((int)$this->getParam('id')));
            $this->set('Frota_veiculos', Frota_veiculos::getList());
            $this->set('Usuarios', Usuario::getList());
            $this->set('Fornecedores', Fornecedor::getList());
            $this->set('Frota_situacao_manutencao', Frota_situacao_manutencao::getList());
            $this->set('Frota_tipo_manutencaos', Frota_tipo_manutencao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Frota_manutencoes', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_manutencoes
    # (true)redireciona ou (false) renderiza a visão /view/Frota_manutencoes/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Manutenção');
        try {
            $Frota_manutencoes = new Frota_manutencoes((int)$_POST['id']);
            $this->set('Frota_manutencoes', $Frota_manutencoes);
            $_POST['alterado_por'] = Session::get('user')->id;
            $_POST['alterado_em'] = date_timestamp_get();
            $total = $_POST['vl_unitario'] * $_POST['quantidade'];
            $_POST['valor'] = $total;
            $Frota_manutencoes->save($_POST);
            new Msg(__('Manutenção atualizada com sucesso'));
            $this->go('Frota_manutencoes', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Frota_tipo_manutencaos', Frota_tipo_manutencao::getList());
        $this->set('Frota_situacao_manutencao', Frota_situacao_manutencao::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_manutencoes
    # renderiza a /view/Frota_manutencoes/delete.php
    function delete()
    {
        $this->setTitle('Apagar Frota_manutencoes');
        try {
            $this->set('Frota_manutencoes', new Frota_manutencoes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_manutencoes', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_manutencoes
    # redireciona para Frota_manutencoes/all
    function post_delete()
    {
        try {
            $Frota_manutencoes = new Frota_manutencoes((int)$_POST['id']);
            $Frota_manutencoes->situacao_id = 3;
            $Frota_manutencoes->save();
            new Msg(__('Manutenção apagada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Frota_manutencoes', 'all');
    }

    function getQuilometragem()
    {
        $veiculo = new Frota_veiculos((int)$_POST['veiculo']);
        echo $veiculo->quilometragem_atual;
        exit;
    }
    function alert_manutencao(){
        $this->set('manutencoes', Frota_manutencoes::getVeiculosManutencoes());
    }
}