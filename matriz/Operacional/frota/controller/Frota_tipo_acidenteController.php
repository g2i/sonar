<?php
final class Frota_tipo_acidenteController extends AppController{ 

    # página inicial do módulo Frota_tipo_acidente
    function index(){
        $this->setTitle('Visualização de Frota_tipo_acidente');
    }

    # lista de Frota_tipo_acidentes
    # renderiza a visão /view/Frota_tipo_acidente/all.php
    function all(){
        $this->setTitle('Listagem de Frota_tipo_acidente');
        $p = new Paginate('Frota_tipo_acidente', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_tipo_acidentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Frota_tipo_acidente
    # renderiza a visão /view/Frota_tipo_acidente/view.php
    function view(){
        $this->setTitle('Visualização de Frota_tipo_acidente');
        try {
            $this->set('Frota_tipo_acidente', new Frota_tipo_acidente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_acidente', 'all');
        }
    }

    # formulário de cadastro de Frota_tipo_acidente
    # renderiza a visão /view/Frota_tipo_acidente/add.php
    function add(){
        $this->setTitle('Cadastro de Frota_tipo_acidente');
        $this->set('Frota_tipo_acidente', new Frota_tipo_acidente);
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_tipo_acidente
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_acidente/add.php
    function post_add(){
        $this->setTitle('Cadastro de Frota_tipo_acidente');
        $Frota_tipo_acidente = new Frota_tipo_acidente();
        $this->set('Frota_tipo_acidente', $Frota_tipo_acidente);
        $_POST['cadastradopor'] = Session::get('user')->id; //salva o usuario que esta cadastrando
        $Rhocorrencia->dt_cadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo*/
        try {
            $Frota_tipo_acidente->save($_POST);
            new Msg(__('Frota_tipo_acidente cadastrado com sucesso'));
            $this->go('Frota_tipo_acidente', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Frota_tipo_acidente
    # renderiza a visão /view/Frota_tipo_acidente/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_tipo_acidente');
        try {
            $this->set('Frota_tipo_acidente', new Frota_tipo_acidente((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_tipo_acidente', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_tipo_acidente
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_acidente/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_tipo_acidente');
        try {
            $Frota_tipo_acidente = new Frota_tipo_acidente((int) $_POST['id']);
            $this->set('Frota_tipo_acidente', $Frota_tipo_acidente);
            $Frota_tipo_acidente->save($_POST);
            new Msg(__('Frota_tipo_acidente atualizado com sucesso'));
            $this->go('Frota_tipo_acidente', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_tipo_acidente
    # renderiza a /view/Frota_tipo_acidente/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_tipo_acidente');
        try {
            $this->set('Frota_tipo_acidente', new Frota_tipo_acidente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_acidente', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_tipo_acidente
    # redireciona para Frota_tipo_acidente/all
    function post_delete(){
        try {
            $Frota_tipo_acidente = new Frota_tipo_acidente((int) $_POST['id']);
            $Frota_tipo_acidente->delete();
            new Msg(__('Frota_tipo_acidente apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_tipo_acidente', 'all');
    }

}