<?php
final class Frota_historico_motorista_veiculoController extends AppController{ 

    # página inicial do módulo Frota_historico_motorista_veiculo
    function index(){
        $this->setTitle('Visualização de Frota_historico_motorista_veiculo');
    }

    # lista de Frota_historico_motorista_veiculos
    # renderiza a visão /view/Frota_historico_motorista_veiculo/all.php
    function all(){
        $this->setTitle('Listagem de Frota_historico_motorista_veiculo');
        $p = new Paginate('Frota_historico_motorista_veiculo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
      
        $c->setOrder('id DESC');
        $c->addCondition('veiculo_id', '=', $this->getParam('id'));
        $this->set('Frota_historico_motorista_veiculos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_motoristas',  Frota_motorista::getList());
        $this->set('Situacaos',  Situacao::getList());
    

    }

    # visualiza um(a) Frota_historico_motorista_veiculo
    # renderiza a visão /view/Frota_historico_motorista_veiculo/view.php
    function view(){
        $this->setTitle('Visualização de Frota_historico_motorista_veiculo');
        try {
            $this->set('Frota_historico_motorista_veiculo', new Frota_historico_motorista_veiculo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_historico_motorista_veiculo', 'all');
        }
    }

    # formulário de cadastro de Frota_historico_motorista_veiculo
    # renderiza a visão /view/Frota_historico_motorista_veiculo/add.php
    function add(){
        $this->setTitle('Cadastro de Frota_historico_motorista_veiculo');
        $this->set('Frota_historico_motorista_veiculo', new Frota_historico_motorista_veiculo);
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_motoristas',  Frota_motorista::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_historico_motorista_veiculo
    # (true)redireciona ou (false) renderiza a visão /view/Frota_historico_motorista_veiculo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Frota_historico_motorista_veiculo');
        $Frota_historico_motorista_veiculo = new Frota_historico_motorista_veiculo();
        $this->set('Frota_historico_motorista_veiculo', $Frota_historico_motorista_veiculo);
        try {
            $Frota_historico_motorista_veiculo->save($_POST);
            new Msg(__('Frota_historico_motorista_veiculo cadastrado com sucesso'));
            $this->go('Frota_historico_motorista_veiculo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_motoristas',  Frota_motorista::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Frota_historico_motorista_veiculo
    # renderiza a visão /view/Frota_historico_motorista_veiculo/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_historico_motorista_veiculo');
        try {
            $this->set('Frota_historico_motorista_veiculo', new Frota_historico_motorista_veiculo((int) $this->getParam('id')));
            $this->set('Frota_veiculos',  Frota_veiculos::getList());
            $this->set('Frota_motoristas',  Frota_motorista::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_historico_motorista_veiculo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_historico_motorista_veiculo
    # (true)redireciona ou (false) renderiza a visão /view/Frota_historico_motorista_veiculo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_historico_motorista_veiculo');
        try {
            $Frota_historico_motorista_veiculo = new Frota_historico_motorista_veiculo((int) $_POST['id']);
            $this->set('Frota_historico_motorista_veiculo', $Frota_historico_motorista_veiculo);
            $Frota_historico_motorista_veiculo->save($_POST);
            new Msg(__('Frota_historico_motorista_veiculo atualizado com sucesso'));
            $this->go('Frota_historico_motorista_veiculo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_motoristas',  Frota_motorista::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_historico_motorista_veiculo
    # renderiza a /view/Frota_historico_motorista_veiculo/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_historico_motorista_veiculo');
        try {
            $this->set('Frota_historico_motorista_veiculo', new Frota_historico_motorista_veiculo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_historico_motorista_veiculo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_historico_motorista_veiculo
    # redireciona para Frota_historico_motorista_veiculo/all
    function post_delete(){
        try {
            $Frota_historico_motorista_veiculo = new Frota_historico_motorista_veiculo((int) $_POST['id']);
            $Frota_historico_motorista_veiculo->delete();
            new Msg(__('Frota_historico_motorista_veiculo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_historico_motorista_veiculo', 'all');
    }

}