<?php
final class Frota_situacao_acidenteController extends AppController{ 

    # página inicial do módulo Frota_situacao_acidente
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Frota_situacao_acidentes
    # renderiza a visão /view/Frota_situacao_acidente/all.php
    function all(){
        $this->setTitle('Listagem');
        $p = new Paginate('Frota_situacao_acidente', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Frota_situacao_acidentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Frota_situacao_acidente
    # renderiza a visão /view/Frota_situacao_acidente/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Frota_situacao_acidente', new Frota_situacao_acidente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_situacao_acidente', 'all');
        }
    }

    # formulário de cadastro de Frota_situacao_acidente
    # renderiza a visão /view/Frota_situacao_acidente/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Frota_situacao_acidente', new Frota_situacao_acidente);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_situacao_acidente
    # (true)redireciona ou (false) renderiza a visão /view/Frota_situacao_acidente/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Frota_situacao_acidente = new Frota_situacao_acidente();
        $this->set('Frota_situacao_acidente', $Frota_situacao_acidente);
        try {
            $Frota_situacao_acidente->situacao_id = 1;
            $Frota_situacao_acidente->cadastrado_por = Session::get('user')->id;
            $Frota_situacao_acidente->dt_cadastro = date('Y-m-d H:i:d');
            $Frota_situacao_acidente->save($_POST);
            new Msg(__('Frota_situacao_acidente cadastrado com sucesso'));
            $this->go('Frota_situacao_acidente', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_situacao_acidente
    # renderiza a visão /view/Frota_situacao_acidente/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Frota_situacao_acidente', new Frota_situacao_acidente((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_situacao_acidente', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_situacao_acidente
    # (true)redireciona ou (false) renderiza a visão /view/Frota_situacao_acidente/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Frota_situacao_acidente = new Frota_situacao_acidente((int) $_POST['id']);
            $this->set('Frota_situacao_acidente', $Frota_situacao_acidente);
            $Frota_situacao_acidente->modificado_por = Session::get('user')->id;
            $Frota_situacao_acidente->dt_modificado = date('Y-m-d H:i:d');
            $Frota_situacao_acidente->save($_POST);
            new Msg(__('Frota_situacao_acidente atualizado com sucesso'));
            $this->go('Frota_situacao_acidente', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_situacao_acidente
    # renderiza a /view/Frota_situacao_acidente/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_situacao_acidente');
        try {
            $this->set('Frota_situacao_acidente', new Frota_situacao_acidente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_situacao_acidente', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_situacao_acidente
    # redireciona para Frota_situacao_acidente/all
    function post_delete(){
        try {
            $Frota_situacao_acidente = new Frota_situacao_acidente((int) $_POST['id']);
            $Frota_situacao_acidente->situacao_id = 3;
            $Frota_situacao_acidente->save();
            new Msg(__('Frota_situacao_acidente apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_situacao_acidente', 'all');
    }

}