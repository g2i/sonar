<?php
final class Frota_tipo_ocorrenciaController extends AppController{ 

    # página inicial do módulo Frota_tipo_ocorrencia
    function index(){
        $this->setTitle('Tipo de Ocorrência');
    }

    # lista de Frota_tipo_ocorrencias
    # renderiza a visão /view/Frota_tipo_ocorrencia/all.php
    function all(){
        $this->setTitle('Listagem de Tipos de Ocorrência');
        $p = new Paginate('Frota_tipo_ocorrencia', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_tipo_ocorrencias', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Frota_tipo_ocorrencia
    # renderiza a visão /view/Frota_tipo_ocorrencia/view.php
    function view(){
        $this->setTitle('Tipo de Ocorrência');
        try {
            $this->set('Frota_tipo_ocorrencia', new Frota_tipo_ocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_ocorrencia', 'all');
        }
    }

    # formulário de cadastro de Frota_tipo_ocorrencia
    # renderiza a visão /view/Frota_tipo_ocorrencia/add.php
    function add(){
        $this->setTitle('Cadastro de Tipo de Ocorrência');
        $this->set('Frota_tipo_ocorrencia', new Frota_tipo_ocorrencia);
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_tipo_ocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_ocorrencia/add.php
    function post_add(){
        $this->setTitle('Cadastro de Tipo de Ocorrência');
        $Frota_tipo_ocorrencia = new Frota_tipo_ocorrencia();
        $this->set('Frota_tipo_ocorrencia', $Frota_tipo_ocorrencia);
        $_POST['usuario_id'] = Session::get('user')->id;
        $_POST['situacao_id'] = 1;
        try {
            $Frota_tipo_ocorrencia->save($_POST);
            new Msg(__('Tipo de Ocorrência cadastrada com sucesso'));
            $this->go('Frota_tipo_ocorrencia', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Frota_tipo_ocorrencia
    # renderiza a visão /view/Frota_tipo_ocorrencia/edit.php
    function edit(){
        $this->setTitle('Editar Tipo de Ocorrência');
        try {
            $this->set('Frota_tipo_ocorrencia', new Frota_tipo_ocorrencia((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_tipo_ocorrencia', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_tipo_ocorrencia
    # (true)redireciona ou (false) renderiza a visão /view/Frota_tipo_ocorrencia/edit.php
    function post_edit(){
        $this->setTitle('Editar Tipo de Ocorrência');
        try {
            $Frota_tipo_ocorrencia = new Frota_tipo_ocorrencia((int) $_POST['id']);
            $this->set('Frota_tipo_ocorrencia', $Frota_tipo_ocorrencia);
            $_POST['usuario_id'] = Session::get('user')->id;
            $Frota_tipo_ocorrencia->save($_POST);
            new Msg(__('Tipo de Ocorrência atualizada com sucesso'));
            $this->go('Frota_tipo_ocorrencia', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Frota_tipo_ocorrencia
    # renderiza a /view/Frota_tipo_ocorrencia/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_tipo_ocorrencia');
        try {
            $this->set('Frota_tipo_ocorrencia', new Frota_tipo_ocorrencia((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_tipo_ocorrencia', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_tipo_ocorrencia
    # redireciona para Frota_tipo_ocorrencia/all
    function post_delete(){
        try {
            $Frota_tipo_ocorrencia = new Frota_tipo_ocorrencia((int) $_POST['id']);
            $Frota_tipo_ocorrencia->situacao_id = 3;
            $Frota_tipo_ocorrencia->save();
            new Msg(__('Tipo de Ocorrência apagada com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_tipo_ocorrencia', 'all');
    }

}