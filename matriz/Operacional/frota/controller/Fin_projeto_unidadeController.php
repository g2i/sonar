<?php
final class Fin_projeto_unidadeController extends AppController{ 

    # página inicial do módulo Fin_projeto_unidade
    function index(){
        $this->setTitle('Visualização de Fin_projeto_unidade');
    }

    # lista de Fin_projeto_unidades
    # renderiza a visão /view/Fin_projeto_unidade/all.php
    function all(){
        $this->setTitle('Listagem de Fin_projeto_unidade');
        $p = new Paginate('Fin_projeto_unidade', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Fin_projeto_unidades', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Fin_projeto_unidade
    # renderiza a visão /view/Fin_projeto_unidade/view.php
    function view(){
        $this->setTitle('Visualização de Fin_projeto_unidade');
        try {
            $this->set('Fin_projeto_unidade', new Fin_projeto_unidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fin_projeto_unidade', 'all');
        }
    }

    # formulário de cadastro de Fin_projeto_unidade
    # renderiza a visão /view/Fin_projeto_unidade/add.php
    function add(){
        $this->setTitle('Cadastro de Fin_projeto_unidade');
        $this->set('Fin_projeto_unidade', new Fin_projeto_unidade);
        $this->set('Fin_projeto_empresas',  Fin_projeto_empresa::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Fin_projeto_unidade
    # (true)redireciona ou (false) renderiza a visão /view/Fin_projeto_unidade/add.php
    function post_add(){
        $this->setTitle('Cadastro de Fin_projeto_unidade');
        $Fin_projeto_unidade = new Fin_projeto_unidade();
        $this->set('Fin_projeto_unidade', $Fin_projeto_unidade);
        try {
            $Fin_projeto_unidade->save($_POST);
            new Msg(__('Fin_projeto_unidade cadastrado com sucesso'));
            $this->go('Fin_projeto_unidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Fin_projeto_empresas',  Fin_projeto_empresa::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Fin_projeto_unidade
    # renderiza a visão /view/Fin_projeto_unidade/edit.php
    function edit(){
        $this->setTitle('Edição de Fin_projeto_unidade');
        try {
            $this->set('Fin_projeto_unidade', new Fin_projeto_unidade((int) $this->getParam('id')));
            $this->set('Fin_projeto_empresas',  Fin_projeto_empresa::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Fin_projeto_unidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Fin_projeto_unidade
    # (true)redireciona ou (false) renderiza a visão /view/Fin_projeto_unidade/edit.php
    function post_edit(){
        $this->setTitle('Edição de Fin_projeto_unidade');
        try {
            $Fin_projeto_unidade = new Fin_projeto_unidade((int) $_POST['id']);
            $this->set('Fin_projeto_unidade', $Fin_projeto_unidade);
            $Fin_projeto_unidade->save($_POST);
            new Msg(__('Fin_projeto_unidade atualizado com sucesso'));
            $this->go('Fin_projeto_unidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Fin_projeto_empresas',  Fin_projeto_empresa::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Fin_projeto_unidade
    # renderiza a /view/Fin_projeto_unidade/delete.php
    function delete(){
        $this->setTitle('Apagar Fin_projeto_unidade');
        try {
            $this->set('Fin_projeto_unidade', new Fin_projeto_unidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fin_projeto_unidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Fin_projeto_unidade
    # redireciona para Fin_projeto_unidade/all
    function post_delete(){
        try {
            $Fin_projeto_unidade = new Fin_projeto_unidade((int) $_POST['id']);
            $Fin_projeto_unidade->delete();
            new Msg(__('Fin_projeto_unidade apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Fin_projeto_unidade', 'all');
    }

    function getListaUnidade(){
        $unidadeCriteria = new Criteria();
        $unidadeCriteria->addCondition('situacao_id','=',1);
        $unidadeCriteria->addCondition('fin_projeto_empresa_id','=',$this->getParam('fin_projeto_empresa_id'));

        $data = Fin_projeto_unidade::getList($unidadeCriteria);

        echo json_encode($data);
        exit();

    }

}