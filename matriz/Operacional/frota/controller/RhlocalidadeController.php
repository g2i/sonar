<?php
final class RhlocalidadeController extends AppController{ 

    # página inicial do módulo Rhlocalidade
    function index(){
        $this->setTitle('Visualização de Rhlocalidade');
    }

    # lista de Rhlocalidades
    # renderiza a visão /view/Rhlocalidade/all.php
    function all(){
        $this->setTitle('Listagem de Rhlocalidade');
        $p = new Paginate('Rhlocalidade', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhlocalidades', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhlocalidade
    # renderiza a visão /view/Rhlocalidade/view.php
    function view(){
        $this->setTitle('Visualização de Rhlocalidade');
        try {
            $this->set('Rhlocalidade', new Rhlocalidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhlocalidade', 'all');
        }
    }

    # formulário de cadastro de Rhlocalidade
    # renderiza a visão /view/Rhlocalidade/add.php
    function add(){
        $this->setTitle('Cadastro de Rhlocalidade');
        $this->set('Rhlocalidade', new Rhlocalidade);
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhlocalidade
    # (true)redireciona ou (false) renderiza a visão /view/Rhlocalidade/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rhlocalidade');
        $Rhlocalidade = new Rhlocalidade();
        $this->set('Rhlocalidade', $Rhlocalidade);
        try {
            $Rhlocalidade->save($_POST);
            new Msg(__('Rhlocalidade cadastrado com sucesso'));
            $this->go('Rhlocalidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Rhlocalidade
    # renderiza a visão /view/Rhlocalidade/edit.php
    function edit(){
        $this->setTitle('Edição de Rhlocalidade');
        try {
            $this->set('Rhlocalidade', new Rhlocalidade((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhlocalidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhlocalidade
    # (true)redireciona ou (false) renderiza a visão /view/Rhlocalidade/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhlocalidade');
        try {
            $Rhlocalidade = new Rhlocalidade((int) $_POST['id']);
            $this->set('Rhlocalidade', $Rhlocalidade);
            $Rhlocalidade->save($_POST);
            new Msg(__('Rhlocalidade atualizado com sucesso'));
            $this->go('Rhlocalidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhlocalidade
    # renderiza a /view/Rhlocalidade/delete.php
    function delete(){
        $this->setTitle('Apagar Rhlocalidade');
        try {
            $this->set('Rhlocalidade', new Rhlocalidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhlocalidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhlocalidade
    # redireciona para Rhlocalidade/all
    function post_delete(){
        try {
            $Rhlocalidade = new Rhlocalidade((int) $_POST['id']);
            $Rhlocalidade->delete();
            new Msg(__('Rhlocalidade apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhlocalidade', 'all');
    }

}