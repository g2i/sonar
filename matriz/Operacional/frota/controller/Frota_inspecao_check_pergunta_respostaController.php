<?php
final class Frota_inspecao_check_pergunta_respostaController extends AppController{ 

    # página inicial do módulo Frota_inspecao_check_pergunta_resposta
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Frota_inspecao_check_pergunta_respostas
    # renderiza a visão /view/Frota_inspecao_check_pergunta_resposta/all.php
    function all(){
        $this->setTitle('Listagem ');
        $p = new Paginate('Frota_inspecao_check_pergunta_resposta', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('frota_inspecao_id','=', $this->getParam('id'));
        $this->set('Frota_inspecao_check_pergunta_respostas', $p->getPage($c));
        $this->set('nav', $p->getNav());

         //manda para view uma lista no modelo do data-source="{'M': 'male', 'F': 'female'}" para conseguir listar e alterar os dados 
         $frota_resposta = Frota_inspecao_resposta::getList();
         $frota_respostaDataSource = '{';
         foreach($frota_resposta as $m) {            
             $frota_respostaDataSource .= "'$m->id': '$m->nome', ";
         }
         $frota_respostaDataSource = substr($frota_respostaDataSource, 0, -2);
         $frota_respostaDataSource .= '}';
         $this->set('frota_respostaDataSource',  $frota_respostaDataSource);
    }

    # visualiza um(a) Frota_inspecao_check_pergunta_resposta
    # renderiza a visão /view/Frota_inspecao_check_pergunta_resposta/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Frota_inspecao_check_pergunta_resposta', new Frota_inspecao_check_pergunta_resposta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao_check_pergunta_resposta', 'all');
        }
    }

    # formulário de cadastro de Frota_inspecao_check_pergunta_resposta
    # renderiza a visão /view/Frota_inspecao_check_pergunta_resposta/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Frota_inspecao_check_pergunta_resposta', new Frota_inspecao_check_pergunta_resposta);
        $this->set('Frota_inspecao_perguntas',  Frota_inspecao_pergunta::getList());
        $this->set('Frota_inspecao_respostas',  Frota_inspecao_resposta::getList());
        $this->set('Frota_inspecaos',  Frota_inspecao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_inspecao_check_pergunta_resposta
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao_check_pergunta_resposta/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Frota_inspecao_check_pergunta_resposta = new Frota_inspecao_check_pergunta_resposta();
        $this->set('Frota_inspecao_check_pergunta_resposta', $Frota_inspecao_check_pergunta_resposta);
        try {
            $Frota_inspecao_check_pergunta_resposta->save($_POST);
            new Msg(__('Frota_inspecao_check_pergunta_resposta cadastrado com sucesso'));
            $this->go('Frota_inspecao_check_pergunta_resposta', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Frota_inspecao_perguntas',  Frota_inspecao_pergunta::getList());
        $this->set('Frota_inspecao_respostas',  Frota_inspecao_resposta::getList());
        $this->set('Frota_inspecaos',  Frota_inspecao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_inspecao_check_pergunta_resposta
    # renderiza a visão /view/Frota_inspecao_check_pergunta_resposta/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Frota_inspecao_check_pergunta_resposta', new Frota_inspecao_check_pergunta_resposta((int) $this->getParam('id')));
            $this->set('Frota_inspecao_perguntas',  Frota_inspecao_pergunta::getList());
            $this->set('Frota_inspecao_respostas',  Frota_inspecao_resposta::getList());
            $this->set('Frota_inspecaos',  Frota_inspecao::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_inspecao_check_pergunta_resposta', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_inspecao_check_pergunta_resposta
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao_check_pergunta_resposta/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Frota_inspecao_check_pergunta_resposta = new Frota_inspecao_check_pergunta_resposta((int) $_POST['id']);
            $this->set('Frota_inspecao_check_pergunta_resposta', $Frota_inspecao_check_pergunta_resposta);
            $Frota_inspecao_check_pergunta_resposta->save($_POST);
            new Msg(__('Frota_inspecao_check_pergunta_resposta atualizado com sucesso'));
            $this->go('Frota_inspecao_check_pergunta_resposta', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_inspecao_perguntas',  Frota_inspecao_pergunta::getList());
        $this->set('Frota_inspecao_respostas',  Frota_inspecao_resposta::getList());
        $this->set('Frota_inspecaos',  Frota_inspecao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_inspecao_check_pergunta_resposta
    # renderiza a /view/Frota_inspecao_check_pergunta_resposta/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_inspecao_check_pergunta_resposta');
        try {
            $this->set('Frota_inspecao_check_pergunta_resposta', new Frota_inspecao_check_pergunta_resposta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao_check_pergunta_resposta', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_inspecao_check_pergunta_resposta
    # redireciona para Frota_inspecao_check_pergunta_resposta/all
    function post_delete(){
        try {
            $Frota_inspecao_check_pergunta_resposta = new Frota_inspecao_check_pergunta_resposta((int) $_POST['id']);
            $Frota_inspecao_check_pergunta_resposta->delete();
            new Msg(__('Frota_inspecao_check_pergunta_resposta apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_inspecao_check_pergunta_resposta', 'all');
    }

    function parcial_edit(){
        $this->setTitle('Edição de Inspeção pergunta');

        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];      
        $check = new Frota_inspecao_check_pergunta_resposta((int)$_POST['pk']);
        $this->set('check', $check);
            $name = $_POST['name'];
            
            //verifica se é o campo dt_execucao se for faz o tratamento para salvar no banco de dados 
            if($name == 'dt_execucao') {                
                $_POST['value'] = DataSQL($_POST['value']);
            }
            //salva dados do tipo text
            $check->$name = $_POST['value'];

            if ($check->save($_POST)) {
                $res = ['res' => $check->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        
            //retorna apenas para o console um resultado para saber se deu certo
            echo json_encode($res);
            exit;
    }
}