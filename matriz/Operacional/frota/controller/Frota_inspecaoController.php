<?php
final class Frota_inspecaoController extends AppController{ 

    # página inicial do módulo Frota_inspecao
    function index(){
        $this->setTitle('Visualização Inspeção');
    }

    # lista de Frota_inspecaos
    # renderiza a visão /view/Frota_inspecao/all.php
    function all(){
        $this->setTitle('Listagem Inspeção');
        $p = new Paginate('Frota_inspecao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Frota_inspecaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_motoristas',  Frota_motorista::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Frota_inspecao
    # renderiza a visão /view/Frota_inspecao/view.php
    function view(){
        $this->setTitle('Visualização Inspeção');
        try {
            $this->set('Frota_inspecao', new Frota_inspecao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao', 'all');
        }
    }

    # formulário de cadastro de Frota_inspecao
    # renderiza a visão /view/Frota_inspecao/add.php
    function add(){
        $this->setTitle('Cadastro Inspeção');
        $this->set('Frota_inspecao', new Frota_inspecao);
        $queryVeiculos = new Criteria();
        $queryVeiculos->addCondition('situacao_id','=',1);
        $queryVeiculos->setOrder('placa');
        $queryMotoristas = new Criteria();       
        $queryMotoristas->setOrder('nome ASC');
        $queryMotoristas->addCondition('status','=',1);
        $this->set('Frota_veiculos',  Frota_veiculos::getList($queryVeiculos));
        $this->set('Frota_motoristas',  Frota_motorista::getList($queryMotoristas));
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_inspecao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao/add.php
    function post_add(){
        $this->setTitle('Cadastro Inspeção');
        $Frota_inspecao = new Frota_inspecao();
        $this->set('Frota_inspecao', $Frota_inspecao);
        try {
            $Frota_inspecao->situacao_id = 1;
            $Frota_inspecao->cadastrado_por = Session::get('user')->id;
            $Frota_inspecao->dt_cadastro = date('Y-m-d H:i:d');
            if($Frota_inspecao->save($_POST)){
                $pergunta_id = [];
                foreach(Frota_inspecao_pergunta::getList() as $p){
                    $pergunta_id[] = $p->id;
                }
                foreach($pergunta_id as $ids){
                    $check = new Frota_inspecao_check_pergunta_resposta;
                    $check->frota_inspecao_id = $Frota_inspecao->id;
                    $check->frota_inspecao_pergunta_id = $ids;
                    $check->situacao_id = 1;
                    $check->cadastrado_por = Session::get('user')->id;
                    $check->dt_cadastro = date('Y-m-d H:i:d');
                    $check->save();
                }
            }
            new Msg(__('Inspeção continuar'));
            $this->go('Frota_inspecao', 'edit', array('id' => $Frota_inspecao->id));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_motoristas',  Frota_motorista::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_inspecao
    # renderiza a visão /view/Frota_inspecao/edit.php
    function edit(){
        $this->setTitle('Edição Inspeção');
        try {
            $queryVeiculos = new Criteria();
            $queryVeiculos->addCondition('situacao_id','=',1);
            $queryVeiculos->setOrder('placa');
            $queryMotoristas = new Criteria();       
            $queryMotoristas->setOrder('nome ASC');
            $queryMotoristas->addCondition('status','=',1);
            $this->set('Frota_inspecao', new Frota_inspecao((int) $this->getParam('id')));
            $this->set('Frota_veiculos',  Frota_veiculos::getList($queryVeiculos));
            $this->set('Frota_motoristas',  Frota_motorista::getList($queryMotoristas));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Rhlocalidades',  Rhlocalidade::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_inspecao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_inspecao
    # (true)redireciona ou (false) renderiza a visão /view/Frota_inspecao/edit.php
    function post_edit(){
        $this->setTitle('Edição Inspeção');
        try {
            $Frota_inspecao = new Frota_inspecao((int) $_POST['id']);
            $this->set('Frota_inspecao', $Frota_inspecao);
            $Frota_inspecao->modificado_por = Session::get('user')->id;
            $Frota_inspecao->dt_modificado = date('Y-m-d H:i:d');
            $Frota_inspecao->save($_POST);
            new Msg(__('Inspeção salvo com sucesso'));
            $this->go('Frota_inspecao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_motoristas',  Frota_motorista::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_inspecao
    # renderiza a /view/Frota_inspecao/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_inspecao');
        try {
            $this->set('Frota_inspecao', new Frota_inspecao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_inspecao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_inspecao
    # redireciona para Frota_inspecao/all
    function post_delete(){
        try {
            $Frota_inspecao = new Frota_inspecao((int) $_POST['id']);
            $Frota_inspecao->situacao_id = 3;
            $Frota_inspecao->save();
            new Msg(__('Frota_inspecao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_inspecao', 'all');
    }

}