<?php
final class Frota_acidenteController extends AppController{ 

    # página inicial do módulo Frota_acidente
    function index(){
        $this->setTitle('Visualização de Frota_acidente');
    }

    # lista de Frota_acidentes
    # renderiza a visão /view/Frota_acidente/all.php
    function all(){
        
        $this->setTitle('Listagem de Frota_acidente');
        $p = new Paginate('Frota_acidente', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        if(!empty($_GET['id'])){
            $c->addCondition('veiculo_id', '=', (int) $_GET['id']);
        }        
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_acidentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    
        $this->set('Frota_situacao_acidentes', Frota_situacao_acidente::getList());
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_tipo_acidentes',  Frota_tipo_acidente::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Finalizados',  Finalizado::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    
    }

    # visualiza um(a) Frota_acidente
    # renderiza a visão /view/Frota_acidente/view.php
    function view(){
        $this->setTitle('Visualização de Frota_acidente');
        try {
            $this->set('Frota_acidente', new Frota_acidente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_acidente', 'all');
        }
    }

    # formulário de cadastro de Frota_acidente
    # renderiza a visão /view/Frota_acidente/add.php
    function add(){
        $veiculo_id = (int)$this->getParam('id');
        $this->setTitle('Cadastro de Frota_acidente');
        $this->set('Frota_acidente', new Frota_acidente);

        if($_GET['id'] != 0){
            $c = new Criteria();
            $c->addCondition('id', '=', $_GET['id']);            
            $this->set('Frota_veiculos',  Frota_veiculos::getList($c));
        }else{
            $this->set('Frota_veiculos',  Frota_veiculos::getList());
        }

        $this->set('Frota_situacao_acidentes', Frota_situacao_acidente::getList());
        $this->set('Frota_tipo_acidentes',  Frota_tipo_acidente::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Finalizados',  Finalizado::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('veiculo_id',  $veiculo_id);

    }

    # recebe os dados enviados via post do cadastro de Frota_acidente
    # (true)redireciona ou (false) renderiza a visão /view/Frota_acidente/add.php
    function post_add(){
        
        $this->setTitle('Cadastro de Frota_acidente');
        $Frota_acidente = new Frota_acidente();
        $this->set('Frota_acidente', $Frota_acidente);
        if(empty($_POST['dt_termino'])){
            $dt_termino = date('Y-m-d');
            $_POST['dt_termino'] = date('Y-m-d', strtotime($dt_termino . '+1 month'));
        }
        $_POST['dt_cadastro'] = date('Y-m-d H:i:s');
        $_POST['situacao_id'] = 1;
        $_POST['cadastradopor'] = Session::get('user')->id;
        $veiculo_id = (int)$_POST['veiculo_id'];
        try {
            $Frota_acidente->save($_POST);
            new Msg(__('Frota_acidente cadastrado com sucesso'));
            $this->go('Frota_acidente', 'all', ['id' => $veiculo_id]);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Frota_situacao_acidentes', Frota_situacao_acidente::getList());
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_tipo_acidentes',  Frota_tipo_acidente::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Finalizados',  Finalizado::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Frota_acidente
    # renderiza a visão /view/Frota_acidente/edit.php
    function edit(){
        $this->setTitle('Edição de Frota_acidente');
        try {
            $this->set('Frota_acidente', new Frota_acidente((int) $this->getParam('id')));
            $this->set('Frota_veiculos',  Frota_veiculos::getList());
            $this->set('Frota_tipo_acidentes',  Frota_tipo_acidente::getList());
            $this->set('Rhprofissionais',  Rhprofissional::getList());
            $this->set('Finalizados',  Finalizado::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Frota_situacao_acidentes', Frota_situacao_acidente::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Frota_acidente', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_acidente
    # (true)redireciona ou (false) renderiza a visão /view/Frota_acidente/edit.php
    function post_edit(){
        $this->setTitle('Edição de Frota_acidente');
        try {
            $Frota_acidente = new Frota_acidente((int) $_POST['id']);
            $this->set('Frota_acidente', $Frota_acidente);
            $_POST['dt_atualizacao'] = date('Y-m-d H:i:s');
            $_POST['modificadopor'] = Session::get('user')->id;
            $Frota_acidente->save($_POST);
            new Msg(__('Frota_acidente atualizado com sucesso'));
            $this->go('Frota_acidente', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_tipo_acidentes',  Frota_tipo_acidente::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Finalizados',  Finalizado::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Frota_situacao_acidentes', Frota_situacao_acidente::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_acidente
    # renderiza a /view/Frota_acidente/delete.php
    function delete(){
        $this->setTitle('Apagar Frota_acidente');
        try {
            $this->set('Frota_acidente', new Frota_acidente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_acidente', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_acidente
    # redireciona para Frota_acidente/all
    function post_delete(){
        try {
            $Frota_acidente = new Frota_acidente((int) $_POST['id']);
            $Frota_acidente->situacao_id = 3;
            $Frota_acidente->save();
            new Msg(__('Frota_acidente apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Frota_acidente', 'all');
    }

    function acidentes(){

        $this->setTitle('Listagem de Frota_acidente');
        $p = new Paginate('Frota_acidente', 10);
        $c = new Criteria();
        $c->addCondition('situacao_id', '=', 1);
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_acidentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    
        $this->set('Frota_situacao_acidentes', Frota_situacao_acidente::getList());
        $this->set('Frota_veiculos',  Frota_veiculos::getList());
        $this->set('Frota_tipo_acidentes',  Frota_tipo_acidente::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
        $this->set('Finalizados',  Finalizado::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
    }
    function alert_acidente(){
        $p = new Paginate('Frota_acidente', 10);
        $c = new Criteria();
        $c->addCondition('situacao_acidente_id','=',1);
        $c->addCondition('situacao_id','=',1);
        $this->set('Frota_acidentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }
}

