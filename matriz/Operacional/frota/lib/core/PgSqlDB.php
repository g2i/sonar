<?php
/**
 * Created by PhpStorm.
 * User: RFTD
 * Date: 29/05/2015
 * Time: 13:37
 */
final class PgSqlDB extends Db {

    private static $conn = NULL;

    function __construct() {
        $this->conectar();
    }

    protected function conectar() {
        if (!self::$conn) {
            if(DEBUG_MODE){
                new DebugMsg('Abriu uma nova conexão com PostgreSql');
            }
            $this->host = Config::get('db_host');
            $this->user = Config::get('db_user');
            $this->pass = Config::get('db_password');
            $this->dbname = Config::get('db_name');
            $dsn = 'pgsql:host=' . $this->host . ';dbname=' . $this->dbname;
            // Set options
            $options = array(
                PDO::ATTR_PERSISTENT => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );
            try {
                $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
                self::$conn = $this->dbh;
            } catch (PDOException $e) {
                $this->error = $e->getMessage();
                $log = fopen('logs/log_db_error.txt', 'a+');
                fwrite($log, date("d/M/Y H:i") . ' - ' . $this->error . "\r\n");
                fwrite($log, __FILE__ . "\r\n");
                fwrite($log, "================================================\r\n\n");
                fclose($log);
                if (DEBUG_MODE) {
                    echo '<h1>' . __('Verifique as configurações do arquivo config.php') . '</h1>';
                    //echo '<strong>' . $this->error . '</strong><br><br>';
                }
                exit;
            }
        }
        $this->dbh = self::$conn;
    }

    public function getTableDescription($tablename){
        $this->query("select c.column_name AS Field, c.data_type AS Type,
                     (CASE (select ix.indisprimary from pg_class t join pg_attribute a on a.attrelid = t.oid
                            join pg_index ix on t.oid = ix.indrelid AND a.attnum = ANY(ix.indkey)
                            join pg_class i on i.oid = ix.indexrelid
                             left join pg_attrdef d on d.adrelid = t.oid and d.adnum = a.attnum
                             where t.relkind = 'r'  and t.relname = c.table_name AND a.attname = c.column_name
                      )WHEN true then 'PRI' ELSE ''END) AS Key,
                      (CASE substr(c.column_default, 1, 7)
                       WHEN 'nextval' then 'auto_increment' ELSE '' END) AS Extra
                       from INFORMATION_SCHEMA.COLUMNS AS c
                       where c.table_name = :table");

        $this->bind(":table", $tablename, PDO::PARAM_STR);
        return $this->getResults();
    }

    public function getTableFields($tablename){
        $this->query("DESCRIBE $tablename");
        $r = $this->getResults();
        $desc = array();
        foreach ($r as $rvalue) {
            $desc[] = $rvalue->Field;
        }
        return $desc;;
    }

    public function getTables(){
        $this->query("SELECT table_name AS name FROM information_schema.tables WHERE table_schema = 'public'");
        return $this->getResults();
    }

}