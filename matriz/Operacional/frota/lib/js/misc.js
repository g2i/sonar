prevent_ajax_view = true;

$(document).ready(function () {
    $('button[data-dismiss="modal"]').click(function () {
        //history.go(-1);
    });

    $(document).on('hidden.bs.modal', function (e) {
        $(e.target).removeData('bs.modal');
    });

    $('body').on('click', 'a[data-toggle=modal]', function (event) {
        event.preventDefault();
        modalharef = $(this).attr('href');
        if (modalharef.indexOf("?") == -1)
            modalharef += '?'
        modalharef += '&ajax=true';
        $(this).attr('href', modalharef);
        if (modalharef.indexOf("first") != -1) {
            Acrescentar(modalharef);
        }

        if (!$(this).attr('data-target')) {
            $(this).attr('data-target', '#modal');
        }
    });

    $("[data-toggle='tooltip']").tooltip();

    $.extend({
        Report: function (report, args) {
            var url = root + '/relatorios/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=' + report;
            $.each(args, function (key, value) {
                url += '&' + key + '=' + value;
            });

            window.open(url, '_blank');
        }
    });

    // $("#quilometragem").blur(function () {
    //     $.ajax({
    //         url:root+"/Frota_abastecimentos/getQuilometragem",
    //         type:'POST',
    //         data:{
    //             veiculo:$("#veiculo_id").val()
    //         },
    //         success:function(txt){
    //             if(parseInt(txt)>$("#quilometragem").val()){
    //                 $("#quilometragem").val("");
    //                 BootstrapDialog.warning("Quilometragem inferior a quilometragem atual do veículo! Confira a quilometragem atual.")
    //             }
    //         }
    //     });
    // });

    $("#veiculo_id").change(function () {
        $.ajax({
            url: root + "/Frota_abastecimentos/getQuilometragem",
            type: 'POST',
            data: {
                veiculo: $("#veiculo_id").val()
            },
            success: function (txt) {
                $("#km_ultimo_abastecimento").val(txt);
            }
        });
    });

    /**
     *  Controller: Frota_abastecimento 
     *  Para todos os scripts abaixo
     * */ 
    $('.quilometragem').keyup(function(){
        kmRodado();
        qntLitro();
        valorTotalPago();
    });
    
    $('#quantidade').keyup(function(){
        qntLitro();
    });

    $('#valor_total_pago').keyup(function(){
        valorTotalPago();
    });

    // $("#quilometragem_manutencao").blur(function () {
    //     $.ajax({
    //         url:root+"/Frota_manutencoes/getQuilometragem",
    //         type:'POST',
    //         data:{
    //             veiculo:$("#veiculo_id").val()
    //         },
    //         success:function(txt){
    //             if(parseInt(txt)>$("#quilometragem_manutencao").val()){
    //                 $("#quilometragem_manutencao").val("");
    //                 BootstrapDialog.warning("Quilometragem inferior a quilometragem atual do veículo! Confira a quilometragem atual.")
    //             }
    //         }
    //     });
    // });
});

function kmRodado(){
    var kmAnterior = $('#quilometragem_anterior').val();
    var kmAtual = $('#quilometragem').val();

    $('#km_rodado').val(kmAtual - kmAnterior);
}

function qntLitro(){
    var kmRodado = $('#km_rodado').val();
    var qntLitro = $('#quantidade').val();

    $('#litro_km').val((kmRodado / qntLitro));
}

function valorTotalPago(){
    var valorTotal = $('#valor_total_pago').val();
    var kmRodado = $('#km_rodado').val();

    $('#valor_por_km').val((valorTotal / kmRodado).toFixed(3));
}


function LoadGif() {
    var div = document.createElement("div");
    div.setAttribute('id', 'dialog-cep');
    var img = document.createElement("img");
    img.setAttribute("src", root + "/lib/css/images/WindowsPhoneProgressbar.gif");
    div.appendChild(img);
    document.body.appendChild(div);
    $("#dialog-cep").css({
        "position": "fixed",
        "top": "0",
        "right": "0",
        "bottom": "0",
        "left": "0",
        "z-index": "1050",
        "background-color": "#000000",
        "opacity": "0.8",
        "filter": "alpha(opacity=80)"
    });

    $("#dialog-cep img").css({
        "position": "fixed",
        "top": "40%",
        "left": "30%"
    });
}
function CloseGif() {
    $("#dialog-cep").remove();
    $(".numero").focus();
}


$(".clientes").select2({
    language: 'pt-BR',
    theme: 'bootstrap',
    placeholder: "Selecione",
    ajax: {
        url: root + "/Clientes/select2_paginado",
        dataType: 'json',
        delay: 10,
        data: function (params) {
            return {
                termo: params.term,
                size: 10,
                page: params.page
            };
        },
        processResults: function (data) {
            // parse the results into the format expected by Select2.
            // since we are using custom formatting functions we do not need to
            // alter the remote JSON data
            return {
                results: data.dados,
                pagination: {
                    more: data.more
                }
            }
        },
        cache: true
    },
    escapeMarkup: function (markup) {
        return markup;
    }, // let our custom formatter work
    minimumInputLength: 1

});

$.fn.extend({
    openSelect: function () {
        return this.each(function (idx, domEl) {
            if (document.createEvent) {
                var event = new MouseEvent("mousedown");
                domEl.dispatchEvent(event);
            } else if (element.fireEvent) {
                domEl.fireEvent("onmousedown");
            }
        });
    },
    loadGrid: function (url, args, selector, callback) {
        this.each(function () {
            $(this).html('');
            $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
        });

        if (selector == '' || selector == null)
            selector = '.table-responsive';
        if (!empty(args)) {
            $.each(args, function (key, value) {
                if (!empty(value) && value.indexOf("/") < 0) {
                    url += '&' + key + "=" + encodeURIComponent(value);
                } else {
                    url += '&' + key + "=" + value;
                }
            });
        }

        url += " " + selector;

        return this.each(function () {
            if ($.isFunction(callback)) {

                $(this).load(url, callback);
            } else {
                $(this).load(url);

            }
        });
    },
    loadNewGrid: function (url, args, selector, callback) {
        this.each(function () {
            $(this).html('');
            $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
        });

        if (selector == '' || selector == null)
            selector = '.table-responsive';
        var n = url.indexOf("?");
        if (n > -1) {
            url += '&' + args;
        } else {
            url += '/?' + args;
        }
        url += ' ' + selector;
        return this.each(function () {
            if ($.isFunction(callback)) {
                $(this).load(url, callback);
            } else {
                $(this).load(url);
            }
        });
    },

    loadPostGrid: function (url, form, selector, callback) {
        var ths = this
        this.each(function () {
            $(this).html('');
            $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
        });

        if (selector == '' || selector == null)
            selector = '.table-responsive';
        var n = url.indexOf("?");
        if (n > -1) {
            url += '&';
        } else {
            url += '/?';
        }
        url += '' + selector;

        this.each(function () {
            if ($.isFunction(callback)) {
                $.post(url, form, function (dados) {
                    var htm = $(dados).find(selector).html();
                    ths.html(htm)
                    carregar()
                });
            } else {
                return $.post(url, form, function (dados) {
                    var htm = $(dados).find(selector).html();
                    ths.html(htm)
                    carregar()
                });
            }
        });

    }

});

$.fn.extend({
    serializeJSON: function (exclude) {
        exclude || (exclude = []);
        return _.reduce(this.serializeArray(), function (hash, pair) {
            pair.value && !(pair.name in exclude) && (hash[pair.name] = pair.value);
            return hash;
        }, {});
    }
});

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

//Define o tipo_equipamento_id de equipamento a ser cadastrado
$('#tipo_equipamento_id').change(function () {
    if ($('#tipo_equipamento_id').val() == '2') {
        $('#munck').hide();
        $('#capacidade').val('');
        $('#validade_laudo_mecânico').val('');
    } else if ($('#tipo_equipamento_id').val() == '1') {
        $('#munck').show();
    } else {
        $('#munck').hide();
        $('#capacidade').val('');
        $('#validade_laudo_mecânico').val('');
    }
});

$('#quilometragem').blur(function () {

    if (($('#quilometragem_anterior').val() * 1.0) > ($('#quilometragem').val() * 1.0) ) {
        $('#quilometragem_anterior').val('');
        $('#quilometragem').val('');
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_WARNING,
            title: 'Atenção!',
            message: 'A quilometragem atual é inferior a quilometragem anterior!',
        });
    }

});

$('#dt_abastecimento').blur(function () {

    dataForm = $('#dt_abastecimento').val();

    var data = moment().format('YYYY-MM-DD');
    if (dataForm > data) {

        BootstrapDialog.warning('Data superior a atual!');
        var data = $('#dt_abastecimento').val("");

        console.log(data);
        console.log(dataForm);

    }

});

$('#dt_manutencao').blur(function () {

    dataForm = $('#dt_manutencao').val();

    var data = moment().format('YYYY-MM-DD');
    if (dataForm > data) {

        BootstrapDialog.warning('Data superior a atual!');
        var data = $('#dt_manutencao').val("");

        console.log(data);
        console.log(dataForm);

    }

});

