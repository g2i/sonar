<?php include_once "dataUtil.php"; ?>
<p><strong>Cadastrado em</strong>: <?php echo convertDataSQL4BR($Frota_veiculos->dt_cadastro);?></p>
<p><strong>Placa</strong>: <?php echo $Frota_veiculos->placa;?></p>
<p><strong>Chassi</strong>: <?php echo $Frota_veiculos->chassi;?></p>
<p><strong>Renavam</strong>: <?php echo $Frota_veiculos->renavam;?></p>
<p><strong>Modelo</strong>: <?php echo $Frota_veiculos->modelo;?></p>
<p><strong>Fabricante</strong>: <?php echo $Frota_veiculos->fabricante;?></p>
<p><strong>Combustivel</strong>: <?php echo $Frota_veiculos->combustivel;?></p>
<p><strong>Ano / Modelo</strong>: <?php echo $Frota_veiculos->ano_modelo;?></p>
<p><strong>Categoria</strong>: <?php echo $Frota_veiculos->categoria;?></p>
<p><strong>Valor FIPE</strong>: <?php echo $Frota_veiculos->valor_fipe;?></p>
<p><strong>Quilometragem</strong>: <?php echo $Frota_veiculos->quilometragem;?></p>
<p><strong>Cor</strong>: <?php echo $Frota_veiculos->cor;?></p>
<p><strong>Localização</strong>: <?php echo $Frota_veiculos->getFrota_localizacao()->cidade;?></p>
<p>
    <strong>Origem</strong>:
    <?php
    echo $this->Html->getLink($Frota_veiculos->getFrota_origem()->descricao, 'Frota_origem', 'view',
    array('id' => $Frota_veiculos->getFrota_origem()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Cadastrado por</strong>:
    <?php
    echo $this->Html->getLink($Frota_veiculos->getUsuario()->login, 'Usuario', 'view',
    array('id' => $Frota_veiculos->getUsuario()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Situação</strong>:
    <?php
    echo $this->Html->getLink($Frota_veiculos->getSituacao()->nome, 'Situacao', 'view',
    array('id' => $Frota_veiculos->getSituacao()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>