<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Veículos</h2>
        <ol class="breadcrumb">
            <li>Veículos</li>
            <li class="active">
                <strong>Editar Veículos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="post" role="form"
              action="<?php echo $this->Html->getUrl('Frota_veiculos', 'edit') ?>">
            <input type="hidden" name="id" id="id" value="<?php echo $Frota_veiculos->id; ?>">

            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title" style="border-color: #007bff">
                        <h2>Informações do Veiculo</h2>
                    </div>

                    <div class="ibox-content">

                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="placa" class="required">Placa</label> <span
                                    class="glyphicon glyphicon-asterisk"></span>
                            <input type="text" name="placa" id="placa" class="form-control"
                                   value="<?php echo $Frota_veiculos->placa ?>" required placeholder="Placa">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="motorista_id">Motorista</label>
                            <select name="motorista_id" class="form-control motorista selectPicker"
                                    id="motorista_id">

                                <?php
                                foreach ($Frota_motoristas as $f) {
                                    if ($f->id == $Frota_veiculos->motorista_id)
                                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                    else
                                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="chassi">Chassi</label>
                            <input type="text" name="chassi" id="chassi" class="form-control"
                                   value="<?php echo $Frota_veiculos->chassi ?>" placeholder="Chassi">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="renavam">Renavam</label>
                            <input type="number" name="renavam" id="renavam" class="form-control"
                                   value="<?php echo $Frota_veiculos->renavam ?>" placeholder="Renavam">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="modelo" class="required">Modelo</label> <span
                                    class="glyphicon glyphicon-asterisk"></span>
                            <input type="text" name="modelo" id="modelo" class="form-control"
                                   value="<?php echo $Frota_veiculos->modelo ?>" required placeholder="Modelo">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="fabricante">Fabricante</label>
                            <input type="text" name="fabricante" id="fabricante" class="form-control"
                                   value="<?php echo $Frota_veiculos->fabricante ?>" placeholder="Fabricante">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="combustivel" class="required">Combustivel</label>
                            <select name="combustivel" id="combustivel" class="form-control"
                                    value="<?php echo $Frota_veiculos->combustivel ?>">
                                <option value="">Selecione</option>


                                <option value="Etanol" <?= $Frota_veiculos->combustivel == "Etanol" ? "selected" : '' ?>>
                                    Etanol
                                </option>
                                <option value="Gasolina" <?= $Frota_veiculos->combustivel == "Gasolina" ? "selected" : '' ?>>
                                    Gasolina
                                </option>
                                <option value="GNV" <?= $Frota_veiculos->combustivel == "GNV" ? "selected" : '' ?>>
                                    GNV
                                </option>
                                <option value="Flex" <?= $Frota_veiculos->combustivel == "Flex" ? "selected" : '' ?>>
                                    Flex
                                </option>
                                <option value="Diesel" <?= $Frota_veiculos->combustivel == "Diesel" ? "selected" : '' ?>>
                                    Diesel
                                </option>
                                <option value="Diesel S10" <?= $Frota_veiculos->combustivel == "Diesel S10" ? "selected" : '' ?>>
                                    Diesel S10
                                </option>
                            </select>
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="ano_modelo" class="required">Ano / Modelo</label><span
                                    class="glyphicon glyphicon-asterisk"></span>
                            <input type="text" name="ano_modelo" id="ano_modelo" class="form-control"
                                   value="<?php echo $Frota_veiculos->ano_modelo ?>" required
                                   placeholder="Ano / Modelo">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="categoria">Categoria</label>
                            <select name="categoria" id="categoria" class="form-control"
                                    value="<?php echo $Frota_veiculos->categoria ?>">
                                <option value="">Selecione</option>

                                <option value="Carro" <?= $Frota_veiculos->categoria == "Carro" ? "selected" : '' ?>>
                                    Carro
                                </option>
                                <option value="Moto" <?= $Frota_veiculos->categoria == "Moto" ? "selected" : '' ?>>
                                    Moto
                                </option>
                                <option value="Caminhão" <?= $Frota_veiculos->categoria == "Caminhão" ? "selected" : '' ?>>
                                    Caminhão
                                </option>
                                <option value="Caminhonete" <?= $Frota_veiculos->categoria == "Caminhonete" ? "selected" : '' ?>>
                                    Caminhonete
                                </option>

                            </select>
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="valor_fipe">Valor FIPE</label>
                            <input type="number" step="any" name="valor_fipe" id="valor_fipe" class="form-control"
                                   value="<?php echo $Frota_veiculos->valor_fipe ?>" placeholder="Valor FIPE">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="quilometragem" class="required">Quilometragem</label><span
                                    class="glyphicon glyphicon-asterisk"></span>
                            <input type="number" step="any" name="quilometragem" id="quilometragem"
                                   class="form-control" value="<?php echo $Frota_veiculos->quilometragem ?>"
                                   required placeholder="Quilometragem">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="cor">Cor</label>
                            <input type="text" name="cor" id="cor" class="form-control"
                                   value="<?php echo $Frota_veiculos->cor ?>" placeholder="Cor">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">

                            <label for="origem_id">Origem</label>
                            <select name="origem_id" class="form-control selectPicker" id="origem_id">
                                <?php
                                foreach ($Frota_origens as $f) {
                                    if ($f->id == $Frota_veiculos->origem_id)
                                        echo '<option selected value="' . $f->id . '">' . $f->descricao . '</option>';
                                    else
                                        echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="localizacao_id">Localização</label>
                            <select name="localizacao_id" class="form-control selectPicker" id="localizacao_id">
                                <option selected>Selecione</option>
                                <?php
                                foreach ($Frota_localizacao as $f) {
                                    if ($f->id == $Frota_veiculos->localizacao_id)
                                        echo '<option selected value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>';
                                    else
                                        echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="numero_serie_bateria">Nº série bateria</label>
                            <input type="text" name="numero_serie_bateria" id="numero_serie_bateria"
                                   class="form-control"
                                   value="<?php echo $Frota_veiculos->numero_serie_bateria ?>"
                                   placeholder="Nº serie bateria">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="fabricante_bateria">Fabricante Bateria</label>
                            <input type="text" name="fabricante_bateria" id="fabricante_bateria"
                                   class="form-control"
                                   value="<?php echo $Frota_veiculos->fabricante_bateria ?>"
                                   placeholder="Fabricante Bateria">
                        </div>

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="situacao_id">Situação</label>
                            <select name="situacao_id" class="form-control situacao_id selectPicker"
                                    id="situacao_id">
                                <option value="">Selecione</option>
                                <?php
                                foreach ($Situacaos as $f) {
                                    if ($f->id == $Frota_veiculos->situacao_id)
                                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                    else
                                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-title" style="border-color: #ffc107;">
                        <h2>Documentação</h2>
                    </div>
                    <div class="ibox-content">

                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="nome_proprietario">Proprietário/Seguro</label>
                            <input type="text" name="nome_proprietario" id="nome_proprietario"
                                   class="form-control"
                                   value="<?php echo $Frota_veiculos->nome_proprietario ?>"
                                   placeholder="Nome Proprietário/Seguro">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="contato_seguradora">Telefone Seguradora</label>
                            <input type="text" name="contato_seguradora" id="contato_seguradora"
                                   class="form-control"
                                   value="<?php echo $Frota_veiculos->contato_seguradora ?>"
                                   placeholder="Telefone Seguradora">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="licenciamento">Licenciamento</label>
                            <input type="Month" name="licenciamento" id="licenciamento"
                                   class="form-control"
                                   value="<?php echo $Frota_veiculos->licenciamento ?>"
                                   placeholder="Licenciamento">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="vencimento_proximo_ipva">Vencimento IPVA</label>
                            <input type="Month" name="vencimento_proximo_ipva" id="vencimento_proximo_ipva"
                                   class="form-control"
                                   value="<?php echo $Frota_veiculos->vencimento_proximo_ipva ?>"
                                   placeholder="Vencimento IPVA">
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="seguro_obrigatorio">Seguro Obrigatório</label>
                            <input type="text" name="seguro_obrigatorio" id="seguro_obrigatorio"
                                   class="form-control"
                                   value="<?php echo $Frota_veiculos->seguro_obrigatorio ?>"
                                   placeholder="Seguro">
                        </div>

                        <div class="clearfix"></div>

                    </div>
                </div>


                <div class="ibox">
                    <div class="ibox-title" style="border-color: #00a65a;">
                        <h2>Projeto</h2>
                    </div>
                    <div class="ibox-content">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="required" for="contabilidade">Grupo <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="fin_projeto_grupo_id" name="fin_projeto_grupo_id"
                                        class="form-control selectPicker"
                                        required>
                                    <option>Selecione Um Grupo</option>
                                    <?php
                                    foreach ($Fin_projeto_grupo as $c) {
                                        if ($c->id == $Fin_projeto->grupo_id)
                                            echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                        else
                                            echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="required" for="contabilidade">Empresa <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="fin_projeto_empresa_id" name="fin_projeto_empresa_id"
                                        class="form-control selectPicker"
                                        required>
                                    <?php
                                    foreach ($Fin_projeto_empresa as $c) {
                                        if ($c->id == $Fin_projeto->empresa_id)
                                            echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                        else
                                            echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="required" for="contabilidade">Unidade<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="fin_projeto_unidade_id" name="fin_projeto_unidade_id"
                                        class="form-control selectPicker"
                                        required>
                                    <?php
                                    foreach ($Fin_projeto_unidade as $c) {
                                        if ($c->id == $Fin_projeto->unidade_id)
                                            echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                        else
                                            echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_veiculos', 'all') ?>"
                               class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>

                    </div>
                </div>

            </div>
        </form>
    </div>

</div>


<script>
    $('#fin_projeto_grupo_id').on('change', function () {
        var url = '<?= $this->Html->getUrl('Fin_projeto_empresa', 'getListaEmpresa') ?>';
        var html = '<option><option>';
        var id = $(this).val();
        $('#fin_projeto_empresa_id').prop("disabled", true);
        $('#fin_projeto_unidade_id').prop("disabled", true);
        $.get(url, {fin_projeto_grupo_id: id}, function (data) {
            $.each(data, function (key, value) {
                html += '<option value="' + value.id + '">' + value.nome + '<option>'
            });
            $('#fin_projeto_empresa_id').html(html).trigger('change.select2');
            $('#fin_projeto_unidade_id').html('').trigger('change.select2');
            $('#fin_projeto_empresa_id').prop("disabled", false);
            $('#fin_projeto_unidade_id').prop("disabled", false);
        }, 'json');
    });

    $('#fin_projeto_empresa_id').on('change', function () {
        var url = '<?= $this->Html->getUrl('Fin_projeto_unidade', 'getListaUnidade') ?>';
        var html = '<option><option>';
        var id = $(this).val();
        $('#fin_projeto_unidade_id').prop("disabled", true);
        $.get(url, {fin_projeto_empresa_id: id}, function (data) {
            $.each(data, function (key, value) {
                html += '<option value="' + value.id + '">' + value.nome + '<option>'
            });
            $('#fin_projeto_unidade_id').html(html).trigger('change.select2');
            $('#fin_projeto_unidade_id').prop("disabled", false);
        }, 'json');
    });

    $(".motorista").change(function () {
        $.ajax({
            url: root + "/Frota_veiculos/getMotoristaEdit",
            type: 'POST',
            data: {
                motorista_id: $(".motorista").val(),
                veiculo_id: $("#id").val()
            },
            success: function (txt) {
                if (txt == 1) {
                    var teste = BootstrapDialog.danger("Esse Motorista está designado para outro veículo, caso for selecionado será remanejado do outro veículo para este.")
                    teste.setTitle("Atenção!")

                }
            }
        });
    });
</script>