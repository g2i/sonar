<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Itens Segurança</h2>
        <ol class="breadcrumb">
            <li>Itens Segurança</li>
            <li class="active">
                <strong>Lista</strong>
            </li></ol></div></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                  <!-- botao de cadastro -->
                  <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Item', 'Frota_manutencoes', 'add', array('id' => (int) $_GET['id']), array('class' => 'btn btn-primary')); ?></p>
                    </div>
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'veiculo_id')); ?>'>
                                            Veículo
                                        </a>
                                    </th>
                                      <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'tipo_manutencao_id')); ?>'>
                                            Itens
                                        </a>
                                    </th>
                                 <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'tipo_manutencao_id')); ?>'>
                                            Situação 
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'fornecedor_id')); ?>'>
                                            Fornecedor
                                        </a>
                                    </th>
                                  
                                    
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'valor')); ?>'>
                                            Valor 
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'garantia')); ?>'>
                                            Garantia
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'dt_manutencao')); ?>'>
                                            Data Manutenção
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'observacao')); ?>'>
                                            Observação
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'quilometragem')); ?>'>
                                            Quilometragem
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                            Cadastrado por
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_manutencoes as $f) {

//                                    if(($f->getFrota_veiculos()->quilometragem_atual - $f->quilometragem) >= $f->vida_util || ($f->getFrota_veiculos()->quilometragem_atual - $f->quilometragem) >= ($f->vida_util - 200)) {
//                                        echo '<tr bgcolor="#FFCDD2">';
//                                    } else {
//                                        echo '<tr>';
//                                    }
                                if(!empty(Frota_manutencoes::getVeiculosManutencoes($f->getFrota_veiculos()->id))) {
                                        echo '<tr bgcolor="#FFCDD2">';
                                    } else {
                                        echo '<tr>';
                                    }

                                  
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
                                        array('id' => $f->getFrota_veiculos()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_tipo_manutencao()->descricao, 'Frota_tipo_manutencao', 'view',
                                        array('id' => $f->getFrota_tipo_manutencao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getSituacaoManutencao()->nome, 'Frota_manutencoes', 'view',
                                        array('id' => $f->getFrota_tipo_manutencao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFornecedor()->nome, 'Fornecedor', 'view',
                                        array('id' => $f->getFornecedor()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink('R$ ' . sprintf('%0.2f', $f->valor), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';                                    
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($f->garantia), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($f->dt_manutencao), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->observacao, 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->quilometragem, 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getUsuario()->login, 'Usuario', 'view',
                                        array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_manutencoes', 'edit',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_manutencoes', 'delete',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function() {
                            $('#search').keyup(function() {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Frota_manutencoes', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function() {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>