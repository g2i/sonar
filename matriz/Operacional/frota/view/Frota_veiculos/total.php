<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Veículos</h2>
        <ol class="breadcrumb">
            <li>Veículos</li>
            <li class="active">
                <strong>Gastos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl('Frota_veiculos', 'total') ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <div class="col-md-6 form-group">
                                    <label for="ano">Ano</label>
                                    <select name="ano" id="ano"
                                            class="form-control selectPicker">
                                        <?php
                                        for ($i = date('Y'); $i >= date("Y", strtotime("-5 year")); $i--) {
                                            echo "<option value='" . $i . "'>" . $i . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" id="btnReset" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- tabela de resultados -->

                    <div class="clearfix">
                        <div class="table-responsive">
                            <h2>Gastos - Abastecimentos</h2>
                            <table class="table table-hover">
                                <tr>
                                    <th>&nbsp;</th>

                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                        <th class="text-right">  <?= $meses[$i] ?>  </th>
                                    <?php } ?>
                                    <th class="text-right">Média</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                <tr class="active">
                                    <td>&nbsp;</td>

                                    <?php if (!empty($abastecimentos)) {

                                        $totalGeralAbs = 0;

                                        foreach ($abastecimentos as $ab) { ?>
                                            <td class="text-right"> <?= number_format($ab->total, 2, ',', '.'); ?>  </td>

                                        <?php $totalGeralAbs += $ab->total; }
                                    } else {
                                        for ($i = 1; $i <= 12; $i++) { ?>
                                            <td class="text-right">0.00</td>
                                        <?php }
                                    } ?>

                                    <td class="text-right">  <?= number_format(($totalGeralAbs / 12), 2, ',', '.'); ?>  </td>
                                    <td class="text-right">  <?= number_format($totalGeralAbs, 2, ',', '.'); ?>  </td>
                                </tr>

                            </table>
                            <hr>
                            <h2>Gastos - Manutenções</h2>
                            <table class="table table-hover">
                                <tr>

                                    <th>&nbsp;</th>

                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                        <th class="text-right">  <?= $meses[$i] ?>  </th>
                                    <?php } ?>
                                    <th class="text-right">Média</th>
                                    <th class="text-right">Total</th>
                                </tr>
                                <tr class="active">
                                    <td>&nbsp;</td>

                                    <?php if (!empty($manutencoes)) {

                                        $totalGeralMan = 0;

                                        foreach ($manutencoes as $ab) { ?>
                                            <td class="text-right"> <?= number_format($ab->total, 2, ',', '.'); ?>  </td>

                                            <?php $totalGeralMan += $ab->total; }
                                    } else {
                                        for ($i = 1; $i <= 12; $i++) { ?>
                                            <td class="text-right">0.00</td>
                                        <?php }
                                    } ?>

                                    <td class="text-right">  <?= number_format(($totalGeralMan / 12), 2, ',', '.'); ?>  </td>
                                    <td class="text-right">  <?= number_format($totalGeralMan, 2, ',', '.'); ?>  </td>
                                </tr>

                            </table>

                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Frota_veiculos', 'total', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Frota_veiculos', 'total') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>
