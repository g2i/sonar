<!--<div class="row wrapper border-bottom white-bg page-heading">-->
<!--    <div class="col-lg-9">-->
<!--        <h2>Veículos</h2>-->
<!--        <ol class="breadcrumb">-->
<!--            <li>Transferência</li>-->
<!--            <li class="active">-->
<!--                <strong>Transferir Veículo</strong>-->
<!--            </li>-->
<!--        </ol>-->
<!--    </div>-->
<!--</div>-->
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Veículos</h2>
                <ol class="breadcrumb">
                    <li>Transferência</li>
                    <li class="active">
                        <strong>Transferir Veículo</strong>
                    </li>
                    <br><br>
                </ol>
            </div>
            <div class="ibox float-e-margins">

                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_veiculos', 'transferir') ?>">
                        <input type="hidden" name="id" value="<?php echo $Frota_veiculos->id; ?>">


                        <div class="well">
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="localizacao_id">Transferir para:</label>
                                <select name="localizacao_id" class="form-control" id="localizacao_id">
                                    <option selected>Selecione</option>
                                    <?php
                                    foreach ($Frota_localizacao as $f) {
                                        if ($f->id == $Frota_veiculos->localizacao_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="descricao">Observação:</label>
                                <input type="text" name="descricao" id="descricao" class="form-control">
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>


                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_veiculos', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="Transferir">
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
</div>