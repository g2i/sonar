<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Licencimantos</h2>
                <ol class="breadcrumb">
                    <li>Licencimantos</li>
                    <li class="active">
                        <strong>À vencer com 30 dias</strong>
                    </li>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    Placa
                                </th>
                                <th>
                                    Modelo
                                </th>
                                <th>
                                    Localização
                                </th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php
                                    foreach ($Frota_veiculos as $f) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $f->placa;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $f->modelo;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $f->getFrota_localizacao()->cidade;
                                        echo '</td>';                                        
                                        echo '</tr>';
                                    }
                                ?>
                        </table>
                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php echo $nav; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>