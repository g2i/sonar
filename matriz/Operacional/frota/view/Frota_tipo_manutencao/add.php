<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Manutenção - Itens</h2>
        <ol class="breadcrumb">
            <li>Manutenção - Itens</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" id="forme-item" action="<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="descricao" class="required">Descrição</label><span class="glyphicon glyphicon-asterisk"></span>
                                <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Frota_tipo_manutencao->descricao ?>"
                                    required placeholder="Descricao">
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="veiculo_id">Manutencao Classificacao</label><span class="glyphicon glyphicon-asterisk"></span>
                                <select name="manutencao_classificacao_id" class="form-control selectPicker" id="manutencao_classificacao_id">
                                    <option></option>
                                    <?php
                                        foreach ($Frota_manutencao_classificacao as $f) {
                                                echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                            }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                          <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                          <?php if($this->getParam('modal')){ ?>
                            <div class="text-right">
                            <input type="hidden" name="modal" value="1"/>
                                <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')"> Cancelar
                                </a>
                                <input type="submit" onclick="EnviarFormulario('forme-item');" class="btn btn-primary" value="salvar">
                            </div>
                            <?php }else{?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                        <?php }?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>