<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Manutenção - Itens</h2>
        <ol class="breadcrumb">
            <li>Manutenção - Itens</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="descricao">Descricao</label>
                                <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Frota_tipo_manutencao->descricao ?>"
                                    placeholder="Descricao">
                            </div>
                            <div class="form-group">
                                <label for="manutencao_classificacao_id">Manutenção Classificacão</label>
                                <select name="manutencao_classificacao_id" class="form-control" id="manutencao_classificacao_id">
                                    <option></option>
                                    <?php
                                        foreach ($Frota_manutencao_classificacao as $f) {
                                            if ($f->id == $Frota_tipo_manutencao->manutencao_classificacao_id)
                                                    echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                            else
                                                echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                            }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Frota_tipo_manutencao->id;?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>