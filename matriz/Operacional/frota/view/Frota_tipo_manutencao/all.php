<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Manutenção - Itens</h2>
        <ol class="breadcrumb">
            <li>Manutenção - Itens</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                <?php if($this->getParam('modal')){ ?>
                            <!--passa param para a modal-->
                <?php }else{?>
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="descricao">Descrição</label>
                                    <input type="text" name="filtro[interno][descricao]" id="descricao" class="form-control"
                                        value="<?php echo $this->getParam('descricao'); ?>">
                                </div>
                                <?php echo $this->getParam('modal'); ?>
                                <div class="col-md-3 form-group">
                                    <label for="manutencao_classificacao_id">Classificação</label>
                                    <select name="filtro[externo][manutencao_classificacao_id]" class="form-control selectPicker"
                                        id="manutencao_classificacao_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_manutencao_classificacao as $f): ?>
                                        <?php echo '<option value="' . $f->id . '">' . $f->nome . ' </option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" id="btnReset" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                     <!-- botao de cadastro -->
                    <div class="text-right">
                        <p>
                            <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Item', 'Frota_tipo_manutencao', 'add', NULL, array('class' => 'btn btn-primary')); ?>
                        </p>
                    </div>
                <?php }?>
                <?php if($this->getParam('modal')){ ?>
                            <!--passa param para a modal-->
                             <!--passa param para a modal-->
                        <p><a href="Javascript:void(0)" class="btn btn-primary" 
                        onclick="Navegar('<?php echo $this->Html->getUrl("Frota_tipo_manutencao","add",array("modal"=>1,"ajax"=>true))?>','go')">
                        <span class="img img-add"></span> Novo Cadastro</a></p>
                <?php } ?>
                   

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                            array('orderBy'=> 'id')); ?>'>
                                            Codigo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                            array('orderBy'=> 'descricao')); ?>'>
                                            Descrição
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                            array('orderBy'=> 'dt_cadastro')); ?>'>
                                            Cadastrado em
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                            array('orderBy'=> 'usuario_id')); ?>'>
                                            Cadastrado por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                            array('orderBy'=> 'manutencao_classificacao_id')); ?>'>
                                            Classificação
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                    foreach ($Frota_tipo_manutencaos as $f) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($f->id, 'Frota_tipo_manutencao', 'view',
                                            array('id' => $f->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($f->descricao, 'Frota_tipo_manutencao', 'view',
                                            array('id' => $f->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink(DataTimeBr($f->dt_cadastro), 'Frota_tipo_manutencao', 'view',
                                            array('id' => $f->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';
                                        echo $this->Html->getLink($f->getUsuario()->nome, 'Usuario', 'view',
                                            array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        echo $this->Html->getLink($f->getManutencao_classificacao()->nome, 'Frota_tipo_manutencao', 'view',
                                            array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_tipo_manutencao', 'edit', 
                                            array('id' => $f->id), 
                                            array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_tipo_manutencao', 'delete', 
                                            array('id' => $f->id), 
                                            array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                        echo '</td>';
                                        echo '</tr>';
                                    }
                                    ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>

       
<script>
        /* faz a pesquisa com ajax */
        $(document).ready(function() {
            $('#search').keyup(function() {
                var r = true;
                if (r) {
                    r = false;
                    $("div.table-responsive").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Frota_tipo_manutencao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    else
                        echo '"' . $this->Html->getUrl('Frota_tipo_manutencao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    ?>
                     , function() {
                        r = true;
                    });
                }
            });
        });
    
    
    </script>
                </div>
            </div>
        </div>
    </div>
</div>    
<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>