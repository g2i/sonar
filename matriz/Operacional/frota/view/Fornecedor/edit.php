<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fornecedor</h2>
        <ol class="breadcrumb">
            <li>Fornecedor</li>
            <li class="active">
                <strong>Editar Fornecedor</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Fornecedor', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome">nome</label>
                                <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Fornecedor->nome ?>"
                                    placeholder="Nome">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="telefone">telefone</label>
                                <input type="text" name="telefone" id="telefone" class="form-control" value="<?php echo $Fornecedor->telefone ?>"
                                    placeholder="Telefone">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="celular">celular</label>
                                <input type="text" name="celular" id="celular" class="form-control" value="<?php echo $Fornecedor->celular ?>"
                                    placeholder="Celular">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="cep">cep</label>
                                <input type="text" name="cep" id="cep" class="form-control" value="<?php echo $Fornecedor->cep ?>"
                                    placeholder="Cep">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="cidade">cidade</label>
                                <input type="text" name="cidade" id="cidade" class="form-control" value="<?php echo $Fornecedor->cidade ?>"
                                    placeholder="Cidade">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="uf">uf</label>
                                <input type="text" name="uf" id="uf" class="form-control" value="<?php echo $Fornecedor->uf ?>"
                                    placeholder="Uf">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="numero">numero</label>
                                <input type="number" name="numero" id="numero" class="form-control" value="<?php echo $Fornecedor->numero ?>"
                                    placeholder="Numero">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="rua">rua</label>
                                <input type="text" name="rua" id="rua" class="form-control" value="<?php echo $Fornecedor->rua ?>"
                                    placeholder="Rua">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="bairro">bairro</label>
                                <input type="text" name="bairro" id="bairro" class="form-control" value="<?php echo $Fornecedor->bairro ?>"
                                    placeholder="Bairro">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="email">email</label>
                                <input type="text" name="email" id="email" class="form-control" value="<?php echo $Fornecedor->email ?>"
                                    placeholder="Email">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="cnpj">cnpj</label>
                                <input type="text" name="cnpj" id="cnpj" class="form-control" value="<?php echo $Fornecedor->cnpj ?>"
                                    placeholder="Cnpj">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="observacao">observacao</label>
                                <textarea name="observacao" id="observacao" class="form-control"><?php echo $Fornecedor->observacao ?></textarea>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="status">status</label>
                                <input type="number" name="status" id="status" class="form-control" value="<?php echo $Fornecedor->status ?>"
                                    placeholder="Status">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="cpf">cpf</label>
                                <input type="text" name="cpf" id="cpf" class="form-control" value="<?php echo $Fornecedor->cpf ?>"
                                    placeholder="Cpf">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="banco">banco</label>
                                <input type="text" name="banco" id="banco" class="form-control" value="<?php echo $Fornecedor->banco ?>"
                                    placeholder="Banco">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="agencia">agencia</label>
                                <input type="text" name="agencia" id="agencia" class="form-control" value="<?php echo $Fornecedor->agencia ?>"
                                    placeholder="Agencia">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="conta">conta</label>
                                <input type="text" name="conta" id="conta" class="form-control" value="<?php echo $Fornecedor->conta ?>"
                                    placeholder="Conta">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="operacao">operacao</label>
                                <input type="text" name="operacao" id="operacao" class="form-control" value="<?php echo $Fornecedor->operacao ?>"
                                    placeholder="Operacao">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Fornecedor->id;?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Fornecedor', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>