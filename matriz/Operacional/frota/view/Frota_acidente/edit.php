<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Frota Acidente</h2>
        <ol class="breadcrumb">
            <li>Frota Acidente</li>
            <li class="active">
                <strong>Editar Frota Acidente</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_acidente', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="dt_ocorrencia">Dt.Ocorrencia</label>
                                <input type="date" name="dt_ocorrencia" id="dt_ocorrencia" class="form-control" value="<?php echo $Frota_acidente->dt_ocorrencia ?>"
                                    placeholder="Dt ocorrencia">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="dt_termino">Dt.Termino</label>
                                <input type="date" name="dt_termino" id="dt_termino" class="form-control" value="<?php echo $Frota_acidente->dt_termino ?>"
                                    placeholder="Dt termino">
                            </div>                           
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="orcamento_01">Orçamento 1</label>
                                <input type="text" name="orcamento_01" id="orcamento_01" class="form-control" value="<?php echo $Frota_acidente->orcamento_01 ?>"
                                    placeholder="Orcamento_01">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="orcamento_02">Orçamento 2</label>
                                <input type="text" name="orcamento_02" id="orcamento_02" class="form-control" value="<?php echo $Frota_acidente->orcamento_02 ?>"
                                    placeholder="Orcamento 02">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="orcamento_03">Orçamento 3</label>
                                <input type="text" name="orcamento_03" id="orcamento_03" class="form-control" value="<?php echo $Frota_acidente->orcamento_03 ?>"
                                    placeholder="Orcamento 03">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="veiculo_id">Frota Veiculos</label>
                                <select name="veiculo_id" class="form-control" id="veiculo_id">
                                    <?php
                                    foreach ($Frota_veiculos as $f) {
                                        if ($f->id == $Frota_acidente->veiculo_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipo_acidente_id">Tipo Acidente</label>
                                <select name="tipo_acidente_id" class="form-control" id="tipo_acidente_id">
                                    <?php
                                    foreach ($Frota_tipo_acidentes as $f) {
                                        if ($f->id == $Frota_acidente->tipo_acidente_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->descricao . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="profissional_id">Profissional</label>
                                <select name="profissional_id" class="form-control" id="profissional_id">
                                    <?php
                                    foreach ($Rhprofissionais as $r) {
                                        if ($r->codigo == $Frota_acidente->profissional_id)
                                            echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                                        else
                                            echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="situacao_acidente_id">Situação</label>
                                <select name="situacao_acidente_id" class="form-control" id="situacao_acidente_id">
                                <?php
                                foreach ($Frota_situacao_acidentes as $f) {
                                    if ($f->id == $Frota_acidente->situacao_acidente_id)
                                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                    else
                                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                }
                                ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="descricao_termino">Descrição Termino</label>
                                <textarea name="descricao_termino" id="descricao_termino" class="form-control"><?php echo $Frota_acidente->descricao_termino ?></textarea>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="descricao_ocorrencia">Descrição Ocorrencia</label>
                                <textarea name="descricao_ocorrencia" id="descricao_ocorrencia" class="form-control"><?php echo $Frota_acidente->descricao_ocorrencia ?></textarea>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <input type="hidden" name="id" value="<?php echo $Frota_acidente->id;?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_acidente', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>