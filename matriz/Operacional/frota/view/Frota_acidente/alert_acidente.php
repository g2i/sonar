<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Acidentes</h2>
                <ol class="breadcrumb">
                    <li>Acidentes</li>
                    <li class="active">
                        <strong>Em aberto</strong>
                    </li>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    Placa
                                </th>
                                <th>
                                    Tipo de acidente
                                </th>
                                <th>
                                    Motorista
                                </th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php
                                    foreach ($Frota_acidentes as $f) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $f->getFrota_veiculos()->placa;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $f->getFrota_tipo_acidente()->descricao;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $f->getRhprofissional()->nome;
                                        echo '</td>'; 
                                        echo '</tr>';
                                    }
                                ?>
                        </table>
                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php echo $nav; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>