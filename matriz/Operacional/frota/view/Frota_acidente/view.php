<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Frota Acidente</h2>
        <ol class="breadcrumb">
        <li>Frota Acidente</li>
        <li class="active">
        <strong>Detalhes</strong>
        </li></ol></div></div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="clearfix">  
                    <div class="table-responsive">
                        <table class="table table-hover">
                        <tr>

                            <th>Descrição Ocorrência</th>
                            <th>Data Ocorrência</th>
                            <th>Descrição terminino</th>   
                            <th>Data Terminino</th>                 
                            <th>Orçamento 1</th>
                            <th>Orçamento 2</th>
                            <th>Orçamento 3</th>
                            <th>Veiculo</th>
                            <th>Tipo Acidente</th>
                            <th>Profissional</th>
                            <th>Situação</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                            <tr>
                                <td><?php echo $Frota_acidente->descricao_ocorrencia;?></td>
                                <td><?php echo DataBR($Frota_acidente->dt_ocorrencia);?></td>
                                <td><?php echo $Frota_acidente->descricao_termino;?></td>    
                                <td><?php echo DataBR($Frota_acidente->dt_termino);?></td>                        
                                <td><?php echo $Frota_acidente->orcamento_01;?></td>
                                <td><?php echo $Frota_acidente->orcamento_02;?></td>
                                <td><?php echo $Frota_acidente->orcamento_03;?></td>
                                <td><?php echo $Frota_acidente->getFrota_veiculos()->placa;?></td>
                                <td><?php echo $Frota_acidente->getFrota_tipo_acidente()->descricao; ?></td>
                                <td><?php echo $Frota_acidente->getRhprofissional()->nome; ?></td>
                                <td><?php echo $Frota_acidente->getSituacaoAcidente()->nome; ?></td>     
                            </tr>
                        </table>
                          <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="fa fa-ambulance"></span> Voltar', 'Frota_acidente', 'acidentes', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    </div>
                </div> 
            </div>
            </div>
            </div>
            </div>

    </div> <!-- col-lg-9-->         
</div> <!--row wrapper-->
    