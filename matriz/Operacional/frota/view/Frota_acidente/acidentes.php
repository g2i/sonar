
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Frota Acidente</h2>
    <ol class="breadcrumb">
    <li>Frota Acidente</li>
    <li class="active">
    <strong>Lista Geral</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                <div class="col-md-3 form-group">
                    <label for="veiculo_id">Placa Veiculo</label>
                    <select name="filtro[externo][veiculo_id]" class="form-control" id="veiculo_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Frota_veiculos as $f): ?>
                            <?php echo '<option value="' . $f->id . '">' . $f->placa . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="situacao_acidente_id">Situação</label>
                    <select name="filtro[externo][situacao_acidente_id]" class="form-control" id="situacao_acidente_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Frota_situacao_acidentes as $f): ?>
                            <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="profissional_id">Profissional</label>
                    <select name="filtro[externo][profissional_id]" class="form-control" id="profissional_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Rhprofissionais as $r): ?>
                            <?php echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="tipo_acidente_id">Tipo Acidente</label>
                    <select name="filtro[externo][tipo_acidente_id]" class="form-control" id="tipo_acidente_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Frota_tipo_acidentes as $f): ?>
                            <?php echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"  class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                           title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
        
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Frota_acidente', 
        'add', NULL, array('class' => 'btn btn-primary')); ?></p>
       

    </div>
  
<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='#'>
                        id
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Dt.Descrição
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Descrição Ocorrencia
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Tipo acidente
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Funcionario
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Dt.Termino
                    </a>
                </th>
                <th>
                    <a href='#'>
                        Situação
                    </a>
                </th>
               
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Frota_acidentes as $f) {
                echo '<tr>';
                echo '<td>';
                echo $f->id; 
                echo '</td>';
                echo '<td>';
                echo DataBR($f->dt_ocorrencia); 
                echo '</td>';
                echo '<td>';
                echo $f->descricao_ocorrencia; 
                echo '</td>';
                echo '<td>';
                echo $f->getFrota_tipo_acidente()->descricao;   
                echo '</td>';
                echo '<td>';
                echo $f->getRhprofissional()->nome; 
                echo '</td>';
                echo '<td>';
                echo DataBR($f->dt_termino); 
                echo '</td>';
                echo '<td>';
                echo $f->getSituacaoAcidente()->nome; 
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_acidente', 'edit', 
                    array('id' => $f->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="fa fa-files-o"></span> Detalhes', 'Frota_acidente', 'view', 
                    array('id' => $f->id), 
                    array('class' => 'btn btn-primary btn-sm'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Frota_acidente', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Frota_acidente', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>