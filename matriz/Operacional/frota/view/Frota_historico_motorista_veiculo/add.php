
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Frota_historico_motorista_veiculo</h2>
    <ol class="breadcrumb">
    <li>Frota_historico_motorista_veiculo</li>
    <li class="active">
    <strong>Adicionar Frota_historico_motorista_veiculo</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_historico_motorista_veiculo', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="ultima_data">ultima_data</label>
            <input type="datetime" name="ultima_data" id="ultima_data" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->ultima_data ?>" placeholder="Ultima_data">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="cadastrado_por">cadastrado_por</label>
            <input type="number" name="cadastrado_por" id="cadastrado_por" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->cadastrado_por ?>" placeholder="Cadastrado_por">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dt_cadastro">dt_cadastro</label>
            <input type="datetime" name="dt_cadastro" id="dt_cadastro" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->dt_cadastro ?>" placeholder="Dt_cadastro">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="modificado_por">modificado_por</label>
            <input type="number" name="modificado_por" id="modificado_por" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->modificado_por ?>" placeholder="Modificado_por">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dt_modificado">dt_modificado</label>
            <input type="datetime" name="dt_modificado" id="dt_modificado" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->dt_modificado ?>" placeholder="Dt_modificado">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="veiculo_id">veiculo_id</label>
            <select name="veiculo_id" class="form-control" id="veiculo_id">
                <?php
                foreach ($Frota_veiculos as $f) {
                    if ($f->id == $Frota_historico_motorista_veiculo->veiculo_id)
                        echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="motorista_antigo_id">motorista_antigo_id</label>
            <select name="motorista_antigo_id" class="form-control" id="motorista_antigo_id">
                <?php
                foreach ($Frota_motoristas as $f) {
                    if ($f->id == $Frota_historico_motorista_veiculo->motorista_antigo_id)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="situacao_id">situacao_id</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Frota_historico_motorista_veiculo->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Frota_historico_motorista_veiculo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>