
    <!-- <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Frota_historico_motorista_veiculo</h2>
    <ol class="breadcrumb">
    <li>Frota_historico_motorista_veiculo</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    formulario de pesquisa 
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                <div class="col-md-3 form-group">
                    <label for="veiculo_id">veiculo_id</label>
                    <select name="filtro[externo][veiculo_id]" class="form-control" id="veiculo_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Frota_veiculos as $f): ?>
                            <?php echo '<option value="' . $f->id . '">' . $f->placa . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="motorista_antigo_id">motorista_antigo_id</label>
                    <select name="filtro[externo][motorista_antigo_id]" class="form-control" id="motorista_antigo_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Frota_motoristas as $f): ?>
                            <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    botao de cadastro 
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Frota_historico_motorista_veiculo', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>-->

<!-- tabela de resultados -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Histórico de Motoristas</h2>
        <ol class="breadcrumb">
            <li>Histórico de Motoristas</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>

<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    
                </th>
                <th>
                    Veículo
                </th>
                <th>
                    Motorista
                </th>
                <th>
                    Ultima data
                    
                </th>           
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Frota_historico_motorista_veiculos as $f) {
                echo '<tr>';
                echo '<td>';
                
                echo '</td>';
                echo '<td>';
                echo  $f->getFrota_veiculos()->placa;
                echo '</td>';
                echo '<td>';
                echo  $f->getFrota_motorista()->nome;
                echo '<td>';
                echo  DataTimeBr($f->ultima_data);             
                echo '</td>';            
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Frota_historico_motorista_veiculo', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Frota_historico_motorista_veiculo', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>