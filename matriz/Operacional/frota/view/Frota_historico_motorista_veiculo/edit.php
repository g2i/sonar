
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Frota_historico_motorista_veiculo</h2>
    <ol class="breadcrumb">
    <li>Frota_historico_motorista_veiculo</li>
    <li class="active">
    <strong>Editar Frota_historico_motorista_veiculo</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_historico_motorista_veiculo', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="ultima_data">Ultima_data</label>
            <input type="datetime" name="ultima_data" id="ultima_data" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->ultima_data ?>" placeholder="Ultima_data">
        </div>
        <div class="form-group">
            <label for="cadastrado_por">Cadastrado_por</label>
            <input type="number" name="cadastrado_por" id="cadastrado_por" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->cadastrado_por ?>" placeholder="Cadastrado_por">
        </div>
        <div class="form-group">
            <label for="dt_cadastro">Dt_cadastro</label>
            <input type="datetime" name="dt_cadastro" id="dt_cadastro" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->dt_cadastro ?>" placeholder="Dt_cadastro">
        </div>
        <div class="form-group">
            <label for="modificado_por">Modificado_por</label>
            <input type="number" name="modificado_por" id="modificado_por" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->modificado_por ?>" placeholder="Modificado_por">
        </div>
        <div class="form-group">
            <label for="dt_modificado">Dt_modificado</label>
            <input type="datetime" name="dt_modificado" id="dt_modificado" class="form-control" value="<?php echo $Frota_historico_motorista_veiculo->dt_modificado ?>" placeholder="Dt_modificado">
        </div>
        <div class="form-group">
            <label for="veiculo_id">Frota_veiculos</label>
            <select name="veiculo_id" class="form-control" id="veiculo_id">
                <?php
                foreach ($Frota_veiculos as $f) {
                    if ($f->id == $Frota_historico_motorista_veiculo->veiculo_id)
                        echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="motorista_antigo_id">Frota_motorista</label>
            <select name="motorista_antigo_id" class="form-control" id="motorista_antigo_id">
                <?php
                foreach ($Frota_motoristas as $f) {
                    if ($f->id == $Frota_historico_motorista_veiculo->motorista_antigo_id)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Frota_historico_motorista_veiculo->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Frota_historico_motorista_veiculo->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Frota_historico_motorista_veiculo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>