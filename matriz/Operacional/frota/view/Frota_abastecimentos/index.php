<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Consumo Médio</h2>
        <ol class="breadcrumb">
            <li>Consumo Médio</li>
            <li class="active">
                <strong>Média de Consumo dos Veículos</strong>
            </li>
        </ol>
    </div>
</div>
<!--aixar de alertas-->
<div class="wrapper wrapper-content animated fadeInRight">   
    <div class="col-md-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-danger pull-right">Últimos 30 dias</span>
                <h5>Licenciamento </h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <?php $totalLicien = null; foreach($contLicen as $li){ $totalLicien +=1;} echo $totalLicien;?>
                </h1>
                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                <small><?php echo $this->Html->getLink('<span ></span>   A vencer', 'Frota_veiculos', 'alert_licenciamento',
                    array('modal' => 1),
                    array('class' => 'btn  btn-xs ', 'data-toggle' => 'modal')); ?>
                  </small>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ibox float-e-margins">       
            <div class="ibox-title">
                <span class="label label-danger pull-right">Últimos 30 dias</span>
                <h5>IPVA</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                <?php $totalIpva = null; foreach($ContIpva as $ip){ $totalIpva +=1;} echo $totalIpva;?>
                </h1>
                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                <small><?php echo $this->Html->getLink('<span ></span>   A vencer', 'Frota_veiculos', 'alert_ipva',
                    array('id' => $f->id, 'modal' => 1),
                    array('class' => 'btn  btn-xs ', 'data-toggle' => 'modal')); ?>
                  </small>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ibox float-e-margins">       
            <div class="ibox-title">
                <span class="label label-success pull-right">Em aberto</span>
                <h5>Acidentes</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <?php $total = null; 
                    foreach($contAcidentes as $co){ 
                        $total +=1;
                    } echo $total;  ?>
                </h1>
                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                <small><?php echo $this->Html->getLink('<span ></span>   Em aberto', 'Frota_acidente', 'alert_acidente',
                    array('id' => $f->id, 'modal' => 1),
                    array('class' => 'btn  btn-xs ', 'data-toggle' => 'modal')); ?>
                  </small>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ibox float-e-margins">       
            <div class="ibox-title">
                <span class="label label-danger pull-right">Últimos 45 dias</span>
                <h5>Manutenções</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <?php echo $contManutencao->total; ?>
                </h1>
                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>                
                <small><?php echo $this->Html->getLink('<span ></span>   A vencer', 'Frota_manutencoes', 'alert_manutencao',
                    array('id' => $f->id, 'modal' => 1),
                    array('class' => 'btn  btn-xs ', 'data-toggle' => 'modal')); ?>
                  </small>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ibox float-e-margins">       
            <div class="ibox-title"> 
                <span class="label label-danger pull-right">Últimos 45 dias</span>
                <h5>Equipamento Auxiliar</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <?php echo $contEquipamentAux->total; ?>
                </h1>
                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                <small><?php echo $this->Html->getLink('<span ></span>   A vencer', 'Frota_equipamentos_veiculo', 'alert_equipamento_vencimento',
                    array('id' => $f->id, 'modal' => 1),
                    array('class' => 'btn  btn-xs ', 'data-toggle' => 'modal')); ?>
                  </small>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="ibox float-e-margins">       
            <div class="ibox-title"> 
                <span class="label label-danger pull-right">Últimos 60 dias</span>
                <h5>CNH</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins">
                    <?php echo $contCnhVencidas->total; ?>
                </h1>
                <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                <small><?php echo $this->Html->getLink('<span ></span>   A vencer', 'Frota_motorista', 'alert_cnh_vencimento',
                    array('id' => $f->id, 'modal' => 1),
                    array('class' => 'btn  btn-xs ', 'data-toggle' => 'modal')); ?>
                  </small>
            </div>
        </div>
    </div>    
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <!-- formulario de pesquisa -->
                <div class="filtros well">
                    <div class="form">

                        <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                            method="post" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                            <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                            <h4 style="padding-top: 5px;">Período de Abastecimento:</h4>

                            <div class="col-md-3 form-group">
                                <label for="data_inicial">Inicio</label>
                                <input type="date" name="inicio" id="inicio" class="form-control maskData" value="">
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="data_final">Fim</label>
                                <input type="date" name="fim" id="fim" class="form-control maskData" value="">
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="veiculo_id">Veículo</label>
                                <select name="filtro[externo][veiculo_id]" class="form-control selectPicker" id="veiculo_id">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($veiculos as $v): ?>
                                    <?php echo '<option value="' . $v->id . '">' . $v->placa . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="localizacao_id">Localização</label>
                                <select name="localizacao_id" class="form-control selectPicker" id="localizacao_id">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Frota_localizacao as $f): ?>
                                    <?php echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-search"
                                        id="btn-filtro"></span></button>
                                <button type="button" id="btnReset" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                            </div>
                            <div class="clearfix"></div>
                        </form>


                    </div>
                </div>
                <!-- tabela de resultados -->
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl(' Frota_veiculos', 'all' , array('orderBy'=>
                                        'id')); ?>'>
                                        Veiculo / Placa
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                        array('orderBy'=> 'descricao')); ?>'>
                                        Data do Abastecimento
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                        array('orderBy'=> 'dt_cadastro')); ?>'>
                                        Quilometragem no abastecimento
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                        array('orderBy'=> 'usuario_id')); ?>'>
                                        Quantidade (Litros)
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                        array('orderBy'=> 'usuario_id')); ?>'>
                                        Preço por litro
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                        array('orderBy'=> 'usuario_id')); ?>'>
                                        Valor do abastecimento
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl(' Frota_tipo_manutencao', 'all' ,
                                        array('orderBy'=> 'usuario_id')); ?>'>
                                        Média de Consumo
                                    </a>
                                </th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>

                            <?php
                            $flag = true;
                            foreach ($abastecimentos as $a) {
                                echo '<tr>';
                                echo '<td>';
                                echo $this->Html->getLink($a->getFrota_veiculos()->placa, 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink(DataBR($a->dt_abastecimento), 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink($a->quilometragem, 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink($a->quantidade, 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink('R$ ' . sprintf('%0.3f', $a->valor_litro), 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink('R$ ' . sprintf('%0.2f', $a->quantidade * $a->valor_litro), 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id),
                                    array('data-toggle' => 'modal'));
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink($a->media_consumo . ' km/l ', 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>

                        </table>

                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php echo $nav; ?>
                        </div>

                    </div>
                </div>

                <?php if (!empty($manutencoes) || !empty($motoristas)) { ?>
                <!-- Após exibir a modal, a variavel id é setada com outro nome para que o modal não seja carregado no javascript caso a pagina seja revisitada, id é uma variavel de sessão setada no LoginController -->
                <div class="modal fade" id="<?php echo Session::get('id');
                    Session::set('id', "
                    none") ?>" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header badge-danger">
                                <h3 class="modal-title">Atenção</h3>
                            </div>

                            <?php if (!empty($manutencoes)) { ?>

                            <div class="modal-body">
                                <h3>Manutenções proximas do vencimento</h3>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>Veículo</th>
                                            <th>Manutenção</th>
                                            <th>Data Manutenção</th>
                                            <th>Km</th>
                                            <th>Vida Util</th>
                                        </tr>

                                        <?php
                                                foreach ($manutencoes as $a) {
                                                    echo '<tr bgcolor="#FFCDD2">';
                                                    echo '<td><strong>' . $a->placa . '</strong></td>';
                                                    echo '<td><strong>' . $a->manutencao . '<strong></td>';
                                                    echo '<td><strong>' . DataBR($a->dt_manutencao) . '</strong></td>';
                                                    echo '<td><strong>' . $a->quilometragem . '</strong></td>';
                                                    echo '<td><strong>' . $a->vida_util . '</strong></td>';
                                                    echo '</tr>';
                                                };
                                                ?>

                                    </table>
                                </div>
                            </div>

                            <?php } ?>

                            <?php if (!empty($motoristas)) { ?>

                            <div class="modal-body">
                                <div class="text-right">
                                    <a href="<?php echo $this->Html->getUrl('Frota_motorista', 'cnhs_vencidas') ?>"
                                        class="btn-lg btn-danger" target="_blank">CNH's Vencidas</a>
                                </div>
                                <hr>
                                <h3>Motoristas com CNH á vencer nos proximos 60 dias</h3>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>Motorista</th>
                                            <th>Vencimento CNH</th>
                                            <th>Veículo atual</th>
                                            <th>Dias restantes</th>
                                        </tr>

                                        <?php
                                                foreach ($motoristas as $m) {
                                                    echo '<tr bgcolor="#FFCDD2">';
                                                    echo '<td><strong>' . $m->nome . '</strong></td>';
                                                    echo '<td><strong>' . DataBR($m->validade_cnh) . '</strong></td>';
                                                    if(!empty($m->placa)) {
                                                        echo '<td><strong>' . $m->placa . '<strong></td>';
                                                    } else {
                                                        echo '<td><strong> Não designado <strong></td>';
                                                    }
                                                    echo '<td style="text-align: center"><strong>' . $m->dias . '</strong></td>';
                                                    echo '</tr>';
                                                };
                                                ?>

                                    </table>
                                </div>
                            </div>
                            <?php } ?>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger" data-dismiss="modal">OK</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <?php } ?>

                <script>
                    /* faz a pesquisa com ajax */
                    $(document).ready(function () {
                        $('#search').keyup(function () {
                            var r = true;
                            if (r) {
                                r = false;
                                $("div.table-responsive").load(
                                    <?php
                                    if (isset($_GET['orderBy']))
                                        echo '"' . $this->Html->getUrl('Frota_abastecimentos', 'index', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    else
                                        echo '"' . $this->Html->getUrl('Frota_abastecimentos', 'index') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    ?>
                                    , function () {
                                        r = true;
                                    });
                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</div>

<script>
    /* $(function ($) {
         $(window).load(function () {
 
             $("#banner").modal({
                 show: true,
                 keyboard: false,
                 backdrop: 'static'
             });
         })
     });
     $('#btnReset').click(function () {
         window.location.href = window.location.href;
     });*/
</script>