<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Abastecimentos</h2>
        <ol class="breadcrumb">
            <li>Abastecimentos</li>
            <li class="active">
                <strong>Adicionar Abastecimentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_abastecimentos', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">

                            <div class="form-group">
                                <label for="veiculo_id">Veículos</label>
                                <select name="veiculo_id" class="form-control" id="veiculo_id" required>
                                    <?php
                                    foreach ($Frota_veiculos as $f) {
                                        if ($f->id == $Frota_abastecimentos->veiculo_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="fornecedor_id">Fornecedor</label>
                                <select name="fornecedor_id" class="form-control" id="fornecedor_id" required>
                                    <?php
                                    foreach ($Fornecedores as $f) {
                                        if ($f->id == $Frota_abastecimentos->fornecedor_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="motorista_id">Motorista</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <select name="motorista_id" class="form-control selectPicker" id="motorista_id"
                                        required>
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Frota_motorista as $motorista) {
                                        if ($motorista->id == $Frota_abastecimentos->motorista_id)
                                            echo '<option selected value="' . $motorista->id . '">' . $motorista->nome . '</option>';
                                        else
                                            echo '<option value="' . $motorista->id . '">' . $motorista->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="quilometragem_anterior">Quilometragem anterior</label>
                                <input type="number" step="any" name="quilometragem_anterior"
                                       id="quilometragem_anterior" class="form-control quilometragem"
                                       value="<?php echo $Frota_abastecimentos->quilometragem_anterior ?>"
                                       placeholder="Quilometragem anterior">
                            </div>

                            <div class="form-group">
                                <label for="quilometragem" class="required">Quilometragem Atual</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="quilometragem" id="quilometragem"
                                       class="form-control quilometragem"
                                       value="<?php echo $Frota_abastecimentos->quilometragem ?>"
                                       required placeholder="Quilometragem Atual"/>
                            </div>

                            <div class="form-group">
                                <label for="quantidade">Quantidade litro</label>
                                <input step="any" type="number" name="quantidade" id="quantidade" class="form-control"
                                       value="<?php echo $Frota_abastecimentos->quantidade ?>" placeholder="Quantidade"
                                       required>
                            </div>
                            <div class="form-group">
                                <label for="valor_litro">Valor por litro</label>
                                <input type="number" step="any" name="valor_litro" id="valor_litro" class="form-control"
                                       value="<?php echo $Frota_abastecimentos->valor_litro ?>"
                                       placeholder="Valor por litro" required>
                            </div>

                            <div class="form-group">
                                <label for="valor_total_pago" class="required">Valor Total Pago</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="valor_total_pago" id="valor_total_pago"
                                       class="form-control"
                                       value="<?php echo $Frota_abastecimentos->valor_total_pago ?>"
                                       required placeholder="Valor Total Pago"/>
                            </div>

                            <div class="form-group">
                                <label for="combustivel_id">Tipo combustível</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <select name="combustivel_id" class="form-control selectPicker" id="combustivel_id"
                                        required>
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Frota_combustivel as $combustivel) {
                                        if ($combustivel->id == $Frota_abastecimentos->combustivel_id)
                                            echo '<option selected value="' . $combustivel->id . '">' . $combustivel->nome . '</option>';
                                        else
                                            echo '<option value="' . $combustivel->id . '">' . $combustivel->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="dt_abastecimento">Data do Abastecimento</label>
                                <input type="date" name="dt_abastecimento" id="dt_abastecimento" class="form-control"
                                       value="<?php echo $Frota_abastecimentos->dt_abastecimento ?>"
                                       placeholder="Data do Abastecimento" required>
                            </div>

                            <div class="form-group">
                                <label for="km_rodado" class="required">KM Rodado</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="km_rodado" id="km_rodado"
                                       value="<?php echo $Frota_abastecimentos->km_rodado ?>"
                                       class="form-control" value=""
                                       required placeholder="KM anterior - KM atual" readonly/>
                            </div>

                            <div class="form-group">
                                <label for="litro_km" class="required">Litro por km</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="litro_km" id="litro_km"
                                       value="<?php echo $Frota_abastecimentos->litro_km ?>"
                                       class="form-control" value=""
                                       required placeholder="KM Rodado / Qnt de litros" readonly/>
                            </div>

                            <div class="form-group">
                                <label for="valor_por_km" class="required">Valor por KM</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="valor_por_km" id="valor_por_km"
                                       value="<?php echo $Frota_abastecimentos->valor_por_km ?>"
                                       class="form-control" value=""
                                       required placeholder="Valor total pago / KM Rodado" readonly/>
                            </div>

                        </div>
                        <input type="hidden" name="id" value="<?php echo $Frota_abastecimentos->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        /**
         *  Controller: Frota_abastecimento
         *  Para todos os scripts abaixo
         * */
        $('.quilometragem').keyup(function () {
            kmRodado();
            qntLitro();
            valorTotalPago();
        });

        $('#quantidade').keyup(function () {
            qntLitro();
        });

        $('#valor_total_pago').keyup(function () {
            valorTotalPago();
        });
    });

    function kmRodado() {
        var kmAnterior = $('#quilometragem_anterior').val();
        var kmAtual = $('#quilometragem').val();

        $('#km_rodado').val(kmAtual - kmAnterior);
    }

    function qntLitro() {
        var kmRodado = $('#km_rodado').val();
        var qntLitro = $('#quantidade').val();

        $('#litro_km').val((kmRodado / qntLitro));
    }

    function valorTotalPago() {
        var valorTotal = $('#valor_total_pago').val();
        var kmRodado = $('#km_rodado').val();

        $('#valor_por_km').val((valorTotal / kmRodado).toFixed(3));
    }

</script>