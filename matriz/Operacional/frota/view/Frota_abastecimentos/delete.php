<form class="form" method="post" action="<?php echo $this->Html->getUrl('Frota_abastecimentos', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Frota_abastecimentos->id; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Frota_abastecimentos->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>