<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Abastecimentos</h2>
        <ol class="breadcrumb">
            <li>Abastecimentos</li>
            <li class="active">
                <strong>Todos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                                <div class="col-md-3 form-group">
                                    <div class="form-group">
                                        <label for="veiculo_id">Veículo</label>
                                        <select id="veiculo_id" name="veiculo_id[]" multiple class="form-control selectPicker required">
                                            <option></option>
                                            <?php foreach ($Frota_veiculos as $f): ?>
                                                <option value="<?php echo $f->id ?>"><?php echo $f->placa ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-3 form-group">
                                    <label for="dt_abastecimento">Data do Abastecimento Inicio</label>
                                    <input type="date" name="dt_abastecimento_inicio" id="dt_abastecimento_inicio"
                                           class="form-control maskData"
                                           value="<?php echo $this->getParam('dt_abastecimento'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="dt_abastecimento">Data do Abastecimento Fim</label>
                                    <input type="date" name="dt_abastecimento_fim" id="dt_abastecimento_fim"
                                           class="form-control maskData"
                                           value="<?php echo $this->getParam('dt_abastecimento'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="localizacao_id">Localização</label>
                                    <select name="localizacao_id" class="form-control"
                                            id="localizacao_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_localizacao as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" onclick="downloadExcel()" class="btn btn-default botao-download"><span
                                            class="fa fa-file-excel-o"></span></button>
                                    <button type="button" id="btnReset" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Abastecimento', 'Frota_abastecimentos', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                            Cadastrado por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'veiculo_id')); ?>'>
                                            Veículo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'fornecedor_id')); ?>'>
                                            Fornecedor
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                                            Cadastrado em
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'quilometragem')); ?>'>
                                            Quilometragem
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'quantidade')); ?>'>
                                            Quantidade
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'valor_litro')); ?>'>
                                            Valor por litro
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => 'dt_abastecimento')); ?>'>
                                            Data do Abastecimento
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_abastecimentos as $f) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getUsuario()->login, 'Usuario', 'view',
                                        array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
                                        array('id' => $f->getFrota_veiculos()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFornecedor()->nome, 'Fornecedor', 'view',
                                        array('id' => $f->getFornecedor()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataTimeBr($f->dt_cadastro), 'Frota_abastecimentos', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->quilometragem, 'Frota_abastecimentos', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->quantidade, 'Frota_abastecimentos', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink('R$ ' . sprintf('%0.3f', $f->valor_litro), 'Frota_abastecimentos', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($f->dt_abastecimento), 'Frota_abastecimentos', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_abastecimentos', 'edit',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-warning btn-sm'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_abastecimentos', 'delete',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Frota_abastecimentos', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Frota_abastecimentos', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function downloadExcel(){
        var veiculos = $('#veiculo_id').val();
        var dt_abastecimento_inicio = $('#dt_abastecimento_inicio').val();
        var dt_abastecimento_fim = $('#dt_abastecimento_fim').val();
        var localizacao_id = $('#localizacao_id').val();


        var queryString = 'dt_abastecimento_inicio='+dt_abastecimento_inicio;
        queryString +='&dt_abastecimento_fim='+dt_abastecimento_fim+'&localizacao_id='+localizacao_id+'&veiculo_id=';
        console.log(veiculos);
        if(veiculos != null )
            queryString += veiculos;
        var url = root+'/Frota_abastecimentos/downloadExcel?'+queryString ;
        window.open(url,'_blank');
    }

    $('#btnReset').unbind('click').bind('click',function () {
        window.location.href = window.location.href;
    });
</script>