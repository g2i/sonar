<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Abastecimentos</h2>
        <ol class="breadcrumb">
            <li>Abastecimentos</li>
            <li class="active">
                <strong>Adicionar Abastecimentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_abastecimentos', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="veiculo_id">Veículo</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <select name="veiculo_id" class="form-control selectPicker" id="veiculo_id" required>
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Frota_veiculos as $f) {
                                        if ($f->id == $Frota_abastecimentos->veiculo_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->placa . '</option>';

                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="fornecedor_id">Fornecedor</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <select name="fornecedor_id" class="form-control selectPicker" id="fornecedor_id"
                                        required>
                                    <?php
                                    foreach ($Fornecedores as $f) {
                                        if ($f->id == $Frota_abastecimentos->fornecedor_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="motorista_id">Motorista</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <select name="motorista_id" class="form-control selectPicker" id="motorista_id"
                                        required>
                                    <option value="">Selecione</option>
                                    <?php

                                    foreach ($Frota_motorista as $motorista) {
                                        echo '<option value="' . $motorista->id . '">' . $motorista->getRhprofissional()->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="quilometragem_anterior">Quilometragem Anterior </label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="quilometragem_anterior"
                                       id="quilometragem_anterior" class="form-control quilometragem"
                                       required placeholder="Quilometragem Anterior"/>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="quilometragem" class="required">Quilometragem Atual</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="quilometragem" id="quilometragem"
                                       class="form-control quilometragem"
                                       value="<?php echo $Frota_abastecimentos->quilometragem ?>"
                                       required placeholder="Quilometragem Atual"/>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="quantidade" class="required">Quantidade litro</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="quantidade" id="quantidade" class="form-control"
                                       value="<?php echo $Frota_abastecimentos->quantidade ?>" required
                                       placeholder="Quantidade">
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="valor_total_pago" class="required">Valor Total Pago</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="valor_total_pago" id="valor_total_pago"
                                       class="form-control" value=""
                                       required placeholder="Valor Total Pago"/>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="dt_abastecimento" class="required"> Data do abastecimento</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="date" name="dt_abastecimento" id="dt_abastecimento" class="form-control"
                                       required
                                       value="<?php echo $Frota_abastecimentos->dt_abastecimento ?>"
                                       placeholder="Data do Abastecimento"/>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="combustivel_id">Tipo combustível</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <select name="combustivel_id" class="form-control selectPicker" id="combustivel_id"
                                        required>
                                    <option value="">Selecione</option>
                                    <?php
                                    foreach ($Frota_combustivel as $combustivel) {
                                        if ($combustivel->id == $Frota_combustivel->combustivel_id)
                                            echo '<option selected value="' . $combustivel->id . '">' . $combustivel->nome . '</option>';
                                        else
                                            echo '<option value="' . $combustivel->id . '">' . $combustivel->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="valor_litro" class="required">Valor por litro</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="valor_litro" id="valor_litro" class="form-control"
                                       value="<?php echo $Frota_abastecimentos->valor_litro ?>"
                                       required placeholder="Valor por litro">
                            </div>

                            <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                <label for="km_rodado" class="required">KM Rodado</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="km_rodado" id="km_rodado"
                                       class="form-control" value=""
                                       required placeholder="KM anterior - KM atual" readonly/>
                            </div>

                            <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                <label for="litro_km" class="required">Litro por km</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="litro_km" id="litro_km"
                                       class="form-control" value=""
                                       required placeholder="KM Rodado / Qnt de litros" readonly/>
                            </div>

                            <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                <label for="valor_por_km" class="required">Valor por KM</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="valor_por_km" id="valor_por_km"
                                       class="form-control" value=""
                                       required placeholder="Valor total pago / KM Rodado" readonly/>
                            </div>

                            <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                <label for="km_ultimo_abastecimento">Maior quilometragem(abastecimentos): </label>
                                <input type="number" step="any" name="km_ultimo_abastecimento"
                                       id="km_ultimo_abastecimento" class="form-control"
                                       required placeholder="000000" readonly/>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_abastecimentos', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
