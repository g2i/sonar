<?php include_once "dataUtil.php"; ?>
<p><strong>Cadastrado em</strong>: <?php echo convertDataSQL4BR($Frota_abastecimentos->dt_cadastro);?></p>
<p><strong>Quilometragem</strong>: <?php echo $Frota_abastecimentos->quilometragem;?></p>
<p><strong>Quantidade</strong>: <?php echo $Frota_abastecimentos->quantidade;?></p>
<p><strong>Valor por litro</strong>: <?php echo $Frota_abastecimentos->valor_litro;?></p>
<p><strong>Data do Abastecimento</strong>: <?php echo convertDataSQL4BR($Frota_abastecimentos->dt_abastecimento);?></p>
<p><strong>Localização</strong>: <?php echo $Frota_abastecimentos->getFrota_veiculos()->getFrota_localizacao()->cidade;?></p>
<p>
    <strong>Veículo</strong>:
    <?php
    echo $this->Html->getLink($Frota_abastecimentos->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
    array('id' => $Frota_abastecimentos->getFrota_veiculos()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Cadastrado por</strong>:
    <?php
    echo $this->Html->getLink($Frota_abastecimentos->getUsuario()->login, 'Usuario', 'view',
    array('id' => $Frota_abastecimentos->getUsuario()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Fornecedor</strong>: 
    <?php
    echo $this->Html->getLink($Frota_abastecimentos->getFornecedor()->nome, 'Fornecedor', 'view',
    array('id' => $Frota_abastecimentos->getFornecedor()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>