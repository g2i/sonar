<div class="clearfix"></div>
<div class="result">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3>Lista de Médicos</h3>
        </div>
        <div class="ibox-content">
            <?php foreach ($Registros as $r): ?>
                <div class="list-group">
                    <a href="<?php echo $this->Html->getUrl('Cliente_solicitacoes', 'add',array('me'=>$r->id)) ?>" class="list-group-item">
                        <h4 class="list-group-item-heading">Procedimento: <?= $r->procedimento ?></h4>
                        <p class="list-group-item-text">Dr: <?= $r->nome ?> </p>
                        <p class="list-group-item-text">Estado: <?= $r->estado ?> - Cidade <?= $r->cidade; ?></p>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>