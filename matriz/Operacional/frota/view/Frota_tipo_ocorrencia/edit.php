
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Tipo de Ocorrência</h2>
    <ol class="breadcrumb">
    <li>Tipo de Ocorrência</li>
    <li class="active">
    <strong>Editar Tipo de Ocorrência</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated ">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_tipo_ocorrencia', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="descricao">Descrição</label>
            <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Frota_tipo_ocorrencia->descricao ?>" placeholder="Descricao">
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Frota_tipo_ocorrencia->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Frota_tipo_ocorrencia', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>