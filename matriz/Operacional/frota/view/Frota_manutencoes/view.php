<?php include_once "dataUtil.php"; ?>
<p><strong>Cadsstrado em</strong>: <?php echo convertDataSQL4BR($Frota_manutencoes->dt_cadastro);?></p>
<p><strong>Descrição</strong>: <?php echo $Frota_manutencoes->descricao;?></p>
<p><strong>Valor</strong>: <?php echo $Frota_manutencoes->valor;?></p>
<p><strong>Garantia</strong>: <?php echo $Frota_manutencoes->garantia;?></p>
<p><strong>Data da Manutenção</strong>: <?php echo convertDataSQL4BR($Frota_manutencoes->dt_manutencao);?></p>
<p><strong>Observação</strong>: <?php echo $Frota_manutencoes->observacao;?></p>
<p><strong>Quilometragem (Troca de Óleo)</strong>: <?php echo $Frota_manutencoes->quilometragem;?></p>
<p>
    <strong>Veículos</strong>:
    <?php
    echo $this->Html->getLink($Frota_manutencoes->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
        array('id' => $Frota_manutencoes->getFrota_veiculos()->id), // variaveis via GET opcionais
        array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Cadastrado por</strong>:
    <?php
    echo $this->Html->getLink($Frota_manutencoes->getUsuario()->login, 'Usuario', 'view',
        array('id' => $Frota_manutencoes->getUsuario()->id), // variaveis via GET opcionais
        array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Fornecedor</strong>:
    <?php
    echo $this->Html->getLink($Frota_manutencoes->getFornecedor()->nome, 'Fornecedor', 'view',
        array('id' => $Frota_manutencoes->getFornecedor()->id), // variaveis via GET opcionais
        array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Tipo de Manutenção</strong>:
    <?php
    echo $this->Html->getLink($Frota_manutencoes->getFrota_tipo_manutencao()->descricao, 'Frota_tipo_manutencao', 'view',
        array('id' => $Frota_manutencoes->getFrota_tipo_manutencao()->id), // variaveis via GET opcionais
        array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>