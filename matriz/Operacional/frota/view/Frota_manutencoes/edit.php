
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Manutenções</h2>
        <ol class="breadcrumb">
            <li>Manutenções</li>
            <li class="active">
                <strong>Editar Manutenção</strong>
            </li></ol></div></div>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_manutencoes', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                                <div class="form-group">
                                        <label for="veiculo_id">Veículos</label>
                                        <select name="veiculo_id" class="form-control" id="veiculo_id">
                                            <?php
                                            foreach ($Frota_veiculos as $f) {
                                                if ($f->id == $Frota_manutencoes->veiculo_id)
                                                    echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                                else
                                                    echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                            }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                        <label for="tipo_manutencao_id">Item</label>
                                        <select name="tipo_manutencao_id" class="form-control" id="tipo_manutencao_id">
                                            <?php
                                            foreach ($Frota_tipo_manutencaos as $f) {
                                                if ($f->id == $Frota_manutencoes->tipo_manutencao_id)
                                                    echo '<option selected value="' . $f->id . '">' . $f->descricao . '</option>';
                                                else
                                                    echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';
                                            }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                        <label for="situacao_manutencao_id">Situação </label><span
                                            class="small glyphicon glyphicon-asterisk"></span>
                                        <select name="situacao_manutencao_id" class="form-control" id="situacao_manutencao_id">
                                        <?php
                                                foreach ($Frota_situacao_manutencao as $f) {
                                                    if ($f->id == $Frota_manutencoes->situacao_manutencao_id)
                                                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                                    else
                                                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                                }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group">
                                        <label for="fornecedor_id">Fornecedor</label>
                                        <select name="fornecedor_id" class="form-control" id="fornecedor_id">
                                            <?php
                                            foreach ($Fornecedores as $f) {
                                                if ($f->id == $Frota_manutencoes->fornecedor_id)
                                                    echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                                else
                                                    echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                            }
                                            ?>
                                        </select>
                                </div>
                                <div class="form-group" id="quantidade" name="quantidade">
                                    <label for="quantidade">Quantidade </label>
                                    <input type="text" name="quantidade" id="qtd"
                                                   class="form-control" value="<?php echo $Frota_manutencoes->quantidade ?>">
                                </div>
                                <div class="form-group" id="vl_unitario" name="vl_unitario">
                                <label for="vl_unitario">Valor unitario </label>
                                <input type="number"  name="vl_unitario" id="vlUnitario"
                                       class="form-control" value="<?php echo $Frota_manutencoes->vl_unitario ?>"  onblur="calcular()">
                                </div>                              
                                <div class="form-group">
                                    <label for="valor">Valor</label>
                                    <input type="number" step="any" name="valor" id="valor" class="form-control" value="<?php echo $Frota_manutencoes->valor ?>" placeholder="Valor">
                                </div>
                                <div class="form-group" id="num_nf" name="num_nf">
                                        <label for="num_nf" class="required">Fota fiscal </label><spanclass="glyphicon glyphicon-asterisk"></span>
                                        <input type="number" step="any" name="num_nf" id="num_nf"
                                               class="form-control" value="<?php echo $Frota_manutencoes->num_nf ?>">
                                </div>
                                <div class="form-group">
                                    <label for="dt_manutencao">Data da Manutenção</label>
                                    <input type="date" name="dt_manutencao" id="dt_manutencao" class="form-control" value="<?php echo $Frota_manutencoes->dt_manutencao ?>" placeholder="Data da Manutenção">
                                </div>
                                <div class="form-group">
                                    <label for="garantia">Garantia</label>
                                    <input type="date" name="garantia" id="garantia" class="form-control" value="<?php echo $Frota_manutencoes->garantia ?>" placeholder="Garantia">
                                </div>
                                

                            <div class="form-group">
                                <label for="quilometragem">Quilometragem</label>
                                <input type="number" step="any" name="quilometragem" id="quilometragem" class="form-control" value="<?php echo $Frota_manutencoes->quilometragem ?>" placeholder="Quilometragem">
                            </div>
                            <div class="form-group">
                                <label for="vida_util">Quilometragem(Vida Util)</label>
                                <input type="number" step="any" name="vida_util" id="vida_util" class="form-control" value="<?php echo $Frota_manutencoes->vida_util ?>" placeholder="Data da Manutenção">
                            </div>      
                            <div class="form-group">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao" class="form-control"><?php echo $Frota_manutencoes->observacao ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Frota_manutencoes->id;?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_manutencoes', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--mostra para o usuario a resultado dentro de val-->
<script>
function calcular() {
  var qtd = parseInt(document.getElementById('qtd').value, 10);
  var vlUnitario = parseInt(document.getElementById('vlUnitario').value, 10);
  document.getElementById('resultado').innerText  = qtd * vlUnitario;
}
</script>