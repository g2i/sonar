<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Manutenções </h2>
                <ol class="breadcrumb">
                    <li>Manutenções </li>
                    <li class="active">
                        <strong>proximas do vencimento</strong>
                    </li>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    Veículo
                                </th>
                                <th>
                                    Manutenção
                                </th>
                                <th>
                                    Dt.Manutenção
                                </th>
                                <th>
                                    Km
                                </th>
                                <th>
                                    Vida Útil
                                </th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php
                                    foreach ($manutencoes as $a) {
                                        echo '<tr>';
                                        echo '<td>' . $a->placa . '</td>';
                                        echo '<td>' . $a->manutencao . '</td>';
                                        echo '<td>' . DataBR($a->dt_manutencao) . '</td>';
                                        echo '<td>' . $a->quilometragem . '</td>';
                                        echo '<td>' . $a->vida_util . '</td>';
                                        echo '</tr>';
                                    };                                    
                                ?>
                        </table>
                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php echo $nav; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>