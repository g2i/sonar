<div class="row wrapper border-bottom white-bg page-heading">
    <?php if($seguranca != 0):  ?>
    <div class="col-lg-9">
        <h2>Segurança</h2>
        <ol class="breadcrumb">
            <li>Segurança</li>
            <li class="active">
                <strong>Adicionar </strong>
            </li>
        </ol>
    </div>
<?php else:  ?>
<div class="col-lg-9">
        <h2>Manutenções</h2>
        <ol class="breadcrumb">
            <li>Manutenções</li>
            <li class="active">
                <strong>Adicionar </strong>
            </li>
        </ol>
    </div>
 <?php endif; ?>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_manutencoes', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">                           
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="veiculo_id">Veículo</label><span
                                    class="small glyphicon glyphicon-asterisk"></span>
                                    <select name="veiculo_id" class="form-control selectPicker" id="veiculo_id">
                                            <option></option>
                                        <?php
                                            foreach ($Frota_veiculos as $f) {
                                                if ($f->id == $Frota_manutencoes->veiculo_id)
                                                    echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                                else
                                                    echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipo_manutencao_id">Item</label><span
                                    class="glyphicon glyphicon-asterisk"></span><span>                                   
                                        <button type="button" id="btnAddFornecedor" class="btn btn-info btn-xs">Novo</button></span>      
                                <select name="tipo_manutencao_id" class="form-control selectPicker" id="tipo_manutencao_id">
                                    <?php
                                    foreach ($Frota_tipo_manutencaos as $f) {
                                        if ($f->id == $Frota_manutencoes->tipo_manutencao_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->descricao . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="situacao_manutencao_id">Situação </label><span
                                    class="small glyphicon glyphicon-asterisk"></span>
                                <select name="situacao_manutencao_id" class="form-control selectPicker" id="situacao_manutencao_id">
                                    <?php  
                                    foreach ($Frota_situacao_manutencao as $f) {
                                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="fornecedor_id">Fornecedor</label>
                                    <select name="fornecedor_id" class="form-control selectPicker" id="fornecedor_id">
                                        <option></option>
                                        <?php
                                        foreach ($Fornecedores as $f) {
                                            if ($f->id == $Frota_manutencoes->fornecedor_id)
                                                echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                            else
                                                echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12" id="quantidade" name="quantidade">
                                <label for="quantidade" class="required">Quantidade </label><span class="glyphicon glyphicon-asterisk"></span>
                                <input type="input" name="quantidade" id="qtd"
                                       class="form-control valor" value="<?php echo $Frota_manutencoes->quantidade; ?>">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12" id="vl_unitario" name="vl_unitario">
                                <label for="vl_unitario" class="required">Valor unitario </label><span class="glyphicon glyphicon-asterisk"></span>
                                <input type="input"  name="vl_unitario" id="vlUnitario" 
                                class="form-control valor" value="<?php echo $Frota_manutencoes->vl_unitario; ?>" onblur="calcular()">
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="valor" class="required">Total Valor</label><br>
                               <label for=""></label>R$ <span id="resultado"></span>                            
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12" id="num_nf" name="num_nf">
                                    <label for="num_nf" class="required">Fota fiscal </label><spanclass="glyphicon glyphicon-asterisk"></span>
                                    <input type="number" step="any" name="num_nf" id="num_nf"
                                           class="form-control" value="<?php echo $Frota_manutencoes->num_nf ?>">
                                </div>
                    
                            
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="dt_manutencao" class="required">Data da Manutenção</label><span
                                    class="small glyphicon glyphicon-asterisk"></span>
                                <input type="date" name="dt_manutencao" id="dt_manutencao" class="form-control"
                                       value="<?php echo $Frota_manutencoes->dt_manutencao ?>" required
                                       placeholder="Data da Manutenção">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="garantia" class="required">Garantia</label><span
                                    class="glyphicon glyphicon-asterisk"></span>
                                <input type="date" name="garantia" id="garantia" class="form-control"
                                       value="<?php echo $Frota_manutencoes->garantia ?>" required
                                       placeholder="Garantia">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12" id="quilometragem" name="quilometragem">
                                <label for="quilometragem" class="required">Quilometragem </label><span
                                    class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="quilometragem" id="quilometragem_manutencao"
                                       class="form-control" value="<?php echo $Frota_manutencoes->quilometragem ?>"
                                       required placeholder="Quilometragem">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12" id="vida_util" name="vida_util">
                                <label for="vida_util" class="required">Quilometragem (Vida Util) </label><span
                                    class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="vida_util" id="vida_util"
                                       class="form-control" value="<?php echo $Frota_manutencoes->vida_util ?>"
                                       required placeholder="Vida Util (KM's)">
                            </div>
                            

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="km_ultimo_abastecimento">Maior quilometragem(abastecimentos): </label>
                                <input type="number" step="any" name="km_ultimo_abastecimento"
                                       id="km_ultimo_abastecimento" class="form-control"
                                       required placeholder="000000" readonly/>
                            </div>

                           

                            

                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao"
                                          class="form-control"><?php echo $Frota_manutencoes->observacao ?></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_manutencoes', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--mostra para o usuario a resultado dentro de val-->
<script>
function calcular() {
  var qtd = parseInt(document.getElementById('qtd').value, 10);
  var vlUnitario = parseInt(document.getElementById('vlUnitario').value, 10);
  document.getElementById('resultado').innerText  = qtd * vlUnitario;
}
$(document).ready(function () {
        $('#btnAddFornecedor').click(function(){
            var url = root + '/Frota_tipo_manutencao/all/ajax:true/modal:1/';
            console.log(url);
            LoadGif();
            $('<div></div>').load(url, function(){
                BootstrapDialog.show({
                    title: 'Adicionar Item',
                    message: this,
                    size: 'size-wide',
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function(dialog){                       
                        CloseGif();
                    }
                });
            });
        });
    });
</script>
