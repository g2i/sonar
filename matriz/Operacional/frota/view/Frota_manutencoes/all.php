<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Manutenções</h2>
        <ol class="breadcrumb">
            <li>Manutenções</li>
            <li class="active">
                <strong>Lista</strong>
            </li></ol></div></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                    <div class="col-md-3 form-group">
                                        <label for="inicio">Inicio</label>
                                        <div class='input-group dateInicio'>
                                            <input type='date' class="form-control dateFormat" name="inicio" id="inicio"
                                                        value="<?php echo $inicio; ?>"/><span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div>  
                                            <div class="col-md-3 form-group">
                                                <label for="fim">Fim</label>
                                                <div class='input-group dateFim'>
                                                    <input type='date' class="form-control dateFormat" name="fim" id="fim"
                                                        value="<?php echo $fim; ?>"/>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                            </div> 
                                    <div class="col-md-3 form-group">
                                    <label for="veiculo_id">Veículo</label>
                                    <select name="veiculo_id" class="form-control selectPicker" id="veiculo_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Frota_veiculos as $f): ?>   
                                            <?php echo '<option value="' . $f->id . '">' . $f->placa . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                
                                <div class="col-md-3 form-group">
                                    <label for="tipo_manutencao_id">Tipo de Manutenção</label>
                                    <select name="tipo_manutencao_id" class="form-control selectPicker" id="tipo_manutencao_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Frota_tipo_manutencaos as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="situacao_manutencao_id">Situação Manutenção</label>
                                    <select name="situacao_manutencao_id" class="form-control selectPicker" id="situacao_manutencao_id">
                                        <?php echo '<option value=""></option>';  ?>
                                        <?php foreach ($Frota_situacao_manutencao as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="localizacao_id">Localização</label>
                                    <select name="localizacao_id" class="form-control selectPicker"
                                            id="localizacao_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_localizacao as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                            class="btn btn-default"
                                            data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                            title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="button" class="btn btn-default" id="buscar-filtro" data-tool="tooltip" data-placement="bottom" title="Pesquisar"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>                               
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Manutenção', 'Frota_manutencoes', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                            <div class="col-md-3 form-group">
                                        <?php 
                                        $total = null;
                                        foreach($Frota_manutencoes as $f){
                                          $total += $f->valor;
                                        }
                                        echo '<h4>Total: R$' . sprintf('%0.2f', $total); echo '</h4>';
                                        ?>
                                       
                                    </select>
                                </div>
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'veiculo_id')); ?>'>
                                            Veículo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'tipo_manutencao_id')); ?>'>
                                            Item
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'tipo_manutencao_id')); ?>'>
                                            Situação
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'fornecedor_id')); ?>'>
                                            Fornecedor
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'valor')); ?>'>
                                            Valor
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'garantia')); ?>'>
                                            Garantia
                                        </a>
                                    </th>  
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'quilometragem')); ?>'>
                                            Quilometragem
                                        </a>
                                    </th>


                                    
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'dt_manutencao')); ?>'>
                                            Data Manutenção
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'observacao')); ?>'>
                                            Observação
                                        </a>
                                    </th>
                                  
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                            Cadastrado por
                                        </a>
                                    </th>  
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_manutencoes as $f) {

//                                    if(($f->getFrota_veiculos()->quilometragem_atual - $f->quilometragem) >= $f->vida_util || ($f->getFrota_veiculos()->quilometragem_atual - $f->quilometragem) >= ($f->vida_util - 200)) {
//                                        echo '<tr bgcolor="#FFCDD2">';
//                                    } else {
//                                        echo '<tr>';
//                                    }
                                if(!empty(Frota_manutencoes::getVeiculosManutencoes($f->getFrota_veiculos()->id))) {
                                        echo '<tr bgcolor="#FFCDD2">';
                                    } else {
                                        echo '<tr>';
                                    }                                    
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
                                        array('id' => $f->getFrota_veiculos()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_tipo_manutencao()->descricao, 'Frota_tipo_manutencao', 'view',
                                        array('id' => $f->getFrota_tipo_manutencao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getSituacaoManutencao()->nome, 'Frota_manutencoes', 'view',
                                        array('id' => $f->getFrota_tipo_manutencao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFornecedor()->nome, 'Fornecedor', 'view',
                                        array('id' => $f->getFornecedor()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink('R$ ' . sprintf('%0.2f', $f->valor), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($f->garantia), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';  
                                    echo '<td>';
                                    echo $this->Html->getLink($f->quilometragem, 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';


                                    
                                    
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($f->dt_manutencao), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->observacao, 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                  
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getUsuario()->login, 'Usuario', 'view',
                                        array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_manutencoes', 'edit',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_manutencoes', 'delete',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
//function para o btn de pesquisa em form 
$('.filtros').find('#buscar-filtro').click(function(){
        let form_action = $(this).closest('form').attr('action');
        let form_serialize = $(this).closest('form').serialize();
        let url_completa = form_action + '?' + form_serialize;
        console.log(url_completa);
        carregaTabelaResponsiva(url_completa);
    });
    
//function para paginar por ajax
    $(document).on('click','.pagination a, .table thead a', function(e){
        e.preventDefault();
        let url = $(this).attr('href');

        if(url != "")
        {            
            carregaTabelaResponsiva(url);
        }
        return false;
    });
   //function para pegar as URLs 
function carregaTabelaResponsiva(url) {
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: () => {
        },
        success: (data) => {
            let conteudo = $('<div>').append(data).find('.table-responsive');
            $(".table-responsive").html(conteudo);
        },
        complete: () => {
        },
        error: () => {
        },
    });
}
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>