<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Motoristas</h2>
        <ol class="breadcrumb">
            <li>Motoristas</li>
            <li class="active">
                <strong>Motoristas com CNH Vencida</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <!-- formulario de pesquisa -->

                <div class="filtros well">
                    <div class="form">

                        <form role="form"
                              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                              method="post" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                            <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                            <h4 style="padding-top: 5px;">Período de Vencimento da CNH:</h4>

                            <div class="col-md-3 form-group">
                                <label for="data_inicial">Inicio</label>
                                <input type="date" name="inicio" id="inicio"
                                       class="form-control maskData" value="">
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="data_final">Fim</label>
                                <input type="date" name="fim" id="fim"
                                       class="form-control maskData" value="">
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="nome">Motorista</label>
                                <input type="text" name="filtro[interno][nome]" id="nome"
                                       class="form-control" value="" placeholder="Motorista">
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="veiculo_id">Veículo</label>
                                <select name="filtro[externo][veiculo_id]" class="form-control selectPicker"
                                        id="veiculo_id">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($veiculos as $v): ?>
                                        <?php echo '<option value="' . $v->id . '">' . $v->placa . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>


                            <div class="col-md-12 text-right">
                                <button type="button" class="btn btn-default"><span
                                            class="glyphicon glyphicon-search" id="btn-filtro"></span></button>
                                <button type="button" id="btnReset" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                            </div>
                            <div class="clearfix"></div>
                        </form>


                    </div>
                </div>

                <!-- tabela de resultados -->
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th bgcolor="#ccc">
                                    Motorista
                                </th>
                                <th bgcolor="#ccc">
                                    Vencimento CNH
                                </th>

                                <th bgcolor="#ccc">
                                    Veiculo Atual
                                </th>
                                <th bgcolor="#ccc">
                                    Localização
                                </th>
                            </tr>

                            <?php
                            $flag = true;
                            foreach ($motoristas as $m) {
                                echo '<tr>';

                                echo '<td><strong>' . $m->nome . '</strong></td>';
                                echo '</td>';

                                echo '<td><strong>' . DataBR($m->validade_cnh) . '</strong></td>';
                                echo '</td>';


                                if (!empty($m->getFrota_veiculos()->placa)) {
                                    echo '<td><strong>' . $m->getFrota_veiculos()->placa . '</strong></td>';
                                } else {
                                    echo '<td><strong> Não designado </strong></td>';
                                }
                                echo '</td>';


                                if (!empty($m->getFrota_veiculos()->getFrota_localizacao()->cidade)) {
                                    echo '<td><strong>' . $m->getFrota_veiculos()->getFrota_localizacao()->cidade . '</strong></td>';
                                } else {
                                    echo '<td><strong> Veículo não designado </strong></td>';
                                }
                                echo '</td>';

                                echo '</tr>';
                            }
                            ?>

                        </table>

                        <!-- menu de paginação -->
                        <div style="text-align:center"><?php echo $nav; ?></div>

                    </div>
                </div>


                <script>
                    /* faz a pesquisa com ajax */
                    $(document).ready(function () {
                        $('#search').keyup(function () {
                            var r = true;
                            if (r) {
                                r = false;
                                $("div.table-responsive").load(
                                    <?php
                                    if (isset($_GET['orderBy']))
                                        echo '"' . $this->Html->getUrl('Frota_motoristas', 'cnhs_vencidas', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    else
                                        echo '"' . $this->Html->getUrl('Frota_motoristas', 'cnhs_vencidas') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    ?>
                                    , function () {
                                        r = true;
                                    });
                            }
                        });
                    });
                </script>

            </div>
        </div>
    </div>
</div>

<script>

    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>