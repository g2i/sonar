<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Motoristas com CNH </h2>
                <ol class="breadcrumb">
                    <li>Motoristas com CNH </li>
                    <li class="active">
                        <strong>á vencer nos proximos 60 dias</strong>
                    </li>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    Motorista
                                </th>
                                <th>
                                    Vencimento CNH
                                </th>
                                <th>
                                    Veículo atual
                                </th>
                                <th>
                                    Dias restantes
                                </th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php
                                   foreach ($motoristas as $m) {
                                    echo '<tr>';
                                    echo '<td>' . $m->nome . '</td>';
                                    echo '<td>' . DataBR($m->validade_cnh) . '</td>';
                                    if(!empty($m->placa)) {
                                        echo '<td>' . $m->placa . '</td>';
                                    } else {
                                        echo '<td> Não designado </td>';
                                    }
                                    echo '<td style="text-align: center">' . $m->dias . '</td>';
                                    echo '</tr>';
                                };                                 
                                ?>
                        </table>
                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php echo $nav; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>