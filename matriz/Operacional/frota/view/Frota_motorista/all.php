<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Motorista</h2>
        <ol class="breadcrumb">
            <li>Motorista</li>
            <li class="active">
                <strong>Todos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                           value="<?php echo $this->getParam('nome'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="validade_cnh">Validade CNH</label>
                                    <input type="date" name="filtro[interno][validade_cnh]" id="validade_cnh"
                                           class="form-control maskData"
                                           value="<?php echo $this->getParam('validade_cnh'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="veiculo_id">Veículo</label>
                                    <select name="filtro[externo][veiculo_id]" class="form-control" id="veiculo_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_veiculos as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->placa . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" id="btnReset" class="btn btn-default botao-reset"><span
                                                class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span
                                                class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Motorista',
                         'Frota_motorista', 'add', NULL, array('class' => 'btn btn-primary', 'data-toggle' => 'modal')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'id')); ?>'>
                                            Codigo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'nome')); ?>'>
                                            Nome
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'validade_cnh')); ?>'>
                                            Validade CNH
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'situacao_habilitacao_id')); ?>'>
                                            Situação Habilitação
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'qtd_pontos')); ?>'>
                                            Pontos
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'dt_atualizacao')); ?>'>
                                            Última Att. CNH
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'localizacao_id')); ?>'>
                                            Base
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => 'veiculo_id')); ?>'>
                                            Veículo
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_motoristas as $f) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->id, 'Frota_motorista', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->nome, 'Frota_motorista', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(DataBR($f->validade_cnh), 'Frota_motorista', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->getFrota_situacao_habilitacao()->nome;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->qtd_pontos;
                                    echo '</td>';
                                    echo '<td>';
                                    echo !empty($f->dt_atualizacao) ? DataBR($f->dt_atualizacao) : '';
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->getFrota_localizacao()->cidade;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
                                        array('id' => $f->getFrota_veiculos()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_motorista', 'edit',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_motorista', 'delete',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Frota_motorista', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Frota_motorista', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>