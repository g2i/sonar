<!--<div class="row wrapper border-bottom white-bg page-heading">-->
<!--    <div class="col-lg-9">-->
<!--        <h2>Motorista</h2>-->
<!--        <ol class="breadcrumb">-->
<!--            <li>Motorista</li>-->
<!--            <li class="active">-->
<!--                <strong>Adicionar Motorista</strong>-->
<!--            </li>-->
<!--        </ol>-->
<!--    </div>-->
<!--</div>-->
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Motorista</h2>
                <ol class="breadcrumb">
                    <li>Motorista</li>
                    <li class="active">
                        <strong>Adicionar Motorista</strong>
                    </li>
                    <br><br>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_motorista', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="motorista_id">Nome</label>
                                <select name="rhprofissional_id" class="form-control select2" id="motorista_id" required>
                                    <?php
                                    echo '<option value="" selected>Selecione um motorista</option>';
                                    foreach ($Rhprof as $f) {
                                        echo '<option value="' . $f->codigo . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="validade_cnh">Validade CNH</label>
                                <input type="date" name="validade_cnh" id="validade_cnh" class="form-control"
                                       value="<?php echo $Frota_motorista->validade_cnh ?>" placeholder="Validade_cnh">
                            </div>

                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="localizacao_id">Base</label>
                                <select name="localizacao_id" class="form-control select2" id="localizacao_id">
                                    <?php
                                    foreach ($Frota_localizacao as $f) {
                                        echo '<option value="' . $f->id . '">' . $f->cidade . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="situacao_habilitacao_id">Situação Habilitação</label>
                                <select name="situacao_habilitacao_id" class="form-control"
                                        id="situacao_habilitacao_id">
                                    <?php
                                    foreach ($Frota_situacao_habilitacao as $f) {
                                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="qtd_pontos">Qtd. Pontos</label>
                                <input type="number" name="qtd_pontos" id="qtd_pontos" class="form-control"
                                       value="<?php echo $Frota_motorista->qtd_pontos ?>"
                                       placeholder="Quantidade de Pontos">
                            </div>
                            <input type="hidden" name="dt_atualizacao" id="dt_atualizacao" class="form-control"
                                   value="<?php echo date('Y-m-d') ?>" placeholder="Validade_cnh" readonly>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_motorista', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.select2').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
        });
    });
</script>