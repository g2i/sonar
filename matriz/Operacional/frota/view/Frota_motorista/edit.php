
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Motorista</h2>
                <ol class="breadcrumb">
                    <li>Motorista</li>
                    <li class="active">
                        <strong>Editar Motorista</strong>
                    </li>
                    <br><br>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_motorista', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                        <div class="form-group">
                                <label for="motorista_id">Nome</label>
                                <select name="rhprofissional_id" class="form-control select2" id="motorista_id" required>
                                    <?php
                                    echo '<option value="">Selecione um motorista</option>';
                                    foreach ($Rhprof as $f) {
                                        if ($f->codigo == $Frota_motorista->rhprofissional_id)
                                            echo '<option selected value="' . $f->codigo . '">' . $f->nome . '</option>';
                                        else
                                            echo '<option value="' . $f->codigo . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="validade_cnh">Valiade CNH</label>
                                <input type="date" name="validade_cnh" id="validade_cnh" class="form-control"
                                       value="<?php echo $Frota_motorista->validade_cnh ?>" placeholder="Validade_cnh">
                            </div>
                            <div class="form-group">
                                <label for="localizacao_id">Base</label>
                                <select name="localizacao_id" class="form-control select2" id="localizacao_id">
                                    <?php
                                    foreach ($Frota_localizacao as $f) {
                                        if ($f->id == $Frota_motorista->localizacao_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->cidade . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->cidade . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="situacao_habilitacao_id">Situação Habilitação</label>
                                <select name="situacao_habilitacao_id" class="form-control"
                                        id="situacao_habilitacao_id">
                                    <?php
                                    foreach ($Frota_situacao_habilitacao as $f) {
                                        if ($f->id == $Frota_motorista->situacao_habilitacao_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="qtd_pontos">Qtd. Pontos</label>
                                <input type="number" name="qtd_pontos" id="qtd_pontos" class="form-control"
                                       value="<?php echo $Frota_motorista->qtd_pontos ?>" placeholder="Quantidade de Pontos">
                            </div>
                            <div class="form-group">
                                <label for="dt_atualizacao">Dta. Atualização</label>
                                <input type="date" name="dt_atualizacao" id="dt_atualizacao" class="form-control"
                                       value="<?php echo $Frota_motorista->dt_atualizacao ?>" placeholder="Validade_cnh" readonly>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Frota_motorista->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_motorista', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('.select2').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
        });

        <?php
        if(empty($Frota_motorista->motorista_id)) {
            echo 'BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: "Atenção",
                    message: "Por favor! Atualize os dados do motorista.",
                    buttons: [{
                        label: "Fechar",
                        action: function(dialogItself){
                            dialogItself.close();
                        }
                    }]
            })';
        }

        ?>




    });
</script>