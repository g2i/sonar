<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-9">
            <h2>Perguntas/Respostas</h2>
            <ol class="breadcrumb">
                <li>Perguntas/Respostas</li>
                <li class="active">
                    <strong>Lista</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- tabela de resultados -->
                    <div class="wrapper wrapper-content animated">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='#'>
                                            id
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Pergunta
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Respostas
                                        </a>
                                    </th>                                   
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_inspecao_check_pergunta_respostas as $f) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->id, 'Frota_inspecao_check_pergunta_resposta', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_inspecao_pergunta()->nome, 'Frota_inspecao_pergunta', 'view',
                                        array('id' => $f->getFrota_inspecao_pergunta()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';?>
                                        <a href="#" class="edit_local" data-controller="Frota_inspecao_check_pergunta_resposta" data-type="select"
                                        data-name="frota_inspecao_resposta_id" data-pk="<?php echo $f->id  ?>"
                                        data-value="<?php echo $f->frota_inspecao_resposta_id  ?>" 
                                        data-source="<?php echo $frota_respostaDataSource ?>">
                                        <?php echo $f->getFrota_inspecao_resposta()->nome; ?>
                                        </a> 
                                   <?php echo '</td>';
                                   
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>
                    <script>
                            /* faz a pesquisa com ajax */
                            $(document).ready(function() {
                                $(document).find(".edit_local").each(function () {
                                    input_editable($(this));
                                });
                                $('#search').keyup(function() {
                                    var r = true;
                                    if (r) {
                                        r = false;
                                        $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Frota_inspecao_check_pergunta_resposta', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Frota_inspecao_check_pergunta_resposta', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function() {
                                            r = true;
                                        });
                                    }
                                });
                            });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>