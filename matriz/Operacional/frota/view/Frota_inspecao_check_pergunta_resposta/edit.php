
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Frota_inspecao_check_pergunta_resposta</h2>
    <ol class="breadcrumb">
    <li>Frota_inspecao_check_pergunta_resposta</li>
    <li class="active">
    <strong>Editar Frota_inspecao_check_pergunta_resposta</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_inspecao_check_pergunta_resposta', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="dt_cadastro">Dt_cadastro</label>
            <input type="datetime" name="dt_cadastro" id="dt_cadastro" class="form-control" value="<?php echo $Frota_inspecao_check_pergunta_resposta->dt_cadastro ?>" placeholder="Dt_cadastro">
        </div>
        <div class="form-group">
            <label for="dt_modificado">Dt_modificado</label>
            <input type="datetime" name="dt_modificado" id="dt_modificado" class="form-control" value="<?php echo $Frota_inspecao_check_pergunta_resposta->dt_modificado ?>" placeholder="Dt_modificado">
        </div>
        <div class="form-group">
            <label for="frota_inspecao_pergunta_id">Frota_inspecao_pergunta</label>
            <select name="frota_inspecao_pergunta_id" class="form-control" id="frota_inspecao_pergunta_id">
                <?php
                foreach ($Frota_inspecao_perguntas as $f) {
                    if ($f->id == $Frota_inspecao_check_pergunta_resposta->frota_inspecao_pergunta_id)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="frota_inspecao_resposta_id">Frota_inspecao_resposta</label>
            <select name="frota_inspecao_resposta_id" class="form-control" id="frota_inspecao_resposta_id">
                <?php
                foreach ($Frota_inspecao_respostas as $f) {
                    if ($f->id == $Frota_inspecao_check_pergunta_resposta->frota_inspecao_resposta_id)
                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="frota_inspecao_id">Frota_inspecao</label>
            <select name="frota_inspecao_id" class="form-control" id="frota_inspecao_id">
                <?php
                foreach ($Frota_inspecaos as $f) {
                    if ($f->id == $Frota_inspecao_check_pergunta_resposta->frota_inspecao_id)
                        echo '<option selected value="' . $f->id . '">' . $f->equipe . '</option>';
                    else
                        echo '<option value="' . $f->id . '">' . $f->equipe . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Frota_inspecao_check_pergunta_resposta->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="cadastrado_por">Usuario</label>
            <select name="cadastrado_por" class="form-control" id="cadastrado_por">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Frota_inspecao_check_pergunta_resposta->cadastrado_por)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="modificado_por">Usuario</label>
            <select name="modificado_por" class="form-control" id="modificado_por">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Frota_inspecao_check_pergunta_resposta->modificado_por)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Frota_inspecao_check_pergunta_resposta->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Frota_inspecao_check_pergunta_resposta', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>