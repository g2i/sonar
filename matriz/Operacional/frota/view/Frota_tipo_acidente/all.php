
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Tipos de Acidentes</h2>
    <ol class="breadcrumb">
    <li>Tipos de Acidentes</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Frota_tipo_acidente', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'all', array('orderBy' => 'descricao')); ?>'>
                        Descrição
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'all', array('orderBy' => 'cadastradopor')); ?>'>
                        Cadastrado por
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                        Dt.Cadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'all', array('orderBy' => 'modificadopor')); ?>'>
                        Modificado por
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'all', array('orderBy' => 'dt_atualizacao')); ?>'>
                        Dt.Atualizacao
                    </a>
                </th>            
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Frota_tipo_acidentes as $f) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($f->id, 'Frota_tipo_acidente', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($f->descricao, 'Frota_tipo_acidente', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $f->getUsuario()->nome;
                  
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink(DataTimeBr($f->dt_cadastro), 'Frota_tipo_acidente', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $f->getUsuario2()->nome;
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink(DataTimeBr($f->dt_atualizacao), 'Frota_tipo_acidente', 'view',
                    array('id' => $f->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';           
                         
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_tipo_acidente', 'edit', 
                    array('id' => $f->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_tipo_acidente', 'delete', 
                    array('id' => $f->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Frota_tipo_acidente', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Frota_tipo_acidente', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>