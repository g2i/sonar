
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Tipos de Acidentes</h2>
    <ol class="breadcrumb">
    <li>Tipos de Acidentes</li>
    <li class="active">
    <strong>Editar </strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="descricao">Descricao</label>
            <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Frota_tipo_acidente->descricao ?>" placeholder="Descricao">
        </div>
        <div class="form-group">
            <label class="required" for="dt_cadastro">Dt_cadastro <span class="glyphicon glyphicon-asterisk"></span></label>
        </div>
        <div class="form-group">
            <label class="required" for="dt_atualizacao">Dt_atualizacao <span class="glyphicon glyphicon-asterisk"></span></label>
        </div>
        <div class="form-group">
            <label for="cadastradopor">Usuario</label>
            <select name="cadastradopor" class="form-control" id="cadastradopor">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Frota_tipo_acidente->cadastradopor)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="modificadopor">Usuario</label>
            <select name="modificadopor" class="form-control" id="modificadopor">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Frota_tipo_acidente->modificadopor)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Frota_tipo_acidente->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Frota_tipo_acidente->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Frota_tipo_acidente', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>