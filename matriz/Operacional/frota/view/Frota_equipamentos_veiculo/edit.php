
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Equipamento Auxiliar</h2>
        <ol class="breadcrumb">
            <li>Equipamento Auxiliar</li>
            <li class="active">
                <strong>Editar Equipamento</strong>
            </li></ol></div></div>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_equipamentos_veiculo', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Frota_equipamentos_veiculo->nome ?>" placeholder="Nome">
                            </div>
                            <div class="form-group">
                                <label for="modelo">Modelo</label>
                                <input type="text" name="modelo" id="modelo" class="form-control" value="<?php echo $Frota_equipamentos_veiculo->modelo ?>" placeholder="Modelo">
                            </div>
                            <div class="form-group">
                                <label for="capacidade">Capacidade</label>
                                <input type="number" name="capacidade" id="capacidade" class="form-control" value="<?php echo $Frota_equipamentos_veiculo->capacidade ?>" placeholder="Capacidade">
                            </div>
                            <div class="form-group">
                                <label for="validade_laudo_mecanico">Validade Laudo Mecânico</label>
                                <input type="date" name="validade_laudo_mecanico" id="validade_laudo_mecanico" class="form-control" value="<?php echo $Frota_equipamentos_veiculo->validade_laudo_mecanico ?>" placeholder="Validade Laudo Mecânico">
                            </div>
                            <div class="form-group">
                                <label for="veiculo_id">Veículo</label>
                                <select name="veiculo_id" class="form-control" id="veiculo_id">
                                    <?php
                                    foreach ($Frota_veiculos as $f) {
                                        if ($f->id == $Frota_equipamentos_veiculo->veiculo_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Frota_equipamentos_veiculo->id;?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_equipamentos_veiculo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>