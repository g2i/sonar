<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Equipamento Auxiliar </h2>
                <ol class="breadcrumb">
                    <li>Equipamento Auxiliar </li>
                    <li class="active">
                        <strong>á vencer nos proximos 45 dias</strong>
                    </li>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    Tipo Equipamento Auxiliar 
                                </th>
                                <th>
                                    Placa Veículo
                                </th>
                                <th>
                                    Validade Laudo Mecânico
                                </th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            <?php
                                   foreach ($equipamentos as $m) {
                                    echo '<tr>';
                                    echo '<td>' . $m->nome . '</td>';
                                    echo '<td>' . $m->placa . '</td>';
                                    echo '<td style="text-align: center">' . DataBR($m->validade_laudo_mecanico) . '</td>';                                
                                   
                                    echo '</tr>';
                                };                                 
                                ?>
                        </table>
                        <!-- menu de paginação -->
                        <div style="text-align:center">
                            <?php echo $nav; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>