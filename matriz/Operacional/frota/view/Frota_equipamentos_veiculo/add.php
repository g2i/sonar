<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Equipamento Auxiliar</h2>
        <ol class="breadcrumb">
            <li>Equipamento Auxiliar</li>
            <li class="active">
                <strong>Adicionar Equipamento</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_equipamentos_veiculo', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                           <!-- <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipo">Tipo</label> <span
                                    class="glyphicon glyphicon-asterisk"></span>
                                <select id="tipo" name="tipo" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="munck">Munck</option>
                                    <option value="radio">Radio Amador</option>
                                </select>
                            </div>-->
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="tipo_equipamento_id">Tipo Equipamento</label>
                                <select name="tipo_equipamento_id" class="form-control selectPicker" id="tipo_equipamento_id">
                                    <option></option>
                                    <?php
                                    foreach ($Frota_tipo_equipamentos as $r) {
                                        if ($r->id == $Frota_equipamentos_veiculo->tipo_equipamento_id)
                                            echo '<option selected value="' . $r->id . '">' . $r->nome . '</option>';
                                        else
                                            echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome" class="required">Nome</label> <span
                                    class="glyphicon glyphicon-asterisk"></span>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Frota_equipamentos_veiculo->nome ?>" required placeholder="Nome">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="veiculo_id">Veículo</label> <span
                                    class="glyphicon glyphicon-asterisk"></span>
                                <select name="veiculo_id" class="form-control selectPicker" id="veiculo_id">
                                    <option></option>
                                    <?php
                                    foreach ($Frota_veiculos as $f) {
                                        if(isset($_GET['id'])) {
                                            $veiculo = new Frota_veiculos((int)$_GET['id']);
                                            echo '<option selected value="' . $veiculo->id . '">' . $veiculo->placa . '</option>';
                                        } else if ($f->id == $Frota_equipamentos_veiculo->veiculo_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="modelo">Modelo</label>
                                <input type="text" name="modelo" id="modelo" class="form-control"
                                       value="<?php echo $Frota_equipamentos_veiculo->modelo ?>" placeholder="Modelo">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="validade_laudo_mecânico">Validade Laudo Mecânico</label>
                                    <input type="date" name="validade_laudo_mecanico" id="validade_laudo_mecanico"
                                           class="form-control"
                                           value="<?php echo $Frota_equipamentos_veiculo->validade_laudo_mecanico ?>"
                                           placeholder="Validade_laudo_mecânico">
                            </div>
                            <div id="munck" name="munck" style="display:none;">
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <label for="capacidade">Capacidade (Kgf)</label> <span
                                        class="glyphicon glyphicon-asterisk"></span>
                                    <input type="number" name="capacidade" id="capacidade" class="form-control"
                                           value="<?php echo $Frota_equipamentos_veiculo->capacidade ?>"
                                           placeholder="Capacidade">
                                </div>                                
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_equipamentos_veiculo', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>