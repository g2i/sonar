<?php include_once "dataUtil.php"; ?>
<p><strong>Nome</strong>: <?php echo $Frota_equipamentos_veiculo->nome;?></p>
<p><strong>Modelo</strong>: <?php echo $Frota_equipamentos_veiculo->modelo;?></p>
<p><strong>Capacidade</strong>: <?php echo $Frota_equipamentos_veiculo->capacidade;?></p>
<p><strong>Validade laudo mecânico</strong>: <?php echo convertDataSQL4BR($Frota_equipamentos_veiculo->validade_laudo_mecanico);?></p>
<p><strong>Cadastrado em</strong>: <?php echo convertDataSQL4BR($Frota_equipamentos_veiculo->dt_cadastro);?></p>
<p>
    <strong>Cadastrado por</strong>:
    <?php
    echo $this->Html->getLink($Frota_equipamentos_veiculo->getUsuario()->login, 'Usuario', 'view',
    array('id' => $Frota_equipamentos_veiculo->getUsuario()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Veículos</strong>:
    <?php
    echo $this->Html->getLink($Frota_equipamentos_veiculo->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
    array('id' => $Frota_equipamentos_veiculo->getFrota_veiculos()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>