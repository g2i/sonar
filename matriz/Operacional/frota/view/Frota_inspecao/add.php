<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Inspeção</h2>
        <ol class="breadcrumb">
            <li>Inspeção</li>
            <li class="active">
                <strong>Continiar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_inspecao', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>                            
                        <div class="well well-lg">
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="localidade_id">Localidade</label>
                                <select name="localidade_id" class="form-control" id="localidade_id">
                                    <?php
                                    foreach ($Rhlocalidades as $f) {
                                        if ($f->id == $Frota_inspecao->localidade_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado .'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="equipe">Equipe</label>
                                <input type="text" name="equipe" id="equipe" class="form-control" value="<?php echo $Frota_inspecao->equipe ?>"
                                    placeholder="Equipe">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="responsavel_vistoria">Responsável Inspeção</label>
                                <input type="text" name="responsavel_vistoria" id="responsavel_vistoria" class="form-control"
                                    value="<?php echo $Frota_inspecao->responsavel_vistoria ?>" placeholder="Responsável">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data_inspecao">Data Inspeção</label>
                                <input type="date" name="data_inspecao" id="data_inspecao" class="form-control" value="<?php echo $Frota_inspecao->data_inspecao ?>"
                                    placeholder="Data_inspecao">
                            </div>                                                     
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="veiculo_id">Veículo</label>
                                <select name="veiculo_id" class="form-control" id="veiculo_id">
                                    <?php
                                    foreach ($Frota_veiculos as $f) {
                                        if ($f->id == $Frota_inspecao->veiculo_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="motorista_id">Motorista</label>
                                <select name="motorista_id" class="form-control" id="motorista_id">
                                    <?php
                                    foreach ($Frota_motoristas as $f) {
                                        if ($f->id == $Frota_inspecao->motorista_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>    
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome_carona">Carona</label>
                                <input type="text" name="nome_carona" id="nome_carona" class="form-control" value="<?php echo $Frota_inspecao->nome_carona ?>"
                                    placeholder="Carona">
                            </div>
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao" class="form-control"><?php echo $Frota_inspecao->observacao ?></textarea>
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_inspecao', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="Continuar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>