<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Inspeção</h2>
        <ol class="breadcrumb">
            <li>Inspeção</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div> 
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                         
                                <div class="col-md-3 form-group">
                                    <label for="veiculo_id">Veículo</label>
                                    <select name="filtro[externo][veiculo_id]" class="form-control selectPicker"
                                            id="veiculo_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_veiculos as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->placa . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="motorista_id">Motorista</label>
                                    <select name="filtro[externo][motorista_id]" class="form-control selectPicker"
                                            id="motorista_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_motoristas as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>                              
                                <div class="col-md-12 text-right">
                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                    class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                    title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p>
                            <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Frota_inspecao', 'add', NULL, array('class' => 'btn btn-primary')); ?>
                        </p>
                    </div>
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='#'>
                                            id
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Veículo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Motorista
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Equipe
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Responsavel Inspeção
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Data Inspeção
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Observação
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_inspecaos as $f) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $f->id; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->getFrota_veiculos()->placa; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->getFrota_motorista()->nome; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->equipe; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->responsavel_vistoria; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo DataBR($f->data_inspecao); 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->observacao; 
                                    echo '</td>';  
                                    echo '<td class="actions text-right">';
                                    echo '<div class="dropdown">';
                                       echo '<button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                        <span class="caret"></span></button>';       
                                        echo '<ul class="dropdown-menu">';
                                        echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-print" target="_blank"> Imprimir</span> ', 'Relatorio', 'reports_inspecao', 
                                            array('id' => $f->id),
                                            array('target' => '_blank'));
                                        echo '</li>';     
                                            echo '<li>';
                                                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"> Editar</span> ', 'Frota_inspecao', 'edit', 
                                                array('id' => $f->id), 
                                                array('class' => 'btn btn-sm'));
                                            echo '</li>';
                                            echo '<li>';
                                                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"> Delete</span> ', 'Frota_inspecao', 'delete', 
                                                array('id' => $f->id), 
                                                array('class' => 'btn btn-sm','data-toggle' => 'modal'));
                                            echo '</li>';
                                        echo '</ul>';
                                    echo '</div>';
                                    echo '</td>';                                   
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>
                    <script>
                    /* faz a pesquisa com ajax */
                    $(document).ready(function() {
                        $('#search').keyup(function() {
                            var r = true;
                            if (r) {
                                r = false;
                                $("div.table-responsive").load(
                                <?php
                                if (isset($_GET['orderBy']))
                                    echo '"' . $this->Html->getUrl('Frota_inspecao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                else
                                    echo '"' . $this->Html->getUrl('Frota_inspecao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                ?>
                                , function() {
                                    r = true;
                                });
                            }
                        });
                    });
                </script>
                </div>
            </div>
        </div>
    </div>
</div>