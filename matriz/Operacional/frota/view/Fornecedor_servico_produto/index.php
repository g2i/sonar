
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Produtos e Serviços</h2>
    <ol class="breadcrumb">
    <li>Produtos e Serviços</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">     
                <input type="hidden" name="paramId" value="<?php echo $this->getParam('id');?>">                   
                <?php if($this->getParam('modal')){ ?>
                <!--passa param para a modal-->      
                <?php }else{?>
                    <div class="form-group col-md-6 col-sm-12 col-xs-12">        
                    <label for="fornecedor">Fornecedor</label>
                    <select name="fornecedor" class="form-control" id="fornecedor">
                    <option value="">Selecione</option>
                        <?php
                        foreach ($Fornecedores as $f) {
                            if ($f->id == $Fornecedor_servico_produto->fornecedor_id)
                                echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                            else
                                echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                        }
                        ?>
                    </select>
                </div>   
                    <?php }?>
                <div class="col-md-6 form-group">
                    <label for="produto_servico">Produtos/Serviços</label>
                    <input type="text" name="produto_servico" id="produto_servico" class="form-control" value="<?php echo $this->getParam('produto_servico'); ?>">
                </div>
                <div class="col-md-12 text-right">
                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"  class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                           title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                <button type="button" class="btn btn-default" id="buscar-filtro" data-tool="tooltip" data-placement="bottom" title="Pesquisar"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">    
        <?php if($this->getParam('modal')){ ?>
            <!--passa param para a modal-->      
            <p><a href="Javascript:void(0)" class="btn btn-default" 
            onclick="Navegar('<?php echo $this->Html->getUrl("Fornecedor_servico_produto","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                <span class="img img-add"></span> Novo Cadastro</a></p>
        <?php }else{?>
            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Cadastro', 'Fornecedor_servico_produto', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>

           <?php }?>
    </div>

<!-- tabela de resultados -->
<div class="ibox-content">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>                   
                        id
                </th>
                <th>
                   Fornecedor
                </th>
                <th>
                   Produtos/Serviços
                    
                </th>              
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Fornecedor_servico_produtos as $f) {
                echo '<tr>';
                echo '<td>';
                echo $f->id;
                echo '</td>';
                echo '<td>';
                echo $f->getFornecedor()->nome;
                echo '</td>';
                echo '<td>';
                echo $f->produto_servico;
                echo '</td>';               
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                echo $this->Html->getLink('<span class="fa fa-pencil-square"></span> ', 'Fornecedor_servico_produto', 'edit',
                    array('id' => $f->id),
                    array('class' => 'btn btn-xs btn-info', 'data-toggle' => 'modal'));
                echo '</td>';
                echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                echo $this->Html->getLink('<span class="fa fa-trash-o"></span> ', 'Fornecedor_servico_produto', 'delete',
                    array('id' => $f->id),
                    array('class' => 'btn btn-xs btn-danger', 'data-toggle' => 'modal'));
                echo 
                '</td>';       
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
//function para o btn de pesquisa em form 
$('.filtros').find('#buscar-filtro').click(function(){
        let form_action = $(this).closest('form').attr('action');
        let form_serialize = $(this).closest('form').serialize();
        let url_completa = form_action + '?' + form_serialize;
        console.log(url_completa);
        carregaTabelaResponsiva(url_completa);
    });
    
//function para paginar por ajax
    $(document).on('click','.pagination a, .table thead a', function(e){
        e.preventDefault();
        let url = $(this).attr('href');

        if(url != "")
        {            
            carregaTabelaResponsiva(url);
        }
        return false;
    });
   //function para pegar as URLs 
function carregaTabelaResponsiva(url) {
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: () => {
        },
        success: (data) => {
            let conteudo = $('<div>').append(data).find('.table-responsive');
            console.log(conteudo);
            $(".table-responsive").html(conteudo);
        },
        complete: () => {
        },
        error: () => {
        },
    });
}
    
</script>
</div>
</div>
</div>
</div>
</div>