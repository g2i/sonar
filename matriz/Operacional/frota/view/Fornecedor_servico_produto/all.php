<!--<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Produtos e Serviços</h2>
        <ol class="breadcrumb">
            <li>Produtos e Serviços</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>-->
<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
        <div class="col-lg-9">
                <h2>Produtos e Serviços</h2>
                <ol class="breadcrumb">
                    <li>Produtos e Serviços</li>
                    <li class="active">
                        <strong>Listar</strong>
                    </li>
                    <br><br>
                </ol>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">    
                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <?php if($this->getParam('modal')){ ?>
                        <!--passa param para a modal-->
                        <p><a href="Javascript:void(0)" class="btn btn-primary" 
                        onclick="Navegar('<?php echo $this->Html->getUrl("Fornecedor_servico_produto","add",array("modal"=>1,"ajax"=>true,"id"=>$this->getParam("id")))?>','go')">
                        <span class="img img-add"></span> Novo Cadastro</a></p>
                        <?php }else{?>
                        <p>
                            <?php echo $this->Html->getLink('<span class="img img-add"></span> Novo Cadastro', 'Fornecedor_servico_produto', 'add', NULL, array('class' => 'btn btn-default')); ?>
                        </p>

                        <?php }?>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        id
                                    </th>
                                    <th>
                                        Fornecedor
                                    </th>
                                    <th>
                                        Produtos/Serviços

                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                    foreach ($Fornecedor_servico_produtos as $f) {
                                        echo '<tr>';
                                        echo '<td>';
                                        echo $f->id;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $f->getFornecedor()->nome;
                                        echo '</td>';
                                        echo '<td>';
                                        echo $f->produto_servico;
                                        echo '</td>';                                        
                                        echo '<td width="30">';
                                    ?>
                                        <a href="Javascript:void(0)" class="btn btn-xs btn-info" 
                                        onclick="Navegar('<?php echo $this->Html->getUrl("Fornecedor_servico_produto","edit",array("modal"=>1,"ajax"=>true,"id"=>$f->id))?>','go')">
                                        <span class="fa fa-pencil-square"></span></a>
                                    <?php
                                        echo '</td>';
                                        echo '<td width="30">';
                                    ?>
                                        <a href="Javascript:void(0)" class="btn btn-xs btn-danger" 
                                        onclick="Navegar('<?php echo $this->Html->getUrl("Fornecedor_servico_produto","delete",array("modal"=>1,"ajax"=>true,"id"=>$f->id))?>','go')">
                                        <span class="fa fa-trash-o"></span></a>
                                    <?php 
                                        echo '</td>';                                                
                                        echo '</tr>';
                                    }
                                    ?>
                            </table>
                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>

                    <script>
                        //function para o btn de pesquisa em form 
                        $('.filtros').find('#buscar-filtro').click(function () {
                            let form_action = $(this).closest('form').attr('action');
                            let form_serialize = $(this).closest('form').serialize();
                            let url_completa = form_action + '?' + form_serialize;
                            console.log(url_completa);
                            carregaTabelaResponsiva(url_completa);
                        });

                        //function para paginar por ajax
                        $(document).on('click', '.pagination a, .table thead a', function (e) {
                            e.preventDefault();
                            let url = $(this).attr('href');

                            if (url != "") {
                                carregaTabelaResponsiva(url);
                            }
                            return false;
                        });
                        //function para pegar as URLs 
                        function carregaTabelaResponsiva(url) {
                            $.ajax({
                                type: 'GET',
                                url: url,
                                beforeSend: () => {
                                },
                                success: (data) => {
                                    let conteudo = $('<div>').append(data).find('.table-responsive');
                                    console.log(conteudo);
                                    $(".table-responsive").html(conteudo);
                                },
                                complete: () => {
                                },
                                error: () => {
                                },
                            });
                        }

                    </script>
                </div>
            </div>
        </div>
    </div>
</div>