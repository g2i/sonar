<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Pergunta</h2>
        <ol class="breadcrumb">
            <li>Pergunta</li>
            <li class="active">
                <strong>Adicionar </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Frota_inspecao_pergunta', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nome">Pergunta</label>
                                <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Frota_inspecao_pergunta->nome ?>"
                                    placeholder="Pergunta">
                            </div>    
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="frota_inspecao_grupo_id">Grupo</label>
                            <select name="frota_inspecao_grupo_id" class="form-control" id="frota_inspecao_grupo_id">
                                <?php
                                foreach ($Frota_inspecao_grupos as $f) {
                                    if ($f->id == $Frota_inspecao_pergunta->frota_inspecao_grupo_id)
                                        echo '<option selected value="' . $f->id . '">' . $f->nome . '</option>';
                                    else
                                        echo '<option value="' . $f->id . '">' . $f->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>                        
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_inspecao_pergunta', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>