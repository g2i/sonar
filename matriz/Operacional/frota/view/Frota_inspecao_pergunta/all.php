<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Perguntas</h2>
        <ol class="breadcrumb">
            <li>Perguntas</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="nome">Pergunta</label>
                                    <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                        value="<?php echo $this->getParam('nome'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="frota_inspecao_grupo_id">Grupo</label>
                                    <select name="filtro[externo][frota_inspecao_grupo_id]" class="form-control" id="frota_inspecao_grupo_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Frota_inspecao_grupos as $f): ?>
                                        <?php echo '<option value="' . $f->id . '">' . $f->nome . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                    class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                    title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p>
                            <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Frota_inspecao_pergunta', 'add', NULL, array('class' => 'btn btn-primary')); ?>
                        </p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='#'>
                                            id
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Pergunta
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Grupo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Cadastrado Por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Data Cadastro
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Modificado Por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Data Modificado
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_inspecao_perguntas as $f) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $f->id; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->nome; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->getFrota_inspecao_grupo()->nome; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->getUsuario()->nome; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo DataTimeBr($f->dt_cadastro); 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $f->getUsuario2()->nome; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo DataTimeBr($f->dt_modificado); 
                                    echo '</td>';  
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_inspecao_pergunta', 'edit', 
                                        array('id' => $f->id), 
                                        array('class' => 'btn btn-warning btn-sm'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_inspecao_pergunta', 'delete', 
                                        array('id' => $f->id), 
                                        array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>

                 
                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function() {
                            $('#search').keyup(function() {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                    <?php
                                    if (isset($_GET['orderBy']))
                                        echo '"' . $this->Html->getUrl('Frota_inspecao_pergunta', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    else
                                        echo '"' . $this->Html->getUrl('Frota_inspecao_pergunta', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    ?>
                                    , function() {
                                        r = true;
                                    });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>