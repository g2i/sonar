<?php include_once "dataUtil.php"; ?>
<p><strong>Cadastrado em</strong>: <?php echo convertDataSQL4BR($Frota_ocorrencias->dt_cadastro);?></p>
<p><strong>Data da Ocorrência</strong>: <?php echo convertDataSQL4BR($Frota_ocorrencias->dt_ocorrencia);?></p>
<p><strong>Descrição</strong>: <?php echo $Frota_ocorrencias->descricao;?></p>
<p>
    <strong>Veículo</strong>:
    <?php
    echo $this->Html->getLink($Frota_ocorrencias->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
    array('id' => $Frota_ocorrencias->getFrota_veiculos()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Usuário</strong>:
    <?php
    echo $this->Html->getLink($Frota_ocorrencias->getUsuario()->login, 'Usuario', 'view',
    array('id' => $Frota_ocorrencias->getUsuario()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Tipo de Ocorrência</strong>:
    <?php
    echo $this->Html->getLink($Frota_ocorrencias->getFrota_tipo_ocorrencia()->descricao, 'Frota_tipo_ocorrencia', 'view',
    array('id' => $Frota_ocorrencias->getFrota_tipo_ocorrencia()->id), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>