<div class="wrapper wrapper-content animated">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>Ocorrências</h2>
                <ol class="breadcrumb">
                    <li>Ocorrências</li>
                    <li class="active">
                        <strong>Editar Ocorrência</strong>
                    </li>
                </ol>
                <br>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Frota_ocorrencias', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="dt_ocorrencia">Data da Ocorrência</label>
                                <input type="date" name="dt_ocorrencia" id="dt_ocorrencia" class="form-control"
                                       value="<?php echo $Frota_ocorrencias->dt_ocorrencia ?>"
                                       placeholder="Dt_ocorrencia">
                            </div>
                            <div class="form-group">
                                <label for="veiculo_id">Veículos</label>
                                <select name="veiculo_id" class="form-control" id="veiculo_id">
                                    <?php
                                    foreach ($Frota_veiculos as $f) {
                                        if ($f->id == $Frota_ocorrencias->veiculo_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->placa . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->placa . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tipo_ocorrencia_id">Tipo de Ocorrência</label>
                                <select name="tipo_ocorrencia_id" class="form-control" id="tipo_ocorrencia_id">
                                    <?php
                                    foreach ($Frota_tipo_ocorrencias as $f) {
                                        if ($f->id == $Frota_ocorrencias->tipo_ocorrencia_id)
                                            echo '<option selected value="' . $f->id . '">' . $f->descricao . '</option>';
                                        else
                                            echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="infrator">Infrator (Nome)</label>
                                <input type="text" name="infrator" id="infrator" class="form-control"
                                       value="<?php echo $Frota_ocorrencias->infrator ?>" placeholder="Infrator">
                            </div>

                            <div class="form-group">
                                <label for="valor" class="required">Valor Infração</label><span
                                        class="glyphicon glyphicon-asterisk"></span>
                                <input type="number" step="any" name="valor" id="valor" class="form-control"
                                       value="<?php echo $Frota_ocorrencias->valor ?>"
                                       required placeholder="Valor">
                            </div>

                            <div class="form-group">
                                <label for="descricao">Descrição</label>
                                <textarea name="descricao" id="descricao"
                                          class="form-control"><?php echo $Frota_ocorrencias->descricao ?></textarea>
                            </div>

                            <div class="form-group">
                                <label for="recorrencia">Infração - Recorrer</label>
                                <textarea name="recorrencia" id="recorrencia"
                                          class="form-control"><?php echo $Frota_ocorrencias->recorrencia ?></textarea>
                            </div>

                            <div class="form-group">
                                <label for="infracao_desconto_folha">Infração - Descontado em Folha?</label>
                                <select name="infracao_desconto_folha" class="form-control selectPicker"
                                        id="infracao_desconto_folha">
                                    <?php if($Frota_ocorrencias->infracao_desconto_folha == "1"): ?>
                                        <option value="0">Não</option>
                                        <option selected value="1">Sim</option>
                                    <?php else: ?>
                                        <option selected value="0">Não</option>
                                        <option value="1">Sim</option>
                                    <?php endif; ?>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="usuario_debito_folha">Usuário - Desconto Folha</label>
                                <input type="text" readonly name="usuario_debito_folha" id="usuario_debito_folha" class="form-control"
                                       value="<?php echo $Frota_ocorrencias->getUsuarioDescontoFolha()->nome ?>">
                            </div>

                            <div class="form-group">
                                <label for="infrator">Data - Desconto Folha</label>
                                <input type="text" readonly name="infrator" id="infrator" class="form-control"
                                       value="<?php echo $Frota_ocorrencias->data_desconto_folha ?>">
                            </div>

                        </div>
                        <input type="hidden" name="id" value="<?php echo $Frota_ocorrencias->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Frota_ocorrencias', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>