<?php
final class Frota_ocorrencias extends Record{ 

    const TABLE = 'frota_ocorrencias';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_ocorrencias pertence a Frota_veiculos
    * @return Frota_veiculos $Frota_veiculos
    */
    function getFrota_veiculos() {
        return $this->belongsTo('Frota_veiculos','veiculo_id');
    }
    
    /**
    * Frota_ocorrencias pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }

    /**
     * Frota_ocorrencias pertence a Usuario
     * @return Usuario $Usuario
     */
    function getUsuarioDescontoFolha() {
        return $this->belongsTo('Usuario','usuario_desconto_folha');
    }
    /**
    * Frota_ocorrencias pertence a Frota_tipo_ocorrencia
    * @return Frota_tipo_ocorrencia $Frota_tipo_ocorrencia
    */
    function getFrota_tipo_ocorrencia() {
        return $this->belongsTo('Frota_tipo_ocorrencia','tipo_ocorrencia_id');
    }
}