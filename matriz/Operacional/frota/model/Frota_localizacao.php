<?php

final class Frota_localizacao extends Record
{

    const TABLE = 'frota_localizacao';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure()
    {
        # $criteria = new Criteria();
        # return $criteria;
    }

    /**
     * Frota_localizacao possui Frota_veiculos
     * @return array de Frota_veiculos
     */
    function getFrota_veiculos($criteria = NULL)
    {
        return $this->hasMany('Frota_veiculos', 'localizacao_id', $criteria);
    }

    /**
     * Frota_localizacao possui Frota_motorista
     * @return array de Frota_motorista
     */
    function getFrota_motorista($criteria = NULL)
    {
        return $this->hasMany('Frota_motorista', 'localizacao_id', $criteria);
    }


    /**
     * Frota_localizacao pertence a Situacao
     * @return Situacao $Situacao
     */
    function getSituacao()
    {
        return $this->belongsTo('Situacao', 'situacao_id');
    }
}