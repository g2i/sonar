<?php
final class Frota_abastecimentos extends Record{ 

    const TABLE = 'frota_abastecimentos';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_abastecimentos pertence a Frota_veiculos
    * @return Frota_veiculos $Frota_veiculos
    */
    function getFrota_veiculos() {
        return $this->belongsTo('Frota_veiculos','veiculo_id');
    }
    
    /**
    * Frota_abastecimentos pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
    
    /**
    * Froota_fornecedor pertence a Froota_fornecedor
    * @return Fornecedor $Fornecedor
    */
    function getFornecedor() {
        return $this->belongsTo('Fornecedor','fornecedor_id');
    }

    /**
    * Frota_motorista pertence a Frota_motorista
    * @return Fornecedor $Fornecedor
    */
    function getMotorista() {
        return $this->belongsTo('Frota_motorista','motorista_id');
    }

    /**
    * Frota_combustivel pertence a Frota_combustivel
    * @return Frota_combustivel $Fornecedor
    */
    function getCombustivel() {
        return $this->belongsTo('Frota_combustivel','combustivel_id');
    }

    public static function getMaxKm($id) {
        $query = "SELECT MAX(frota_abastecimentos.quilometragem) AS km_max FROM frota_abastecimentos WHERE veiculo_id = :veiculo_id";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':veiculo_id', $id);
        return $db->getResults();
    }

    public static function getTotalAbastecimento($ano, $mes) {

        $query = "SELECT SUM(fa.valor_litro * fa.quantidade) AS total FROM frota_abastecimentos fa WHERE YEAR(fa.`dt_abastecimento`) = :ano AND MONTH(fa.dt_abastecimento) = :mes";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':ano', $ano);
        $db->bind(':mes', $mes);
        return $db->getRow();
    }
}