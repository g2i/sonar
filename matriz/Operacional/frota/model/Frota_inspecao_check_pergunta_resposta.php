<?php
final class Frota_inspecao_check_pergunta_resposta extends Record{ 

    const TABLE = 'frota_inspecao_check_pergunta_resposta';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_inspecao_check_pergunta_resposta pertence a Frota_inspecao_pergunta
    * @return Frota_inspecao_pergunta $Frota_inspecao_pergunta
    */
    function getFrota_inspecao_pergunta() {
        return $this->belongsTo('Frota_inspecao_pergunta','frota_inspecao_pergunta_id');
    }
    
    /**
    * Frota_inspecao_check_pergunta_resposta pertence a Frota_inspecao_resposta
    * @return Frota_inspecao_resposta $Frota_inspecao_resposta
    */
    function getFrota_inspecao_resposta() {
        return $this->belongsTo('Frota_inspecao_resposta','frota_inspecao_resposta_id');
    }
    
    /**
    * Frota_inspecao_check_pergunta_resposta pertence a Frota_inspecao
    * @return Frota_inspecao $Frota_inspecao
    */
    function getFrota_inspecao() {
        return $this->belongsTo('Frota_inspecao','frota_inspecao_id');
    }
    
    /**
    * Frota_inspecao_check_pergunta_resposta pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Frota_inspecao_check_pergunta_resposta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Frota_inspecao_check_pergunta_resposta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}