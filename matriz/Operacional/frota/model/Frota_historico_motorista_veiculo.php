<?php
final class Frota_historico_motorista_veiculo extends Record{ 

    const TABLE = 'frota_historico_motorista_veiculo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_historico_motorista_veiculo pertence a Frota_veiculos
    * @return Frota_veiculos $Frota_veiculos
    */
    function getFrota_veiculos() {
        return $this->belongsTo('Frota_veiculos','veiculo_id');
    }
    
    /**
    * Frota_historico_motorista_veiculo pertence a Frota_motorista
    * @return Frota_motorista $Frota_motorista
    */
    function getFrota_motorista() {
        return $this->belongsTo('Frota_motorista','motorista_antigo_id');
    }
    
    /**
    * Frota_historico_motorista_veiculo pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}