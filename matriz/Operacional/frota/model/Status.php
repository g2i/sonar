<?php
final class Status extends Record{ 

    const TABLE = 'status';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Status possui Centro_custos
    * @return array de Centro_custos
    */
    function getCentro_custos($criteria=NULL) {
        return $this->hasMany('Centro_custo','status',$criteria);
    }
    
    /**
    * Status possui Clientes
    * @return array de Clientes
    */
    function getClientes($criteria=NULL) {
        return $this->hasMany('Cliente','status',$criteria);
    }
    
    /**
    * Status possui Contabilidades
    * @return array de Contabilidades
    */
    function getContabilidades($criteria=NULL) {
        return $this->hasMany('Contabilidade','status',$criteria);
    }
    
    /**
    * Status possui Contaspagares
    * @return array de Contaspagares
    */
    function getContaspagares($criteria=NULL) {
        return $this->hasMany('Contaspagar','status',$criteria);
    }
    
    /**
    * Status possui Contasreceberes
    * @return array de Contasreceberes
    */
    function getContasreceberes($criteria=NULL) {
        return $this->hasMany('Contasreceber','status',$criteria);
    }
    
    /**
    * Status possui Correcaomonetarias
    * @return array de Correcaomonetarias
    */
    function getCorrecaomonetarias($criteria=NULL) {
        return $this->hasMany('Correcaomonetaria','status_id',$criteria);
    }
    
    /**
    * Status possui Deducoes
    * @return array de Deducoes
    */
    function getDeducoes($criteria=NULL) {
        return $this->hasMany('Deducoes','status',$criteria);
    }
    
    /**
    * Status possui Empresa_custos
    * @return array de Empresa_custos
    */
    function getEmpresa_custos($criteria=NULL) {
        return $this->hasMany('Empresa_custo','status',$criteria);
    }
    
    /**
    * Status possui Estq_configuracaos
    * @return array de Estq_configuracaos
    */
    function getEstq_configuracaos($criteria=NULL) {
        return $this->hasMany('Estq_configuracao','status',$criteria);
    }
    
    /**
    * Status possui Fornecedores
    * @return array de Fornecedores
    */
    function getFornecedores($criteria=NULL) {
        return $this->hasMany('Fornecedor','status',$criteria);
    }
    
    /**
    * Status possui Frota_motoristas
    * @return array de Frota_motoristas
    */
    function getFrota_motoristas($criteria=NULL) {
        return $this->hasMany('Frota_motorista','status',$criteria);
    }
    
    /**
    * Status possui Frota_situacao_habilitacaos
    * @return array de Frota_situacao_habilitacaos
    */
    function getFrota_situacao_habilitacaos($criteria=NULL) {
        return $this->hasMany('Frota_situacao_habilitacao','status',$criteria);
    }
    
    /**
    * Status possui Medidores_localidades
    * @return array de Medidores_localidades
    */
    function getMedidores_localidades($criteria=NULL) {
        return $this->hasMany('Medidores_localidade','status_id',$criteria);
    }
    
    /**
    * Status possui Movimentos
    * @return array de Movimentos
    */
    function getMovimentos($criteria=NULL) {
        return $this->hasMany('Movimento','status',$criteria);
    }
    
    /**
    * Status possui Plano_centrocustos
    * @return array de Plano_centrocustos
    */
    function getPlano_centrocustos($criteria=NULL) {
        return $this->hasMany('Plano_centrocusto','status',$criteria);
    }
    
    /**
    * Status possui Planocontas
    * @return array de Planocontas
    */
    function getPlanocontas($criteria=NULL) {
        return $this->hasMany('Planocontas','status',$criteria);
    }
    
    /**
    * Status possui Programacaos
    * @return array de Programacaos
    */
    function getProgramacaos($criteria=NULL) {
        return $this->hasMany('Programacao','status',$criteria);
    }
    
    /**
    * Status possui Rateio_contaspagares
    * @return array de Rateio_contaspagares
    */
    function getRateio_contaspagares($criteria=NULL) {
        return $this->hasMany('Rateio_contaspagar','status',$criteria);
    }
    
    /**
    * Status possui Rateio_contasreceberes
    * @return array de Rateio_contasreceberes
    */
    function getRateio_contasreceberes($criteria=NULL) {
        return $this->hasMany('Rateio_contasreceber','status',$criteria);
    }
    
    /**
    * Status possui Rateio_programacaos
    * @return array de Rateio_programacaos
    */
    function getRateio_programacaos($criteria=NULL) {
        return $this->hasMany('Rateio_programacao','status',$criteria);
    }
    
    /**
    * Status possui Tipo_classificacaos
    * @return array de Tipo_classificacaos
    */
    function getTipo_classificacaos($criteria=NULL) {
        return $this->hasMany('Tipo_classificacao','status',$criteria);
    }
    
    /**
    * Status possui Tipo_documentos
    * @return array de Tipo_documentos
    */
    function getTipo_documentos($criteria=NULL) {
        return $this->hasMany('Tipo_documento','status_id',$criteria);
    }
    
    /**
    * Status possui Tipo_pagamentos
    * @return array de Tipo_pagamentos
    */
    function getTipo_pagamentos($criteria=NULL) {
        return $this->hasMany('Tipo_pagamento','status_id',$criteria);
    }
}