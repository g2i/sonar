<?php
final class Frota_inspecao_pergunta extends Record{ 

    const TABLE = 'frota_inspecao_pergunta';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_inspecao_pergunta pertence a Frota_inspecao_grupo
    * @return Frota_inspecao_grupo $Frota_inspecao_grupo
    */
    function getFrota_inspecao_grupo() {
        return $this->belongsTo('Frota_inspecao_grupo','frota_inspecao_grupo_id');
    }
    
    /**
    * Frota_inspecao_pergunta pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Frota_inspecao_pergunta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Frota_inspecao_pergunta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}