<?php
final class Fornecedor extends Record{ 

    const TABLE = 'fornecedor';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Fornecedor possui Frota_abastecimentos
    * @return array de Frota_abastecimentos
    */
    function getFrota_abastecimentos($criteria=NULL) {
        return $this->hasMany('Frota_abastecimentos','fornecedor_id',$criteria);
    }
    
    /**
    * Fornecedor possui Frota_manutencoes
    * @return array de Frota_manutencoes
    */
    function getFrota_manutencoes($criteria=NULL) {
        return $this->hasMany('Frota_manutencoes','fornecedor_id',$criteria);
    }
    function getRhstatus() {
        return $this->belongsTo('Status','status');
    }
}