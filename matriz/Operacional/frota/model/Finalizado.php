<?php
final class Finalizado extends Record{ 

    const TABLE = 'finalizado';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Finalizado possui Frota_acidentes
    * @return array de Frota_acidentes
    */
    function getFrota_acidentes($criteria=NULL) {
        return $this->hasMany('Frota_acidente','finalizado_id',$criteria);
    }
}