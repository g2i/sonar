<?php
final class Frota_inspecao extends Record{ 

    const TABLE = 'frota_inspecao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_inspecao pertence a Frota_veiculos
    * @return Frota_veiculos $Frota_veiculos
    */
    function getFrota_veiculos() {
        return $this->belongsTo('Frota_veiculos','veiculo_id');
    }
    
    /**
    * Frota_inspecao pertence a Frota_motorista
    * @return Frota_motorista $Frota_motorista
    */
    function getFrota_motorista() {
        return $this->belongsTo('Frota_motorista','motorista_id');
    }
    
    /**
    * Frota_inspecao pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Frota_inspecao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Frota_inspecao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
    function getLocalidade() {
        return $this->belongsTo('Rhlocalidade','localidade_id');
    }
}