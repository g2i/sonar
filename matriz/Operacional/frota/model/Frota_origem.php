<?php
final class Frota_origem extends Record{ 

    const TABLE = 'frota_origem';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_origem pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
    
    /**
    * Frota_origem possui Frota_veiculos
    * @return array de Frota_veiculos
    */
    function getFrota_veiculos($criteria=NULL) {
        return $this->hasMany('Frota_veiculos','origem_id',$criteria);
    }
}