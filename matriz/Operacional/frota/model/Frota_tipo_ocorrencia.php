<?php
final class Frota_tipo_ocorrencia extends Record{ 

    const TABLE = 'frota_tipo_ocorrencia';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_tipo_ocorrencia possui Frota_ocorrencias
    * @return array de Frota_ocorrencias
    */
    function getFrota_ocorrencias($criteria=NULL) {
        return $this->hasMany('Frota_ocorrencias','tipo_ocorrencia_id',$criteria);
    }
    
    /**
    * Frota_tipo_ocorrencia pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
}