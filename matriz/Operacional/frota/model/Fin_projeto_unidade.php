<?php
final class Fin_projeto_unidade extends Record{ 

    const TABLE = 'fin_projeto_unidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Fin_projeto_unidade pertence a Fin_projeto_empresa
    * @return Fin_projeto_empresa $Fin_projeto_empresa
    */
    function getFin_projeto_empresa() {
        return $this->belongsTo('Fin_projeto_empresa','fin_projeto_empresa_id');
    }
    
    /**
    * Fin_projeto_unidade pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Fin_projeto_unidade pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','user_created');
    }
    
    /**
    * Fin_projeto_unidade pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','user_modified');
    }
}