<?php

final class Frota_situacao_habilitacao extends Record
{

    const TABLE = 'frota_situacao_habilitacao';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure()
    {
        # $criteria = new Criteria();
        # return $criteria;
    }

    /**
     * Frota_situacao_habilitacao possui Frota_motorista
     * @return array de Frota_motorista
     */
    function getFrota_motorista($criteria = NULL)
    {
        return $this->hasMany('Frota_motorista', 'situacao_habilitacao_id', $criteria);
    }

}