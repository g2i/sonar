<?php
final class Frota_acidente extends Record{ 

    const TABLE = 'frota_acidente';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_acidente pertence a Frota_veiculos
    * @return Frota_veiculos $Frota_veiculos
    */
    function getFrota_veiculos() {
        return $this->belongsTo('Frota_veiculos','veiculo_id');
    }
    
    /**
    * Frota_acidente pertence a Frota_tipo_acidente
    * @return Frota_tipo_acidente $Frota_tipo_acidente
    */
    function getFrota_tipo_acidente() {
        return $this->belongsTo('Frota_tipo_acidente','tipo_acidente_id');
    }
    
    /**
    * Frota_acidente pertence a Rhprofissional
    * @return Rhprofissional $Rhprofissional
    */
    function getRhprofissional() {
        return $this->belongsTo('Rhprofissional','profissional_id');
    }
    
    /**
    * Frota_acidente pertence a Finalizado
    * @return Finalizado $Finalizado
    */
    function getFinalizado() {
        return $this->belongsTo('Finalizado','finalizado_id');
    }
    
    /**
    * Frota_acidente pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Frota_acidente pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Frota_acidente pertence a Usuario
    * @return Usuario $Usuario Frota_situacao_acidente
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
    function getSituacaoAcidente() {
        return $this->belongsTo('Frota_situacao_acidente','situacao_acidente_id');
    }
}