<?php
final class Usuario extends Record{ 

    const TABLE = 'usuario';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Usuario possui Frota_abastecimentos
    * @return array de Frota_abastecimentos
    */
    function getFrota_abastecimentos($criteria=NULL) {
        return $this->hasMany('Frota_abastecimentos','usuario_id',$criteria);
    }
    
    /**
    * Usuario possui Frota_manutencoes
    * @return array de Frota_manutencoes
    */
    function getFrota_manutencoes($criteria=NULL) {
        return $this->hasMany('Frota_manutencoes','usuario_id',$criteria);
    }
    
    /**
    * Usuario possui Frota_ocorrencias
    * @return array de Frota_ocorrencias
    */
    function getFrota_ocorrencias($criteria=NULL) {
        return $this->hasMany('Frota_ocorrencias','usuario_id',$criteria);
    }
    
    /**
    * Usuario possui Frota_origens
    * @return array de Frota_origens
    */
    function getFrota_origens($criteria=NULL) {
        return $this->hasMany('Frota_origem','usuario_id',$criteria);
    }
    
    /**
    * Usuario possui Frota_veiculos
    * @return array de Frota_veiculos
    */
    function getFrota_veiculos($criteria=NULL) {
        return $this->hasMany('Frota_veiculos','usuario_id',$criteria);
    }
    
    /**
    * Usuario pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
}