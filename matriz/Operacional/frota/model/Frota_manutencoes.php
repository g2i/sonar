<?php

final class Frota_manutencoes extends Record
{

    const TABLE = 'frota_manutencoes';
    const PK = 'id';

    public static function getTotalManutencoes($ano, $mes) {
        $query = "SELECT SUM(fm.valor) AS total FROM frota_manutencoes fm WHERE YEAR(fm.`dt_manutencao`) = :ano AND MONTH(fm.`dt_manutencao`) = :mes";
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        $db->bind(':ano', $ano);
        $db->bind(':mes', $mes);
        return $db->getRow();
    }

    public static function getVeiculosManutencoes($veiculo_id = NULL)
    {

        if (!empty($veiculo_id)) {
            $query = "SELECT fv.placa AS placa ,tbl.km AS km, fm.*  FROM frota_manutencoes fm 
            INNER JOIN frota_veiculos fv ON fm.veiculo_id = fv.id 
            INNER JOIN (SELECT veiculo_id, MAX(quilometragem) AS km FROM frota_abastecimentos 
            GROUP BY veiculo_id) AS tbl ON tbl.veiculo_id = fv.`id` WHERE (fm.`vida_util` + fm.`quilometragem`) - tbl.km <= 100
            AND (fm.`vida_util` + fm.`quilometragem`) - tbl.km >= 0  AND fm.`veiculo_id` = :veiculo_id";
        } else {
            $query = "SELECT fv.placa AS placa ,tbl.km AS km, ftm.descricao AS manutencao, fm.* FROM frota_manutencoes fm 
            INNER JOIN frota_veiculos fv ON fm.veiculo_id = fv.id 
            INNER JOIN (SELECT veiculo_id, MAX(quilometragem) AS km FROM frota_abastecimentos 
            GROUP BY veiculo_id) AS tbl ON tbl.veiculo_id = fv.`id` 
            INNER JOIN frota_tipo_manutencao ftm ON fm.`tipo_manutencao_id` = ftm.id 
            WHERE (fm.`vida_util` + fm.`quilometragem`) - tbl.km <= 100 AND (fm.`vida_util` + fm.`quilometragem`) - tbl.km >= 0 AND fm.`situacao_id` = 1";

        }
        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        if(!empty($veiculo_id)) {
            $db->bind(':veiculo_id', $veiculo_id);
        }
        return $db->getResults();
    }

    public function contManutencoes(){
        $contManut = "SELECT COUNT(fm.id) as total FROM frota_manutencoes fm 
        INNER JOIN frota_veiculos fv ON fm.veiculo_id = fv.id 
        INNER JOIN (SELECT veiculo_id, MAX(quilometragem) AS km FROM frota_abastecimentos 
        GROUP BY veiculo_id) AS tbl ON tbl.veiculo_id = fv.`id` 
        INNER JOIN frota_tipo_manutencao ftm ON fm.`tipo_manutencao_id` = ftm.id 
        WHERE (fm.`vida_util` + fm.`quilometragem`) - tbl.km <= 100 
        AND (fm.`vida_util` + fm.`quilometragem`) - tbl.km >= 0 AND fm.`situacao_id` = 1";
          
          $db = new MysqlDB();
          $db->query($contManut);
          $db->execute();
        
          return $db->getResults();
    }

    public static function configure()
    {
        # $criteria = new Criteria();
        # return $criteria;
    }

  
    function getFrota_veiculos()
    {
        return $this->belongsTo('Frota_veiculos', 'veiculo_id');
    }
 
    
    function getSituacaoManutencao()
    {
        return $this->belongsTo('Frota_situacao_manutencao', 'situacao_manutencao_id');
    }
   
    function getUsuario()
    {
        return $this->belongsTo('Usuario', 'usuario_id');
    }

 
    function getFornecedor()
    {
        return $this->belongsTo('Fornecedor', 'fornecedor_id');
    }

  
    function getFrota_tipo_manutencao()
    {
        return $this->belongsTo('Frota_tipo_manutencao', 'tipo_manutencao_id');
    }
    
}