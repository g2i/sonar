<?php
final class Frota_manutencao_classificacao extends Record{ 

    const TABLE = 'frota_manutencao_classificacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_manutencao_classificacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Frota_manutencao_classificacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
}