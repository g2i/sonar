<?php
final class Frota_equipamentos_veiculo extends Record{

    const TABLE = 'frota_equipamentos_veiculo';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }

    /**
     * Frota_equipamentos_veiculo pertence a Usuario
     * @return Usuario $Usuario
     */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }

    /**
     * Frota_equipamentos_veiculo pertence a Frota_veiculos
     * @return Frota_veiculos $Frota_veiculos
     */
    function getFrota_veiculos() {
        return $this->belongsTo('Frota_veiculos','veiculo_id');
    }

    //get para tipo equipamento 
    function getTipo_item() {
        return $this->belongsTo('Frota_tipo_equipamento','tipo_equipamento_id');
    }

    //get para todos os equipamentos com vencimento 45 dias 
    function getEquipamentoVencimento(){
        $query = "SELECT fev.id, te.nome, fv.`placa`, fev.validade_laudo_mecanico FROM `frota_equipamentos_veiculo` fev
        LEFT JOIN `frota_tipo_equipamento` te ON te.id = fev.tipo_equipamento_id
        LEFT JOIN `frota_veiculos` fv ON fv.id = fev.veiculo_id
        WHERE DATEDIFF(fev.validade_laudo_mecanico, CURDATE()) <= 60 
        AND DATEDIFF(fev.validade_laudo_mecanico, CURDATE()) >= 0 
        AND fev.situacao_id = 1";

        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        return $db->getResults();
    }
}