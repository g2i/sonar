<?php
final class Fornecedor_servico_produto extends Record{ 

    const TABLE = 'fornecedor_servico_produto';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Fornecedor_servico_produto pertence a Fornecedor
    * @return Fornecedor $Fornecedor
    */
    function getFornecedor() {
        return $this->belongsTo('Fornecedor','fornecedor_id');
    }
    
    /**
    * Fornecedor_servico_produto pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Fornecedor_servico_produto pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Fornecedor_servico_produto pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}