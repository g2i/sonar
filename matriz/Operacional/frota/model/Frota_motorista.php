<?php

final class Frota_motorista extends Record
{

    const TABLE = 'frota_motorista';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure()
    {
        # $criteria = new Criteria();
        # return $criteria;
    }

    /**
     * Frota_motorista pertence a Frota_veiculos
     * @return Frota_veiculos $Frota_veiculos
     */
    function getFrota_veiculos()
    {
        return $this->belongsTo('Frota_veiculos', 'veiculo_id');
    }

    function getRhprofissional()
    {
        return $this->belongsTo('Rhprofissional', 'rhprofissional_id');
    }
    /**
     * Frota_motorista pertence a Frota_localizacao
     * @return Frota_localizacao $Frota_lozalicao
     */
    function getFrota_localizacao()
    {
        return $this->belongsTo('Frota_localizacao', 'localizacao_id');
    }

    /**
     * Frota_motorista pertence a Frota_situacao_habilitacao
     * @return Frota_situacao_habilitacao $Frota_situacao_habilitacao
     */
    function getFrota_situacao_habilitacao()
    {
        return $this->belongsTo('Frota_situacao_habilitacao', 'situacao_habilitacao_id');
    }


    public static function getMotoristasVencimentoCNH()
    {

        $query = "SELECT DATEDIFF(fm.validade_cnh, CURDATE()) AS dias, fm.`nome`,
                  fm.`validade_cnh`, fv.`placa` FROM frota_motorista fm LEFT JOIN frota_veiculos fv 
                  ON fv.`id` = fm.`veiculo_id` WHERE DATEDIFF(fm.validade_cnh, CURDATE()) <= 60 
                  AND DATEDIFF(fm.validade_cnh, CURDATE()) >= 0 AND fm.`status` = 1
                  AND fm.status = 1";

        $db = new MysqlDB();
        $db->query($query);
        $db->execute();
        return $db->getResults();

    }

}