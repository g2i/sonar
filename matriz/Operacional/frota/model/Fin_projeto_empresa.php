<?php
final class Fin_projeto_empresa extends Record{ 

    const TABLE = 'fin_projeto_empresa';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Fin_projeto_empresa pertence a Fin_projeto_grupo
    * @return Fin_projeto_grupo $Fin_projeto_grupo
    */
    function getFin_projeto_grupo() {
        return $this->belongsTo('Fin_projeto_grupo','fin_projeto_grupo_id');
    }
    
    /**
    * Fin_projeto_empresa pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Fin_projeto_empresa pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','user_created');
    }
    
    /**
    * Fin_projeto_empresa pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','user_modified');
    }
    
    /**
    * Fin_projeto_empresa possui Fin_projeto_unidades
    * @return array de Fin_projeto_unidades
    */
    function getFin_projeto_unidades($criteria=NULL) {
        return $this->hasMany('Fin_projeto_unidade','fin_projeto_empresa_id',$criteria);
    }
}