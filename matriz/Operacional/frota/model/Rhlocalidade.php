<?php
final class Rhlocalidade extends Record{ 

    const TABLE = 'rhlocalidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rhlocalidade possui Frota_inspecaos
    * @return array de Frota_inspecaos
    */
    function getFrota_inspecaos($criteria=NULL) {
        return $this->hasMany('Frota_inspecao','localidade_id',$criteria);
    }
    
    /**
    * Rhlocalidade possui Imobilizado_bens
    * @return array de Imobilizado_bens
    */
    function getImobilizado_bens($criteria=NULL) {
        return $this->hasMany('Imobilizado_bem','localidade_id',$criteria);
    }
    
    /**
    * Rhlocalidade possui Imobilizado_movimentos
    * @return array de Imobilizado_movimentos
    */
    function getImobilizado_movimentos($criteria=NULL) {
        return $this->hasMany('Imobilizado_movimento','localidade_id',$criteria);
    }
    
    /**
    * Rhlocalidade pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Rhlocalidade possui Rhusuario_localidades
    * @return array de Rhusuario_localidades
    */
    function getRhusuario_localidades($criteria=NULL) {
        return $this->hasMany('Rhusuario_localidade','rhlocalidade_id',$criteria);
    }
}