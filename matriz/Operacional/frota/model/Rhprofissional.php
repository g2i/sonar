<?php
final class Rhprofissional extends Record{ 

    const TABLE = 'rhprofissional';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rhprofissional possui Anexoprofissionais
    * @return array de Anexoprofissionais
    */
    function getAnexoprofissionais($criteria=NULL) {
        return $this->hasMany('Anexoprofissional','codigo_profissional',$criteria);
    }
    
    /**
    * Rhprofissional possui C_complemento_movs
    * @return array de C_complemento_movs
    */
    function getC_complemento_movs($criteria=NULL) {
        return $this->hasMany('C_complemento_mov','colaborador',$criteria);
    }
    
    /**
    * Rhprofissional possui Estq_movs
    * @return array de Estq_movs
    */
    function getEstq_movs($criteria=NULL) {
        return $this->hasMany('Estq_mov','colaborador',$criteria);
    }
    
    /**
    * Rhprofissional possui Frota_acidentes
    * @return array de Frota_acidentes
    */
    function getFrota_acidentes($criteria=NULL) {
        return $this->hasMany('Frota_acidente','profissional_id',$criteria);
    }
    
    /**
    * Rhprofissional possui Rhocorrencias
    * @return array de Rhocorrencias
    */
    function getRhocorrencias($criteria=NULL) {
        return $this->hasMany('Rhocorrencia','codigo_profissional',$criteria);
    }
    
    /**
    * Rhprofissional possui Rhprofissional_contratacaos
    * @return array de Rhprofissional_contratacaos
    */
    function getRhprofissional_contratacaos($criteria=NULL) {
        return $this->hasMany('Rhprofissional_contratacao','codigo_profissional',$criteria);
    }
    
    /**
    * Rhprofissional possui Rhprofissional_dependentes
    * @return array de Rhprofissional_dependentes
    */
    function getRhprofissional_dependentes($criteria=NULL) {
        return $this->hasMany('Rhprofissional_dependente','codigo_profissional',$criteria);
    }
}