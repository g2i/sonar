<?php
final class Frota_tipo_manutencao extends Record{ 

    const TABLE = 'frota_tipo_manutencao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_tipo_manutencao possui Frota_manutencoes
    * @return array de Frota_manutencoes
    */
    function getFrota_manutencoes($criteria=NULL) {
        return $this->hasMany('Frota_manutencoes','tipo_manutencao_id',$criteria);
    }
    
    /**
    * Frota_tipo_manutencao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }

      /**
    * Frota_tipo_manutencao pertence a Frota_manutencao_classificacao
    * @return Usuario $Frota_manutencao_classificacao
    */
    function getManutencao_classificacao() {
        return $this->belongsTo('Frota_manutencao_classificacao','manutencao_classificacao_id');
    }
}