<?php
final class Situacao extends Record{ 

    const TABLE = 'situacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Situacao possui Frota_veiculos
    * @return array de Frota_veiculos
    */
    function getFrota_veiculos($criteria=NULL) {
        return $this->hasMany('Frota_veiculos','situacao_id',$criteria);
    }
}