<?php
final class Frota_tipo_acidente extends Record{ 

    const TABLE = 'frota_tipo_acidente';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Frota_tipo_acidente possui Frota_acidentes
    * @return array de Frota_acidentes
    */
    function getFrota_acidentes($criteria=NULL) {
        return $this->hasMany('Frota_acidentes','tipo_acidente_id',$criteria);
    }
    
    /**
    * Frota_tipo_acidente pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastradopor');
    }
    
    /**
    * Frota_tipo_acidente pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificadopor');
    }
    
    /**
    * Frota_tipo_acidente pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}