<?php

final class Frota_abastecimentosController extends AppController
{

    # página inicial do módulo Frota_abastecimentos
    function index()
    {
        $this->setTitle('Abastecimentos');
        $p = new Paginate('Frota_abastecimentos', 10);
        $a = new Criteria();

        //Consulta de veiculos
        $a->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_abastecimentos.veiculo_id', 'INNER');

        if (!empty($_POST['veiculo_id'])) {
            $a->addCondition('veiculo_id', '=', $_POST['veiculo_id']);

        }

        if (!empty($_POST['inicio'])) {
            $a->addCondition('dt_abastecimento', '>=', $_POST['inicio']);


        }
        if (!empty($_POST['fim'])) {
            $a->addCondition('dt_abastecimento', '<=', $_POST['fim']);

        }

        if (!empty($_POST['localizacao_id'])) {
            $a->addCondition('frota_veiculos.localizacao_id', '=', $_POST['localizacao_id']);

        }

        $this->set('veiculos', Frota_veiculos::getList());
        $this->set('Frota_localizacao', Frota_localizacao::getList());

        $this->set('abastecimentos', $p->getPage($a));
        $this->set('nav', $p->getNav());

    }

    # lista de Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/all.php
    function all()
    {
        $this->setTitle('Listagem de Abastecimentos');
        $p = new Paginate('Frota_abastecimentos', 10);
        $c = new Criteria();
        //Consulta de veículos
        $c->addJoin('frota_veiculos', 'frota_veiculos.id', 'frota_abastecimentos.veiculo_id', 'INNER');
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {

                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        //Filtro por localização do veículo
        if(!empty($_POST['localizacao_id'])) {
            $c->addCondition('frota_veiculos.localizacao_id' , '=' , $_POST['localizacao_id']);
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Frota_abastecimentos', $p->getPage($c));
        $this->set('nav', $p->getNav());


        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());

        $this->set('Frota_localizacao', Frota_localizacao::getList());


    }

    # visualiza um(a) Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/view.php
    function view()
    {
        $this->setTitle('Abastecimentos');
        try {
            $this->set('Frota_abastecimentos', new Frota_abastecimentos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_abastecimentos', 'all');
        }
    }

    # formulário de cadastro de Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/add.php
    function add()
    {
        $this->setTitle('Cadastro de Abastecimentos');
        $this->set('Frota_abastecimentos', new Frota_abastecimentos);
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Usuarios', Usuario::getList());
        $this->set('Fornecedores', Fornecedor::getList());
    }

    # recebe os dados enviados via post do cadastro de Frota_abastecimentos
    # (true)redireciona ou (false) renderiza a visão /view/Frota_abastecimentos/add.php
    function post_add()
    {
        $this->setTitle('Cadastro de Abastecimentos');
        $Frota_abastecimentos = new Frota_abastecimentos();
        $this->set('Frota_abastecimentos', $Frota_abastecimentos);
        $veiculo = new Frota_veiculos((int)$_POST['veiculo_id']);
        $c = new Criteria();
        $c->addCondition('veiculo_id', '=', (int)$_POST['veiculo_id']);
        if (empty($veiculo->getFrota_abastecimentos($c))) {
            $_POST['media_consumo'] = 0;
        } else {
            $_POST['media_consumo'] = ($_POST['quilometragem'] - $veiculo->quilometragem_atual) / $_POST['quantidade'];
        }
        $_POST['usuario_id'] = Session::get('user')->id;

        try {
            if ($Frota_abastecimentos->save($_POST)) {
                $veiculo->quilometragem_atual = $Frota_abastecimentos->quilometragem;
                $veiculo->save();
            }
            new Msg(__('Abastecimento cadastrado com sucesso'));
            $this->go('Frota_abastecimentos', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Fornecedores', Fornecedor::getList());
    }

    # formulário de edição de Frota_abastecimentos
    # renderiza a visão /view/Frota_abastecimentos/edit.php
    function edit()
    {
        $this->setTitle('Editar Abastecimento');
        try {
            $this->set('Frota_abastecimentos', new Frota_abastecimentos((int)$this->getParam('id')));
            $this->set('Frota_veiculos', Frota_veiculos::getList());
            $this->set('Usuarios', Usuario::getList());
            $this->set('Fornecedores', Fornecedor::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Frota_abastecimentos', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Frota_abastecimentos
    # (true)redireciona ou (false) renderiza a visão /view/Frota_abastecimentos/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Abastecimento');
        try {
            $Frota_abastecimentos = new Frota_abastecimentos((int)$_POST['id']);
            $this->set('Frota_abastecimentos', $Frota_abastecimentos);
            $_POST['usuario_id'] = Session::get('user')->id;
            $Frota_abastecimentos->save($_POST);
            new Msg(__('Abastecimento atualizado com sucesso'));
            $this->go('Frota_abastecimentos', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Frota_veiculos', Frota_veiculos::getList());
        $this->set('Fornecedores', Fornecedor::getList());
    }

    # Confirma a exclusão ou não de um(a) Frota_abastecimentos
    # renderiza a /view/Frota_abastecimentos/delete.php
    function delete()
    {
        $this->setTitle('Apagar Frota_abastecimentos');
        try {
            $this->set('Frota_abastecimentos', new Frota_abastecimentos((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Frota_abastecimentos', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Frota_abastecimentos
    # redireciona para Frota_abastecimentos/all
    function post_delete()
    {
        try {
            $Frota_abastecimentos = new Frota_abastecimentos((int)$_POST['id']);
            $Frota_abastecimentos->delete();
            new Msg(__('Abastecimento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Frota_abastecimentos', 'all');
    }

    function getQuilometragem()
    {
        $veiculo = new Frota_veiculos((int)$_POST['veiculo']);
        echo $veiculo->quilometragem_atual;
        exit;
    }
}