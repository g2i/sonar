<?php require_once 'dataUtil.php' ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Consumo Médio</h2>
        <ol class="breadcrumb">
            <li>Consumo Médio</li>
            <li class="active">
                <strong>Média de Consumo dos Veículos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <!-- formulario de pesquisa -->
                <div class="filtros well">
                    <div class="form">

                        <form role="form" method="post" action="<?php echo $this->Html->getUrl('Frota_abastecimentos', 'index') ?>">

                            <h4 style="padding-top: 5px;">Período de Abastecimento:</h4>

                            <div class="col-md-3 form-group">
                                <label for="data_inicial">Inicio</label>
                                <input type="date" name="inicio" id="inicio"
                                       class="form-control maskData" value="<?= $inicio ?>">
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="data_final">Fim</label>
                                <input type="date" name="fim" id="fim"
                                       class="form-control maskData" value="<?= $fim ?>">
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="id_veiculo">Veículo</label>
                                <select name="id_veiculo" class="form-control" id="id_veiculo">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($veiculos as $v):
                                        echo '<option value="' . $v->id . '">' . $v->placa . '</option>';
                                    endforeach; ?>
                                </select>
                            </div>

                            <div class="col-md-3 form-group">
                                <label for="localizacao_id">Localização</label>
                                <select name="localizacao_id" class="form-control"
                                        id="localizacao_id">
                                    <?php echo '<option value="">Selecione:</option>'; ?>
                                    <?php foreach ($Frota_localizacao as $f): ?>
                                        <?php echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>'; ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-default"><span
                                        class="glyphicon glyphicon-search" id="btn-filtro"></span></button>
                                <button type="button" id="btnReset" class="btn btn-default botao-reset"><span
                                        class="glyphicon glyphicon-refresh"></span></button>
                            </div>
                            <div class="clearfix"></div>
                        </form>


                    </div>
                </div>
                <!-- tabela de resultados -->
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Frota_veiculos', 'all', array('orderBy' => 'id')); ?>'>
                                        Veiculo / Placa
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all', array('orderBy' => 'descricao')); ?>'>
                                        Data do Abastecimento
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                                        Quilometragem no abastecimento
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                        Quantidade (Litros)
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                        Preço por litro
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                        Valor do abastecimento
                                    </a>
                                </th>
                                <th>
                                    <a href='<?php echo $this->Html->getUrl('Frota_tipo_manutencao', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                        Média de Consumo
                                    </a>
                                </th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>

                            <?php
                            $flag = true;
                            foreach ($abastecimentos as $a) {
                                echo '<tr>';
                                echo '<td>';
                                echo $this->Html->getLink($a->getFrota_veiculos()->placa, 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink(convertDataSQL4BR($a->dt_abastecimento), 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink($a->quilometragem, 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink($a->quantidade, 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink('R$ ' . sprintf('%0.3f', $a->valor_litro), 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink('R$ ' . sprintf('%0.2f', $a->quantidade * $a->valor_litro), 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id),
                                    array('data-toggle' => 'modal'));
                                echo '</td>';
                                echo '<td>';
                                echo $this->Html->getLink($a->media_consumo . ' km/l ', 'Frota_abastecimentos', 'view',
                                    array('id' => $a->id), // variaveis via GET opcionais
                                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                                echo '</td>';
                                echo '</tr>';
                            }
                            ?>

                        </table>

                        <!-- menu de paginação -->
                        <div style="text-align:center"><?php echo $nav; ?></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>