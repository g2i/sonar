<?php include_once "dataUtil.php"; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Manutenções</h2>
        <ol class="breadcrumb">
            <li>Manutenções</li>
            <li class="active">
                <strong>Todas</strong>
            </li></ol></div></div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="veiculo_id">Veículo</label>
                                    <select name="filtro[externo][veiculo_id]" class="form-control" id="veiculo_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Frota_veiculos as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->placa . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tipo_manutencao_id">Tipo de Manutenção</label>
                                    <select name="filtro[externo][tipo_manutencao_id]" class="form-control" id="tipo_manutencao_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Frota_tipo_manutencaos as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->descricao . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="col-md-3 form-group">
                                    <label for="dt_manutencao">Data da Manutenção</label>
                                    <input type="date" name="filtro[interno][dt_manutencao]" id="dt_manutencao" class="form-control maskData" value="<?php echo $this->getParam('dt_manutencao'); ?>">
                                </div>

                                <div class="col-md-3 form-group">
                                    <label for="localizacao_id">Localização</label>
                                    <select name="localizacao_id" class="form-control"
                                            id="localizacao_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_localizacao as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                                    <button type="button" id="btnReset" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Manutenção', 'Frota_manutencoes', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                            Cadastrado por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'veiculo_id')); ?>'>
                                            Veículo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'fornecedor_id')); ?>'>
                                            Fornecedor
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'tipo_manutencao_id')); ?>'>
                                            Tipo de Manutenção
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'valor')); ?>'>
                                            Valor da Manutenção
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'garantia')); ?>'>
                                            Garantia
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'dt_manutencao')); ?>'>
                                            Data da Manutenção
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'observacao')); ?>'>
                                            Observação
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => 'quilometragem')); ?>'>
                                            Quilometragem (Troca de Óleo / Pneus)
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_manutencoes as $f) {

                                    if(($f->getFrota_veiculos()->quilometragem_atual - $f->quilometragem) >= $f->vida_util || ($f->getFrota_veiculos()->quilometragem_atual - $f->quilometragem) >= ($f->vida_util - 200)) {
                                        echo '<tr bgcolor="#FFCDD2">';
                                    } else {
                                        echo '<tr>';
                                    }
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getUsuario()->login, 'Usuario', 'view',
                                        array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
                                        array('id' => $f->getFrota_veiculos()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFornecedor()->nome, 'Fornecedor', 'view',
                                        array('id' => $f->getFornecedor()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_tipo_manutencao()->descricao, 'Frota_tipo_manutencao', 'view',
                                        array('id' => $f->getFrota_tipo_manutencao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink('R$ ' . sprintf('%0.2f', $f->valor), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(convertDataSQL4BR($f->garantia), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(convertDataSQL4BR($f->dt_manutencao), 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->observacao, 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->quilometragem, 'Frota_manutencoes', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_manutencoes', 'edit',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_manutencoes', 'delete',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function() {
                            $('#search').keyup(function() {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Frota_manutencoes', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Frota_manutencoes', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function() {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>