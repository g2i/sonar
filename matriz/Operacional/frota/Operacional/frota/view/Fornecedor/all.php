<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fornecedor</h2>
        <ol class="breadcrumb">
            <li>Fornecedor</li>
            <li class="active">
                <strong>Todos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" name="filtro[interno][nome]" id="nome" class="form-control"
                                           value="<?php echo $this->getParam('nome'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="cidade">Cidade</label>
                                    <input type="text" name="filtro[interno][cidade]" id="cidade" class="form-control"
                                           value="<?php echo $this->getParam('cidade'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="uf">Estado</label>
                                    <input type="text" name="filtro[interno][uf]" id="uf" class="form-control"
                                           value="<?php echo $this->getParam('uf'); ?>">
                                </div>
                                <div class="col-md-12 text-right">
                                    <!--                                    <button type="button" class="btn btn-default botao-impressao"><span-->
                                    <!--                                            class="glyphicon glyphicon-print"></span></button>-->
                                    <button type="button" id="btnReset" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default" id="btn-filtro"><span
                                            class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo fornecedor', 'Fornecedor', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Fornecedor', 'all', array('orderBy' => 'nome')); ?>'>
                                            Nome
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Fornecedor', 'all', array('orderBy' => 'cidade')); ?>'>
                                            Cidade
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Fornecedor', 'all', array('orderBy' => 'uf')); ?>'>
                                            Estado
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Fornecedor', 'all', array('orderBy' => 'numero')); ?>'>
                                            Número
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Fornecedor', 'all', array('orderBy' => 'rua')); ?>'>
                                            Rua
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Fornecedor', 'all', array('orderBy' => 'bairro')); ?>'>
                                            Bairro
                                        </a>
                                    </th>

                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Fornecedores as $f) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->nome, 'Fornecedor', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->cidade, 'Fornecedor', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->uf, 'Fornecedor', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->numero, 'Fornecedor', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->rua, 'Fornecedor', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->bairro, 'Fornecedor', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
//                echo '<td width="50">';
//                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Fornecedor', 'edit',
//                    array('id' => $f->id),
//                    array('class' => 'btn btn-warning btn-sm'));
//                echo '</td>';
//                echo '<td width="50">';
//                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Fornecedor', 'delete',
//                    array('id' => $f->id),
//                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
//                echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Fornecedor', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Fornecedor', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>