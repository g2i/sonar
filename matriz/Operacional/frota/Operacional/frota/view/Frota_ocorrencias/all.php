<?php include_once "dataUtil.php"; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Ocorrências</h2>
        <ol class="breadcrumb">
            <li>Ocorrências</li>
            <li class="active">
                <strong>Todas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                  method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="veiculo_id">Veículo</label>
                                    <select name="filtro[externo][veiculo_id]" class="form-control" id="veiculo_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_veiculos as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->placa . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tipo_ocorrencia_id">Tipo de Ocorrência</label>
                                    <select name="filtro[externo][tipo_ocorrencia_id]" class="form-control"
                                            id="tipo_ocorrencia_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_tipo_ocorrencias as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->descricao . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="dt_ocorrencia">Data da Ocorrência</label>
                                    <input type="date" name="filtro[interno][dt_ocorrencia]" id="dt_ocorrencia"
                                           class="form-control maskData"
                                           value="<?php echo $this->getParam('dt_ocorrencia'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="localizacao_id">Localização</label>
                                    <select name="localizacao_id" class="form-control"
                                            id="localizacao_id">
                                        <?php echo '<option value="">Selecione:</option>'; ?>
                                        <?php foreach ($Frota_localizacao as $f): ?>
                                            <?php echo '<option value="' . $f->id . '">' . $f->cidade . ' - ' . $f->estado . '</option>'; ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-impressao"><span
                                            class="glyphicon glyphicon-print"></span></button>
                                    <button type="button" id="btnReset" class="btn btn-default botao-reset"><span
                                            class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span
                                            class="glyphicon glyphicon-search" id="btn-filtro"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Ocorrência', 'Frota_ocorrencias', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_ocorrencias', 'all', array('orderBy' => 'veiculo_id')); ?>'>
                                            Veículo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_ocorrencias', 'all', array('orderBy' => 'usuario_id')); ?>'>
                                            Cadastrado por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_ocorrencias', 'all', array('orderBy' => 'tipo_ocorrencia_id')); ?>'>
                                            Tipo de Ocorrência
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_ocorrencias', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                                            Cadastrado em
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_ocorrencias', 'all', array('orderBy' => 'dt_ocorrencia')); ?>'>
                                            Data da Ocorrência
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Frota_ocorrencias', 'all', array('orderBy' => 'descricao')); ?>'>
                                            Descrição
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Frota_ocorrencias as $f) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_veiculos()->placa, 'Frota_veiculos', 'view',
                                        array('id' => $f->getFrota_veiculos()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getUsuario()->nome, 'Usuario', 'view',
                                        array('id' => $f->getUsuario()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->getFrota_tipo_ocorrencia()->descricao, 'Frota_tipo_ocorrencia', 'view',
                                        array('id' => $f->getFrota_tipo_ocorrencia()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(convertDataSQL4BR($f->dt_cadastro), 'Frota_ocorrencias', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink(convertDataSQL4BR($f->dt_ocorrencia), 'Frota_ocorrencias', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($f->descricao, 'Frota_ocorrencias', 'view',
                                        array('id' => $f->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Frota_ocorrencias', 'edit',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Frota_ocorrencias', 'delete',
                                        array('id' => $f->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Frota_ocorrencias', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Frota_ocorrencias', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#btnReset').click(function () {
        window.location.href = window.location.href;
    });
</script>