<?php
final class DeducoesContasreceberController extends AppController{ 

    # página inicial do módulo DeducoesContasreceber
    function index(){
        $this->setTitle('DeducoesContasreceber');
    }

    # lista de DeducoesContasreceberes
    # renderiza a visão /view/DeducoesContasreceber/all.php
    function all(){
        $this->setTitle('DeducoesContasreceberes');
        $p = new Paginate('DeducoesContasreceber', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (isset($_GET['search'])) {
            $c->addCondition('percentual', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('DeducoesContasreceberes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) DeducoesContasreceber
    # renderiza a visão /view/DeducoesContasreceber/view.php
    function view(){
        try {
            $this->set('DeducoesContasreceber', new DeducoesContasreceber((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('DeducoesContasreceber', 'all');
        }
    }

    # formulário de cadastro de DeducoesContasreceber
    # renderiza a visão /view/DeducoesContasreceber/add.php
    function add(){
        $this->setTitle('Adicionar DeducoesContasreceber');
        $this->set('DeducoesContasreceber', new DeducoesContasreceber);
        $this->set('Deducoes',  Deducoes::getList());
        $this->set('Contasreceberes',  Contasreceber::getList());
    }

    # recebe os dados enviados via post do cadastro de DeducoesContasreceber
    # (true)redireciona ou (false) renderiza a visão /view/DeducoesContasreceber/add.php
    function post_add(){
        $this->setTitle('Adicionar DeducoesContasreceber');
        $DeducoesContasreceber = new DeducoesContasreceber();
        $this->set('DeducoesContasreceber', $DeducoesContasreceber);
        try {
            $DeducoesContasreceber->save($_POST);
            new Msg(__('DeducoesContasreceber cadastrado com sucesso'));
            $this->go('DeducoesContasreceber', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Deducoes',  Deducoes::getList());
        $this->set('Contasreceberes',  Contasreceber::getList());
    }

    # formulário de edição de DeducoesContasreceber
    # renderiza a visão /view/DeducoesContasreceber/edit.php
    function edit(){
        $this->setTitle('Editar DeducoesContasreceber');
        try {
            $this->set('DeducoesContasreceber', new DeducoesContasreceber((int) $this->getParam('id')));
            $this->set('Deducoes',  Deducoes::getList());
            $this->set('Contasreceberes',  Contasreceber::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('DeducoesContasreceber', 'all');
        }
    }

    # recebe os dados enviados via post da edição de DeducoesContasreceber
    # (true)redireciona ou (false) renderiza a visão /view/DeducoesContasreceber/edit.php
    function post_edit(){
        $this->setTitle('Editar DeducoesContasreceber');
        try {
            $DeducoesContasreceber = new DeducoesContasreceber((int) $_POST['id']);
            $this->set('DeducoesContasreceber', $DeducoesContasreceber);
            $DeducoesContasreceber->save($_POST);
            new Msg(__('DeducoesContasreceber atualizado com sucesso'));
            $this->go('DeducoesContasreceber', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Deducoes',  Deducoes::getList());
        $this->set('Contasreceberes',  Contasreceber::getList());
    }

    # Confirma a exclusão ou não de um(a) DeducoesContasreceber
    # renderiza a /view/DeducoesContasreceber/delete.php
    function delete(){
        $this->setTitle('Apagar DeducoesContasreceber');
        try {
            $this->set('DeducoesContasreceber', new DeducoesContasreceber((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('DeducoesContasreceber', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) DeducoesContasreceber
    # redireciona para DeducoesContasreceber/all
    function post_delete(){
        try {
            $DeducoesContasreceber = new DeducoesContasreceber((int) $_POST['id']);
            $DeducoesContasreceber->delete();
            new Msg(__('DeducoesContasreceber apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('DeducoesContasreceber', 'all');
    }

}