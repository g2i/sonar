<?php
final class Rateio_projetos_planos_contasController extends AppController{ 

    # página inicial do módulo Rateio_projetos_planos_contas
    function index(){
        $this->setTitle('Visualização de Rateio_projetos_planos_contas');
    }

    # lista de Rateio_projetos_planos_contas
    # renderiza a visão /view/Rateio_projetos_planos_contas/all.php
    function all(){
        $this->setTitle('Listagem de Rateio_projetos_planos_contas');
        $p = new Paginate('Rateio_projetos_planos_contas', 10);
        $c = new Criteria();
        $c->addCondition('situacao','=',1);

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $criteriaPlano = new Criteria();
        $criteriaPlano->addCondition("id", "=", $this->getParam('id_plano'));

        $this->set("PlanoContas", Planocontas::getFirst($criteriaPlano));
        $this->set('Rateio_projetos_planos_contas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rateio_projetos_planos_contas
    # renderiza a visão /view/Rateio_projetos_planos_contas/view.php
    function view(){
        $this->setTitle('Visualização de Rateio projetos');
        try {
            $this->set('Rateio_projetos_planos_contas', new Rateio_projetos_planos_contas((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rateio_projetos_planos_contas', 'all');
        }
    }

    # formulário de cadastro de Rateio_projetos_planos_contas
    # renderiza a visão /view/Rateio_projetos_planos_contas/add.php
    function add(){
        $this->setTitle('Cadastro de Rateio_projetos_planos_contas');
        $this->set('Rateio_projetos_planos_contas', new Rateio_projetos_planos_contas);
        $criteriaPlano = new Criteria();
        $criteriaPlano->addCondition("id", "=", $this->getParam('id_plano'));

        $criteriaGrupo = new Criteria();
        $criteriaGrupo->addCondition("situacao_id", "=", 1);
        $this->set("Fin_projeto_grupo", Fin_projeto_grupo::getList($criteriaGrupo));
        $this->set("PlanoContas", Planocontas::getFirst($criteriaPlano));
    }

    # recebe os dados enviados via post do cadastro de Rateio_projetos_planos_contas
    # (true)redireciona ou (false) renderiza a visão /view/Rateio_projetos_planos_contas/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rateio_projetos_planos_contas');
        $Rateio_projetos_planos_contas = new Rateio_projetos_planos_contas();
        $this->set('Rateio_projetos_planos_contas', $Rateio_projetos_planos_contas);
        try {
            $user = Session::get('user');
            $Rateio_projetos_planos_contas->situacao = 1;
            $Rateio_projetos_planos_contas->created = date('Y-m-d H:i');
            $Rateio_projetos_planos_contas->user_created = $user->id;
            $Rateio_projetos_planos_contas->save($_POST);

            new Msg(__('Rateio projetos cadastrado com sucesso'));
            $this->go('Rateio_projetos_planos_contas', 'all',array('id_plano' => $Rateio_projetos_planos_contas->id_plano_contas));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Planocontas',  Planocontas::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Fin_projeto_unidades',  Fin_projeto_unidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Rateio_projetos_planos_contas
    # renderiza a visão /view/Rateio_projetos_planos_contas/edit.php
    function edit(){
        $this->setTitle('Edição de Rateio projetos');
        try {
            $rateio = new Rateio_projetos_planos_contas((int) $this->getParam('id'));
            $sql =  "Select * From view_grupo_empresa_unidade WHERE unidade_id = :unidade";
            $db = $this::getConn();
            $db->query($sql);
            $db->bind(":unidade",$rateio->id_unidade);
            $dados = $db->getResults();
            $dados = $dados[0];

            $criteriaPlano = new Criteria();
            $criteriaPlano->addCondition("id", "=", $rateio->id_plano_contas);

            $criteriaGrupo = new Criteria();
            $criteriaGrupo->addCondition("situacao_id", "=", 1);
            $this->set("Fin_projeto_grupo", Fin_projeto_grupo::getList($criteriaGrupo));

            $criteriaEmpresa = new Criteria();
            $criteriaEmpresa->addCondition("situacao_id", "=", 1);
            $criteriaEmpresa->addCondition("fin_projeto_grupo_id", "=", $dados->grupo_id);
            $this->set("Fin_projeto_empresa", Fin_projeto_empresa::getList($criteriaEmpresa));

            $criteriaUnidade = new Criteria();
            $criteriaUnidade->addCondition("situacao_id", "=", 1);
            $criteriaUnidade->addCondition("fin_projeto_empresa_id", "=", $dados->empresa_id);

            $this->set("PlanoContas", Planocontas::getFirst($criteriaPlano));
            $this->set("Fin_projeto_unidade", Fin_projeto_unidade::getList($criteriaUnidade));
            $this->set('Rateio_projetos_planos_contas', $rateio);
            $this->set("Fin_projeto", $dados);

        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rateio_projetos_planos_contas', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rateio_projetos_planos_contas
    # (true)redireciona ou (false) renderiza a visão /view/Rateio_projetos_planos_contas/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rateio_projetos_planos_contas');
        try {
            $user = Session::get('user');
            $Rateio_projetos_planos_contas = new Rateio_projetos_planos_contas((int) $_POST['id']);
            $Rateio_projetos_planos_contas->modified = date('Y-m-d H:i');
            $Rateio_projetos_planos_contas->user_modified = $user->id;
            $this->set('Rateio_projetos_planos_contas', $Rateio_projetos_planos_contas);
            $Rateio_projetos_planos_contas->save($_POST);

            new Msg(__('Rateio projetos atualizado com sucesso'));
            $this->go('Rateio_projetos_planos_contas', 'all',array('id_plano' => $Rateio_projetos_planos_contas->id_plano_contas));
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Planocontas',  Planocontas::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Fin_projeto_unidades',  Fin_projeto_unidade::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Rateio_projetos_planos_contas
    # renderiza a /view/Rateio_projetos_planos_contas/delete.php
    function delete(){
        $this->setTitle('Apagar Rateio_projetos_planos_contas');
        try {
            $this->set('Rateio_projetos_planos_contas', new Rateio_projetos_planos_contas((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rateio_projetos_planos_contas', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rateio_projetos_planos_contas
    # redireciona para Rateio_projetos_planos_contas/all
    function post_delete(){
        try {
            $Rateio_projetos_planos_contas = new Rateio_projetos_planos_contas((int) $_POST['id']);
            $Rateio_projetos_planos_contas->situacao = 3;
            $Rateio_projetos_planos_contas->save();
            new Msg(__('Rateio projetos apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rateio_projetos_planos_contas', 'all',array('id_plano'=>$Rateio_projetos_planos_contas->id_plano_contas));
    }

}