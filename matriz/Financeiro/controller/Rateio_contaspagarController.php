<?php

final class Rateio_contaspagarController extends AppController
{

    # página inicial do módulo Rateio_contaspagar
    function index()
    {
        $this->setTitle('Rateio_contaspagar');
    }

    # lista de Rateio_contaspagares
    # renderiza a visão /view/Rateio_contaspagar/all.php
    function all()
    {
        $this->setTitle('Rateio_contaspagares');
        $conta = new Contaspagar((int)$this->getParam("contar_pagar"));
        $this->set('contaId', $conta->id);
        $this->set('valor', $conta->valor);
    }

    # visualiza um(a) Rateio_contaspagar
    # renderiza a visão /view/Rateio_contaspagar/view.php
    function view()
    {
        try {
            $this->set('Rateio_contaspagar', new Rateio_contaspagar((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rateio_contaspagar', 'all');
        }
    }

    # formulário de cadastro de Rateio_contaspagar
    # renderiza a visão /view/Rateio_contaspagar/add.php
    function add()
    {
        $this->setTitle('Adicionar Rateio');
        $this->set('Contaspagares', new Contaspagar($this->getParam('id')));
        $this->set('Contabilidades', Contabilidade::getList());
        $this->set('Planocontas', Planocontas::getList());
        $this->set('Fornecedores', Fornecedor::getList());
        $this->set('Centro_custo', Centro_custo::getList());
    }

    function getRateios()
    {
        $ret = array();
        $ret['rateios'] = array();

        try {

            $sql = "SELECT r.id, r.centro_custo AS centroId, r.data_documento, r.plano_contas_id, r.fornecedor_id,  r.contar_pagar AS conta,
                    ct.descricao AS centro, e.id AS empresaId, e.nome AS empresa,
                    r.observacao, r.valor, 'S' AS situacao FROM rateio_contaspagar AS r
                    LEFT JOIN contaspagar AS c ON (c.id = r.contar_pagar)
                    LEFT JOIN centro_custo AS ct ON (ct.id = r.centro_custo)
                    LEFT JOIN contabilidade AS e ON (e.id = r.empresa)
                    WHERE r.contar_pagar = :conta";

            $db = $this::getConn();
            $db->query($sql);
            $db->bind(':conta', $this->getParam('id'));
            $dados = $db->getResults();
            $ret['rateios'] = empty($dados) ? array() : $dados;

        } catch (Exception $e) {
            $ret['error'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function saveRateios()
    {
        $rateios = json_decode(str_replace('&#34;', '"', $_POST['rateios']));
        $ret = array();
        $ret['result'] = false;

        try {

            foreach ($rateios AS $r) {
                switch ($r->situacao) {
                    case 'D':
                        $rateio = new Rateio_contaspagar((int)$r->id);
                        $rateio->delete();
                        break;

                    case 'E':
                        $rateio = new Rateio_contaspagar((int)$r->id);
                        $rateio->contar_pagar = $r->conta;
                        $rateio->valor = $r->valor;
                        $rateio->empresa = $r->empresaId;
                        $rateio->centro_custo = $r->centroId;
                        $rateio->observacao = $r->observacao;
                        $rateio->plano_contas_id = $r->plano_contas_id;
                        $rateio->fornecedor_id = $r->fornecedor_id;
                        $rateio->data_documento = $r->data_documento;
                        $rateio->status = 1;
                        $rateio->save();
                        break;

                    case 'N':
                        $rateio = new Rateio_contaspagar();
                        $rateio->contar_pagar = $r->conta;
                        $rateio->valor = $r->valor;
                        $rateio->empresa = $r->empresaId;
                        $rateio->centro_custo = $r->centroId;
                        $rateio->observacao = $r->observacao;
                        $rateio->plano_contas_id = $r->plano_contas_id;
                        $rateio->fornecedor_id = $r->fornecedor_id;
                        $rateio->data_documento = $r->data_documento;
                        $rateio->status = 1;
                        $rateio->save();
                        break;
                }
            }

            $ret['result'] = true;
            $ret['msg'] = 'Rateios salvo com sucesso !';
        } catch (Exception $e) {
            $ret['result'] = false;
            $ret['msg'] = 'Erro ao salvar rateios !';
            $ret['error'] = $e->getMessage();
            $ret['trace'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }
}