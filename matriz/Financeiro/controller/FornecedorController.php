<?php

final class FornecedorController extends AppController
{
    # página inicial do módulo Fornecedor

    function index()
    {
        $this->setTitle('Credor');
    }

    # lista de Fornecedores
    # renderiza a visão /view/Fornecedor/all.php

    function all()
    {
        $this->setTitle('Credores');
        $p = new Paginate('Fornecedor', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        $c->setOrder("nome");
        if (isset($_GET['search'])) {
            $c->addCondition('nome', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Fornecedores', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Fornecedor
    # renderiza a visão /view/Fornecedor/view.php

    function view()
    {
        try {
            $this->set('Fornecedor', new Fornecedor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Fornecedor', 'all');
        }
    }

    # formulário de cadastro de Fornecedor
    # renderiza a visão /view/Fornecedor/add.php

    function add()
    {
        $this->setTitle('Adicionar Credor');
        $this->set('Fornecedor', new Fornecedor);
        $this->set('Status', Status::getList());

    }

    # recebe os dados enviados via post do cadastro de Fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor/add.php

    function post_add()
    {
        $this->setTitle('Adicionar Credor');
        $Fornecedor = new Fornecedor();
        $this->set('Fornecedor', $Fornecedor);
        try {
            $Fornecedor->save($_POST);
            new Msg(__('Credor cadastrado com sucesso'));
            if (!empty($_POST['origem'])) {
                echo $Fornecedor->id;
                exit;
            } else {
                $this->go('Fornecedor', 'all');
            }
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Status', Status::getList());
    }

    # formulário de edição de Fornecedor
    # renderiza a visão /view/Fornecedor/edit.php

    function edit()
    {
        $this->setTitle('Editar Credor');
        try {
            $this->set('Fornecedor', new Fornecedor((int)$this->getParam('id')));
            $this->set('Status', Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Fornecedor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Fornecedor
    # (true)redireciona ou (false) renderiza a visão /view/Fornecedor/edit.php

    function post_edit()
    {
        $this->setTitle('Editar Credor');
        try {
            $Fornecedor = new Fornecedor((int)$_POST['id']);
            $this->set('Fornecedor', $Fornecedor);
            $Fornecedor->save($_POST);
            new Msg(__('Credor atualizado com sucesso'));
            $this->go('Fornecedor', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status', Status::getList());
    }

    # Recebe o id via post e exclui um(a) Fornecedor
    # redireciona para Fornecedor/all

    function delete()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Fornecedor = new Fornecedor((int)$_POST['id']);
            $Fornecedor->status = 3;
            $Fornecedor->save();
            $ret['result'] = true;
            $ret['msg'] = 'Credor apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir credor !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function buscaCep()
    {

        $cep = $_POST['cep'];

        $a = validaCep($cep);

        echo $a;
        exit();
    }

    function validaCnpj()
    {
        $cnpj = $_POST['cnpj'];

        $a = validaCnpj($cnpj);// retorna true/false
        var_dump($a);
        echo ($a) ? '1' : '0';
        exit();
    }

    function combo()
    {
        $fornecedores = new Fornecedor($_POST['id']);
        $option = "<option value=" . $fornecedores->id . ">" . $fornecedores->nome . "</option>";
        echo $option;
        exit;
    }

    function findCredor()
    {
        try {
            $termo = $this->getParam('termo');
            $size = (int)$this->getParam('size');
            $page = (int)$this->getParam('page');

            if (!isset($termo))
                $termo = '';

            if (!isset($size) || $size < 1)
                $size = 10;

            if (!isset($page) || $page < 1)
                $page = 1;

            $db = $this::getConn();
            $sqlCount = "SELECT count(*) AS count FROM `fornecedor` WHERE status = 1 AND `nome` LIKE :termo";
            $sql = "SELECT `id`,`nome` FROM `fornecedor` WHERE status = 1 AND `nome` LIKE :termo LIMIT :li OFFSET :off";

            $db->query($sqlCount);
            $db->bind(":termo", $termo . "%");
            $db->execute();

            $ret = array();
            $ret["total"] = $db->getRow()->count;
            $ret["dados"] = array();

            $db->query($sql);
            $db->bind(":termo", $termo . "%");
            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);

            $dados = $db->getResults();

            foreach ($dados as $d) {
                $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
            }

            echo json_encode($ret);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }

    function getCredores()
    {

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM fornecedor AS f WHERE f.status <> 3";
            $sql = "SELECT f.id, f.nome, f.celular, f.telefone, f.email, s.descricao AS status
                    FROM fornecedor AS f INNER JOIN status AS s ON (s.id = f.status)
                    WHERE f.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND f.nome LIKE :termo";
                $sql .= " AND f.nome LIKE :termo";
            }

            $sortCount = count($sort);
            if ($sortCount < 1) {
                $sql .= " ORDER BY nome";
                $sort[] = array('nome' => 'asc');
            } else {
                $sql .= " ORDER BY";
                for ($i = 0; $i < $sortCount; $i++) {
                    if ($i > 0)
                        $sql .= ", ";

                    $sql .= " " . $sort[$i][0] . " " . $sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search . "%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search . "%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
            //$ret['sql'] = $sql;

            echo json_encode($ret);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        exit;
    }

    function unir()
    {
        $de = $_GET['de'];
        $para = $_GET['para'];
        $db = $this::getConn();

        $sql = "UPDATE contaspagar SET idFornecedor = :para WHERE idFornecedor = :de;
                UPDATE estq_mov SET fornecedor = :para WHERE fornecedor = :de;
                UPDATE frota_abastecimentos SET fornecedor_id = :para WHERE fornecedor_id = :de;
                UPDATE frota_manutencoes SET fornecedor_id = :para WHERE fornecedor_id = :de;
                UPDATE programacao SET idFornecedor = :para WHERE idFornecedor = :de;
                DELETE FROM fornecedor WHERE id = :de;";

        $db->query($sql);

        $db->bind(":para", $para, PDO::PARAM_INT);
        $db->bind(":de", $de, PDO::PARAM_INT);

        $db->execute();
        exit;
    }
}
