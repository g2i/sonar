<?php
final class ReportController extends AppController{

    public function index(){
        $report = $_GET['report'];
        $this->set('report', $report);
        switch($report){
            case "cr_g":
                break;

            case "cp_g":
                break;

            case "cp_new":
                break;

            case "cr_new":
                break;

            case "pr_r":
                break;

            case "cr_c":
                break;

            case "cp_q":
                break;

            case "movi":
                $this->setTitle('Relat&oacute;rio de Movimenta&ccedil;&otilde;es');
                $this->set('relatorio', 'Relat&oacute;rio de Movimenta&ccedil;&otilde;es');
                break;

            case "cp_prog":
                break;

            case "analit":
                break;

            case "sint":
                break;

            case "d_analitico":
                break;

            case "d_sint":
                break;

            case "movi_geral":
                break;
        }
    }

    public function getReport(){
        $report = $_GET['id'];
        $file = '';
        switch($report){
            case "cr_g":
                break;

            case "cp_g":
                break;

            case "cp_new":
                break;

            case "cr_new":
                break;

            case "pr_r":
                break;

            case "cr_c":
                break;

            case "cp_q":
                break;

            case "movi":
                $file = 'Relatorios/movimentacao.mrt';
                break;

            case "cp_prog":
                break;

            case "analit":
                break;

            case "sint":
                break;

            case "d_analitico":
                break;

            case "d_sint":
                break;

            case "movi_geral":
                break;
        }

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/json');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Content-Length: '.filesize($file));
            readfile($file);
            exit;
        }

        echo $file . '\n';
        echo 'Relatorio n�o existe !';
        exit;
    }

    public function getDados(){
        $report = $_GET['id'];
        $sql = '';
        switch($report){
            case "cr_g":
                break;

            case "cp_g":
                break;

            case "cp_new":
                break;

            case "cr_new":
                break;

            case "pr_r":
                break;

            case "cr_c":
                break;

            case "cp_q":
                break;

            case "movi":
                $sql = "SELECT m.id, m.`data`, CONCAT(mb.mes,'/',mb.ano) AS movimento,
                	    CONVERT(CAST(CONVERT(IF(m.tipo = 1, f.nome, c.nome) USING latin1) AS BINARY) USING utf8) AS credor,
                	    CONVERT(CAST(CONVERT(b.nome USING latin1) AS BINARY) USING utf8) AS banco,
                	    CONVERT(CAST(CONVERT(p.nome USING latin1) AS BINARY) USING utf8) AS plano,
                	    CONVERT(CAST(CONVERT(e.nome USING latin1) AS BINARY) USING utf8) AS empresa
                	    CONVERT(CAST(CONVERT(m.documento USING latin1) AS BINARY) USING utf8) AS documento,
                	    IFNULL(m.credito, 0) AS credito, IFNULL(m.debito, 0) AS debito, IFNULL(ROUND(mb.saldoInicial  + (SELECT SUM(CASE WHEN m1.tipo = 1 THEN (IFNULL(m1.debito, 0) * -1) ELSE IFNULL(m1.credito, 0) END)
                	       FROM movimento AS m1 LEFT JOIN movimento_banco AS mb1 ON (mb1.id = m1.idMovimentoBanco)
                	       WHERE `banco`= 66 AND m1.`idMovimentoBanco` = 981 AND IF(2 = 1, mb1.`status` = 'Encerrado' ,mb1.`status` = 'Aberto')
                	       AND m1.`situacao` = 1 AND m1.status <> 3 AND DATE(m1.`data`) < '2015-10-16'), 2), 0) AS inicial,
                	       (IFNULL(ROUND(mb.saldoInicial  + (SELECT SUM(CASE WHEN m2.tipo = 1 THEN (IFNULL(m2.debito, 0) * -1) ELSE IFNULL(m2.credito, 0) END)
                	         FROM movimento AS m2 LEFT JOIN movimento_banco AS mb2 ON (mb2.id = m2.idMovimentoBanco)
                	         WHERE `banco`= 66 AND m2.`idMovimentoBanco` = 981
                	         AND IF(2 = 1, mb2.`status` = 'Encerrado' ,mb2.`status` = 'Aberto')
                             AND m2.`situacao` = 1 AND m2.status <> 3 AND DATE(m2.`data`) < '2015-10-16'), 2), 0) +
                            ROUND(@odd := IFNULL(@odd, 0) + IFNULL(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END, 0), 2)) as saldo
                        FROM movimento AS m
                        LEFT JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                        LEFT JOIN fornecedor AS f ON (f.id = m.idFornecedor)
                        LEFT JOIN cliente AS c ON (c.id = m.idCliente)
                        LEFT JOIN contabilidade AS e ON (e.id = m.idContabilidade)
                        LEFT JOIN banco AS b ON (b.id = m.banco)
                        LEFT JOIN planocontas AS p ON (p.id = m.idPlanoContas)
                        WHERE m.`banco`= 66 AND m.`idMovimentoBanco` = 981 AND m.`situacao` = 1
                        AND IF(2 = 1, mb.`status` = 'Encerrado' ,mb.`status` = 'Aberto') AND mb.`idBanco`= 66
                        AND m.status <> 3 AND DATE(m.`data`) BETWEEN '2015-10-16' AND '2015-10-16' ORDER BY m.`data`";
                break;

            case "cp_prog":
                break;

            case "analit":
                break;

            case "sint":
                break;

            case "d_analitico":
                break;

            case "d_sint":
                break;

            case "movi_geral":
                break;
        }

        $db = self::getConn();
        $db->query($sql);
        $dados = $db->getResults();
        echo json_encode($dados);
        echo $sql;
        exit;
    }
}