<?php
final class ParametrosController extends AppController{ 

    # página inicial do módulo Parametros
    function index(){
        $this->setTitle('Parametros');
    }

    # lista de Parametros
    # renderiza a visão /view/Parametros/all.php
    function all(){
        $this->setTitle('Parametros');
        $p = new Paginate('Parametros', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (isset($_GET['search'])) {
            $c->addCondition('id', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('status','!=',3);
        $this->set('Parametros', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Parametros
    # renderiza a visão /view/Parametros/view.php
    function view(){
        try {
            $this->set('Parametros', new Parametros((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Parametros', 'all');
        }
    }

    # formulário de cadastro de Parametros
    # renderiza a visão /view/Parametros/add.php
    function add(){
        $this->setTitle('Adicionar Parametros');
        $this->set('Parametros', new Parametros);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Parametros
    # (true)redireciona ou (false) renderiza a visão /view/Parametros/add.php
    function post_add(){
        $this->setTitle('Adicionar Parametros');
        $Parametros = new Parametros();
        $this->set('Parametros', $Parametros);
        try {
            $Parametros->save($_POST);
            new Msg(__('Parametros cadastrado com sucesso'));
            $this->go('Parametros', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Parametros
    # renderiza a visão /view/Parametros/edit.php
    function edit(){
        $this->setTitle('Editar Parametros');
        try {
            $this->set('Parametros', new Parametros((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Parametros', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Parametros
    # (true)redireciona ou (false) renderiza a visão /view/Parametros/edit.php
    function post_edit(){
        $this->setTitle('Editar Parametros');
        try {
            $Parametros = new Parametros((int) $_POST['id']);
            $this->set('Parametros', $Parametros);
            $Parametros->save($_POST);
            new Msg(__('Parametros atualizado com sucesso'));
            $this->go('Parametros', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Confirma a exclusão ou não de um(a) Parametros
    # renderiza a /view/Parametros/delete.php
    function delete(){
        $this->setTitle('Apagar Parametros');
        try {
            $this->set('Parametros', new Parametros((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Parametros', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Parametros
    # redireciona para Parametros/all
    function post_delete(){
        try {
            $Parametros = new Parametros((int) $_POST['id']);
            $Parametros->status=3;
            $Parametros->save();
            new Msg(__('Parametros apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Parametros', 'all');
    }

}