<?php
final class Estq_usuario_substoqueController extends AppController{ 

    # página inicial do módulo Estq_usuario_substoque
    function index(){
        $this->setTitle('Visualização de Estq_usuario_substoque');
    }

    # lista de Estq_usuario_substoques
    # renderiza a visão /view/Estq_usuario_substoque/all.php
    function all(){
        $this->setTitle('Listagem de Estq_usuario_substoque');
        $p = new Paginate('Estq_usuario_substoque', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Estq_usuario_substoques', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Estq_usuario_substoque
    # renderiza a visão /view/Estq_usuario_substoque/view.php
    function view(){
        $this->setTitle('Visualização de Estq_usuario_substoque');
        try {
            $this->set('Estq_usuario_substoque', new Estq_usuario_substoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_usuario_substoque', 'all');
        }
    }

    # formulário de cadastro de Estq_usuario_substoque
    # renderiza a visão /view/Estq_usuario_substoque/add.php
    function add(){
        $this->setTitle('Cadastro de Estq_usuario_substoque');
        $this->set('Estq_usuario_substoque', new Estq_usuario_substoque);
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Estq_subestoques',  Estq_subestoque::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Estq_usuario_substoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_usuario_substoque/add.php
    function post_add(){
        $this->setTitle('Cadastro de Estq_usuario_substoque');
        $Estq_usuario_substoque = new Estq_usuario_substoque();
        $this->set('Estq_usuario_substoque', $Estq_usuario_substoque);
        try {
            $Estq_usuario_substoque->save($_POST);
            new Msg(__('Estq_usuario_substoque cadastrado com sucesso'));
            $this->go('Estq_usuario_substoque', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Estq_subestoques',  Estq_subestoque::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Estq_usuario_substoque
    # renderiza a visão /view/Estq_usuario_substoque/edit.php
    function edit(){
        $this->setTitle('Edição de Estq_usuario_substoque');
        try {
            $this->set('Estq_usuario_substoque', new Estq_usuario_substoque((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Estq_subestoques',  Estq_subestoque::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Estq_usuario_substoque', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Estq_usuario_substoque
    # (true)redireciona ou (false) renderiza a visão /view/Estq_usuario_substoque/edit.php
    function post_edit(){
        $this->setTitle('Edição de Estq_usuario_substoque');
        try {
            $Estq_usuario_substoque = new Estq_usuario_substoque((int) $_POST['id']);
            $this->set('Estq_usuario_substoque', $Estq_usuario_substoque);
            $Estq_usuario_substoque->save($_POST);
            new Msg(__('Estq_usuario_substoque atualizado com sucesso'));
            $this->go('Estq_usuario_substoque', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Estq_subestoques',  Estq_subestoque::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Estq_usuario_substoque
    # renderiza a /view/Estq_usuario_substoque/delete.php
    function delete(){
        $this->setTitle('Apagar Estq_usuario_substoque');
        try {
            $this->set('Estq_usuario_substoque', new Estq_usuario_substoque((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Estq_usuario_substoque', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Estq_usuario_substoque
    # redireciona para Estq_usuario_substoque/all
    function post_delete(){
        try {
            $Estq_usuario_substoque = new Estq_usuario_substoque((int) $_POST['id']);
            $Estq_usuario_substoque->delete();
            new Msg(__('Estq_usuario_substoque apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Estq_usuario_substoque', 'all');
    }

}