<?php

final class GraficoController extends AppController
{
    function geral()
    {
        $a = new Criteria();
        $a->addCondition('status','=',1);
        $a->setOrder('descricao');
        $grupos = GrupoContabilidade::getList($a);

        $this->set('grupos', $grupos);
        $this->setTitle('Geral');
    }

    function especifico()
    {
        $a = new Criteria();
        $a->addCondition('status','=',1);
        $a->setOrder('descricao');
        $grupos = GrupoContabilidade::getList($a);

        $this->set('grupos', $grupos);

        $a = new Criteria();
        $a->addCondition('status','=',1);
        $a->addCondition('considera','=',1);
        $a->setOrder('descricao');
        $classificacao = Classificacao::getList($a);

        $this->set('classi', $classificacao);
        $this->setTitle('Específico');
    }

    public function getFinanceiro(){
        $ano = $_GET['ano'];
        $grupo = $_GET['grupo'];
        $empresa = $_GET['empresa'];

        $ret = array();
        $ret['result'] = false;

        try{
            $b = new Criteria();
            $b->addCondition('status','=',1);
            $b->addCondition('tipo','=',1);
            $b->addCondition('considera','=',1);
            $Classificacao_entrada = Classificacao::getList($b);

            $c = new Criteria();
            $c->addCondition('status','=',1);
            $c->addCondition('tipo','=',2);
            $c->addCondition('considera','=',1);
            $Classificacao_saida = Classificacao::getList($c);

            $entrada = array();
            foreach ($Classificacao_entrada as $c) {
                for($i = 0; $i < 12; $i++) {
                    $movimento = Movimento::simplificado($ano, $grupo, $empresa, $i + 1, $c->id);
                    $entrada[$i] += $movimento->total;
                }
            }

            $saidas = array();
            foreach ($Classificacao_saida as $c) {
                for($i = 0; $i < 12; $i++) {
                    $movimento = Movimento::simplificado($ano, $grupo, $empresa, $i + 1, $c->id);
                    $saidas[$i] += $movimento->total;
                }
            }

            $dados = array();
            for($i = 0; $i < 12; $i++) {
                $dados[$i]['entrada'] = $entrada[$i];
                $dados[$i]['saida'] = abs($saidas[$i]);
                $dados[$i]['resultado'] = $entrada[$i] - abs($saidas[$i]);
            }

            $ret['result'] = true;
            $ret['dados'] = $dados;

        }catch (Exception $e){
            $ret['msg'] = "Erro ao carregar dados financeiro";
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    public function getFinEsp(){
        $ano = $_GET['ano'];
        $grupo = $_GET['grupo'];
        $empresa = $_GET['empresa'];

        $ret = array();
        $ret['result'] = false;

        try{
            $sql = "SELECT CC.descricao, CC.tipo,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 1, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS jan,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 2, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS fev,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 3, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS mar,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 4, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS abr,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 5, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS maio,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 6, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS jun,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 7, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS jul,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 8, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS ago,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 9, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS sete,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 10, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS outu,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 11, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS nov,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 12, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS dez
                    FROM classificacaocontas AS CC
                    LEFT JOIN planocontas AS PC ON CC.id = PC.classificacao
                    LEFT JOIN movimento AS M ON PC.id = M.idPlanoContas
                    WHERE CC.considera = 1 AND CC.status = 1
                    AND EXTRACT(YEAR FROM M.`data`) = :ano";

            if(empty($ano)){
                $ano = date('Y');
            }

            if(!empty($grupo) && empty($empresa)){
                $sql.=" AND M.idContabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo) ";
            }else if(!empty($empresa)){
                $sql.=" AND M.idContabilidade IN(:empresa)";
            }

            $sql .= "GROUP BY CC.descricao ORDER BY CC.tipo, CC.descricao";
            $db = self::getConn();

            $db->query($sql);
            $db->bind(':ano', $ano);
            if(!empty($grupo) && empty($empresa)) {
                $db->bind(':grupo', $grupo);
            }else if(!empty($empresa)) {
                $db->bind(':empresa', $empresa);
            }

            $db->execute();
            $dados = $db->getResults();
            $ret['result'] = true;
            $ret['dados'] = $dados;
        }catch (Exception $e){
            $ret['msg'] = "Erro ao carregar dados grafico";
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    public function getFinEspMov(){
        $ano = $_GET['ano'];
        $grupo = $_GET['grupo'];
        $empresa = $_GET['empresa'];
        $classi = $_GET['classi'];

        $ret = array();
        $ret['result'] = false;

        try{
            $sql = "SELECT PC.nome as descricao,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 1, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS jan,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 2, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS fev,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 3, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS mar,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 4, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS abr,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 5, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS maio,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 6, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS jun,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 7, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS jul,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 8, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS ago,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 9, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS sete,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 10, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS outu,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 11, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS nov,
                    TRUNCATE(SUM(IF(EXTRACT(MONTH FROM M.`data`) = 12, IFNULL(M.credito, 0)+IFNULL(M.debito, 0), 0)), 2) AS dez
                    FROM classificacaocontas AS CC
                    LEFT JOIN planocontas AS PC ON CC.id = PC.classificacao
                    LEFT JOIN movimento AS M ON PC.id = M.idPlanoContas
                    WHERE CC.considera = 1 AND CC.status = 1 AND CC.id = :classi
                    AND EXTRACT(YEAR FROM M.`data`) = :ano";

            if(empty($ano)){
                $ano = date('Y');
            }

            if(!empty($grupo) && empty($empresa)){
                $sql.=" AND M.idContabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo) ";
            }else if(!empty($empresa)){
                $sql.=" AND M.idContabilidade IN(:empresa)";
            }

            $sql .= "GROUP BY PC.nome ORDER BY PC.nome";
            $db = self::getConn();

            $db->query($sql);
            $db->bind(':ano', $ano);
            $db->bind(':classi', $classi);

            if(!empty($grupo) && empty($empresa)) {
                $db->bind(':grupo', $grupo);
            }else if(!empty($empresa)) {
                $db->bind(':empresa', $empresa);
            }

            $db->execute();
            $dados = $db->getResults();
            $ret['result'] = true;
            $ret['dados'] = $dados;
        }catch (Exception $e){
            $ret['msg'] = "Erro ao carregar dados grafico";
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }
}