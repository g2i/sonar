<?php
final class Tipo_pagamentoController extends AppController{ 

    # página inicial do módulo Tipo_pagamento
    function index(){
        $this->setTitle('Tipo_pagamento');
    }

    # lista de Tipo_pagamentos
    # renderiza a visão /view/Tipo_pagamento/all.php
    function all(){
        $this->setTitle('Tipo_pagamentos');
    }

    # visualiza um(a) Tipo_pagamento
    # renderiza a visão /view/Tipo_pagamento/view.php
    function view(){
        try {
            $this->set('Tipo_pagamento', new Tipo_pagamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tipo_pagamento', 'all');
        }
    }

    # formulário de cadastro de Tipo_pagamento
    # renderiza a visão /view/Tipo_pagamento/add.php
    function add(){
        $this->setTitle('Adicionar Tipo_pagamento');
        $this->set('Tipo_pagamento', new Tipo_pagamento);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Tipo_pagamento
    # (true)redireciona ou (false) renderiza a visão /view/Tipo_pagamento/add.php
    function post_add(){
        $this->setTitle('Adicionar Tipo_pagamento');
        $Tipo_pagamento = new Tipo_pagamento();
        $this->set('Tipo_pagamento', $Tipo_pagamento);
        try {
            $Tipo_pagamento->save($_POST);
            new Msg(__('Tipo_pagamento cadastrado com sucesso'));
            $this->go('Tipo_pagamento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Tipo_pagamento
    # renderiza a visão /view/Tipo_pagamento/edit.php
    function edit(){
        $this->setTitle('Editar Tipo_pagamento');
        try {
            $this->set('Tipo_pagamento', new Tipo_pagamento((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Tipo_pagamento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Tipo_pagamento
    # (true)redireciona ou (false) renderiza a visão /view/Tipo_pagamento/edit.php
    function post_edit(){
        $this->setTitle('Editar Tipo_pagamento');
        try {
            $Tipo_pagamento = new Tipo_pagamento((int) $_POST['id']);
            $this->set('Tipo_pagamento', $Tipo_pagamento);
            $Tipo_pagamento->save($_POST);
            new Msg(__('Tipo_pagamento atualizado com sucesso'));
            $this->go('Tipo_pagamento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Recebe o id via post e exclui um(a) Tipo_pagamento
    # redireciona para Tipo_pagamento/all
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $Tipo_pagamento = new Tipo_pagamento((int) $_POST['id']);
            $Tipo_pagamento->status_id = 3;
            $Tipo_pagamento->save();
            $ret['result'] = true;
            $ret['msg'] = 'Centro de custo apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Centro de custo !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function getPagamentos(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM tipo_pagamento AS p WHERE p.status_id <> 3";
            $sql = "SELECT p.id, p.descricao, s.descricao AS status
                    FROM tipo_pagamento AS p INNER JOIN status AS s ON (s.id = p.status_id)
                    WHERE p.status_id <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND p.descricao LIKE :termo";
                $sql .= " AND p.descricao LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY p.descricao";
                $sort[] = array('descricao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }

}