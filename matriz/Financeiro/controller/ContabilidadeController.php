<?php
final class ContabilidadeController extends AppController{ 

    # página inicial do módulo Contabilidade
    function index(){
        $this->setTitle('Empresa');
    }

    # lista de Contabilidades
    # renderiza a visão /view/Contabilidade/all.php
    function all(){
        $this->setTitle('Empresa');
    }

    # visualiza um(a) Contabilidade
    # renderiza a visão /view/Contabilidade/view.php
    function view(){
        try {
            $this->set('Contabilidade', new Contabilidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Contabilidade', 'all');
        }
    }

    # formulário de cadastro de Contabilidade
    # renderiza a visão /view/Contabilidade/add.php
    function add(){
        $this->setTitle('Adicionar Empresa');
        $this->set('Contabilidade', new Contabilidade);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Contabilidade
    # (true)redireciona ou (false) renderiza a visão /view/Contabilidade/add.php
    function post_add(){
        $this->setTitle('Adicionar Empresa');
        $Contabilidade = new Contabilidade();
        $this->set('Contabilidade', $Contabilidade);
        try {
            $Contabilidade->save($_POST);
            new Msg(__('Empresa cadastrada com sucesso'));
            $this->go('Contabilidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Contabilidade
    # renderiza a visão /view/Contabilidade/edit.php
    function edit(){
        $this->setTitle('Editar Empresa');
        try {
            $this->set('Contabilidade', new Contabilidade((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Contabilidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Contabilidade
    # (true)redireciona ou (false) renderiza a visão /view/Contabilidade/edit.php
    function post_edit(){
        $this->setTitle('Editar Empresa');
        try {
            $Contabilidade = new Contabilidade((int)$_POST['id']);
            $this->set('Contabilidade', $Contabilidade);
            $Contabilidade->save($_POST);
            new Msg(__('Empresa atualizada com sucesso'));
            $this->go('Contabilidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Recebe o id via post e exclui um(a) Contabilidade
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $Contabilidade = new Contabilidade((int)$_POST['id']);
            $Contabilidade->status = 3;
            $Contabilidade->save();
            $ret['result'] = true;
            $ret['msg'] = 'Banco apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Banco !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function grupos()
    {
        $this->setTitle('Grupos');
    }

    function addGrupo() {
        $ret = array();
        $ret['result'] = false;

        try {
            $grupo = new ContabilidadeGrupos();
            $grupo->grupo = $_POST['grupo'];
            $grupo->contabilidade = $_POST['contabilidade'];
            $grupo->save();
            $ret['result'] = true;
            $ret['msg'] = 'Grupo adicionado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao adicionar grupo !';
            $ret['erro'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function deleteGrupo() {
        $ret = array();
        $ret['result'] = false;

        try {
            $contabilidade = new ContabilidadeGrupos((int)$_POST['id']);
            $contabilidade->delete();
            $ret['result'] = true;
            $ret['msg'] = 'Grupo removido com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao remover grupo !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function getEmpresas(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM contabilidade AS c WHERE c.status <> 3";
            $sql = "SELECT c.id, c.nome, c.descricao, c.cidade, c.uf, s.descricao AS status
                    FROM contabilidade AS c INNER JOIN status AS s ON (s.id = c.status)
                    WHERE c.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND c.nome LIKE :termo";
                $sql .= " AND c.nome LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.nome";
                $sort[] = array('nome' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    function getGrupos(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $cont = $this->getParam('cont');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM contabilidade_grupo AS g
                         LEFT JOIN contabilidade AS c ON (c.id = g.contabilidade)
                         WHERE g.contabilidade = :cont";

            $sql = "SELECT g.id, c.descricao AS nome FROM contabilidade_grupo AS g
                    LEFT JOIN grupo_contabilidade AS c ON (c.id = g.grupo)
                    WHERE g.contabilidade = :cont";

            if (!empty($search)) {
                $sqlCount .= " AND c.descricao LIKE :termo";
                $sql .= " AND c.descricao LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.descricao";
                $sort[] = array('descricao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":cont", $cont);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            $db->bind(":cont", $cont);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    function getContGrupos(){
        $ret = array();
        $ret['result'] = false;

        try{

            $cont = $this->getParam('id');
            $sql = "SELECT c.id, c.descricao AS nome FROM grupo_contabilidade AS c
                WHERE c.id NOT IN(SELECT b.grupo
                FROM contabilidade_grupo AS b WHERE b.contabilidade = :cont)";

            $db = $this::getConn();
            $db->query($sql);

            $db->bind(":cont", $cont);
            $ret['dados'] = $db->getResults();
            $ret['result'] = true;
        }catch (Exception $e){
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }
}