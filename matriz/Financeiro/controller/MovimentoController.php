<?php

final class MovimentoController extends AppController {
    # página inicial do módulo Movimento

    function index() {
        $this->setTitle('Movimento');
    }

    function all() {
        $this->setTitle('Movimentos');

        $d = new Criteria();
        $d->setOrder('nome');
        $d->addCondition('status','=',1);
        $banco = Banco::getList($d);

        $this->set('Banco', $banco);
    }

    function new_all() {
        $this->setTitle('Movimentos');
        $inicio =  date("Y") . '-' . date("m") . '-' . "01";
        $this->set('inicio',$inicio);
        $fim = date("Y-m-t");
        $this->set('fim', $fim);

        $crit = new Criteria();
        $crit->addCondition('status','=',1);
        $crit->setOrder('nome ASC');
        $this->set('Empresas',Contabilidade::getList($crit));

        $a = new Criteria();
        $a->addCondition('status','=',1);
        $a->setOrder('nome ASC');
        $this->set('Bancos',Banco::getList($a));
    }

    function view() {
        try {
            $this->set('Movimento', new Movimento((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Movimento', 'all');
        }
    }

    function add() {
        $this->setTitle('Adicionar Movimento');
        $order = new Criteria();
        $order->setOrder('cliente');

        $a = new Criteria();
        $a->setOrder('nome');

        $aa = new Criteria();
        $aa->setOrder('nome');

        $movimento = new MovimentoBanco();
        $c = new Criteria();
        $c->addCondition('status', '=', 'Aberto');

        foreach ($movimento->getList($c) as $m) {
            $dados = array();
            $dados[] = $m->id;
            $dados[] = $m->getBanco($m->id)->nome . ' - ' . $m->mes . '/' . $m->ano;
            $mov[] = $dados;
        }

        $this->set('movimento', $mov);
        $this->set('Planocontas', Planocontas::getList($a));
        $this->set('Empresa', Contabilidade::getList());

    }

    function post_add() {
        $ret = array();
        $ret["sucess"] = false;
        $ret["msg"] = "Erro ao adicionar movimento";

        try {
            $m = new MovimentoBanco($_POST['movimento']);
            $movimento = new Movimento();
            $movimento->id = NULL;
            $movimento->categoria = 1;
            $movimento->situacao = $_POST['situacao'];
            $movimento->data = convertDataBR4SQL($_POST['data']);
            $movimento->idPlanoContas = $_POST['idPlanoContas'];
            $movimento->tipo = $_POST['tipo'];
            $movimento->idFornecedor = ($_POST['tipo'] == 2) ? NULL : $_POST['idFornecedor'];
            $movimento->idCliente = ($_POST['tipo'] == 1) ? NULL : $_POST['idCliente'];
            $movimento->complemento = $_POST['complemento'];
            $movimento->documento = $_POST['documento'];
            $movimento->debito = ($_POST['tipo'] == 2) ? 0 : getAmount($_POST['debito']);
            $movimento->credito = ($_POST['tipo'] == 1) ? 0 : getAmount($_POST['credito']);
            $movimento->banco = $m->idBanco;
            $movimento->status = 1;
            $movimento->idConta = NULL;
            $movimento->idMovimentoBanco = $_POST['movimento'];
            $movimento->idContabilidade = $_POST['idContabilidade'];

            if ($movimento->save()){
                $ret["sucess"] = true;
                $ret["msg"] = "Movimento cadastrado com sucesso !";
            }
        } catch (Exception $e) {
            $ret["sucess"] = false;
            $ret["msg"] = "Erro ao adicionar movimento";
            $ret["error"] = $e->getMessage();
            $ret["trace"] = $e->getTraceAsString();
        }

        $this->returnAsJson($ret);
    }

    function transferencia() {
        $this->setTitle('Transferência');
        $order = new Criteria();
        $order->setOrder('cliente');

        $a = new Criteria();
        $a->setOrder('nome');

        $aa = new Criteria();
        $aa->setOrder('nome');

        $mov = array();

        $movimento = new MovimentoBanco();
        $c = new Criteria();
        $c->addCondition('status', '=', 'Aberto');

        foreach ($movimento->getList($c) as $m) {
            $dados = array();
            $dados[] = $m->id;
            $dados[] = $m->getBanco($m->id)->nome . ' - ' . $m->mes . '/' . $m->ano;
            $mov[] = $dados;
        }

        $this->set('movimento', $mov);
        $this->set('empresa', Contabilidade::getList());

    }

    function getPara(){
        $c = new Criteria();
        $c->addCondition('id','<>',$_POST['id']);
        $c->addCondition('status','=','Aberto');
        $Movimento = MovimentoBanco::getList($c);
            if($Movimento){
                $rs = "<option value=''>Selecione</option>";
                foreach ($Movimento as $m) {
                    $rs .= "<option value='".$m->id."'>".$m->getBanco()->nome."</option>";
                }
                echo $rs;
        }
        exit;

    }

    function post_transferencia() {
        $ret = array();
        $ret["sucess"] = false;
        $ret["msg"] = "Não foi possivel fazer a transferencia !";

        try {
            $m = new MovimentoBanco($_POST['de']);
            $movimento = new Movimento();
            $movimento->categoria = 1;
            $movimento->situacao = $_POST['situacao'];
            $movimento->data = convertDataBR4SQL($_POST['data']);
            $movimento->idPlanoContas = $_POST['idPlanoContas'];
            $movimento->tipo = 1;
            $movimento->idFornecedor = $_POST['idFornecedor'];
            $movimento->idCliente =  null;
            $movimento->complemento = $_POST['complemento'];
            $movimento->documento = $_POST['documento'];
            $movimento->debito = getAmount($_POST['valor']);
            $movimento->credito = 0;
            $movimento->banco = $m->idBanco;
            $movimento->status = 1;
            $movimento->idConta = NULL;
            $movimento->idMovimentoBanco = $_POST['de'];
            $movimento->idContabilidade = $_POST['idContabilidade'];

            if ($movimento->save()) {

                $m = new MovimentoBanco($_POST['para']);
                $movimento = new Movimento();
                $movimento->categoria = 1;
                $movimento->situacao = $_POST['situacao'];
                $movimento->data = convertDataBR4SQL($_POST['data']);
                $movimento->idPlanoContas = $_POST['idPlanoContas'];
                $movimento->tipo = 2;
                $movimento->idFornecedor = NULL;
                $movimento->idCliente = $_POST['idCliente'];
                $movimento->complemento = $_POST['complemento'];
                $movimento->documento = $_POST['documento'];
                $movimento->debito = 0;
                $movimento->credito = getAmount($_POST['valor']);
                $movimento->banco = $m->idBanco;
                $movimento->status = 1;
                $movimento->idConta = NULL;
                $movimento->idMovimentoBanco = $_POST['para'];
                $movimento->idContabilidade = $_POST['idContabilidade'];

                if ($movimento->save()) {
                    $ret["sucess"] = true;
                    $ret["msg"] = "Transferencia executada com sucesso !";
                }
            }
        } catch (Exception $e) {
            $ret["sucess"] = false;
            $ret["msg"] = "Erro ao fazer transferencia !";
            $ret["error"] = $e->getMessage();
            $ret["trace"] = $e->getTraceAsString();
        }

        $this->returnAsJson($ret);
    }

    function edit() {
        $this->setTitle('Editar Movimento');
        $order = new Criteria();
        $order->setOrder('cliente');

        $a = new Criteria();
        $a->setOrder('nome');
        $Movimento = new Movimento((int) $this->getParam('id'));
        $this->set('Movimento', $Movimento);
        $this->set('Planocontas', Planocontas::getList($a));
        $this->set('movimento', new MovimentoBanco());
        $this->set('status', Status::getList());
        $this->set('Empresa', Contabilidade::getList());

        $this->set('forncedorSelect', new Fornecedor($Movimento->idFornecedor));
        $this->set('clienteSelect', new Cliente($Movimento->idCliente));

    }

    function post_edit() {
        $this->setTitle('Editar Movimento');

        try {
            $m = new MovimentoBanco($_POST['movimento']);

            $movimento = new Movimento($_POST['id']);
            $movimento->categoria = 1;
            $movimento->situacao = $_POST['situacao'];
            $movimento->data = convertDataBR4SQL($_POST['data']);
            $movimento->idPlanoContas = $_POST['idPlanoContas'];
            $movimento->tipo = $_POST['tipo'];
            $movimento->idFornecedor = ($_POST['tipo'] == 2) ? NULL : $_POST['idFornecedor'];
            $movimento->idCliente = ($_POST['tipo'] == 1) ? NULL : $_POST['idCliente'];
            $movimento->complemento = $_POST['complemento'];
            $movimento->documento = $_POST['documento'];
            $movimento->debito = ($_POST['tipo'] == 2) ? 0 : getAmount($_POST['debito']);
            $movimento->credito = ($_POST['tipo'] == 1) ? 0 : getAmount($_POST['credito']);
            $movimento->banco = $m->idBanco;
            $movimento->status = 1;
            $movimento->idMovimentoBanco = $_POST['movimento'];
            $movimento->idContabilidade = $_POST['idContabilidade'];
            if ($movimento->save())
                new Msg(__("Movimento cadastrado com sucesso"));
            $this->go('Movimento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    function estornar()
    {
        $ret = array();
        $ret['result'] = false;

        try {
            $Movimento = new Movimento((int)$_POST['id']);
            $Movimento->status = 3;
            if($Movimento->save()){
                if(!empty($Movimento->idConta)||$Movimento->idConta > 0){
                    if(!empty($Movimento->debito)||$Movimento->debito > 0){
                        $aux = new Contaspagar($Movimento->idConta);
                        $aux->dataPagamento = NULL;
                        $aux->save();
                    }else if(!empty($Movimento->credito)||$Movimento->credito > 0){
                        $aux = new Contasreceber($Movimento->idConta);
                        $aux->dataPagamento = NULL;
                        $aux->save();
                    }
                }
            }
            $ret['result'] = true;
            $ret['msg'] = 'Movimento estornado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao estornar movimento !';
            $ret['erro'] = $e->getMessage();
        }

        $this->returnAsJson($ret);
        exit;
    }

    function delete() {
        $this->setTitle('Apagar Movimento');
        try {
            $this->set('Movimento', new Movimento((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Movimento', 'all');
        }
    }

    function post_delete() {
        try {
            $Movimento = new Movimento((int) $_POST['id']);
            $Movimento->delete();
            new Msg(__('Movimento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Movimento', 'all');
    }

    function controle() {
        $this->setTitle('Movimento - Controle');
        $bancos = Banco::getList();
        $this->set('bancos', $bancos);

        $c = new Criteria();
        $this->set('mes', NULL);
        $this->set('ano', NULL);
        $this->set('banco', NULL);
        $this->set('status', NULL);

        if (!empty($_GET['banco']) && $_GET['banco'] != '0') {
            $c->addCondition('idBanco', '=', $_GET['banco']);
            $this->set('banco', $this->getParam('banco'));
        }
        if (!empty($_GET['ano'])) {
            $c->addCondition('ano', '=', $_GET['ano']);
            $this->set('ano', $this->getParam('ano'));
        }
        if (!empty($_GET['mes'])) {
            $c->addCondition('mes', '=', $_GET['mes']);
            $this->set('mes', $this->getParam('mes'));
        }
        if (!empty($_GET['status'])) {
            $c->addCondition('status', '=', $_GET['status']);
            $this->set('status', $this->getParam('status'));
        }

        $controle = new MovimentoBanco();
        $this->set('controle', $controle);
        $this->set('criteria', $c);
    }

    function abertura() {
        $this->setTitle('Abrir Movimento');
        $banco = Banco::getList();
        $this->set('banco', $banco);
    }

    function post_abertura() {
        if (!MovimentoBanco::HasMovimento($_POST['mes'], $_POST['ano'], $_POST['banco'])) {
            try {
                $user = Session::get('user');

                $controle = new MovimentoBanco();
                $controle->id = NULL;
                $controle->mes = $_POST['mes'];
                $controle->ano = $_POST['ano'];
                $controle->status = 'Aberto';
                $controle->idBanco = $_POST['banco'];
                $controle->saldoInicial = getAmount($_POST['saldoInicial']);
                $controle->saldoFinal = NULL;
                $controle->data = date('Y-m-d H:i:s');
                $controle->por = $user->id;
                $controle->encerradoDt = '0000-00-00 00:00:00';
                $controle->encerradoPor = NULL;

                if ($controle->save())
                    new Msg(__('Movimento cadastrado com sucesso'), 1);
            } catch (Exception $e) {
                new Msg($e->getMessage(), 3);
            }
        } else {
            new Msg(__("Esse movimento já foi cadastrado."), 3);
        }
        $this->go('Movimento', 'controle');
    }

    function fechar() {
        $this->setTitle('Encerrar Movimenta&ccedil;&atilde;o');
        try {
            $this->set('controle', new MovimentoBanco((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Movimento', 'controle');
        }
    }

    function post_fechar() {
        try {
            $controle = new MovimentoBanco($_POST['id']);
            $controle->status = 'Encerrado';
            $controle->save();
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Movimento', 'controle');
    }

    function reabrir() {
        $this->setTitle('Reabrir Movimenta&ccedil;&atilde;o');
        try {
            $this->set('controle', new MovimentoBanco((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Movimento', 'controle');
        }
    }

    function post_reabrir() {
        try {
            $controle = new MovimentoBanco($_POST['id']);
            $controle->status = 'Aberto';
            $controle->save();
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Movimento', 'controle');
    }

    function validarMovimentacao() {
        $data = $_POST['data'];
        $banco = $_POST['banco'];

        $aux = explode('-', $data);

        $c = new Criteria();
        $c->addCondition('status', '=', 'Aberto');
        $c->addCondition('banco', '=', $banco);
        $c->addCondition('mes', '=', $aux[1]);
        $c->addCondition('ano', '=', $aux[0]);

        $controle = MovimentoControle::getList($c);

        if (!empty($controle)) {
            echo "1";
        }
        print_r($controle);

        exit;
    }

    function getSaldo() {
        if (!empty($_POST['movimento'])) {
            $movimento = new MovimentoBanco($_POST['movimento']);
            echo number_format($movimento->saldoInicial, 2, ',', '.');
        }
        exit();
    }

    function getMovimentoBanco() {
        $banco = $_POST['banco'];
        $categoria = ($_POST['categoria'] == 1) ? "Encerrado" : "Aberto";

        $c = new Criteria();
        $c->addCondition("status", "=", $categoria);
        $c->addCondition("idBanco", "=", $banco);
        $movimento = MovimentoBanco::getList($c);
        $html = "";

        $html .= "<option value=''></option>";
        foreach ($movimento as $m) {
            $html .= "<option value='" . $m->id . "'>" . $m->mes . "/" . $m->ano . "</option>";
        }

        echo $html;
        exit;
    }

    function SaldoInicialData(){
        $inicio = !empty($_GET['inicio']) ? $_GET['inicio'] : date("Y-m-01");
        $inicioAux = convertDataBR4SQL($inicio);
        if(!empty($inicioAux)){
            $inicio = convertDataBR4SQL($inicio);
        }

        try {
            $sql = "SELECT b.saldoInicial AS inicial,
                    IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0) as saldo
                    FROM movimento AS m INNER JOIN movimento_banco AS b ON (b.id = m.idMovimentoBanco)
                    WHERE m.status <> 3";

            $db = self::getConn();
            if (!empty($_POST['sit'])) {
                $sql .= " AND m.situacao = :sit";
            }

            if (!empty($_POST['mov'])) {
                $sql .= " AND m.idMovimentoBanco = :mov";
            }

            if (!empty($_POST['inicio'])) {
                $sql .= " AND DATE(m.data) < :inicio";
            }

            $sql .= " ORDER BY  m.data";
            $db->query($sql);

            if (!empty($_POST['sit'])) {
                $db->bind(":sit", $_POST['sit']);
            }

            if (!empty($_POST['mov'])) {
                $db->bind(":mov", $_POST['mov']);
            }

            if (!empty($_POST['inicio'])) {
                $db->bind(":inicio", convertDataBR4SQL($_POST['inicio']));
            }

            $saldo = $db->getRow();
            if (!empty($_POST['inicio'])) {
                echo $saldo->inicial + $saldo->saldo;
            }else{
                echo $saldo->inicial;
            }
        }catch (Exception $e){
            echo $e->getMessage();
        }
        exit;
    }

    function getMovimento(){

        $ret = array();

        try {
            $page = (int)$this->getParam('current');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : json_decode('[]');
            $inicio = !empty($_GET['inicio']) ? $_GET['inicio'] : date("Y-m-01");
            $fim = !empty($_GET['fim']) ? $_GET['fim'] : date("Y-m-t");

            $inicioAux = convertDataBR4SQL($inicio);
            if(!empty($inicioAux)){
                $inicio = convertDataBR4SQL($inicio);
            }

            $fimAux = convertDataBR4SQL($fim);
            if(!empty($fimAux)){
                $fim = convertDataBR4SQL($fim);
            }

            $sqlCount = "SELECT count(*) AS count FROM movimento AS m
                         WHERE m.status <> 3 AND m.idMovimentoBanco = :movimento
                         AND DATE(m.data) BETWEEN :inicio AND :fim";

            $sql = "SELECT m.id, m.data, IF(m.tipo = 1, f.nome, c.nome) AS nome,
                    p.nome AS plano, e.nome AS empresa, m.documento, IFNULL(m.credito, 0) AS credito,
                    IFNULL(m.debito, 0) AS debito, 0 AS saldo FROM movimento AS m
                    LEFT JOIN planocontas AS p ON (p.id = m.idPlanoContas)
                    LEFT JOIN cliente AS c ON (c.id = m.idCliente)
                    LEFT JOIN fornecedor AS f ON (f.id = m.idFornecedor)
                    LEFT JOIN contabilidade AS e ON (e.id = m.idContabilidade)
                    WHERE m.status <> 3 AND m.idMovimentoBanco = :movimento
                    AND DATE(m.data) BETWEEN :inicio AND :fim";

            if (!empty($_GET['situacao'])) {
                $sqlCount .= " AND m.situacao = :situacao";
                $sql .= " AND m.situacao = :situacao";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY m.data";
                $sort[] = array('data' => 'ASC');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($_GET['movimento'])) {
                $db->bind(":movimento", $_GET['movimento']);
            }else{
                $db->bind(":movimento", 0);
            }

            if (!empty($_GET['situacao'])) {
                $db->bind(":situacao", $_GET['situacao']);
            }

            $db->bind(":inicio", $inicio);
            $db->bind(":fim", $fim);

            //echo $sqlCount;
            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($_GET['movimento'])) {
                $db->bind(":movimento", $_GET['movimento']);
            }else{
                $db->bind(":movimento", 0);
            }

            if (!empty($_GET['situacao'])) {
                $db->bind(":situacao", $_GET['situacao']);
            }

            $db->bind(":inicio", $inicio);
            $db->bind(":fim", $fim);
            $dados = $db->getResults();

            $somatoria = array();
            $somatoria['inicial'] = 0;
            $somatoria['final'] = 0;
            $somatoria['credito'] = 0;
            $somatoria['debito'] = 0;
            if (!empty($_GET['movimento'])) {
                $somatoria['inicial'] = MovimentoBanco::getSaldoMovimento($inicio, $_GET['movimento']);
                $somatoria['final'] = $somatoria['inicial'];
                foreach ($dados AS $d) {
                    $somatoria['final'] += $d->credito - $d->debito;
                    $somatoria['credito'] += $d->credito;
                    $somatoria['debito'] -= $d->debito;
                    $d->saldo = $somatoria['final'];
                }
            }

            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $ret["rowCount"] > 0 ? $dados : array();
            $ret["total"] = $total;
            $ret["sort"] = $sort;
            $ret["somatoria"] = $somatoria;
        }catch (Exception $e){
            $ret['erro'] = $e->getMessage();
            $ret['trace'] = $e->getTraceAsString();
        }

        $this->returnAsJson($ret);
    }

    function getMovimentoNew(){

        try {
            $page = (int)$this->getParam('current');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : json_decode('[]');
            $inicio = !empty($_GET['inicio']) ? $_GET['inicio'] : date("Y-m-01");
            $fim = !empty($_GET['fim']) ? $_GET['fim'] : date("Y-m-t");

            $inicioAux = convertDataBR4SQL($inicio);
            if(!empty($inicioAux)){
                $inicio = convertDataBR4SQL($inicio);
            }

            $fimAux = convertDataBR4SQL($fim);
            if(!empty($fimAux)){
                $fim = convertDataBR4SQL($fim);
            }

            $sqlCount = "SELECT count(*) AS count FROM movimento AS m
                         LEFT JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                         WHERE m.status <> 3 AND DATE(m.data) BETWEEN :inicio AND :fim
                         AND mb.status = 'Aberto'";

            $sql = "SELECT m.id, m.data, IF(m.tipo = 1, f.nome, c.nome) AS nome,
                    p.nome AS plano, e.nome AS empresa, m.documento, IFNULL(m.credito, 0) AS credito,
                    CONCAT(b.nome, ' - ', mb.mes, '/', mb.ano) AS movimento, m.complemento AS Obs,
                    IFNULL(m.debito, 0) AS debito, 0 AS saldo FROM movimento AS m
                    LEFT JOIN planocontas AS p ON (p.id = m.idPlanoContas)
                    LEFT JOIN cliente AS c ON (c.id = m.idCliente)
                    LEFT JOIN fornecedor AS f ON (f.id = m.idFornecedor)
                    LEFT JOIN contabilidade AS e ON (e.id = m.idContabilidade)
                    LEFT JOIN banco AS b ON (b.id = m.banco)
                    LEFT JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                    WHERE m.status <> 3 AND DATE(m.data) BETWEEN :inicio AND :fim
                    AND mb.status = 'Aberto'";


            if (!empty($_GET['empresa'])) {
                $sqlCount .= " AND m.idContabilidade = :empresa";
                $sql .= " AND m.idContabilidade = :empresa";
            }

            if (!empty($_GET['documento'])) {
                $sqlCount .= " AND m.documento = :documento";
                $sql .= " AND m.documento = :documento";
            }

            if (!empty($_GET['plano'])) {
                $sqlCount .= " AND m.idPlanoContas = :plano";
                $sql .= " AND m.idPlanoContas = :plano";
            }

            if (!empty($_GET['fornecedor'])) {
                $sqlCount .= " AND m.idFornecedor = :fornecedor";
                $sql .= " AND m.idFornecedor = :fornecedor";
            }

            if (!empty($_GET['cliente'])) {
                $sqlCount .= " AND m.idCliente = :cliente";
                $sql .= " AND m.idCliente = :cliente";
            }

            if (!empty($_GET['movimento'])) {
                $sqlCount .= " AND m.idMovimentoBanco = :movimento";
                $sql .= " AND m.idMovimentoBanco = :movimento";
            }

            if (!empty($_GET['banco'])) {
                $sqlCount .= " AND m.banco = :banco";
                $sql .= " AND m.banco = :banco";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY m.data";
                $sort[] = array('data' => 'ASC');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":inicio", $inicio);
            $db->bind(":fim", $fim);

            if (!empty($_GET['empresa'])) {
                $db->bind(":empresa", $_GET['empresa']);
            }

            if (!empty($_GET['documento'])) {
                $db->bind(":documento", $_GET['documento']);
            }

            if (!empty($_GET['plano'])) {
                $db->bind(":plano", $_GET['plano']);
            }

            if (!empty($_GET['fornecedor'])) {
                $db->bind(":fornecedor", $_GET['fornecedor']);
            }

            if (!empty($_GET['cliente'])) {
                $db->bind(":cliente", $_GET['cliente']);
            }

            if (!empty($_GET['movimento'])) {
                $db->bind(":movimento", $_GET['movimento']);
            }

            if (!empty($_GET['banco'])) {
                $db->bind(":banco", $_GET['banco']);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            $db->bind(":inicio", $inicio);
            $db->bind(":fim", $fim);

            if (!empty($_GET['empresa'])) {
                $db->bind(":empresa", $_GET['empresa']);
            }

            if (!empty($_GET['documento'])) {
                $db->bind(":documento", $_GET['documento']);
            }

            if (!empty($_GET['plano'])) {
                $db->bind(":plano", $_GET['plano']);
            }

            if (!empty($_GET['fornecedor'])) {
                $db->bind(":fornecedor", $_GET['fornecedor']);
            }

            if (!empty($_GET['cliente'])) {
                $db->bind(":cliente", $_GET['cliente']);
            }

            if (!empty($_GET['movimento'])) {
                $db->bind(":movimento", $_GET['movimento']);
            }

            if (!empty($_GET['banco'])) {
                $db->bind(":banco", $_GET['banco']);
            }

            $dados = $db->getResults();

            $somatoria = array();
            $somatoria['inicial'] = 0;
            $somatoria['final'] = 0;
            $somatoria['credito'] = 0;
            $somatoria['debito'] = 0;

            if (empty($_GET['empresa']) && empty($_GET['documento']) &&
                empty($_GET['plano']) && empty($_GET['fornecedor']) &&
                empty($_GET['cliente']))
            {
                $somatoria['inicial'] = MovimentoBanco::getSaldoFiltro($inicio, $_GET['banco'], $_GET['movimento']);
            }

            $somatoria['final'] = $somatoria['inicial'];
            foreach ($dados AS $d) {
                $somatoria['final'] += $d->credito - $d->debito;
                $somatoria['credito'] += $d->credito;
                $somatoria['debito'] -= $d->debito;
                $d->saldo = $somatoria['final'];
            }

            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $ret["rowCount"] > 0 ? $dados : array();
            $ret["total"] = $total;
            $ret["sort"] = $sort;
            $ret["somatoria"] = $somatoria;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }
}
