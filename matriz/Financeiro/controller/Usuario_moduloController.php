<?php
final class Usuario_moduloController extends AppController{ 

    # página inicial do módulo Usuario_modulo
    function index(){
        $this->setTitle('Visualização de Usuario_modulo');
    }

    # lista de Usuario_modulos
    # renderiza a visão /view/Usuario_modulo/all.php
    function all(){
        $this->setTitle('Listagem de Usuario_modulo');
        $p = new Paginate('Usuario_modulo', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Usuario_modulos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Usuario_modulo
    # renderiza a visão /view/Usuario_modulo/view.php
    function view(){
        $this->setTitle('Visualização de Usuario_modulo');
        try {
            $this->set('Usuario_modulo', new Usuario_modulo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario_modulo', 'all');
        }
    }

    # formulário de cadastro de Usuario_modulo
    # renderiza a visão /view/Usuario_modulo/add.php
    function add(){
        $this->setTitle('Cadastro de Usuario_modulo');
        $this->set('Usuario_modulo', new Usuario_modulo);
        $this->set('Modulos',  Modulo::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Usuario_modulo
    # (true)redireciona ou (false) renderiza a visão /view/Usuario_modulo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Usuario_modulo');
        $Usuario_modulo = new Usuario_modulo();
        $this->set('Usuario_modulo', $Usuario_modulo);
        try {
            $Usuario_modulo->save($_POST);
            new Msg(__('Usuario_modulo cadastrado com sucesso'));
            $this->go('Usuario_modulo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Modulos',  Modulo::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Usuario_modulo
    # renderiza a visão /view/Usuario_modulo/edit.php
    function edit(){
        $this->setTitle('Edição de Usuario_modulo');
        try {
            $this->set('Usuario_modulo', new Usuario_modulo((int) $this->getParam('id')));
            $this->set('Modulos',  Modulo::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Usuario_modulo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Usuario_modulo
    # (true)redireciona ou (false) renderiza a visão /view/Usuario_modulo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Usuario_modulo');
        try {
            $Usuario_modulo = new Usuario_modulo((int) $_POST['id']);
            $this->set('Usuario_modulo', $Usuario_modulo);
            $Usuario_modulo->save($_POST);
            new Msg(__('Usuario_modulo atualizado com sucesso'));
            $this->go('Usuario_modulo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Modulos',  Modulo::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Usuario_modulo
    # renderiza a /view/Usuario_modulo/delete.php
    function delete(){
        $this->setTitle('Apagar Usuario_modulo');
        try {
            $this->set('Usuario_modulo', new Usuario_modulo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario_modulo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Usuario_modulo
    # redireciona para Usuario_modulo/all
    function post_delete(){
        try {
            $Usuario_modulo = new Usuario_modulo((int) $_POST['id']);
            $Usuario_modulo->delete();
            new Msg(__('Usuario_modulo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Usuario_modulo', 'all');
    }

}