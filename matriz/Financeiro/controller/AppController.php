<?php

/**
 * Classe AppController
 *
 * @author Miguel
 * @package \controller
 */
class AppController extends Controller
{

    /**
     * Esta função é executada para gerar todas as modificações feitas
     * nas programações
     *
     * Situação 1: Usuário faz uma alteração na programação
     * Situação 2: Usuário faz uma alteração na contas a receber
     * Situação 3: Usuário faz uma alteração na contas a pagar
     *
     * Em todas essas situações o sistema verifica se necessita fazer modificações
     * nas programações ou se não há necessidade
     *
     */
    public static function gerar_programacao()
    {
        if (
            CONTROLLER == "Programacao" ||
            CONTROLLER == "Contasreceber" ||
            CONTROLLER == "Contaspagar"
        ) {


            return true;

        } return false;
    }

    /**
     * Esta função é executada sempre antes da execução da funçao do
     * Controller especificado.
     *
     * Implemente aqui regras globais que podem valer para todos os Controllers
     */
    public function beforeRun()
    {

        if(AppController::gerar_programacao())
            $this->set("refresh_programacao", true);

        if ($this->getParam('acessoExterno')) {
            $acessoExterno = json_decode(base64_decode($this->getParam('acessoExterno')));
            if ($acessoExterno->remetente == "exito") {
                if (isset($acessoExterno->usuario->login) &&
                    isset($acessoExterno->usuario->senha)
                ) {
                    $c = new Criteria();
                    $c->addCondition('login', '=', $acessoExterno->usuario->login);
                    $c->addCondition('senha', '=', $acessoExterno->usuario->senha);
                    $user = Usuario::getFirst($c);
                    if ($user) {
                        Session::set('user', $user);
                        if ($this->getParam('id')) {
                            $this->go("Atividade", "edit", array("id" => $this->getParam('id')));
                        } else {
                            $this->go(Config::get('indexController'), Config::get('indexAction'));
                        }
                    } else {
                        new Msg('Voce n&atilde;o tem acesso a esta p&aacute;gina.', 3);
                        $this->go('login', 'login');
                    }
                }
            }
        }

        $user = Session::get('user');
        if (!$user && (CONTROLLER != 'Login' || (ACTION != 'login' && ACTION != 'lock')) && (CONTROLLER != 'Boleto' || ACTION != 'gerar')) {
            new Msg('Voce n&atilde;o tem acesso a esta p&aacute;gina.', 3);
            if(CONTROLLER != 'Login' &&
                (CONTROLLER != Config::get('indexController') && ACTION != Config::get('indexAction'))) {
                $this->go('login', 'login', array('redirect' => $this->getCurrentURL()));
            }
            else {
                $this->go('login', 'login');
            }
        }
        $this->set('user_',$user);

        Config::set('usuario_logado', $user);

        //Configuração menu - efeitos active
        switch (CONTROLLER) {
            case 'Contaspagar':
            case 'Contasreceber':
            case 'Movimento':
                $menu = array('menu' . CONTROLLER, 'menu' . CONTROLLER . ACTION);
                $this->set('menu', $menu);
                break;
            case 'Index':
                $this->set('menu', NULL);
                break;
            case 'Planocontas':
            case 'Fornecedor':
            case 'Classificacao':
            case 'Banco':
            case 'Contabilidade':
                $menu = array('menuCadastro', 'menu' . CONTROLLER);
                $this->set('menu', $menu);
                break;
            default:
                $this->set('menu', 'menu' . CONTROLLER);
                break;
        }


        # evitando Cross-site Scripting
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
        //$_GET = filter_input_array(INPUT_GET, FILTER_SANITIZE_SPECIAL_CHARS);
        # preservando as ordenações
        if ($this->getParam('orderBy'))
            Session::set(CONTROLLER . ACTION . APPKEY . '.orderBy', $this->getParam('orderBy'));
        $or = Session::get(CONTROLLER . ACTION . APPKEY . '.orderBy');
        if (!empty($or) && !($this->getParam('orderBy'))) {
            $this->setParam('orderBy', Session::get(CONTROLLER . ACTION . APPKEY . '.orderBy'));
        }
    }

}
