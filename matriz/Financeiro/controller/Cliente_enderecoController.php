<?php
final class Cliente_enderecoController extends AppController{ 

    # página inicial do módulo Cliente_endereco
    function index(){
        $this->setTitle('Visualização de Cliente_endereco');
    }

    # lista de Cliente_enderecos
    # renderiza a visão /view/Cliente_endereco/all.php
    function all(){
        $this->setTitle('Listagem de Cliente_endereco');
        $p = new Paginate('Cliente_endereco', 10);
        $c = new Criteria();
        $c->addCondition('status','=',1);
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Cliente_enderecos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Clientes',  Cliente::getList());
    

    }

    # visualiza um(a) Cliente_endereco
    # renderiza a visão /view/Cliente_endereco/view.php
    function view(){
        $this->setTitle('Visualização de Cliente_endereco');
        try {
            $this->set('Cliente_endereco', new Cliente_endereco((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cliente_endereco', 'all');
        }
    }

    # formulário de cadastro de Cliente_endereco
    # renderiza a visão /view/Cliente_endereco/add.php
    function add(){
        $this->setTitle('Cadastro de Cliente_endereco');
        $this->set('Cliente_endereco', new Cliente_endereco);
        $this->set('Clientes',  Cliente::getList());
    }

    # recebe os dados enviados via post do cadastro de Cliente_endereco
    # (true)redireciona ou (false) renderiza a visão /view/Cliente_endereco/add.php
    function post_add(){
        $this->setTitle('Cadastro de Cliente_endereco');
        $Cliente_endereco = new Cliente_endereco();
        $this->set('Cliente_endereco', $Cliente_endereco);
        try {
            $user = Session::get('user');
            $Cliente_endereco->dtcadastro = date('Y-m-d H:i');
            $Cliente_endereco->cadastradopor = $user->id;
            $Cliente_endereco->status = 1;
            $Cliente_endereco->save($_POST);
            new Msg(__('Cliente_endereco cadastrado com sucesso'));
            $this->go('Cliente_endereco', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Clientes',  Cliente::getList());
    }

    # formulário de edição de Cliente_endereco
    # renderiza a visão /view/Cliente_endereco/edit.php
    function edit(){
        $this->setTitle('Edição de Cliente_endereco');
        try {
            $this->set('Cliente_endereco', new Cliente_endereco((int) $this->getParam('id')));
            $this->set('Clientes',  Cliente::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Cliente_endereco', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Cliente_endereco
    # (true)redireciona ou (false) renderiza a visão /view/Cliente_endereco/edit.php
    function post_edit(){
        $this->setTitle('Edição de Cliente_endereco');
        try {
            $user = Session::get('user');
            $Cliente_endereco = new Cliente_endereco((int) $_POST['id']);
            $Cliente_endereco->dtatualizacao = date('Y-m-d H:i');
            $Cliente_endereco->atualizadopor = $user->id;
            $this->set('Cliente_endereco', $Cliente_endereco);
            $Cliente_endereco->save($_POST);
            new Msg(__('endereço atualizado com sucesso'));
            $this->go('Cliente_endereco', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Clientes',  Cliente::getList());
    }

    # Confirma a exclusão ou não de um(a) Cliente_endereco
    # renderiza a /view/Cliente_endereco/delete.php
    function delete(){
        $this->setTitle('Apagar Cliente_endereco');
        try {
            $this->set('Cliente_endereco', new Cliente_endereco((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cliente_endereco', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Cliente_endereco
    # redireciona para Cliente_endereco/all
    function post_delete(){
        try {
            $Cliente_endereco = new Cliente_endereco((int) $_POST['id']);
            $Cliente_endereco->status = 2;
            $Cliente_endereco->save();
            new Msg(__('Cliente_endereco apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Cliente_endereco', 'all');
    }

}