<?php

final class UsuarioController extends AppController
{

    # p�gina inicial do m�dulo Usuario
    function index()
    {
        $this->setTitle('Usuario');
    }

    # lista de Usuarios
    # renderiza a vis�o /view/Usuario/all.php
    function all()
    {
        $this->setTitle('Usuarios');
        $p = new Paginate('Usuario', 15);
        //$this->set('search', NULL);
        $c = new Criteria();

//        if (isset($_GET['search'])) {
//            $c->addCondition('nome', 'LIKE', '%' . $_GET['search'] . '%');
//            $this->set('search', $this->getParam('search'));
//        }

        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }

        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }

        $this->set('Status', Status::getList());
        $this->set('Usuarios', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Usuario
    # renderiza a vis�o /view/Usuario/view.php
    function view()
    {
        try {
            $this->set('Usuario', new Usuario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'all');
        }
    }

    # formul�rio de cadastro de Usuario
    # renderiza a vis�o /view/Usuario/add.php
    function add()
    {
        $this->setTitle('Adicionar Usuario');
        $this->set('Usuario', new Usuario);
        $this->set('Modulos', Modulo::getList());
        $this->set('Usuario_modulo', new Usuario_modulo());
        $this->set('Rhlocalidade',  Rhlocalidade::getList());
        $c = new Criteria();
        $c->addCondition('situacao', '=', 1);
        $this->set('Estq_subestoque', Estq_subestoque::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Usuario
    # (true)redireciona ou (false) renderiza a vis�o /view/Usuario/add.php
    function post_add()
    {
        $this->setTitle('Adicionar Usuario');
        $Usuario = new Usuario();
        $this->set('Usuario', $Usuario);

        try {
            $_POST['senha'] = md5(Config::get('salt') . $_POST['senha']);
            $_POST['status'] = 1;
            $_POST['primeiro_acesso'] = 1;
            if($Usuario->save($_POST)){
                foreach ($_POST["modulo"] as $m) {
                    $Usuario_modulo = new Usuario_modulo();
                    $Usuario_modulo->usuario_id = $Usuario->id;
                    $Usuario_modulo->modulo_id = $m;
                    $Usuario_modulo->save();
                    //para salvar o usuario em uma localidade e 
                    //dar acesso a ele no sistema do RH para o filtro de funcionarios por localidade
                    if($m == 2){
                        foreach($_POST["rhlocalidade"] as $p) {
                            $Rhusuario_localidade = new Rhusuario_localidade();
                            $Rhusuario_localidade->usuario_id = $Usuario->id;
                            $Rhusuario_localidade->rhlocalidade_id =  $p;
                            $Rhusuario_localidade->situacao_id = 1;
                            $Rhusuario_localidade->dt_cadastro = date('Y-m-d H:i:s');
                            $Rhusuario_localidade->save();
                        }                        
                    }  
                    if($m == 4){
                        foreach($_POST['estq_subestoque'] as $g){
                            $Estq_usuario_substoque = new Estq_usuario_substoque();
                            $Estq_usuario_substoque->usario_id = $Usuario->id;
                            $Estq_usuario_substoque->substoque_id = $g;
                            $Estq_usuario_substoque->situacao_id = 1;
                            $Estq_usuario_substoque->dt_cadastro = date('Y-m-d H:i:s');
                            $Estq_usuario_substoque->save();
                        }
                    }                                
                }             
            }
            new Msg(__('Usuario cadastrado com sucesso '));
            $this->go('Usuario', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # formul�rio de edi��o de Usuario
    # renderiza a vis�o /view/Usuario/edit.php
    function edit()
    {
        $user = $this->getParam('id');
        $this->setTitle('Editar Usuario');
        try {
            $this->set('Usuario', new Usuario((int)$this->getParam('id')));
            $this->set('Status', Status::getList());
            
            $this->set('Modulos', Modulo::getList());   

            /**
             * filtra apenas os modulos dos usuarios
             */
            $g = new Criteria();
            $g->addCondition('usuario_id', '=', $user);
            $usuario_modulo = [];
            $usuarioModuloID = [];
            foreach(Usuario_modulo::getList($g) as $um) {
                $usuario_modulo[] = $um->modulo_id;
                $usuarioModuloID[] = $um->id;
            }
            $this->set('usuarioModuloID',  $usuarioModuloID);
            $this->set('Usuario_modulo',  $usuario_modulo);
            /**
             * filtra apenas os modulos dos usuarios da localidade 
             */
            $lo = new Criteria();
            $lo->addCondition('usuario_id', '=', $user);
            $rhusuario_localidade = [];
            $rhusuarioLocalidadeId = [];
            foreach(Rhusuario_localidade::getList($lo) as $localidade){
                $rhusuario_localidade[] = $localidade->rhlocalidade_id;
                $rhusuarioLocalidadeId[] = $localidade->id;
            }
            $this->set('rhusuarioLocalidadeId',  $rhusuarioLocalidadeId);
            $this->set('rhusuario_localidade',  $rhusuario_localidade);

            
            $this->set('Rhlocalidade',  Rhlocalidade::getList());

            $c = new Criteria();
            $c->addCondition('situacao', '=', 1);
            $this->set('Estq_subestoque', Estq_subestoque::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Usuario', 'all');
        }
    }

    # recebe os dados enviados via post da edi��o de Usuario
    # (true)redireciona ou (false) renderiza a vis�o /view/Usuario/edit.php
    function post_edit()
    {
      
        $this->setTitle('Editar Usuario');
        try {             
           /* foreach($_POST["rhusuarioLocalidadeId"] as $ll) {             
            
                var_dump($ll);
                //$localidadeDelete->delete();
            } exit;*/
            foreach ($_POST["usuarioModuloID"] as $tt) {
                $Usuario_modulo = new Usuario_modulo();
                $Usuario_modulo->id = $tt;
                $Usuario_modulo->delete();                
            }
             
            $Usuario = new Usuario((int)$_POST['id']);
            $this->set('Usuario', $Usuario);
            if($Usuario->senha != $_POST['senha']) {
                $_POST['senha'] = md5(Config::get('salt') . $_POST['senha']);
            }
            if($Usuario->save($_POST)){
                foreach ($_POST["modulo"] as $m) {
                    $Usuario_modulo = new Usuario_modulo();
                    $Usuario_modulo->usuario_id = $Usuario->id;
                    $Usuario_modulo->modulo_id = $m;
                    $Usuario_modulo->save();
                    //para salvar o usuario em uma localidade e 
                    //dar acesso a ele no sistema do RH para o filtro de funcionarios por localidade
                    if($m == 2){                      
                        //deleta antes de salvar para não repetir os dados 
                        foreach($_POST["rhusuarioLocalidadeId"] as $ll) {             
                            $Rhusuarioll = new Rhusuario_localidade();
                            $Rhusuarioll->id =  $ll;
                            $Rhusuarioll->delete();
                        }
                        foreach($_POST["rhlocalidade"] as $p) {
                            $Rhusuario_localidade = new Rhusuario_localidade();
                            $Rhusuario_localidade->usuario_id = $Usuario->id;
                            $Rhusuario_localidade->rhlocalidade_id =  $p;
                            $Rhusuario_localidade->situacao_id = 1;
                            $Rhusuario_localidade->dt_cadastro = date('Y-m-d H:i:s');
                            $Rhusuario_localidade->save();
                        }                        
                    }  
                    if($m == 4){
                        foreach($_POST['estq_subestoque'] as $g){
                            $Estq_usuario_substoque = new Estq_usuario_substoque();
                            $Estq_usuario_substoque->usario_id = $Usuario->id;
                            $Estq_usuario_substoque->substoque_id = $g;
                            $Estq_usuario_substoque->situacao_id = 1;
                            $Estq_usuario_substoque->dt_cadastro = date('Y-m-d H:i:s');
                            $Estq_usuario_substoque->save();
                        }
                    }                                
                }  
            }
            new Msg(__('Usuario atualizado com sucesso'));
            $this->go('Usuario', 'all');
        } catch (Exception $e) {
            new Msg(__('N�o foi poss�vel atualizar.'), 2);
        }
    }

    # Confirma a exclus�o ou n�o de um(a) Usuario
    # renderiza a /view/Usuario/delete.php
    function delete()
    {
        $this->setTitle('Apagar Usuario');
        try {
            $this->set('Usuario', new Usuario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Usuario
    # redireciona para Usuario/all
    function post_delete()
    {
        try {
            $Usuario = new Usuario((int)$_POST['id']);
            $Usuario->status = 3;
            $Usuario->save();
            new Msg(__('Usuario apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Usuario', 'all');
    }

    function alterar_senha()
    {
        $this->setTitle("Alterar Senha");
        $user = Session::get('user');
        $this->set('Usuario', $user);
    }

    function post_alterar_senha()
    {
        try {
            $Usuario = new Usuario((int)$_POST['id']);
            $Usuario->senha = md5(Config::get('salt') . $_POST['senha']);
            $Usuario->save();
            new Msg(__('Senha Alterada com'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Usuario', 'conta');
    }

    function resetar_senha()
    {
        $this->setTitle("Resetar Senha");
        $user = Session::get('user');
        $this->set('Usuario', $user);
    }

    function post_resetar_senha()
    {
        try {
            $Usuario = new Usuario((int)$_POST['id']);
            $Usuario->senha = md5(Config::get('salt') . $_POST['senha']);
            if($Usuario->primeiro_acesso == 1) {
                $Usuario->primeiro_acesso = 0;
            }

            $Usuario->save();
            new Msg(__('Senha resetada com sucesso!'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        Session::set('user', NULL);
        $this->go('Login', 'login');
    }


    function conta()
    {
        $this->setTitle('Conta');
        $user = Session::get('user');
        $this->set('Usuario', $user);
    }

}