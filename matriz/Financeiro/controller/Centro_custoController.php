<?php
final class Centro_custoController extends AppController{ 

    # página inicial do módulo Centro_custo
    function index(){
        $this->setTitle('Centro_custo');
    }

    # lista de Centro_custos
    # renderiza a visão /view/Centro_custo/all.php
    function all(){
        $this->setTitle('Centro Custos');
    }

    # visualiza um(a) Centro_custo
    # renderiza a visão /view/Centro_custo/view.php
    function view(){
        try {
            $this->set('Centro_custo', new Centro_custo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Centro_custo', 'all');
        }
    }

    # formulário de cadastro de Centro_custo
    # renderiza a visão /view/Centro_custo/add.php
    function add(){
        $this->setTitle('Adicionar Centro_custo');
        $this->set('Centro_custo', new Centro_custo);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Centro_custo
    # (true)redireciona ou (false) renderiza a visão /view/Centro_custo/add.php
    function post_add(){
        $this->setTitle('Adicionar Centro_custo');
        $Centro_custo = new Centro_custo();
        $this->set('Centro_custo', $Centro_custo);
        try {
            $Centro_custo->save($_POST);
            if(!empty($_POST['modal'])){
                echo $Centro_custo->id;
                exit;
            }
            new Msg(__('Centro_custo cadastrado com sucesso'));
            $this->go('Centro_custo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Centro_custo
    # renderiza a visão /view/Centro_custo/edit.php
    function edit(){
        $this->setTitle('Editar Centro_custo');
        try {
            $this->set('Centro_custo', new Centro_custo((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Centro_custo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Centro_custo
    # (true)redireciona ou (false) renderiza a visão /view/Centro_custo/edit.php
    function post_edit(){
        $this->setTitle('Editar Centro_custo');
        try {
            $Centro_custo = new Centro_custo((int) $_POST['id']);
            $this->set('Centro_custo', $Centro_custo);
            $Centro_custo->save($_POST);
            new Msg(__('Centro_custo atualizado com sucesso'));
            $this->go('Centro_custo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Recebe o id via post e exclui um(a) Centro_custo
    # redireciona para Centro_custo/all
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $Centro_custo = new Centro_custo((int) $_POST['id']);
            $Centro_custo->status=3;
            $Centro_custo->save();
            $ret['result'] = true;
            $ret['msg'] = 'Centro de custo apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Centro de custo !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function combo(){
        $id = (!empty($_POST['id'])) ? $_POST['id'] : "";
        $Centro_custo = Centro_custo::getList();
        $option = "<option value=''>Selecione</option>";
        foreach ($Centro_custo as $d) {
            if($d->id==$id)
                $option.="<option value='".$d->id."' selected>".$d->descricao."</option>";
            else
                $option .= "<option value=".$d->id.">".$d->descricao."</option>";
        }
        echo $option;
        exit;
    }

    function getCentros(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM centro_custo AS c WHERE c.status <> 3";
            $sql = "SELECT c.id, c.descricao, s.descricao AS status
                    FROM centro_custo AS c INNER JOIN status AS s ON (s.id = c.status)
                    WHERE c.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND c.descricao LIKE :termo";
                $sql .= " AND c.descricao LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.descricao";
                $sort[] = array('descricao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }
}