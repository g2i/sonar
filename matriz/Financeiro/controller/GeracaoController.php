<?php

final class GeracaoController extends AppController
{
    # página inicial do módulo Fornecedor

    function gerar_pagar()
    {
        $this->setTitle('Geração de programações');

        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $a->addCondition('tipo', '=', "A pagar");
        $a->addSqlConditions('IF(prazoDeterminado IS NOT NULL,(geradas < prazoDeterminado OR geradas IS NULL),((geradas IS NULL OR geradas = 0) OR proximaGeracao = "' . date('Y-m-d') . '"))');

        $programacoes = Programacao::getList($a);


        foreach ($programacoes as $p) {
            //caso nao aja prazo determinado
            if (empty($p->prazoDeterminado) || $p->prazoDeterminado <= 0) {
                //caso a periodicidade seja menor que 31 dias irei gerar 12 parcelas
                if ($p->getPeriodicidade()->dias <= 31) {
                    $cont_per = 12;
                } else {
                    $cont_per = 6;
                }
                $inicio = !empty($p->primeiro_vencimento) ? $p->primeiro_vencimento : date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime(date('Y-m-d'))));

                //caso ja tenha mais de uma conta e a proxima geracao seja hoje
                if ($p->proximaGeracao == date('Y-m-d')) {
                    $cont_per = 1;
                    $b = new Criteria();
                    $b->addCondition('idProgramacao', '=', $p->id);
                    $b->setOrder('vencimento DESC');
                    $contas_pagar = Contaspagar::getFirst($b);
                    $inicio = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime($contas_pagar->vencimento)));
                    if(!empty($p->diaVencimento) && $p->getPeriodicidade()->dias > 15)
                        $inicio = tratar_ultimos_dias_do_mes($p->diaVencimento,$inicio);

//                    $feriados = getFeriados(date('Y', strtotime($inicio)));
//                    $inicio = tratar_feriados($inicio, $feriados);
                }
            } else {
                $cont_per = $p->prazoDeterminado;
                if (!empty($p->geradas)) {
                    $b = new Criteria();
                    $b->addCondition('idProgramacao', '=', $p->id);
                    $b->setOrder('vencimento DESC');
                    $contas_pagar = Contaspagar::getFirst($b);
                    $inicio = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime($contas_pagar->vencimento)));
                    if(!empty($p->diaVencimento) && $p->getPeriodicidade()->dias > 15)
                        $inicio = tratar_ultimos_dias_do_mes($p->diaVencimento,$inicio);
//                    $feriados = getFeriados(date('Y', strtotime($inicio)));
//                    $inicio = tratar_feriados($inicio, $feriados);
                } else {
                    $inicio = !empty($p->primeiro_vencimento) ? $p->primeiro_vencimento : date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime(date('Y-m-d'))));
//                    $feriados = getFeriados(date('Y', strtotime($inicio)));
//                    $inicio = tratar_feriados($inicio, $feriados);
                }

            }
            $aux_x = !empty($p->geradas) ? $p->geradas : 1;
            for ($x = $aux_x; $x <= $cont_per; $x++) {
                $concasPagar = new Contaspagar();
                $concasPagar->data = !empty($p->data_documento) ? $p->data_documento : date('Y-m-d');
                if ($x > $aux_x) {
                    $inicio = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime($inicio)));
                    if(!empty($p->diaVencimento) && $p->getPeriodicidade()->dias > 15)
                        $inicio = tratar_ultimos_dias_do_mes($p->diaVencimento,$inicio);
//                    $feriados = getFeriados(date('Y', strtotime($inicio)));
//                    $inicio = tratar_feriados($inicio, $feriados);
                }

                $concasPagar->fin_projeto_unidade_id = $p->fin_projeto_unidade_id;
                $concasPagar->vencimento = $inicio;
                $concasPagar->valor = $p->valorParcela;
                $concasPagar->juros = 0;
                $concasPagar->multa = 0;
                $concasPagar->dataPagamento = NULL;
                $concasPagar->idPlanoContas = $p->idPlanoContas;
                $concasPagar->idFornecedor = $p->idFornecedor;
                $concasPagar->status = 1;
                $concasPagar->complemento = $p->complemento;
                $concasPagar->valorBruto = $p->valorParcela;
                $concasPagar->desconto = 0;
                $concasPagar->tipoPagamento = $p->tipoPagamento;
                $concasPagar->tipoDocumento = $p->tipoDocumento;
                $concasPagar->idProgramacao = $p->id;
                $concasPagar->contabilidade = $p->contabilidade;
                $concasPagar->numerodocumento = $p->mumero_documento;
                $concasPagar->data_cadastro = date('Y-m-d H:i:s');
                $concasPagar->parcela = $x . '/' . $cont_per;

                if ($concasPagar->save()) {

                    $programacao = new Programacao($p->id);
                    $programacao->geradas = $programacao->geradas + 1;
                    if ($x == 1) {
                        if (empty($p->prazoDeterminado) || $p->prazoDeterminado <= 0) {
                            $proxima = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime($inicio)));
                            if(!empty($p->diaVencimento) && $p->getPeriodicidade()->dias > 15)
                                $proxima = tratar_ultimos_dias_do_mes($p->diaVencimento,$proxima);
                            $programacao->proximaGeracao = $proxima;
                        }
                    }
                    $programacao->ultimaGeracao = date('Y-m-d');
                    $programacao->save();


                    $c = new Criteria();
                    $c->addCondition('programacao', '=', $p->id);
                    $c->addCondition('status', '=', 1);
                    $rateios = Rateio_programacao::getList($c);
                    if (!empty($rateios)) {
                        foreach ($rateios as $r) {
                            $rateio_pagar = new Rateio_contaspagar();
                            $rateio_pagar->contar_pagar = $concasPagar->id;
                            $rateio_pagar->valor = $r->valor;
                            $rateio_pagar->observacao = $r->observacao;
                            $rateio_pagar->empresa = $r->empresa;
                            $rateio_pagar->centro_custo = $r->centro_custo;
                            $rateio_pagar->status = $r->status;
                            $rateio_pagar->data_documento = $concasPagar->data;
                            $rateio_pagar->fornecedor_id = $concasPagar->idFornecedor;
                            $rateio_pagar->plano_contas_id = $concasPagar->idPlanoContas;
                            $rateio_pagar->save();
                        }
                    }

                    $d = new Criteria();
                    $d->addCondition('programacao_id', '=', $p->id);
                    $deducoes = DeducoesProgramacao::getList($d);
                    if (!empty($deducoes)) {
                        foreach ($deducoes as $deducoe) {
                            $deducoes_pagar = new DeducoesContaspagar();
                            $deducoes_pagar->deducoes_id = $deducoe->deducoes_id;
                            $deducoes_pagar->valor = $deducoe->valor;
                            $deducoes_pagar->contaspagar_id = $concasPagar->id;
                            $deducoes_pagar->save();
                        }
                    }

                }
            }
        }

        exit;
    }

    function gerar_receber()
    {
        $this->setTitle('Geração de Contas Receber');

        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $a->addCondition('tipo', '=', "A receber");
        $a->addSqlConditions('IF(prazoDeterminado IS NOT NULL,(geradas < prazoDeterminado OR geradas IS NULL),((geradas IS NULL OR geradas = 0) OR proximaGeracao = "' . date('Y-m-d') . '"))');

        $programacoes = Programacao::getList($a);

        if (!empty($programacoes)) {
            foreach ($programacoes as $p) {
                //caso nao aja prazo determinado
                if (empty($p->prazoDeterminado) || $p->prazoDeterminado <= 0) {
                    //caso a periodicidade seja menor que 31 dias irei gerar 12 parcelas
                    if ($p->getPeriodicidade()->dias <= 31) {
                        $cont_per = 12;
                    } else {
                        $cont_per = 6;
                    }
                    $inicio = !empty($p->primeiro_vencimento) ? $p->primeiro_vencimento : date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime(date('Y-m-d'))));
                    //caso ja tenha mais de uma conta e a proxima geracao seja hoje
                    if ($p->proximaGeracao == date('Y-m-d')) {
                        $cont_per = 1;
                        $b = new Criteria();
                        $b->addCondition('idProgramacao', '=', $p->id);
                        $b->setOrder('vencimento DESC');
                        $contas_receber = Contasreceber::getFirst($b);
                        $inicio = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime($contas_receber->vencimento)));
                        $feriados = getFeriados(date('Y', strtotime($inicio)));
                        $inicio = tratar_feriados($inicio, $feriados);
                    }
                } else {
                    $cont_per = $p->prazoDeterminado;
                    if (!empty($p->geradas)) {
                        $b = new Criteria();
                        $b->addCondition('idProgramacao', '=', $p->id);
                        $b->setOrder('vencimento DESC');
                        $contas_receber = Contasreceber::getFirst($b);
                        $inicio = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime($contas_receber->vencimento)));
                        $feriados = getFeriados(date('Y', strtotime($inicio)));
                        $inicio = tratar_feriados($inicio, $feriados);
                    } else {
                        $inicio = !empty($p->primeiro_vencimento) ? $p->primeiro_vencimento : date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime(date('Y-m-d'))));
                        $feriados = getFeriados(date('Y', strtotime($inicio)));
                        $inicio = tratar_feriados($inicio, $feriados);
                    }
                }

                $aux_x = !empty($p->geradas) ? $p->geradas : 1;
                for ($x = $aux_x; $x <= $cont_per; $x++) {
                    $concasReceber = new Contasreceber();
                    $concasReceber->data = !empty($p->data_documento) ? $p->data_documento : date('Y-m-d');
                    if ($x > $aux_x) {
                        $inicio = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime($inicio)));
                        $feriados = getFeriados(date('Y', strtotime($inicio)));
                        $inicio = tratar_feriados($inicio, $feriados);
                    }

                    $concasReceber->valor = $p->valorParcela;
                    $concasReceber->juros = 0;
                    $concasReceber->multa = 0;
                    $concasReceber->dataPagamento = NULL;
                    $concasReceber->idPlanoContas = $p->idPlanoContas;
                    $concasReceber->idCliente = $p->idCliente;
                    $concasReceber->status = 1;
                    $concasReceber->complemento = $p->complemento;
                    $concasReceber->valorBruto = $p->valorParcela;
                    $concasReceber->desconto = 0;
                    $concasReceber->tipoPagamento = $p->tipoPagamento;
                    $concasReceber->tipoDocumento = $p->tipoDocumento;
                    $concasReceber->idProgramacao = $p->id;
                    $concasReceber->vencimento = $inicio;
                    $concasReceber->contabilidade = $p->contabilidade;
                    $concasReceber->data_cadastro = date('Y-m-d H:i:s');
                    $concasReceber->numerodocumento = $p->mumero_documento;
                    if (empty($p->prazoDeterminado) || $p->prazoDeterminado <= 0) {
                        $concasReceber->parcela = date('m/Y', strtotime($inicio));
                    } else {
                        $concasReceber->parcela = $x . '/' . $cont_per;
                    }


                    if ($concasReceber->save()) {
                        $programacao = new Programacao($p->id);
                        $programacao->geradas = $programacao->geradas + 1;
                        if ($x == 1) {
                            if (empty($p->prazoDeterminado) || $p->prazoDeterminado <= 0) {
                                $proxima = date('Y-m-d', strtotime('+' . $p->getPeriodicidade()->dias . ' days', strtotime(date('Y-m-d'))));
                                $feriados = getFeriados(date('Y', strtotime($proxima)));
                                $proxima = tratar_feriados($proxima, $feriados);
                                $programacao->proximaGeracao = $proxima;
                            }
                        }
                        $programacao->ultimaGeracao = date('Y-m-d');
                        $programacao->save();


                        $c = new Criteria();
                        $c->addCondition('programacao', '=', $p->id);
                        $c->addCondition('status', '=', 1);
                        $rateios = Rateio_programacao::getList($c);
                        if (!empty($rateios)) {
                            foreach ($rateios as $r) {
                                $rateio_receber = new Rateio_contasreceber();
                                $rateio_receber->contas_receber = $concasReceber->id;
                                $rateio_receber->valor = $r->valor;
                                $rateio_receber->observacao = $r->observacao;
                                $rateio_receber->empresa = $r->empresa;
                                $rateio_receber->centro_custo = $r->centro_custo;
                                $rateio_receber->status = $r->status;
                                $rateio_receber->data_documento = $concasReceber->data;
                                $rateio_receber->cliente_id = $concasReceber->idCliente;
                                $rateio_receber->plano_contas_id = $concasReceber->idPlanoContas;
                                $rateio_receber->save();

                            }
                        }

                        $d = new Criteria();
                        $d->addCondition('programacao_id', '=', $p->id);
                        $deducoes = DeducoesProgramacao::getList($d);
                        if (!empty($deducoes)) {
                            foreach ($deducoes as $deducoe) {
                                $deducoes_receber = new DeducoesContaspagar();
                                $deducoes_receber->deducoes_id = $deducoe->deducoes_id;
                                $deducoes_receber->percentual = $deducoe->percentual;
                                $deducoes_receber->contasreceber_id = $concasReceber->id;
                                $deducoes_receber->save();
                            }
                        }

                    }
                }
            }
            //  echo '<script>window.close();</script>';
        }
        exit;
    }

}