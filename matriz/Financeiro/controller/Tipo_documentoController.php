<?php
final class Tipo_documentoController extends AppController{ 

    # página inicial do módulo Tipo_documento
    function index(){
        $this->setTitle('Tipo_documento');
    }

    # lista de Tipo_documentos
    # renderiza a visão /view/Tipo_documento/all.php
    function all(){
        $this->setTitle('Tipo_documentos');
    }

    # visualiza um(a) Tipo_documento
    # renderiza a visão /view/Tipo_documento/view.php
    function view(){
        try {
            $this->set('Tipo_documento', new Tipo_documento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Tipo_documento', 'all');
        }
    }

    # formulário de cadastro de Tipo_documento
    # renderiza a visão /view/Tipo_documento/add.php
    function add(){
        $this->setTitle('Adicionar Tipo_documento');
        $this->set('Tipo_documento', new Tipo_documento);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Tipo_documento
    # (true)redireciona ou (false) renderiza a visão /view/Tipo_documento/add.php
    function post_add(){
        $this->setTitle('Adicionar Tipo_documento');
        $Tipo_documento = new Tipo_documento();
        $this->set('Tipo_documento', $Tipo_documento);
        try {
            $Tipo_documento->save($_POST);
            new Msg(__('Tipo_documento cadastrado com sucesso'));
            $this->go('Tipo_documento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Tipo_documento
    # renderiza a visão /view/Tipo_documento/edit.php
    function edit(){
        $this->setTitle('Editar Tipo_documento');
        try {
            $this->set('Tipo_documento', new Tipo_documento((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Tipo_documento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Tipo_documento
    # (true)redireciona ou (false) renderiza a visão /view/Tipo_documento/edit.php
    function post_edit(){
        $this->setTitle('Editar Tipo_documento');
        try {
            $Tipo_documento = new Tipo_documento((int) $_POST['id']);
            $this->set('Tipo_documento', $Tipo_documento);
            $Tipo_documento->save($_POST);
            new Msg(__('Tipo_documento atualizado com sucesso'));
            $this->go('Tipo_documento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Confirma a exclusão ou não de um(a) Tipo_documento
    # renderiza a /view/Tipo_documento/delete.php
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $Tipo_documento = new Tipo_documento((int) $_POST['id']);
            $Tipo_documento->status_id = 3;
            $Tipo_documento->save();
            $ret['result'] = true;
            $ret['msg'] = 'Centro de custo apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Centro de custo !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function getDocumentos(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM tipo_documento AS d WHERE d.status_id <> 3";
            $sql = "SELECT d.id, d.descricao, s.descricao AS status
                    FROM tipo_documento AS d INNER JOIN status AS s ON (s.id = d.status_id)
                    WHERE d.status_id <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND d.descricao LIKE :termo";
                $sql .= " AND d.descricao LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY d.descricao";
                $sort[] = array('descricao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }
}