<?php
final class BoletoController extends AppController
{
    # página inicial do módulo Banco

    function index()
    {
        $this->setTitle('Banco');
    }

    function boleto(){
        $this->setTitle('Boleto');
        $id = (!empty($_POST['id'])) ? ($_POST['id']) : 0;
        $ced= (!empty($_POST['cedente'])) ? ($_POST['cedente']) : 0;
        $vencimento = (!empty($_POST['vencBoleto'])) ? ($_POST['vencBoleto']) : "";
        $print = (!empty($_POST['print'])) ? ($_POST['print']) : "";
        $this->set("print",$print);
        $valorPagar = (!empty($_POST['recalculado'])) ? ($_POST['recalculado']) : "";
        $valorPagar = str_replace('.','',$valorPagar);
        $valorPagar = str_replace(',','.',$valorPagar);

        $juros = (!empty($_POST['tjuros'])) ? ($_POST['tjuros']) : "";
        $juros = str_replace('.','',$juros);
        $juros = str_replace(',','.',$juros);

        $multa = (!empty($_POST['tmulta'])) ? ($_POST['tmulta']) : "";
        $multa = str_replace('.','',$multa);
        $multa = str_replace(',','.',$multa);

        $desconto= (!empty($_POST['desconto'])) ? ($_POST['desconto']) : "";
        $desconto = str_replace('.','',$desconto);
        $desconto = str_replace(',','.',$desconto);

        $perJuros= (!empty($_POST['juros'])) ? ($_POST['juros']) : "";
        $perMulta= (!empty($_POST['multa'])) ? ($_POST['multa']) : "";


        $Bol = new Contasreceber($id);
        $Bol->impresso = 1;
        $Bol->boletocedente = $ced;
        $Bol->vencimentoBoleto= $vencimento;
        $Bol->valorPagar= $valorPagar;
        $Bol->juros= $juros;
        $Bol->multa= $multa;
        $Bol->desconto=$desconto;
        $Bol->perjuros=$perJuros;
        $Bol->permulta=$perMulta;
        $Bol->save();

        $c = new Contasreceber($id);
        $c->getDeducoes();
        $this->set('Contareceber',$c);
        $Cedente = new Cedente($ced);
        $this->set('Cedente',$Cedente);


    }

    function viewBoleto() {
        $this->setTitle('Gerar Boleto');
        try {
            $c = new Criteria();
            $c->addCondition('status','<>',3);
            $this->set('Cedente',Cedente::getList($c));
            $Contas = new Contasreceber($this->getParam('id'));
            $email  = $Contas->getCliente()->e_mailcob;
            $this->set("email",$email);

            $this->set('Contas',$Contas);
            $parametros = Parametros::getFirst();

            $this->set("perJuros",$parametros->juros);
            $this->set("perMulta",$parametros->multa);

            $perJuros = $parametros->juros/100;
            $perMulta = $parametros->multa/100;

            $j = floatval(($Contas->valor*$perJuros)/30);
            $m = floatval($Contas->valor*$perMulta);

            $juros=0.00;
            $multa=0.00;
            $inicio = $Contas->vencimento;
            $fim = date('Y-m-d');
            $diferen = strtotime($fim) - strtotime($inicio);
            $dias = floor($diferen / (60 * 60 * 24));
            $this->set("dias",$dias);
            if($dias>0){
                $juros = floatval($j*$dias);
                $multa = floatval($m);
            }
            $recalc = $Contas->valor+$juros+$multa-$Contas->desconto;
            $this->set("recalculado",$recalc);

            $this->set('juros',$juros);
            $this->set('multa',$multa);

        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Contasreceber', 'all');
        }
    }

    function enviar_email(){
        $id = (!empty($_POST['id'])) ? ($_POST['id']) : 0;
        $ced= (!empty($_POST['cedente'])) ? ($_POST['cedente']) : 0;
        $vencimento = (!empty($_POST['vencBoleto'])) ? ($_POST['vencBoleto']) : "";

        $valorPagar = (!empty($_POST['recalculado'])) ? ($_POST['recalculado']) : "";
        $valorPagar = str_replace('.','',$valorPagar);
        $valorPagar = str_replace(',','.',$valorPagar);

        $juros = (!empty($_POST['tjuros'])) ? ($_POST['tjuros']) : "";
        $juros = str_replace('.','',$juros);
        $juros = str_replace(',','.',$juros);

        $multa = (!empty($_POST['tmulta'])) ? ($_POST['tmulta']) : "";
        $multa = str_replace('.','',$multa);
        $multa = str_replace(',','.',$multa);

        $desconto= (!empty($_POST['desconto'])) ? ($_POST['desconto']) : "";
        $desconto = str_replace('.','',$desconto);
        $desconto = str_replace(',','.',$desconto);

        $perJuros= (!empty($_POST['juros'])) ? ($_POST['juros']) : "";
        $perMulta= (!empty($_POST['multa'])) ? ($_POST['multa']) : "";

        $Bol = new Contasreceber($id);
        $Bol->impresso = 1;
        $Bol->boletocedente = $ced;
        $Bol->vencimentoBoleto= $vencimento;
        $Bol->valorPagar= $valorPagar;
        $Bol->juros= $juros;
        $Bol->multa= $multa;
        $Bol->desconto=$desconto;
        $Bol->perjuros=$perJuros;
        $Bol->permulta=$perMulta;
        $Bol->save();

        $Contareceber = new Contasreceber($id);
        $Cliente = $Contareceber->getCliente();
        $planoContas = $Contareceber->getPlanocontas();
        $email = (!empty($_POST['email'])) ? ($_POST['email']) : "";
            $mail = new PHPMailer();
            $mail->IsSMTP(); // Define que a mensagem será SMTP
            $mail->Host = "vps.chiesaonline.com.br";
            $mail->SMTPAuth = true;
            $mail->Username = 'contato@chiesaonline.com.br';
            $mail->Password = 'chiesa6990';
            $mail->From = "contato@chiesaonline.com.br";
            $mail->FromName = "Chiesa Advogados Associados";

            $mail->AddAddress($email, $Cliente->nomeetiqueta);
//            $mail->AddAddress('e-mail@destino2.com.br');
//            $mail->AddCC('copia@dominio.com.br', 'Copia');
//            $mail->AddBCC('CopiaOculta@dominio.com.br', 'Copia Oculta');

            $mail->IsHTML(true);
            $mail->CharSet = 'iso-8859-1';
//          Texto e Assunto
            $mail->Subject = "Envio de Boleto"; // Assunto da mensagem
            $mail->Body = '<table>';
            $mail->Body .= '<tr>';
            $mail->Body = '<td colspan="2"><IMG src="http://chiesaonline.com.br/chiesaweb/img/logo_chiesa.jpg" alt="Chiesa Advodados Associados" class="wp-smiley" style="margin-right: 20px"><td>';
            $mail->Body .='<td><IMG src="http://chiesaonline.com.br/chiesaweb/img/exitoGestor.png" alt="Êxito Gestor Jurídico" class="wp-smiley"><td>';
            $mail->Body .= '</tr>';
            $mail->Body .= '<tr>';
            $mail->Body .= '<td colspan="3"><p><strong>Ol&aacute;!</strong></p></td>';
            $mail->Body .= '</tr>';
            $mail->Body .= '<tr>';
            $mail->Body .= '<td colspan="3"><p>Segue o link para visualizar o boleto referente a <strong>'.$planoContas->nome.'</strong>, com vencimento em: '.convertDataSQL4BR($Contareceber->vencimento).'</p></td>';
            $mail->Body .= '</tr>';
            $mail->Body .= '<tr>';
            $mail->Body .= "<td colspan='3'><a href='" . $_SERVER['HTTP_HOST'] . "/" . SITE_PATH . "/index.php?m=Boleto&p=gerar&i=" . Cript::cript(Config::get('salt') . $id) . "'>Imprimir Boleto</a></td>";
            $mail->Body .= '<tr>';
            $mail->Body .= '</table>';
//            Anexos (opcional)
//            $mail->AddAttachment("e:\home\login\web\documento.pdf", "novo_nome.pdf");

            $enviado = $mail->Send();
//            Limpa os destinatários e os anexos
            $mail->ClearAllRecipients();
            $mail->ClearAttachments();

            if ($enviado) {
                new Msg(__('Boleto enviado com sucesso!'), 1);
            } else {
                new Msg("Erro ao enviar o boleto.", 3);
                new Msg("Informações de erro: " . $mail->ErrorInfo, 3);
            }
            $this->go("Contasreceber", "all");
    }

    function gerar(){
        $this->setTitle("Imprimir Boleto");
        $id = (int)str_replace(Config::get('salt'), '', Cript::decript($_GET['i']));
        $this->set("print",1);
        $c = new Contasreceber($id);
        $c->getDeducoes();
        $cedente = $c->getCedente();
        $this->set('Contareceber',$c);
        $this->set('Cedente',$cedente);
    }
}