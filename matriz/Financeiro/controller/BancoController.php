<?php

final class BancoController extends AppController {
    # página inicial do módulo Banco
    function index() {
        $this->setTitle('Banco');
    }

    # lista de Bancos
    # renderiza a visão /view/Banco/all.php

    function all() {
        $this->setTitle('Bancos');
    }

    # visualiza um(a) Banco
    # renderiza a visão /view/Banco/view.php

    function view() {
        try {
            $this->set('Banco', new Banco((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Banco', 'all');
        }
    }

    # formulário de cadastro de Banco
    # renderiza a visão /view/Banco/add.php

    function add() {
        $this->setTitle('Adicionar Banco');
        $this->set('Banco', new Banco);
    }

    # recebe os dados enviados via post do cadastro de Banco
    # (true)redireciona ou (false) renderiza a visão /view/Banco/add.php

    function post_add() {
        $this->setTitle('Adicionar Banco');
        $Banco = new Banco();
        $this->set('Banco', $Banco);
        try {
            $_POST['saldo'] = getAmount($_POST['saldo']);
            $_POST['limite'] = getAmount($_POST['limite']);
            $_POST['status'] =1;
            $Banco->save($_POST);
            new Msg(__('Banco cadastrado com sucesso'));
            $this->go('Banco', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # formulário de edição de Banco
    # renderiza a visão /view/Banco/edit.php

    function edit() {
        $this->setTitle('Editar Banco');
        try {
            $this->set('Banco', new Banco((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Banco', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Banco
    # (true)redireciona ou (false) renderiza a visão /view/Banco/edit.php

    function post_edit() {
        $this->setTitle('Editar Banco');
        try {
            $Banco = new Banco((int) $_POST['id']);
            $this->set('Banco', $Banco);
            $_POST['saldo'] = getAmount($_POST['saldo']);
            $_POST['limite'] = getAmount($_POST['limite']);
            $Banco->save($_POST);
            new Msg(__('Banco atualizado com sucesso'));
            $this->go('Banco', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Recebe o id via post e exclui um(a) Banco
    # redireciona para Banco/all

    function delete() {
        $ret = array();
        $ret['result'] = false;

        try {
            $Banco = new Banco((int) $_POST['id']);
            $Banco->status=3;
            $Banco->save();
            $ret['result'] = true;
            $ret['msg'] = 'Banco apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Banco !';
            $ret['erro'] = $e->getMessage();
        }

        $this->returnAsJson($ret);
    }

    function contabilidades()
    {
        $this->setTitle('Contabilidades');
    }

    function addContabilidade() {
        $ret = array();
        $ret['result'] = false;

        try {
            $contabilidade = new ContabilidadeBancos();
            $contabilidade->banco = $_POST['banco'];
            $contabilidade->contabilidade = $_POST['contabilidade'];
            $contabilidade->save();
            $ret['result'] = true;
            $ret['msg'] = 'Empresa adicionada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao adiciona empresa !';
            $ret['erro'] = $e->getTraceAsString();
        }

        $this->returnAsJson($ret);
    }

    function deleteContabilidade() {
        $ret = array();
        $ret['result'] = false;

        try {
            $contabilidade = new ContabilidadeBancos((int)$_POST['id']);
            $contabilidade->delete();
            $ret['result'] = true;
            $ret['msg'] = 'Empresa removida com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao remover empresa !';
            $ret['erro'] = $e->getMessage();
        }

        $this->returnAsJson($ret);
    }

    function getBancos(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM banco AS b WHERE b.status <> 3";
            $sql = "SELECT b.id, b.nome, b.agencia, b.conta, b.saldo, b.limite,
                    if(b.mostra_fluxo, 'Sim', 'N&atilde;o') AS fluxo,
                    IF(b.mostra_saldo_geral, 'Sim', 'N&atilde;o') AS geral
                    FROM banco AS b INNER JOIN status AS s ON (s.id = b.status)
                    WHERE b.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND b.nome LIKE :termo";
                $sql .= " AND b.nome LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY b.nome";
                $sort[] = array('nome' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            //echo $sqlCount;
            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
        }catch (Exception $e){
            $ret['erro'] = $e->getMessage();
            $ret['trace'] = $e->getTraceAsString();
        }

        $this->returnAsJson($ret);
    }

    function getContabilidades(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $banco = $this->getParam('banco');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM contabilidade_banco AS b
                         LEFT JOIN contabilidade AS c ON (c.id = b.contabilidade)
                         WHERE b.banco = :banco";

            $sql = "SELECT b.id, c.nome FROM contabilidade_banco AS b
                    LEFT JOIN contabilidade AS c ON (c.id = b.contabilidade)
                    WHERE b.banco = :banco";

            if (!empty($search)) {
                $sqlCount .= " AND c.nome LIKE :termo";
                $sql .= " AND c.nome LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.nome";
                $sort[] = array('nome' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            $db->bind(":banco", $banco);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            $db->bind(":banco", $banco);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;
        }catch (Exception $e){
            $ret['erro'] = $e->getMessage();
        }

        $this->returnAsJson($ret);
    }

    function getContBanco(){
        $ret = array();
        $ret['result'] = false;

        try{

            $banco = $this->getParam('id');
            $sql = "SELECT c.id, c.nome FROM contabilidade AS c
                WHERE c.id NOT IN(SELECT b.contabilidade
                FROM contabilidade_banco AS b WHERE b.banco = :banco)";

            $db = $this::getConn();
            $db->query($sql);

            $db->bind(":banco", $banco);
            $ret['dados'] = $db->getResults();
            $ret['result'] = true;
        }catch (Exception $e){
            $ret['error'] = $e->getTraceAsString();
        }

        $this->returnAsJson($ret);
    }
}
