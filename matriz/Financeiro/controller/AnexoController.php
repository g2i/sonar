<?php
final class AnexoController extends AppController{ 

    # lista de Anexos
    # renderiza a visão /view/Anexo/all.php
    function all(){
        # seta o titulo
        $this->setTitle('Anexos');
        
        # inicia a pagina��o
        $p = new Paginate('Anexo', 10);
        
        # inicia o criteria
        $c = new Criteria();
        
        # verifica se foi setado o id externo
        if ($this->getParam('id_externo')) {
            $c->addCondition('id_externo', '=', $this->getParam('id_externo'));
            
        }
        
        #
        # 1: Contas a pagar
        # 2: Contas a receber
        # 3: Movimenta��o
        # 
        # verifica se foi setado o tipo
        if ($this->getParam('tipo')) {
            $c->addCondition('tipo', '=', $this->getParam('tipo'));
            $this->set('tipo',$this->getParam('tipo'));
        }
        
        # seta a ordem
        $c->setOrder('titulo');
       $externo = $this->getParam('id_externo');
        # recupera os anexos com a criteria $c
        $this->set('Anexos', Anexo::getList($c));
        $this->set('id_externo', $externo);
    }
    # formulário de cadastro de Anexo
    function add(){
        $p = new Paginate('Anexo', 10);
        
        # inicia o criteria
        $c = new Criteria();
         # verifica se foi setado o id externo
         if ($this->getParam('id_externo')) {
            $c->addCondition('id_externo', '=', $this->getParam('id_externo'));
            $this->set('id_externo', $this->getParam('id_externo'));
        }
        #
        # 1: Contas a pagar
        # 2: Contas a receber
        # 3: Movimenta��o
        # 
        # verifica se foi setado o tipo
        if ($this->getParam('tipo')) {
            $c->addCondition('tipo', '=', $this->getParam('tipo'));
            $this->set('tipo',$this->getParam('tipo'));
        }
     }
    # recebe os dados enviados via post do cadastro de Anexo
    # (true)redireciona ou (false) renderiza a visão /view/Anexo/add.php
    function post_anexo(){
        # variavel que vai salvar os erros do upload
        $erros = array();
        
        for($i=0;$i<count($_POST['add_anexo']);$i++)
        {
            try
            {
                $FileUploader[] = new FileUploader( array(
                        "name"      => $_FILES['add_anexo']['name'][$i]['arquivo'],
                        "type"      => $_FILES['add_anexo']['type'][$i]['arquivo'],
                        "tmp_name"  => $_FILES['add_anexo']['tmp_name'][$i]['arquivo'],
                        "error"     => $_FILES['add_anexo']['error'][$i]['arquivo'],
                        "size"      => $_FILES['add_anexo']['size'][$i]['arquivo'],
                    ), 25000000 /* 25mb */, array('zip','doc','docx','pdf','rar','png','jpg','jpeg','gif','odt','txt','xls','xlsx')
                ); 
            }  catch ( Exception $e ){
                $erros[] = $_FILES['add_anexo']['name'][$i]['arquivo'] . " - " . $e->getMessage();
            }
        }
        
        # caso tenha acontecido algum erro, trava, e o mostra
        if(count($erros) > 0){
            echo implode('<br /> ', $erros); exit;
        }
        
        # inicio do processo de upload dos anexos
        
        # variavel de retorno para o ajax
        $retorno = false;
        
        # percorre todos os anexos inseridos
        for($i=0;$i<count($_POST['add_anexo']);$i++)
        {
            $FileUploader = new FileUploader( array(
                    "name"      => $_FILES['add_anexo']['name'][$i]['arquivo'],
                    "type"      => $_FILES['add_anexo']['type'][$i]['arquivo'],
                    "tmp_name"  => $_FILES['add_anexo']['tmp_name'][$i]['arquivo'],
                    "error"     => $_FILES['add_anexo']['error'][$i]['arquivo'],
                    "size"      => $_FILES['add_anexo']['size'][$i]['arquivo'],
                ), 25000000 /* 25mb */, array('zip','doc','docx','pdf','rar','png','jpg','jpeg','gif','odt','txt','xls','xlsx')
            ); 

            $nome_arquivo = uniqid() . "." . pathinfo($_FILES['add_anexo']['name'][$i]['arquivo'], PATHINFO_EXTENSION);
            $FileUploader->save($nome_arquivo, "anexos");

            $Anexo = new Anexo();
            $Anexo->save(array(
                "titulo"        => $_POST['add_anexo'][$i]['titulo'],
                "caminho"       => SITE_PATH . "/uploads/anexos/" . $nome_arquivo,
                "tipo"          => $_POST['add_anexo'][$i]['tipo'],
				"status_id"		=> 1,
                "id_externo"  => $_POST['add_anexo'][$i]['id_externo']
            )); $retorno = true;
        }

        # caso nao occorreu nenhum erro, printa true
        if( $retorno ) echo "true"; 
        
        # trava o processo para nao renderizar a view pois esta por ajax
        exit;
    }

    # Recebe o id via post e exclui um(a) Anexo
    # redireciona para Anexo/all
    function post_delete(){
        try {
            $Anexo = new Anexo((int) $this->getParam('id'));
            $id_externo = $Anexo->id_externo;
            $tipo = $Anexo->tipo;
            
            if(file_exists($_SERVER['DOCUMENT_ROOT'] . $Anexo->caminho)) unlink($_SERVER['DOCUMENT_ROOT'] . $Anexo->caminho);

            $Anexo->delete();
            $HTML = new Html();
            new Msg(__('Anexo apagado com sucesso'), 1);
            echo "<script type=\"text/javascript\">
                        $(function(){
                            $(\"div.modal\").find(\".modal-content\").load(\"".$HTML->getUrl("Anexo", "all",array( "id_externo" => $id_externo, "ajax"=>"true",'tipo'=>$tipo )) ."\");
                        })
                    </script>";
            exit;
        } catch (Exception $e){
            new Msg($e->getMessage(),3);
        } $this->go('Anexo', 'all');
    }
    
 

}