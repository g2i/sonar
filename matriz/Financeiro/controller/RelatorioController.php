<?php

final class RelatorioController extends AppController
{
# página inicial do módulo Movimento

    function index()
    {
        $this->setTitle('Relatorio');
    }

    function Meses()
    {
        $mes = array();
        $mes[1] = "Jan";
        $mes[2] = "Fev";
        $mes[3] = "Mar";
        $mes[4] = "Abr";
        $mes[5] = "Mai";
        $mes[6] = "Jun";
        $mes[7] = "Jul";
        $mes[8] = "Ago";
        $mes[9] = "Set";
        $mes[10] = "Out";
        $mes[11] = "Nov";
        $mes[12] = "Dez";
        return $mes;
    }

    function mensal_()
    {
        $mes = array();
        $mes[1] = "Janeiro";
        $mes[2] = "Fevereiro";
        $mes[3] = "Março";
        $mes[4] = "Abril";
        $mes[5] = "Maio";
        $mes[6] = "Junho";
        $mes[7] = "Julho";
        $mes[8] = "Agosto";
        $mes[9] = "Setembro";
        $mes[10] = "Outubro";
        $mes[11] = "Novembro";
        $mes[12] = "Dezembro";
        $this->set("Mes", $mes);

        $this->setTitle("Demonstrativo mensal");
        $where = "WHERE M.`status` <> 3 AND CC.tipo=1 ";
        $whereSaida = "WHERE M.`status` <> 3 AND CC.tipo=2 ";
        if (!empty($_GET['mes'])) {
            $where .= " AND MB.`mes` = " . $_GET['mes'];
            $whereSaida .= " AND MB.`mes` = " . $_GET['mes'];
        }
        if (!empty($_GET['ano'])) {
            $where .= " AND MB.`ano` = " . $_GET['ano'];
            $whereSaida .= " AND MB.`ano` = " . $_GET['ano'];
        }
        if (!empty($_GET['empresa'])) {
            $where .= " AND M.`idContabilidade` = " . $_GET['empresa'];
            $whereSaida .= " AND M.`idContabilidade` = " . $_GET['empresa'];
        }


        $this->set("Classificacao", Classificacao::getList());

        $query = $this->query('SELECT MB.`id`,E.`nome` AS empresa,MB.`data`,MB.`status`,PC.`nome`,CC.`descricao`,CC.`tipo`
                                FROM movimento M
                                LEFT JOIN movimento_banco MB
                                ON MB.id = M.`idMovimentoBanco`
                                LEFT JOIN planocontas PC
                                ON PC.id = M.`idPlanoContas`
                                LEFT JOIN classificacaocontas CC
                                ON CC.id = PC.`classificacao`
                                LEFT JOIN contabilidade E
                                ON E.`id` = M.`idContabilidade`
                            ' . $where . ' GROUP BY M.`idMovimentoBanco`');

        $querySaidas = $this->query('SELECT MB.`id`,E.`nome` AS empresa,MB.`data`,MB.`status`,PC.`nome`,CC.`descricao`,CC.`tipo`
                                    FROM movimento M
                                    LEFT JOIN movimento_banco MB
                                    ON MB.id = M.`idMovimentoBanco`
                                    LEFT JOIN planocontas PC
                                    ON PC.id = M.`idPlanoContas`
                                    LEFT JOIN classificacaocontas CC
                                    ON CC.id = PC.`classificacao`
                                    LEFT JOIN contabilidade E
                                    ON E.`id` = M.`idContabilidade`
                                    ' . $whereSaida . ' GROUP BY M.`idMovimentoBanco` ');

        $this->set("MovimentoBanco", $query);
        $this->set("MovimentoSaida", $querySaidas);
        $this->set("Empresa", Contabilidade::getList());

    }

    function mensal()
    {
        $mes = array();
        $mes[1] = "Janeiro";
        $mes[2] = "Fevereiro";
        $mes[3] = "Março";
        $mes[4] = "Abril";
        $mes[5] = "Maio";
        $mes[6] = "Junho";
        $mes[7] = "Julho";
        $mes[8] = "Agosto";
        $mes[9] = "Setembro";
        $mes[10] = "Outubro";
        $mes[11] = "Novembro";
        $mes[12] = "Dezembro";
        $this->set("Mes", $mes);

        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $grupos = GrupoContabilidade::getList($a);
        $this->set('grupos', $grupos);
        $this->setTitle("Demonstrativo mensal");
    }

    function Demonstrativo()
    {
        $sqlClassificacao = "SELECT CC.`id`,CC.`descricao`
                             FROM classificacaocontas CC
                             WHERE CC.`status` = 1 AND CC.considera = 1
                             AND CC.tipo = " . $_POST['tipo'] . "  ORDER BY CC.`descricao`";

        $sqlPlano = "SELECT PC.`nome` AS planoContas, PC.id,M.`idContabilidade`,
				     SUM(IF(M.`credito` IS NULL,0,M.`credito`)) AS creditoPL,
                     SUM(IF(M.`debito` IS NULL,0,M.`debito`)) AS debitoPL
                     FROM movimento M
                     LEFT JOIN movimento_banco MB ON MB.`id` = M.`idMovimentoBanco`
                     LEFT JOIN planocontas PC ON PC.id = M.`idPlanoContas`
                     LEFT JOIN classificacaocontas CC ON CC.`id` = PC.`classificacao`
                     LEFT JOIN banco B ON B.`id` = M.`banco`
                     WHERE M.`status` = 1";

        $movSql = "SELECT M.`data`,M.`credito`,M.`debito`, 
                   IFNULL(IF(M.tipo = 1, f.nome, c.nome), 'Não Informado') AS nome,
                   B.`nome` AS banco,M.`complemento`, M.`idMovimentoBanco` FROM movimento M 
                   LEFT JOIN movimento_banco MB ON MB.id = M.`idMovimentoBanco` 
                   LEFT JOIN banco B ON B.`id` = M.banco
                   LEFT JOIN cliente AS c ON (c.id = M.idCliente)
                   LEFT JOIN fornecedor AS f ON (f.id = M.idFornecedor)
                   WHERE M.`status` = 1";

        if (!empty($_POST['mes'])) {
            $sqlPlano .= " AND MB.`mes` = '" . $_POST['mes'] . "' ";
            $movSql .= " AND MB.`mes` = '" . $_POST['mes'] . "' ";
        }

        if (!empty($_POST['ano'])) {
            $sqlPlano .= " AND MB.`ano` = '" . $_POST['ano'] . "' ";
            $movSql .= " AND MB.`ano` = '" . $_POST['ano'] . "' ";
        }

        if (!empty($_POST['grupo']) && empty($_POST['empresa'])) {
            $sqlPlano .= " AND M.`idContabilidade`
                     IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = " . $_POST['grupo'] . ")";
            $movSql .= " AND M.`idContabilidade`
                     IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = " . $_POST['grupo'] . ")";
        } else if (!empty($_POST['empresa'])) {
            $sqlPlano .= " AND M.`idContabilidade` IN(" . $_POST['empresa'] . ")";
            $movSql .= " AND M.`idContabilidade` IN(" . $_POST['empresa'] . ")";
        }

        $Classificacao = $this->query($sqlClassificacao);
        $html = "<div class='col-md-12'>";
        $html .= "<table class='table'><thead><tr>
                <td colspan='4'></td></tr></thead><tbody>";

        $TGcredito = 0;
        $TGdebito = 0;
        foreach ($Classificacao as $c) {
            $sql = $sqlPlano;
            $sql .= " AND PC.`classificacao` = " . $c->id;
            $sql .= " GROUP BY PC.`nome` ORDER BY PC.`nome`";

            //echo $sqlPlano;
            $PlanoContas = $this->query($sql);

            $creditoClass = 0;
            $debitoClass = 0;
            $html .= "<tr><td colspan='4'><div class='col-md-12'>";
            $html .= "   <table class='table table-hover''>
                               <thead>
                               <tr>
                               <td style='width: 377px;'><h3> " . $c->descricao . "</h3></td>
                               <td style='width: 280px; text-align: right;'><h3> Crédito</h3></td>
                               <td style='width: 300px; text-align: right;'><h3> Débito</h3></td>
                               <td style='width: 300px; text-align: right;'></td>
                               </tr>
                               </thead>
                               <tbody>";
            foreach ($PlanoContas as $p) {

                $html .= "<tr>
                              <td> " . $p->planoContas . "</td>
                              <td style='text-align: right;'>R$ " . number_format($p->creditoPL, 2, ',', '.') . "</td>
                              <td style='text-align: right;'>R$ " . number_format($p->debitoPL, 2, ',', '.') . "</td>
                                <td style='text-align: right;'>
                                <div class='dropdown' style='overflow: visible !important;'>
                                  <span class='dropdown abrirDropdown' data-toggle='dropdown' id=" . $p->id . $_POST['tipo'] . "><a>Movimentos</a><span class='caret'></span>
                                  </span>
                                  <ul class='dropdown-menu' role='menu' aria-labelledby=" . $p->id . $_POST['tipo'] . "
                                  style='left: -555px; min-width: 800px;'";
                $html .= "<li role='presentation'>
                            <div class='table-responsive'>
                            <table class='table table-hover' style='overflow: auto;'>
                                  <thead>
                                    <tr>
                                        <td style='font-weight: bold; text-align: center;'>Data</td>
                                        <td style='font-weight: bold; text-align: center;'>Credor/Cliente</td>
                                        <td style='font-weight: bold; text-align: center;'>Banco</td>
                                        <td style='font-weight: bold; text-align: center;'>Complemento</td>
                                        <td style='font-weight: bold; text-align: center; max-width: 130px'>Crédito</td>
                                        <td style='font-weight: bold; text-align: center; max-width: 130px'>Débito</td>
                                        <td style='font-weight: bold; text-align: center; max-width: 130px'>Total</td>
                                    </tr>
                                  </thead>
                                  <tbody>";

                $sql = $movSql;
                $sql .= " AND M.`idPlanoContas` = " . $p->id;
                $sql .= " ORDER BY M.data,B.`nome";
                $movimento = $this->query($sql);
                $totalCredt = 0;
                $totalDeb = 0;
                //echo  $movSql;
                foreach ($movimento as $mov) {
                    $html .= "<tr>
                               <td style='padding-left: 0px'>" . convertDataSQL4BR($mov->data) . "</td>
                               <td style='overflow: hidden;'>" . $mov->nome . "</td>
                               <td style='overflow: hidden;'>" . $mov->banco . "</td>
                               <td style='overflow: hidden;'>" . $mov->complemento . "</td>
                               <td style='text-align: right;'>R$ " . number_format($mov->credito, 2, ',', '.') . "</td>
                               <td style='text-align: right;'>R$ " . number_format($mov->debito, 2, ',', '.') . "</td>
                               <td style='text-align: right;'>R$ " . number_format($mov->credito - $mov->debito, 2, ',', '.') . "</td>
                             </tr>";
                    $totalCredt += $mov->credito;
                    $totalDeb += $mov->debito;
                }
                $html .= "</tbody>
                               <tfoot>
                                <tr>
                                    <td colspan='6'></td>
                                    <td style='text-align: right;'>
                                        <strong>Total: R$ " . number_format($totalCredt - $totalDeb, 2, ',', '.') . "</strong>
                                    </td>
                                </tr>
                              </tfoot>
                    </table></div></li>";
                $creditoClass += $totalCredt;
                $debitoClass += $totalDeb;
                $html .= "</ul>
                                </div>
                                </td>";
                $html .= "</td></tr>";
            }
            $html .= "<tfoot>
                          <tr>
                            <td></td>
                            <td style='text-align: right;'>R$ " . number_format($creditoClass, 2, ',', '.') . "</span></td>
                            <td style='text-align: right;'>R$ " . number_format($debitoClass, 2, ',', '.') . "</span></td>
                            <td style='text-align: right;'>R$ " . number_format(($creditoClass - $debitoClass), 2, ',', '.') . "</td>
                          </tr>
                        </tfoot>";

            $html .= "</tbody>
                       </table>
                    </div>
                    </tr>";

            $TGcredito += $creditoClass;
            $TGdebito += $debitoClass;
        }
        $html .= "</tbody>";

        if ($_POST['tipo'] == 1) {
            $html .= "<tfoot>
                        <tr>
                            <td><h4>Total de Entrada</h4></td>
                            <td style='text-align: right; padding-left: 45px;'><h4 id='TEcredito'>R$ " . number_format($TGcredito, 2, ',', '.') . "</h4></td>
                            <td style='text-align: right; padding-left: 30px;'><h4 id='TEdebito'>R$ " . number_format($TGdebito, 2, ',', '.') . "</h4></td>
                            <td style='text-align: right; padding-right: 30px;'><h4 id='TEsoma'>R$ " . number_format(($TGcredito - $TGdebito), 2, ',', '.') . "</h4></td>
                        </tr>
                    <tfoot>";
        } else {
            $html .= "<tfoot>
                        <tr>
                            <td><h4>Total de Saída</h4></td>
                            <td style='text-align: right; padding-left: 110px;'><h4 id='TScredito'>R$ " . number_format($TGcredito, 2, ',', '.') . "</h4></td>
                            <td style='text-align: right; padding-left: 30px'><h4 id='TSdebito'>R$ " . number_format($TGdebito, 2, ',', '.') . "</h4></td>
                            <td style='text-align: right; padding-right: 30px;'><h4 id='TSsoma'>R$ " . number_format(($TGcredito - $TGdebito), 2, ',', '.') . "</h4></td>
                        </tr>
                    <tfoot>";
        }
        $html .= "</table></div>";
        echo $html;
        exit;
    }

    function Movimentos()
    {
        $query = $this->query('SELECT M.`data`,B.nome as banco,M.`credito`,M.`debito`
                        FROM movimento M
                        LEFT JOIN planocontas PC
                        ON PC.`id` = M.`idPlanoContas`
                        LEFT JOIN classificacaocontas CC
                        ON CC.`id` = PC.`classificacao`
                        LEFT JOIN banco B
                        ON B.id = M.banco
                        WHERE CC.`tipo` = ' . $_POST['mov'] . ' AND M.`idMovimentoBanco` = ' . $_POST['idMovimentoBanco'] . ' AND M.`status`<> 3 AND M.`idPlanoContas` = ' . $_POST['planoContas']);

        $html = "";

        $html .= "<li><a><strong>
                    <div class='row' style='padding-top:5px '>
                        <div class='col-md-2' style='padding-left: 5px'>Data</div>
                        <div class='col-md-4'>Banco</div>
                        <div class='col-md-2'>Crédito</div>
                        <div class='col-md-2'>Débito</div>
                        <div class='col-md-2'>Total</div>
                    </div>
                </strong></a></li>

<li class='divider'></li>";
        $totalCredt = 0;
        $totalDeb = 0;
        foreach ($query as $mov) {
            $html .= "
<li><a>
        <div class='row text-left'>
            <div class='col-md-2'>" . convertDataSQL4BR($mov->data) . "</div>
            <div class='col-md-4' style='overflow: hidden;'>" . $mov->banco . "</div>
            <div class='col-md-2 text-right'> R$ " . number_format($mov->credito, 2, ',', '.') . "</div>
            <div class='col-md-2 text-right'> R$ " . number_format($mov->debito, 2, ',', '.') . "</div>
            <div class='col-md-2 text-right'> R$ " . number_format($mov->credito - $mov->debito, 2, ',', '.') . "</div>
            ";
            $html .= "
        </div>
    </a></li>";
            $totalCredt = $totalCredt + $mov->credito;
            $totalDeb = $totalDeb + $mov->debito;
        }
        $html .= "
<li><a>
        <div class='row'>
            <div class='col-md-12 text-right'><strong>Total: R$
                    " . number_format($totalCredt - $totalDeb, 2, ',', '.') . "</strong></div>
        </div>
    </a></li>";
        $totalCredt = 0;
        $totalDeb = 0;

        echo $html;
        exit;

    }

    function mapa()
    {
        $this->setTitle("Mapa Anual");

        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $grupos = GrupoContabilidade::getList($a);
        $this->set('grupos', $grupos);
    }

    function dadosMapa()
    {
        $b = new Criteria();
        $b->addCondition('status', '=', 1);
        $b->addCondition('tipo', '=', 1);
        $b->addCondition('considera', '=', 1);
        $Classificacao_entrada = Classificacao::getList($b);

        $c = new Criteria();
        $c->addCondition('status', '=', 1);
        $c->addCondition('tipo', '=', 2);
        $c->addCondition('considera', '=', 1);
        $Classificacao_saida = Classificacao::getList($c);

        $meses = $this->meses();
        $ano = $_POST['ano'];
        $grupo = $_POST['grupo'];
        $empresa = $_POST['empresa'];

        $retorno = '<div class="col-md-12">';
        $retorno .= '<h2>Entradas</h2>';
        $retorno .= '<div class="col-md-12">';

        foreach ($Classificacao_entrada as $c) {
            $retorno .= '<h3>' . $c->descricao . '</h3>';
            $retorno .= '<div class="table-responsive">';
            $retorno .= '<table class="table table-hover">';
            $retorno .= '<tr><th>Plano Contas</th>';
            $tot = array();
            for ($i = 1; $i <= 12; $i++) {
                $retorno .= '<th class="text-right">' . $meses[$i] . '</th>';
                $tot[$i] = 0;
            }
            $retorno .= '<th class="text-right">Média</th>';
            $retorno .= '<th class="text-right">Total</th></tr>';
            $med = 0;
            $sum = 0;
            $med_geral = 0;
            $sum_geral = 0;
            foreach ($c->getPlanocontas(1) as $p) {
                $retorno .= '<tr>';
                $retorno .= '<td>' . $p->nome . '</td>';
                for ($i = 1; $i <= 12; $i++) {
                    $movimento = Movimento::mapa($ano, $grupo, $empresa, $i, $p->id);
                    if (!empty($movimento)) {
                        $retorno .= '<td class="text-right">' . number_format($movimento->total, 2, ',', '.') . '</td>';
                        $med++;
                        $sum += $movimento->total;
                        $tot[$i] = $tot[$i] + $movimento->total;
                    } else {
                        $tot[$i] = $tot[$i] + 0;
                        $retorno .= '<td class="text-right">0.00</td>';
                    }
                }

                if ($med != 0)
                    $retorno .= '<td class="text-right">' . number_format(($sum / $med), 2, ',', '.') . '</td>';
                else
                    $retorno .= '<td class="text-right">0.00</td>';

                $retorno .= '<td class="text-right">' . number_format($sum, 2, ',', '.') . '</td>';
                $med_geral += $med;
                $sum_geral += $sum;
                $med = 0;
                $sum = 0;
                $retorno .= '<tr>';
            }

            $retorno .= '<tr class="active">';
            $retorno .= '<td>&nbsp;</td>';
            foreach ($tot as $t) {
                $retorno .= '<td class="text-right">' . number_format($t, 2, ',', '.');
            }

            if ($med_geral != 0)
                $retorno .= '<td class="text-right">' . number_format(($sum_geral / 12), 2, ',', '.') . '</td>';
            else
                $retorno .= '<td class="text-right">0.00</td>';
            $retorno .= '<td class="text-right">' . number_format($sum_geral, 2, ',', '.') . '</td>';
            $retorno .= '</tr>';
            $retorno .= '</table></div>';
        }

        $retorno .= '</div></div><div class="col-md-12"><h2>Saídas</h2><div class="col-md-12">';
        foreach ($Classificacao_saida as $c) {
            $retorno .= '<h3>' . $c->descricao . '</h3>';
            $retorno .= '<div class="table-responsive"><table class="table table-hover">';
            $retorno .= '<tr><th>Plano Contas</th>';
            $tot = array();
            for ($i = 1; $i <= 12; $i++) {
                $retorno .= '<th class="text-right">' . $meses[$i] . '</th>';
                $tot[$i] = 0;
            }
            $retorno .= '<th class="text-right">Média</th><th class="text-right">Total</th></tr>';
            $med = 0;
            $sum = 0;
            $med_geral = 0;
            $sum_geral = 0;
            foreach ($c->getPlanocontas(1) as $p) {
                $retorno .= '<tr>';
                $retorno .= '<td>' . $p->nome . '</td>';
                for ($i = 1; $i <= 12; $i++) {
                    $movimento = Movimento::mapa($ano, $grupo, $empresa, $i, $p->id);
                    if (!empty($movimento)) {
                        $retorno .= '<td class="text-right">' . number_format($movimento->total, 2, ',', '.') . '</td>';
                        $med++;
                        $sum += $movimento->total;
                        $tot[$i] = $tot[$i] + $movimento->total;
                    } else {
                        $tot[$i] = $tot[$i] + 0;
                        $retorno .= '<td class="text-right">0.00</td>';
                    }
                }

                if ($med != 0)
                    $retorno .= '<td class="text-right">' . number_format(($sum / $med), 2, ',', '.') . '</td>';
                else
                    $retorno .= '<td class="text-right">0.00</td>';
                $retorno .= '<td class="text-right">' . number_format($sum, 2, ',', '.') . '</td>';
                $med_geral += $med;
                $sum_geral += $sum;
                $med = 0;
                $sum = 0;
                $retorno .= '<tr>';
            }

            $retorno .= '<tr class="active">';
            $retorno .= '<td>&nbsp;</td>';
            foreach ($tot as $t) {
                $retorno .= '<td class="text-right">' . number_format($t, 2, ',', '.');
            }

            if ($med_geral != 0)
                $retorno .= '<td class="text-right">' . number_format(($sum_geral / 12), 2, ',', '.') . '</td>';
            else
                $retorno .= '<td class="text-right">0.00</td>';

            $retorno .= '<td class="text-right">' . number_format($sum_geral, 2, ',', '.') . '</td>';
            $retorno .= '</tr></table></div>';
        }

        $retorno .= '</div></div>';

        echo $retorno;
        exit;
    }

    function simplificado()
    {
        $this->setTitle("Mapa Simplificado");

        $a = new Criteria();
        $a->addCondition('status', '=', 1);
        $grupos = GrupoContabilidade::getList($a);
        $this->set('grupos', $grupos);
    }

    function dadosSimplificado()
    {
        $b = new Criteria();
        $b->addCondition('status', '=', 1);
        $b->addCondition('tipo', '=', 1);
        $b->addCondition('considera', '=', 1);
        $Classificacao_entrada = Classificacao::getList($b);

        $c = new Criteria();
        $c->addCondition('status', '=', 1);
        $c->addCondition('tipo', '=', 2);
        $c->addCondition('considera', '=', 1);
        $Classificacao_saida = Classificacao::getList($c);

        $meses = $this->meses();
        $ano = $_POST['ano'];
        $grupo = $_POST['grupo'];
        $empresa = $_POST['empresa'];

        $retorno = '<div class="col-md-12">';
        $retorno .= '<h2>Entradas</h2>';
        $retorno .= '<div class="col-md-12">';
        $retorno .= '<div class="table-responsive"><table class="table table-hover">';
        $retorno .= '<tr><th>Classificação</th>';
        $tot = array();
        for ($i = 1; $i <= 12; $i++) {
            $retorno .= '<th class="text-right">' . $meses[$i] . '</th>';
            $tot[$i] = 0;
        }
        $retorno .= '<th class="text-right">Média</th><th class="text-right">Total</th></tr>';
        $med = 0;
        $sum = 0;
        $med_geral = 0;
        $sum_geral = 0;
        foreach ($Classificacao_entrada as $c) {
            $retorno .= '<tr>';
            $retorno .= '<td>' . $c->descricao . '</td>';
            for ($i = 1; $i <= 12; $i++) {
                $movimento = Movimento::simplificado($ano, $grupo, $empresa, $i, $c->id);
                if (!empty($movimento)) {
                    $med++;
                    $sum += $movimento->total;
                    $retorno .= '<td class="text-right">' . number_format($movimento->total, 2, ',', '.') . '</td>';
                    $tot[$i] = $tot[$i] + $movimento->total;
                } else {
                    $retorno .= '<td class="text-right">0.00</td>';
                }
            }
            if ($med != 0)
                $retorno .= '<td class="text-right">' . number_format(($sum / $med), 2, ',', '.') . '</td>';
            else
                $retorno .= '<td class="text-right">0.00</td>';
            $retorno .= '<td class="text-right">' . number_format($sum, 2, ',', '.') . '</td>';
            $med_geral += $med;
            $sum_geral += $sum;
            $med = 0;
            $sum = 0;
            $retorno .= '</tr>';
        }

        $retorno .= '<tr class="active">';
        $retorno .= '<td>&nbsp;</td>';
        foreach ($tot as $t) {
            $retorno .= '<td class="text-right">' . number_format($t, 2, ',', '.');
        }
        if ($med_geral != 0)
            $retorno .= '<td class="text-right">' . number_format(($sum_geral / 12), 2, ',', '.') . '</td>';
        else
            $retorno .= '<td class="text-right">0.00</td>';
        $retorno .= '<td class="text-right">' . number_format($sum_geral, 2, ',', '.') . '</td>';
        $retorno .= '</tr>';
        $retorno .= '</table></div></div></div>';
        $retorno .= '<div class="col-md-12"><h2>Saídas</h2>';
        $retorno .= '<div class="col-md-12"><div class="table-responsive">';
        $retorno .= '<table class="table table-hover"><tr><th>Classificação</th>';
        $tot = array();
        for ($i = 1; $i <= 12; $i++) {
            $retorno .= '<th class="text-right">' . $meses[$i] . '</th>';
            $tot[$i] = 0;
        }
        $retorno .= '<th class="text-right">Média</th><th class="text-right">Total</th></tr>';
        $med_geral = 0;
        $sum_geral = 0;
        foreach ($Classificacao_saida as $c) {
            $retorno .= '<tr>';
            $retorno .= '<td>' . $c->descricao . '</td>';
            for ($i = 1; $i <= 12; $i++) {
                $movimento = Movimento::simplificado($ano, $grupo, $empresa, $i, $c->id);
                if (!empty($movimento)) {
                    $med++;
                    $sum += $movimento->total;
                    $retorno .= '<td class="text-right">' . number_format($movimento->total, 2, ',', '.') . '</td>';
                    $tot[$i] = $tot[$i] + $movimento->total;
                } else {
                    $retorno .= '<td class="text-right">0.00</td>';
                }
            }
            if ($med != 0)
                $retorno .= '<td class="text-right">' . number_format(($sum / $med), 2, ',', '.') . '</td>';
            else
                $retorno .= '<td class="text-right">0.00</td>';
            $retorno .= '<td class="text-right">' . number_format($sum, 2, ',', '.') . '</td>';
            $med_geral += $med;
            $sum_geral += $sum;
            $med = 0;
            $sum = 0;
            $retorno .= '</tr>';
        }
        $retorno .= '<tr class="active">';
        $retorno .= '<td>&nbsp;</td>';
        foreach ($tot as $t) {
            $retorno .= '<td class="text-right">' . number_format($t, 2, ',', '.');
        }
        if ($med_geral != 0)
            $retorno .= '<td class="text-right">' . number_format(($sum_geral / 12), 2, ',', '.') . '</td>';
        else
            $retorno .= '<td class="text-right">0.00</td>';
        $retorno .= '<td class="text-right">' . number_format($sum_geral, 2, ',', '.') . '</td>';
        $retorno .= '</tr></table></div></div></div>';

        echo $retorno;
        exit;
    }

    function contas_pagar_por_vencimento()
    {

        $inicio = $this->getParam('inicio');
        $fim = $this->getParam('fim');
        $fornecedor = $this->getParam('fornecedor');
        $empresa = $this->getParam('empresa');
        $plano = $this->getParam('plano');
        $pagamento = $this->getParam('pagamento');

        $this->setTemplate('reports');

        $sql = "SELECT E.nome AS empresa,
                    CP.`data`,
                    CP.`vencimento`,
                    PL.`nome`,
                    F.`nome` AS fornecedor,
                    CP.`valor`,
                    CP.`dataPagamento`,
                    CP.`complemento` AS complemento,
                    CP.numerodocumento,
                    TP.descricao AS tipo_pagamento
                FROM contaspagar CP
                LEFT JOIN planocontas PL ON PL.`id` = CP.`idPlanoContas`
                LEFT JOIN fornecedor F ON F.`id` = CP.`idFornecedor`
                INNER JOIN tipo_pagamento TP ON TP.id = CP.tipoPagamento
                INNER JOIN contabilidade E ON E.id = CP.contabilidade
                WHERE CP.`status` = 1";

        if ($inicio != -1)
            $sql .= " AND CP.`vencimento` >= '{$inicio}'";

        if ($fim != -1)
            $sql .= " AND CP.`vencimento` <= '{$fim}'";


        if ($fornecedor != -1)
            $sql .= " AND CP.`idFornecedor` = {$fornecedor}";


        if ($empresa != -1)
            $sql .= " AND CP.`contabilidade` = {$empresa}";


        if ($plano != -1)
            $sql .= " AND CP.`idPlanoContas` = {$plano}";

        if($pagamento == 2)
            $sql .= " AND CP.`dataPagamento` IS  NULL ";
        else
            $sql .= " AND CP.`dataPagamento` IS NOT NULL ";

        $json = array();
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);


        $data = $this->query($sql);
        $conta =array();

        foreach($data AS $value){
            $conta['empresa'] = $value->empresa;
            $conta['data'] = DataBR($value->data);
            $conta['vencimento'] = DataBR($value->vencimento);
            $conta['nome'] = $value->nome;
            $conta['fornecedor'] = html_entity_decode($value->fornecedor);
            $conta['valor'] = $value->valor;
            $conta['dataPagamento'] = DataBR($value->dataPagamento);    
            $conta['complemento'] = html_entity_decode($value->complemento);
            $conta['numerodocumento'] = $value->numerodocumento;
            $conta['tipo_pagamento'] = $value->tipo_pagamento;
            $contas[] = $conta;
        }
        $json['relatorio']['contas'] = $contas;


        $reportCreate = new Json();

        $reportCreate->create('reportContasPagarPorVencimento.json', json_encode($json));

    }

    function contas_pagar_geral(){
        $inicio = $this->getParam('inicio');
        $fim = $this->getParam('fim');
        $fornecedor = $this->getParam('fornecedor');
        $empresa = $this->getParam('empresa');
        $plano = $this->getParam('plano');
        $pagamento = $this->getParam('pagamento');

        $this->setTemplate('reports');

        $sql = "SELECT E.nome AS empresa,
                    CP.`data`,
                    CP.`vencimento`,
                    PL.`nome`,
                    F.`nome` AS fornecedor,
                    CP.`valor`,
                    CP.`dataPagamento`,
                    CP.`complemento` AS complemento,
                    CP.numerodocumento,
                    TP.descricao AS tipo_pagamento
                FROM contaspagar CP
                LEFT JOIN planocontas PL ON PL.`id` = CP.`idPlanoContas`
                LEFT JOIN fornecedor F ON F.`id` = CP.`idFornecedor`
                INNER JOIN tipo_pagamento TP ON TP.id = CP.tipoPagamento
                INNER JOIN contabilidade E ON E.id = CP.contabilidade
                WHERE CP.`status` = 1";


        if ($inicio != -1)
                    $sql .= " AND CP.`vencimento` >= '{$inicio}'";

        if ($fim != -1)
            $sql .= " AND CP.`vencimento` <= '{$fim}'";


        if ($fornecedor != -1)
            $sql .= " AND CP.`idFornecedor` = {$fornecedor}";


        if ($empresa != -1)
            $sql .= " AND CP.`contabilidade` = {$empresa}";


        if ($plano != -1)
            $sql .= " AND CP.`idPlanoContas` = {$plano}";

        if($pagamento == 2)
            $sql .= " AND CP.`dataPagamento` IS  NULL ";
        else
            $sql .= " AND CP.`dataPagamento` IS NOT NULL ";



        $json = array();
        $json['relatorio']['inicio'] = DataBR($inicio);
        $json['relatorio']['fim'] = DataBR($fim);

        $sql.= " ORDER BY CP.`vencimento` ASC,F.`nome` ASC ";

        $data = $this->query($sql);
        $conta =array();

        foreach($data AS $value){
            $conta['empresa'] = $value->empresa;
            $conta['data'] = DataBR($value->data);
            $conta['vencimento'] = DataBR($value->vencimento);
            $conta['nome'] = $value->nome;
            $conta['fornecedor'] = html_entity_decode($value->fornecedor);
            $conta['valor'] = $value->valor;
            $conta['dataPagamento'] = DataBR($value->dataPagamento);
            $conta['complemento'] = html_entity_decode($value->complemento);
            $conta['numerodocumento'] = $value->numerodocumento;
            $conta['tipo_pagamento'] = $value->tipo_pagamento;
            $contas[] = $conta;
        }
        $json['relatorio']['contas'] = $contas;


        $reportCreate = new Json();

        $reportCreate->create('reportContasPagarGeral.json', json_encode($json));
    }
}