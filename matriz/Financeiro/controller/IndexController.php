<?php

class IndexController extends AppController {

    public function index(){
        $a = new Criteria();
        $a->addCondition('status','=',1);
        $grupos = GrupoContabilidade::getList($a);

        $this->set('inicio', '01/'.date('m/Y'));
        $this->set('fim', date('t/m/Y'));
        $this->set('grupos', $grupos);
    }

    public function keepAlive()
    {
        session_start();
        exit;
    }

    public function dadosGrafico(){
        $empresa = $_GET['empresa'];
        $inicio = date('Y-m-d', strtotime("-90 days"));
        $fim = date('Y-m-d', strtotime("+90 days"));

        $contasPagar = Contaspagar::graficoMes($inicio, $fim, $empresa);
        $contasReceber = Contasreceber::graficoMes($inicio, $fim, $empresa);
        $movimento = Movimento::graficoMes($inicio, $fim, $empresa);

        $ret = array();
        $ret["contasPagar"] = array();
        foreach ($contasPagar as $c) {
            $ret["contasPagar"][] = array($c->mes, $c->valor);
        }

        $ret["contasReceber"] = array();
        foreach ($contasReceber as $c) {
            $ret["contasReceber"][] = array($c->mes, $c->valor);
        }

        $ret["movimento"] = array();
        foreach ($movimento as $c) {
            $ret["movimento"]["debito"][] = array($c->mes, $c->debito);
            $ret["movimento"]["credito"][] = array($c->mes, $c->credito);
            $ret["movimento"]["saldo"][] = array($c->mes, $c->credito - $c->debito);
        }

        echo json_encode($ret);
        exit;
    }

    public function getFluxo(){

        $inicio = !empty($_GET['inicio']) ? new DateTime(convertDataBR4SQL($_GET['inicio'])) : new DateTime(date("Y-m-01"));
        $fim = !empty($_GET['fim']) ? new DateTime(convertDataBR4SQL($_GET['fim'])) : new DateTime(date("Y-m-t"));
        $empresas = !empty($_GET['empresas']) ? $_GET['empresas'] : '';
        $grupo = !empty($_GET['grupo']) ? $_GET['grupo'] : '';
        $pagar = !empty($_GET['pagar']) ? $_GET['pagar'] == 'true' ? true : false : false;
        $receber = !empty($_GET['receber']) ? $_GET['receber'] == 'true' ? true : false : false;

        $ret = array();
        $ret['result'] = false;

        try {
            $fluxo = array();
            $fluxo[0]['banco'] = MovimentoBanco::getSaldoInicial($grupo, $empresas, $inicio->format('Y-m-d'));
            $fluxo[0]['receber'] = Contasreceber::SumMes($inicio->format('Y-m-d'), $grupo, $empresas);
            $fluxo[0]['pagar'] = Contaspagar::SumMes($inicio->format('Y-m-d'), $grupo, $empresas);
            $pgVal = $pagar ? 0 : $fluxo[0]['pagar'];
            $rbVal = $receber ? 0 : $fluxo[0]['receber'];
            $fluxo[0]['saldo'] = $fluxo[0]['banco'] + ($rbVal - $pgVal);
            $ret['debug'] = $pgVal;

            $interval = $inicio->diff($fim);
            $dias = intval($interval->format('%R%a')) + 1;

            for ($i = 1; $i <= $dias; $i++) {
                $fluxo[$i]['banco'] = $fluxo[$i - 1]['saldo'];
                $fluxo[$i]['pagar'] = Contaspagar::SumDiaDia($inicio->format('Y-m-d'), $grupo, $empresas);
                $fluxo[$i]['receber'] = Contasreceber::SumDiaDia($inicio->format('Y-m-d'), $grupo, $empresas);
                $fluxo[$i]['saldo'] = $fluxo[$i]['banco'] + ($fluxo[$i]['receber'] - $fluxo[$i]['pagar']);
                $inicio->add(new DateInterval("P1D"));
            }

            $ret['result'] = true;
            $ret['dados'] = $fluxo;
        }catch (Exception $e){
            $ret['msg'] = "Erro ao pesquisar fluxo";
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    public function getFluxoCaixa(){

        $empresas = !empty($_GET['empresas']) ? $_GET['empresas'] : '';
        $grupo = !empty($_GET['grupo']) ? $_GET['grupo'] : '';

        $ret = array();
        $ret['result'] = false;

        try {
            $movimento = array();
            $pagar = Contaspagar::fluxoCaixaResumido($grupo, $empresas);
            $receber = Contasreceber::fluxoCaixaResumido($grupo, $empresas);

            $movimento[0]['pagar'] = intval($pagar->dia);
            $movimento[0]['receber'] = intval($receber->dia);
            $movimento[0]['saldo'] = $receber->dia - $pagar->dia;

            $movimento[1]['pagar'] = intval($pagar->mes);
            $movimento[1]['receber'] = intval($receber->mes);
            $movimento[1]['saldo'] = $receber->mes - $pagar->mes;

            $movimento[2]['pagar'] = intval($pagar->ano);
            $movimento[2]['receber'] = intval($receber->ano);
            $movimento[2]['saldo'] = $receber->ano - $pagar->ano;

            $ret['result'] = true;
            $ret['dados'] = $movimento;
        }catch (Exception $e){
            $ret['msg'] = "Erro ao pesquisar fluxo";
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    public function getMovimento(){

        $empresas = !empty($_GET['empresas']) ? $_GET['empresas'] : '';
        $grupo = !empty($_GET['grupo']) ? $_GET['grupo'] : '';

        $ret = array();
        $ret['result'] = false;

        try {
            $movimentos = MovimentoBanco::getMovimento('Aberto', $grupo, $empresas);

            $dados = array();

            $i = 0;
            $ret['total'] = 0;
            foreach($movimentos as $m){
                $dados[$i]['banco'] = $m->banco;
                $dados[$i]['saldo'] = $m->saldo;
                $dados[$i]['movimento'] = $m->mes;
                $ret['total'] += $dados[$i]['saldo'];
                $i++;
            }

            $ret['result'] = true;
            $ret['dados'] = $dados;
        }catch (Exception $e){
            $ret['msg'] = "Erro ao pesquisar fluxo";
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    public function getEmpresas(){
        $ret = array();
        $ret['result'] = false;

        try{
            $grupo = new GrupoContabilidade($_GET['id']);
            $empresas = $grupo->getContabilidades();
            $ret['result'] = true;
            $ret['dados'] = $empresas;
        }catch (Exception $e){
            $ret['msg'] = "Erro ao pesquisar empresas";
            $ret['error'] = $e->getTraceAsString();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }


    public function detalhes_pagar(){
        $this->setTitle('Detalhes fluxo');

        $data = convertDataBR4SQL($_GET['dia']);
        $inicio= convertDataBR4SQL($_GET['inicio']);

        $a = new Criteria();
        if($_GET['coluna']==0){
            $a->addCondition('vencimento','<=',$inicio);
        }else{
            $a->addCondition('vencimento','=',$data);
        }
        $a->addCondition('status','=',1);
        $a->addCondition('dataPagamento','is',null);
        if(empty($_GET['empresas'])){
            $a->addSqlConditions("contaspagar.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = ".$_GET['grupos'].") ");
        }else{
            $a->addSqlConditions("contaspagar.contabilidade IN(".$_GET['empresas'].")");
        }

        $contas_pagar = Contaspagar::getList($a);


        $this->set('contas_pagar',$contas_pagar);

    }
    public function detalhes_receber(){
        $this->setTitle('Detalhes fluxo');

        $data = convertDataBR4SQL($_GET['dia']);
        $inicio= convertDataBR4SQL($_GET['inicio']);

        $b = new Criteria();
        if($_GET['coluna']==0){
            $b->addCondition('vencimento','<=',$inicio);
        }else{
            $b->addCondition('vencimento','=',$data);
        }
        $b->addCondition('status_id','=',1);
        $b->addCondition('dataPagamento','is',null);
        if(empty($_GET['empresas'])){
            $b->addSqlConditions("contasreceber.contabilidade_id IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = ".$_GET['grupos'].") ");
        }else{
            $b->addSqlConditions("contasreceber.contabilidade_id IN(".$_GET['empresas'].")");
        }

        $contas_receber= Contasreceber::getList($b);

        $this->set('contas_receber',$contas_receber);


    }
}
