<?php
final class DeducoesController extends AppController{ 

    # página inicial do módulo Deducoes
    function index(){
        $this->setTitle('Deducoes');
    }

    # lista de Deducoes
    # renderiza a visão /view/Deducoes/all.php
    function all(){
        $this->setTitle('Dedu&ccedil;&otilde;es');
    }

    # visualiza um(a) Deducoes
    # renderiza a visão /view/Deducoes/view.php
    function view(){
        try {
            $this->set('Deducoes', new Deducoes((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Deducoes', 'all');
        }
    }

    # formulário de cadastro de Deducoes
    # renderiza a visão /view/Deducoes/add.php
    function add(){
        $this->setTitle('Adicionar Deducoes');
        $this->set('Deducoes', new Deducoes);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Deducoes
    # (true)redireciona ou (false) renderiza a visão /view/Deducoes/add.php
    function post_add(){
        $this->setTitle('Adicionar Deducoes');
        $Deducoes = new Deducoes();
        $this->set('Deducoes', $Deducoes);
        try {
            $Deducoes->save($_POST);
            new Msg(__('Dedução cadastrada com sucesso'));
            $this->go('Deducoes', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Deducoes
    # renderiza a visão /view/Deducoes/edit.php
    function edit(){
        $this->setTitle('Editar Dedu&ccedil;&otilde;es');
        try {
            $this->set('Deducoes', new Deducoes((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Deducoes', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Deducoes
    # (true)redireciona ou (false) renderiza a visão /view/Deducoes/edit.php
    function post_edit(){
        $this->setTitle('Editar Dedu&ccedil;&otilde;es');
        try {
            $Deducoes = new Deducoes((int) $_POST['id']);
            $this->set('Deducoes', $Deducoes);
            $Deducoes->save($_POST);
            new Msg(__('Dedução atualizada com sucesso'));
            $this->go('Deducoes', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Recebe o id via post e exclui um(a) Deducoes
    # redireciona para Deducoes/all
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $Deducoes = new Deducoes((int) $_POST['id']);
            $Deducoes->status = 3;
            $Deducoes->save();
            $ret['result'] = true;
            $ret['msg'] = 'Dedu&ccedil;&atilde;o apagada com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir dedu&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function getDeducoes(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM deducoes AS d WHERE d.status <> 3";
            $sql = "SELECT d.id, d.nome AS descricao, s.descricao AS status
                    FROM deducoes AS d INNER JOIN status AS s ON (s.id = d.status)
                    WHERE d.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND d.nome LIKE :termo";
                $sql .= " AND d.nome LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY d.nome";
                $sort[] = array('descricao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }

}