<?php

final class PeriodicidadeController extends AppController
{

    # página inicial do módulo Periodicidade
    function index()
    {
        $this->setTitle('Detalhes');
    }

    # lista de Periodicidades
    # renderiza a visão /view/Periodicidade/all.php
    function all()
    {
        $this->setTitle('Periodicidades');
        $p = new Paginate('Periodicidade', 10);
        $c = new Criteria();
        if (!empty($_POST["filtro"])) {
            if (!empty($_POST["filtro"]["interno"])) {
                foreach ($_POST["filtro"]["interno"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if (!empty($_POST["filtro"]["externo"])) {
                foreach ($_POST["filtro"]["externo"] as $fl => $fv) {
                    $this->setParam($fl, $fv);
                    if (!empty($fv)) {
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Periodicidades', $p->getPage($c));
        $this->set('nav', $p->getNav());


    }

    # visualiza um(a) Periodicidade
    # renderiza a visão /view/Periodicidade/view.php
    function view()
    {
        $this->setTitle('Detalhes');
        try {
            $this->set('Periodicidade', new Periodicidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Periodicidade', 'all');
        }
    }

    # formulário de cadastro de Periodicidade
    # renderiza a visão /view/Periodicidade/add.php
    function add()
    {
        $this->setTitle('Cadastrar Periodicidade');
        $this->set('Periodicidade', new Periodicidade);
    }

    # recebe os dados enviados via post do cadastro de Periodicidade
    # (true)redireciona ou (false) renderiza a visão /view/Periodicidade/add.php
    function post_add()
    {
        $this->setTitle('Cadastrar Periodicidade');
        $Periodicidade = new Periodicidade();
        $this->set('Periodicidade', $Periodicidade);
        try {
            $Periodicidade->save($_POST);
            new Msg(__('Periodicidade cadastrado com sucesso'));
            $this->go('Periodicidade', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
    }

    # formulário de edição de Periodicidade
    # renderiza a visão /view/Periodicidade/edit.php
    function edit()
    {
        $this->setTitle('Editar Periodicidade');
        try {
            $this->set('Periodicidade', new Periodicidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Periodicidade', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Periodicidade
    # (true)redireciona ou (false) renderiza a visão /view/Periodicidade/edit.php
    function post_edit()
    {
        $this->setTitle('Editar Periodicidade');
        try {
            $Periodicidade = new Periodicidade((int)$_POST['id']);
            $this->set('Periodicidade', $Periodicidade);
            $Periodicidade->save($_POST);
            new Msg(__('Periodicidade atualizado com sucesso'));
            $this->go('Periodicidade', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Periodicidade
    # renderiza a /view/Periodicidade/delete.php
    function delete()
    {
        $this->setTitle('Apagar Periodicidade');
        try {
            $this->set('Periodicidade', new Periodicidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Periodicidade', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Periodicidade
    # redireciona para Periodicidade/all
    function post_delete()
    {
        try {
            $Periodicidade = new Periodicidade((int)$_POST['id']);
            $Periodicidade->status=2;
            $Periodicidade->save();
            new Msg(__('Periodicidade apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('Periodicidade', 'all');
    }

}