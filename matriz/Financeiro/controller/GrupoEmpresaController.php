<?php
final class GrupoEmpresaController extends AppController{

    # página inicial do módulo Empresa_custo
    function index(){
        $this->setTitle('Grupo Empresa');
    }

    # lista de Empresa_custos
    # renderiza a visão /view/Empresa_custo/all.php
    function all(){
        $this->setTitle('Grupos de Empresas');
    }

    # visualiza um(a) Empresa_custo
    # renderiza a visão /view/Empresa_custo/view.php
    function view(){
        try {
            $this->set('GrupoEmpresa', new GrupoContabilidade((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('GrupoEmpresa', 'all');
        }
    }

    # formulário de cadastro de Empresa_custo
    # renderiza a visão /view/Empresa_custo/add.php
    function add(){
        $this->setTitle('Adicionar Grupo Empresa');
    }

    # recebe os dados enviados via post do cadastro de Empresa_custo
    # (true)redireciona ou (false) renderiza a visão /view/Empresa_custo/add.php
    function post_add(){
        $this->setTitle('Adicionar Grupo Empresa');
        $user = Session::get('user');

        $grupo = new GrupoContabilidade();
        $grupo->dtcriacao = date('Y-m-d');
        $grupo->criadopor = $user->nome;
        $grupo->dtalteracao = date('Y-m-d');
        $grupo->alteradopor = $user->nome;
        try {
            $grupo->save($_POST);
            new Msg(__('Empresa_custo cadastrado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('GrupoEmpresa', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Empresa_custo
    # renderiza a visão /view/Empresa_custo/edit.php
    function edit(){
        $this->setTitle('Editar Grupo Empresa');
        try {
            $this->set('grupo', new GrupoContabilidade((int)$this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('GrupoEmpresa', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Empresa_custo
    # (true)redireciona ou (false) renderiza a visão /view/Empresa_custo/edit.php
    function post_edit(){
        $this->setTitle('Editar Grupo Empresa');
        try {
            $user = Session::get('user');
            $grupo = new GrupoContabilidade((int) $_POST['id']);
            $grupo->dtalteracao = date('Y-m-d');
            $grupo->alteradopor = $user->nome;
            $grupo->save($_POST);

            $this->set('grupo', $grupo);
            new Msg(__('Grupo Empresa atualizado com sucesso'));
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            $this->go('GrupoEmpresa', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Recebe o id via post e exclui um(a) Empresa_custo
    # redireciona para Empresa_custo/all
    function delete(){
        $ret = array();
        $ret['result'] = false;

        try {
            $grupo = new GrupoContabilidade((int) $_POST['id']);
            $grupo->status=3;
            $grupo->save();
            $ret['result'] = true;
            $ret['msg'] = 'Centro de custo apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Centro de custo !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function getGrupos(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

            $sqlCount = "SELECT count(*) AS count FROM grupo_contabilidade AS g WHERE g.status <> 3";
            $sql = "SELECT g.id, g.descricao, s.descricao AS status
                    FROM grupo_contabilidade AS g INNER JOIN status AS s ON (s.id = g.status)
                    WHERE g.status <> 3";

            if (!empty($search)) {
                $sqlCount .= " AND g.descricao LIKE :termo";
                $sql .= " AND g.descricao LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY g.descricao";
                $sort[] = array('descricao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }
}