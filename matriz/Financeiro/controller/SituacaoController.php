<?php
final class SituacaoController extends AppController{ 

    # página inicial do módulo Situacao
    function index(){
        $this->setTitle('Situacao');
    }

    # lista de Situacaos
    # renderiza a visão /view/Situacao/all.php
    function all(){
        $this->setTitle('Situacaos');
        $p = new Paginate('Situacao', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (isset($_GET['search'])) {
            $c->addCondition('nome', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('status','=','Ativo');
        $this->set('Situacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Situacao
    # renderiza a visão /view/Situacao/view.php
    function view(){
        try {
            $this->set('Situacao', new Situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Situacao', 'all');
        }
    }

    # formulário de cadastro de Situacao
    # renderiza a visão /view/Situacao/add.php
    function add(){
        $this->setTitle('Adicionar Situacao');
        $this->set('Situacao', new Situacao);
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Situacao
    # (true)redireciona ou (false) renderiza a visão /view/Situacao/add.php
    function post_add(){
        $this->setTitle('Adicionar Situacao');
        $Situacao = new Situacao();
        $this->set('Situacao', $Situacao);
        try {
            $Situacao->save($_POST);
            new Msg(__('Situacao cadastrado com sucesso'));
            $this->go('Situacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Situacao
    # renderiza a visão /view/Situacao/edit.php
    function edit(){
        $this->setTitle('Editar Situacao');
        try {
            $this->set('Situacao', new Situacao((int) $this->getParam('id')));
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Situacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Situacao
    # (true)redireciona ou (false) renderiza a visão /view/Situacao/edit.php
    function post_edit(){
        $this->setTitle('Editar Situacao');
        try {
            $Situacao = new Situacao((int) $_POST['id']);
            $this->set('Situacao', $Situacao);
            $Situacao->save($_POST);
            new Msg(__('Situacao atualizado com sucesso'));
            $this->go('Situacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Situacao
    # renderiza a /view/Situacao/delete.php
    function delete(){
        $this->setTitle('Apagar Situacao');
        try {
            $this->set('Situacao', new Situacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Situacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Situacao
    # redireciona para Situacao/all
    function post_delete(){
        try {
            $Situacao = new Situacao((int) $_POST['id']);
            $Situacao->status = 'Excluido';
            $Situacao->save();
            new Msg(__('Situacao apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Situacao', 'all');
    }

}