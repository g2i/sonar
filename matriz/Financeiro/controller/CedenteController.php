<?php
final class CedenteController extends AppController{ 

    # página inicial do módulo Cedente
    function index(){
        $this->setTitle('Cedente');
    }

    # lista de Cedentes
    # renderiza a visão /view/Cedente/all.php
    function all(){
        $this->setTitle('Cedentes');
        $p = new Paginate('Cedente', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        if (isset($_GET['search'])) {
            $c->addCondition('cedente', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
            $c->addCondition('status','<>',3);
        $this->set('Cedentes', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Cedente
    # renderiza a visão /view/Cedente/view.php
    function view(){
        try {
            $this->set('Cedente', new Cedente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cedente', 'all');
        }
    }

    # formulário de cadastro de Cedente
    # renderiza a visão /view/Cedente/add.php
    function add(){
        $this->setTitle('Adicionar Cedente');
        $this->set('Cedente', new Cedente);
        $this->set('Status',  Status::getList());
    }

    # recebe os dados enviados via post do cadastro de Cedente
    # (true)redireciona ou (false) renderiza a visão /view/Cedente/add.php
    function post_add(){
        $this->setTitle('Adicionar Cedente');
        $Cedente = new Cedente();
        $this->set('Cedente', $Cedente);
        try {
            $Cedente->save($_POST);
            new Msg(__('Cedente cadastrado com sucesso'));
            $this->go('Cedente', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Status',  Status::getList());
    }

    # formulário de edição de Cedente
    # renderiza a visão /view/Cedente/edit.php
    function edit(){
        $this->setTitle('Editar Cedente');
        try {
            $this->set('Cedente', new Cedente((int) $this->getParam('id')));
            $this->set('Status',  Status::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Cedente', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Cedente
    # (true)redireciona ou (false) renderiza a visão /view/Cedente/edit.php
    function post_edit(){
        $this->setTitle('Editar Cedente');
        try {
            $Cedente = new Cedente((int) $_POST['id']);
            $this->set('Cedente', $Cedente);
            $Cedente->save($_POST);
            new Msg(__('Cedente atualizado com sucesso'));
            $this->go('Cedente', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status',  Status::getList());
    }

    # Confirma a exclusão ou não de um(a) Cedente
    # renderiza a /view/Cedente/delete.php
    function delete(){
        $this->setTitle('Apagar Cedente');
        try {
            $this->set('Cedente', new Cedente((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Cedente', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Cedente
    # redireciona para Cedente/all
    function post_delete(){
        try {
            $Cedente = new Cedente((int) $_POST['id']);
            $Cedente->status = 3;
            $Cedente->save();
            new Msg(__('Cedente apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Cedente', 'all');
    }

    function cep(){
        $cep = $_POST['cep'];
        $url =  "http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL,$url);
        $aux = curl_exec($ch);
        curl_close($ch);
        $dados = simplexml_load_string($aux);
        echo $dados->resultado.",".$dados->logradouro.",".$dados->bairro.",".$dados->cidade.",".$dados->uf;
        exit();
    }

}