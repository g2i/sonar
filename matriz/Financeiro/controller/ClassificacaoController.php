<?php

final class ClassificacaoController extends AppController {
    # página inicial do módulo Classificacao

    function index() {
        $this->setTitle('Classificação');
    }

    # lista de Classificacaos
    # renderiza a visão /view/Classificacao/all.php

    function all() {
        $this->setTitle('Classificação');
    }

    # visualiza um(a) Classificacao
    # renderiza a visão /view/Classificacao/view.php

    function view() {
        try {
            $this->set('Classificacao', new Classificacao((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Classificacao', 'all');
        }
    }

    # formulário de cadastro de Classificacao
    # renderiza a visão /view/Classificacao/add.php

    function add() {
        $this->setTitle('Adicionar Classificação');
        $this->set('Classificacao', new Classificacao);
        $this->set('Tipo', Tipoclassificacao::getList());
        $c = new Criteria();
        $c->addCondition('id', '<>', '3');
        $this->set('Status', Status::getList($c));
    }

    # recebe os dados enviados via post do cadastro de Classificacao
    # (true)redireciona ou (false) renderiza a visão /view/Classificacao/add.php

    function post_add() {
        $this->setTitle('Adicionar Classificacao');
        $Classificacao = new Classificacao();
        $this->set('Classificacao', $Classificacao);
        try {
            $Classificacao->save($_POST);
            new Msg(__('Classificacao cadastrado com sucesso'));
            $this->go('Classificacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $c = new Criteria();
        $c->addCondition('id', '<>', '3');
        $this->set('Status', Status::getList($c));
    }

    # formulário de edição de Classificacao
    # renderiza a visão /view/Classificacao/edit.php

    function edit() {
        $this->setTitle('Editar Classificação');
        try {
            $this->set('Classificacao', new Classificacao((int) $this->getParam('id')));
            $this->set('Tipo', Tipoclassificacao::getList());
            $c = new Criteria();
            $c->addCondition('id', '<>', '3');
            $this->set('Status', Status::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('Classificacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Classificacao
    # (true)redireciona ou (false) renderiza a visão /view/Classificacao/edit.php

    function post_edit() {
        $this->setTitle('Editar Classificação');
        try {
            $Classificacao = new Classificacao((int) $_POST['id']);
            $this->set('Classificacao', $Classificacao);
            $Classificacao->save($_POST);
            new Msg(__('Classificacao atualizado com sucesso'));
            $this->go('Classificacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $c = new Criteria();
        $c->addCondition('id', '<>', '3');
        $this->set('Status', Status::getList($c));
    }

    # Recebe o id via post e exclui um(a) Classificacao
    # redireciona para Classificacao/all

    function delete() {

        $ret = array();
        $ret['result'] = false;

        try {
            $Classificacao = new Classificacao((int) $_POST['id']);
            $Classificacao->status = 3;
            $Classificacao->save();
            $ret['result'] = true;
            $ret['msg'] = 'Classifica&ccedil;&atilde;o apagado com sucesso !';
        } catch (Exception $e) {
            $ret['msg'] = 'Erro ao excluir Classifica&ccedil;&atilde;o !';
            $ret['erro'] = $e->getMessage();
        }

        header('Content-Type: application/json');
        echo json_encode($ret);
        exit;
    }

    function getClassificacao(){

        try {
            $page = (int)$this->getParam('current');
            $size = (int)$this->getParam('rowCount');
            $search = $this->getParam('searchPhrase');
            $sort = !empty($_GET['sort']) ? json_decode($_GET['sort']) : '';

            if (!isset($size) || $size < 1)
                $size = 30;

            if (!isset($page) || $page < 1)
                $page = 1;

                    $sqlCount = "SELECT count(*) AS count FROM `classificacaocontas` AS c WHERE c.status <> 3";
                    $sql = "SELECT c.id, c.descricao, t.descricao AS tipo, s.descricao AS status,
                            IF(c.considera, 'Sim', 'N&atilde;o') AS considera
                            FROM classificacaocontas AS c INNER JOIN status AS s ON (s.id = c.status)
                            LEFT JOIN tipo_classificacao AS t ON (t.id = c.tipo)
                            WHERE c.status <> 3";
            if (!empty($search)) {
                $sqlCount .= " AND c.descricao LIKE :termo";
                $sql .= " AND c.descricao LIKE :termo";
            }

            $sortCount = count($sort);
            if($sortCount < 1)
            {
                $sql .= " ORDER BY c.descricao";
                $sort[] = array('descricao' => 'asc');
            }
            else
            {
                $sql .= " ORDER BY";
                for($i = 0; $i < $sortCount; $i++){
                    if($i > 0)
                        $sql .= ", ";

                    $sql .= " ".$sort[$i][0]." ".$sort[$i][1];
                }
            }

            $sql .= " LIMIT :li OFFSET :off";

            $db = $this::getConn();
            $db->query($sqlCount);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->execute();
            $total = $db->getRow()->count;

            $db->query($sql);

            if (!empty($search)) {
                $db->bind(":termo", $search."%", PDO::PARAM_STR);
            }

            $db->bind(":li", $size, PDO::PARAM_INT);
            $db->bind(":off", $size * ($page - 1), PDO::PARAM_INT);
            $dados = $db->getResults();

            $ret = array();
            $ret["current"] = $page;
            $ret["rowCount"] = count($dados);
            $ret["rows"] = $dados;
            $ret["total"] = $total;
            $ret['sort'] = $sort;

            echo json_encode($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
        exit;
    }
}
