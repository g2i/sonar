$(document).ready(function() {
    $("#email").focusout(function() {
        ChecarEmail($("#email").val());
    });


    $("#fone,#cpf,#rg,#celular,#cep,#numero").keypress(function() {
        var tecla = (window.event) ? event.keyCode : event.which;
        if ((tecla > 47 && tecla < 58))
            return true;
        else {
            if (tecla != 8)
                return false;
            else
                return true;
        }
    });

    $("#ConSenha").focusout(function() {
        if ($("#senha").val() != $("#ConSenha").val()) {
            OpenDialog('Erro', 'As senhas não são iguais');
            $("#ConSenha").val('');
        }
    });
});


function getTurma(idCurso) {
    $.ajax({
        type: "POST",
        url: root + '/?m=Turma&p=lista',
        data: "idCurso=" + idCurso,
        async: false,
        success: function(txt) {
            $('#turma').html(txt);
            $('#turma option[value="-1"]').prop('selected', true);

            getModulos(idCurso);
        },
        error: function(txt) {
            // $('#listagem_msg').html(txt);
        }
    });
}

function getAlunos(idCurso, idTurma, idModulo) {
    $.ajax({
        type: "POST",
        url: root + '/?m=Nota&p=lista',
        data: "idCurso=" + idCurso + "&idTurma=" + idTurma + "&idModulo=" + idModulo,
        async: false,
        success: function(txt) {
            $('#tabl').html(txt);
        },
        error: function(txt) {
            // $('#listagem_msg').html(txt);
        }
    });
}

function CarregarProvas(){
    $.ajax({
        type:'POST',
        url:root+'/Nota/GetNew',
        data:'curso='+$("#curso").val()+'&modulo='+$("#modulo").val()+"&disciplina="+$("#disciplina").val(),
        success:function(txt){
            $("#Provas").html(txt);
        }
    });
}

function getModulos(idCurso) {
    $.ajax({
        type: "POST",
        url: root + '/?m=Turma&p=listarModulos',
        data: "idCurso=" + idCurso,
        async: false,
        success: function(txt) {
            $('#modulo').html(txt);
            $('#modulo option[value="-1"]').prop('selected', true);
        },
        error: function(txt) {
            // $('#listagem_msg').html(txt);
        }
    });
}

function AlterarNota(idNota, nota, idProfessor) {

    $.ajax({
        type: "POST",
        url: root + '/?m=Nota&p=alterarNota',
        data: "idNota=" + idNota + "&nota=" + nota + "&idProfessor=" + idProfessor,
        async: false,
        success: function(txt) {
            if (txt == 1) {
                jQuery('#result').attr('class', '');
                jQuery('#result').addClass('success');
                jQuery('#result').text('Nota salva com sucesso!');
                jQuery('#result').show("slow");
                setTimeout(function() {
                    jQuery('#result').hide("slow");
                }, 2000);


                //alert('Nota Alterada com sucesso!');
            } else {
                jQuery('#result').attr('class', '');
                jQuery('#result').addClass('error');
                jQuery('#result').text('Erro ao salvar nota!');
                jQuery('#result').show("slow");
                setTimeout(function() {
                    jQuery('#result').hide("slow");
                }, 3000);
            }
        },
        error: function(txt) {
            // $('#listagem_msg').html(txt);
        }
    });
}
$(function(){
    $('#cep').blur(function() {
        $.ajax({
            url: root + '/?m=Cidades&p=buscaCep',
            type: 'POST',
            data: 'cep=' + $('#cep').val(),
            dataType: 'json',
            success: function(data) {
                if (data.sucesso == 1) {
                    $('#endereco').val(data.rua);
                    $('#bairro').val(data.bairro);
                    $('#cidade').val(data.cidade);
                    $('#estado').val(data.estado);
                    $('#numero').focus();
                } else {
                    OpenDialog('Erro', 'O Cep não foi encontrado.Preencha manualmente os campos!');
                }
            }
        });
    });
});

    $('#cepEmpresa').blur(function() {
        $.ajax({
            url: root + '/?m=Cidades&p=buscaCep',
            type: 'POST',
            data: 'cep=' + $('#cepEmpresa').val(),
            dataType: 'json',
            success: function(data) {
                if (data.sucesso == 1) {
                    $('#enderecoEmpresa').val(data.rua);
                    $('#bairroEmpresa').val(data.bairro);
                    $('#cidadeEmpresa').val(data.cidade);
                    $('#estadoEmpresa').val(data.estado);
                } else {
                    OpenDialog('Erro', 'O Cep não foi encontrado.Preencha manualmente os campos!');
                }
            }
        });
    })

jQuery(document).ready(function($) {
    $('#senha').pstrength();
    $('#ConSenha').pstrength();
});

function OpenDialog(titulo, mensagem) {
    $(function() {
        $("#dialog-message").dialog({
            modal: true,
            moveToTop:true,
            buttons: {
                Ok: function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialog-message").dialog({title: titulo});
        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});
        $("#dialog-message").text(mensagem);
    });
}
function ConfirmDialog(titulo, mensagem,caminho,id_externo,tipo) {
    $("#dialog-message").css({"z-index":"1052"});
    function Continuar() {
        $(this).dialog("close");
        $('div.modal').find('div.modal-content').load(caminho,{
            'id_externo': id_externo,
            'tipo':tipo
        });
    }
    $(function() {
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Sim:Continuar,
                Não: function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialog-message").dialog({title: titulo});
        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});
        $("#dialog-message").text(mensagem);
    });
}

function moveRelogio() {

    momentoAtual = new Date()
    hora = momentoAtual.getHours()
    minuto = momentoAtual.getMinutes()
    segundo = momentoAtual.getSeconds()
    horaImprimivel = hora + ":" + minuto + ":" + segundo

    $("#entrada").val(horaImprimivel);
    $("#saida").val(horaImprimivel);

    setTimeout("moveRelogio()", 100);
}

function BuscarAluno(idAluno) {
    $.ajax({
        type: "POST",
        url: root + '/?m=Faltas&p=fillAluno',
        data: "idAluno=" + idAluno,
        async: false,
        success: function(txt) {
            $('#tabl').html(txt);
        },
        error: function(txt) {
            // $('#listagem_msg').html(txt);
        }
    });
}

function AlterarSenha(categoria, senha, id, status) {
    if (categoria == 'aluno') {
        link = '/?m=Alunos&p=AlterarSenha';
    } else if (categoria == 'administrador') {
        link = '/?m=Administrador&p=AlterarSenha';
    } else if (categoria == 'professor') {
        link = '/?m=Professor&p=AlterarSenha';
    }

    $.ajax({
        type: "POST",
        url: root + link,
        data: "senha=" + senha + "&id=" + id + "&status=" + status,
        async: false,
        success: function(txt) {
            if (txt == 1) {
                OpenDialog('Sucesso', 'Senha Atualizada com sucesso!');
                setTimeout(function() {
                    location.href = root + '/?mIndex&p=Index';
                }, 3000);
            } else {
                OpenDialog('Erro', 'Erro ao alterar senha!');
            }
        },
        error: function(txt) {
            // $('#listagem_msg').html(txt);
        }
    });
}
function ChecarCpf(cpf) {
    $.ajax({
        type: "POST",
        url: root + '/?m=Check&p=ChecarCpf',
        data: "cpf=" + cpf,
        async: false,
        success: function(txt) {
            if (txt == 1) {
                OpenDialog('Erro', 'Este cpf ja esta cadastrado!');
                $("#cpf").val('');
                $(".cpf").val('');
            }
        }

    });

}

function ChecarEmail(email) {
    /*$.ajax({
        type: "POST",
        url: root + '/?m=Check&p=ChecarEmail',
        data: "email=" + email,
        async: false,
        success: function(txt) {
            if (txt == 1) {
                OpenDialog('Erro', 'Email já cadastrado!');
                $("#email").val('');
            }
        }

    });*/

}

$(document).ready(function() {
    $('#cep').mask('00000-000');
    $('#cepEmpresa').mask('00000-000');
    $('#fone').mask('(00) 0000-00000');
    $('#fonoEmpresa').mask('(00) 0000-00000');
    $('#celular').mask('(00) 0000-00000');
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('#login').mask('000.000.000-00', {reverse: true});
});


function DeletarArquivo(id) {
    $("#dialog-message").css({"z-index":"1052"});
    function Sim() {
        jQuery.ajax({
            type: "POST",
            url: root + '/?m=Arquivos&p=post_delete',
            data: "idArquivo=" + id,
            success: function(txt) {
                $("#dialog-message").dialog("close");
                if(txt==1){
                    Carregar();
                }
            }
        });
    }
    $(function() {
        $("#dialog-message").dialog({
            modal: true,
            buttons: {
                Sim:Sim,
                Não: function() {
                    $(this).dialog("close");
                }
            }
        });
        $("#dialog-message").dialog({title: "Deletar Arquivo"});
        $("#dialog-message").dialog({position: {my: "center", at: "center", of: window}});
        $("#dialog-message").text("Deseja realmente deletar esse arquivo?");
    });
}

function AnexoArquivo(tipo,id){
    jQuery.ajax({
        type: "POST",
        url: root + '/?m=Arquivos&p=Listar',
        data: "idPasta=" + id,
        success: function(txt) {
            $(".inp").css({"display":"block"});
            kendoUpload(txt, tipo, id);
        }
    });
}

function kendoUpload(files, div, id) {
    var aux = "[";
    $.each(eval(files), function(name, value) {
        type = value.descricao.split('.');
        var a = type.length - 1;
        type = type[a];
        aux += ("{ name:'" + value.descricao + "',size:'" + value.url + "',extension:'"+type+"'},");
    });
    aux += "]";
    $("#" + div).kendoUpload({
        multiple: true,
        async: {
            saveUrl: root + '/?m=Arquivos&p=post_add&idPasta='+id,
            autoUpload: true
        },
        template: kendo.template($('#fileTemplate').html()),
        files: eval(aux),
        localization: {
            statusUploading: "Enviando",
            uploadSelectedFiles: "Enviar arquivos",
            customStatusFailed: "Falha ao enviar",
            select: "Selecione",
            retry: "Tente Novamente",
            remove: "Remover",
            headerStatusUploading: 'enviando',
            dropFilesHere: "Arraste seus arquivos aqui",
            headerStatusUploaded: "Enviado"
        },
        progress: onProgress,
        select: onSelect,
        cancel: onCancel
    });
}

function onProgress(e) {
    // Array with information about the uploaded files
    var files = e.files;
    console.log(e.percentComplete);
}
function onCancel(e) {
    // Array with information about the uploaded files
    var files = e.files;
    // Process the Cancel event
}

function onError(e) {
    console.log("Error (" + e.operation + ") :: " + getFileInfo(e));
}

function Carregar(){
    var html="";
    html+=" <label for='descricao'>Arquivos da Pasta</label><br />"+
    "<input type='file' name='arquivos' id='arquivos' />";
    $("#inptFile").html(html);
    AnexoArquivo("arquivos",$("#idPasta").val());
}

function onSelect(e) {
    $.each(e.files, function(index, value) {
        switch (value.extension) {
            case '.jpg':
            case '.img':
            case '.png':
            case '.gif':
            case '.doc':
            case '.docx':
            case '.xls':
            case '.xlsx':
            case '.pdf':
            case '.pptx':
            case '.ppt':
            case '.zip':
            case '.rar':
            case '.txt':
                break;
            default:
                $('#dialog-message').text('Formato de arquivo invalido! O sistema so aceita os arquivos .jpg,.img,.png,.gif,.doc,.docx,.xls,.xlsx,.pdf,.zip,.rar,.txt');
                mensagem2('dialog-message','Anexo',500);
                break;
        }
    });
}

function addExtensionClass(extension) {
    switch (extension) {
        case '.jpg':
        case 'jpg':
        case '.img':
        case 'img':
        case '.png':
        case 'png':
        case '.gif':
        case 'gif':
            return "img-file";
        case '.doc':
        case 'doc':
        case '.docx':
        case 'docx':
            return "doc-file";
        case '.xls':
        case 'xls':
        case '.xlsx':
        case 'xlsx':
            return "xls-file";
        case '.pdf':
        case 'pdf':
            return "pdf-file";
        case '.zip':
        case 'zip':
        case '.rar':
        case 'rar':
            return "zip-file";

        case '.ppt':
        case 'ppt':
        case '.pptx':
        case 'pptx':
            return "ppt-file";
        default:
            return "default-file";
    }
}

function abrirNavegador(dados) {
    var data = root+dados;
    $("#iFrameRelatorioId").attr("src", data);
    $("#previewAnexo").modal("show");
}

function mensagem2(div, title, width) {
    $(function() {
        $("#" + div).dialog({
            modal: true,
            autoOpen: true,
            title: title,
            resizable: true,
            buttons: {
                Ok: function() {
                    $(this).dialog("close");
                }
            }
        }, {minWidth: width});
    });
}

function Message(title,message,tipe){
    var notification = $("#notification").kendoNotification({
        position: {
            pinned: true,
            top: 30,
            right: 30
        },
        autoHideAfter: 5000,
        stacking: "down",
        templates: [{
            type: "info",
            template: $("#emailTemplate").html()
        }, {
            type: "error",
            template: $("#errorTemplate").html()
        }, {
            type: "upload-success",
            template: $("#successTemplate").html()
        }]

    }).data("kendoNotification");


    if(tipe=='danger') {
        notification.show({
            title: title,
            message: message
        }, "error");
    }
    if(tipe=='success'){
        notification.show({
            title: title,
            message: message
        }, "upload-success");
    }

    if(tipe=='email'){
        notification.show({
            title: title,
            message: message
        }, "info");
    }

}


$(function () {
    $('[data-toggle="tooltip2"]').tooltip2()
})

function isset() {
    //  discuss at: http://phpjs.org/functions/isset/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: FremyCompany
    // improved by: Onno Marsman
    // improved by: Rafa? Kukawski
    //   example 1: isset( undefined, true);
    //   returns 1: false
    //   example 2: isset( 'Kevin van Zonneveld' );
    //   returns 2: true

    var a = arguments,
        l = a.length,
        i = 0,
        undef;

    if (l === 0) {
        throw new Error('Empty isset');
    }

    while (i !== l) {
        if (a[i] === undef || a[i] === null) {
            return false;
        }
        i++;
    }
    return true;
}



function Confirmar(mensagem){
    var div = document.createElement("div");
    div.setAttribute('id', 'dialog-message');
    document.body.appendChild(div);

    var html = '<script type="text/x-kendo-template" id="windowTemplate">'+
                '<p><strong>'+mensagem+'</strong> </p><br /><br />'+
                '<button class="k-button" id="yesButton">Sim</button>'+
                '<button class="k-button" id="noButton"> Não</button>'+
                '</script>';
    $("#dialog-message").html(html);
}
const myJSONObject = {"navegacao": [{"p": 1, "u": root}]};

function Navegar(url,param){
    var pos = myJSONObject.navegacao.length;
    if(param=='go'){
        pos = pos+1;
        var url = {"p": pos, "u": url};
        myJSONObject.navegacao.push(url);
    }else{
        pos = pos-1;
        myJSONObject.navegacao.pop();
    }

    $.each(myJSONObject,function(key,data){
        $.each(data,function(id,valor){
            if(pos==1){
                $('.modal').modal('hide');
            }else
            if(valor.p==pos) {
                if(param=='go')
                $('.modal').find('.modal-content').load(valor.u);
                else
                    if(valor.u!="")
                        $('.modal').find('.modal-content').load(valor.u);
            }
        });
    });
}

function Acrescentar(url){
    var pos = myJSONObject.navegacao.length;
    pos = pos+1;
    var url = {"p": pos, "u": url};
    myJSONObject.navegacao.push(url);
}

function Decrementar(url,param){
    var pos = myJSONObject.navegacao.length;
    pos = pos-1;
    myJSONObject.navegacao.pop();

    Navegar(url,param);
}

function EnviarFormulario(form){
    $(form).ajaxForm({
        success: function(d){
            if(d!=1){
                $('#msg_error').html(d);
            }else{
                Navegar('','back');
            }
        }
    });
}




function EnviarFormularioProfessor(form){
    $(form).ajaxForm({
        success: function(d){
            var data = d.split(',');
            if(data[1]!=1){
                $('#msg_error').html(d);
            }else{
                Navegar('','back');
                SelectProfessores(data[0]);
            }
        }
    });
}

function SelectProfessores(id){
    $.ajax({
        type:'GET',
        url:root+'Professor/combo',
        data:"id="+id,
        success:function(txt){
            $("#professor").html(txt);
        }
    });

}



