<?php

class Calendario {

    public $ano;
    public $html = "";

    function __construct($mes, $ano) {
        $this->ano = $ano;

        $this->MostreCalendario($mes);
    }

    function MostreSemanas() {
        $semanas = array('Dom','Seg','Ter','Qua','Qui','Sex','Sáb');
//                "DSTQQSS";
        for ($i = 0; $i < 7; $i++)
            $this->html .= "<td class='semana'>" . $semanas[$i] . "</td>";
    }

    function GetNumeroDias($mes) {
        $numero_dias = array(
            '01' => 31, '02' => 28, '03' => 31, '04' => 30, '05' => 31, '06' => 30,
            '07' => 31, '08' => 31, '09' => 30, '10' => 31, '11' => 30, '12' => 31
        );
        if ((($this->ano % 4) == 0 and ( $this->ano % 100) != 0) or ( $this->ano % 400) == 0) {
            $numero_dias['02'] = 29; // altera o numero de dias de fevereiro se o ano for bissexto
        }
        return $numero_dias[$mes];
    }

    function GetNomeMes($mes) {
        $meses = array('01' => "Janeiro", '02' => "Fevereiro", '03' => "Março",
            '04' => "Abril", '05' => "Maio", '06' => "Junho",
            '07' => "Julho", '08' => "Agosto", '09' => "Setembro",
            '10' => "Outubro", '11' => "Novembro", '12' => "Dezembro"
        );
        if ($mes >= 01 && $mes <= 12)
            return $meses[$mes];
        return "Mês deconhecido";
    }

    /**
     * 
     * @param type $mes Recebe o mes desejado
     */
    function MostreCalendario($mes) {
        $data = explode("-", date('Y-m-d'));
        $diaAtual = $data[2];
        $mesAtual = $data[1];

        $numero_dias = $this->GetNumeroDias($mes); // retorna o número de dias que tem o mês desejado
        $nome_mes = $this->GetNomeMes($mes);
        $diacorrente = 0;
        $diasemana = jddayofweek(cal_to_jd(CAL_GREGORIAN, $mes, "01", $this->ano), 0); // função que descobre o dia da semana
        $this->html .= "<table  id='calendario' class='table-responsive'>";
        $this->html .= "<tr>";
        $this->html .= "<td colspan = 7 class='titulo'><h3>" . $nome_mes . " - " . $this->ano . "</h3></td>";
        $this->html .= "</tr>";
        $this->html .= "<tr>";
        $this->MostreSemanas(); // função que mostra as semanas aqui
        $this->html .= "</tr>";
        for ($linha = 0; $linha < 6; $linha++) {
            $this->html .= "<tr>";
            for ($coluna = 0; $coluna < 7; $coluna++) {
                $this->html .= "<td  ";
                if (($diacorrente == ( date('d') - 1) && date('m') == $mes)) {
                    $this->html .= " class = 'diaAtual' ";
                } else {
                    if (($diacorrente + 1) <= $numero_dias) {
                        if ($coluna < $diasemana && $linha == 0) {
                            $this->html .= " id = 'dia_branco' ";
                        } else {
                            $this->html .= " id = 'dia_comum' ";
                        }
                    } else {
                        $this->html .= "style='display:none'";
                    }
                }
                $this->html .= ">";
                /* TRECHO IMPORTANTE: A PARTIR DESTE TRECHO É MOSTRADO UM DIA DO CALENDÁRIO (MUITA ATENÇÃO NA HORA DA MANUTENÇÃO) */
                if ($diacorrente + 1 <= $numero_dias) {
                    if ($coluna < $diasemana && $linha == 0) {
                        $this->html .= " ";
                    } else {
                        // echo "<input type = 'button' id = 'dia_comum' name = 'dia".($diacorrente+1)."'  value = '".++$diacorrente."' onclick = "acao(this.value)">";
                        $this->html .= "<a  href= " . $this->getUrl('' . CONTROLLER . '', '' . ACTION . '', array('mes' => $mes, 'dia' => ($diacorrente + 1))) . ">" . ++$diacorrente . "</a>";
                    }
                } else {
                    break;
                }
                /* FIM DO TRECHO MUITO IMPORTANTE */
                $this->html .= "</td>";
            }
            $this->html .= "</tr>";
        }
        $this->html .= "</table>";
    }

    function MostreCalendarioCompleto() {
        $this->html .= "<table >";
        $cont = 1;
        for ($j = 0; $j < 4; $j++) {
            $this->html .= "<tr>";
            for ($i = 0; $i < 3; $i++) {

                $this->html .= "<td>";
                MostreCalendario(($cont < 10 ) ? "0" . $cont : $cont );

                $cont++;
                $this->html .= "</td>";
            }
            $this->html .= "</tr>";
        }
        $this->html .= "</table>";
    }

    public function getUrl($controller, $action, Array $urlParams = NULL) {
        $link = '';
        $url = SITE_PATH . '/?m=' . $controller . '&p=' . $action;
        if (Config::get('rewriteURL'))
            $url = SITE_PATH . '/' . $controller . '/' . $action . '/';
        $link .=$url;
        if (is_array($urlParams)) {
            $carr = (Config::get('criptedGetParamns'));
            if (is_array($carr))
                foreach ($carr as $param) {
                    foreach ($urlParams as $key => $value) {
                        if (is_int($key) && $param === ($key + 1)) {
                            $urlParams[$key] = Cript::cript($value);
                            continue;
                        } elseif ($param === $key) {
                            $urlParams[$key] = Cript::cript($value);
                        }
                    }
                }

            if (Config::get('rewriteURL'))
                foreach ($urlParams as $key => $value) {
                    if (is_int($key))
                        $link .= $value . '/';
                    else
                        $link .= $key . ':' . $value . '/';
                    unset($urlParams[$key]);
                }
            foreach ($urlParams as $key => $value) {
                if (is_int($key))
                    $urlParams['arg' . ++$key] = $value;
            }
            if (Config::get('rewriteURL') && count($urlParams))
                $link .= '?';
            if (count($urlParams)) {
                $params = '&' . http_build_query($urlParams);
                $link .= $params;
            }
        }
        return str_replace('//', '/', $link);
    }

}
