<?php
date_default_timezone_set("America/Campo_Grande");
##################################
# CONFIGURAÇÕES DO SGBD 
##################################

# servidor do SGBD:

///-----JS LOCAL
Config::set('db_host', 'localhost');
Config::set('db_user', 'root');
Config::set('db_password', '123456');
Config::set('db_name', 'g2ioper_js_local');
///-----END JS

////-----JS
/*Config::set('db_host', '192.175.99.74');
Config::set('db_user', 'g2ioper_js');
Config::set('db_password', 'js6990');
Config::set('db_name', 'g2ioper_js');*/
////END JS-----

//-----STA_LUCCI
//Config::set('db_host', '72.55.172.178');
//Config::set('db_user', 'g2isoluc_stalucc');
//Config::set('db_password', 'stalucci6990');
//Config::set('db_name', 'g2isoluc_stalucci');
//END STA_LUCCI-----

//-----CRCG
//Config::set('db_host', '72.55.172.178');
//Config::set('db_user', 'g2isoluc_crcg');
//Config::set('db_password', 'crcg6990');
//Config::set('db_name', 'g2isoluc_crcg');
//END CRCG-----

//-----LAC
//Config::set('db_host', '72.55.172.178');
//Config::set('db_user', 'g2isoluc_lac');
//Config::set('db_password', 'lac6990');
//Config::set('db_name', 'g2isoluc_lac');
//END LAC-----

//-----RHesultado
//Config::set('db_host', '72.55.172.178');
//Config::set('db_user', 'g2isoluc_rhesult');
//Config::set('db_password', 'rhesultado6990');
//Config::set('db_name', 'g2isoluc_rhesultado');
//END LAC-----

//-----Cardioritmo
//Config::set('db_host', '72.55.172.178');
//Config::set('db_user', 'g2isoluc_cardio');
//Config::set('db_password', 'cardio6990');
//Config::set('db_name', 'g2isoluc_cardioritmo');
//END LAC-----

//----LOCALHOST-------
//Config::set('db_host', 'localhost');
//Config::set('db_user', 'root');
//Config::set('db_password', '');
//Config::set('db_name', 'financeiro_unic');
// END LOCALHOST------

#Email
// Email address of the sender
Config::set('email_from', 'emailfrom@domain.com');
// Name and surname of the sender
Config::set('email_name', 'John Smith');
// Set to true if authentication is required
Config::set('email_auth', true);
// Address of the SMTP server
Config::set('email_host', 'smtp.gmail.com');
// Port of the SMTP server
Config::set('email_port', 465);
// The secure connection prefix - ssl or tls
Config::set('email_secure', 'ssl');
// Login (Username or Email)
Config::set('email_login', 'login');
// Password
Config::set('email_password', 'password');

#Lucros e Dividendos
#Disponibiliza o relatorio de lucros e dividendos
Config::set('lucros', false);

# Origem
# se 1 = cultura
# se 2 js e stalucci
# se 3 js e chiesa
Config::set('origem',2);

# MODO DE DEPURAÇÃO
# desenvolvimento: true
# produção: false
Config::set('debug', false);

# TEMPLATE PADRÃO
# Ex: para o diretório /template/default:
# Config::set('template', 'default');
Config::set('template', 'inspinia');
//Config::set('template', 'sidebar');
// Config::set('template', 'topbar');
// Config::set('template', 'default');


# Chave da aplicação, para controle de sessões e criptografia
# Utilize uma cadeia alfanumérica aleatória única
Config::set('key', 'r528qy2014161d5jqtjeo3h8875gfFDGfqqqk3djghkH&*');

# SALT - Utilizada na criptografia
# Utiliza uma chave alfa-numéria complexa de no mínimo 16 dígitos
Config::set('salt', 'TjcqT8jgR8H6v6vhJhBcdZmCvnZs4Ghs9JcvW48gCqUTtVEkcK5hYLpsw6As8AbU');

Config::set('lang', 'pt_br');
Config::set('rewriteURL', true);
Config::set('indexController', 'Index');
Config::set('indexAction', 'index');
Config::set('criptedGetParamns', array());