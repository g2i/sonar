<?php
final class Cliente_endereco extends Record{ 

    const TABLE = 'cliente_endereco';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Cliente_endereco pertence a Cliente
    * @return Cliente $Cliente
    */
    function getCliente() {
        return $this->belongsTo('Cliente','cliente_id');
    }
}