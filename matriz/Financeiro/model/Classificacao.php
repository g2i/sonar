<?php
final class Classificacao extends Record{ 

    const TABLE = 'classificacaocontas';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Classificacao pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
    
    function getTipo() {
        return $this->belongsTo('Tipoclassificacao','tipo');
    }

    /**
    * Classificacao possui Planocontas
    * @return array de Planocontas
    */
    function getPlanocontas($criteria=NULL) {
        if(!empty($criteria)){
            $criteria = new Criteria();
            $criteria->addCondition('status', '=', 1);
            $criteria->setOrder('nome ASC');
        }
        return $this->hasMany('Planocontas','classificacao',$criteria);
    }
}