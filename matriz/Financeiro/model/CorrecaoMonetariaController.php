<?php

final class CorrecaoMonetariaController extends AppController {
    # página inicial do módulo CorrecaoMonetaria

    function index() {
        $this->setTitle('CorrecaoMonetaria');
    }

    # lista de CorrecaoMonetarias
    # renderiza a visão /view/CorrecaoMonetaria/all.php

    function all() {
        $this->setTitle('CorrecaoMonetarias');
        $p = new Paginate('CorrecaoMonetaria', 10);
        $this->set('search', NULL);
        $c = new Criteria();
        $c->addCondition("status_id", "<>", "3");
        if (isset($_GET['search'])) {
            $c->addCondition('nome', 'LIKE', '%' . $_GET['search'] . '%');
            $this->set('search', $this->getParam('search'));
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('CorrecaoMonetarias', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) CorrecaoMonetaria
    # renderiza a visão /view/CorrecaoMonetaria/view.php

    function view() {
        try {
            $this->set('CorrecaoMonetaria', new CorrecaoMonetaria((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('CorrecaoMonetaria', 'all');
        }
    }

    # formulário de cadastro de CorrecaoMonetaria
    # renderiza a visão /view/CorrecaoMonetaria/add.php

    function add() {
        $this->setTitle('Adicionar CorrecaoMonetaria');
        $this->set('CorrecaoMonetaria', new CorrecaoMonetaria);
        $c = new Criteria();
        $c->addCondition("id", "<>", 3);
        $this->set('Status', Status::getList($c));
    }

    # recebe os dados enviados via post do cadastro de CorrecaoMonetaria
    # (true)redireciona ou (false) renderiza a visão /view/CorrecaoMonetaria/add.php

    function post_add() {
        $this->setTitle('Adicionar CorrecaoMonetaria');
        $CorrecaoMonetaria = new CorrecaoMonetaria();
        $this->set('CorrecaoMonetaria', $CorrecaoMonetaria);
        try {
            $CorrecaoMonetaria->save($_POST);
            new Msg(__('CorrecaoMonetaria cadastrado com sucesso'));
            $this->go('CorrecaoMonetaria', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->set('Status', Status::getList());
    }

    # formulário de edição de CorrecaoMonetaria
    # renderiza a visão /view/CorrecaoMonetaria/edit.php

    function edit() {
        $this->setTitle('Editar CorrecaoMonetaria');
        try {
            $this->set('CorrecaoMonetaria', new CorrecaoMonetaria((int) $this->getParam('id')));
            $c = new Criteria();
            $c->addCondition("id", "<>", 3);
            $this->set('Status', Status::getList($c));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
            $this->go('CorrecaoMonetaria', 'all');
        }
    }

    # recebe os dados enviados via post da edição de CorrecaoMonetaria
    # (true)redireciona ou (false) renderiza a visão /view/CorrecaoMonetaria/edit.php

    function post_edit() {
        $this->setTitle('Editar CorrecaoMonetaria');
        try {
            $CorrecaoMonetaria = new CorrecaoMonetaria((int) $_POST['id']);
            $this->set('CorrecaoMonetaria', $CorrecaoMonetaria);
            $CorrecaoMonetaria->save($_POST);
            new Msg(__('CorrecaoMonetaria atualizado com sucesso'));
            $this->go('CorrecaoMonetaria', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Status', Status::getList());
    }

    # Confirma a exclusão ou não de um(a) CorrecaoMonetaria
    # renderiza a /view/CorrecaoMonetaria/delete.php

    function delete() {
        $this->setTitle('Apagar CorrecaoMonetaria');
        try {
            $this->set('CorrecaoMonetaria', new CorrecaoMonetaria((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('CorrecaoMonetaria', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) CorrecaoMonetaria
    # redireciona para CorrecaoMonetaria/all

    function post_delete() {
        try {
            $CorrecaoMonetaria = new CorrecaoMonetaria((int) $_POST['id']);
            $CorrecaoMonetaria->status_id = 3;
            $CorrecaoMonetaria->save();
            new Msg(__('CorrecaoMonetaria apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        $this->go('CorrecaoMonetaria', 'all');
    }

}
