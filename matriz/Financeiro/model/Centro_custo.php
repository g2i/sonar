<?php
final class Centro_custo extends Record{ 

    const TABLE = 'centro_custo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
         $criteria->addCondition('status','<>',3);
            return $criteria;
    }
    
    /**
    * Centro_custo pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
    
    /**
    * Centro_custo possui Empresa_custos
    * @return array de Empresa_custos
    */
    function getEmpresa_custos($criteria=NULL) {
        return $this->hasMany('Empresa_custo','centro_custo',$criteria);
    }
    
    /**
    * Centro_custo possui Plano_centrocustos
    * @return array de Plano_centrocustos
    */
    function getPlano_centrocustos($criteria=NULL) {
        return $this->hasMany('Plano_centrocusto','centro_custo',$criteria);
    }
}