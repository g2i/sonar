<?php
final class DeducoesProgramacao extends Record{ 

    const TABLE = 'deducoes_programacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * DeducoesProgramacao pertence a Programacao
    * @return Programacao $Programacao
    */
    function getProgramacao() {
        return $this->belongsTo('Programacao','Programacao_id');
    }
    
    /**
    * DeducoesProgramacao pertence a Deducoes
    * @return Deducoes $Deducoes
    */
    function getDeducoes() {
        return $this->belongsTo('Deducoes','deducoes_id');
    }
}