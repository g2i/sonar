<?php

final class Periodicidade extends Record {

    const TABLE = 'periodicidade';
    const PK = 'id';

    function getStatus() {
        return $this->belongsTo('Status', 'status');
    }
    
    function getProgramacao() {
        return $this->hasMany('Programacao', 'periodicidade');
    }

}
