<?php
final class Rhusuario_localidade extends Record{ 

    const TABLE = 'rhusuario_localidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rhusuario_localidade pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','usuario_id');
    }
    
    /**
    * Rhusuario_localidade pertence a Rhlocalidade
    * @return Rhlocalidade $Rhlocalidade
    */
    function getRhlocalidade() {
        return $this->belongsTo('Rhlocalidade','rhlocalidade_id');
    }
    
    /**
    * Rhusuario_localidade pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
}