<?php
final class Rhlocalidade extends Record{ 

    const TABLE = 'rhlocalidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rhlocalidade pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Rhlocalidade possui Rhusuario_localidades
    * @return array de Rhusuario_localidades
    */
    function getRhusuario_localidades($criteria=NULL) {
        return $this->hasMany('Rhusuario_localidade','rhlocalidade_id',$criteria);
    }
}