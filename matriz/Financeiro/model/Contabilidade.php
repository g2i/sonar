<?php
final class Contabilidade extends Record{ 

    const TABLE = 'contabilidade';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Contabilidade pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }

    /**
     * Contabilidade possui Grupos
     * @return array de Grupos
     */
    function getGrupos() {
        return $this->hasMany('GruposContabilidade','contabilidade');
    }
    
    /**
    * Contabilidade possui Contaspagares
    * @return array de Contaspagares
    */
    function getContaspagares() {
        return $this->hasMany('Contaspagar','contabilidade');
    }
    
    /**
    * Contabilidade possui Contasreceberes
    * @return array de Contasreceberes
    */
    function getContasreceberes() {
        return $this->hasMany('Contasreceber','contabilidade');
    }
}