<?php
final class Rateio_projetos_planos_contas extends Record{ 

    const TABLE = 'rateio_projetos_planos_contas';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Rateio_projetos_planos_contas pertence a Planocontas
    * @return Planocontas $Planocontas
    */
    function getPlanocontas() {
        return $this->belongsTo('Planocontas','id_plano_contas');
    }
    
    /**
    * Rateio_projetos_planos_contas pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao');
    }
    
    /**
    * Rateio_projetos_planos_contas pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','user_created');
    }
    
    /**
    * Rateio_projetos_planos_contas pertence a Fin_projeto_unidade
    * @return Fin_projeto_unidade $Fin_projeto_unidade
    */
    function getFin_projeto_unidade() {
        return $this->belongsTo('Fin_projeto_unidade','id_unidade');
    }
    
    /**
    * Rateio_projetos_planos_contas pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','user_modified');
    }
}