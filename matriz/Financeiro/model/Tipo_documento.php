<?php
final class Tipo_documento extends Record{ 

    const TABLE = 'tipo_documento';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Tipo_pagamento possui Contaspagares
    * @return array de Contaspagares
    */
    function getContaspagares() {
        return $this->hasMany('Contaspagar','tipoDocumento');
    }
    
    /**
    * Tipo_pagamento possui Contasreceberes
    * @return array de Contasreceberes
    */
    function getContasreceberes() {
        return $this->hasMany('Contasreceber','tipoDocumento');
    }
    
    /**
    * Tipo_pagamento possui Programacaos
    * @return array de Programacaos
    */
    function getProgramacaos() {
        return $this->hasMany('Programacao','tipoDocumento');
    }
    
    /**
    * Tipo_pagamento pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status_id');
    }
}