<?php
final class Plano_centrocusto extends Record{ 

    const TABLE = 'plano_centrocusto';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
        return $criteria;
    }
    
    /**
    * Plano_centrocusto pertence a Planocontas
    * @return Planocontas $Planocontas
    */
    function getPlanocontas() {
        return $this->belongsTo('Planocontas','plano_contas');
    }
    
    /**
    * Plano_centrocusto pertence a Centro_custo
    * @return Centro_custo $Centro_custo
    */
    function getCentro_custo() {
        return $this->belongsTo('Centro_custo','centro_custo');
    }
    
    /**
    * Plano_centrocusto pertence a Contabilidade
    * @return Contabilidade $Contabilidade
    */
    function getContabilidade() {
        return $this->belongsTo('Contabilidade','empresa');
    }
    
    /**
    * Plano_centrocusto pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
}