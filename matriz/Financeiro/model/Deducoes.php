<?php
final class Deducoes extends Record{ 

    const TABLE = 'deducoes';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Deducoes pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
}