<?php
final class Estq_subestoque extends Record{ 

    const TABLE = 'estq_subestoque';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Estq_subestoque possui Estq_artigos
    * @return array de Estq_artigos
    */
    function getEstq_artigos($criteria=NULL) {
        return $this->hasMany('Estq_artigo','substoque_id',$criteria);
    }
    
    /**
    * Estq_subestoque possui Estq_artigo_subestoques
    * @return array de Estq_artigo_subestoques
    */
    function getEstq_artigo_subestoques($criteria=NULL) {
        return $this->hasMany('Estq_artigo_subestoque','substoque_id',$criteria);
    }
    
    /**
    * Estq_subestoque possui Estq_grupos
    * @return array de Estq_grupos
    */
    function getEstq_grupos($criteria=NULL) {
        return $this->hasMany('Estq_grupo','substoque_id',$criteria);
    }
    
    /**
    * Estq_subestoque possui Estq_lote_substoques
    * @return array de Estq_lote_substoques
    */
    function getEstq_lote_substoques($criteria=NULL) {
        return $this->hasMany('Estq_lote_substoque','subestoque',$criteria);
    }
    
    /**
    * Estq_subestoque pertence a Estq_situacao
    * @return Estq_situacao $Estq_situacao
    */
    function getEstq_situacao() {
        return $this->belongsTo('Estq_situacao','situacao');
    }
    
    /**
    * Estq_subestoque pertence a Estq_tiposubestoque
    * @return Estq_tiposubestoque $Estq_tiposubestoque
    */
    function getEstq_tiposubestoque() {
        return $this->belongsTo('Estq_tiposubestoque','tipo');
    }
    
    /**
    * Estq_subestoque possui Estq_usuario_substoques
    * @return array de Estq_usuario_substoques
    */
    function getEstq_usuario_substoques($criteria=NULL) {
        return $this->hasMany('Estq_usuario_substoque','substoque_id',$criteria);
    }
}