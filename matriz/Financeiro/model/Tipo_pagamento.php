<?php
final class Tipo_pagamento extends Record{ 

    const TABLE = 'tipo_pagamento';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Tipo_pagamento pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status_id');
    }
}