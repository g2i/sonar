<?php

final class MovimentoBanco extends Record {

    const TABLE = 'movimento_banco';
    const PK = 'id';

    /**
     * Configurações e filtros globais do modelo
     * @return Criteria $criteria
     */
    public static function configure() {
        # $criteria = new Criteria();
        # return $criteria;
    }

    public function getBanco() {
        return $this->belongsTo('Banco', 'idBanco');
    }

    function getSaldo($mes, $ano, $banco) {
        $db = new MysqlDB();
        $sql = "SELECT * FROM movimento_banco WHERE mes = :mes and ano = :ano and idBanco = :banco ORDER BY id DESC limit 1 ";
        $db->query($sql);
        $db->bind(':mes', $mes);
        $db->bind(':ano', $ano);
        $db->bind(':banco', $banco);
        $db->execute();
        return $db->getRow();
    }

    public static function getMovimento($situacao, $grupo, $empresas) {
        $db = new MysqlDB();
        $query = "SELECT b.nome AS banco, CONCAT(CONCAT(LPAD(mb.`mes`, 2, '0'),'/'),mb.`ano`) AS mes,
                  (mb.saldoInicial + IFNULL(SUM(CASE WHEN m.tipo = 1
	              THEN (m.debito * -1) ELSE m.credito END), 0)) AS saldo
                  FROM movimento_banco mb
                  LEFT JOIN banco b ON b.`id` = mb.`idBanco`
                  LEFT JOIN movimento m ON m.`idMovimentoBanco` = mb.`id` AND m.`status`=1
                  WHERE  mb.`status` = :situacao AND b.`mostra_saldo_geral` = 1  ";

        if(empty($empresas)){
            $query .= " AND mb.idBanco IN(SELECT banco FROM contabilidade_banco AS cb WHERE
                     cb.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo))";
        }else{
            $query .= " AND mb.idBanco IN(SELECT banco FROM contabilidade_banco AS cb WHERE
                     cb.contabilidade IN(:empresas))";
        }

        $query .= " GROUP BY mes, mb.idBanco ORDER BY mes, banco";

        $db->query($query);
        $db->bind(':situacao', $situacao);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        $db->execute();
        return $db->getResults();
    }

    public static function getSaldoInicial($grupo, $empresas, $inicio){

        if(empty($empresas)){
            $sql = "SELECT (SUM(IFNULL(mb.saldoInicial, 0)) +
                    (SELECT IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0)
                    FROM movimento AS m INNER JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                    INNER JOIN banco AS b ON (b.id = mb.idBanco) WHERE b.mostra_fluxo = TRUE AND
                    DATE(m.data) <= :inicio AND mb.status = 'Aberto' AND m.status = 1 AND
                    mb.idBanco IN(SELECT DISTINCT banco FROM contabilidade_banco AS cb WHERE
                    cb.contabilidade IN(SELECT DISTINCT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo)))) AS valor FROM movimento_banco AS mb
                    INNER JOIN banco AS b ON (b.id = mb.idBanco)
                    WHERE mb.status = 'Aberto' AND b.mostra_fluxo = 1
                    AND mb.idBanco IN(SELECT DISTINCT banco FROM contabilidade_banco AS cb WHERE
                    cb.contabilidade IN(SELECT DISTINCT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo))";
        }else{
            $sql = "SELECT (SUM(IFNULL(mb.saldoInicial, 0)) +
                    (SELECT IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0)
                    FROM movimento AS m INNER JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                    INNER JOIN banco AS b ON (b.id = mb.idBanco) WHERE b.mostra_fluxo = TRUE AND
                    DATE(m.data) <= :inicio AND mb.status = 'Aberto' AND m.status = 1 AND
                    mb.idBanco IN(SELECT DISTINCT banco FROM contabilidade_banco AS cb WHERE
                    cb.contabilidade IN(:empresas)))) AS valor FROM movimento_banco AS mb
                    INNER JOIN banco AS b ON (b.id = mb.idBanco)
                    WHERE mb.status = 'Aberto' AND b.mostra_fluxo = 1
                    AND mb.idBanco IN(SELECT DISTINCT banco FROM contabilidade_banco AS cb WHERE
                    cb.contabilidade IN(:empresas))";
        }

        $db = new MysqlDB();
        $db->query($sql);
        $db->execute();

        $db->bind(':inicio', $inicio);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        $ret = $db->getRow()->valor;
        return $ret == null ? 0 : floatval($ret);
    }

    public static function getSaldoFiltro($inicio, $banco, $movimento){
        if(!empty($movimento)){
            $sql = "SELECT (SUM(IFNULL(mb.saldoInicial, 0)) +
                    (SELECT IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0)
                    FROM movimento AS m INNER JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                    WHERE DATE(m.data) < :inicio AND m.status <> 3 AND mb.id = :movimento)) AS valor
                    FROM movimento_banco AS mb WHERE mb.id = :movimento";
        }else if(!empty($banco)){
            $sql = "SELECT (SUM(IFNULL(mb.saldoInicial, 0)) +
                    (SELECT IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0)
                    FROM movimento AS m INNER JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                    WHERE DATE(m.data) < :inicio AND mb.status = 'Aberto' AND m.status <> 3 AND
                    mb.idBanco = :banco)) AS valor FROM movimento_banco AS mb
                    WHERE mb.status = 'Aberto' AND mb.idBanco = :banco";
        }else{
            $sql = "SELECT (SUM(IFNULL(mb.saldoInicial, 0)) +
                    (SELECT IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0)
                    FROM movimento AS m INNER JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                    WHERE DATE(m.data) < :inicio AND mb.status = 'Aberto' AND m.status <> 3)) AS valor
                    FROM movimento_banco AS mb WHERE mb.status = 'Aberto'";
        }

        $db = new MysqlDB();
        $db->query($sql);
        $db->bind(':inicio', $inicio);

        if(!empty($movimento)){
            $db->bind(':movimento', $movimento);
        }else if(!empty($banco)){
            $db->bind(':banco', $banco);
        }

        $db->execute();
        $ret = $db->getRow()->valor;
        return $ret == null ? 0 : floatval($ret);
    }

    public static function getSaldoMovimento($inicio, $movimento){

        $sql = "SELECT (SUM(IFNULL(mb.saldoInicial, 0)) +
                (SELECT IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0)
                FROM movimento AS m INNER JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                WHERE DATE(m.data) < :inicio AND mb.id = :movimento AND m.status <> 3)) AS valor
                FROM movimento_banco AS mb WHERE  mb.id = :movimento";

        $db = new MysqlDB();
        $db->query($sql);
        $db->execute();

        $db->bind(':inicio', $inicio);

        if(!empty($banco)){
            $db->bind(':banco', $banco);
        }else if(!empty($movimento)){
            $db->bind(':movimento', $movimento);
        }

        $ret = $db->getRow()->valor;
        return $ret == null ? 0 : floatval($ret);
    }

    public static function SumDiaDia($dia){
        $sql = "SELECT IFNULL(SUM(CASE WHEN m.tipo = 1 THEN (m.debito * -1) ELSE m.credito END), 0) AS valor
                FROM movimento AS m
                INNER JOIN banco AS b ON (b.id = m.banco)
                INNER JOIN movimento_banco AS mb ON (mb.id = m.idMovimentoBanco)
                WHERE m.data = CONCAT(DATE_FORMAT(NOW(), '%Y-%m-'), :dia)
			    AND mb.status = 'Aberto' AND b.mostra_fluxo = TRUE";

        $db = new MysqlDB();
        $db->query($sql);
        $db->bind(':dia', $dia);
        try{
            $db->execute();
            return $db->getRow();
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
    }

    public static function HasMovimento($mes, $ano, $banco){
        $sql = "SELECT EXISTS(SELECT * FROM movimento_banco AS mb
                WHERE mb.mes = :mes AND mb.ano = :ano AND mb.idBanco = :banco) AS existe";

        $db = new MysqlDB();
        $db->query($sql);
        $db->bind(':mes', $mes);
        $db->bind(':ano', $ano);
        $db->bind(':banco', $banco);
        try{
            $db->execute();
            $ret = $db->getRow();
            return $ret->existe == 1 ? true : false;
        }catch (Exception $e){
            return true;
        }
    }

}
