<?php
final class Planocontas extends Record{ 

    const TABLE = 'planocontas';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
         $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Planocontas possui Contaspagares
    * @return array de Contaspagares
    */
    function getContaspagares() {
        return $this->hasMany('Contaspagar','idPlanoContas');
    }
    
    /**
    * Planocontas pertence a Classificacao
    * @return Classificacao $Classificacao
    */
    function getClassificacao() {
        return $this->belongsTo('Classificacao','classificacao');
    }

    static function TipoPlano($tipo) {
        $db = new MysqlDB();
        $sql = "SELECT P.* FROM planocontas P LEFT JOIN classificacaocontas C ON P.`classificacao` = C.`id`
                WHERE C.tipo = :tipo and P.status = 1 ORDER BY P.nome";
        $db->query($sql);
        $db->bind(':tipo', $tipo);
        $db->execute();
        return $db->getResults();
    }

    /**
    * Planocontas pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
}