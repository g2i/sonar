<?php
final class Situacao extends Record{ 

    const TABLE = 'situacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Situacao possui Bancos
    * @return array de Bancos
    */
    function getBancos($criteria=NULL) {
        return $this->hasMany('Banco','situacao',$criteria);
    }
    
    /**
    * Situacao possui Casas
    * @return array de Casas
    */
    function getCasas($criteria=NULL) {
        return $this->hasMany('Casa','situacao',$criteria);
    }
    
    /**
    * Situacao possui Contas
    * @return array de Contas
    */
    function getContas($criteria=NULL) {
        return $this->hasMany('Contas','situacao',$criteria);
    }
    
    /**
    * Situacao possui Custos
    * @return array de Custos
    */
    function getCustos($criteria=NULL) {
        return $this->hasMany('Custos','situacao',$criteria);
    }
    
    /**
    * Situacao possui Diarias_lancamentos
    * @return array de Diarias_lancamentos
    */
    function getDiarias_lancamentos($criteria=NULL) {
        return $this->hasMany('Diarias_lancamento','situacao',$criteria);
    }
    
    /**
    * Situacao possui Funcionarios
    * @return array de Funcionarios
    */
    function getFuncionarios($criteria=NULL) {
        return $this->hasMany('Funcionarios','situacao',$criteria);
    }
    
    /**
    * Situacao possui Maquinas
    * @return array de Maquinas
    */
    function getMaquinas($criteria=NULL) {
        return $this->hasMany('Maquinas','situacao',$criteria);
    }
    
    /**
    * Situacao possui Producaos
    * @return array de Producaos
    */
    function getProducaos($criteria=NULL) {
        return $this->hasMany('Producao','situacao',$criteria);
    }
    
    /**
    * Situacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','quemCadastro');
    }
    
    /**
    * Situacao possui Tipo_contas
    * @return array de Tipo_contas
    */
    function getTipo_contas($criteria=NULL) {
        return $this->hasMany('Tipo_conta','situacao',$criteria);
    }
    
    /**
    * Situacao possui Tipo_maquinas
    * @return array de Tipo_maquinas
    */
    function getTipo_maquinas($criteria=NULL) {
        return $this->hasMany('Tipo_maquina','situacao',$criteria);
    }
    
    /**
    * Situacao possui Tipo_producaos
    * @return array de Tipo_producaos
    */
    function getTipo_producaos($criteria=NULL) {
        return $this->hasMany('Tipo_producao','situacao',$criteria);
    }
    
    /**
    * Situacao possui Tipos_lancamentos
    * @return array de Tipos_lancamentos
    */
    function getTipos_lancamentos($criteria=NULL) {
        return $this->hasMany('Tipos_lancamentos','situacao',$criteria);
    }
    
    /**
    * Situacao possui Usuarios
    * @return array de Usuarios
    */
    function getUsuarios($criteria=NULL) {
        return $this->hasMany('Usuario','situacao',$criteria);
    }
}