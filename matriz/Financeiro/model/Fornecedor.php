<?php
final class Fornecedor extends Record{ 

    const TABLE = 'fornecedor';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Fornecedor possui Contaspagares
    * @return array de Contaspagares
    */
    function getContaspagares() {
        return $this->hasMany('Contaspagar','idFornecedor');
    }
    
    /**
    * Fornecedor pertence a Status
    * @return Status $Status
    */
    function getStatus() {
        return $this->belongsTo('Status','status');
    }
}