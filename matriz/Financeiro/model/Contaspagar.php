<?php

final class Contaspagar extends Record {

    const TABLE = 'contaspagar';
    const PK = 'id';

    public static function configure() {
        # $criteria = new Criteria();
        # return $criteria;
    }

    function getPlanocontas() {
        return $this->belongsTo('Planocontas', 'idPlanoContas');
    }

    function getFornecedor() {
        return $this->belongsTo('Fornecedor', 'idFornecedor');
    }

    function getStatus() {
        return $this->belongsTo('Status', 'status');
    }

    function getContabilidade() {
        return $this->belongsTo('Contabilidade', 'contabilidade');
    }

    function getTipo_documento() {
        return $this->belongsTo('Tipo_documento', 'tipoDocumento');
    }

    public function graficoMes_old() {
        $db = new MysqlDB();
        $sql = "SELECT EXTRACT(MONTH FROM DATA) AS mes,
            (SELECT COUNT(id) FROM contaspagar WHERE dataPagamento IS NULL AND EXTRACT(MONTH FROM DATA) = mes) AS pendentes,
            (SELECT COUNT(id) FROM contaspagar WHERE dataPagamento IS NOT NULL AND EXTRACT(MONTH FROM DATA) = mes) AS pagas
            FROM contaspagar GROUP BY EXTRACT(MONTH FROM DATA)";
        $db->query($sql);
        return $db->getResults();
    }

    public static function graficoMes($inicio,$fim,$empresa) {
        $query = "SELECT DATE_FORMAT(CP.`vencimento`,'%Y%m') AS mes, (SUM(IFNULL(CP.`valor`, 0))-SUM(IFNULL(DCP.`valor`, 0))) AS valor
                  FROM contaspagar CP LEFT JOIN deducoes_contaspagar DCP ON DCP.`contaspagar_id`= CP.`id`
                  WHERE CP.`vencimento`>= :inicio AND CP.`vencimento` <= :fim AND CP.contabilidade = :empresa  GROUP BY mes ORDER BY mes ASC";
        $db = new MysqlDB();
        $db->query($query);
        $db->bind(':inicio', $inicio);
        $db->bind(':fim', $fim);
        $db->bind(':empresa', $empresa);
        $db->execute();
        return $db->getResults();
    }

    public function newMes($inicio,$fim) {
        $db = new MysqlDB();
        $sql = "SELECT DATE_FORMAT(CP.`vencimento`,'%Y%m') AS mes, IF(SUM(DCP.`valor`) IS NULL,SUM(CP.`valor`),(SUM(CP.`valor`)-SUM(DCP.`valor`))) AS valor
                FROM contaspagar CP
                LEFT JOIN deducoes_contaspagar DCP
                ON DCP.`contaspagar_id`= CP.`id`
                WHERE CP.`vencimento`>= '".$inicio."'
                AND CP.`vencimento` <= '".$fim."'
                GROUP BY mes ORDER BY mes ASC";
        $db->query($sql);
        return $db->getResults();
    }

    public static function fluxoCaixaResumido($grupo, $empresas){
        if(empty($empresas)){
            $query = "SELECT
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contaspagar AS c
                    WHERE c.status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) = NOW()
                    AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo)) AS dia,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contaspagar AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01')
                    AND LAST_DAY(NOW()) AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo)) AS mes,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contaspagar AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-01-01')
                    AND DATE_FORMAT(NOW() ,'%Y-12-31') AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                    WHERE cg.grupo = :grupo)) AS ano ";
        }else{
            $query = "SELECT
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contaspagar AS c
                    WHERE c.status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) = NOW()
                    AND c.contabilidade IN(:empresas)) AS dia,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contaspagar AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(c.vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01')
                    AND LAST_DAY(NOW()) AND c.contabilidade IN(:empresas)) AS mes,
                  (SELECT IFNULL(ROUND(SUM(IFNULL(c.valor, 0)), 2), 0) FROM contaspagar AS c
                    WHERE status = 1 AND c.dataPagamento IS NULL AND DATE(vencimento) BETWEEN DATE_FORMAT(NOW() ,'%Y-01-01')
                    AND DATE_FORMAT(NOW() ,'%Y-12-31') AND c.contabilidade IN(:empresas)) AS ano ";
        }

        $db = new MysqlDB();
        $db->query($query);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        $db->execute();
        return $db->getRow();
    }

    public static function fluxoCaixa($empid){

        $fluxo = array();
        $query="CALL getContasPagarMes(:empid)";
        $smtp = MysqlDB::Conexao()->prepare($query);
        $smtp->bindParam(':empid', $empid,PDO::PARAM_INT);
        try {
            $smtp->execute();
            do {
                $fluxo[] = $smtp->fetch(PDO::FETCH_OBJ);
            } while ($smtp->nextRowset());

            return $fluxo;
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public static function SumMes($inicio, $grupo, $empresas){

        $sql = "SELECT ROUND(SUM(IFNULL(valor, 0)), 2) AS valor
                FROM contaspagar AS c WHERE DATE(c.vencimento) < :inicio
                AND c.status = 1 AND c.dataPagamento IS NULL";

        if(empty($empresas)){
            $sql .= " AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo)";
        }else{
            $sql .= " AND c.contabilidade IN(:empresas)";
        }

        $db = new MysqlDB();
        $db->query($sql);
        $db->bind(':inicio', $inicio);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        try{
            $db->execute();
            $ret = $db->getRow()->valor;
            return $ret == null ? 0 : floatval($ret);
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
    }

    public static function SumDiaDia($dia, $grupo, $empresas){
        $sql = "SELECT ROUND(IFNULL(SUM(IFNULL(valor, 0)),0), 2) AS valor
                FROM contaspagar AS c WHERE c.vencimento = :dia
                AND c.status = 1 AND c.dataPagamento IS NULL";

        if(empty($empresas)){
            $sql .= " AND c.contabilidade IN(SELECT contabilidade FROM contabilidade_grupo AS cg
                     WHERE cg.grupo = :grupo)";
        }else{
            $sql .= " AND c.contabilidade IN(:empresas)";
        }

        $db = new MysqlDB();
        $db->query($sql);
        $db->bind(':dia', $dia);

        if(empty($empresas)){
            $db->bind(':grupo', $grupo);
        }else{
            $db->bind(':empresas', $empresas);
        }

        try{
            $db->execute();
            $ret = $db->getRow()->valor;
            return $ret;
        }catch (Exception $e){
            echo $e->getTraceAsString();
        }
    }

    function getSolicitcaoPendentes(){
        $db = new MysqlDB();
        $sql = "SELECT s.id, s.`solicitante`, ss.`nome` AS situacao, 
        f.`nome` AS fornecedor, con.nome AS filial  
        FROM solicitacao s 
        JOIN situacao_solicitacao ss ON ss.`id` = s.`situacao_solicitacao_id`
        JOIN fornecedor f ON f.`id` = s.`idFornecedor`
        JOIN `contabilidade` con ON con.`id` = s.`contabilidade`
        WHERE s.`status` = 2
        AND s.`situacao_solicitacao_id` = 1 ";
  
        $db->query($sql);
          return $db->getResults();
      }
}
