<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Rateio</h2>
        <ol class="breadcrumb">
            <li>Contas a Receber</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Valor</strong>: <?php echo $Rateio_contasreceber->valor; ?></p>

                <p><strong>Observacao</strong>: <?php echo $Rateio_contasreceber->observacao; ?></p>

                <p>
                    <strong>Contasreceber</strong>:
                    <?php
                    echo $this->Html->getLink($Rateio_contasreceber->getContasreceber()->documento, 'Contasreceber', 'view',
                        array('id' => $Rateio_contasreceber->getContasreceber()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Contabilidade</strong>:
                    <?php
                    echo $this->Html->getLink($Rateio_contasreceber->getContabilidade()->nome, 'Contabilidade', 'view',
                        array('id' => $Rateio_contasreceber->getContabilidade()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>