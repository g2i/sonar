<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Mapa</li>
            <li class="active">
                <strong>Mensal</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row clearfix">
                    <form class="form-inline">
                        <div class="form-group" style="width: 250px">
                            <label for="grupos" class="form-group">Grupos</label>
                            <div class="input-group select2-bootstrap-append">
                                <select name="grupos" class="form-control" id="grupos">
                                    <option></option>
                                    <?php foreach ($grupos as $e): ?>
                                        <option value="<?php echo $e->id ?>"><?php echo $e->descricao ?></option>
                                    <?php endforeach; ?>
                                </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-clear="grupos"
                                                data-toggle="tooltip" data-placement="top" title="Limpar">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group" style="width: 250px">
                            <label for="empresa">Empresa</label>
                            <div class="input-group select2-bootstrap-append">
                                <select name="empresa" class="form-control" id="empresa">
                                    <option></option>
                                </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-clear="empresa"
                                                data-toggle="tooltip" data-placement="top" title="Limpar">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </span>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="ano" class="form-group">Ano</label><br/>
                            <select id="ano" name="ano" class="form-control selectPicker"
                                    data-placeholder="Selecione">
                                <option></option>
                                <?php
                                for ($i = date('Y'); $i >= date("Y",strtotime("-5 year")); $i--) {
                                    echo "<option value='" . $i . "'>" . $i . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" style="margin-top: 25px">
                            <button type="button" class="btn btn-default" id="btnFiltro"><span
                                    class="glyphicon glyphicon-search"></span></button>
                            <button id="btnRel" title="Gerar Relatório" type="button"
                                    class="btn btn-default">
                                <span class="glyphicon glyphicon-print"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div id="dados" class="row clearfix">
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function getDados(){
        var url = root + '/Relatorio/dadosMapa';
        var data = {
            grupo: $('#grupos').val(),
            empresa: $('#empresa').val() ? $('#empresa').val().join(',') : '',
            ano: $("#ano").val()
        }

        $('#dados').html('');
        $('#dados').html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        $.post(url, data, function(result){
            if(!result){
                $("#dados").html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            $("#dados").html(result);
        });
    }

    $(document).ready(function(){
        var grupos = $('#grupos').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Todos'
        });

        var empresa = $('#empresa').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            multiple: true,
            placeholder: 'Todas'
        });

        grupos.change(function() {
            if ($(this).val() == '') {
                empresa.select2('destroy');
                empresa.val('');
                empresa.html('');
                empresa.append($('<option></option>'));
                empresa.select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    multiple: true,
                    placeholder: 'Todas'
                });
            }
            else {
                var url = root + '/Index/getEmpresas';
                var data = {
                    id: $(this).val()
                }

                $.get(url, data, function (ret) {
                    if (ret.result) {
                        empresa.select2('destroy');
                        empresa.html('');
                        empresa.append($('<option></option>'));
                        ret.dados.forEach(function (dado) {
                            var option = $('<option value="' + dado.id + '" >' + dado.nome + '</option>');
                            empresa.append(option);
                        });
                        empresa.select2({
                            language: 'pt-BR',
                            theme: 'bootstrap',
                            width: '100%',
                            multiple: true,
                            placeholder: 'Todas'
                        });
                    } else {
                        BootstrapDialog.alert(ret.msg);
                        console.log(ret.error);
                    }
                });
            }
        });

        $("button[data-select2-clear]").click(function() {
            $("#" + $(this).data("select2-clear")).val('').trigger('change');
        });

        $('#btnFiltro').click(function(){
            getDados();
        });

        $('#btnRel').click(function(){
            var data = {
                ano: $("#ano").val(),
                grupo: $("#grupos").val() ? $("#grupos").val() : '-1',
                empresa: $("#empresa").val() ? $("#empresa").val().join(',') : '-1'
            }

            if(data.grupo == ''){
                data.grupo = -1;
            }

            if(data.empresa == ''){
                data.empresa = -1;
            }

            if (data.ano == "" || data.ano == null) {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione um ano!',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return false;
            }

            $.ReportPost('analit', data);
        });

        getDados();
    });
</script>