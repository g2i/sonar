<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Empresa/Centro Custo</h2>
        <ol class="breadcrumb">
            <li> Empresa/Centro Custo</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Empresa_custo', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="form-group col-md-12">
                        <label for="centro_custo">Centro de custo</label>
                        <select name="centro_custo" class="form-control selectPicker" id="centro_custo"
                                role="centro_custo">
                            <option value="">Selecione</option>
                            <?php
                            foreach ($Centro_custos as $c) {
                                if ($c->id == $Empresa_custo->centro_custo)
                                    echo '<option selected value="' . $c->id . '">' . $c->descricao . '</option>';
                                else
                                    echo '<option value="' . $c->id . '">' . $c->descricao . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-12">
                        <label for="empresa">Empresa</label>
                        <select name="empresa" class="form-control selectPicker" id="empresa">
                            <option value="">Selecione</option>
                            <?php
                            foreach ($Contabilidades as $c) {
                                if ($c->id == $Empresa_custo->empresa)
                                    echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                else
                                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" name="status" value="1"/>

                    <div class="row">
                        <?php if ($this->getParam('modal') == 1) { ?>
                            <input type="hidden" name="modal" value="1"/>

                            <div class="text-right">
                                <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
                                   data-toggle="tooltip2" title="Voltar"
                                   onclick="Navegar('','back')">
                                    Cancelar</a>
                                <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                                       value="salvar">
                            </div>
                        <?php } else { ?>
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Empresa_custo', 'all') ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                        <?php } ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>