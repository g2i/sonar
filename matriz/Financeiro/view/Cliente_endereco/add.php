<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cadrastrar Endereco</h2>
        <ol class="breadcrumb">
            <li>Cadrastrar Endereco</li>
            <li class="active">
                <strong>Adicionar Endereco</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Cliente_endereco', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="col-md-6">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="cliente_id" class="required">Cliente <span class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="cliente_id" class="form-control" required id="cliente_id">
                                        <?php
                                        foreach ($Clientes as $c) {
                                            if ($c->id == $Cliente_endereco->cliente_id)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="cep" class="required">Cep<span class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" rel="txtTooltip" role="cep" cep-ajax-url="<?= $this->Html->getUrl('Cep', 'busca')?>" required cep-init="LoadGif" cep-done="CloseGif"
                                           name="cep" id="cep" class="form-control cep"
                                           placeholder="Cep" title="Informe o CEP - Preenchimento Automático"
                                           data-toggle="tooltip" data-placement="top">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="endereco" class="required">endereco <span class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="endereco" data-cep="endereco" required id="endereco" class="form-control"
                                           value="<?php echo $Cliente_endereco->endereco ?>" placeholder="Endereco">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="uf" class="required">Estado <span class="glyphicon glyphicon-asterisk"></span></label>
                                    <select name="uf" id="uf" data-cep="uf" required class="form-control selectPicker">
                                        <option value="">Selecione</option>
                                        <option value="AC">AC</option>
                                        <option value="AL">AL</option>
                                        <option value="AM">AM</option>
                                        <option value="AP">AP</option>
                                        <option value="BA">BA</option>
                                        <option value="CE">CE</option>
                                        <option value="DF">DF</option>
                                        <option value="ES">ES</option>
                                        <option value="GO">GO</option>
                                        <option value="MA">MA</option>
                                        <option value="MG">MG</option>
                                        <option value="MS">MS</option>
                                        <option value="MT">MT</option>
                                        <option value="PA">PA</option>
                                        <option value="PB">PB</option>
                                        <option value="PE">PE</option>
                                        <option value="PI">PI</option>
                                        <option value="PR">PR</option>
                                        <option value="RJ">RJ</option>
                                        <option value="RN">RN</option>
                                        <option value="RS">RS</option>
                                        <option value="RO">RO</option>
                                        <option value="RR">RR</option>
                                        <option value="SC">SC</option>
                                        <option value="SE">SE</option>
                                        <option value="SP">SP</option>
                                        <option value="TO">TO</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="cidade" class="required">Cidade <span class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="cidade" data-cep="cidade" required id="cidade" class="form-control"
                                           value="<?php echo $Cliente_endereco->cidade ?>" placeholder="Cidade">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="bairro" class="required">Bairro <span class="glyphicon glyphicon-asterisk"></span></label>
                                    <input type="text" name="bairro" data-cep="bairro" required id="bairro" class="form-control"
                                           value="<?php echo $Cliente_endereco->bairro ?>" placeholder="Bairro">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="numero">Numero</label>
                                    <input type="text" name="numero" id="numero" class="form-control"
                                           value="<?php echo $Cliente_endereco->numero ?>" placeholder="Numero">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="complemento">Complemento</label>
                                    <input type="text" name="complemento" id="complemento" class="form-control"
                                           value="<?php echo $Cliente_endereco->complemento ?>"
                                           placeholder="Complemento">
                                </div>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <label for="observacao">Observacao</label>
                                <textarea name="observacao" rows="12" id="observacao"
                                          class="form-control"><?php echo $Cliente_endereco->observacao ?></textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Cliente_endereco', 'all') ?>"
                               class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>