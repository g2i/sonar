<div class="text-right pull-right">
    <p><?php
       /* echo $this->Html->getLink('Anexar arquivo', 'Anexo', 'add',
            array('ajax' => 'true'),
            array(
                'class' => 'btn btn-success btn-sm',
                'title' => 'Anexar arquivo',
                'id' => 'abrir-modal'
            )
        );*/
        echo '<a href="javascript:;" data-placement="bottom" data-role="tooltip"  title="Anexar arquivo" class="btn btn-success btn-sm" id="abrir-modal"
      onclick="Navegar(\'' . $this->Html->getUrl("Anexo", "add", array("ajax" => true,  "modal" => "1", "id_externo" => @$id_externo, "tipo" => @$tipo)) . '\',\'go\')">
        Anexar arquivo
      </a>';
       
        ?></p>
</div>
<br/>

<div class="clearfix">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Titulo</th>
                <!--<th>Tipo</th>-->
                <th>Anexo</th>
                <th width="50">&nbsp;</th>
            </tr>
            <?php
            $i = 1;
            foreach ($Anexos as $a) {
                echo '<tr>';
                echo '<td>' . (($a->titulo == "") ? "Anexo {$i}" : $a->titulo) . '</td>';
                echo '<!--<td>' . $a->tipo . '</td>-->';

                echo '<td>';
                echo '<a href="' . $a->caminho . '" target="_new" title="Download do anexo" data-role="tooltip" data-placement="bottom" > <span class="glyphicon download"></span>
                        </a>';
                echo '</td>';

                echo '<td>';
                echo '<a href="javascript:;" data-placement="bottom" data-role="tooltip" title="Deletar" onclick="Navegar(\'' . $this->Html->getUrl("Anexo", "post_delete", array("ajax" => true, "id" => $a->id, "modal" => "1")) . '\',\'go\')">
                                <span class="glyphicon delet"></span></a>';

                echo '</td>';

                echo '</tr>';
                $i++;
            }
            ?>
        </table>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('a#abrir-modal').click(function () {
            $('div.modal').find('div.modal-content').load($(this).attr('href'), {
                'id_externo': '<?php echo @$id_externo; ?>',
                'tipo': '<?php echo @$tipo; ?>'
            });
            return false;
        });

        $('a.btn-remove_anexo').click(function () {
            var pop = confirm('Deseja mesmo excluir este anexo?');
            if (pop) {
                $('div.modal').find('div.modal-content').load($(this).attr('href'), {
                    'id_externo': '<?php echo @$id_externo; ?>',
                    'tipo': '<?php echo @$tipo; ?>'
                });
            }
            return false;
        });
    });
</script>