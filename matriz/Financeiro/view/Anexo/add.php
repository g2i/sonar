<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexo</h2>
        <ol class="breadcrumb">
            <li>Anexo</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div id="msg_error"></div>
                    <p>Extensões permitidas: <i>zip,doc,docx,pdf,rar,png,jpg,jpeg,gif,odt,txt,xls,xlsx;</i></p>

                    <br/>

                    <div class="clearfix">
                        <form enctype="multipart/form-data" method="post" role="form" id="formAnexo"
                              action="<?php echo $this->Html->getUrl('Anexo', 'post_anexo'); ?>">
                            <table>
                                <tr class="tr_anexo">
                                    <td width="35"></td>
                                    <td width="10"> &nbsp; </td>
                                    <td><input type="text" class="form-control" name="add_anexo[0][titulo]"
                                               placeholder="Titulo do anexo"/></td>
                                    <td width="10"> &nbsp; </td>
                                    <td>
                                        <input type="file" name="add_anexo[0][arquivo]"/>
                                        <input type="hidden" name="add_anexo[0][tipo]"
                                               value="<?= $tipo; ?>"/>
                                        <input type="hidden" name="add_anexo[0][id_externo]"
                                               value="<?= $id_externo; ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5"> &nbsp; </td>
                                </tr>

                                <tbody id="content_arquivos"></tbody>
                            </table>

                            <div style="clear: both"></div>
                            <br/>

                            <div class="text-right">

                                <button type="button" id="adicionar_anexo" class="pull-left btn btn-primary">Adicionar</button>
                                <button type="button" id="save" class="pull-right btn btn-primary">Salvar</button>

                                <span class="pull-right" style="width: 10px;height: 1px;pull-right"></span>

                                <button type="button" id="cancelar" class="pull-right btn btn-default">Voltar</button>
                            </div>
                            <br/><br/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#save').click(function () {
            console.log('ttese');
            $('#formAnexo').submit();
        });

        $('#formAnexo').ajaxForm({
            success: function (d) {
                if (d != "true") {
                    $('#msg_error').html(d);
                } else {
                    Navegar('<?php echo $this->Html->getUrl('Anexo','all',array('id_externo'=>$_POST['id_externo'], 'tipo'=>$_POST['tipo'], 'ajax'=>'true')); ?>', 'back');
                    /*$('div.modal').find('div.modal-content').load('<?php //echo $this->Html->getUrl('Anexo','all',array('id_externo'=>$_POST['id_externo'], 'tipo'=>$_POST['tipo'], 'ajax'=>'true')); ?>');*/
                }
            }
        });

        $('#adicionar_anexo').click(function () {
            var i_next = parseInt($("tr.tr_anexo").length - 1) + 1;

            var string = ''
                + '<tr class="tr_anexo row_anexo_' + i_next + '">'
                + ' <td><span data-rm="row_anexo_' + i_next + '" class="btn btn-danger btn_remover_anexo">X</span></td>'
                + ' <td width="10"> &nbsp; </td>'
                + ' <td><input type="text" class="form-control" name="add_anexo[' + i_next + '][titulo]" placeholder="Titulo do anexo" /></td>'
                + ' <td width="10"> &nbsp; </td>'
                + ' <td>'
                + '     <input type="file" name="add_anexo[' + i_next + '][arquivo]" />'
                + '     <input type="hidden" name="add_anexo[' + i_next + '][tipo]" value="<?php echo $_POST['tipo']; ?>" />'
                + '     <input type="hidden" name="add_anexo[' + i_next + '][id_externo]" value="<?php echo $_POST['id_externo']; ?>" />'
                + ' </td>'
                + ' </tr> <tr class="row_anexo_' + i_next + '"><td colspan="5"> &nbsp; </td></tr>';

            $('tbody#content_arquivos').append(string);

            $('tbody#content_arquivos').find('span.btn_remover_anexo').bind('click', function () {
                $('.' + $(this).data('rm')).remove();
            })


            return false;
        });

        $('#cancelar').click(function () {
            $('div.modal').find('div.modal-content').load('<?php echo $this->Html->getUrl('Anexo','all',array('id_externo'=>$_POST['id_externo'], 'tipo'=>$_POST['tipo'], 'ajax'=>'true')); ?>');
        })

        $('div#msg_error').click(function () {
            $(this).html('');
        });
    });
</script>