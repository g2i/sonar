<style>
    .img-circle{
        width: 140px;
        height: 140px;
    }
</style>
<div class="lock-word animated fadeInDown">
    <span class="first-word">TELA DE</span><span>BLOQUEIO</span>
</div>
<div class="middle-box text-center lockscreen animated fadeInDown">
    <div>
        <div class="m-b-md">
            <img class="img-circle circle-border" src="<?php echo $image ?>" alt="<?php echo $nome ?>"/>
        </div>
        <h3><?php echo $nome ?></h3>
        <p>Você está na tela de bloqueo. O tempo de espera foi atingindo e você precisa digita sua senha para desbloquear o sistema.</p>
        <form class="m-t" role="form"
              action="<?php echo $this->Html->getUrl('Login', 'lock') ?>"
              method="POST" autocomplete="off">
            <input type="hidden" name="redirect" value="<?php echo $redirect ?>">
            <input type="hidden" name="login" value="<?php echo $login ?>">
            <div class="form-group">
                <input name="password" type="password" class="form-control" placeholder="******" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width">Desbloquear</button>
        </form>
    </div>
</div>