<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Plano de Contas : <?= $PlanoContas->nome ?></h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Rateio_projetos_planos_contas', 'add', array('id_plano'=>$this->getParam('id_plano')), array('class' => 'btn btn-primary')); ?></p>
                    </div>

                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        Id
                                    </th>
                                    <th>
                                        Grupo
                                    </th>
                                    <th>
                                        Empresa
                                    </th>
                                    <th>
                                        Unidade
                                    </th>
                                    <th>
                                        Percentual
                                    </th>
                                    <th>
                                        Situação
                                    </th>

                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Rateio_projetos_planos_contas as $r) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($r->id, 'Rateio_projetos_planos_contas', 'view',
                                        array('id' => $r->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($r->getFin_projeto_unidade()->getFin_projeto_empresa()->getFin_projeto_grupo()->nome, 'Fin_projeto_grupo', 'view',
                                        array('id' => $r->getFin_projeto_unidade()->getFin_projeto_empresa()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($r->getFin_projeto_unidade()->getFin_projeto_empresa()->nome, 'Fin_projeto_empresa', 'view',
                                        array('id' => $r->getFin_projeto_unidade()->getFin_projeto_empresa()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($r->getFin_projeto_unidade()->nome, 'Fin_projeto_unidade', 'view',
                                        array('id' => $r->getFin_projeto_unidade()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($r->percentual, 'Situacao', 'view',
                                        array('id' => $r->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td>';
                                    echo $this->Html->getLink($r->getSituacao()->nome, 'Situacao', 'view',
                                        array('id' => $r->getSituacao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';

                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Rateio_projetos_planos_contas', 'edit',
                                        array('id' => $r->id),
                                        array('class' => 'btn btn-warning btn-sm'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Rateio_projetos_planos_contas', 'delete',
                                        array('id' => $r->id),
                                        array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>

                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function () {
                            $('#search').keyup(function () {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Rateio_projetos_planos_contas', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Rateio_projetos_planos_contas', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function () {
                                            r = true;
                                        });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>