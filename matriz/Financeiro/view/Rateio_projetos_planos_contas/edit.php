<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Plano de Contas : <?= $PlanoContas->nome ?></h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="post" role="form"
              action="<?php echo $this->Html->getUrl('Rateio_projetos_planos_contas', 'edit') ?>">
            <div class="ibox ">
                <div class="ibox-title" style="border-color: #00a65a;">
                    <h2>Projeto</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="percentual">Percentual</label>
                            <input type="number" step="any" name="percentual" id="percentual" class="form-control"
                                   value="<?php echo $Rateio_projetos_planos_contas->percentual ?>"
                                   placeholder="Percentual">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" for="contabilidade">Grupo <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="fin_projeto_grupo_id" name="fin_projeto_grupo_id"
                                    class="form-control selectPicker"
                                    required>
                                <option>Selecione Um Grupo</option>
                                <?php
                                foreach ($Fin_projeto_grupo as $c) {
                                    if ($c->id == $Fin_projeto->grupo_id)
                                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                    else
                                        echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" for="contabilidade">Empresa <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="fin_projeto_empresa_id" name="fin_projeto_empresa_id"
                                    class="form-control selectPicker"
                                    required>
                                <?php
                                foreach ($Fin_projeto_empresa as $c) {
                                    if ($c->id == $Fin_projeto->empresa_id)
                                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                    else
                                        echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" for="contabilidade">Unidade<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="fin_projeto_unidade_id" name="fin_projeto_unidade_id"
                                    class="form-control selectPicker"
                                    required>
                                <?php
                                foreach ($Fin_projeto_unidade as $c) {
                                    if ($c->id == $Fin_projeto->unidade_id)
                                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                    else
                                        echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>
                <div class="ibox-footer">
                    <input type="hidden" name="id" value="<?php echo $Rateio_projetos_planos_contas->id; ?>">
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Rateio_projetos_planos_contas', 'all', array('id_plano' => $Rateio_projetos_planos_contas->id_plano_contas)) ?>"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>

<script>
    $(function () {
        $('#fin_projeto_grupo_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_empresa', 'getListaEmpresa') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_empresa_id').prop("disabled", true);
            $('#fin_projeto_unidade_id').prop("disabled", true);
            $.get(url, {fin_projeto_grupo_id: id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_empresa_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').html('').trigger('change.select2');
                $('#fin_projeto_empresa_id').prop("disabled", false);
                $('#fin_projeto_unidade_id').prop("disabled", false);
            }, 'json');
        });

        $('#fin_projeto_empresa_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_unidade', 'getListaUnidade') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_unidade_id').prop("disabled", true);
            $.get(url, {fin_projeto_empresa_id: id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_unidade_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').prop("disabled", false);
            }, 'json');
        });
    });
</script>