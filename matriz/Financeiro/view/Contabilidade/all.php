<style>
    .col-opcoes{
        min-width: 90px; !important;
        max-width: 90px; !important;
        width: 90px; !important;
    }
    tr > td > button {
        margin-left: 2px;
    }
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Empresa</h2>
        <ol class="breadcrumb">
            <li>empresa</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="tblEmpresas" class="table table-hover">
                        <thead>
                        <tr>
                            <th data-column-id="id" data-identifier="true" data-visible="false"
                                data-visible-in-selection="false">Id</th>
                            <th data-column-id="nome">Nome</th>
                            <th data-column-id="descricao">Descri&ccedil;&atilde;o</th>
                            <th data-column-id="cidade">Cidade</th>
                            <th data-column-id="ud">UF</th>
                            <th data-column-id="status">Status</th>
                            <th data-column-id="opcoes" data-formatter="opcoes"
                                data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                data-sortable="false"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>

        function mostrarGrupos(empresa){
            var url = root + '/Contabilidade/grupos?ajax=True';
            $('<div></div>').load(url, function(){
                BootstrapDialog.show({
                    title: 'Grupos',
                    message: this,
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function(dialog){
                        var grid = dialog.getModalBody().find('#tblGrupos');
                        grid.bootgrid({
                            rowSelect: true,
                            multiSort: false,
                            rowCount: [10, 25, 50],
                            ajax: true,
                            url: root + '/Contabilidade/getGrupos',
                            ajaxSettings: {
                                method: "GET",
                                cache: true
                            },
                            formatters:{
                                opcoes: function(column, row){
                                    return '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                                }
                            },
                            labels: {
                                search: 'Procura por nome'
                            },
                            templates:{
                                search: '<div class="{{css.search}} pull-left" style="width: 250px"><div class="input-group"><span class="{{css.icon}} input-group-addon {{css.iconSearch}}"></span><input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div></div>',
                            },
                            requestHandler: function (request) {
                                request.cont = empresa;
                                if(request.sort){
                                    var sort = [];
                                    $.each(request.sort, function(key, value){
                                        sort.push([key, value]);
                                    });

                                    delete request.sort;
                                    request.sort = $.toJSON(sort);
                                }

                                return request;
                            }
                        });

                        grid.on("loaded.rs.jquery.bootgrid", function() {
                            grid.find('[data-toggle="tooltip"]').tooltip();

                            grid.find(".command-delete").unbind('click');
                            grid.find(".command-delete").on("click", function(e) {
                                var conta = $(this).data("row-id");
                                BootstrapDialog.confirm({
                                    title: 'Aviso',
                                    message: 'Voc\u00ea tem certeza ?',
                                    type: BootstrapDialog.TYPE_WARNING,
                                    closable: true,
                                    draggable: true,
                                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                                    btnOKLabel: 'Sim desejo excluir!',
                                    btnOKClass: 'btn-warning',
                                    callback: function(result) {
                                        if(result) {
                                            var url = root + '/Contabilidade/deleteGrupo';
                                            var data = {
                                                id: conta
                                            }

                                            $.post(url, data, function(ret){
                                                if(ret.result){
                                                    BootstrapDialog.success(ret.msg);
                                                    grid.bootgrid('reload');
                                                }else{
                                                    BootstrapDialog.warning(ret.msg);
                                                }
                                            });
                                        }
                                    }
                                });
                            });
                        });
                    },
                    buttons: [{
                        label: 'Adicionar',
                        icon: 'glyphicon glyphicon-plus-sign',
                        cssClass: 'btn-primary',
                        action: function (dialog) {
                            var grid = dialog.getModalBody().find('#tblGrupos');
                            BootstrapDialog.show({
                                title: 'Adicionar Contabilidades',
                                message: '<form class="form"><div class="form-group">' +
                                '<div class="input-group select2-bootstrap-append">' +
                                '<select id="grupos" class="form-control">' +
                                '<option></option></select><span class="input-group-btn">' +
                                '<button class="btn btn-default" type="button" data-select2-clear="grupos"' +
                                'data-toggle="tooltip" data-placement="top" title="Limpar" >' +
                                '<i class="fa fa-times"></i></button>' +
                                '</span></div></div></form>',
                                type: BootstrapDialog.TYPE_DEFAULT,
                                onshown: function(dialog){
                                    var grupos = dialog.getModalBody().find('#grupos');
                                    var url = root + '/Contabilidade/getContGrupos';
                                    var data = { id: empresa  }

                                    $.get(url, data, function(ret){
                                        if(ret.result) {
                                            ret.dados.forEach(function(dado){
                                                var opt = $('<option value="'+dado.id+'">'+dado.nome+'</option>');
                                                $(grupos).append(opt);
                                            });
                                        }

                                        grupos.select2({
                                            language: 'pt-BR',
                                            theme: 'bootstrap',
                                            width: '100%',
                                            minimumResultsForSearch: 6,
                                            placeholder: 'Selecione:'
                                        });

                                        dialog.getModalBody().find('[data-toggle="tooltip"]').tooltip();
                                        var buttons = dialog.getModalBody().find('button[data-select2-clear]');

                                        $(buttons).click(function() {
                                            $("#" + $(this).data("select2-clear")).val('').trigger('change');
                                        });

                                        var modal = dialog.getModal();
                                        $(modal).removeAttr('tabindex');
                                    });
                                },
                                buttons: [{
                                    label: 'Cancelar',
                                    icon: 'fa fa-times',
                                    cssClass: 'btn-danger',
                                    action: function (dialog) {
                                        dialog.close();
                                    }
                                },{
                                    label: 'Adicionar',
                                    icon: 'glyphicon glyphicon-plus-sign',
                                    cssClass: 'btn-primary',
                                    action: function (dialog) {
                                        var grupo = dialog.getModalBody().find('#grupos');
                                        var value = $(grupo).val();
                                        if(!value){
                                            BootstrapDialog.alert('Selecione um grupo !');
                                            return;
                                        }

                                        var url = root + '/Contabilidade/addGrupo';
                                        var data = {
                                            grupo: value,
                                            contabilidade: empresa
                                        }

                                        $.post(url, data, function(ret){
                                            if(ret.result){
                                                grid.bootgrid('reload');
                                                BootstrapDialog.success(ret.msg);
                                                dialog.close();
                                            }else{
                                                BootstrapDialog.warning(ret.msg);
                                            }
                                        });
                                    }
                                }]
                            });
                        }
                    },{
                        label: 'Fechar',
                        icon: 'fa fa-times',
                        cssClass: 'btn-danger',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            });
        }

        $(document).ready(function () {
            var grid = $('#tblEmpresas').bootgrid({
                rowSelect: true,
                multiSort: true,
                rowCount: [30, 50, 100],
                ajax: true,
                url: root + '/Contabilidade/getEmpresas',
                ajaxSettings: {
                    method: "GET",
                    cache: true
                },
                formatters:{
                    opcoes: function(column, row){
                        return  '<button type="button" data-toggle="tooltip" data-placement="top" title="Grupos" class="btn btn-xs btn-primary command-grupos" data-row-id="' + row.id + '"><span class="fa fa-list"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                    }
                },
                labels: {
                    search: 'Procura por nome'
                },
                templates:{
                    search: '<div class="{{css.search}} pull-left" style="width: 250px"><div class="input-group"><span class="{{css.icon}} input-group-addon {{css.iconSearch}}"></span><input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div></div>' +
                            '<button id="btnNovo" class="{{css.search}} btn btn-primary" style="width: auto" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Cadastrar Empresa</button>',
                },
                requestHandler: function (request) {
                    if(request.sort){
                        var sort = [];
                        $.each(request.sort, function(key, value){
                            sort.push([key, value]);
                        });

                        delete request.sort;
                        request.sort = $.toJSON(sort);
                    }

                    return request;
                }
            });

            grid.on("loaded.rs.jquery.bootgrid", function() {
                grid.find('[data-toggle="tooltip"]').tooltip();

                grid.find(".command-edit").unbind('click');
                grid.find(".command-edit").on("click", function (e) {
                    var conta = $(this).data("row-id");
                    var url = root + '/Contabilidade/edit?id=' + conta;
                    document.location = url;
                });

                grid.find(".command-grupos").unbind('click');
                grid.find(".command-grupos").on("click", function (e) {
                    var empresa = $(this).data("row-id");
                    mostrarGrupos(empresa);
                });

                grid.find(".command-delete").unbind('click');
                grid.find(".command-delete").on("click", function(e) {
                    var conta = $(this).data("row-id");
                    BootstrapDialog.confirm({
                        title: 'Aviso',
                        message: 'Voc\u00ea tem certeza ?',
                        type: BootstrapDialog.TYPE_WARNING,
                        closable: false,
                        draggable: false,
                        btnCancelLabel: 'N\u00e3o desejo excluir!',
                        btnOKLabel: 'Sim desejo excluir!',
                        btnOKClass: 'btn-warning',
                        callback: function(result) {
                            if(result) {
                                var url = root + '/Contabilidade/delete';
                                var data = {
                                    id: conta
                                }

                                $.post(url, data, function(ret){
                                    if(ret.result){
                                        BootstrapDialog.success(ret.msg);
                                        grid.bootgrid('reload');
                                    }else{
                                        BootstrapDialog.warning(ret.msg);
                                    }
                                });
                            }
                        }
                    });
                });

                $('#btnNovo').unbind('click');
                $('#btnNovo').click(function(e){
                    var url = root + '/Contabilidade/add';
                    document.location = url;
                });
            });
        });
    </script>