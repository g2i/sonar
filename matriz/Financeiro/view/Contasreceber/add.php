<style type="text/css">
    .nav-tabs > li > a {
        border: 1px solid #ddd;
    }
    .nav-tabs {
        border-bottom: none;
    }
</style>
<?php if($this->getParam('modal')): ?>
    <style>
        .select2-close-mask{
            z-index: 2099;
        }
        .select2-dropdown{
            z-index: 3051;
        }
        .has-error .select2-selection {
            border: 1px solid #a94442;
            border-radius: 4px;
        }
    </style>
<?php else: ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas Receber</h2>
        <ol class="breadcrumb">
            <li>Receber</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php endif; ?>
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>

                    <form id="frmConta" method="post" role="form" action="<?php echo $this->Html->getUrl('Contasreceber', 'add') ?>"
                          id='formContasreceber'>
                        <input type="hidden" name="formulario" value="individual">
                        <input type="hidden" name="deducao" id="deducao">

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="data">Data do Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <div class='input-group date'>
                                    <input type='text' class="form-control dateFormat" name="data" id="data" required>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="vencimento">Vencimento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <div class='input-group date'>
                                    <input type='text' class="form-control dateFormat" name="vencimento" id="vencimento" required>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="idCliente">Cliente <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="idCliente" name="idCliente" class="form-control clientes-ajax" required></select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="idPlanoContas">Plano de Contas <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas" required>
                                    <option></option>
                                    <?php
                                    foreach ($Planocontas as $p) {
                                        echo '<option value="' . $p->id . '">' . $p->nome . '</option>';                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="tipoDocumento">Tipo de Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento" required>
                                    <option></option>
                                    <?php
                                    foreach ($tipoDocumento as $t) {
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="tipoPagamento" class="required">Forma de Pagamento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento" required>
                                    <option></option>
                                    <?php
                                    foreach ($tipoPagamento as $t) {
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="numfiscal">N&uacute;mero do Documento Fiscal</label>
                                <input type="text" name="numfiscal" id="numfiscal" class="form-control"
                                       placeholder="N&uacute;mero Fiscal">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="datafiscal">Data do Documento Fiscal</label>
                                <div class='input-group date'>
                                    <input type='text' class="form-control dateFormat" name="datafiscal" id="datafiscal">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="valorBruto">Valor Bruto <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <div class='input-group'>
                                    <span class="input-group-addon">R$</span>
                                    <input type="text" onblur="calculaValor()" name="valorBruto" id="valorBruto"
                                           class="form-control money2" value="0.00"
                                           placeholder="Valor Bruto" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="juros">Juros</label>
                                <div class='input-group'>
                                    <span class="input-group-addon">R$</span>
                                    <input type="text" onblur="calculaValor()" name="juros" id="juros"
                                           class="form-control money2" value="0.00"
                                           placeholder="Juros">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="multa">Multa</label>
                                <div class='input-group'>
                                    <span class="input-group-addon">R$</span>
                                    <input type="text" onblur="calculaValor()" name="multa" id="multa"
                                           class="form-control money2" value="0.00"
                                           placeholder="Juros">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="desconto">Desconto</label>
                                <div class='input-group'>
                                    <span class="input-group-addon">R$</span>
                                    <input type="text" onblur="calculaValor()" name="desconto" id="desconto"
                                           class="form-control money2" value="0.00"
                                           placeholder="Desconto">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="valor">Valor L&iacute;quido</label>
                                <div class='input-group'>
                                    <span class="input-group-addon">R$</span>
                                    <input readonly="readonly" type="text" name="valor" id="valor" class="form-control"
                                           value="0.00" placeholder="Valor">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="correcaoMonetaria">Atualiza&ccedil;&atilde;o Monet&aacute;ria
                                    <span class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="correcaoMonetaria" name="correcaoMonetaria" class="form-control selectPicker" required>
                                    <?php
                                    foreach ($correcaoMonetaria as $c) {
                                        echo "<option value='" . $c->id . "'>" . $c->nome . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="dataPagamento">Data de Pagamento</label>
                                <div class='input-group date'>
                                    <input type='text' class="form-control dateFormat" name="dataPagamento" id="dataPagamento">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label class="required" for="contabilidade">Empresas <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                                <select name="contabilidade" class="form-control selectPicker" id="contabilidade" required>
                                    <option></option>
                                    <?php
                                    foreach ($Contabilidades as $c) {
                                        echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group" style="height:120px;">
                                <label for="status">Status</label>
                                <select name="status" class="form-control selectPicker" id="status">
                                    <?php
                                    foreach ($Status as $s) {
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                    }
                                    ?>
                                </select>

                                <div class="tooltip bottom fade in" role="tooltip" style="z-index:1; display:block;">
                                    <div class="tooltip-arrow"></div>
                                    <div class="tooltip-inner" style="max-width:300px;text-align: left;">
                                        <span style='display:block;width:250px;font-size:10px;'> Ativo =  Em cobrança/ Quitada <br/> Inativo = Arquivada/Cobrança suspensa</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="complemento">Observa&ccedil;&atilde;o</label>
                                <textarea name="complemento" id="complemento"
                                          class="form-control"></textarea>
                            </div>
                        </div>
                        <div style="clear:both;"></div>

                        <input type="hidden" name="rateio_automatico" id="rateio_automatico"/>

                        <?php if(!$this->getParam('modal')): ?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Contasreceber', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({
            html: true,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="max-width:300px;text-align: left;"></div></div>'
        });
    });

    function calculaValor() {
        if ($('#multa').val() != "undefined" && $('#multa').val() != "") {
            var multa = $('#multa').unmask();
        } else {
            var multa = 0;
        }

        if ($('#desconto').val() != "undefined" && $('#desconto').val() != "") {
            var desconto = $('#desconto').unmask();
        } else {
            var desconto = 0;
        }

        if ($('#valorBruto').val() != "undefined" && $('#valorBruto').val() != "") {
            var valorBruto = $('#valorBruto').unmask();
        } else {
            var valorBruto = 0;
        }

        if ($('#juros').val() != "undefined" && $('#juros').val() != "") {
            var juros = $('#juros').unmask();
        } else {
            var juros = 0;
        }

        var aux = (valorBruto + juros + multa) - desconto;
        aux = Math.round(aux*100)/100;

        var valorDeducao = aux * 0;
        valorDeducao = aux - valorDeducao;
        valorDeducao = Math.round(valorDeducao*100)/100;

        $('#valorDeduzido').unpriceFormat();
        $('#valorDeduzido').text(valorDeducao.toFixed(2));
        $('#valorDeduzido').priceFormat({
            prefix: 'R$ ',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });

        $('#valor').unpriceFormat();
        $('#valor').val(aux.toFixed(2));
        $('#valor').priceFormat({
            prefix: 'R$ ',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
    }
</script>