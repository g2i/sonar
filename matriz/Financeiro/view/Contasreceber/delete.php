<form class="form" method="post" action="<?php echo $this->Html->getUrl('Contasreceber', 'delete') ?>">
    <h1>Confirmação</h1>

    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o Contasreceber
            <strong><?php echo $Contasreceber->id; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Contasreceber->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Contasreceber', 'all') ?>" class="btn btn-default"
           data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>