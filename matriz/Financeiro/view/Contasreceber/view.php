<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas a Receber</h2>
        <ol class="breadcrumb">
            <li>Receber</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Data</strong>: <?php echo $Contasreceber->data; ?></p>

                <p><strong>Valor</strong>: <?php echo $Contasreceber->valor; ?></p>

                <p><strong>IdCliente</strong>: <?php echo $Contasreceber->idCliente; ?></p>

                <p><strong>Complemento</strong>: <?php echo $Contasreceber->complemento; ?></p>

                <p><strong>DataPagamento</strong>: <?php echo $Contasreceber->dataPagamento; ?></p>

                <p><strong>ValorPago</strong>: <?php echo $Contasreceber->valorPago; ?></p>

                <p><strong>Situacao</strong>: <?php echo $Contasreceber->situacao; ?></p>

                <p>
                    <strong>Planocontas</strong>:
                    <?php
                    echo $this->Html->getLink($Contasreceber->getPlanocontas()->nome, 'Planocontas', 'view',
                        array('id' => $Contasreceber->getPlanocontas()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Contasreceber->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Contasreceber->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Contabilidade</strong>:
                    <?php
                    echo $this->Html->getLink($Contasreceber->getContabilidade()->nome, 'Contabilidade', 'view',
                        array('id' => $Contasreceber->getContabilidade()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>