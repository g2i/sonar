<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Deduções</h2>
        <ol class="breadcrumb">
            <li>deduções</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Deducoes', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                        <div class="form-group">
                            <label for="nome">Nome</label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Deducoes->nome ?>" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control selectPicker" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Deducoes->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Deducoes', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>