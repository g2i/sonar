<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Plano/Centro de Custo</h2>
        <ol class="breadcrumb">
            <li> Plano/Centro de Custo</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Porcentagem</strong>: <?php echo $Plano_centrocusto->porcentagem; ?></p>

                <p>
                    <strong>Planocontas</strong>:
                    <?php
                    echo $this->Html->getLink($Plano_centrocusto->getPlanocontas()->nome, 'Planocontas', 'view',
                        array('id' => $Plano_centrocusto->getPlanocontas()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Centro_custo</strong>:
                    <?php
                    echo $this->Html->getLink($Plano_centrocusto->getCentro_custo()->descricao, 'Centro_custo', 'view',
                        array('id' => $Plano_centrocusto->getCentro_custo()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Contabilidade</strong>:
                    <?php
                    echo $this->Html->getLink($Plano_centrocusto->getContabilidade()->nome, 'Contabilidade', 'view',
                        array('id' => $Plano_centrocusto->getContabilidade()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Plano_centrocusto->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Plano_centrocusto->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>