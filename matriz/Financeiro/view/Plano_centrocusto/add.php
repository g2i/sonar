<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Plano_centrocusto', 'add') ?>">
    <div class="alert alert-info">Os campos marcados com <span
            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
    </div>
    <div class="form-group">
        <label for="plano_contas" class="required">Plano de contas <span
                class="glyphicon glyphicon-asterisk"></span></label>
        <select name="plano_contas" class="form-control selectPicker" id="plano_contas" required>
            <option value=""></option>
            <?php
            foreach ($Planocontas as $p) {
                if ($p->id == $this->getParam('plano'))
                    echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                else
                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group col-sm-6" style="padding-left: 0px;">
        <label for="centro_custo" class="required">Centro de custo <span
                class="glyphicon glyphicon-asterisk"></span></label>
        <select name="centro_custo" class="form-control selectPicker" id="centro_custo" required
                role="centro_custo">
            <option value=""></option>
            <?php
            foreach ($Centro_custos as $c) {
                if ($c->id == $Plano_centrocusto->centro_custo)
                    echo '<option selected value="' . $c->id . '">' . $c->descricao . '</option>';
                else
                    echo '<option value="' . $c->id . '">' . $c->descricao . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group col-sm-6">
        <label for="centro_custo">Adicionar Centro de custo</label>
        <span>
        <?php if($this->getParam('modal')) { ?>
            <?php echo '<a href="javascript:;" data-placement="bottom" class="btn btn-info" data-toggle="tooltip2" title="Adicionar Centro de Custo"
                       onclick="Navegar(\''.$this->Html->getUrl("Centro_custo","add", array("ajax" => true, "modal"=>"1")).'\',\'go\')">'; ?>
                    <span class="glyphicon centro_add"></span> Centro Custo</a>
            <?php } else {
            echo $this->Html->getLink('<span  class="glyphicon centro_add" data-toggle="tooltip2" data-placement="bottom" title="Adicionar Centro de Custo"></span>', 'Centro_custo', 'add',
                array("ajax" => true, "modal"=>"1","first"=>1),
                array('data-toggle' => 'modal',"class"=>"btn btn-info"));
        } ?>
             </span>
    </div>
    <div class="clearfix"></div>
    <div class="form-group">
        <label for="empresa" class="required">Empresa <span
                class="glyphicon glyphicon-asterisk"></span></label>
        <select name="empresa" class="form-control selectPicker" id="empresa" required>
            <option value=""></option>
            <?php
            foreach ($Contabilidades as $c) {
                if ($c->id == $Plano_centrocusto->empresa)
                    echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                else
                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label class="required" for="porcentagem">Porcentagem <span
                class="glyphicon glyphicon-asterisk"></span></label>
        <div class='input-group'>
            <input type="text" value="<?php echo $Plano_centrocusto->porcentagem ?>"
                   class="form-control text-right percent" name="porcentagem" id="porcentagem" placeholder="Porcentagem"/>
            <span class="input-group-addon">%</span>
        </div>
    </div>
    <input type="hidden" name="status" value="1"/>
    <div class="row">
        <input type="hidden" name="modal" value="1"/>
        <div class="text-right">
            <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
               data-toggle="tooltip2" title="Voltar"
               onclick="Navegar('','back')">
                Cancelar</a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                   value="salvar">
        </div>
    </div>
</form>