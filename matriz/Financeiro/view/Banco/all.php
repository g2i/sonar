<style>
    .col-opcoes{
        min-width: 90px; !important;
        max-width: 90px; !important;
        width: 90px; !important;
    }
    tr > td > button {
        margin-left: 2px;
    }
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Bancos</h2>
        <ol class="breadcrumb">
            <li>Bancos</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="tblBancos" class="table table-hover">
                        <thead>
                        <tr>
                            <th data-column-id="id" data-identifier="true" data-visible="false"
                                data-visible-in-selection="false">Id</th>
                            <th data-column-id="nome">Nome</th>
                            <th data-column-id="agencia">Ag&ecirc;ncia</th>
                            <th data-column-id="conta">Conta</th>
                            <th data-column-id="saldo">Saldo</th>
                            <th data-column-id="limite">Limite</th>
                            <th data-column-id="fluxo">Mostrar Fluxo</th>
                            <th data-column-id="geral">Mostra Saldo Geral</th>
                            <th data-column-id="opcoes" data-formatter="opcoes"
                                data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                data-sortable="false"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function mostrarContabilidade(banco){
        var url = root + '/Banco/contabilidades?ajax=True&id=' + banco;
        $('<div></div>').load(url, function(){
            BootstrapDialog.show({
                title: 'Contabilidades',
                message: this,
                type: BootstrapDialog.TYPE_DEFAULT,
                onshown: function(dialog){
                    var grid = dialog.getModalBody().find('#tblContabilidades');
                    grid.bootgrid({
                        rowSelect: true,
                        multiSort: false,
                        rowCount: [10, 25, 50],
                        ajax: true,
                        url: root + '/Banco/getContabilidades',
                        ajaxSettings: {
                            method: "GET",
                            cache: true
                        },
                        formatters:{
                            opcoes: function(column, row){
                                return '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                            }
                        },
                        labels: {
                            search: 'Procura por nome'
                        },
                        templates:{
                            search: '<div class="{{css.search}} pull-left" style="width: 250px"><div class="input-group"><span class="{{css.icon}} input-group-addon {{css.iconSearch}}"></span><input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div></div>',
                        },
                        requestHandler: function (request) {
                            request.banco = banco;
                            if(request.sort){
                                var sort = [];
                                $.each(request.sort, function(key, value){
                                    sort.push([key, value]);
                                });

                                delete request.sort;
                                request.sort = $.toJSON(sort);
                            }

                            return request;
                        }
                    });

                    grid.on("loaded.rs.jquery.bootgrid", function() {
                        grid.find('[data-toggle="tooltip"]').tooltip();

                        grid.find(".command-delete").unbind('click');
                        grid.find(".command-delete").on("click", function(e) {
                            var conta = $(this).data("row-id");
                            BootstrapDialog.confirm({
                                title: 'Aviso',
                                message: 'Voc\u00ea tem certeza ?',
                                type: BootstrapDialog.TYPE_WARNING,
                                closable: true,
                                draggable: true,
                                btnCancelLabel: 'N\u00e3o desejo excluir!',
                                btnOKLabel: 'Sim desejo excluir!',
                                btnOKClass: 'btn-warning',
                                callback: function(result) {
                                    if(result) {
                                        var url = root + '/Banco/deleteContabilidade';
                                        var data = {
                                            id: conta
                                        }

                                        $.post(url, data, function(ret){
                                            if(ret.result){
                                                BootstrapDialog.success(ret.msg);
                                                grid.bootgrid('reload');
                                            }else{
                                                BootstrapDialog.warning(ret.msg);
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    });
                },
                buttons: [{
                    label: 'Adicionar',
                    icon: 'glyphicon glyphicon-plus-sign',
                    cssClass: 'btn-primary',
                    action: function (dialog) {
                        var grid = dialog.getModalBody().find('#tblContabilidades');
                        BootstrapDialog.show({
                            title: 'Adicionar Contabilidades',
                            message: '<form class="form"><div class="form-group">' +
                            '<div class="input-group select2-bootstrap-append">' +
                            '<select id="contabilidade" class="form-control">' +
                            '<option></option></select><span class="input-group-btn">' +
                            '<button class="btn btn-default" type="button" data-select2-clear="contabilidade"' +
                            'data-toggle="tooltip" data-placement="top" title="Limpar" >' +
                            '<i class="fa fa-times"></i></button>' +
                            '</span></div></div></form>',
                            type: BootstrapDialog.TYPE_DEFAULT,
                            onshown: function(dialog){
                                var contabilidade = dialog.getModalBody().find('#contabilidade');
                                var url = root + '/Banco/getContBanco';
                                var data = { id: banco  }

                                $.get(url, data, function(ret){
                                    if(ret.result) {
                                        ret.dados.forEach(function(dado){
                                            var opt = $('<option value="'+dado.id+'">'+dado.nome+'</option>');
                                            $(contabilidade).append(opt);
                                        });
                                    }

                                    contabilidade.select2({
                                        language: 'pt-BR',
                                        theme: 'bootstrap',
                                        width: '100%',
                                        minimumResultsForSearch: 6,
                                        placeholder: 'Selecione:'
                                    });

                                    dialog.getModalBody().find('[data-toggle="tooltip"]').tooltip();
                                    var buttons = dialog.getModalBody().find('button[data-select2-clear]');

                                    $(buttons).click(function() {
                                        $("#" + $(this).data("select2-clear")).val('').trigger('change');
                                    });

                                    var modal = dialog.getModal();
                                    $(modal).removeAttr('tabindex');
                                });
                            },
                            buttons: [{
                                label: 'Cancelar',
                                icon: 'fa fa-times',
                                cssClass: 'btn-danger',
                                action: function (dialog) {
                                    dialog.close();
                                }
                            },{
                                label: 'Adicionar',
                                icon: 'glyphicon glyphicon-plus-sign',
                                cssClass: 'btn-primary',
                                action: function (dialog) {
                                    var contabilidade = dialog.getModalBody().find('#contabilidade');
                                    var value = $(contabilidade).val();
                                    if(!value){
                                        BootstrapDialog.alert('Selecione uma contabilidade !');
                                        return;
                                    }

                                    var url = root + '/Banco/addContabilidade';
                                    var data = {
                                        banco: banco,
                                        contabilidade: value
                                    }

                                    $.post(url, data, function(ret){
                                        if(ret.result){
                                            grid.bootgrid('reload');
                                            BootstrapDialog.success(ret.msg);
                                            dialog.close();
                                        }else{
                                            BootstrapDialog.warning(ret.msg);
                                        }
                                    });
                                }
                            }]
                        });
                    }
                },{
                    label: 'Fechar',
                    icon: 'fa fa-times',
                    cssClass: 'btn-danger',
                    action: function (dialog) {
                        dialog.close();
                    }
                }]
            });
        });
    }

    $(document).ready(function () {
        var grid = $('#tblBancos').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: [30, 50, 100],
            ajax: true,
            url: root + '/Banco/getBancos',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            formatters:{
                opcoes: function(column, row){
                    return  '<button type="button" data-toggle="tooltip" data-placement="top" title="Contabilidades" class="btn btn-xs btn-primary command-contabilidade" data-row-id="' + row.id + '"><span class="fa fa-list"></span></button>' +
                        '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                        '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                }
            },
            labels: {
                search: 'Procura por nome'
            },
            templates:{
                search: '<div class="{{css.search}} pull-left" style="width: 250px"><div class="input-group"><span class="{{css.icon}} input-group-addon {{css.iconSearch}}"></span><input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div></div>' +
                '<button id="btnNovo" class="{{css.search}} btn btn-primary" style="width: auto" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Cadastrar Banco</button>',
            },
            requestHandler: function (request) {
                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                return request;
            }
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {
            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Banco/edit?id=' + conta;
                document.location = url;
            });

            grid.find(".command-contabilidade").unbind('click');
            grid.find(".command-contabilidade").on("click", function (e) {
                var banco = $(this).data("row-id");
                mostrarContabilidade(banco);
            });

            grid.find(".command-delete").unbind('click');
            grid.find(".command-delete").on("click", function(e) {
                var conta = $(this).data("row-id");
                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function(result) {
                        if(result) {
                            var url = root + '/Banco/delete';
                            var data = {
                                id: conta
                            }

                            $.post(url, data, function(ret){
                                if(ret.result){
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                }else{
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });

            $('#btnNovo').unbind('click');
            $('#btnNovo').click(function(e){
                var url = root + '/Banco/add';
                document.location = url;
            });
        });
    });
</script>