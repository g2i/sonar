<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Banco</h2>
        <ol class="breadcrumb">
            <li>Banco</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">

                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Banco', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="nome">Nome *</label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Banco->nome ?>" placeholder="Nome" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="agencia">Agencia *</label>
                            <input type="text" name="agencia" id="agencia" class="form-control"
                                   value="<?php echo $Banco->agencia ?>" placeholder="Agencia" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="conta">Conta *</label>
                            <input type="text" name="conta" id="conta" class="form-control"
                                   value="<?php echo $Banco->conta ?>" placeholder="Conta" required>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="limite">Limite</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="limite" id="limite" class="form-control money2"
                                       value="<?php echo number_format($Banco->limite, 2, ',', '.') ?>"
                                       placeholder="Limite">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="mostra_fluxo">Mostrar Fluxo?</label>
                            <select name="mostra_fluxo" id="mostra_fluxo" class="form-control">
                                <option value="1" <?php echo ($Banco->mostra_fluxo==1)? "selected" : ""; ?>>Sim</option>
                                <option value="0" <?php echo ($Banco->mostra_fluxo==0)? "selected" : ""; ?>>Não</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="mostra_saldo_geral">Mostrar Saldo Geral?</label>
                            <select name="mostra_saldo_geral" id="mostra_saldo_geral" class="form-control">
                                <option value="1" <?php echo ($Banco->mostra_saldo_geral==1)? "selected" : ""; ?>>Sim</option>
                                <option value="0" <?php echo ($Banco->mostra_saldo_geral==0)? "selected" : ""; ?>>Não</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" id="id" name="id" value="<?php echo $Banco->id; ?>">

                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <button type="button" id="btnContabilidades" class="btn btn-info"> Empresas</button>
                        <a href="<?php echo $this->Html->getUrl('Banco', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#btnContabilidades').click(function(){
            banco = $('#id').val();
            var url = root + '/Banco/contabilidades?ajax=True&id=' + banco;
            $('<div></div>').load(url, function(){
                BootstrapDialog.show({
                    title: 'Contabilidades',
                    message: this,
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function(dialog){
                        var grid = dialog.getModalBody().find('#tblContabilidades');
                        grid.bootgrid({
                            rowSelect: true,
                            multiSort: false,
                            rowCount: [10, 25, 50],
                            ajax: true,
                            url: root + '/Banco/getContabilidades',
                            ajaxSettings: {
                                method: "GET",
                                cache: true
                            },
                            formatters:{
                                opcoes: function(column, row){
                                    return '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                                }
                            },
                            labels: {
                                search: 'Procura por nome'
                            },
                            templates:{
                                search: '<div class="{{css.search}} pull-left" style="width: auto"><div class="input-group"><span class="{{css.icon}} input-group-addon {{css.iconSearch}}"></span><input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div></div>',
                            },
                            requestHandler: function (request) {
                                request.banco = banco;
                                if(request.sort){
                                    var sort = [];
                                    $.each(request.sort, function(key, value){
                                        sort.push([key, value]);
                                    });

                                    delete request.sort;
                                    request.sort = $.toJSON(sort);
                                }

                                return request;
                            }
                        });

                        grid.on("loaded.rs.jquery.bootgrid", function() {
                            grid.find('[data-toggle="tooltip"]').tooltip();

                            grid.find(".command-delete").unbind('click');
                            grid.find(".command-delete").on("click", function(e) {
                                var conta = $(this).data("row-id");
                                BootstrapDialog.confirm({
                                    title: 'Aviso',
                                    message: 'Voc\u00ea tem certeza ?',
                                    type: BootstrapDialog.TYPE_WARNING,
                                    closable: true,
                                    draggable: true,
                                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                                    btnOKLabel: 'Sim desejo excluir!',
                                    btnOKClass: 'btn-warning',
                                    callback: function(result) {
                                        if(result) {
                                            var url = root + '/Banco/deleteContabilidade';
                                            var data = {
                                                id: conta
                                            }

                                            $.post(url, data, function(ret){
                                                if(ret.result){
                                                    BootstrapDialog.success(ret.msg);
                                                    grid.bootgrid('reload');
                                                }else{
                                                    BootstrapDialog.warning(ret.msg);
                                                }
                                            });
                                        }
                                    }
                                });
                            });
                        });
                    },
                    buttons: [{
                        label: 'Adicionar',
                        icon: 'glyphicon glyphicon-plus-sign',
                        cssClass: 'btn-primary',
                        action: function (dialog) {
                            var grid = dialog.getModalBody().find('#tblContabilidades');
                            BootstrapDialog.show({
                                title: 'Adicionar Contabilidades',
                                message: '<form class="form"><div class="form-group">' +
                                '<div class="input-group select2-bootstrap-append">' +
                                '<select id="contabilidade" class="form-control">' +
                                '<option></option></select><span class="input-group-btn">' +
                                '<button class="btn btn-default" type="button" data-select2-clear="contabilidade"' +
                                'data-toggle="tooltip" data-placement="top" title="Limpar" >' +
                                '<i class="fa fa-times"></i></button>' +
                                '</span></div></div></form>',
                                type: BootstrapDialog.TYPE_DEFAULT,
                                onshown: function(dialog){
                                    var contabilidade = dialog.getModalBody().find('#contabilidade');
                                    var url = root + '/Banco/getContBanco';
                                    var data = { id: banco  }

                                    $.get(url, data, function(ret){
                                        console.log(ret);
                                        if(ret.result) {
                                            ret.dados.forEach(function(dado){
                                                var opt = $('<option value="'+dado.id+'">'+dado.nome+'</option>');
                                                $(contabilidade).append(opt);
                                            });
                                        }

                                        contabilidade.select2({
                                            language: 'pt-BR',
                                            theme: 'bootstrap',
                                            width: '100%',
                                            minimumResultsForSearch: 6,
                                            placeholder: 'Selecione:'
                                        });

                                        dialog.getModalBody().find('[data-toggle="tooltip"]').tooltip();
                                        var buttons = dialog.getModalBody().find('button[data-select2-clear]');

                                        $(buttons).click(function() {
                                            $("#" + $(this).data("select2-clear")).val('').trigger('change');
                                        });

                                        var modal = dialog.getModal();
                                        $(modal).removeAttr('tabindex');
                                    });
                                },
                                buttons: [{
                                    label: 'Cancelar',
                                    icon: 'fa fa-times',
                                    cssClass: 'btn-danger',
                                    action: function (dialog) {
                                        dialog.close();
                                    }
                                },{
                                    label: 'Adicionar',
                                    icon: 'glyphicon glyphicon-plus-sign',
                                    cssClass: 'btn-primary',
                                    action: function (dialog) {
                                        var contabilidade = dialog.getModalBody().find('#contabilidade');
                                        var value = $(contabilidade).val();
                                        if(!value){
                                            BootstrapDialog.alert('Selecione uma contabilidade !');
                                            return;
                                        }

                                        var url = root + '/Banco/addContabilidade';
                                        var data = {
                                            banco: banco,
                                            contabilidade: value
                                        }

                                        $.post(url, data, function(ret){
                                            if(ret.result){
                                                grid.bootgrid('reload');
                                                BootstrapDialog.success(ret.msg);
                                                dialog.close();
                                            }else{
                                                BootstrapDialog.warning(ret.msg);
                                            }
                                        });
                                    }
                                }]
                            });
                        }
                    },{
                        label: 'Fechar',
                        icon: 'fa fa-times',
                        cssClass: 'btn-danger',
                        action: function (dialog) {
                            dialog.close();
                        }
                    }]
                });
            });
        });
    });
</script>