<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Banco</h2>
        <ol class="breadcrumb">
            <li>Banco</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Nome</strong>: <?php echo $Banco->nome; ?></p>

                <p><strong>Agencia</strong>: <?php echo $Banco->agencia; ?></p>

                <p><strong>Conta</strong>: <?php echo $Banco->conta; ?></p>

                <p><strong>Limite</strong>: <?php echo $Banco->limite; ?></p>
            </div>
        </div>
    </div>
</div>