<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Classificação</h2>
        <ol class="breadcrumb">
            <li>classificação</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Classificacao', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="form-group col-md-12">
                        <label for="descricao">Descrição</label>
                        <input type="text" required="required" name="descricao" id="descricao" class="form-control"
                               placeholder="Descricao">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="tipo">Tipo</label>
                        <select class="form-control selectPicker" name="tipo" id="tipo" required="required">
                            <option value="">Selecione</option>
                            <?php foreach ($Tipo as $t): ?>
                                <option value="<?php echo $t->id ?>"><?php echo $t->descricao ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="tipo">Considera na Demonstração?</label>
                        <select class="form-control selectPicker" name="considera" id="considera" required="required" data-role="tooltip" data-placement="bottom" title="Caso esta opcao seja sim, esta classificação será apresentada no Relatório Mapa Mensal!">
                            <option value="2" selected>Não</option>
                            <option value="1" >Sim</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="status">Status</label>
                        <select name="status" class="form-control selectPicker" id="status" required="required">
                            <option></option>
                            <?php  foreach ($Status as $s): ?>
                             <option value="<?php echo $s->id ?>"><?php echo $s->descricao ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Classificacao', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>