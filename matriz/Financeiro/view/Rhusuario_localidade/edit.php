
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Rhusuario_localidade</h2>
    <ol class="breadcrumb">
    <li>Rhusuario_localidade</li>
    <li class="active">
    <strong>Editar Rhusuario_localidade</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhusuario_localidade', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label class="required" for="dt_cadastro">Dt_cadastro <span class="glyphicon glyphicon-asterisk"></span></label>
        </div>
        <div class="form-group">
            <label class="required" for="dt_modificacao">Dt_modificacao <span class="glyphicon glyphicon-asterisk"></span></label>
        </div>
        <div class="form-group">
            <label for="usuario_id">Usuario</label>
            <select name="usuario_id" class="form-control" id="usuario_id">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Rhusuario_localidade->usuario_id)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="rhlocalidade_id">Rhlocalidade</label>
            <select name="rhlocalidade_id" class="form-control" id="rhlocalidade_id">
                <?php
                foreach ($Rhlocalidades as $r) {
                    if ($r->id == $Rhusuario_localidade->rhlocalidade_id)
                        echo '<option selected value="' . $r->id . '">' . $r->cidade . '</option>';
                    else
                        echo '<option value="' . $r->id . '">' . $r->cidade . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Rhusuario_localidade->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Rhusuario_localidade->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhusuario_localidade', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>