
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Rhusuario_localidade</h2>
    <ol class="breadcrumb">
    <li>Rhusuario_localidade</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Rhusuario_localidade', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhusuario_localidade', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhusuario_localidade', 'all', array('orderBy' => 'usuario_id')); ?>'>
                        usuario_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhusuario_localidade', 'all', array('orderBy' => 'rhlocalidade_id')); ?>'>
                        rhlocalidade_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhusuario_localidade', 'all', array('orderBy' => 'situacao_id')); ?>'>
                        situacao_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhusuario_localidade', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                        dt_cadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rhusuario_localidade', 'all', array('orderBy' => 'dt_modificacao')); ?>'>
                        dt_modificacao
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Rhusuario_localidades as $r) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($r->id, 'Rhusuario_localidade', 'view',
                    array('id' => $r->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->dt_cadastro, 'Rhusuario_localidade', 'view',
                    array('id' => $r->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->dt_modificacao, 'Rhusuario_localidade', 'view',
                    array('id' => $r->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->getUsuario()->id, 'Usuario', 'view',
                    array('id' => $r->getUsuario()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->getRhlocalidade()->id, 'Rhlocalidade', 'view',
                    array('id' => $r->getRhlocalidade()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->getSituacao()->id, 'Situacao', 'view',
                    array('id' => $r->getSituacao()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Rhusuario_localidade', 'edit', 
                    array('id' => $r->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Rhusuario_localidade', 'delete', 
                    array('id' => $r->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Rhusuario_localidade', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Rhusuario_localidade', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>