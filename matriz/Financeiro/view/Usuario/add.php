<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Usuários</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Cadastro de Usuário</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Usuario', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="nome">Nome: <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Usuario->nome ?>"
                                    placeholder="Nome" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="email">E-mail <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="email" name="email" id="email" class="form-control" value="<?php echo $Usuario->email ?>"
                                    placeholder="E-mail" onblur="validarEmail($(this).val())" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="login">Login<span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="login" id="login" class="form-control" onblur="validarLogin($(this).val())"
                                    value="<?php echo $Usuario->email ?>" placeholder="Login" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="senha">Senha <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="password" name="senha" id="senha" class="form-control" value="<?php echo $Usuario->senha ?>"
                                    placeholder="Senha" required>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="con_senha">Confirmar Senha<span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="password" name="con_senha" id="con_senha" class="form-control" placeholder="Confirmar Senha"
                                    required>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="gestor_estoque">Gestor Estoque?<span class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="gestor_estoque" name="gestor_estoque" required class="form-control">
                                    <option value="2">Não</option>
                                    <option value="1">Sim</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="acompanhar_solicitacao">Acompanhar solicitação?</label>
                                <select name="acompanhar_solicitacao" class="form-control" id="acompanhar_solicitacao">
                                        <option selected value="0">Não</option>
                                        <option value="1">Sim</option>
                                </select>

                            </div>
                            <div class="clearfix">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 well">
                                <fieldset class="form-group">
                                    <legend>Acesso aos modulos</legend>
                                    <?php foreach ($Modulos as $m): ?>
                                    <?php
                                    echo '<div class="form-check">';
                                    echo '<label class="form-check-label">';
                                    echo '<input type="checkbox" class="form-check-input" name="modulo[]" id="modulo"
                                    value="' . $m->id . '">';
                                    echo ' ' . $m->nome;
                                    echo '</label>';
                                    ?>
                                    <?php endforeach ?>
                                </fieldset>
                            </div>
                          
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 well" id="localidade" style="display: none;">
                                <fieldset class="form-group">
                                    <legend>Acesso RH Localidade</legend>
                                    <?php foreach ($Rhlocalidade as $m): ?>
                                    <?php
                                    echo '<div class="form-check">';
                                    echo '<label class="form-check-label">';
                                    echo '<input type="checkbox" class="form-check-input" name="rhlocalidade[]"
                                    value="' . $m->id . '">';
                                    echo ' ' . $m->cidade;
                                    echo '</label>';
                                    ?>
                                    <?php endforeach ?>
                                </fieldset>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 well" id="estoque" style="display: none;">
                                <fieldset class="form-group">
                                    <legend>Acesso EPI Estoque Subestoque</legend>
                                    <?php foreach ($Estq_subestoque as $m): ?>
                                    <?php
                                    echo '<div class="form-check">';
                                    echo '<label class="form-check-label">';
                                    echo '<input type="checkbox" class="form-check-input" name="estq_subestoque[]"
                                    value="' . $m->id . '">';
                                    echo ' ' . $m->nome;
                                    echo '</label>';
                                    ?>
                                    <?php endforeach ?>
                                </fieldset>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Usuario', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div><!-- /ibox content -->
            </div><!-- / ibox -->
        </div><!-- /wrapper-->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->

<script>

    var login = document.getElementById("login");

    function validarLogin(r) {
        $.ajax({
            type: 'POST',
            url: '<?php echo SITE_PATH; ?>/Login/check_login',
            data: 'login=' + r,
            success: function (txt) {
                if (txt > 0) {
                    login.setCustomValidity("Login já cadastrado!");
                } else {
                    login.setCustomValidity("");
                }
            }
        });
    }

    var email = document.getElementById("email");

    function validarEmail(r) {
        $.ajax({
            type: 'POST',
            url: '<?php echo SITE_PATH; ?>/Login/check_email',
            data: 'login=' + r,
            success: function (txt) {
                if (txt > 0) {
                    email.setCustomValidity("Email já cadastrado!");
                } else {
                    email.setCustomValidity("");
                }
            }
        });
    }


    var password = document.getElementById("senha")
        , confirm_password = document.getElementById("con_senha");

    function validatePassword() {

        if (password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Senhas não conferem");
        } else {
            confirm_password.setCustomValidity('');
        }

    }
    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;

 $(document).ready(function(){
    $('input[name="modulo[]"]').change(function() {

        if($(this).is(':checked')) {
            if ($(this).val() == 2) {
                $('#localidade').show();
            } 
            if ($(this).val() == 4) {
            $('#estoque').show();
        } 
        }
    });
    
});

</script>