<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Usuários</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <strong>Edição de Usuário</strong>
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Usuario', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                    class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="nome">Nome <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Usuario->nome ?>" placeholder="Nome" required>
                            </div>

                            <!--        <div class="form-group col-md-12 col-sm-6 col-xs-12">-->
                            <!--            <label class="required" for="email">E-mail <span class="glyphicon glyphicon-asterisk"></span></label>-->
                            <!--            <input type="text" name="email" id="email" class="form-control" value="-->
                            <?php //echo $Usuario->email ?><!--" onblur="validarEmail($(this).val())" placeholder="Email" required>-->
                            <!--        </div>-->

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="email">E-mail <span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="email" id="email" class="form-control"
                                       value="<?php echo $Usuario->email ?>" placeholder="Email" required>
                            </div>

                            <!--        <div class="form-group col-md-4 col-sm-6 col-xs-12">-->
                            <!--            <label class="required" for="login">Login<span class="glyphicon glyphicon-asterisk"></span></label>-->
                            <!--            <input type="text" name="login" id="login" class="form-control" onblur="validarLogin($(this).val())" value="-->
                            <?php //echo $Usuario->login ?><!--" placeholder="Login" required>-->
                            <!--        </div>-->


                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="login">Login<span
                                            class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="login" id="login" class="form-control"
                                       value="<?php echo $Usuario->login ?>" placeholder="Login" required>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="senha">Senha</label>
                                <input type="password" id="senha" class="form-control" name="senha"
                                       value="<?php echo $Usuario->senha ?>" required/>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="status">Status</label>
                                <select name="status" class="form-control" id="status">
                                    <?php
                                    foreach ($Status as $r) {
                                        if ($r->id == $Usuario->status)
                                            echo '<option selected value="' . $r->id . '">' . $r->descricao . '</option>';
                                        else
                                            echo '<option value="' . $r->id . '">' . $r->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="primeiro_acesso">Resetar senha?</label>
                                <select name="primeiro_acesso" class="form-control" id="primeiro_acesso">
                                    <?php

                                    if ($Usuario->primeiro_acesso == 0) { ?>
                                        <option selected value="0">Não</option>
                                        <option value="1">Sim</option>
                                    <?php } else { ?>
                                        <option selected value="1">Sim</option>
                                        <option value="0">Não</option>
                                    <?php } ?>

                                </select>

                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="acompanhar_solicitacao">Acompanhar solicitação?</label>
                                <select name="acompanhar_solicitacao" class="form-control" id="acompanhar_solicitacao">
                                    <?php

                                    if ($Usuario->acompanhar_solicitacao == 0) { ?>
                                        <option selected value="0">Não</option>
                                        <option value="1">Sim</option>
                                    <?php } else { ?>
                                        <option selected value="1">Sim</option>
                                        <option value="0">Não</option>
                                    <?php } ?>
                                </select>

                            </div>

                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="gestor_estoque">Gestor Estoque?<span class="glyphicon glyphicon-asterisk"></span></label>
                                <select id="gestor_estoque" name="gestor_estoque" required class="form-control">
                                    <option value="2" <?= $Usuario->gestor_estoque == 2 ? 'selected' : '' ?> >Não</option>
                                    <option value="1" <?= $Usuario->gestor_estoque == 1  ? 'selected' : '' ?>>Sim</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 well">
                                <fieldset class="form-group">
                                    <legend>Acesso aos modulos</legend>               
                                    <?php  
                                        foreach($Modulos as $m){
                                            echo '<div class="form-check">';
                                            echo '<label class="form-check-label">';
                                            echo '<input type="checkbox" class="form-check-input" name="modulo[]" id="modulo" '. (in_array($m->id, $Usuario_modulo) ? 'checked' : null) .'
                                            value="' . $m->id . '">';
                                            echo ' ' . $m->nome;
                                            echo '</label>';
                                        } 
                                    ?>                                  
                                </fieldset>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 well" id="localidade" style="display: none;">
                                <fieldset class="form-group">
                                    <legend>Acesso RH Localidade</legend>
                                    <?php foreach ($Rhlocalidade as $m){                             
                                        echo '<div class="form-check">';
                                        echo '<label class="form-check-label">';
                                        echo '<input type="checkbox" class="form-check-input" name="rhlocalidade[]" '. (in_array($m->id, $rhusuario_localidade) ? 'checked' : null) .'
                                        value="' . $m->id . '">';
                                        echo ' ' . $m->cidade;
                                        echo '</label>';
                                    }?>
                                   
                                </fieldset>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12 well" id="estoque" style="display: none;">
                                <fieldset class="form-group">
                                    <legend>Acesso EPI Estoque Subestoque</legend>
                                    <?php foreach ($ccc as $m){                                    
                                        echo '<div class="form-check">';
                                        echo '<label class="form-check-label">';
                                        echo '<input type="checkbox" class="form-check-input" name="estq_subestoque[]"
                                        value="' . $m->id . '">';
                                        echo ' ' . $m->nome;
                                        echo '</label>';
                                    }?>
                                   
                                </fieldset>
                            </div>
                            <div class="clearfix"></div>
                        </div> 
                        <input type="hidden" name="id" id="id" value="<?php echo $Usuario->id; ?>">
                   
                        <?php  
                            foreach($usuarioModuloID as $m){
                            echo '<div class="form-check">';
                            echo '<label class="form-check-label">';
                            echo '<input type="hidden" class="form-check-input" name="usuarioModuloID[]" id="usuarioModuloID" checked="checked" value="' . $m . '">';
                            echo '</label>';
                            } 
                            foreach($rhusuarioLocalidadeId as $ll){
                                echo '<div class="form-check">';
                                echo '<label class="form-check-label">';
                                echo '<input type="hidden" class="form-check-input" name="rhusuarioLocalidadeId[]" id="rhusuarioLocalidadeId" checked="checked" value="' . $ll . '">';
                               
                                echo '</label>';
                                } 
                        ?>  
                       
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Usuario', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <!--                            <button type="button" class="btn btn-primary" data-toggle="modal"-->
                            <!--                                    onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar-->
                            <!--                            </button>-->
                            <!--                            <input type="submit" id="validForm" style="display: none;"/>-->
                            <input type="submit" class="btn btn-success" id="salvar" value="Salvar"/>
                        </div>
                    </form>

                </div><!-- /ibox content -->
            </div><!-- / ibox -->
        </div><!-- /wrapper-->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->

<script>

    var login = document.getElementById("login");

    function validarLogin(r) {
        $.ajax({
            type: 'POST',
            url: '<?php echo SITE_PATH; ?>/Login/check_login',
            data: 'login=' + r + '&user=<?php echo $Usuarios->id; ?>',
            success: function (txt) {
                if (txt > 0) {
                    login.setCustomValidity("Login já cadastrado!");
                } else {
                    login.setCustomValidity("");
                }
            }
        });
    }

    var email = document.getElementById("email");

    function validarEmail(r) {
        $.ajax({
            type: 'POST',
            url: '<?php echo SITE_PATH; ?>/Login/check_email',
            data: 'login=' + r + '&user=<?php echo $Usuarios->id; ?>',
            success: function (txt) {
                if (txt > 0) {
                    email.setCustomValidity("Email já cadastrado!");
                } else {
                    email.setCustomValidity("");
                }
            }
        });
    }
    $(document).ready(function(){
    $('input[name="modulo[]"]').change(function() {

        if($(this).is(':checked')) {
            if ($(this).val() == 2) {
                $('#localidade').show();
            } 
            if ($(this).val() == 4) {
            $('#estoque').show();
        } 
        }
    });
    
});
</script>