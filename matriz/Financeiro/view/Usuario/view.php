<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">

                    <div class="text-right" style="padding-bottom: 20px">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <legend>Usuário</legend>
<p><strong>Nome</strong>: <?php echo $Usuario->nome;?></p>
<p><strong>E-mail</strong>: <?php echo $Usuario->email;?></p>
<p>
    <strong>Status</strong>:
    <?php
    echo $this->Html->getLink($Usuario->getRhstatus()->descricao, 'Rhstatus', 'view',
    array('id' => $Usuario->getRhstatus()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
                </div><!-- /ibox content -->
            </div><!-- / ibox -->
        </div><!-- /wrapper-->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->
