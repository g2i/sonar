<div class="row">
    <div class="col-md-6 text-right pull-right">
        <h1>
            <?php echo '<a href="javascript:;" data-placement="bottom" class="btn btn-info" data-toggle="tooltip2" id="ratear" title="Adicionar Rateio" onclick="Navegar(\'' . $this->Html->getUrl("Rateio_programacao", "add", array("ajax" => true, "modal" => "1", "programacao" => $this->getParam('programacao'))) . '\',\'go\')">'; ?>
            <span class="glyphicon glyphicon-plus"></span> Cadastrar Rateio</a>
        </h1>
    </div>
</div>

<!-- tabela de resultados -->
<div class="row clearfix">
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rateio_programacao', 'all', array('orderBy' => 'contar_pagar', 'search' => $search)); ?>'>
                        Centro de Custo
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rateio_programacao', 'all', array('orderBy' => 'empresa', 'search' => $search)); ?>'>
                        Empresa
                    </a>
                </th>

                <th>
                    <a href='<?php echo $this->Html->getUrl('Rateio_programacao', 'all', array('orderBy' => 'valor', 'search' => $search)); ?>'>
                        Valor
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Rateio_programacao', 'all', array('orderBy' => 'observacao', 'search' => $search)); ?>'>
                        Observação
                    </a>
                </th>

                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Rateio_programacaoes as $r) {
                echo '<tr>';

                echo '<td>';
                echo $this->Html->getLink($r->getCentro_custo()->descricao, 'Centro_custo', 'view',
                    array('id' => $r->getCentro_custo()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink($r->getContabilidade()->nome, 'Contabilidade', 'view',
                    array('id' => $r->getContabilidade()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                echo '<td>';
                echo $this->Html->getLink(number_format($r->valor, 2, ',', '.'), 'Rateio_programacao', 'view',
                    array('id' => $r->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($r->observacao, 'Rateio_programacao', 'view',
                    array('id' => $r->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';

                if ($this->getParam('modal')) {
                    echo '<td>';
                    echo '<a href="javascript:;" data-placement="bottom" data-role="tooltip" title="Editar" onclick="Navegar(\'' . $this->Html->getUrl("Rateio_programacao", "edit", array("ajax" => true, "id" => $r->id, "modal" => "1")) . '\',\'go\')">
                                <span class="glyphicon update"></span></a>';
                    echo '</td>';

                    echo '<td >';
                    echo '<a href="javascript:;" data-placement="bottom" data-role="tooltip" title="Deletar" onclick="Navegar(\'' . $this->Html->getUrl("Rateio_programacao", "delete", array("ajax" => true, "id" => $r->id, "modal" => "1")) . '\',\'go\')">
                                <span class="glyphicon delet"></span></a>';
                    echo '</td>';
                } else {
                    echo '<td >';
                    echo $this->Html->getLink('<span class="glyphicon update"></span> ', 'Rateio_programacao', 'edit',array('id' => $r->id),array('data-role'=>'tooltip' ,'data-placement'=>'bottom','title'=>'Editar'));
                    echo '</td>';
                    echo '<td >';
                    echo $this->Html->getLink('<span class="glyphicon delet"></span> ', 'Rateio_programacao', 'delete',
                        array('id' => $r->id),
                        array('data-role'=>'tooltip' ,'data-placement'=>'bottom','title'=>'Deletar', 'data-toggle' => 'modal'));
                    echo '</td>';
                }
                echo '</tr>';
            }
            ?>
        </table>
        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function () {
        $('#search').keyup(function () {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Rateio_programacao', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    else
                        echo '"' . $this->Html->getUrl('Rateio_programacao', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });
    });


</script>