<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
        <div class="col-md-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-info pull-right">Aceitas</span>
                            <h5>Solicitações</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                <?php echo $solicitacaoAceitaTotal->total; ?>
                            </h1>
                            <div class="stat-percent font-bold text-success"><i class="fa fa-bolt""></i></div>
                            <small>Aceitas</small>
                        </div>
                    </div>
                </div>
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">Esperando aceite</span>
                        <h5>Solicitações</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">
                            <?php echo $solicitacaoAguardandoAceiteTotal->total; ?>
                        </h1>
                        <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>Esperando aceite</small>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-danger pull-right">Recusadas</span>
                            <h5>Solicitações</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">
                                <?php echo $solicitacaoRecusadaTotal->total; ?>
                            </h1>
                            <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                            <small>Recusadas</small>
                        </div>
                    </div>
                </div>          
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Solicitações</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Solicitante</th>
                                    <th>Data Documento</th>
                                    <th>Fornecedor</th>
                                    <th>Empresa</th>
                                    <th>Aplicação/Projeto</th>
                                    <th>Observações</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php                                 
                                foreach ($solicitacaoAguardandoAceite as $d) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $d->id;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->solicitante;
                                    echo '</td>';
                                    echo '<td>';
                                    echo convertDataSQL4BR($d->data);
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->situacao;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->empresa;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->projeto;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $d->observacao;
                                    echo '</td>';
                                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                                    echo $this->Html->getLink('<span class="fa fa-exclamation-triangle" data-placement="top"  title="#"></span> ', 'Contaspagar', 'solicitacaoContasPagar',
                                        array('id' => $d->id, 'first' => 1,'modal' => 1),
                                        array('class' => 'btn btn-warning btn-sm','data-toggle' => 'modal')); 
                                    echo '</td>';         
                                    echo '</tr>';
                                }exit;
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
   
    $(function () {
        $.ajax({
            type: 'POST',
            url: root + '/Index/setor',
            success: function (txt) {
                Morris.Donut({
                    element: 'morris-donut-chart',
                    data: jQuery.parseJSON(txt),
                    resize: true,
                    colors: ['#87d6c6', '#54cdb4', '#1ab394', '#9de8d8', '#b7eee2']
                });
            }
        });


    });
</script>