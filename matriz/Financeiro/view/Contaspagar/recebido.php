<form method="post" role="form" action="<?php echo $this->Html->getUrl('Contaspagar', 'recebido') ?>">
    <div class="well">
        <input type="hidden" name="id" value="<?php echo $this->getParam('id');?>">
        <fieldset class="form-group">
            <h3>O documento ou boleto foi recebido para pagamento?</h3>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="recebido" value="1">
                    Recebido
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="recebido" value="2">
                    Não Recebido
                </label>
            </div>
            <h5 style="color: #0a6aa1">Ultima atualização: <?php if($Contaspagar->recebido == 1) { echo '<strong>' . 'Recebido' . '</strong>'; } else { echo '<strong>' . 'Não recebido' . '</strong>'; } ?></h5>
        </fieldset>
    </div>
    <input type="submit" class="btn btn-warning" value="Confirmar">
</form>