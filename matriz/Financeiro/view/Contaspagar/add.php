<style type="text/css">
    .nav-tabs > li > a {
        border: 1px solid #ddd;
    }

    .nav-tabs {
        border-bottom: none;
    }
</style>
<?php if ($this->getParam('modal')): ?>
    <style>
        .select2-close-mask {
            z-index: 2099;
        }

        .select2-dropdown {
            z-index: 3051;
        }

        .has-error .select2-selection {
            border: 1px solid #a94442;
            border-radius: 4px;
        }
    </style>
<?php else: ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a Pagar</h2>
            <ol class="breadcrumb">
                <li>Pagar</li>
                <li class="active">
                    <strong>Adicionar</strong>
                </li>
            </ol>
        </div>
    </div>
<?php endif; ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form id="frmConta" method="post" role="form" action="<?php echo $this->Html->getUrl('Contaspagar', 'add') ?>"
              id="formIndividual">
            <div class="ibox">
                <div class="ibox-title" style="border-color: #1c84c6;">
                    <h2>Informações do Pagamento</h2>
                </div>
                <div class="ibox-content">
                    <div class="alert alert-info">Os campos marcados com
                        <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <input type="hidden" name="formulario" value="individual">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="required" for="data">Data do Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="data" id="data" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="required" for="vencimento">Vencimento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="vencimento" id="vencimento"
                                       required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="valorBruto">Valor Bruto <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" value="0.00" name="valorBruto"
                                       id="valorBruto"
                                       class="form-control money2" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="juros">Juros</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" value="0.00" name="juros" id="juros"
                                       class="form-control money2">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="multa">Multa</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" onblur="calculaValor()" value="0.00" name="multa" id="multa"
                                       class="form-control money2">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="desconto">Desconto</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="desconto" value="0.00" onblur="calculaValor()" id="desconto"
                                       class="form-control money2">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="valor">Valor Liquido</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input readOnly type="text" name="valor" id="valor" class="form-control money2"
                                       value="0.00"
                                       placeholder="Valor">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="dataPagamento">Data de Pagamento</label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="dataPagamento"
                                       id="dataPagamento">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="required" for="idPlanoContas">Plano de contas <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas" required>
                                <option></option>
                                <?php
                                foreach ($Planocontas as $p) {
                                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="required" for="idFornecedor">Credor <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <td width="50">
                                <button type="button" id="btnAddFornecedor" class="btn btn-info btn-xs">Novo</button>
                            </td>
                            <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax"
                                    role="credor" required>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="tipoDocumento">Tipo de Documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento" required>
                                <option></option>
                                <?php
                                foreach ($tipoDocumento as $t) {
                                    echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="required" for="numerodocumento">Número do documento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" name="numerodocumento" id="numerodocumento" class="form-control"
                                   required>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="tipoPagamento">Forma de Pagamento <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento" required>
                                <option></option>
                                <?php
                                foreach ($tipoPagamento as $t) {
                                    echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group" style="display:none">
                            <label for="status">Status</label>
                            <select name="status" class="form-control selectPicker" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">

                        <div class="form-group">
                            <label class="required" for="contabilidade">Empresas <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="contabilidade" name="contabilidade" class="form-control selectPicker" required>
                                <option></option>
                                <?php
                                foreach ($Contabilidade as $c) {
                                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                            <textarea name="complemento" id="complemento"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <?php if (!$this->getParam('modal')): ?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Contaspagar', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="ibox">
                <div class="ibox-title" style="border-color: #00a65a;">
                    <h2>Projeto</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" for="contabilidade">Grupo <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="fin_projeto_grupo_id" name="fin_projeto_grupo_id"
                                    class="form-control selectPicker"
                                    required>
                                <option>Selecione Um Grupo</option>
                                <?php
                                foreach ($Fin_projeto_grupo as $c) {
                                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" for="contabilidade">Empresa <span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="fin_projeto_empresa_id" name="fin_projeto_empresa_id"
                                    class="form-control selectPicker"
                                    required>

                            </select>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="required" for="contabilidade">Unidade<span
                                        class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="fin_projeto_unidade_id" name="fin_projeto_unidade_id"
                                    class="form-control selectPicker"
                                    required>

                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>



<script type="text/javascript">

    function calculaValor() {
        if ($('#multa').val() != "undefined" && $('#multa').val() != "") {
            var multa = $('#multa').unmask();
        } else {
            var multa = 0;
        }

        if ($('#desconto').val() != "undefined" && $('#desconto').val() != "") {
            var desconto = $('#desconto').unmask();
        } else {
            var desconto = 0;
        }

        if ($('#valorBruto').val() != "undefined" && $('#valorBruto').val() != "") {
            var valorBruto = $('#valorBruto').unmask();
        } else {
            var valorBruto = 0;
        }

        if ($('#juros').val() != "undefined" && $('#juros').val() != "") {
            var juros = $('#juros').unmask();
        } else {
            var juros = 0;
        }
        var valorLiquido = (valorBruto + juros + multa) - desconto;
        valorLiquido = Math.round(valorLiquido * 100) / 100;

        $('#valor').unpriceFormat();
        $('#valor').val(valorLiquido.toFixed(2));
        $('#valor').priceFormat({
            prefix: '',
            centsSeparator: ',',
            thousandsSeparator: '.',
            allowNegative: true
        });
        $('#valor').change();
    }

    $(document).ready(function () {
        $('#btnAddFornecedor').click(function () {
            var url = root + '/Fornecedor/add/origem:-1/ajax:true/modal:1/';
            LoadGif();
            $('<div></div>').load(url, function () {
                BootstrapDialog.show({
                    title: 'Adicionar Fornecedor',
                    message: this,
                    size: 'size-wide',
                    type: BootstrapDialog.TYPE_DEFAULT,
                    onshown: function (dialog) {
                        initeComponentes(dialog.getModalBody());
                        CloseGif();
                    }
                });
            });
        });
    });
</script>