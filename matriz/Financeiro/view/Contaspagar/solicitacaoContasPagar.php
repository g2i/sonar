
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Solicitação Rateio </h2>
    <ol class="breadcrumb">
    <li>Solicitacao Rateio </li>
    <li class="active">
    <strong>Parcelas</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">            
            <form id="frmSolicitacao" method="post" role="form" action="<?php echo $this->Html->getUrl('Contaspagar', 'solicitacaoContasPagar') ?>">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                <div class="clearfix"></div>
                <input type="hidden" name="id" value="<?php echo $id; ?>" >
                    <div class="col-xs-6 col-md-3">
                        <label for="inicio">Inicio</label>
                        <div class='input-group dateInicio'>
                            <input type='text' class="form-control dateFormat" name="iniio" id="inicio"
                                value="<?php echo $inicio; ?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <label for="fim">Fim</label>
                        <div class='input-group dateFim'>
                            <input type='text' class="form-control dateFormat" name="fim" id="fim"
                                value="<?php echo $fim; ?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>                    
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label for="idFornecedor">Credores</label>
                            <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax"></select>
                        </div>
                    </div>                    
                    <div class="col-xs-12 col-md-3">
                        <div class="form-group">
                            <label for="contabilidade">Empresas</label>
                            <select name="contabilidade" id="contabilidade" class="form-control selectPicker">
                                <option></option>
                                <?php foreach ($Contabilidade as $c): ?>
                                    <option value="<?php echo $c->id ?>"><?php echo $c->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="text-right" style="margin-top: 25px">   
                    <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"   class="btn btn-default"  data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                           title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                        <button type="button" class="btn btn-default" id="buscar-filtro" data-tool="tooltip"  data-placement="bottom" title="Pesquisar"><span class="glyphicon glyphicon-search"></span></button>
                    </div>
                <div class="clearfix"></div>
                  <!-- <botao de cadastro -->
                <div class="text-left ">
                    <input type="submit" class="btn btn-primary" value="Confirmar">
                    <a href="<?php echo $this->Html->getUrl('Contaspagar', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                </div>
        </div>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>Id Solicitação</th>
                <th>                
                    Vencimento                 
                </th>
                <th>
                    Credores
                </th>
                <th>
                    Empresa
                </th>
                <th>Valor</th>
                <th>
                    Observação
                </th>              
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php 
            foreach ($ContasPagar as $r) {
                echo '<tr>'; 
                echo '<td>';
                echo  $r->solicitacao_id ;
                echo '</td>';    
                echo '<td>';
                echo  convertDataSQL4BR($r->vencimento);
                echo '</td>';   
                echo '<td>';
                echo  $r->getFornecedor()->nome;
                echo '</td>';
                echo '<td>';
                echo $r->getContabilidade()->nome;
                echo '</td>';                
                echo '<td>';
                echo  $r->valor;
                echo '</td>';
                echo '<td>';
                echo $r->complemento;
                echo '</td>';
                echo '<td id="idsContas">';
                echo '<input type="checkbox" class="form-check-input" name="contaspagar_id[]" id="contaspagar_id" value="' .  $r->id . '">';
                echo '</td>';
                echo '</tr>';
            }            
            ?>
        </table>
        <!-- menu de paginação 
        <div style="text-align:center"><?php echo $nav; ?></div>-->
    </div>
</div>
</form>
<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Rhprojetos', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Rhprojetos', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });

        //
            //function para o btn de pesquisa em form 
        $('.filtros').find('#buscar-filtro').click(function(){
            let form_action = $(this).closest('form').attr('action');
            let form_serialize = $(this).closest('form').serialize();
            let url_completa = form_action + '?' + form_serialize;
            console.log(url_completa);
            carregaTabelaResponsiva(url_completa);
        });
        
    //function para paginar por ajax
        $(document).on('click','.pagination a, .table thead a', function(e){
            e.preventDefault();
            let url = $(this).attr('href');

            if(url != "")
            {            
                carregaTabelaResponsiva(url);
            }
            return false;
        });
    });
    //function para pegar as URLs 
    function carregaTabelaResponsiva(url) {
        $.ajax({
            type: 'GET',
            url: url,
            beforeSend: () => {
            },
            success: (data) => {
                let conteudo = $('<div>').append(data).find('.table-responsive');
                $(".table-responsive").html(conteudo);
            },
            complete: () => {
            },
            error: () => {
            },
        });
    }

    
</script>
</div>
</div>
</div>
</div>
</div>