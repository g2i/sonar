<style>
    #formulario td {
        padding: 10px;
    }

    th {
        background: #F5F5F5;
    }
    .bootgrid-table tr > td > button {
        margin-left: 2px;
        margin-top: 2px;
    }
    .has-error .select2-selection {
        border: 1px solid #a94442;
        border-radius: 4px;
    }

</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas a Pagar</h2>
        <ol class="breadcrumb">
            <li>Pagar</li>
            <li class="active">
                <strong>Quitação</strong>
            </li>
        </ol>
    </div>
    <div style="margin-top: 100px">
        <div class="col-xs-12 col-md-6">
            <legend>Filtros</legend>
            <div>
                <form id="frmFiltros" role="form">
                    <div class="col-xs-12 col-md-6">
                        <label for="inicio">Inicio</label>
                        <div class='input-group dateInicio'>
                            <input type='text' class="form-control dateFormat" name="inicio" id="inicio"
                                   value="<?php echo $inicio; ?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="fim">Fim</label>
                        <div class='input-group dateFim'>
                            <input type='text' class="form-control dateFormat" name="fim" id="fim"
                                   value="<?php echo $fim; ?>"/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="fornecedor">Credores</label>
                            <select id="fornecedor" name="fornecedor" class="form-control credor-ajax"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idPlanoContas">Plano de Contas</label>
                            <select id="idPlanoContas" name="idPlanoContas" class="form-control selectPicker">
                                <option></option>
                                <?php foreach ($Planocontas as $p): ?>
                                    <option value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6" style="padding-top: 30px;">
                        <div class="form-group">
                            <label for="totald">Total do Per&iacute;odo:</label>
                                            <span id="totalPeriodo"
                                                  style="margin-bottom: 5px;display: inline-block;"></span>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <button id="btnProcurar" class="btn btn-default" type="button"
                                    data-role="tooltip" data-placement="bottom" title="Procurar">
                                <i class="fa fa-search"></i>
                            </button>
                            <button id="btnLimpar" class="btn btn-default" type="button"
                                    data-role="tooltip" data-placement="bottom" title="Limpar">
                                <i class="fa fa-refresh"></i>
                            </button>
                            <button id="btnRelatorio" type="button" class="btn btn-default"
                                    data-role="tooltip" data-placement="bottom" title="Gerar Relatório">
                                <i class="fa fa-print"></i>
                            </button>
                        </div>
                    </div>
                </form>
                <div style="clear:both;"></div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6" style="border-left:1px solid #E5E5E5;">
            <fieldset>
                <legend>Quita&ccedil;&atilde;o</legend>
                <div>
                    <form id="frmPagamento" role="form">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="movimento">Banco:</label>
                                <select id="movimento" name="movimento" class="form-control selectPicker">
                                    <option></option>
                                    <?php foreach ($movimento as $m): ?>
                                        <option value="<?php echo $m->id ?>"><?php echo $m->getBanco($m->idBanco)->nome . " - " . $m->mes . "/" . $m->ano ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="situacao">Situação:</label>
                                <select id="situacao" class="form-control selectPicker">
                                    <option value="1"> Consolidadado</option>
                                    <option value="2">A cair</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="dtBase">Data do Pagamento:</label>
                                <div class='input-group date'>
                                    <input type='text' class="form-control dateFormat" name="dtBase" id="dtBase"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="numeroDocumento">Número do Documento :</label>
                                <input id="numeroDocumento" class="form-control">
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="tipoPagamento">Forma de Pagamento :</label>
                                <select id="tipoPagamento" class="form-control selectPicker">
                                    <option></option>
                                    <?php foreach ($tipoPagamento as $tipo): ?>
                                        <option value="<?php echo $tipo->id ?>"><?php echo $tipo->descricao ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div style="clear:both"></div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="totald">Total selecionado:</label>
                                <span id="totalSelecionado"> R$ 0,00</span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <input id="btnQuitar" class="btn btn-primary" type="button"
                                       data-role="tooltip" data-placement="bottom" title="Quitar Contas"
                                       value="Confirmar"/>
                            </div>
                        </div>
                    </form>
                </div>
            </fieldset>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="table-responsive mCustomScrollbar" data-mcs-theme="light" data-mcs-axis="x">
                    <table id="tblContas" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th data-column-id="id" data-identifier="true"
                                data-visible="false" data-visible-in-selection="false">Id</th>
                            <th data-column-id="vencimento" data-converter="date">Vencimento</th>
                            <th data-column-id="credor">Credores</th>
                            <th data-column-id="plano">Plano de Contas</th>
                            <th data-column-id="empresa">Empresa</th>
                            <th data-column-id="tipo">Tipo. Documento</th>
                            <th data-column-id="obs">Obs.</th>
                            <th data-column-id="valor" data-converter="currency">Valor</th>
                            <th data-column-id="deducao" data-converter="currency">Valor c/ Dedução</th>
                            <th data-column-id="opcoes" data-formatter="opcoes" data-width="75"
                                data-visible-in-selection="false" data-sortable="false"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var valorTotal = 0;
    var valorSelecionado = 0;
    var contas = [];

    $(document).ready(function(){
        $('[data-role="tooltip"]').tooltip();

        var periodo = moment().date(1);
        $('#inicio').val(periodo.format('DD/MM/YYYY'));
        $('#fim').val(periodo.endOf('month').format('DD/MM/YYYY'));

        var grid = $('#tblContas').bootgrid({
            sorting: false,
            selection: true,
            multiSelect: true,
            selectColumnRight: true,
            rowCount: -1,
            ajax: true,
            url: root + '/Contaspagar/getContasPagar',
            ajaxSettings: {
                method: "GET"
            },
            converters:{
                currency: {
                    from: function (value) { return value; },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return 'R$ ' + $.number(value, 2);
                    }
                },
                date: {
                    from: function (value) {
                        if(value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                    }
                }
            },
            formatters:{
                opcoes: function(column, row){

                    return '<button type="button" data-toggle="tooltip" data-placement="top" title="Pasta de Documentos/Anexos" class="btn btn-xs btn-primary command-galeria" data-row-id-programacao="'+row.idProgramacao+'" data-row-id="' + row.id + '"><span class="fa fa-paperclip"></span></button>';

                }
            },
            requestHandler: function (request) {
                request.inicio = $('#inicio').val();
                request.fim = $('#fim').val();
                request.fornecedor = $('#fornecedor').val();
                request.idPlanoContas = $('#idPlanoContas').val();

                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                valorTotal = 0;
                contas = [];
                return request;
            },
            responseHandler: function (response){
                valorTotal = response.valorTotal;
                return response;
            },
            templates: {
                search: ""
            }
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {
            $("#totalPeriodo").text('R$ ' + $.number(valorTotal, 2));
            grid.find('[data-toggle="tooltip"]').tooltip();


            grid.find(".command-galeria").unbind('click');
            grid.find(".command-galeria").on("click", function (e) {
                var conta = $(this).data("row-id");
                var idProgramacao = $(this).data("row-id-programacao")==null?'':$(this).data("row-id-programacao");
                var urlLista = root + '/Anexo_financeiro/all/tipo_doc:10/origem:1/id_programacao:'+idProgramacao+'/id_externo:' + conta;
                window.open(urlLista,'_blank');
            });

        });

        grid.on("selected.rs.jquery.bootgrid", function(e, rows){

            for (var i = 0; i < rows.length; i++){
                contas.push(rows[i].id);
                valorSelecionado += parseFloat(rows[i].deducao);
            }

            $("#totalSelecionado").text('R$ ' + $.number(valorSelecionado, 2));
        });

        grid.on("deselected.rs.jquery.bootgrid", function(e, rows){
            for (var i = 0; i < rows.length; i++){
                var idx  = contas.indexOf(rows[i].id);
                contas.splice(idx, 1);
                valorSelecionado -= parseFloat(rows[i].deducao);
            }

            $("#totalSelecionado").text('R$ ' + $.number(valorSelecionado, 2));
        });

        $('#btnProcurar').click(function(){
            grid.bootgrid('reload');
        });

        $('#btnLimpar').click(function(){
            var data = moment().date(1);
            $('#inicio').val(data.format('DD/MM/YYYY'));
            $('#fim').val(data.endOf('month').format('DD/MM/YYYY'));
            $('#fornecedor').val('').trigger('change');
            $('#idPlanoContas').val('').trigger('change');
            grid.bootgrid('reload');
        });

        $('#btnRelatorio').click(function(){
            var data = {
                inicio: moment($("#inicio").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                fim: moment($("#fim").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                forn: $("#fornecedor").val(),
                plano: $("#idPlanoContas").val()
            };

            if (data.inicio == "") {
                data.inicio = -1;
                data.fim = -1;
            }

            if (data.forn == "") {
                data.forn = -1;
            }

            if (data.plano == "") {
                data.plano = -1;
            }

            $.ReportPost('cp_q', data);
        });

        $('#btnQuitar').click(function(){
            if(contas.length < 1) {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    message: 'Selecione uma conta !',
                    type: BootstrapDialog.TYPE_WARNING
                });
                return;
            }

            BootstrapDialog.confirm({
                title: 'Aviso',
                message: 'Voc\u00ea tem certeza ?',
                type: BootstrapDialog.TYPE_WARNING,
                closable: false,
                draggable: false,
                btnCancelLabel: 'N\u00e3o',
                btnOKLabel: 'Sim',
                btnOKClass: 'btn-warning',
                callback: function(result) {
                    if(result) {
                        var controle = 1; // se 1 Realiza o pagamento
                        //Validacao 1 - N�o realiza o pagamento se a data de pagamento estiver fora do movimento e situacao for consolidado.

                        if ($('#situacao').val() == '1') {
                            $.ajax({
                                type: "POST",
                                url: root + "/Contaspagar/validaRecebimento",
                                data: { dtBase: moment($('#dtBase').val(), "DD/MM/YYYY").format('YYYY-MM-DD') },
                                async: false,
                                success: function (txt) {
                                    if (txt != "1") {
                                        BootstrapDialog.alert('Data Base invalida ! Selecione a data de um movimento em Aberto');
                                        controle = 0;
                                    }
                                }
                            });
                        }

                        if (controle == 1) {
                            var url = root + '/Contaspagar/post_lista';
                            var data = {
                                dtBase: moment($('#dtBase').val(), "DD/MM/YYYY").format('YYYY-MM-DD'),
                                movimento: $('#movimento').val(),
                                tipoPagamento: $('#tipoPagamento').val(),
                                situacao: $('#situacao').val(),
                                numeroDocumento: $('#numeroDocumento').val(),
                                id: contas.join(',')
                            }

                            $.post(url, data, function (txt) {
                                grid.bootgrid('reload');
                            });
                        }
                    }
                }
            });
        });
    });

</script>