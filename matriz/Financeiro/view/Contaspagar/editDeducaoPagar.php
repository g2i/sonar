<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
    .has-error .select2-selection {
        border: 1px solid #a94442;
        border-radius: 4px;
    }
</style>
<form id="frmDeducao">
    <input type="hidden" value="<?php echo $DeducaoPagar->contaspagar_id ?>" name="contaspagar_id"
           id="contaspagar_id">
    <input type="hidden" value="<?php echo $DeducaoPagar->id ?>" name="id" id="id">

    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="deducoes_id">Dedu&ccedil;&atilde;o</label>
            <select name="deducoes_id" class="form-control" id="deducoes_id">
                <?php
                foreach ($Deducao as $d) {
                    if ($d->id == $DeducaoPagar->deducoes_id) {
                        echo '<option selected value="' . $d->id . '">' . $d->nome . '</option>';
                    } else {
                        echo '<option value="' . $d->id . '">' . $d->nome . '</option>';
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">
            <label for="valor">Valor</label>
            <input type="text" id="valor" name="valor" class="form-control money2"
                   value="<?php echo $DeducaoPagar->valor ?>">
        </div>
    </div>
</form>
<div class="clearfix"></div>