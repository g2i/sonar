<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contas a Pagar</h2>
        <ol class="breadcrumb">
            <li>Pagar</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Data</strong>: <?php echo convertDataSQL4BR($Contaspagar->data); ?></p>

                <p><strong>Vencimento</strong>: <?php echo convertDataSQL4BR($Contaspagar->vencimento); ?></p>

                <p><strong>Valor</strong>: <?php echo number_format($Contaspagar->valor, 2, ',', '.'); ?></p>

                <p><strong>Juros</strong>: <?php echo $Contaspagar->juros . '%'; ?></p>

                <p><strong>Multa</strong>: <?php echo number_format($Contaspagar->multa, 2, ',', '.'); ?></p>

                <p><strong>ValorPago</strong>: <?php echo number_format($Contaspagar->valorPago, 2, ',', '.'); ?></p>

                <p><strong>DataPagamento</strong>: <?php echo convertDataSQL4BR($Contaspagar->dataPagamento); ?></p>

                <p><strong>Complemento</strong>: <?php echo $Contaspagar->complemento; ?></p>

                <p>
                    <strong>Planocontas</strong>:
                    <?php
                    echo $this->Html->getLink($Contaspagar->getPlanocontas()->nome, 'Planocontas', 'view',
                        array('id' => $Contaspagar->getPlanocontas()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Fornecedor</strong>:
                    <?php
                    echo $this->Html->getLink($Contaspagar->getFornecedor()->nome, 'Fornecedor', 'view',
                        array('id' => $Contaspagar->getFornecedor()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Contaspagar->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Contaspagar->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>