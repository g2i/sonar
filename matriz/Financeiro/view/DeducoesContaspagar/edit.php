<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Deduções Contas a Pagar</h2>
        <ol class="breadcrumb">
            <li>deduções contas pagar</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form method="post" role="form"
                      action="<?php echo $this->Html->getUrl('DeducoesContaspagar', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group">
                            <label for="valor">Valor</label>
                            <input type="number" step="0,01" name="valor" id="valor" class="form-control"
                                   value="<?php echo $DeducoesContaspagar->valor ?>" placeholder="Valor">
                        </div>
                        <div class="form-group">
                            <label for="deducoes_id">Deducoes</label>
                            <select name="deducoes_id" class="form-control" id="deducoes_id">
                                <?php
                                foreach ($Deducoes as $d) {
                                    if ($d->id == $DeducoesContaspagar->deducoes_id)
                                        echo '<option selected value="' . $d->id . '">' . $d->nome . '</option>';
                                    else
                                        echo '<option value="' . $d->id . '">' . $d->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="contaspagar_id">Contaspagar</label>
                            <select name="contaspagar_id" class="form-control" id="contaspagar_id">
                                <?php
                                foreach ($Contaspagares as $c) {
                                    if ($c->id == $DeducoesContaspagar->contaspagar_id)
                                        echo '<option selected value="' . $c->id . '">' . $c->id . '</option>';
                                    else
                                        echo '<option value="' . $c->id . '">' . $c->id . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $DeducoesContaspagar->id; ?>">

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('DeducoesContaspagar', 'all') ?>"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>