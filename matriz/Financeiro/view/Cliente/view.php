<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Clientes</h2>
        <ol class="breadcrumb">
            <li>cliente</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Nome</strong>: <?php echo $Cliente->nome; ?></p>

                <p><strong>Telefone</strong>: <?php echo $Cliente->telefone; ?></p>

                <p><strong>Celular</strong>: <?php echo $Cliente->celular; ?></p>

                <p><strong>Cep</strong>: <?php echo $Cliente->cep; ?></p>

                <p><strong>Rua</strong>: <?php echo $Cliente->endereco; ?></p>

                <p><strong>Cidade</strong>: <?php echo $Cliente->cidade; ?></p>

                <p><strong>Uf</strong>: <?php echo $Cliente->uf; ?></p>

                <p><strong>Bairro</strong>: <?php echo $Cliente->bairro; ?></p>

                <p><strong>Numero</strong>: <?php echo $Cliente->numero; ?></p>

                <p><strong>Complemento</strong>: <?php echo $Cliente->complemento; ?></p>

                <p><strong>Cpf</strong>: <?php echo $Cliente->cpf; ?></p>

                <p><strong>Cnpj</strong>: <?php echo $Cliente->cnpj; ?></p>

                <p><strong>Email</strong>: <?php echo $Cliente->email; ?></p>

                <p><strong>Email de Cobrança</strong>: <?php echo $Cliente->email_cobranca; ?></p>

                <p><strong>RG</strong>: <?php echo $Cliente->rg; ?></p>

                <p><strong>Observação</strong>: <?php echo $Cliente->observacao; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Cliente->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Cliente->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>