<style>
    .col-opcoes{
        min-width: 70px; !important;
        max-width: 70px; !important;
        width: 70px; !important;
    }
    tr > td > button {
        margin-left: 2px;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Clientes</h2>
        <ol class="breadcrumb">
            <li>cliente</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                    <!-- tabela de resultados -->
                    <div class="row clearfix">
                        <div class="table-responsive">
                            <table id="tblClientes" class="table table-hover">
                                <thead>
                                <tr>
                                    <th data-column-id="id" data-identifier="true" data-visible="false"
                                        data-visible-in-selection="false">Id</th>
                                    <th data-column-id="nome">Nome</th>
                                    <th data-column-id="telefone">Telefone</th>
                                    <th data-column-id="celular">Celular</th>
                                    <th data-column-id="email">E-mail</th>
                                    <th data-column-id="status">Status</th>
                                    <th data-column-id="opcoes" data-formatter="opcoes"
                                        data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                        data-sortable="false"></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        var grid = $('#tblClientes').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: [30, 50, 100],
            ajax: true,
            url: root + '/Cliente/getClientes',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            formatters:{
                opcoes: function(column, row){
                    return  '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Endereco" class="btn btn-xs btn-primary command-endereco" data-row-id="' + row.id + '"><span class="fa fa-home"></span></button>' +
                            '<button type="button" data-toggle="tooltip" data-placement="top" title="Apagar" class="btn btn-xs btn-danger command-delete" data-row-id="' + row.id + '"><span class="fa fa-trash-o"></span></button>';
                }
            },
            labels: {
                search: 'Procura por nome'
            },
            templates:{
                search: '<div class="{{css.search}} pull-left" style="width: 350px"><div class="input-group"><span class="{{css.icon}} input-group-addon {{css.iconSearch}}"></span><input type="text" class="{{css.searchField}}" placeholder="{{lbl.search}}" /></div></div>' +
                        '<button id="btnNovo" class="{{css.search}} btn btn-primary" style="width: auto" type="button"><span class="glyphicon glyphicon-plus-sign"></span> Cadastrar Cliente</button>',
            },
            requestHandler: function (request) {
                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                return request;
            }
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {
            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-endereco").unbind('click');
            grid.find(".command-endereco").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Cliente_endereco/all'
                document.location = url;
            });
            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var conta = $(this).data("row-id");
                var url = root + '/Cliente/edit?id=' + conta;
                document.location = url;
            });

            grid.find(".command-delete").unbind('click');
            grid.find(".command-delete").on("click", function(e) {
                var conta = $(this).data("row-id");

                BootstrapDialog.confirm({
                    title: 'Aviso',
                    message: 'Voc\u00ea tem certeza ?',
                    type: BootstrapDialog.TYPE_WARNING,
                    closable: false,
                    draggable: false,
                    btnCancelLabel: 'N\u00e3o desejo excluir!',
                    btnOKLabel: 'Sim desejo excluir!',
                    btnOKClass: 'btn-warning',
                    callback: function(result) {
                        if(result) {
                            var url = root + '/Cliente/delete';
                            var data = {
                                id: conta
                            }

                            $.post(url, data, function(ret){
                                if(ret.result){
                                    BootstrapDialog.success(ret.msg);
                                    grid.bootgrid('reload');
                                }else{
                                    BootstrapDialog.warning(ret.msg);
                                }
                            });
                        }
                    }
                });
            });

            $('#btnNovo').unbind('click');
            $('#btnNovo').click(function(e){
                var url = root + '/Cliente/add';
                document.location = url;
            });
        });

    });
</script>