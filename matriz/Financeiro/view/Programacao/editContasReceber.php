<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">

                <div class="col-md-12">
                    <div class="row pull-right">
                        <p class="text-right ">
                            <?php
                            echo $this->Html->getLink('<span class="glyphicon centro_view"></span> Rateio ', 'Rateio_programacao', 'all', array('programacao' => $Programacao->id, 'modal' => 1, 'ajax' => true, 'first' => 1), array('class' => 'btn btn-info btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-modal-lg'));
                            ?>
                        </p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <form method="post" role="form"
                      action="<?php echo $this->Html->getUrl('Programacao', 'editContasReceber') ?>" id="formGerar">
                    <input type="hidden" name="id" id="id" value="<?php echo $Programacao->id ?>">

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="valorParcela">Valor da Parcela</label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="valorParcela" id="valorParcela"
                                       value='<?php echo $Programacao->valorParcela ?>' class="form-control money2"
                                       placeholder="Valor da Parcela">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idPlanoContas">Plano de contas</label>
                            <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas">
                                <option value="0">Selecione</option>
                                <?php
                                foreach ($Planocontas as $p) {
                                    if ($p->id == $Programacao->idPlanoContas) {
                                        echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                                    } else {
                                        echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoDocumento">Tipo de Documento</label>
                            <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento">
                                <option value="">Selecione</option>
                                <?php
                                foreach ($tipoDocumento as $t) {
                                    if ($t->id == $Programacao->tipoDocumento)
                                        echo '<option selected value="' . $t->id . '">' . $t->descricao . '</option>';
                                    else
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="numero_documento" >Número documento</label>
                            <input type="text" name="numero_documento" id="numero_documento" class="form-control " placeholder="Número do documento" value='<?php echo $Programacao->numero_documento ?>'>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoPagamento">Forma de Pagamento</label>
                            <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento">
                                <option value="">Selecione</option>
                                <?php
                                foreach ($tipoPagamento as $t) {
                                    if ($t->id == $Programacao->tipoPagamento)
                                        echo '<option selected value="' . $t->id . '">' . $t->descricao . '</option>';
                                    else
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idCliente">Cliente</label>
                            <select id="idCliente" name="idCliente" class="form-control clientes-ajax" required>
                                <?php if (!empty($clienteSelect)) echo '<option value="' . $clienteSelect->id . '">' . $clienteSelect->nome . '</option>'; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Primeiro Vencimento<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="primeiro_vencimento" id="primeiro_vencimento" value='<?php echo $Programacao->primeiro_vencimento ?>' required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Data do Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="data_documento" id="data_documento" value='<?php echo $Programacao->data_documento ?>' required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="diaVencimento">Dia do Vencimento</label>
                            <input type="number" name="diaVencimento"
                                   value='<?php echo $Programacao->diaVencimento ?>' id="diaVencimento"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="periodicidade">Periodicidade</label>
                            <select id="periodicidade" name="periodicidade selectPicker" class="form-control">
                                <?php
                                foreach ($Periodicidade as $p) {
                                    if ($p->id == $Programacao->periodicidade)
                                        echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                                    else
                                        echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="contabilidade">Empresas</label>
                            <select name="contabilidade" class="form-control selectPicker" id="contabilidade">
                                <option value="0">Selecione</option>
                                <?php
                                foreach ($Contabilidades as $c) {
                                    if ($c->id == $Programacao->contabilidade)
                                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                    else
                                        echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="prazoDeterminado">Prazo Determinado?</label>
                        <input <?php echo (!empty($Programacao->prazoDeterminado)) ? "checked" : "" ?> type='radio'
                                                                                                       name='prazoDeter'
                                                                                                       value='1'>
                        Sim <input <?php echo (empty($Programacao->prazoDeterminado)) ? "checked" : "" ?>
                            type='radio' name='prazoDeter' value='0'> N&atilde;o
                        <div
                            id='prazoDeter' <?php echo (empty($Programacao->prazoDeterminado)) ? "style='display: none'" : "" ?>>
                            <input type="number" name="prazoDeterminado"
                                   value='<?php echo $Programacao->prazoDeterminado ?>' id="prazoDeterminado"
                                   class="form-control" placeholder="Número de Parcelas">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="correcaoMonetaria">Corre&ccedil;&atilde;o Monet&aacute;ria</label>
                            <select name="correcaoMonetaria" class="form-control" id="correcaoMonetaria">
                                <?php
                                foreach ($correcaoMonetaria as $c) {
                                    if ($c->id == $Programacao->correcaoMonetaria)
                                        echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                    else
                                        echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="row text-right pull-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Adicionar Dedu&ccedil;&atilde;o', 'Programacao', 'addDeducaoReceber', array('programacao' => $Programacao->id), array('class' => 'btn btn-primary', "data-toggle" => 'modal')); ?></p>
                        </div>
                        <!-- tabela de resultados -->
                        <div class="row clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Dedu&ccedil;&atilde;o</th>
                                        <th>Percentual</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <?php
                                    foreach ($Deducao as $d) {
                                        echo "<tr>";
                                        echo "<td>" . $d->getDeducoes()->nome . "</td>";
                                        echo "<td>" . $d->percentual . '%' . "</td>";
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Programacao', 'editDeducaoReceber', array('id' => $d->id), array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';
                                        echo '<td width="50">';
                                        //echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Contasreceber', 'delete', array('id' => $d->id), array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';
                                        echo "</tr>";
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                                <textarea name="complemento" id="complemento" class="form-control"><?php echo $Programacao->complemento ?></textarea>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Programacao', 'contasReceber') ?>"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('input[name=prazoDeter]').change(function () {
            if ($('input[name=prazoDeter]:checked').val() == 1) {
                $('#prazoDeter').show(500);
            } else {
                $('#prazoDeterminado').val("");
                $('#prazoDeter').hide(500);
            }
        });
    });
</script>