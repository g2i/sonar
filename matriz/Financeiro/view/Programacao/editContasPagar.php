<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="post" role="form"
              action="<?php echo $this->Html->getUrl('Programacao', 'editContasPagar') ?>" id="formGerar">
            <input type="hidden" name="formulario" value="gerar">

            <div class="ibox">
                <div class="ibox-title">
                    <h2>Informações do Pagamento</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-md-12">
                        <div class="row pull-right">
                            <p class="text-right ">
                                <?php
                                echo $this->Html->getLink('<span class="glyphicon centro_view"></span> Rateio ', 'Rateio_programacao', 'all', array('programacao' => $Programacao->id, 'modal' => 1, 'ajax' => true, 'first' => 1), array('class' => 'btn btn-info btn-sm', 'data-toggle' => 'modal', 'data-target' => '.bs-modal-lg'));
                                ?>
                            </p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="tipoDocumento">Tipo de Documento</label>
                                    <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento">
                                        <option></option>
                                        <?php foreach ($tipoDocumento as $t):
                                            $aux = $t->id == $Programacao->tipoDocumento ? ' selected' : ''; ?>
                                            <option <?php echo $aux ?>
                                                    value="<?php echo $t->id ?>"><?php echo $t->descricao ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="numero_documento">Número documento</label>
                                    <input type="text" name="mumero_documento" id="mumero_documento"
                                           class="form-control "
                                           placeholder="Número do documento"
                                           value="<?= $Programacao->mumero_documento ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label class="required" for="data">Data do Documento <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <div class='input-group date'>
                                        <input type='text' class="form-control dateFormat" name="data_documento"
                                               id="data_documento" value='<?php echo $Programacao->data_documento ?>'
                                               required>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="idPlanoContas">Plano de contas</label>
                                    <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas">
                                        <option></option>
                                        <?php foreach ($Planocontas as $p):
                                            $aux = $p->id == $Programacao->idPlanoContas ? ' selected' : ''; ?>
                                            <option <?php echo $aux ?>
                                                    value="<?php echo $p->id ?>"><?php echo $p->nome ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="tipoPagamento">Forma de Pagamento</label>
                                    <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento">
                                        <option></option>
                                        <?php foreach ($tipoPagamento as $t):
                                            $aux = $t->id == $Programacao->tipoPagamento ? ' selected' : ''; ?>
                                            <option <?php echo $aux ?>
                                                    value="<?php echo $t->id ?>"><?php echo $t->descricao ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="valorParcela">Valor da Parcela</label>
                                    <div class='input-group'>
                                        <span class="input-group-addon">R$</span>
                                        <input type="text" name="valorParcela" id="valorParcela"
                                               value='<?php echo $Programacao->valorParcela ?>'
                                               class="form-control money2"
                                               placeholder="Valor da Parcela">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="idFornecedor">Credor</label>
                                    <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax"
                                            required>
                                        <?php if (!empty($forncedorSelect)) echo '<option value="' . $forncedorSelect->id . '">' . $forncedorSelect->nome . '</option>'; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label for="contabilidade">Empresas</label>
                                    <select name="contabilidade" class="form-control selectPicker" id="contabilidade">
                                        <option></option>
                                        <?php foreach ($Contabilidades as $c):
                                            $aux = $c->id == $Programacao->contabilidade ? ' selected' : ''; ?>
                                            <option <?php echo $aux ?>
                                                    value="<?php echo $c->id ?>"><?php echo $c->nome ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                            <textarea name="complemento" id="complemento"
                                      class="form-control" rows="12"><?php echo $Programacao->complemento ?></textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-title">
                    <h2>Periodo de Pagamento</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-xs-12 col-md-12">
                        <div class="row text-right pull-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Adicionar Dedu&ccedil;&atilde;o', 'Programacao', 'addDeducaoPagar', array('programacao' => $Programacao->id), array('class' => 'btn btn-primary', "data-toggle" => 'modal')); ?></p>
                        </div>
                        <!-- tabela de resultados -->
                        <div class="row clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Dedu&ccedil;&atilde;o</th>
                                        <th>Valor</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <?php
                                    foreach ($Deducao as $d) {
                                        echo "<tr>";
                                        if ($Programacao->id == $d->programacao_id) {
                                            echo "<td>" . $d->getDeducoes()->nome . "</td>";
                                            echo "<td>" . number_format($d->valor, 2, ',', '.') . "</td>";
                                            echo '<td width="50">';
                                            echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Programacao', 'editDeducaoPagar', array('id' => $d->id), array('class' => 'btn btn-warning btn-sm', 'data-toggle' => 'modal'));
                                            echo '</td>';
                                            echo '<td width="50">';
                                            //echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Contasreceber', 'delete', array('id' => $d->id), array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                            echo '</td>';
                                        }
                                        echo "</tr>";
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" value="<?php echo $Programacao->id; ?>">
                    <div style="clear:both;"></div>

                </div>
            </div>

            <div class="ibox ">
                <div class="ibox-title" style="border-color: #00a65a;">
                    <h2>Projeto</h2>
                </div>
                <div class="ibox-content">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Grupo <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_grupo_id" name="fin_projeto_grupo_id"
                                            class="form-control selectPicker"
                                            required>
                                        <option>Selecione Um Grupo</option>
                                        <?php
                                        foreach ($Fin_projeto_grupo as $c) {
                                            if ($c->id == $Fin_projeto->grupo_id)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Empresa <span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_empresa_id" name="fin_projeto_empresa_id"
                                            class="form-control selectPicker"
                                            required>
                                        <?php
                                        foreach ($Fin_projeto_empresa as $c) {
                                            if ($c->id == $Fin_projeto->empresa_id)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="required" for="contabilidade">Unidade<span
                                                class="glyphicon glyphicon-asterisk"></span></label>
                                    <select id="fin_projeto_unidade_id" name="fin_projeto_unidade_id"
                                            class="form-control selectPicker"
                                            required>
                                        <?php
                                        foreach ($Fin_projeto_unidade as $c) {
                                            if ($c->id == $Fin_projeto->unidade_id)
                                                echo '<option selected value="' . $c->id . '">' . $c->nome . '</option>';
                                            else
                                                echo '<option  value="' . $c->id . '">' . $c->nome . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Programacao', 'contasPagar') ?>"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </div>
            </div>

        </form>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {

        $('#fin_projeto_grupo_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_empresa', 'getListaEmpresa') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_empresa_id').prop( "disabled", true );
            $('#fin_projeto_unidade_id').prop( "disabled", true );
            $.get(url, {fin_projeto_grupo_id:id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_empresa_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').html('').trigger('change.select2');
                $('#fin_projeto_empresa_id').prop( "disabled", false );
                $('#fin_projeto_unidade_id').prop( "disabled", false );
            }, 'json');
        });

        $('#fin_projeto_empresa_id').on('change', function () {
            var url = '<?= $this->Html->getUrl('Fin_projeto_unidade', 'getListaUnidade') ?>';
            var html = '<option><option>';
            var id = $(this).val();
            $('#fin_projeto_unidade_id').prop( "disabled", true );
            $.get(url, {fin_projeto_empresa_id:id}, function (data) {
                $.each(data, function (key, value) {
                    html += '<option value="' + value.id + '">' + value.nome + '<option>'
                });
                $('#fin_projeto_unidade_id').html(html).trigger('change.select2');
                $('#fin_projeto_unidade_id').prop( "disabled", false );
            }, 'json');
        });

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green'
        });

        $('input[name=prazoDeter]').change(function () {
            if ($('input[name=prazoDeter]:checked').val() == 1) {
                $('#prazoDeter').show(500);
            } else {
                $('#prazoDeterminado').val("");
                $('#prazoDeter').hide(500);
            }
        });

        $('input[name=fixarDia]').on('ifChanged', function () {
            if ($('input[name=fixarDia]:checked').val() == 1) {
                $('#fixarDia').show(500);
            } else {
                $('#diaVencimento').val('');
                $('#fixarDia').hide(500);
            }
        });
    });

</script>