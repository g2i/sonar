<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Adicionar Dedução</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="well well-lg" style="border-top:none">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Programacao', 'editDeducaoPagar') ?>">
                        <input type="hidden" value="<?php echo $DeducaoProgramacao->programacao_id ?>"
                               name="programacao_id" id="programacao_id">
                        <input type="hidden" value="<?php echo $DeducaoProgramacao->id ?>" name="id" id="id">

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="deducoes_id">Dedu&ccedil;&atilde;o</label>
                                <select name="deducoes_id" class="form-control" id="deducoes_id">
                                    <?php
                                    foreach ($Deducao as $d) {
                                        if ($d->id == $DeducaoProgramacao->deducoes_id) {
                                            echo '<option selected value="' . $d->id . '">' . $d->nome . '</option>';
                                        } else {
                                            echo '<option value="' . $d->id . '">' . $d->nome . '</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="valor">Valor</label>
                                <input type="text" id="valor" name="valor" class="form-control money2"
                                       value="<?php echo $DeducaoProgramacao->valor ?>">
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Programacao', 'editContasPagar/id:' . $_GET['id']) ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="Salvar">
                            </div>
                        </div>
                    </form>
                    <div style="clear:both">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.money2').mask("#.##0,00", {reverse: true, maxlength: false});
        });
    </script>