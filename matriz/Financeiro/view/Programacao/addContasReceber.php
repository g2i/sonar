<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form method="post" role="form"
                      action="<?php echo $this->Html->getUrl('Programacao', 'addContasReceber') ?>" id="formGerar">
                    <input type="hidden" id="deducao" name="deducao">

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="valorParcela" class="required">Valor da Parcela <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group'>
                                <span class="input-group-addon">R$</span>
                                <input type="text" name="valorParcela" id="valorParcela"
                                       value="0.00" class="form-control money2"
                                       placeholder="Valor da Parcela" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="idPlanoContas" class="required">Plano de contas <span
                                    class="glyphicon glyphicon-asterisk"></span> </label>
                            <select name="idPlanoContas" class="form-control selectPicker" id="idPlanoContas" required>
                                <option value="0">Selecione</option>
                                <?php
                                foreach ($Planocontas as $p) {
                                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoDocumento" class="required">Tipo de Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="tipoDocumento" class="form-control selectPicker" id="tipoDocumento" required>
                                <option value="0">Selecione</option>
                                <?php
                                foreach ($tipoDocumento as $t) {
                                    if ($Config_contas->documento_receber == $t->id) {
                                        echo '<option value="' . $t->id . '" selected>' . $t->descricao . '</option>';
                                    } else {
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="numero_documento">Número documento</label>
                            <input type="text" name="mumero_documento" id="mumero_documento" class="form-control "
                                   placeholder="Número do documento">
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="tipoPagamento">Forma de Pagamento</label>
                            <select name="tipoPagamento" class="form-control selectPicker" id="tipoPagamento">
                                <option value="0">Selecione</option>
                                <?php
                                foreach ($tipoPagamento as $t) {
                                    if ($Config_contas->pagamento_receber == $t->id) {
                                        echo '<option value="' . $t->id . '" selected>' . $t->descricao . '</option>';
                                    } else {
                                        echo '<option value="' . $t->id . '">' . $t->descricao . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="idCliente">Cliente <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="idCliente" name="idCliente" class="form-control clientes-ajax" required>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Primeiro Vencimento<span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="primeiro_vencimento" id="primeiro_vencimento" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label class="required" for="data">Data do Documento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <div class='input-group date'>
                                <input type='text' class="form-control dateFormat" name="data_documento" id="data_documento" required>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="periodicidade" class="required">Periodicidade <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select id="periodicidade" name="periodicidade" class="form-control selectPicker">
                                <option value="">Selecione:</option>
                                <?php
                                foreach ($Periodicidade as $p) {
                                    echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="diaVencimento" class="required">Dia do Vencimento <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="text" name="diaVencimento"
                                   value='01' id="diaVencimento"
                                   class="form-control" required>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6" id="divMesVencimento" style="visibility:hidden;">
                        <div class="form-group">
                            <label for="mesVencimento">Mês do Vencimento</label>
                            <input type="text" value='01' name="mesVencimento" max="12" id="mesVencimento"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="contabilidade" class="required">Empresas <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="contabilidade" class="form-control selectPicker" id="contabilidade" required>
                                <option value="0">Selecione</option>
                                <?php
                                foreach ($Contabilidades as $c) {
                                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="correcaoMonetaria" class="required">Corre&ccedil;&atilde;o Monet&aacute;ria
                                <span class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="correcaoMonetaria" class="form-control selectPicker" id="correcaoMonetaria">
                                <?php
                                foreach ($correcaoMonetaria as $c) {
                                    echo '<option value="' . $c->id . '">' . $c->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <label for="prazoDeterminado">Prazo Determinado?</label> <input type='radio'
                                                                                        name='prazoDeter' value='1'>
                        Sim <input checked type='radio' name='prazoDeter' value='0'> N&atilde;o
                        <div id='prazoDeter' style='display:none'>
                            <input type="number" name="prazoDeterminado"
                                   value='0' id="prazoDeterminado"
                                   class="form-control" placeholder="Número de Parcelas">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label for="complemento">Observa&ccedil;&atilde;o</label>
                            <textarea name="complemento" id="complemento"
                                      class="form-control"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="rateio_automatico" id="rateio_automatico"/>

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Programacao', 'contasReceber') ?>"
                           class="btn btn-default" data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" id="enviar" value="salvar"
                               onclick="return salvar()">
                    </div>
                    <div style="clear:both;"></div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="primeraParcela">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Contas a Receber</h4>
            </div>
            <div class="modal-body">
                <p>Essa programação possui retenções de impostos ?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="alteraDt('1')">Sim</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="alteraDt('0')">Não</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<input type="hidden" id="auxx" value="0"/>
<script type="text/javascript">
    $(document).ready(function () {
        $('#periodicidade').change(function () {
            if ($(this).find('option:selected').val() == '4') {
                $('#divMesVencimento').css('visibility', 'visible');
            } else {
                $('#divMesVencimento').css('visibility', 'hidden');
                $('#mesVencimento').val('');
            }
        });

        $('#diaVencimento').inputmask("d", {"placeholder": "dd"});
        $('#mesVencimento').inputmask("m", {"placeholder": "mm"});

        $("#diaVencimento").keyup(function () {
            if ($(this).val() > 30) {
                BootstrapDialog.alert("O valor nao pode ser superior a 30");
                $(this).val('30');
            }
        })

        $('input[name=prazoDeter]').change(function () {
            if ($('input[name=prazoDeter]:checked').val() == 1) {
                $('#prazoDeter').show(500);
            } else {
                $('#prazoDeter').hide(500);
            }
        });
    });

    function salvar() {
        if ($("#auxx").val() == 0) {
            $('#primeraParcela').modal('show');
            $("#auxx").val(1);
            return false;
        } else {
            return true;
        }
        if ($("#periodicidade").val() == 4) {
            if ($('#mesVencimento').val() == "") {
                OpenMensagem("Alerta", "Selecione um mes de vencimento!");
                return false;
            }
        }
    }
    function alteraDt(resp) {
        $('#deducao').val(resp);
        $('#enviar').click();
    }
    $("#idPlanoContas").change(function () {
        $.ajax({
            type: "POST",
            url: root + "/Planocontas/automatico",
            data: "id=" + $(this).val(),
            success: function (txt) {
                if (txt == 1) {
                    $("#rateio_automatico").val(1);
                } else {
                    $("#rateio_automatico").val(2);
                }
            }
        });
    });

</script>