<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Programação</h2>
        <ol class="breadcrumb">
            <li>programação</li>
            <li class="active">
                <strong>Adicionar dedução</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <div class="well well-lg" style="border-top:none">
                    <form method="post" role="form"
                          action="<?php echo $this->Html->getUrl('Programacao', 'addDeducaoPagar') ?>">
                        <input type="hidden" value="<?php echo $_GET['programacao'] ?>" name="programacao_id"
                               id="programacao_id">

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="deducoes_id">Dedu&ccedil;&atilde;o</label>
                                <select name="deducoes_id" class="form-control" id="deducoes_id">
                                    <?php
                                    foreach ($Deducao as $d) {
                                        echo '<option selected value="' . $d->id . '">' . $d->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                <label for="valor">Valor</label>
                                <div class='input-group'>
                                    <span class="input-group-addon">R$</span>
                                    <input type="text" id="valor" name="valor" class="form-control money2">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('Programacao', 'editContasPagar/id:' . $_GET['programacao']) ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                        </div>
                    </form>

                    <div style="clear:both">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">


        function salvar() {
            $.ajax({
                type: "POST",
                url: "<?php echo SITE_PATH.'/?m=Programacao&p=post_salvarDeducaoPagar'; ?>",
                data: "deducoes_id=" + $('#deducoes_id').val() + "&valor=" + $('#valor').val() + "&programacao_id=" + $('#programacao_id').val(),
                success: function (txt) {
                    if (txt == '1') {
                        alert("Salvo com sucesso!");
                    } else {
                        alert("Erro!");
                    }

                }
            });

        }
    </script>
