<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Graficos</h2>
        <ol class="breadcrumb">
            <li>Graficós</li>
            <li class="active">
                <strong>Resultado Geral</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top: 20px">
        <form role="form" id="form" class="col-md-6 col-md-offset-6">
            <div class="form-group col-md-6">
                <label for="grupos">Grupos</label>
                <div class="input-group select2-bootstrap-append">
                    <select name="grupos" class="form-control" id="grupos">
                        <option></option>
                        <?php foreach ($grupos as $e): ?>
                            <option value="<?php echo $e->id ?>"><?php echo $e->descricao ?></option>
                        <?php endforeach; ?>
                    </select>
                 <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="grupos"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                 </span>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="empresa">Empresa</label>
                <div class="input-group select2-bootstrap-append">
                    <select name="empresa" class="form-control" id="empresa"></select>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="empresa"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <small style="font-weight: bold">Resultado Financeiro</small>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row" style="border-bottom:1px solid #E5E5E5;">
                        <div class="col-xs-6 col-md-3">
                            <label for="ano">Ano</label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="ano" name="ano" class="form-control"
                                        data-placeholder="Atual">
                                    <option></option>
                                    <?php
                                    for ($i = date('Y', strtotime("-1 year")); $i >= date("Y", strtotime("-5 year")); $i--) {
                                        echo "<option value='" . $i . "'>" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-clear="ano"
                                            data-toggle="tooltip" data-placement="top" title="Ano Atual">
                                        <i class="fa fa-calendar-check-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="rstChartRow" class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function updateFinanceiro(){
        var chart = $('#rstChartRow');
        chart.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        var grupo = $('#grupos').val();
        if(!grupo){
            chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            return;
        }

        var empresas = '';
        if($('#empresa').val()){
            empresas = $('#empresa').val().join(',');
        }

        var url =  root+'/Grafico/getFinanceiro'
        var data = {
            ano: $('#ano').val(),
            grupo: grupo,
            empresas: empresas
        }

        $.get(url, data, function(ret) {
            if (!ret.result) {
                chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            var chartData = {
                data: {
                    labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    datasets: [{
                        label: "Resultado",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(223, 214, 0, 0.8)',
                        backgroundColor: 'rgba(223, 214, 0, 0.8)',
                        hoverBorderColor: 'rgba(223, 214, 0, 1)',
                        hoverBackgroundColor: 'rgba(223, 214, 0, 1)',
                        borderWidth: 1
                    },{
                        label: "Entradas",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(40, 152, 44, 0.8)',
                        backgroundColor: 'rgba(40, 152, 44, 0.8)',
                        hoverBorderColor: 'rgba(40, 152, 44, 1)',
                        hoverBackgroundColor: 'rgba(40, 152, 44, 1)',
                        borderWidth: 1,
                    },{
                        label: "Saidas",
                        fill: false,
                        data: [],
                        borderColor: 'rgba(215, 44, 44, 0.8)',
                        backgroundColor: 'rgba(215, 44, 44, 0.8)',
                        hoverBorderColor: 'rgba(215, 44, 44, 1)',
                        hoverBackgroundColor: 'rgba(215, 44, 44, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    tooltips: {
                        mode: 'label'
                    },
                    hover: {
                        mode: 'dataset'
                    }
                }
            };
            ret.dados.forEach(function(dados) {
                chartData.data.datasets[0].data.push(dados.resultado);
                chartData.data.datasets[1].data.push(dados.entrada);
                chartData.data.datasets[2].data.push(dados.saida);
            });

            var ctx = $('<canvas></canvas>');
            chart.html('');
            chart.append(ctx);
            var barChart = Chart.Bar(ctx, chartData);
            barChart.update();
        });
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();

        var grupos = $('#grupos').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione um grupo'
        });

        var empresa = $('#empresa').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            multiple: true,
            placeholder: 'Selecione as empresas'
        });

        var ano = $('#ano').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Atual'
        });

        grupos.change(function(){
            if($(this).val() == '') {
                empresa.select2('destroy');
                empresa.val('');
                empresa.html('');
                var option = $('<option></option>');
                empresa.append(option);
                empresa.select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    multiple: true,
                    placeholder: 'Selecione as empresas'
                });
            }
            else {
                var url = root + '/Index/getEmpresas';
                var data = {
                    id: $(this).val()
                }

                $.get(url, data, function (ret) {
                    if (ret.result) {
                        empresa.select2('destroy');
                        empresa.html('');
                        ret.dados.forEach(function (dado) {
                            var option = $('<option value="' + dado.id + '" >' + dado.nome + '</option>');
                            empresa.append(option);
                        });

                        empresa.select2({
                            language: 'pt-BR',
                            theme: 'bootstrap',
                            width: '100%',
                            multiple: true,
                            placeholder: 'Selecione as empresas'
                        });
                    } else {
                        BootstrapDialog.alert(ret.msg);
                        console.log(ret.error);
                    }
                });
            }

            updateFinanceiro();
        });

        empresa.change(function(){
            updateFinanceiro();
        });

        ano.change(function(){
            updateFinanceiro();
        });

        $("button[data-select2-clear]").click(function() {
            $("#" + $(this).data("select2-clear")).val('').trigger('change');
        });

    });
</script>