<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Graficos</h2>
        <ol class="breadcrumb">
            <li>Graficos</li>
            <li class="active">
                <strong>Resultado Específico</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12 col-md-12 col-xs-12" style="padding-top: 20px">
        <form role="form" id="form" class="col-md-6 col-md-offset-6">
            <div class="form-group col-md-6">
                <label for="grupos">Grupos</label>
                <div class="input-group select2-bootstrap-append">
                    <select name="grupos" class="form-control" id="grupos">
                        <option></option>
                        <?php foreach ($grupos as $e): ?>
                            <option value="<?php echo $e->id ?>"><?php echo $e->descricao ?></option>
                        <?php endforeach; ?>
                    </select>
                 <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="grupos"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                 </span>
                </div>
            </div>
            <div class="form-group col-md-6">
                <label for="empresa">Empresa</label>
                <div class="input-group select2-bootstrap-append">
                    <select name="empresa" class="form-control" id="empresa"></select>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="empresa"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <small style="font-weight: bold">Resultado Financeiro</small>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row" style="border-bottom:1px solid #E5E5E5;">
                        <div class="col-xs-6 col-md-3">
                            <label for="ano">Ano</label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="ano" name="ano" class="form-control"
                                        data-placeholder="Atual">
                                    <option></option>
                                    <?php
                                    for ($i = date('Y', strtotime("-1 year")); $i >= date("Y", strtotime("-5 year")); $i--) {
                                        echo "<option value='" . $i . "'>" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-clear="ano"
                                            data-toggle="tooltip" data-placement="top" title="Ano Atual">
                                        <i class="fa fa-calendar-check-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="chartRow1" class="row" style="margin-top: 5px">
                        <div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <small style="font-weight: bold">Resultado Financeiro</small>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row" style="border-bottom:1px solid #E5E5E5;">
                        <div class="col-xs-6 col-md-3">
                            <label for="anoc">Ano</label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="anoc" name="anoc" class="form-control"
                                        data-placeholder="Atual">
                                    <option></option>
                                    <?php
                                    for ($i = date('Y', strtotime("-1 year")); $i >= date("Y", strtotime("-5 year")); $i--) {
                                        echo "<option value='" . $i . "'>" . $i . "</option>";
                                    }
                                    ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-clear="ano"
                                            data-toggle="tooltip" data-placement="top" title="Ano Atual">
                                        <i class="fa fa-calendar-check-o"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label for="classi">Classificação</label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="classi">
                                    <option></option>
                                    <?php foreach ($classi as $c): ?>
                                        <option value="<?php echo $c->id ?>"><?php echo $c->descricao ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-clear="classi"
                                            data-toggle="tooltip" data-placement="top" title="Limpar">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div id="chartRow2" class="row">
                        <div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    function updateFinanceiro(){
        var chart = $('#chartRow1');
        chart.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        var grupo = $('#grupos').val();
        if(!grupo){
            chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            return;
        }

        var empresas = '';
        if($('#empresa').val()){
            empresas = $('#empresa').val().join(',');
        }

        var url =  root+'/Grafico/getFinEsp'
        var data = {
            ano: $('#ano').val(),
            grupo: grupo,
            empresas: empresas
        }

        $.get(url, data, function(ret) {
            if (!ret.result) {
                chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            var randomColorFactor = function() {
                return Math.round(Math.random() * 255);
            };
            var randomColor = function(opacity) {
                return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
            };

            var chartData = {
                data: {
                    labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    datasets: []
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom'
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    hover: {
                        mode: 'dataset',
                        label: {
                            FontSize: 8
                        },
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Mês'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Valor'
                            }
                        }]
                    }
                }
            };
            ret.dados.forEach(function(dados) {
                var dataset = {
                    label: dados.descricao,
                    fill: false,
                    data: [dados.jan, dados.fev, dados.mar, dados.abr, dados.maio, dados.jun,
                        dados.jul, dados.ago, dados.sete, dados.outu, dados.nov, dados.dez],
                    borderColor: randomColor('0.8'),
                    backgroundColor: randomColor('0.8'),
                    pointBorderColor: randomColor('1'),
                    pointBackgroundColor: randomColor('1'),
                };
                chartData.data.datasets.push(dataset);
            });

            var ctx = $('<canvas style="position: relative; z-index: 99999"></canvas>');
            chart.html('');
            chart.append(ctx);
            var lineChart = Chart.Line(ctx, chartData);
            lineChart.update();
        });
    }

    function updateClassi(){
        var chart = $('#chartRow2');
        chart.html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");

        var grupo = $('#grupos').val();
        if(!grupo){
            chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            return;
        }

        var classi = $('#classi').val();
        if(!classi){
            chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
            return;
        }

        var empresas = '';
        if($('#empresa').val()){
            empresas = $('#empresa').val().join(',');
        }

        var url =  root+'/Grafico/getFinEspMov'
        var data = {
            ano: $('#anoc').val(),
            grupo: grupo,
            empresas: empresas,
            classi: classi
        }

        $.get(url, data, function(ret) {
            if (!ret.result) {
                chart.html("<div class='text-center'><h3><i class='fa fa-info-circle'></i> Sem dados para exibir.</h3></div>");
                return;
            }

            var randomColorFactor = function() {
                return Math.round(Math.random() * 255);
            };
            var randomColor = function(opacity) {
                return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '.3') + ')';
            };

            var chartData = {
                data: {
                    labels: ['Jan', 'Fev', 'Mar', 'Abr', 'Maio', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                    datasets: []
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom'
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    hover: {
                        mode: 'dataset',
                        label: {
                            FontSize: 8
                        },
                    },
                    scales: {
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Mês'
                            }
                        }],
                        yAxes: [{
                            display: true,
                            scaleLabel: {
                                show: true,
                                labelString: 'Valor'
                            }
                        }]
                    }
                }
            };
            ret.dados.forEach(function(dados) {
                var dataset = {
                    label: dados.descricao,
                    fill: false,
                    data: [dados.jan, dados.fev, dados.mar, dados.abr, dados.maio, dados.jun,
                        dados.jul, dados.ago, dados.sete, dados.outu, dados.nov, dados.dez],
                    borderColor: randomColor('0.8'),
                    backgroundColor: randomColor('0.8'),
                    pointBorderColor: randomColor('1'),
                    pointBackgroundColor: randomColor('1'),
                };
                chartData.data.datasets.push(dataset);
            });

            var ctx = $('<canvas style="position: relative; z-index: 99999"></canvas>');
            chart.html('');
            chart.append(ctx);
            var lineChart = Chart.Line(ctx, chartData);
            lineChart.update();
        });
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();

        $("button[data-select2-clear]").click(function() {
            $("#" + $(this).data("select2-clear")).val('').trigger('change');
        });

        var grupos = $('#grupos').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione um grupo'
        });

        var empresa = $('#empresa').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            multiple: true,
            placeholder: 'Selecione as empresas'
        });

        var ano = $('#ano').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Atual'
        });

        var anoc = $('#anoc').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Atual'
        });

        var classi = $('#classi').select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione uma classificação'
        });

        grupos.change(function(){
            if($(this).val() == '') {
                empresa.select2('destroy');
                empresa.val('');
                empresa.html('');
                var option = $('<option></option>');
                empresa.append(option);
                empresa.select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    multiple: true,
                    placeholder: 'Selecione as empresas'
                });
            }
            else {
                var url = root + '/Index/getEmpresas';
                var data = {
                    id: $(this).val()
                }

                $.get(url, data, function (ret) {
                    if (ret.result) {
                        empresa.select2('destroy');
                        empresa.html('');
                        ret.dados.forEach(function (dado) {
                            var option = $('<option value="' + dado.id + '" >' + dado.nome + '</option>');
                            empresa.append(option);
                        });

                        empresa.select2({
                            language: 'pt-BR',
                            theme: 'bootstrap',
                            width: '100%',
                            multiple: true,
                            placeholder: 'Selecione as empresas'
                        });
                    } else {
                        BootstrapDialog.alert(ret.msg);
                        console.log(ret.error);
                    }
                });
            }

            updateFinanceiro();
        });

        empresa.change(function(){
            updateFinanceiro();
        });

        ano.change(function(){
            updateFinanceiro();
        });

        classi.change(function(){
            updateClassi();
        });

        anoc.change(function(){
            updateClassi();
        });
    });
</script>