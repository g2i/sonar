<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Plano de Contas</h2>
        <ol class="breadcrumb">
            <li> plano de contas</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Nome</strong>: <?php echo $Planocontas->nome; ?></p>

                <p>
                    <strong>Classificacao</strong>:
                    <?php
                    echo $this->Html->getLink($Planocontas->getClassificacao()->descricao, 'Classificacao', 'view',
                        array('id' => $Planocontas->getClassificacao()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Planocontas->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Planocontas->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>