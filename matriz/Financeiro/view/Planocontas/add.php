<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Plano de Contas</h2>
        <ol class="breadcrumb">
            <li> plano de contas</li>
            <li class="active">
                <strong>Adicionar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Planocontas', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="span10">
                        <div class="form-group col-md-12">
                            <label class="required" for="nome">Nome <span
                                    class="glyphicon glyphicon-asterisk"></span> </label>
                            <input type="text" name="nome" id="nome" class="form-control"
                                   value="<?php echo $Planocontas->nome ?>" placeholder="Nome">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="classificacao" class="required">Classificação <span
                                    class="glyphicon glyphicon-asterisk"></span></label>
                            <select name="classificacao" class="form-control selectPicker" id="classificacao">
                                <option value=""></option>
                                <?php
                                foreach ($Classificacaos as $c) {
                                    if ($c->id == $Planocontas->classificacao)
                                        echo '<option selected value="' . $c->id . '">' . $c->descricao . '</option>';
                                    else
                                        echo '<option value="' . $c->id . '">' . $c->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Planocontas->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="status">Rateio Automático</label>
                            <select name="rateio" class="form-control" id="rateio">
                                <option value="">Selecione</option>
                                <option value="1">Sim</option>
                                <option value="2">Não</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6 ratear" id="ratear" style="display: none">
                        <div class="form-group">
                            <label for="status">Adicionar Rateio</label><br/>
                            <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="bottom" title="Adicionar Rateio"></span> Rateio', 'Plano_centrocusto', 'add',
                                array("ajax" => true, "modal" => "1", "first" => 1, "plano" => $Planocontas->id),
                                array('data-toggle' => 'modal', 'class' => 'btn btn-info'));
                            ?>
                        </div>
                    </div>

                    <div style="clear:both;"></div>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Planocontas', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#rateio option[value="<?php echo $Planocontas->rateio ?>"]').prop('selected', true);
        if ("<?php echo $Planocontas->rateio ?>" == 1) {
            $(".ratear").show(500);
        }
        $("#rateio").change(function () {
            if ($(this).val() == 1) {
                $(".ratear").show(500);
            } else {
                $(".ratear").hide(500);
            }
        });
    });
</script>