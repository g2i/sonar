
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Rhlocalidade</h2>
    <ol class="breadcrumb">
    <li>Rhlocalidade</li>
    <li class="active">
    <strong>Editar Rhlocalidade</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhlocalidade', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="cidade">Cidade</label>
            <input type="text" name="cidade" id="cidade" class="form-control" value="<?php echo $Rhlocalidade->cidade ?>" placeholder="Cidade">
        </div>
        <div class="form-group">
            <label for="estado">Estado</label>
            <input type="text" name="estado" id="estado" class="form-control" value="<?php echo $Rhlocalidade->estado ?>" placeholder="Estado">
        </div>
        <div class="form-group">
            <label for="dt_cadastro">Dt_cadastro</label>
        </div>
        <div class="form-group">
            <label for="dt_modificacao">Dt_modificacao</label>
        </div>
        <div class="form-group">
            <label for="situacao_id">Situacao</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Rhlocalidade->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Rhlocalidade->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhlocalidade', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>