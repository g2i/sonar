
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Anexo_tipo</h2>
    <ol class="breadcrumb">
    <li>Anexo_tipo</li>
    <li class="active">
    <strong>Editar Anexo_tipo</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Anexo_tipo', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Anexo_tipo->nome ?>" placeholder="Nome">
        </div>
        <div class="form-group">
            <label for="dir">Dir</label>
            <input type="text" name="dir" id="dir" class="form-control" value="<?php echo $Anexo_tipo->dir ?>" placeholder="Dir">
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Anexo_tipo->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Anexo_tipo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>