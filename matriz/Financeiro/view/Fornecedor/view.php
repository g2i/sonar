<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Credor</h2>
        <ol class="breadcrumb">
            <li> credor</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Nome</strong>: <?php echo $Fornecedor->nome; ?></p>

                <p><strong>Telefone</strong>: <?php echo $Fornecedor->telefone; ?></p>

                <p><strong>Celular</strong>: <?php echo $Fornecedor->celular; ?></p>

                <p><strong>Cep</strong>: <?php echo $Fornecedor->cep; ?></p>

                <p><strong>Cidade</strong>: <?php echo $Fornecedor->cidade; ?></p>

                <p><strong>Uf</strong>: <?php echo $Fornecedor->uf; ?></p>

                <p><strong>Numero</strong>: <?php echo $Fornecedor->numero; ?></p>

                <p><strong>Rua</strong>: <?php echo $Fornecedor->rua; ?></p>

                <p><strong>Bairro</strong>: <?php echo $Fornecedor->bairro; ?></p>

                <p><strong>Email</strong>: <?php echo $Fornecedor->email; ?></p>

                <p><strong>Cnpj</strong>: <?php echo $Fornecedor->cnpj; ?></p>

                <p><strong>Observacao</strong>: <?php echo $Fornecedor->observacao; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Fornecedor->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Fornecedor->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>