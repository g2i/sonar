<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Movimento</h2>
        <ol class="breadcrumb">
            <li> Movimento</li>
            <li class="active">
                <strong>Controle</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-12" style="margin-top: 50px">
        <form role="form" method="get"
              action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>">
            <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
            <input type="hidden" name="p" value="<?php echo ACTION; ?>">

            <div class="col-xs-2">
                <label for="mes">M&ecirc;s</label>
                <input type="text" id="mes" name="mes" class="form-control" value='<?php echo $mes ?>'>
            </div>
            <div class="col-xs-2">
                <label for="ano">Ano</label>
                <input type="text" id="ano" name="ano" class="form-control" value="<?php echo $ano ?>">
            </div>
            <div class="col-xs-4">
                <label for="banco">Banco</label>
                <select id="banco" name="banco" class="form-control selectPicker">
                    <option></option>
                    <?php
                    foreach ($bancos as $b) {
                        if ($b->id == $banco)
                            echo "<option selected value='" . $b->id . "'>" . $b->nome . "</option>";
                        else
                            echo "<option value='" . $b->id . "'>" . $b->nome . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-xs-2">
                <label for="status">Status</label>
                <select id="status" name="status" class="form-control selectPicker">
                    <option></option>
                    <option <?php if ($status == "Aberto") echo "selected"; ?> value="Aberto">Aberto
                    </option>
                    <option <?php if ($status == "Encerrado") echo "selected"; ?> value="Encerrado">
                        Encerrado
                    </option>
                </select>
            </div>
            <div class="col-xs-2">
                <button style="margin-top: 25px;" type="submit" class="btn btn-default"><span
                        class="glyphicon glyphicon-search"></span></button>
            </div>
            <div style="clear:both;"></div>
        </form>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row text-right pull-right">
                    <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Abrir nova Movimenta&ccedil;&atilde;o', 'Movimento', 'abertura', NULL, array('class' => 'btn btn-primary')); ?></p>
                </div>
                <div class="row">
                    <table class="table table-hover">
                        <tr>
                            <th>Banco</th>
                            <th>M&ecirc;s</th>
                            <th>Ano</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        <?php
                        foreach ($controle::getList($criteria) as $c) {
                            echo '<tr>';
                            echo '<td>' . $c->getBanco()->nome . '</td>';
                            echo '<td>' . $c->mes . '</td>';
                            echo '<td>' . $c->ano . '</td>';
                            echo '<td>' . $c->status . '</td>';
                            if ($c->status == 'Aberto') {
                                echo '<td width="50">';
                                echo $this->Html->getLink('<span class="glyphicon glyphicon-off"></span> ', 'Movimento', 'fechar', array('id' => $c->id), array('class' => 'btn btn-success btn-sm', 'data-toggle' => 'modal', 'title' => 'Encerrar Movimenta&ccedil;&atilde;o'));
                                echo '</td>';
                            } else {
                                echo '<td width="50">';
                                echo $this->Html->getLink('<span class="glyphicon glyphicon-off"></span> ', 'Movimento', 'reabrir', array('id' => $c->id), array('class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal', 'title' => 'Reabrir Movimenta&ccedil;&atilde;o'));
                                echo '</td>';
                            }
                            echo '</tr>';
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#mes').inputmask("m",{ "placeholder": "mm" });
        $('#ano').inputmask("y",{ "placeholder": "yyyy" });
    });
</script>
