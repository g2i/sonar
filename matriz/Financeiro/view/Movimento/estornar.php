<form class="form" method="post" action="<?php echo $this->Html->getUrl('Movimento', 'estornar') ?>">
    <h1>Confirmação</h1>

    <div class="well well-lg">
        <p>Voce tem certeza que deseja Estornar este Movimento ?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Movimento->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Movimento', 'all') ?>" class="btn btn-default"
           data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Estornar">
    </div>
</form>