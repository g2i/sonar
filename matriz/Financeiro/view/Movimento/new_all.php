<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Movimento</h2>
        <ol class="breadcrumb">
            <li> Movimento</li>
            <li class="active">
                <strong>Pesquisa</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-12" style="margin-top: 50px">
        <div class="col-xs-12 col-md-6">
            <legend>Filtros</legend>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label for="empresa">Empresa</label>
                <div class="input-group select2-bootstrap-append">
                    <select id="empresa" name="empresa" class="form-control selectPicker">
                        <option></option>
                        <?php foreach ($Empresas as $e): ?>
                            <option value="<?php echo $e->id ?>"><?php echo $e->nome ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="empresa"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label>
                    <label class="radio-inline">
                        <input type="radio" name="tipo" id="tipo" value="1" checked>
                        Credor
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="tipo" id="tipo" value="2">
                        Cliente
                    </label>
                </label>
                <br/>
                <div class="credot">
                    <div class="input-group select2-bootstrap-append">
                        <select id="fornecedor" name="fornecedor" class="form-control credor-ajax"></select>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-clear="fornecedor"
                                    data-toggle="tooltip" data-placement="top" title="Limpar">
                                <i class="fa fa-times"></i>
                            </button>
                        </span>
                    </div>
                </div>
                <div class="client" style="display: none">
                    <div class="input-group select2-bootstrap-append">
                        <select id="cliente" name="cliente" class="form-control clientes-ajax"></select>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-clear="cliente"
                                    data-toggle="tooltip" data-placement="top" title="Limpar">
                                <i class="fa fa-times"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label for="documento">Documento</label>
                <input type="text" name="documento" id="documento" class="form-control" placeholder="Documento" value="<?php echo $documento; ?>"/>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label for="plano">Plano de contas</label>
                <div class="input-group select2-bootstrap-append">
                    <select id="plano" name="plano" class="form-control plano-ajax"></select>
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" data-select2-clear="plano"
                                data-toggle="tooltip" data-placement="top" title="Limpar">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6" style="border-left:1px solid #E5E5E5;">
            <legend>Saldo</legend>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label for="inicio">Inicio</label>
                <div class='input-group dateInicio'>
                    <input type='text' class="form-control dateFormat" name="inicio" id="inicio"/>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <label for="fim">Fim</label>
                <div class='input-group dateFim'>
                    <input type='text' class="form-control dateFormat" name="fim" id="fim"/>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="banco">Banco</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="banco" name="banco" class="form-control selectPicker">
                            <option></option>
                            <?php foreach ($Bancos as $b): ?>
                                <option value="<?php echo $b->id ?>"><?php echo $b->nome ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-clear="banco"
                                    data-toggle="tooltip" data-placement="top" title="Limpar">
                                <i class="fa fa-times"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                <div class="form-group">
                    <label for="movimento">Movimento</label>
                    <div class="input-group select2-bootstrap-append">
                        <select id="movimento" name="movimento" class="form-control">
                        </select>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" data-select2-clear="movimento"
                                    data-toggle="tooltip" data-placement="top" title="Limpar">
                                <i class="fa fa-times"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-right">
        <div class="form-group">
            <button id="btnPesquisar" style="margin-top: 20px;" type="button" data-role="tooltip"
                    data-placement="bottom" title="Pesquisar" class="btn btn-default">
                <i class="fa fa-search"></i></button>
            <button id="btnReset" type="button" style='margin-top:20px;' data-role="tooltip" data-placement="bottom"
                    title="Limpar Campos" class="btn btn-default">
                <i class="fa fa-refresh"></i></button>
            <button id="btnRel" style='margin-top:20px;' title="Relatório" type="button"
                    data-role="tooltip" data-placement="bottom" class="btn btn-default">
                <i class="fa fa-print"></i></button>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <div id="tblDadosResp" class="table-responsive" style="overflow: auto">
                    <table id="tblDados" class="table table-hover">
                        <thead>
                        <tr>
                            <th data-column-id="id" data-identifier="true">Nr. Lanc.</th>
                            <th data-column-id="data" data-converter="date">Data</th>
                            <th data-column-id="nome">Credor/Cliente</th>
                            <th data-column-id="plano">Plano de Contas</th>
                            <th data-column-id="empresa">Empresa</th>
                            <th data-column-id="documento">Documento</th>
                            <th data-column-id="movimento">Banco/Movimento</th>
                            <th data-column-id="observacao">Observa&ccedil;&atilde;o</th>
                            <th data-column-id="credito" data-converter="currency">Crédito</th>
                            <th data-column-id="debito" data-converter="currency">Débito</th>
                            <th data-column-id="saldo" data-converter="currency">Saldo</th>
                            <th data-column-id="opcoes" data-formatter="opcoes"
                                data-header-css-class="col-opcoes" data-visible-in-selection="false"
                                data-sortable="false"></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var totais = null;
    $(document).ready(function () {
        var periodo = moment().date(1);
        $('#inicio').val(periodo.format('DD/MM/YYYY'));
        $('#fim').val(periodo.endOf('month').format('DD/MM/YYYY'));

        $('#tblDadosResp').mCustomScrollbar({
            axis:"x",
            theme:"minimal-dark",
        });

        $("button[data-select2-clear]").click(function() {
            $("#" + $(this).data("select2-clear")).val('').trigger('change');
        });

        var movimento = $('#movimento');
        var banco = $('#banco');

        movimento.select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: 'Selecione:'
        })

        var grid = $('#tblDados').bootgrid({
            rowSelect: true,
            multiSort: true,
            rowCount: -1,
            ajax: true,
            url: root + '/Movimento/getMovimentoNew',
            ajaxSettings: {
                method: "GET",
                cache: true
            },
            converters:{
                date: {
                    from: function (value) {
                        if(value == null)
                            return null;

                        return moment(value);
                    },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return moment(value, 'YYYY-MM-DDTHH:mm:ss').format("L");
                    }
                },
                currency: {
                    from: function (value) { return value; },
                    to: function (value) {
                        if(value == null)
                            return null;

                        return $.number(value, 2);
                    }
                }
            },
            formatters:{
                opcoes: function(column, row){
                    return  '<button type="button" data-toggle="tooltip" data-placement="top" title="Editar" class="btn btn-xs btn-info command-edit" data-row-id="' + row.id + '"><span class="fa fa-pencil-square"></span></button>';
                }
            },
            templates:{
                search: '',
            },
            requestHandler: function (request) {
                request.inicio = $('#inicio').val();
                request.fim = $('#fim').val();
                request.documento = $('#documento').val();
                request.plano = $('#plano').val();
                request.empresa = $('#empresa').val();
                request.fornecedor = $('#fornecedor').val();
                request.cliente = $('#cliente').val();
                request.movimento = movimento.val();
                request.banco = banco.val();

                if(request.sort){
                    var sort = [];
                    $.each(request.sort, function(key, value){
                        sort.push([key, value]);
                    });

                    delete request.sort;
                    request.sort = $.toJSON(sort);
                }

                totais = null;
                return request;
            },
            responseHandler: function (response){
                totais = response.somatoria;
                return response;
            },
        });

        grid.on('load.rs.jquery.bootgrid', function(){
            var header = grid.find('#gridHeader');
            header.remove();

            var footer = grid.find('tfoot');
            footer.remove();
        });

        grid.on("loaded.rs.jquery.bootgrid", function() {
            grid.find('[data-toggle="tooltip"]').tooltip();

            grid.find(".command-edit").unbind('click');
            grid.find(".command-edit").on("click", function (e) {
                var id = $(this).data("row-id");
                var url = root + '/Movimento/edit?id=' + id;
                document.location = url;
            });

            var header = $('<thead id="gridHeader"></thead>');
            var headeIni = $('<tr></tr>');
            headeIni.append($('<td colspan="8" class="text-right" style="font-weight: bold">Valor Inicial:</td>'));
            headeIni.append($('<td id="saldo_init" style="font-weight: bold">' + $.number(totais.inicial, 2) + '</td>'));
            headeIni.append($('<td></td>'));
            header.append(headeIni);
            grid.prepend(header);

            var footer = $('<tfoot></tfoot>');
            var sumario = $('<tr></tr>');

            sumario.append($('<td colspan="6" class="text-right" style="font-weight: bold">Total:</td>'));
            sumario.append($('<td style="font-weight: bold">' + $.number(totais.credito, 2) + '</td>'));
            sumario.append($('<td style="font-weight: bold">' + $.number(totais.debito, 2) + '</td>'));
            sumario.append($('<td style="font-weight: bold">' + $.number(totais.final, 2) + '</td>'));
            sumario.append($('<td></td>'));
            footer.append(sumario);
            grid.append(footer);
            $('#tblDadosResp').mCustomScrollbar("update");
        });

        banco.change(function () {
            $('body').css("cursor", "progress");
            var url = root + '/Movimento/getMovimentoBanco';
            var data = {
                banco: $('#banco').val(),
                categoria: $('#categoria').val()
            }

            $.post(url, data, function (txt) {
                $('body').css("cursor", "default");
                movimento.select2('destroy');
                movimento.html(txt);
                movimento.select2({
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: '100%',
                    placeholder: 'Selecione:'
                })
            });
        });

        $('#filtro').click(function(){
            grid.bootgrid('reload');
        });

        $("input[type='radio'][name='tipo']").click(function () {
            if ($(this).val() == 1) {
                $(".client").hide('fast');
                $(".client").val('').trigger('change');
                $(".credot").show(500);
            } else {
                $(".credot").hide('fast');
                $(".credot").val('').trigger('change');
                $(".client").show(500);
            }
        })

        $('#btnPesquisar').click(function(){
            grid.bootgrid('reload');
        });

        $('#btnReset').click(function(){
            var data = moment().date(1);
            $('#inicio').val(data.format('DD/MM/YYYY'));
            $('#fim').val(data.endOf('month').format('DD/MM/YYYY'));
            $("#cliente").val('').trigger('change');
            $("#fornecedor").val('').trigger('change');
            movimento.val('').trigger('change');
            banco.val('').trigger('change');
            $('#empresa').val('').trigger('change');
            $('#plano').val('').trigger('change');
            $('#documento').val('');
        });

        $('#btnRel').click(function(){
           var data = {
               inicio: moment($("#inicio").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
               fim: moment($("#fim").val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
               empresa: $("#empresa").val(),
               credor: $("#fornecedor").val(),
               cliente: $("#cliente").val(),
               documento: $("#documento").val(),
               plano: $("#plano").val(),
               banco: $("#banco").val(),
               mov: $("#movimento").val(),
               saldo: 0
           }

            if (data.empresa == "" || data.empresa == null)
                data.empresa =- 1;

            if (data.credor == "" || data.credor == null)
                data.credor = -1;

            if (data.cliente == "" || data.cliente == null)
                data.cliente = -1;

            if (data.documento == "" || data.documento == null)
                data.documento = -1;

            if (data.plano == "" || data.plano == null)
                data.plano = -1;

            if (data.banco == "" || data.banco == null)
                data.banco = -1;

            if (data.mov == "" || data.mov == null) {
                data.mov = -1;

                data.saldo = $("#saldo_init").text().replace('.','');
                data.saldo = data.saldo.replace(',','.');
                data.saldo = data.saldo.replace('R$ ','');

                $.ReportPost('movi_geral', data);
            }else{
                LoadGif();
                $.ajax({
                    type: 'POST',
                    url: root + '/Movimento/SaldoInicialData',
                    data: 'situacao=&movimento=' + data.mov + '&inicio=' + data.inicio + '&fim=' + data.fim,
                    success: function (txt) {
                        CloseGif();
                        data.saldo = txt;
                        $.ReportPost('movi_geral', data);
                    }
                });
            }
        });
    });
</script>

