<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
    .has-error .select2-selection {
        border: 1px solid #a94442;
        border-radius: 4px;
    }
</style>

<form method="post" role="form" action="<?= $this->Html->getUrl('Movimento', 'add') ?>"
      id="formulario">
    <div class="alert alert-info">Os campos marcados com <span
            class="small glyphicon glyphicon-asterisk"></span> são de
        preenchimento obrigatório.
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="data" class="required">Data do Documento <span
                    class="glyphicon glyphicon-asterisk"></span></label>
            <div class='input-group date'>
                <input type='text' class="form-control dateFormat" name="data" id="data"/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="situacao" class="required">Situação <span class="glyphicon glyphicon-asterisk"></span></label>
            <select id="situacao" name="situacao" class="form-control selectPicker" required>
                <option value="1">Consolidado</option>
                <option value="2">A cair</option>
            </select>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="idPlanoContas" class="required">Plano de Contas <span
                    class="glyphicon glyphicon-asterisk"></span></label>
            <select name="idPlanoContas" id="idPlanoContas" class="form-control selectPicker" required>
                <option></option>
                <?php foreach ($Planocontas as $p): ?>
                    <option value="<?= $p->id ?>"><?= $p->nome ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="tipo" class="required">Tipo <span
                    class="glyphicon glyphicon-asterisk"></span></label>
            <select id="tipo" name="tipo" class="form-control selectPicker" onchange="valorTipo($(this).val())"
                    required>
                <option></option>
                <option value="1">Débito</option>
                <option value="2">Crédito</option>
            </select>
        </div>
    </div>

    <div class="col-xs-6" id="formFornecedor" style="display:none">
        <div class="form-group">
            <label for="idFornecedor">Credor</label>
            <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax">
                <?php if (!empty($clienteSelect)) echo '<option value="' . $forncedorSelect->id . '">' . $forncedorSelect->nome . '</option>'; ?>
            </select>
        </div>
    </div>

    <div class="col-xs-6" id="formCliente" style="display:none">
        <div class="form-group">
            <label for="idCliente">Cliente</label>
            <select id="idCliente" name="idCliente" class="form-control clientes-ajax">
                <?php if (!empty($clienteSelect)) echo '<option value="' . $clienteSelect->codigo . '">' . $clienteSelect->cliente . '</option>'; ?>
            </select>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="documento">Documento</label>
            <input type="text" name="documento" id="documento" class="form-control"
                   placeholder="Documento">
        </div>
    </div>
    <div class="col-xs-6" id="divCredito" style="display:none">
        <div class="form-group">
            <label for="credito" class="required">Valor <span
                    class="glyphicon glyphicon-asterisk"></span></label>
            <div class='input-group'>
                <span class="input-group-addon">R$</span>
                <input type="text" id="credito" name="credito" value="0" class="form-control money2">
            </div>
        </div>
    </div>
    <div class="col-xs-6" id="divDebito" style="display:none">
        <div class="form-group">
            <label for="debito" class="required">Valor <span
                    class="glyphicon glyphicon-asterisk"></span></label>
            <div class='input-group'>
                <span class="input-group-addon">R$</span>
                <input type="text" id="debito" value="0" name="debito" class="form-control money2">
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="movimento" class="required">Empresa <span
                    class="glyphicon glyphicon-asterisk"></span> </label>
            <select id="idContabilidade" name="idContabilidade" class="form-control selectPicker" required>
                <option></option>
                <?php foreach ($Empresa as $e): ?>
                    <option value="<?= $e->id ?>"><?= $e->nome ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div class="col-xs-6">
        <div class="form-group">
            <label for="movimento" class="required">Banco <span
                    class="glyphicon glyphicon-asterisk"></span></label>
            <select id="movimento" name="movimento" class="form-control selectPicker" required>
                <option></option>
                <?php foreach ($movimento as $m): ?>
                    <option value="<?= $m[0] ?>"><?= $m[1]?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <label for="complemento">Observa&ccedil;&atilde;o</label>
            <textarea name="complemento" id="complemento" class="form-control"></textarea>
        </div>
    </div>

    <div style="clear:both;"></div>
    <div class="text-right">
        <a href="<?= $this->Html->getUrl('Movimento', 'all') ?>" class="btn btn-default"
           data-dismiss="modal">Cancelar</a>
        <button type="submit" onclick="EnviarDados('form')" class="btn btn-primary"> Salvar</button>
    </div>
</form>

<script type="text/javascript">
    function valorTipo(value) {
        if (value == 2) {
            $('#divDebito').hide();
            $('#divDebito').val('');
            $('#formFornecedor').hide();
            $('#formCliente').show(500);
            $('#divCredito').show(500);
            $('#debito').attr('required', false);
            $('#credito').attr('required', true);
        } else if (value == 1) {
            $('#divCredito').hide();
            $('#divCredito').val('');
            $('#formCliente').hide();
            $('#formFornecedor').show(500);
            $('#divDebito').show(500);
            $('#debito').attr('required', true);
            $('#credito').attr('required', false);
        } else {
            $('#divDebito').hide(500);
            $('#divCredito').hide(500);
            $('#formCliente').hide(500);
            $('#formFornecedor').hide(500);
        }
    }
</script>