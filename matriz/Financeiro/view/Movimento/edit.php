<style>
    .select2-close-mask{
        z-index: 2099;
    }
    .select2-dropdown{
        z-index: 3051;
    }
    .has-error .select2-selection {
        border: 1px solid #a94442;
        border-radius: 4px;
    }
</style>
<form method="post" role="form" action="<?= $this->Html->getUrl('Movimento', 'edit') ?>">
    <div class="alert alert-info">Os campos marcados com <span
            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="data">Data do Documento*</label>
            <div class='input-group date'>
                <input type='text' class="form-control dateFormat" name="data" id="data" value="<?= $Movimento->data ?>" required/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="situacao">Situação</label>
            <select id="situacao" name="situacao" class="form-control selectPicker">
                <option value="1" <?= ($Movimento->situacao == '1') ? 'selected' : '' ?>>
                    Consolidado
                </option>
                <option value="2" <?= ($Movimento->situacao == '2') ? 'selected' : '' ?>>A
                    cair
                </option>
            </select>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="idPlanoContas">Plano de Contas</label>
            <select name="idPlanoContas" id="idPlanoContas" class="form-control selectPicker">
                <option></option>
                <?php
                foreach ($Planocontas as $p) {
                    if ($p->id == $Movimento->idPlanoContas)
                        echo '<option selected value="' . $p->id . '">' . $p->nome . '</option>';
                    else
                        echo '<option value="' . $p->id . '">' . $p->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="tipo">Tipo</label>
            <select id="tipo" name="tipo" class="form-control selectPicker" onchange="valorTipo($(this).val())">
                <option value="0">Selecione</option>
                <option value="2" <?= ($Movimento->tipo == '2') ? 'selected' : '' ?>>
                    Crédito
                </option>
                <option value="1" <?= ($Movimento->tipo == '1') ? 'selected' : '' ?>>Débito
                </option>
            </select>
        </div>
    </div>
    <div class="col-xs-6" id="formFornecedor" style="display:none">
        <div class="form-group">
            <label for="idFornecedor">Credor</label>
            <select id="idFornecedor" name="idFornecedor" class="form-control credor-ajax">
                <?php if (!empty($clienteSelect)) echo '<option value="' . $forncedorSelect->id . '">' . $forncedorSelect->nome . '</option>'; ?>
            </select>
        </div>
    </div>
    <div class="col-xs-6" id="formCliente" style="display:none">
        <div class="form-group">
            <label for="idCliente">Cliente</label>
            <select id="idCliente" name="idCliente" class="form-control clientes-ajax">
                <?php if (!empty($clienteSelect)) echo '<option value="' . $clienteSelect->codigo . '">' . $clienteSelect->cliente . '</option>'; ?>
            </select>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="documento">Documento</label>
            <input type="text" name="documento" id="documento" class="form-control"
                   value="<?= $Movimento->documento ?>" placeholder="Documento">
        </div>
    </div>
    <div class="col-xs-6" id="divCredito" style="display:none">
        <div class="form-group">
            <label for="credito">Valor</label>
            <div class='input-group'>
                <span class="input-group-addon">R$</span>
                <input type="text" id="credito" name="credito" class="form-control money2"
                       value="<?= number_format($Movimento->credito, '2', ',', '.') ?>">
            </div>
        </div>
    </div>
    <div class="col-xs-6" id="divDebito" style="display:none">
        <div class="form-group">
            <label for="debito">Valor</label>
            <div class='input-group'>
                <span class="input-group-addon">R$</span>
                <input type="text" id="debito" name="debito" class="form-control money2"
                       value="<?= number_format($Movimento->debito, '2', ',', '.') ?>">
            </div>
        </div>
    </div>
    <div class="col-xs-6">
        <div class="form-group">
            <label for="movimento">Empresa *</label>
            <select id="idContabilidade" name="idContabilidade" class="form-control selectPicker" required>
                <option></option>
                <?php
                foreach ($Empresa as $e) {
                    if ($Movimento->idContabilidade == $e->id)
                        echo '<option value="' . $e->id . '" selected>' . $e->nome . '</option>';
                    else
                        echo '<option value="' . $e->id . '" >' . $e->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>

    <div class="col-xs-6">
        <div class="form-group">
            <label for="movimento">Banco *</label>
            <select id="movimento" name="movimento" class="form-control selectPicker" required>
                <option></option>
                <?php
                $c = new Criteria();
                $c->addCondition('status', '=', 'Aberto');
                foreach ($movimento->getList($c) as $m) {
                    if ($m->id == $Movimento->idMovimentoBanco) {
                        echo '<option selected value="' . $m->id . '">' . $m->getBanco($m->id)->nome . ' - ' . $m->mes . '/' . $m->ano . '</option>';
                    } else {
                        echo '<option value="' . $m->id . '">' . $m->getBanco($m->id)->nome . ' - ' . $m->mes . '/' . $m->ano . '</option>';
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="form-group">
            <label for="complemento">Observa&ccedil;&atilde;o</label>
                                <textarea name="complemento" id="complemento"
                                          class="form-control"><?= $Movimento->complemento ?></textarea>
        </div>
    </div>
    <input type="hidden" name="id" value="<?= $Movimento->id; ?>">

    <div style="clear:both;"></div>
    <div class="text-right">
        <a href="<?= $this->Html->getUrl('Movimento', 'all') ?>" class="btn btn-default"
           data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary"
               onclick="EnviarFormulario('form')" value="salvar">
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        <?php
        if ($Movimento->tipo == '1') {
            echo "valorTipo(1);";
        } else {
            echo "valorTipo(2);";
        }
        ?>
    });

    function valorTipo(value) {
        if (value == 2) {
            $('#divDebito').hide();
            $('#divDebito').val('');
            $('#formFornecedor').hide();
            $('#formCliente').show(500);
            $('#divCredito').show(500);
            $('#debito').attr('required', false);
            $('#credito').attr('required', true);
        } else if (value == 1) {
            $('#divCredito').hide();
            $('#divCredito').val('');
            $('#formCliente').hide();
            $('#formFornecedor').show(500);
            $('#divDebito').show(500);
            $('#debito').attr('required', true);
            $('#credito').attr('required', false);
        } else {
            $('#divDebito').hide(500);
            $('#divCredito').hide(500);
            $('#formCliente').hide(500);
            $('#formFornecedor').hide(500);
        }
    }
</script> 
