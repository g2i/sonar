<form class="form" method="post" action="<?php echo $this->Html->getUrl('Movimento', 'reabrir') ?>">
    <h1>Confirma&ccedil;&atilde;o</h1>

    <div class="well well-lg">
        <p>Voce tem certeza que deseja Reabrir esse movimento?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $controle->id; ?>">
        <a href="<?php echo $this->Html->getUrl('Movimento', 'controle') ?>" class="btn btn-default"
           data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-success" value="Reabrir">
    </div>
</form>