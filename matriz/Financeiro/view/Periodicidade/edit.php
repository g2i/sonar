<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Periodicidade</h2>
        <ol class="breadcrumb">
            <li>Periodicidade</li>
            <li class="active">
                <strong>Editar Periodicidade</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Periodicidade', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span
                                class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                        </div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="nome">Nome</label>
                                <input type="text" name="nome" id="nome" class="form-control"
                                       value="<?php echo $Periodicidade->nome ?>" placeholder="Nome">
                            </div>
                            <div class="form-group">
                                <label for="dias">Dias</label>
                                <input type="number" name="dias" id="dias" class="form-control"
                                       value="<?php echo $Periodicidade->dias ?>" placeholder="Dias">
                            </div>
                            <input type="hidden" name="status" value="1" />

                        </div>
                        <input type="hidden" name="id" value="<?php echo $Periodicidade->id; ?>">
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Periodicidade', 'all') ?>" class="btn btn-default"
                               data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>