<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cedente</h2>
        <ol class="breadcrumb">
            <li>cedente</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Cedente', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group">
                            <label for="cedente">Cedente</label>
                            <input type="text" name="cedente" id="cedente" class="form-control"
                                   value="<?php echo $Cedente->cedente ?>" placeholder="Cedente" required>
                        </div>
                        <div class="form-group">
                            <label for="cep">Cep</label>
                            <input type="text" name="cep" id="cep" class="form-control"
                                   value="<?php echo $Cedente->cep ?>" placeholder="Cep">
                        </div>
                        <div class="form-group">
                            <label for="endereco">Endereço</label>
                            <input type="text" name="endereco" id="endereco" class="form-control"
                                   value="<?php echo $Cedente->endereco ?>" placeholder="Endereço">
                        </div>

                        <div class="form-group">
                            <label for="numero">Número</label>
                            <input type="number" name="numero" id="numero" class="form-control"
                                   value="<?php echo $Cedente->numero ?>" placeholder="Número">
                        </div>

                        <div class="form-group">
                            <label for="bairro">Bairro *</label>
                            <input type="text" name="bairro" id="bairro" class="form-control"
                                   value="<?php echo $Cedente->bairro ?>" placeholder="Bairro" required>
                        </div>

                        <div class="form-group">
                            <label for="cidade">Cidade</label>
                            <input type="text" name="cidade" id="cidade" class="form-control"
                                   value="<?php echo $Cedente->cidade ?>" placeholder="Cidade">
                        </div>

                        <div class="form-group">
                            <label for="estado">Estado</label>
                            <input type="text" name="estado" id="estado" class="form-control"
                                   value="<?php echo $Cedente->estado ?>" placeholder="Estado">
                        </div>


                        <div class="form-group">
                            <label for="cnpj">Cnpj</label>
                            <input type="text" name="cnpj" id="cnpj" class="form-control"
                                   value="<?php echo $Cedente->cnpj ?>" placeholder="Cnpj">
                        </div>
                        <div class="form-group">
                            <label for="codBanco">Cod Banco</label>
                            <input type="number" name="codBanco" id="codBanco" class="form-control"
                                   value="<?php echo $Cedente->codBanco ?>" placeholder="Cod Banco">
                        </div>
                        <div class="form-group">
                            <label for="codConvenio">Cod Convênio</label>
                            <input type="number" name="codConvenio" id="codConvenio" class="form-control"
                                   value="<?php echo $Cedente->codConvenio ?>" placeholder="Cod Convênio">
                        </div>
                        <div class="form-group">
                            <label for="codAgencia">Cod Agência</label>
                            <input type="number" name="codAgencia" id="codAgencia" class="form-control"
                                   value="<?php echo $Cedente->codAgencia ?>" placeholder="Cod Agência">
                        </div>

                        <div class="form-group">
                            <label for="agenciaDv">Agência DV</label>
                            <input type="number" name="agenciaDv" id="agenciaDv" class="form-control"
                                   value="<?php echo $Cedente->agenciaDv ?>" placeholder="Agência Dv">
                        </div>
                        <div class="form-group">
                            <label for="codConta">Cod Conta</label>
                            <input type="number" name="codConta" id="codConta" class="form-control"
                                   value="<?php echo $Cedente->codConta ?>" placeholder="Cod Conta">
                        </div>
                        <div class="form-group">
                            <label for="codCedente">Cod Cedente</label>
                            <input type="number" name="codCedente" id="codCedente" class="form-control"
                                   value="<?php echo $Cedente->codCedente ?>" placeholder="Cod Cedente">
                        </div>
                        <div class="form-group">
                            <label for="cedenteDv">Cedente Dv</label>
                            <input type="number" name="cedenteDv" id="cedenteDv" class="form-control"
                                   value="<?php echo $Cedente->cedenteDv ?>" placeholder="Cedente Dv">
                        </div>
                        <div class="form-group">
                            <label for="cedenteCarteira">Cedente Carteira</label>
                            <input type="number" name="cedenteCarteira" id="cedenteCarteira" class="form-control"
                                   value="<?php echo $Cedente->cedenteCarteira ?>" placeholder="Cedente Carteira">
                        </div>
                        <div class="form-group">
                            <label for="cedenteJuros">Cedente Juros</label>
                            <input type="number" name="cedenteJuros" id="cedenteJuros" class="form-control"
                                   value="<?php echo $Cedente->cedenteJuros ?>" placeholder="Cedente Juros">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Cedente->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Cedente->id; ?>">

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Cedente', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        $('#cep').mask('00000-000');
        $('#cnpj').mask('00.000.000/0000-00');
    });

    $(document).ready(function () {
        $('#cep').blur(function () {
            /* Configura a requisição AJAX */
            $.ajax({
                url: root + "/Cedente/cep",
                type: 'POST',
                data: 'cep=' + $('#cep').val(),
                success: function (data) {
                    var data = data.split(',');
                    if (data[0] == 1) {
                        $('#endereco').val(data[1]);
                        $('#bairro').val(data[2]);
                        $('#cidade').val(data[3]);
                        $('#estado').val(data[4]);
                        $('#numero').focus();
                    } else {
                        OpenMensagem("Alerta", "Cep não Encontrado!");
                    }
                }
            });
            return false;
        });
    });
</script>