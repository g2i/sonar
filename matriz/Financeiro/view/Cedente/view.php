<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cedente</h2>
        <ol class="breadcrumb">
            <li>cedente</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

                <p><strong>Cedente</strong>: <?php echo $Cedente->cedente; ?></p>

                <p><strong>Cep</strong>: <?php echo $Cedente->cep; ?></p>

                <p><strong>Endereço</strong>: <?php echo $Cedente->endereco; ?></p>

                <p><strong>Numero</strong>: <?php echo $Cedente->numero; ?></p>

                <p><strong>Bairro</strong>: <?php echo $Cedente->bairro; ?></p>

                <p><strong>Cidade</strong>: <?php echo $Cedente->cidade; ?></p>

                <p><strong>Estado</strong>: <?php echo $Cedente->estado; ?></p>

                <p><strong>Cnpj</strong>: <?php echo $Cedente->cnpj; ?></p>

                <p><strong>Codigo Banco</strong>: <?php echo $Cedente->codBanco; ?></p>

                <p><strong>Codigo Convênio</strong>: <?php echo $Cedente->codConvenio; ?></p>

                <p><strong>Codigo Agência</strong>: <?php echo $Cedente->codAgencia; ?></p>

                <p><strong>Codigo Conta</strong>: <?php echo $Cedente->codConta; ?></p>

                <p><strong>Codigo Cedente</strong>: <?php echo $Cedente->codCedente; ?></p>

                <p><strong>Cedente Dv</strong>: <?php echo $Cedente->cedenteDv; ?></p>

                <p><strong>Cedente Carteira</strong>: <?php echo $Cedente->cedenteCarteira; ?></p>

                <p><strong>Cedente Juros</strong>: <?php echo $Cedente->cedenteJuros; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Cedente->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Cedente->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>