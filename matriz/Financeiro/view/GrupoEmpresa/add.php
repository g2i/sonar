<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Grupo Empresa</h2>
        <ol class="breadcrumb">
            <li> Grupo Empresa</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('GrupoEmpresa', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="form-group">
                        <label for="descricao">Descricao</label>
                        <input type="text" name="descricao" required="required" id="descricao" class="form-control"
                               placeholder="Descricao">
                    </div>
                    <input type="hidden" name="status" value="1"/>

                    <div class="row">
                        <?php if ($this->getParam('modal') == 1) { ?>
                            <input type="hidden" name="modal" value="1"/>

                            <div class="text-right">
                                <a href="javascript:void(0)" data-placement="bottom" class="btn btn-default"
                                   data-toggle="tooltip2" title="Voltar"
                                   onclick="Navegar('','back')">
                                    Cancelar</a>
                                <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary"
                                       value="salvar">
                            </div>
                        <?php } else { ?>
                            <div class="text-right">
                                <a href="<?php echo $this->Html->getUrl('GrupoEmpresa', 'all') ?>"
                                   class="btn btn-default" data-dismiss="modal">Cancelar</a>
                                <input type="submit" class="btn btn-primary" value="salvar">
                            </div>
                        <?php } ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>