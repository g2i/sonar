<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Empresa/Centro Custo</h2>
        <ol class="breadcrumb">
            <li> Empresa/Centro Custo</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p>
                    <strong>Centro_custo</strong>:
                    <?php
                    echo $this->Html->getLink($Empresa_custo->getCentro_custo()->descricao, 'Centro_custo', 'view',
                        array('id' => $Empresa_custo->getCentro_custo()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>

                <p>
                    <strong>Contabilidade</strong>:
                    <?php
                    echo $this->Html->getLink($Empresa_custo->getContabilidade()->nome, 'Contabilidade', 'view',
                        array('id' => $Empresa_custo->getContabilidade()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>