<form class="form" method="post" action="<?php echo $this->Html->getUrl('Anexo_financeiro', 'delete') ?>">

    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Anexo_financeiro->titulo; ?></strong>?</p>
    </div>
    <div class="text-right">
        <input type="hidden" name="id" value="<?php echo $Anexo_financeiro->id; ?>">
        <input type="hidden" name="origem" value="<?php echo $this->getParam('origem') ?>">
        <input type="hidden" name="rota_id_externo" value="<?php echo $this->getParam('rota_id_externo'); ?>">
        <input type="hidden" name="rota_id_programacao" value="<?php echo $this->getParam('rota_id_programacao'); ?>">
        <a href="<?php echo $this->Html->getUrl('Anexo_financeiro', 'all',array('origem' => $this->getParam('origem'),'id_externo'=> $this->getParam('rota_id_externo'),'id_programacao'=>$this->getParam('rota_id_programacao'),'tipo'=>$Anexo_financeiro->tipo)) ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-danger" value="Excluir">
    </div>
</form>