<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexo </h2>
        <ol class="breadcrumb">
            <li>Anexo</li>
            <li class="active">
                <strong>All</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-12" style="margin-top: 50px">
        <form id="pesquisa" method="get" action="<?= $this->Html->getUrl('Anexo_financeiro', 'all') ?>">
            <input type="hidden" name="id_programacao" value="<?= $this->getParam('id_programacao') ?>">
            <input type="hidden" name="id_externo" value="<?= $this->getParam('id_externo') ?>">
            <input type="hidden" name="origem" value="<?= $this->getParam('origem') ?>">
            <div class="form-group col-md-4 col-sm-4 col-xs-4">
                <label for="titulo">Tipo de documento</label>
                <select name="tipo" class="form-control selectPicker" id="tipo">
                    <option value=""></option>
                    <?php
                    foreach ($tiposDoc as $t) {
                        if ($t->id == $this->getParam('tipo'))
                            echo '<option selected value="' . $t->id . '">' . $t->nome . '</option>';
                        else
                            echo '<option  value="' . $t->id . '">' . $t->nome . '</option>';
                    }
                    ?>
                </select>
            </div>

            <div class="col-xs-10 col-md-10">
                <div class="form-group pull-left">
                    <button type="submit" data-role="tooltip"
                            data-placement="bottom" title="Pesquisar" class="btn btn-default">
                        <i class="fa fa-search"></i></button>

                    <button id="clearForm" type="button" data-role="tooltip"
                            data-placement="bottom" title="limpar filtros" class="btn btn-default">
                        <i class="fa fa-refresh"></i>
                    </button>
                </div>
            </div>
            <div style="clear:both"></div>
        </form>
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">
    <? if (!empty($this->getParam('id_programacao'))) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Anexos da Programação</h5>
                        <div class="form-group pull-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Anexo Programação', 'Anexo_financeiro', 'add', array('id_programacao' => $this->getParam('id_programacao'), 'rota_id_externo' => $this->getParam('id_externo'),'rota_id_programacao' => $this->getParam('id_programacao'), 'tipo_doc' => $this->getParam('tipo_doc'), 'origem' => $this->getParam('origem')), array('class' => 'btn btn-primary')); ?></p>
                        </div>
                        <div class="form-group pull-right">
                            <button id="visualizadorProgramacao" type="button" style="margin-right: 10px" class="btn btn-primary">
                                Visualizar Anexos da Programação
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            Id
                                        </th>
                                        <th>
                                            Tipo
                                        </th>

                                        <th>
                                            Arquivo
                                        </th>

                                        <th>
                                            Criado Por
                                        </th>
                                        <th>
                                            Data Criação
                                        </th>
                                        <th>
                                            Alterado Por
                                        </th>
                                        <th>
                                            Data Alteração
                                        </th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    foreach ($Anexo_programacao as $a) {

                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->id, 'Anexo_financeiro', 'view',
                                            array('id' => $a->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        $tipoCriteria = new Criteria();
                                        $tipoCriteria->addCondition('id', '=', $a->tipo);
                                        $tipo = Anexo_tipo::getFirst($tipoCriteria);
                                        echo $this->Html->getLink($tipo->nome, 'Anexo_financeiro', 'view',
                                            array('id' => $a->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';

                                        echo $this->Html->getLink($a->titulo, 'Anexo_financeiro', 'view',
                                            array('id' => $a->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        echo $a->getUserCreate()->nome;
                                        echo '</td>';

                                        echo '<td>';
                                        echo DataBR($a->dtcriacao);
                                        echo '</td>';

                                        echo '<td>';
                                        echo $a->getUserAlter()->nome;
                                        echo '</td>';

                                        echo '<td>';
                                        echo DataBR($a->dtalteracao);
                                        echo '</td>';


                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Anexo_financeiro', 'edit',
                                            array('id' => $a->id,
                                                'rota_id_externo' => $this->getParam('id_externo'),
                                                'rota_id_programacao' => $this->getParam('id_programacao'),
                                                'tipo' => $a->tipo,
                                                'origem' => $this->getParam('origem')
                                            ),
                                            array('title' => 'Editar/Visualizar', 'class' => 'btn btn-warning btn-sm'));
                                        echo '</td>';

                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Anexo_financeiro', 'delete',
                                            array(
                                                'rota_id_externo' => $this->getParam('id_externo'),
                                                'rota_id_programacao' => $this->getParam('id_programacao'),
                                                'id' => $a->id,
                                                'tipo' => $a->tipo,
                                                'origem' => $this->getParam('origem')
                                            ),
                                            array('title' => 'Excluir', 'class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';

                                        echo '</tr>';
                                    }
                                    ?>
                                </table>

                            </div>
                        </div>
                        <script>
                            /* faz a pesquisa com ajax */
                            $(document).ready(function () {
                                $('#visualizadorProgramacao').on('click', function () {
                                    var conta = <?= $this->getParam('id_programacao');?>;
                                    var origem = <?= $this->getParam('origem');?>;
                                    var url = root + '/Anexo_financeiro/galeria/modal:1/ajax:true/origem:' + origem + '/id_programacao:' + conta + '/hide:1';

                                    $('<div></div>').load(url, function () {
                                        BootstrapDialog.show({
                                            title: 'Visualizar Documentos Anexados',
                                            message: this,
                                            size: 'size-wide',
                                            type: BootstrapDialog.TYPE_DEFAULT,
                                            onshown: function (dialog) {
                                                var body = dialog.getModalBody();
                                                initAnexos(body);
                                                dialog.getModal().removeAttr('tabindex');
                                            },
                                            buttons: [{
                                                label: 'Fechar',
                                                action: function (dialog) {
                                                    dialog.close();
                                                }
                                            }]
                                        });

                                    });
                                });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
    <? if (!empty($this->getParam('id_externo'))) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Anexos</h5>
                        <div class="form-group pull-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo Anexo', 'Anexo_financeiro', 'add', array('id_externo' => $this->getParam('id_externo'),'rota_id_externo' => $this->getParam('id_externo'),'rota_id_programacao' => $this->getParam('id_programacao'), 'tipo_doc' => $this->getParam('tipo_doc'), 'origem' => $this->getParam('origem')), array('class' => 'btn btn-primary')); ?></p>
                        </div>
                        <div class="form-group pull-right">
                            <button id="visualizador" type="button" style="margin-right: 10px" class="btn btn-primary">
                                Visualizar todos
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="ibox-content">

                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>
                                            Id
                                        </th>
                                        <th>
                                            Tipo
                                        </th>
                                        <th>
                                            Arquivo
                                        </th>
                                        <th>
                                            Criado Por
                                        </th>
                                        <th>
                                            Data Criação
                                        </th>
                                        <th>
                                            Alterado Por
                                        </th>
                                        <th>
                                            Data Alteração
                                        </th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php

                                    foreach ($Anexo_financeiros as $a) {

                                        echo '<tr>';
                                        echo '<td>';
                                        echo $this->Html->getLink($a->id, 'Anexo_financeiro', 'view',
                                            array('id' => $a->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        $tipoCriteria = new Criteria();
                                        $tipoCriteria->addCondition('id', '=', $a->tipo);
                                        $tipo = Anexo_tipo::getFirst($tipoCriteria);
                                        echo $this->Html->getLink($tipo->nome, 'Anexo_financeiro', 'view',
                                            array('id' => $a->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';
                                        echo '<td>';

                                        echo $this->Html->getLink($a->titulo, 'Anexo_financeiro', 'view',
                                            array('id' => $a->id), // variaveis via GET opcionais
                                            array('data-toggle' => 'modal')); // atributos HTML opcionais
                                        echo '</td>';

                                        echo '<td>';
                                        echo $a->getUserCreate()->nome;
                                        echo '</td>';

                                        echo '<td>';
                                        echo DataBR($a->dtcriacao);
                                        echo '</td>';

                                        echo '<td>';
                                        echo $a->getUserAlter()->nome;
                                        echo '</td>';

                                        echo '<td>';
                                        echo DataBR($a->dtalteracao);
                                        echo '</td>';


                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Anexo_financeiro', 'edit',
                                            array('id' => $a->id,
                                                'rota_id_externo' => $this->getParam('id_externo'),
                                                'rota_id_programacao' => $this->getParam('id_programacao'),
                                                'origem' => $this->getParam('origem')
                                            ),
                                            array('title' => 'Editar/Visualizar', 'class' => 'btn btn-warning btn-sm'));
                                        echo '</td>';

                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Anexo_financeiro', 'delete',
                                            array(
                                                'rota_id_externo' => $this->getParam('id_externo'),
                                                'rota_id_programacao' => $this->getParam('id_programacao'),
                                                'id' => $a->id,
                                                'tipo' => $a->tipo,
                                                'origem' => $this->getParam('origem')
                                            ),
                                            array('title' => 'Excluir', 'class' => 'btn btn-danger btn-sm', 'data-toggle' => 'modal'));
                                        echo '</td>';

                                        echo '</tr>';
                                    }
                                    ?>
                                </table>

                                <!-- menu de paginação -->

                            </div>
                        </div>

                        <script>
                            /* faz a pesquisa com ajax */
                            $(document).ready(function () {
                                $('#visualizador').on('click', function () {
                                    var conta = <?= $this->getParam('id_externo');?>;
                                    var origem = <?= $this->getParam('origem');?>;
                                    var url = root + '/Anexo_financeiro/galeria/modal:1/ajax:true/origem:' + origem + '/id_externo:' + conta + '/hide:1';

                                    $('<div></div>').load(url, function () {
                                        BootstrapDialog.show({
                                            title: 'Visualizar Documentos Anexados',
                                            message: this,
                                            size: 'size-wide',
                                            type: BootstrapDialog.TYPE_DEFAULT,
                                            onshown: function (dialog) {
                                                var body = dialog.getModalBody();
                                                initAnexos(body);
                                                dialog.getModal().removeAttr('tabindex');
                                            },
                                            buttons: [{
                                                label: 'Fechar',
                                                action: function (dialog) {
                                                    dialog.close();
                                                }
                                            }]
                                        });

                                    });
                                });

                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    <? } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div style="text-align:center"><?php echo $nav; ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function () {
        $('#clearForm').on('click', function () {
            // $(this).reset();
            $('#tipo').val('').trigger('change');
        });
    });
</script>

