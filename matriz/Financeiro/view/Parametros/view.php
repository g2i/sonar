<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Parāmetros</h2>
        <ol class="breadcrumb">
            <li> parāmetros</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <p><strong>Juros</strong>: <?php echo $Parametros->juros; ?></p>

                <p><strong>Multa</strong>: <?php echo $Parametros->multa; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Parametros->getStatus()->descricao, 'Status', 'view',
                        array('id' => $Parametros->getStatus()->id), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>