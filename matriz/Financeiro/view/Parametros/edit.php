<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Parâmetros</h2>
        <ol class="breadcrumb">
            <li> parâmetros</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Parametros', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="well well-lg">
                        <div class="form-group">
                            <label class="required" for="juros">Juros <span class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="number" name="juros" id="juros" class="form-control"
                                   value="<?php echo $Parametros->juros ?>" placeholder="Juros" required>
                        </div>
                        <div class="form-group">
                            <label class="required" for="multa">Multa <span class="glyphicon glyphicon-asterisk"></span></label>
                            <input type="number" name="multa" id="multa" class="form-control"
                                   value="<?php echo $Parametros->multa ?>" placeholder="Multa" required>
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select name="status" class="form-control" id="status">
                                <?php
                                foreach ($Status as $s) {
                                    if ($s->id == $Parametros->status)
                                        echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                    else
                                        echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Parametros->id; ?>">

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Parametros', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>