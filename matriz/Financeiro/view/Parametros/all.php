<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2> Parâmetros</h2>
        <ol class="breadcrumb">
            <li> parâmetros</li>
            <li class="active">
                <strong>Listar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="col-md-12">

                    <div class="row">
                        <!-- botao de cadastro -->
                        <div class="row text-right pull-right">
                            <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Cadastrar Parametros', 'Parametros', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
                        </div>

                        <!-- formulario de pesquisa -->
                        <div class="pull-left">
                            <form class="form-inline" role="form" method="get"
                                  action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">

                                <div class="form-group">
                                    <label class="sr-only" for="search">Pesquisar</label>
                                    <input value="<?php echo $search; ?>" type="search" class="form-control"
                                           name="search"
                                           id="search" placeholder="Pesquisar id">
                                </div>
                                <button type="submit" class="btn btn-default"><span
                                        class="glyphicon glyphicon-search"></span></button>
                            </form>
                        </div>

                    </div>
                    <!-- tabela de resultados -->
                    <div class="row clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Parametros', 'all', array('orderBy' => 'juros', 'search' => $search)); ?>'>
                                            Juros
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Parametros', 'all', array('orderBy' => 'multa', 'search' => $search)); ?>'>
                                            Multa
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl('Parametros', 'all', array('orderBy' => 'status', 'search' => $search)); ?>'>
                                            Status
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Parametros as $p) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $this->Html->getLink($p->juros . " %", 'Parametros', 'view',
                                        array('id' => $p->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($p->multa . " %", 'Parametros', 'view',
                                        array('id' => $p->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($p->getStatus()->descricao, 'Status', 'view',
                                        array('id' => $p->getStatus()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink('<span class="glyphicon update"></span> ', 'Parametros', 'edit',array('id' => $p->id),
                                        array('data-role'=>'tooltip' ,'data-placement'=>'bottom','title'=>'Editar'));
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink('<span class="glyphicon delet"></span> ', 'Parametros', 'delete',array('id' => $p->id),array('data-role'=>'tooltip' ,'data-placement'=>'bottom','title'=>'Deletar', 'data-toggle' => 'modal'));
                                    echo '</td>';
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center"><?php echo $nav; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function () {
        $('#search').keyup(function () {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                    <?php
                    if (isset($_GET['orderBy']))
                        echo '"' . $this->Html->getUrl('Parametros', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    else
                        echo '"' . $this->Html->getUrl('Parametros', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                    ?>
                    , function () {
                        r = true;
                    });
            }
        });
    });
</script>