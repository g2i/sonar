
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Estq_subestoque</h2>
    <ol class="breadcrumb">
    <li>Estq_subestoque</li>
    <li class="active">
    <strong>Adicionar Estq_subestoque</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Estq_subestoque', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="nome">nome <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Estq_subestoque->nome ?>" placeholder="Nome" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="usuario_id">usuario_id</label>
            <input type="number" name="usuario_id" id="usuario_id" class="form-control" value="<?php echo $Estq_subestoque->usuario_id ?>" placeholder="Usuario_id">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="usuario_dt">usuario_dt <span class="glyphicon glyphicon-asterisk"></span></label>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="origem">origem <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="number" name="origem" id="origem" class="form-control" value="<?php echo $Estq_subestoque->origem ?>" placeholder="Origem" required>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="situacao">situacao</label>
            <select name="situacao" class="form-control" id="situacao">
                <?php
                foreach ($Estq_situacaos as $e) {
                    if ($e->id == $Estq_subestoque->situacao)
                        echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                    else
                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="tipo">tipo</label>
            <select name="tipo" class="form-control" id="tipo">
                <?php
                foreach ($Estq_tiposubestoques as $e) {
                    if ($e->id == $Estq_subestoque->tipo)
                        echo '<option selected value="' . $e->id . '">' . $e->nome . '</option>';
                    else
                        echo '<option value="' . $e->id . '">' . $e->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Estq_subestoque', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>