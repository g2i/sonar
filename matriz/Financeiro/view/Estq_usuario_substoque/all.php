
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Estq_usuario_substoque</h2>
    <ol class="breadcrumb">
    <li>Estq_usuario_substoque</li>
    <li class="active">
    <strong>All</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Estq_usuario_substoque', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_usuario_substoque', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_usuario_substoque', 'all', array('orderBy' => 'usario_id')); ?>'>
                        usario_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_usuario_substoque', 'all', array('orderBy' => 'substoque_id')); ?>'>
                        substoque_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_usuario_substoque', 'all', array('orderBy' => 'situacao_id')); ?>'>
                        situacao_id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_usuario_substoque', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                        dt_cadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Estq_usuario_substoque', 'all', array('orderBy' => 'dt_modificacao')); ?>'>
                        dt_modificacao
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Estq_usuario_substoques as $e) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($e->id, 'Estq_usuario_substoque', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->dt_cadastro, 'Estq_usuario_substoque', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->dt_modificacao, 'Estq_usuario_substoque', 'view',
                    array('id' => $e->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->getUsuario()->id, 'Usuario', 'view',
                    array('id' => $e->getUsuario()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->getEstq_subestoque()->id, 'Estq_subestoque', 'view',
                    array('id' => $e->getEstq_subestoque()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($e->getSituacao()->id, 'Situacao', 'view',
                    array('id' => $e->getSituacao()->id), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Estq_usuario_substoque', 'edit', 
                    array('id' => $e->id), 
                    array('class' => 'btn btn-warning btn-sm'));
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Estq_usuario_substoque', 'delete', 
                    array('id' => $e->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Estq_usuario_substoque', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Estq_usuario_substoque', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>