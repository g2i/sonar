<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Centro de Custos</h2>
        <ol class="breadcrumb">
            <li>centro de custos</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Centro_custo', 'edit') ?>">
                    <div class="alert alert-info">Os campos marcados com <span
                            class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.
                    </div>
                    <div class="form-group">
                        <label class="required" for="descricao">Descricao <span
                                class="glyphicon glyphicon-asterisk"></span></label>
                        <input type="text" name="descricao" id="descricao" class="form-control"
                               value="<?php echo $Centro_custo->descricao ?>" placeholder="Descricao" required>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select name="status" class="form-control selectPicker" id="status">
                            <?php
                            foreach ($Status as $s) {
                                if ($s->id == $Centro_custo->status)
                                    echo '<option selected value="' . $s->id . '">' . $s->descricao . '</option>';
                                else
                                    echo '<option value="' . $s->id . '">' . $s->descricao . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $Centro_custo->id; ?>">

                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Centro_custo', 'all') ?>" class="btn btn-default"
                           data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>