<?php

function completaZeros($numero, $digitos) {
    while (strlen($numero) < $digitos) {
        $numero = 0 . $numero;
    }
    return $numero;
}

// +----------------------------------------------------------------------+
// | BoletoPhp - Vers�o Beta                                              |
// +----------------------------------------------------------------------+
// | Este arquivo est� dispon�vel sob a Licen�a GPL dispon�vel pela Web   |
// | em http://pt.wikipedia.org/wiki/GNU_General_Public_License           |
// | Voc� deve ter recebido uma c�pia da GNU Public License junto com     |
// | esse pacote; se n�o, escreva para:                                   |
// |                                                                      |
// | Free Software Foundation, Inc.                                       |
// | 59 Temple Place - Suite 330                                          |
// | Boston, MA 02111-1307, USA.                                          |
// +----------------------------------------------------------------------+
// +----------------------------------------------------------------------+
// | Originado do Projeto BBBoletoFree que tiveram colabora��es de Daniel |
// | William Schultz e Leandro Maniezo que por sua vez foi derivado do	  |
// | PHPBoleto de Jo�o Prado Maia e Pablo Martins F. Costa				        |
// | 														                                   			  |
// | Se vc quer colaborar, nos ajude a desenvolver p/ os demais bancos :-)|
// | Acesse o site do Projeto BoletoPhp: www.boletophp.com.br             |
// +----------------------------------------------------------------------+
// +----------------------------------------------------------------------+
// | Equipe Coordena��o Projeto BoletoPhp: <boletophp@boletophp.com.br>   |
// | Desenvolvimento Boleto Ita�: Glauber Portella                        |
// +----------------------------------------------------------------------+
// ------------------------- DADOS DIN�MICOS DO SEU CLIENTE PARA A GERA��O DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formul�rio c/ POST, GET ou de BD (MySql,Postgre,etc)	//
// DADOS DO BOLETO PARA O SEU CLIENTE

    $percentual="";
    foreach($Contareceber->DeducoesContasrecebercontasreceber_ids as $d){
        $percentual += (float)str_replace(',','.',$d->percentual);
    }
    $valorDeduzido = (float)$Contareceber->valorPagar * ((float) $percentual / 100);
    $desconto = $valorDeduzido+$Contareceber->desconto;

    $valorDeduzido = (float)$Contareceber->valorPagar - (float)$valorDeduzido;

    $vencimento = date('d/m/Y',strtotime($Contareceber->vencimentoBoleto));
    if(strtotime($Contareceber->vencimentoBoleto) < strtotime(date('Y-m-d')))
        $vencimento = date('d/m/Y',strtotime('+3 Days'));

    $Cliente = $Contareceber->getCliente();

    $totalPagar = $valorDeduzido;
    $moraMulta = $Contareceber->juros+$Contareceber->multa;
    $dias_de_prazo_para_pagamento = 3;
    $taxa_boleto = "";
    //$data_venc = date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400));  // Prazo de X dias OU informe data: "13/04/2006";
    $data_venc = $vencimento;
    $valor_cobrado = '' . number_format($totalPagar, 2, ",", ""); // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
//$dadosboleto["nosso_numero"] = '12345678';  // Nosso numero - REGRA: M�ximo de 8 caracteres!
    $dadosboleto["numero_documento"] = '0123'; // Num do pedido ou nosso numero
    $dadosboleto["desconto"] = '' . number_format($desconto, 2, ",", "");
    $dadosboleto["valor_cobrado"] = $valor_cobrado;
    $dadosboleto["outros_acrescimos"] = "";
//    $dadosboleto["desconto_abatimentos"] = $boleto->desconto;
//    $dadosboleto["valor_cobrado"] = $valor_boleto - $boleto->desconto;

    $dadosboleto["data_vencimento"] = $vencimento; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
    $dadosboleto["data_documento"] = date("d/m/Y"); // Data de emiss�o do Boleto
    $dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
    $dadosboleto["valor_documento"] = number_format($Contareceber->valor, 2, ",", "");  // Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula
    $dadosboleto["moraMulta"] = number_format($moraMulta, 2, ",", "");  // Valor do Boleto - REGRA: Com v�rgula e sempre com duas casas depois da virgula
// DADOS DO SEU CLIENTE
    $dadosboleto["sacado"] = $Cliente->cliente;
    $dadosboleto["endereco1"] = $Cliente->endereco.', '.$Cliente->bairro;
    $dadosboleto["endereco2"] = "" . $Cliente->cidade . " " . $Cliente->uf;

// INFORMACOES PARA O CLIENTE
    $dadosboleto["demonstrativo1"] = "";
    $dadosboleto["demonstrativo2"] = "<br> ";
    $dadosboleto["demonstrativo3"] = "";
    $dadosboleto["instrucoes1"] = "N&atilde;o aceitar pagamento ap&oacute;s o vencimento.";
    $dadosboleto["instrucoes2"] = "Em caso de d&uacute;vidas entre em contato conosco: financeiro@chiesa.com.br";
    $dadosboleto["instrucoes3"] = "";
    $dadosboleto["instrucoes4"] = "";

// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
    $dadosboleto["quantidade"] = "";
    $dadosboleto["valor_unitario"] = "";
    $dadosboleto["aceite"] = "";
    $dadosboleto["especie"] = "R$";
    $dadosboleto["especie_doc"] = "";


// ---------------------- DADOS FIXOS DE CONFIGURA��O DO SEU BOLETO --------------- //
// DADOS DA SUA CONTA - ITA�
    $dadosboleto["agencia"] = $Cedente->codAgencia; // Num da agencia, sem digito
    $dadosboleto["conta"] = $Cedente->codConta; // Num da conta, sem digito
    $dadosboleto["conta_dv"] = $Cedente->contadv;  // Digito do Num da conta
// DADOS PERSONALIZADOS - ITA�
    $dadosboleto["carteira"] = $Cedente->cedenteCarteira;  // C�digo da Carteira: pode ser 175, 174, 104, 109, 178, ou 157
// SEUS DADOS
    $dadosboleto["identificacao"] = $Cedente->cedente;
    $dadosboleto["cpf_cnpj"] = $Cedente->cnpj;
    $dadosboleto["endereco"] = $Cedente->endereco.",".$Cedente->numero.", ".$Cedente->bairro;
    $dadosboleto["cidade_uf"] = $Cedente->cidade."-".$Cedente->estado;
    $dadosboleto["cedente"] = strtoupper($Cedente->cedente);
//nosso numero
//    $emp = completaZeros($_POST['empresa_id'], 4);
//    $nnum = date('y') . date('m') . $emp;
    $dadosboleto["nosso_numero"] = "99" . str_pad($Contareceber->id, 6, 0, STR_PAD_LEFT);
//    $dadosboleto["nosso_numero"] = "99" . str_pad($boleto->id, 6, 0, STR_PAD_LEFT);
//$nossonumero = $dadosboleto["carteira"].'/'.$nnum.'-'.modulo_10($dadosboleto["agencia"].$dadosboleto["conta"].$dadosboleto["conta_dv"].$dadosboleto["carteira"].$nnum);
// N�O ALTERAR!
    include("include/funcoes_itau.php");
    include("include/layout_itau.php");

?>
<script type="text/javascript">
    if('<?php echo $print; ?>'==1){
        print();
    }
</script>
