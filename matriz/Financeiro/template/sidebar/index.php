<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php $this->getHeaders(); ?>
    <style>
        body {

        }

        .starter-template {
            padding: 0px 15px;
        }
    </style>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="col-md-2 col-sm-3 col-xs-12 navbar-black">
    <ul class="menu">
        <li><?php echo $this->Html->getLink(__('Início'), Config::get('indexController'), Config::get('indexAction')); ?></li>
        <?php include 'template/menu.php' ?>
    </ul>
</div>
<div class="col-md-10 col-sm-9 col-xs-12 container-block">

    <div class="navbar navbar-inverse hidden-xs" role="navigation">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><?php echo Config::get('titulo'); ?></a>
        </div>
    </div>
    <div class="container-fluid">
        <div class="starter-template">
            <?php $this->getContents(); ?>
        </div>
    </div>
    <!-- /.container -->
</div>
<div class="clearfix"></div>

<!-- Generic Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <div class="modal-dialog">
        <div class="modal-content">
            <div style="text-align:center">
                <img src="<?php echo SITE_PATH; ?>/template/default/images/loading.gif" alt="LazyPHP">
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div style="text-align:center"><img src="<?php echo SITE_PATH;?>/template/default/images/loading.gif" alt="LazyPHP"></div>
        </div>
    </div>
</div>

</body>
</html>
