<li class="nav-header">
    <div class="dropdown profile-element">
    <span>
    <?php echo(file_exists(SITE_PATH . "/content/img/" . sprintf("%06d", @Session::get("user")->id) . ".png") ? '<img class="img-circle" src="' . SITE_PATH . '/content/img/' . sprintf("%06d", @Session::get("user")->id) . '.png" alt="Foto"/>' : '<img class="img-circle" src="' . SITE_PATH . '/content/img/empty-avatar.png" alt="Foto"/>') ?>
    </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
    <span class="clear"> <span class="block m-t-xs"> <strong
                class="font-bold"><?php echo @Session::get("user")->nome; ?></strong>
    </span> <span class="text-muted text-xs block">Art Director <b class="caret"></b></span> </span> </a>
        <ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li><?php echo $this->Html->getLink('Sair', 'Login', 'logout'); ?></li>
        </ul>
    </div>
    <div class="logo-element">
        G2i
    </div>
</li>

<li <?php echo(CONTROLLER == "Index" ? 'class="active"' : ''); ?>>
    <?php echo $this->Html->getLink('<i class="fa fa-line-chart"></i><span class="nav-label">Financeiro</span><span class="fa arrow"></span>', 'Index', 'index'); ?>
</li>

<li <?php echo(CONTROLLER == "Contasreceber" || (CONTROLLER == "Programacao" && ACTION == "contasReceber") ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-money"></i><span class="nav-label">Contas a Receber</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Contasreceber" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-money"></i> Gerenciamento', 'Contasreceber', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Contasreceber" && ACTION == "lista" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-money"></i> Quita&ccedil;&atilde;o', 'Contasreceber', 'lista'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Programacao" && ACTION == "contasReceber" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-money"></i> Programa&ccedil;&otilde;es', 'Programacao', 'contasReceber'); ?>
        </li>
    </ul>
</li>

<li <?php echo(CONTROLLER == "Contaspagar" || (CONTROLLER == "Programacao" && ACTION == "contasPagar") ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-usd"></i><span class="nav-label">Contas a Pagar</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Contaspagar" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-usd"></i> Gerenciamento', 'Contaspagar', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Contaspagar" && ACTION == "lista" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-usd"></i> Quita&ccedil;&atilde;o', 'Contaspagar', 'lista'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Programacao" && ACTION == "contasPagar" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-usd"></i> Programa&ccedil;&otilde;es', 'Programacao', 'contasPagar'); ?>
        </li>
    </ul>
</li>

<li <?php echo(CONTROLLER == "Movimento" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-exchange"></i><span class="nav-label">Movimento</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Movimento" && ACTION == "all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Movimenta&ccedil;&atilde;o', 'Movimento', 'all'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Movimento" && ACTION == "controle" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Controle', 'Movimento', 'controle'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Movimento" && ACTION == "new_all" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-exchange"></i> Movimenta&ccedil;&atilde;o Geral', 'Movimento', 'new_all'); ?>
        </li>
    </ul>
</li>

<li <?php echo(CONTROLLER == "Relatorio" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-file-code-o"></i><span class="nav-label">Relatórios</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Relatorio" && ACTION == "mensal" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-file-code-o"></i> Demonstrativo Mensal', 'Relatorio', 'mensal'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Relatorio" && ACTION == "mapa" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-file-code-o"></i> Análise Anual -  Analítico', 'Relatorio', 'mapa'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Relatorio" && ACTION == "simplificado" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-file-code-o"></i> Análise Anual -  Sintético', 'Relatorio', 'simplificado'); ?>
        </li>
    </ul>
</li>

<li <?php echo(CONTROLLER == "Grafico" ? 'class="active"' : ''); ?>>
    <a href="#"><i class="fa fa-bar-chart"></i><span class="nav-label">Análise Grafica</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li <?php echo(CONTROLLER == "Grafico" && ACTION == "geral" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-bar-chart"></i> Resultado Geral', 'Grafico', 'geral'); ?>
        </li>
        <li <?php echo(CONTROLLER == "Grafico" && ACTION == "especifico" ? 'class="active"' : ''); ?>>
            <?php echo $this->Html->getLink('<i class="fa fa-bar-chart"></i> Resultado Específico', 'Grafico', 'especifico'); ?>
        </li>
    </ul>
</li>

 <li>
     <a href="http://www.g2ioperacional.com.br/JS/matriz/solicitacoes/"><i class="fa fa-clone"></i>Solicitação</a>
 </li>
