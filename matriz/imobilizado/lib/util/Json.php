<?php

class Json {
    const DIRECTORY_FILES_UPLOAD = 'report/';
    public function path(){
        $path = dirname(dirname(dirname(__FILE__))) .DIRECTORY_SEPARATOR . "Relatorios" .DIRECTORY_SEPARATOR ."reports".DIRECTORY_SEPARATOR ."json";
        $dir =  mkdir($path, 0755, true);
        if(!$dir){
            $dir->create($path,0755);
        } 
        return $dir->path;
    }
    public function jsonFolder()
    {
        return dirname(dirname(dirname(__FILE__))) .DIRECTORY_SEPARATOR . "Relatorios" .DIRECTORY_SEPARATOR ."reports".DIRECTORY_SEPARATOR ."json";
    }

    function create($file_name,$data){
        mkdir($this->jsonFolder().DIRECTORY_SEPARATOR, 0755, true);
        $file = fopen($this->jsonFolder().'/'.$file_name, "w+");
        fwrite($file,$data);
        fclose($file);
    }
}