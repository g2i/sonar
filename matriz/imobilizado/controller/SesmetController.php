<?php

class SesmetController extends AppController
{
    public function all()
    {
        $this->setTitle('Listagem de Ocorrência');
        $c = new Criteria();
        if(!empty($this->getParam('codigo_profissional'))) {
            $c->addCondition('codigo_profissional', "LIKE", "%" . $this->getParam('codigo_profissional') . "%");
        }
        if(!empty($this->getParam('historico'))) {
            $c->addCondition('historico', "LIKE", "%" . $this->getParam('historico') . "%");

           // $c->addCondition('historico', "=", $this->getParam('historico'));
        }
        if(!empty($this->getParam('inicio'))) {
            $inicio = $this->getParam('inicio');
            $fim = $this->getParam('fim');
            $c->addCondition('data_especifica','>=', $inicio);
            $c->addCondition('data_especifica','<=', $fim);
        }else{
            $c->addCondition('data_especifica','<', date('Y-m-d'));
        }

        
        $c->addCondition('status','=', 1);     
       /* $c->addCondition('historico','!=', 3);
        $c->addCondition('historico','!=', 14);
        $c->addCondition('historico','!=', 4);
        $c->addCondition('historico','!=', 9);  */
        $c->addCondition('historico','!=', 27);
        $c->addCondition('historico','!=', 28);
        $c->addCondition('historico','!=', 29);
        /*$c->addCondition('historico','!=', 1);
        $c->addCondition('historico','!=', 21);
        $c->addCondition('historico','!=', 20);
        $c->addCondition('historico','!=', 19);
        $c->addCondition('historico','!=', 18);
        $c->addCondition('historico','!=', 30);
        $c->addCondition('historico','!=', 2);*/
        $c->setOrder('data ASC');
    

        $this->set('RhocorrenciasAtrasadas',  Rhocorrencia::getList($c));
        $ocor = new Rhocorrencia();
        $this->set('ocor',$ocor->getRhocorrencia());
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        $gg = new Criteria();
        $gg->addCondition('status', '=', 1);
        $this->set('Rhprofissionais',  Rhprofissional::getList($gg));
        $this->set('Usuario',  Usuario::getList());
        $this->set('Rhstatus',  Rhstatus::getList());



        //****************************************************************** */
        $this->setTitle('Início');
        //$c->addSqlConditions('frota_motorista.validade_cnh < CURRENT_DATE()');

      $ocorencias_vencidas = $this->query_count("SELECT COUNT(o.`codigo_profissional`) AS totale
        FROM rhocorrencia o
        LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
        LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
        LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
        WHERE o.`status` != 3
        AND o.`status` != 4
        AND p.`status` != 3
        AND oh.codigo != 27
        AND oh.codigo != 28
        AND oh.codigo != 29
        AND CURDATE() > o.`data_especifica`
        AND oh.`periodicidade` != 'Nenhuma'
        AND C.`tipo` != 'Demissão'");
            $this->set('ocorencias_vencidas',$ocorencias_vencidas);

        $ocorrencias = new Rhocorrencia();
        $this->set('ocorrencias',$ocorrencias->getTabela_ocorrencia());
       
        //monta a query para o sesmet
        $ocor = new Rhocorrencia();
        $this->set('ocor', $ocor->sesmetRhocorrenciaVencidas());
        //query para buscar todas as ocorrencias com vencimento daqui 45 dias
        $this->set('vencimentoCom45Dias',sesmetAlertOcorrencias());

        //query para contar as ocorrencias com vencimento daqui 45 dias apartir da data atual
        $ocorenciasCont = $this->query_count("SELECT COUNT(o.`codigo`) AS totale
        FROM rhocorrencia o
        LEFT JOIN `rhprofissional`  p ON p.`codigo` = o.`codigo_profissional`
        LEFT JOIN `rhocorrencia_historico` oh ON oh.`codigo` = o.`historico`
        LEFT JOIN rhprofissional_contratacao C  ON C.`codigo_profissional` = o.`codigo_profissional`
        LEFT JOIN ocorencia_modulo om ON om.`rhocorrencia_historico_id` =  o.`codigo`
        WHERE o.`status` != 3
        AND o.`status` != 4
        AND p.`status` != 3
        AND oh.codigo != 27
        AND oh.codigo != 28  
        AND oh.codigo != 29
        AND oh.`periodicidade` != 'Nenhuma'
        AND C.`tipo` != 'Demissão'
         AND o.`data_especifica` >= CURDATE() AND o.`data_especifica` <=  DATE_ADD(CURDATE(),INTERVAL 45 DAY)");
        $this->set('ocorenciasCont',$ocorenciasCont);    
}


    function setor(){
            $setor = $this->query("SELECT S.nome as label, COUNT(P.codigo) as value
                        FROM rhprofissional P
                          INNER JOIN rhsetor S
                          on S.codigo = P.setor
                        WHERE P.setor IS NOT NULL GROUP BY P.setor");

            echo json_encode($setor);
        exit;
    }

 

    function seg_periodo(){
        $this->setTitle('Segundo período');
        $periodo = $this->query("SELECT P.codigo,P.nome,F.nome as funcao, C.`local_trabalho`, C.dtInicial, DATE_ADD(C.dtInicial,INTERVAL 90 DAY) as termino  FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      LEFT join rhprofissional_funcao F
                      on F.codigo = C.funcao
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 90 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 90 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('seg_periogo',$periodo);
    }
    function prim_periodo(){
        $this->setTitle('Primeiro período');
        $periodo = $this->query("SELECT P.codigo,P.nome,F.nome as funcao, C.`local_trabalho`, C.dtInicial, DATE_ADD(C.dtInicial,INTERVAL 90 DAY) as termino  FROM rhprofissional P
                      LEFT JOIN rhprofissional_contratacao C
                      ON C.codigo_profissional = P.codigo
                      LEFT join rhprofissional_funcao F
                      on F.codigo = C.funcao
                      WHERE C.tipo = 'Admissão' AND
                      DATE_ADD(C.dtInicial,INTERVAL 45 DAY) >= CURDATE()
                      AND DATE_ADD(C.dtInicial,INTERVAL 45 DAY) <= DATE_ADD(CURDATE(),INTERVAL 30 DAY)");

        $this->set('prim_periogo',$periodo);
    }

    function admissao (){
        $this->setTitle('Admissão');
        $admissao =  $this->query("SELECT  P.nome, C.`dtInicial` AS contratacao, 
        C.`local_trabalho`, F.nome AS funcao 
        FROM rhprofissional P
        LEFT JOIN rhprofissional_contratacao C ON C.codigo_profissional = P.codigo
        LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
        WHERE C.tipo = 'Admissão' AND
        C.dtInicial >= DATE_SUB(CURDATE(),INTERVAL 30 DAY)");
    
        $this->set('admissao',$admissao);
    }
    function demissao(){
        $this->setTitle('Demissão');
        $demissao = $this->query("SELECT  P.nome, C.`dtFinal` AS contratacao, 
        C.`local_trabalho`, F.nome AS funcao 
        FROM rhprofissional P
        LEFT JOIN rhprofissional_contratacao C ON C.codigo_profissional = P.codigo
        LEFT JOIN rhprofissional_funcao F   ON F.codigo = C.funcao
        WHERE C.tipo = 'Demissão' AND C.dtFinal >= DATE_SUB(CURDATE(),INTERVAL 30 DAY)");

        $this->set('demissao', $demissao);
    }
}