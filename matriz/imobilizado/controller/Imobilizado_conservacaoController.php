<?php
final class Imobilizado_conservacaoController extends AppController{ 

    # página inicial do módulo Imobilizado_conservacao
    function index(){
        $this->setTitle('Visualização de Imobilizado_conservacao');
    }

    # lista de Imobilizado_conservacaos
    # renderiza a visão /view/Imobilizado_conservacao/all.php
    function all(){
        $this->setTitle('Listagem de Imobilizado_conservacao');
        $p = new Paginate('Imobilizado_conservacao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Imobilizado_conservacaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Imobilizado_conservacao
    # renderiza a visão /view/Imobilizado_conservacao/view.php
    function view(){
        $this->setTitle('Visualização de Imobilizado_conservacao');
        try {
            $this->set('Imobilizado_conservacao', new Imobilizado_conservacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_conservacao', 'all');
        }
    }

    # formulário de cadastro de Imobilizado_conservacao
    # renderiza a visão /view/Imobilizado_conservacao/add.php
    function add(){
        $this->setTitle('Cadastro de Imobilizado_conservacao');
        $this->set('Imobilizado_conservacao', new Imobilizado_conservacao);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Imobilizado_conservacao
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_conservacao/add.php
    function post_add(){
        $this->setTitle('Cadastro de Imobilizado_conservacao');
        $Imobilizado_conservacao = new Imobilizado_conservacao();
        $this->set('Imobilizado_conservacao', $Imobilizado_conservacao);
        try {
            $Imobilizado_conservacao->situacao_id = 1;
            $Imobilizado_conservacao->cadastrado_por = Session::get('user')->id;
            $Imobilizado_conservacao->dt_cadastro = date('Y-m-d H:i:d');
            $Imobilizado_conservacao->save($_POST);
            new Msg(__('Estado de Conservação cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_conservacao', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Imobilizado_conservacao
    # renderiza a visão /view/Imobilizado_conservacao/edit.php
    function edit(){
        $this->setTitle('Edição de Imobilizado_conservacao');
        try {
            $this->set('Imobilizado_conservacao', new Imobilizado_conservacao((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Imobilizado_conservacao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Imobilizado_conservacao
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_conservacao/edit.php
    function post_edit(){
        $this->setTitle('Edição de Imobilizado_conservacao');
        try {
            $Imobilizado_conservacao = new Imobilizado_conservacao((int) $_POST['id']);
            $this->set('Imobilizado_conservacao', $Imobilizado_conservacao);
            $Imobilizado_conservacao->modificado_por = Session::get('user')->id;
            $Imobilizado_conservacao->dt_modificado = date('Y-m-d H:i:d');
            $Imobilizado_conservacao->save($_POST);
            new Msg(__('Estado de Conservação atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_conservacao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Imobilizado_conservacao
    # renderiza a /view/Imobilizado_conservacao/delete.php
    function delete(){
        $this->setTitle('Apagar Imobilizado_conservacao');
        try {
            $this->set('Imobilizado_conservacao', new Imobilizado_conservacao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_conservacao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Imobilizado_conservacao
    # redireciona para Imobilizado_conservacao/all
    function post_delete(){
        try {
            $Imobilizado_conservacao = new Imobilizado_conservacao((int) $_POST['id']);
            $Imobilizado_conservacao->situacao_id = 3;
            $Imobilizado_conservacao->save();
            new Msg(__('Estado de Conservação  apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if (!empty($_POST['modal'])) {
            echo 1;
            exit;
        }
        $this->go('Imobilizado_conservacao', 'all');
    }

}