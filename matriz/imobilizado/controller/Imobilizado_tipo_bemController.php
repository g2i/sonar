<?php
final class Imobilizado_tipo_bemController extends AppController{ 

    # página inicial do módulo Imobilizado_tipo_bem
    function index(){
        $this->setTitle('Visualização de Imobilizado_tipo_bem');
    }

    # lista de Imobilizado_tipo_bens
    # renderiza a visão /view/Imobilizado_tipo_bem/all.php
    function all(){
        $this->setTitle('Listagem de Imobilizado_tipo_bem');
        $p = new Paginate('Imobilizado_tipo_bem', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Imobilizado_tipo_bens', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Imobilizado_tipo_bem
    # renderiza a visão /view/Imobilizado_tipo_bem/view.php
    function view(){
        $this->setTitle('Visualização de Imobilizado_tipo_bem');
        try {
            $this->set('Imobilizado_tipo_bem', new Imobilizado_tipo_bem((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_tipo_bem', 'all');
        }
    }

    # formulário de cadastro de Imobilizado_tipo_bem
    # renderiza a visão /view/Imobilizado_tipo_bem/add.php
    function add(){
        $this->setTitle('Cadastro de Imobilizado_tipo_bem');
        $this->set('Imobilizado_tipo_bem', new Imobilizado_tipo_bem);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Imobilizado_tipo_bem
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_tipo_bem/add.php
    function post_add(){
        $this->setTitle('Cadastro de Imobilizado_tipo_bem');
        $Imobilizado_tipo_bem = new Imobilizado_tipo_bem();
        $this->set('Imobilizado_tipo_bem', $Imobilizado_tipo_bem);
        try {
            $Imobilizado_tipo_bem->cadastrado_por = Session::get('user')->id;
            $Imobilizado_tipo_bem->situacao_id = 1;
            $Imobilizado_tipo_bem->dt_cadastro = date('Y-m-d H:i:d');
            $Imobilizado_tipo_bem->save($_POST);
            new Msg(__('Imobilizado_tipo_bem cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_tipo_bem', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Imobilizado_tipo_bem
    # renderiza a visão /view/Imobilizado_tipo_bem/edit.php
    function edit(){
        $this->setTitle('Edição de Imobilizado_tipo_bem');
        try {
            $this->set('Imobilizado_tipo_bem', new Imobilizado_tipo_bem((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Imobilizado_tipo_bem', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Imobilizado_tipo_bem
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_tipo_bem/edit.php
    function post_edit(){
        $this->setTitle('Edição de Imobilizado_tipo_bem');
        try {
            $Imobilizado_tipo_bem = new Imobilizado_tipo_bem((int) $_POST['id']);
            $this->set('Imobilizado_tipo_bem', $Imobilizado_tipo_bem);
            $Imobilizado_tipo_bem->modificado_por = Session::get('user')->id;
            $Imobilizado_tipo_bem->dt_modificado = date('Y-m-d H:i:d');
            $Imobilizado_tipo_bem->save($_POST);
            new Msg(__('Imobilizado_tipo_bem atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_tipo_bem', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Imobilizado_tipo_bem
    # renderiza a /view/Imobilizado_tipo_bem/delete.php
    function delete(){
        $this->setTitle('Apagar Imobilizado_tipo_bem');
        try {
            $this->set('Imobilizado_tipo_bem', new Imobilizado_tipo_bem((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_tipo_bem', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Imobilizado_tipo_bem
    # redireciona para Imobilizado_tipo_bem/all
    function post_delete(){
        try {
            $Imobilizado_tipo_bem = new Imobilizado_tipo_bem((int) $_POST['id']);
            $Imobilizado_tipo_bem->situacao_id = 3;
            $Imobilizado_tipo_bem->save();
            new Msg(__('Imobilizado_tipo_bem apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if (!empty($_POST['modal'])) {
            echo 1;
            exit;
        }
        $this->go('Imobilizado_tipo_bem', 'all');
    }

}