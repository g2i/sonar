<?php
final class Sesmet_perguntaController extends AppController{ 

    # página inicial do módulo Sesmet_pergunta
    function index(){
        $this->setTitle('Visualização');
    }

    # lista de Sesmet_perguntas
    # renderiza a visão /view/Sesmet_pergunta/all.php
    function all(){
        $this->setTitle('Lista');
        $p = new Paginate('Sesmet_pergunta', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Sesmet_perguntas', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Sesmet_grupos',  Sesmet_grupo::getList());
    

    }

    # visualiza um(a) Sesmet_pergunta
    # renderiza a visão /view/Sesmet_pergunta/view.php
    function view(){
        $this->setTitle('Visualização');
        try {
            $this->set('Sesmet_pergunta', new Sesmet_pergunta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_pergunta', 'all');
        }
    }

    # formulário de cadastro de Sesmet_pergunta
    # renderiza a visão /view/Sesmet_pergunta/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Sesmet_pergunta', new Sesmet_pergunta);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Sesmet_grupos',  Sesmet_grupo::getList());
    }

    # recebe os dados enviados via post do cadastro de Sesmet_pergunta
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_pergunta/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Sesmet_pergunta = new Sesmet_pergunta();
        $this->set('Sesmet_pergunta', $Sesmet_pergunta);
        try {
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_pergunta->situacao_id = 1;
            $Sesmet_pergunta->cadastrado_por = $user->id;
            $Sesmet_pergunta->dt_cadastro = date('Y-m-d H:i:s');
            $Sesmet_pergunta->save($_POST);
            new Msg(__('Sesmet_pergunta cadastrado com sucesso'));
            $this->go('Sesmet_pergunta', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Sesmet_grupos',  Sesmet_grupo::getList());
    }

    # formulário de edição de Sesmet_pergunta
    # renderiza a visão /view/Sesmet_pergunta/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Sesmet_pergunta', new Sesmet_pergunta((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Sesmet_grupos',  Sesmet_grupo::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Sesmet_pergunta', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Sesmet_pergunta
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_pergunta/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Sesmet_pergunta = new Sesmet_pergunta((int) $_POST['id']);
            $this->set('Sesmet_pergunta', $Sesmet_pergunta);
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_pergunta->modificado_por = $user->id;
            $Sesmet_pergunta->dt_modificado = date('Y-m-d H:i:s');
            $Sesmet_pergunta->save($_POST);
            new Msg(__('Sesmet_pergunta atualizado com sucesso'));
            $this->go('Sesmet_pergunta', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Sesmet_grupos',  Sesmet_grupo::getList());
    }

    # Confirma a exclusão ou não de um(a) Sesmet_pergunta
    # renderiza a /view/Sesmet_pergunta/delete.php
    function delete(){
        $this->setTitle('Apagar Sesmet_pergunta');
        try {
            $this->set('Sesmet_pergunta', new Sesmet_pergunta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_pergunta', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Sesmet_pergunta
    # redireciona para Sesmet_pergunta/all
    function post_delete(){
        try {
            $Sesmet_pergunta = new Sesmet_pergunta((int) $_POST['id']);
            $Sesmet_pergunta->delete();
            new Msg(__('Sesmet_pergunta apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Sesmet_pergunta', 'all');
    }

}