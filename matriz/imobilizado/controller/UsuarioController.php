<?php
final class UsuarioController extends AppController{ 

    # página inicial do módulo Usuario
    function index(){
        $this->setTitle('Visualização de Usuario');
    }

    # lista de Usuarios
    # renderiza a visão /view/Usuario/all.php
    function all(){
        $this->setTitle('Listagem de Usuario');
        $p = new Paginate('Usuario', 10);
        $c = new Criteria();
            if ($this->getParam('orderBy')) {
                $c->setOrder($this->getParam('orderBy'));
            }
            $this->set('Usuarios', $p->getPage($c));
            $this->set('nav', $p->getNav());
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # visualiza um(a) Usuario
    # renderiza a visão /view/Usuario/view.php
    function view(){
        $this->setTitle('Visualização de Usuario');
        $this->set('Rhstatus',  Rhstatus::getList());
        try {
            $this->set('Usuario', new Usuario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'all');
        }
    }

    # formulário de cadastro de Usuario
    # renderiza a visão /view/Usuario/add.php
    function add(){
        $this->setTitle('Cadastro de Usuario');
        $this->set('Usuario', new Usuario);
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # recebe os dados enviados via post do cadastro de Usuario
    # (true)redireciona ou (false) renderiza a visão /view/Usuario/add.php
    function post_add(){
        $this->setTitle('Cadastro de Usuario');
        $Usuario = new Usuario();
        $this->set('Usuario', $Usuario);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Usuario->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Usuario->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $_POST['senha'] = md5(Config::get('salt') . $_POST['senha']);
            $Usuario->save($_POST);
            new Msg(__('Usuario cadastrado com sucesso'));
            $this->go('Usuario', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # formulário de edição de Usuario
    # renderiza a visão /view/Usuario/edit.php
    function edit(){
        $this->setTitle('Edição de Usuario');
        try {
            $this->set('Usuarios', new Usuario($this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Usuario', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Usuario
    # (true)redireciona ou (false) renderiza a visão /view/Usuario/edit.php
    function post_edit(){
        $this->setTitle('Edição de Usuario');
        try {
            $Usuario = new Usuario((int) $_POST['id']);
            $this->set('Usuario', $Usuario);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Usuario->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Usuario->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Usuario->save($_POST);
            new Msg(__('Usuario atualizado com sucesso'));
            $this->go('Usuario', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
    }

    # Confirma a exclusão ou não de um(a) Usuario
    # renderiza a /view/Usuario/delete.php
    function delete(){
        $this->setTitle('Apagar Usuario');
        try {
            $this->set('Usuario', new Usuario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Usuario', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Usuario
    # redireciona para Usuario/all
    function post_delete(){
        try {
            $Usuario = new Usuario((int) $_POST['id']);
            $user=Session::get('user');
            $Usuario->atualizadopor=$user->id;
            $Usuario->dtAtualizacao=date('Y-m-d H:i:s');
            $Usuario->status=3;
            $Usuario->save();
            new Msg(__('Usuario deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Usuario', 'all');
    }

    function alterar_senha(){
        $this->setTitle("Alterar Senha");
        $user = Session::get('user');
        $this->set('Usuario',$user);
    }

    function post_alterar_senha(){
        try {
            $Usuario = new Usuario((int) $_POST['id']);
            $Usuario->senha = md5(Config::get('salt').$_POST['senha']);
            $Usuario->save();
            new Msg(__('Senha Alterada com sucesso!'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Usuario', 'conta');
    }

    function resetar_senha()
    {
        $this->setTitle("Resetar Senha");
        $user = Session::get('user');
        $this->set('Usuario', $user);
    }

    function post_resetar_senha()
    {
        try {
            $Usuario = new Usuario((int)$_POST['id']);
            $Usuario->senha = md5(Config::get('salt') . $_POST['senha']);
            if($Usuario->primeiro_acesso == 1) {
                $Usuario->primeiro_acesso = 0;
            }

            $Usuario->save();
            new Msg(__('Senha resetada com sucesso!'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(), 3);
        }
        Session::set('user', NULL);
        $this->go('Login', 'login');
    }


    function conta(){
        $this->setTitle('Conta');
        $user = Session::get('user');
        $this->set('Usuario',$user);
    }
}