<?php
final class Sesmet_inspecao_funcionarioController extends AppController{ 

    # página inicial do módulo Sesmet_inspecao_funcionario
    function index(){
        $this->setTitle('Visualização de Sesmet_inspecao_funcionario');
    }

    # lista de Sesmet_inspecao_funcionarios
    # renderiza a visão /view/Sesmet_inspecao_funcionario/all.php
    function all(){
        $this->setTitle('Listagem de Sesmet_inspecao_funcionario');
        $p = new Paginate('Sesmet_inspecao_funcionario', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('sesmet_inspecao_id','=', $this->getParam('id'));
        $this->set('Sesmet_inspecao_funcionarios', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    

    }

    # visualiza um(a) Sesmet_inspecao_funcionario
    # renderiza a visão /view/Sesmet_inspecao_funcionario/view.php
    function view(){
        $this->setTitle('Visualização de Sesmet_inspecao_funcionario');
        try {
            $this->set('Sesmet_inspecao_funcionario', new Sesmet_inspecao_funcionario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_inspecao_funcionario', 'all');
        }
    }

    # formulário de cadastro de Sesmet_inspecao_funcionario
    # renderiza a visão /view/Sesmet_inspecao_funcionario/add.php   $this->getParam('id')
    function add(){
        $this->setTitle('Cadastro de Sesmet_inspecao_funcionario');
        $this->set('Sesmet_inspecao_funcionario', new Sesmet_inspecao_funcionario);

        $c = new Criteria();
        $c->addCondition('id','=', $this->getParam('id'));
        $g = new Criteria();
        $g->setOrder('nome ASC');
        $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList($c));
        $this->set('Rhprofissionais',  Rhprofissional::getList($g)); 
    }

    # recebe os dados enviados via post do cadastro de Sesmet_inspecao_funcionario
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_inspecao_funcionario/add.php
    function post_add(){
        $this->setTitle('Cadastro de Sesmet_inspecao_funcionario');
        $Sesmet_inspecao_funcionario = new Sesmet_inspecao_funcionario();
        $this->set('Sesmet_inspecao_funcionario', $Sesmet_inspecao_funcionario);
        try {
            $Sesmet_inspecao_funcionario->save($_POST);
            new Msg(__('Sesmet_inspecao_funcionario cadastrado com sucesso'));
             //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
            $this->go('Sesmet_inspecao_funcionario', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # formulário de edição de Sesmet_inspecao_funcionario
    # renderiza a visão /view/Sesmet_inspecao_funcionario/edit.php
    function edit(){
        $this->setTitle('Edição de Sesmet_inspecao_funcionario');
        try {
            $this->set('Sesmet_inspecao_funcionario', new Sesmet_inspecao_funcionario((int) $this->getParam('id')));
            $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
            $this->set('Rhprofissionais',  Rhprofissional::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Sesmet_inspecao_funcionario', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Sesmet_inspecao_funcionario
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_inspecao_funcionario/edit.php
    function post_edit(){
        $this->setTitle('Edição de Sesmet_inspecao_funcionario');
        try {
            $Sesmet_inspecao_funcionario = new Sesmet_inspecao_funcionario((int) $_POST['id']);
            $this->set('Sesmet_inspecao_funcionario', $Sesmet_inspecao_funcionario);
            $Sesmet_inspecao_funcionario->save($_POST);
            new Msg(__('Sesmet_inspecao_funcionario atualizado com sucesso'));
            $this->go('Sesmet_inspecao_funcionario', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
        $this->set('Rhprofissionais',  Rhprofissional::getList());
    }

    # Confirma a exclusão ou não de um(a) Sesmet_inspecao_funcionario
    # renderiza a /view/Sesmet_inspecao_funcionario/delete.php
    function delete(){
        $this->setTitle('Apagar Sesmet_inspecao_funcionario');
        try {
            $this->set('Sesmet_inspecao_funcionario', new Sesmet_inspecao_funcionario((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_inspecao_funcionario', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Sesmet_inspecao_funcionario
    # redireciona para Sesmet_inspecao_funcionario/all
    function post_delete(){
        //var_dump('teste');exit;
        try {
            $Sesmet_inspecao_funcionario = new Sesmet_inspecao_funcionario((int) $_POST['id']);
            $Sesmet_inspecao_funcionario->delete();
            new Msg(__('Sesmet_inspecao_funcionario apagado com sucesso'), 1);
             //NAVEGAÇÃO ENTRE MODAIS
            //se nao for nulo o valor da modal sai
            if(!empty($_POST['modal'])){
                echo 1;
                exit;
            }
            //TERMINA AQUI
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Sesmet_inspecao_funcionario', 'all');
    }

}