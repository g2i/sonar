<?php
final class Imobiliado_tipo_movimentoController extends AppController{ 

    # página inicial do módulo Imobiliado_tipo_movimento
    function index(){
        $this->setTitle('Visualização de Imobiliado_tipo_movimento');
    }

    # lista de Imobiliado_tipo_movimentos
    # renderiza a visão /view/Imobiliado_tipo_movimento/all.php
    function all(){
        $this->setTitle('Listagem de Imobiliado_tipo_movimento');
        $p = new Paginate('Imobiliado_tipo_movimento', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Imobiliado_tipo_movimentos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Imobiliado_tipo_movimento
    # renderiza a visão /view/Imobiliado_tipo_movimento/view.php
    function view(){
        $this->setTitle('Visualização de Imobiliado_tipo_movimento');
        try {
            $this->set('Imobiliado_tipo_movimento', new Imobiliado_tipo_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobiliado_tipo_movimento', 'all');
        }
    }

    # formulário de cadastro de Imobiliado_tipo_movimento
    # renderiza a visão /view/Imobiliado_tipo_movimento/add.php
    function add(){
        $this->setTitle('Cadastro de Imobiliado_tipo_movimento');
        $this->set('Imobiliado_tipo_movimento', new Imobiliado_tipo_movimento);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Imobiliado_tipo_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Imobiliado_tipo_movimento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Imobiliado_tipo_movimento');
        $Imobiliado_tipo_movimento = new Imobiliado_tipo_movimento();
        $this->set('Imobiliado_tipo_movimento', $Imobiliado_tipo_movimento);
        try {
            $Imobiliado_tipo_movimento->situacao_id = 1;
            $Imobiliado_tipo_movimento->cadastrado_por = Session::get('user')->id;
            $Imobiliado_tipo_movimento->dt_cadastro = date('Y-m-d  H:i:d');
            $Imobiliado_tipo_movimento->save($_POST);
            new Msg(__('Tipo de movimento cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobiliado_tipo_movimento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Imobiliado_tipo_movimento
    # renderiza a visão /view/Imobiliado_tipo_movimento/edit.php
    function edit(){
        $this->setTitle('Edição de Imobiliado_tipo_movimento');
        try {
            $this->set('Imobiliado_tipo_movimento', new Imobiliado_tipo_movimento((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Imobiliado_tipo_movimento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Imobiliado_tipo_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Imobiliado_tipo_movimento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Imobiliado_tipo_movimento');
        try {
            $Imobiliado_tipo_movimento = new Imobiliado_tipo_movimento((int) $_POST['id']);
            $this->set('Imobiliado_tipo_movimento', $Imobiliado_tipo_movimento);
            $Imobiliado_tipo_movimento->modificado_por = Session::get('user')->id;
            $Imobiliado_tipo_movimento->dt_modificado = date('Y-m-d H:i:d');
            $Imobiliado_tipo_movimento->save($_POST);
            new Msg(__('Tipo de movimento atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobiliado_tipo_movimento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Imobiliado_tipo_movimento
    # renderiza a /view/Imobiliado_tipo_movimento/delete.php
    function delete(){
        $this->setTitle('Apagar Imobiliado_tipo_movimento');
        try {
            $this->set('Imobiliado_tipo_movimento', new Imobiliado_tipo_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobiliado_tipo_movimento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Imobiliado_tipo_movimento
    # redireciona para Imobiliado_tipo_movimento/all
    function post_delete(){
        try {
            $Imobiliado_tipo_movimento = new Imobiliado_tipo_movimento((int) $_POST['id']);
            $Imobiliado_tipo_movimento->situacao_id = 3;
            $Imobiliado_tipo_movimento->save();
            new Msg(__('Tipo de movimento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if (!empty($_POST['modal'])) {
            echo 1;
            exit;
        }
        $this->go('Imobiliado_tipo_movimento', 'all');
    }

}