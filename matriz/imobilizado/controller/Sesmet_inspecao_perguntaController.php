<?php
final class Sesmet_inspecao_perguntaController extends AppController{ 

    # página inicial do módulo Sesmet_inspecao_pergunta
    function index(){
        $this->setTitle('Visualização de Sesmet_inspecao_pergunta');
    }

    # lista de Sesmet_inspecao_perguntas
    # renderiza a visão /view/Sesmet_inspecao_pergunta/all.php $this->getParam('id')
    function all(){
        
        $this->setTitle('Listagem de Sesmet_inspecao_pergunta');
        $p = new Paginate('Sesmet_inspecao_pergunta', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('sesmet_inspecao_id','=', $this->getParam('id'));
        $this->set('Sesmet_inspecao_perguntas', $p->getPage($c));
        $this->set('nav', $p->getNav());

        //manda para view uma lista no modelo do data-source="{'M': 'male', 'F': 'female'}" para conseguir listar e alterar os dados 
           $Sesmet_resposta = Sesmet_resposta::getList();
           $Sesmet_respostaDataSource = '{';
           foreach($Sesmet_resposta as $m) {            
               $Sesmet_respostaDataSource .= "'$m->id': '$m->nome', ";
           }
           $Sesmet_respostaDataSource = substr($Sesmet_respostaDataSource, 0, -2);
           $Sesmet_respostaDataSource .= '}';
           $this->set('Sesmet_respostaDataSource',  $Sesmet_respostaDataSource);
    }

    # visualiza um(a) Sesmet_inspecao_pergunta
    # renderiza a visão /view/Sesmet_inspecao_pergunta/view.php
    function view(){
        $this->setTitle('Visualização de Sesmet_inspecao_pergunta');
        try {
            $this->set('Sesmet_inspecao_pergunta', new Sesmet_inspecao_pergunta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_inspecao_pergunta', 'all');
        }
    }

    # formulário de cadastro de Sesmet_inspecao_pergunta
    # renderiza a visão /view/Sesmet_inspecao_pergunta/add.php
    function add(){
        $this->setTitle('Cadastro de Sesmet_inspecao_pergunta');
        $this->set('Sesmet_inspecao_pergunta', new Sesmet_inspecao_pergunta);
        $this->set('Sesmet_perguntas',  Sesmet_pergunta::getList());
        $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Sesmet_respostas',  Sesmet_resposta::getList());
    }

    # recebe os dados enviados via post do cadastro de Sesmet_inspecao_pergunta
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_inspecao_pergunta/add.php
    function post_add(){
        $this->setTitle('Cadastro de Sesmet_inspecao_pergunta');
        $Sesmet_inspecao_pergunta = new Sesmet_inspecao_pergunta();
        $this->set('Sesmet_inspecao_pergunta', $Sesmet_inspecao_pergunta);
        try {
            $Sesmet_inspecao_pergunta->save($_POST);
            new Msg(__('Sesmet_inspecao_pergunta cadastrado com sucesso'));
            $this->go('Sesmet_inspecao_pergunta', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Sesmet_perguntas',  Sesmet_pergunta::getList());
        $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Sesmet_respostas',  Sesmet_resposta::getList());
    }

    # formulário de edição de Sesmet_inspecao_pergunta
    # renderiza a visão /view/Sesmet_inspecao_pergunta/edit.php
    function edit(){
        $this->setTitle('Edição de Sesmet_inspecao_pergunta');
        try {
            $this->set('Sesmet_inspecao_pergunta', new Sesmet_inspecao_pergunta((int) $this->getParam('id')));
            $this->set('Sesmet_perguntas',  Sesmet_pergunta::getList());
            $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Sesmet_respostas',  Sesmet_resposta::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Sesmet_inspecao_pergunta', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Sesmet_inspecao_pergunta
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_inspecao_pergunta/edit.php
    function post_edit(){
        $this->setTitle('Edição de Sesmet_inspecao_pergunta');
        try {
            $Sesmet_inspecao_pergunta = new Sesmet_inspecao_pergunta((int) $_POST['id']);
            $this->set('Sesmet_inspecao_pergunta', $Sesmet_inspecao_pergunta);
            $Sesmet_inspecao_pergunta->save($_POST);
            new Msg(__('Sesmet_inspecao_pergunta atualizado com sucesso'));
            $this->go('Sesmet_inspecao_pergunta', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Sesmet_perguntas',  Sesmet_pergunta::getList());
        $this->set('Sesmet_inspecaos',  Sesmet_inspecao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Sesmet_respostas',  Sesmet_resposta::getList());
    }

    # Confirma a exclusão ou não de um(a) Sesmet_inspecao_pergunta
    # renderiza a /view/Sesmet_inspecao_pergunta/delete.php
    function delete(){
        $this->setTitle('Apagar Sesmet_inspecao_pergunta');
        try {
            $this->set('Sesmet_inspecao_pergunta', new Sesmet_inspecao_pergunta((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_inspecao_pergunta', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Sesmet_inspecao_pergunta
    # redireciona para Sesmet_inspecao_pergunta/all
    function post_delete(){
        try {
            $Sesmet_inspecao_pergunta = new Sesmet_inspecao_pergunta((int) $_POST['id']);
            $Sesmet_inspecao_pergunta->delete();
            new Msg(__('Sesmet_inspecao_pergunta apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Sesmet_inspecao_pergunta', 'all');
    }

     function parcial_edit()
    {
        $this->setTitle('Edição de Inspeção pergunta');

        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];      
        $Sesmet_inspecao_pergunta = new Sesmet_inspecao_pergunta((int)$_POST['pk']);
        $this->set('Sesmet_inspecao_pergunta', $Sesmet_inspecao_pergunta);
            $name = $_POST['name'];
            //verifica se é o campo dt_execucao se for faz o tratamento para salvar no banco de dados 
            if($name == 'dt_execucao') {                
                $_POST['value'] = DataSQL($_POST['value']);
            }
            //salva dados do tipo text
            $Sesmet_inspecao_pergunta->$name = $_POST['value'];

            if ($Sesmet_inspecao_pergunta->save($_POST)) {
                $res = ['res' => $Sesmet_inspecao_pergunta->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        
            //retorna apenas para o console um resultado para saber se deu certo
            echo json_encode($res);
            exit;
    }

}