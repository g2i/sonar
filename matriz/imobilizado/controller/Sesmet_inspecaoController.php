<?php
final class Sesmet_inspecaoController extends AppController{ 

    # página inicial do módulo Sesmet_inspecao
    function index(){
        $this->setTitle('Lista');
    }

    # lista de Sesmet_inspecaos
    # renderiza a visão /view/Sesmet_inspecao/all.php
    function all(){
        $this->setTitle('Lista');
        $p = new Paginate('Sesmet_inspecao', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        
        $c->setOrder('id DESC');
        $c->addCondition('situacao_id','=',1);
        $this->set('Sesmet_inspecaos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Sesmet_inspecao
    # renderiza a visão /view/Sesmet_inspecao/view.php
    function view(){
        $this->setTitle('Lista');
        try {
            $this->set('Sesmet_inspecao', new Sesmet_inspecao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_inspecao', 'all');
        }
    }

    # formulário de cadastro de Sesmet_inspecao
    # renderiza a visão /view/Sesmet_inspecao/add.php
    function add(){
        $this->setTitle('Cadastro ');
        $this->set('Sesmet_inspecao', new Sesmet_inspecao);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }
    # recebe os dados enviados via post do cadastro de Sesmet_inspecao
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_inspecao/add.php
    function post_add(){
        $this->setTitle('Cadastro ');
        $Sesmet_inspecao = new Sesmet_inspecao();
        $this->set('Sesmet_inspecao', $Sesmet_inspecao);
        try {
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_inspecao->situacao_id = 1;
            $Sesmet_inspecao->cadastrado_por = $user->id;
            $Sesmet_inspecao->dt_cadastro = date('Y-m-d H:i:s');
            if($Sesmet_inspecao->save($_POST)){
                $Sesmet_pergunta = Sesmet_pergunta::getList();
                $pergunta_id = [];
                foreach(Sesmet_pergunta::getList() as $um) {
                    $pergunta_id[] = $um->id;
                }
                foreach($pergunta_id  as $ids){                
                    $Sesmet_inspecao_pergunta =  new Sesmet_inspecao_pergunta;
                   $Sesmet_inspecao_pergunta->sesmet_inspecao_id = $Sesmet_inspecao->id;
                   $Sesmet_inspecao_pergunta->sesmet_pergunta_id = $ids;
                   $Sesmet_inspecao_pergunta->situacao_id = 1;
                   //$Sesmet_inspecao_pergunta->cadastrado_por = $user->id;
                   $Sesmet_inspecao_pergunta->dt_cadastro = date('Y-m-d H:i:s');
                   $Sesmet_inspecao_pergunta->save();
                }
            }
            new Msg(__('Inspeções cadastrado com sucesso, continuar'));
            $this->go('Sesmet_inspecao', 'edit', array('id' => $Sesmet_inspecao->id));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Sesmet_inspecao
    # renderiza a visão /view/Sesmet_inspecao/edit.php
    function edit(){
        $this->setTitle('Continuar ');
        try {
            $this->set('Sesmet_inspecao', new Sesmet_inspecao((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());

            $g = new Criteria();
            $inpesaoId = $this->getParam('id');
            $g->addCondition('sesmet_inspecao_id','=',$inpesaoId);
            $this->set('Sesmet_inspecao_pergunta', Sesmet_inspecao_pergunta::getList($g));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Sesmet_inspecao', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Sesmet_inspecao
    # (true)redireciona ou (false) renderiza a visão /view/Sesmet_inspecao/edit.php
    function post_edit(){
        $this->setTitle('Continuar ');
        try {
            $Sesmet_inspecao = new Sesmet_inspecao((int) $_POST['id']);
            $this->set('Sesmet_inspecao', $Sesmet_inspecao);
            $user = Session::get('user');// para salvar o usuario que está fazendo
            $Sesmet_inspecao->modificado_por = $user->id;
            $Sesmet_inspecao->dt_modificado = date('Y-m-d H:i:s');
            $Sesmet_inspecao->save($_POST);
            new Msg(__('Inspeções salva com sucesso'));
            $this->go('Sesmet_inspecao', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Sesmet_inspecao
    # renderiza a /view/Sesmet_inspecao/delete.php
    function delete(){
        $this->setTitle('Apagar Sesmet_inspecao');
        try {
            $this->set('Sesmet_inspecao', new Sesmet_inspecao((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Sesmet_inspecao', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Sesmet_inspecao
    # redireciona para Sesmet_inspecao/all
    function post_delete(){
        try {
            $this->set('Sesmet_inspecao', $Sesmet_inspecao);

            $Sesmet_inspecao = new Sesmet_inspecao((int) $_POST['id']);
            $Sesmet_inspecao->situacao_id = 3;
           // var_dump($Sesmet_inspecao);exit;
            $Sesmet_inspecao->save($Sesmet_inspecao);
            new Msg(__('Sesmet_inspecao apagado com sucesso '), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Sesmet_inspecao', 'all');
    }

}