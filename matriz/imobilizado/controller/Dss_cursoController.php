<?php
final class Dss_cursoController extends AppController{ 

    # página inicial do módulo Dss_curso
    function index(){
        $this->setTitle('Visualização de Dss_curso');
    }

    # lista de Dss_cursos
    # renderiza a visão /view/Dss_curso/all.php
    function all(){
        $this->setTitle('Listagem de Dss_curso');
        $p = new Paginate('Dss_curso', 10);
        $c = new Criteria();
      
        $c->setOrder('id DESC');
        $this->set('Dss_cursos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Dss_curso
    # renderiza a visão /view/Dss_curso/view.php
    function view(){
        $this->setTitle('Visualização de Dss_curso');
        try {
            $this->set('Dss_curso', new Dss_curso((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Dss_curso', 'all');
        }
    }

    # formulário de cadastro de Dss_curso
    # renderiza a visão /view/Dss_curso/add.php
    function add(){
        $this->setTitle('Cadastro de Dss_curso');
        $this->set('Dss_curso', new Dss_curso);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Dss_curso
    # (true)redireciona ou (false) renderiza a visão /view/Dss_curso/add.php
    function post_add(){
        $this->setTitle('Cadastro de Dss_curso');
        $Dss_curso = new Dss_curso();
        $this->set('Dss_curso', $Dss_curso);
        try {
            $Dss_curso->save($_POST);
            new Msg(__('Dss_curso cadastrado com sucesso'));
            $this->go('Dss_curso', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Dss_curso
    # renderiza a visão /view/Dss_curso/edit.php
    function edit(){
        $this->setTitle('Edição de Dss_curso');
        try {
            $this->set('Dss_curso', new Dss_curso((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Dss_curso', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Dss_curso
    # (true)redireciona ou (false) renderiza a visão /view/Dss_curso/edit.php
    function post_edit(){
        $this->setTitle('Edição de Dss_curso');
        try {
            $Dss_curso = new Dss_curso((int) $_POST['id']);
            $this->set('Dss_curso', $Dss_curso);
            $Dss_curso->save($_POST);
            new Msg(__('Dss_curso atualizado com sucesso'));
            $this->go('Dss_curso', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Dss_curso
    # renderiza a /view/Dss_curso/delete.php
    function delete(){
        $this->setTitle('Apagar Dss_curso');
        try {
            $this->set('Dss_curso', new Dss_curso((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Dss_curso', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Dss_curso
    # redireciona para Dss_curso/all
    function post_delete(){
        try {
            $Dss_curso = new Dss_curso((int) $_POST['id']);
            $Dss_curso->delete();
            new Msg(__('Dss_curso apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Dss_curso', 'all');
    }

}