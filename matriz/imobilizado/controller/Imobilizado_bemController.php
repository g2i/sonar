<?php
final class Imobilizado_bemController extends AppController{ 

    # página inicial do módulo Imobilizado_bem
    function index(){
        $this->setTitle('Visualização de Imobilizado_bem');
    }

    # lista de Imobilizado_bens
    # renderiza a visão /view/Imobilizado_bem/all.php
    function all(){
        $this->setTitle('Listagem de Imobilizado_bem');
        $p = new Paginate('Imobilizado_bem', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Imobilizado_bens', $p->getPage($c));
        $this->set('nav', $p->getNav());
    
        
        $this->set('Imobilizado_situacao_bens',  Imobilizado_situacao_bem::getList());
        $this->set('Imobilizado_tipo_bens',  Imobilizado_tipo_bem::getList());
        $this->set('Imobilizado_conservacaos',  Imobilizado_conservacao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Imobilizado_grupos',  Imobilizado_grupo::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Imobilizado_bem
    # renderiza a visão /view/Imobilizado_bem/view.php
    function view(){
        $this->setTitle('Visualização de Imobilizado_bem');
        try {
            $this->set('Imobilizado_bem', new Imobilizado_bem((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_bem', 'all');
        }
    }

    # formulário de cadastro de Imobilizado_bem
    # renderiza a visão /view/Imobilizado_bem/add.php
    function add(){
        $this->setTitle('Cadastro');
        $this->set('Imobilizado_situacao_bens',  Imobilizado_situacao_bem::getList());
        $this->set('Imobilizado_bem', new Imobilizado_bem);
        $this->set('Imobilizado_tipo_bens',  Imobilizado_tipo_bem::getList());
        $this->set('Imobilizado_conservacaos',  Imobilizado_conservacao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Imobilizado_grupos',  Imobilizado_grupo::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Imobilizado_bem
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_bem/add.php
    function post_add(){
        $this->setTitle('Cadastro');
        $Imobilizado_bem = new Imobilizado_bem();
        $this->set('Imobilizado_bem', $Imobilizado_bem);
        try {
            $Imobilizado_bem->situacao_id = 1;
            $Imobilizado_bem->cadastrado_por = Session::get('user')->id;
            $Imobilizado_bem->dt_cadastro = date('Y-m-d H:i:d');
            if($Imobilizado_bem->save($_POST)){
                if (!empty($_FILES['foto']['name'])) {
                    $img = $_FILES['foto'];
                    $foto = new ImageUploader($img, 150);
                    $path = "/fotos/" . $Imobilizado_bem->id;
                    $nome = uniqid() . $img['name'];
                    $foto->save($nome, $path);
                    $ImobilizadoBemImg = new Estq_artigo($Imobilizado_bem->id);
                    $ImobilizadoBemImg->foto = SITE_PATH . '/uploads' . $path . "/" . $nome;
                    $ImobilizadoBemImg->save();
                }
            }
            new Msg(__('Imobilizado bem cadastrado com sucesso'));
            $this->go('Imobilizado_bem', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Imobilizado_situacao_bens',  Imobilizado_situacao_bem::getList());
        $this->set('Imobilizado_tipo_bens',  Imobilizado_tipo_bem::getList());
        $this->set('Imobilizado_conservacaos',  Imobilizado_conservacao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Imobilizado_grupos',  Imobilizado_grupo::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Imobilizado_bem
    # renderiza a visão /view/Imobilizado_bem/edit.php
    function edit(){
        $this->setTitle('Edição');
        try {
            $this->set('Imobilizado_bem', new Imobilizado_bem((int) $this->getParam('id')));
            $this->set('Imobilizado_tipo_bens',  Imobilizado_tipo_bem::getList());
            $this->set('Imobilizado_conservacaos',  Imobilizado_conservacao::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Imobilizado_grupos',  Imobilizado_grupo::getList());
            $this->set('Rhlocalidades',  Rhlocalidade::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Imobilizado_situacao_bens',  Imobilizado_situacao_bem::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Imobilizado_bem', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Imobilizado_bem
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_bem/edit.php
    function post_edit(){
        $this->setTitle('Edição');
        try {
            $Imobilizado_bem = new Imobilizado_bem((int) $_POST['id']);
            $this->set('Imobilizado_bem', $Imobilizado_bem);
            $Imobilizado_bem->modificado_por = Session::get('user')->id;
            $Imobilizado_bem->dt_modificado = date('Y-m-d H:i:d');
            $Imobilizado_bem->save($_POST);
            new Msg(__('Imobilizado bem atualizado com sucesso'));
            $this->go('Imobilizado_bem', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Imobilizado_tipo_bens',  Imobilizado_tipo_bem::getList());
        $this->set('Imobilizado_conservacaos',  Imobilizado_conservacao::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Imobilizado_grupos',  Imobilizado_grupo::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Imobilizado_situacao_bens',  Imobilizado_situacao_bem::getList());
    }

    # Confirma a exclusão ou não de um(a) Imobilizado_bem
    # renderiza a /view/Imobilizado_bem/delete.php
    function delete(){
        $this->setTitle('Apagar Imobilizado_bem');
        try {
            $this->set('Imobilizado_bem', new Imobilizado_bem((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_bem', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Imobilizado_bem
    # redireciona para Imobilizado_bem/all
    function post_delete(){
        try {
            $Imobilizado_bem = new Imobilizado_bem((int) $_POST['id']);
            $Imobilizado_bem->situacao_id = 3;
            $Imobilizado_bem->save();
            new Msg(__('Imobilizado bem apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Imobilizado_bem', 'all');
    }

}