<?php
final class Imobilizado_grupoController extends AppController{ 

    # página inicial do módulo Imobilizado_grupo
    function index(){
        $this->setTitle('Visualização de Imobilizado_grupo');
    }

    # lista de Imobilizado_grupos
    # renderiza a visão /view/Imobilizado_grupo/all.php
    function all(){
        $this->setTitle('Listagem de Imobilizado_grupo');
        $p = new Paginate('Imobilizado_grupo', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $this->set('Imobilizado_grupos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    

        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Imobilizado_grupo
    # renderiza a visão /view/Imobilizado_grupo/view.php
    function view(){
        $this->setTitle('Visualização de Imobilizado_grupo');
        try {
            $this->set('Imobilizado_grupo', new Imobilizado_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_grupo', 'all');
        }
    }

    # formulário de cadastro de Imobilizado_grupo
    # renderiza a visão /view/Imobilizado_grupo/add.php
    function add(){
        $this->setTitle('Cadastro de Imobilizado_grupo');
        $this->set('Imobilizado_grupo', new Imobilizado_grupo);
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Imobilizado_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_grupo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Imobilizado_grupo');
        $Imobilizado_grupo = new Imobilizado_grupo();
        $this->set('Imobilizado_grupo', $Imobilizado_grupo);
        try {
            $Imobilizado_grupo->situacao_id = 1; 
            $Imobilizado_grupo->cadastrado_por = Session::get('user')->id;
            $Imobilizado_grupo->dt_cadastro = date('Y-m-d H:i:d');
            $Imobilizado_grupo->save($_POST);
            new Msg(__('Grupo cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_grupo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Imobilizado_grupo
    # renderiza a visão /view/Imobilizado_grupo/edit.php
    function edit(){
        $this->setTitle('Edição de Imobilizado_grupo');
        try {
            $this->set('Imobilizado_grupo', new Imobilizado_grupo((int) $this->getParam('id')));
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Imobilizado_grupo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Imobilizado_grupo
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_grupo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Imobilizado_grupo');
        try {
            $Imobilizado_grupo = new Imobilizado_grupo((int) $_POST['id']);
            $this->set('Imobilizado_grupo', $Imobilizado_grupo);
            $Imobilizado_grupo->modificado_por = Session::get('user')->id;
            $Imobilizado_grupo->dt_modificado = date('Y-m-d H:i:d');
            $Imobilizado_grupo->save($_POST);
            new Msg(__('Grupo atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_grupo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Imobilizado_grupo
    # renderiza a /view/Imobilizado_grupo/delete.php
    function delete(){
        $this->setTitle('Apagar Imobilizado_grupo');
        try {
            $this->set('Imobilizado_grupo', new Imobilizado_grupo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_grupo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Imobilizado_grupo
    # redireciona para Imobilizado_grupo/all
    function post_delete(){
        try {
            $Imobilizado_grupo = new Imobilizado_grupo((int) $_POST['id']);
            $Imobilizado_grupo->situacao_id = 3;
            $Imobilizado_grupo->save();
            new Msg(__('Imobilizado_grupo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if (!empty($_POST['modal'])) {
            echo 1;
            exit;
        }
        $this->go('Imobilizado_grupo', 'all');
    }

}