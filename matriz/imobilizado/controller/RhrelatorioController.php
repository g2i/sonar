<?php
final class RhrelatorioController extends AppController{ 

    # página inicial do módulo Rhrelatorio
    function index(){
        $this->setTitle('Visualização de Rhrelatorio');
    }

    # lista de Rhrelatorios
    # renderiza a visão /view/Rhrelatorio/all.php
    function all(){
        $this->setTitle('Listagem de Rhrelatorio');
        $p = new Paginate('Rhrelatorio', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
   
        //busca ocorrencias apenas aso DISTINCT
       $query = $this->query("SELECT DISTINCT * FROM `rhocorrencia_historico` oh
       WHERE  oh.`codigo` = 3
       OR oh.`codigo` = 14
       OR oh.`codigo` = 24
       OR oh.`codigo` = 27
       OR oh.`codigo` = 23");

        $this->set('Rhprojetos', Rhprojetos::getList());
        $this->set('Rhocorrencia_historicos',   $query);
        $this->set('Rhlocalidade', Rhlocalidade::getList());
        $this->set('Rhrelatorios', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/view.php
    function view(){
        $this->setTitle('Visualização de Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # formulário de cadastro de Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/add.php
    function add(){
        $this->setTitle('Cadastro de Rhrelatorio');
        $this->set('Rhrelatorio', new Rhrelatorio);
    }

    # recebe os dados enviados via post do cadastro de Rhrelatorio
    # (true)redireciona ou (false) renderiza a visão /view/Rhrelatorio/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rhrelatorio');
        $Rhrelatorio = new Rhrelatorio();
        $this->set('Rhrelatorio', $Rhrelatorio);
        try {
            $Rhrelatorio->save($_POST);
            new Msg(__('Rhrelatorio cadastrado com sucesso'));
            $this->go('Rhrelatorio', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Rhrelatorio
    # renderiza a visão /view/Rhrelatorio/edit.php
    function edit(){
        $this->setTitle('Edição de Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhrelatorio
    # (true)redireciona ou (false) renderiza a visão /view/Rhrelatorio/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhrelatorio');
        try {
            $Rhrelatorio = new Rhrelatorio((int) $_POST['inicio']);
            $this->set('Rhrelatorio', $Rhrelatorio);
            $Rhrelatorio->save($_POST);
            new Msg(__('Rhrelatorio atualizado com sucesso'));
            $this->go('Rhrelatorio', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhrelatorio
    # renderiza a /view/Rhrelatorio/delete.php
    function delete(){
        $this->setTitle('Apagar Rhrelatorio');
        try {
            $this->set('Rhrelatorio', new Rhrelatorio((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhrelatorio', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhrelatorio
    # redireciona para Rhrelatorio/all
    function post_delete(){
        try {
            $Rhrelatorio = new Rhrelatorio((int) $_POST['id']);
            $Rhrelatorio->delete();
            new Msg(__('Rhrelatorio apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhrelatorio', 'all');
    }


    function print_distribuicao(){
        $this->setTemplate('empty');
        $this->setTitle('Relatórios');
        $inicio =$_POST['inicio'];
        $fim = $_POST['fim'];
        $tipo = $_POST['tipo'];
        $historico = $_POST['historico'];       
        $localidade = $_POST['localidade'];
        $projetos = $_POST['Rhprojetos'];

        $titulo = ' ';
        if($tipo == 1){
            $admissao = print_admitidos($inicio, $fim,$localidade);
            $this->set('dados',  $admissao);
            $titulo = "Listagens Contratação (Por Período)";            
        }else if($tipo == 2){
            $demissao = print_demitidos($inicio, $fim,$localidade);
            $this->set('dados',  $demissao);
            $titulo = "Listagens Demissão (Por Período)";            
        }else if($tipo == 3){
            $ferias = print_ferias($inicio, $fim,$localidade);
            $this->set('dados',  $ferias);
            $titulo = "Listagem - Férias a Conceder";            
        }else if($tipo == 4){
            $aso = print_aso($inicio, $fim, $historico, $localidade);
            $this->set('dados',$aso);
            $titulo = "Listagens - ASO (Período Vencimento - Ativos)";            
        }else if($tipo == 5){
            $iniss = print_inss($inicio, $fim, $localidade);
            $this->set('dados', $iniss);
            $titulo = "Listagens INSS - Afastamento";            
        }else if($tipo == 6){
            $atestado = print_atestado($inicio, $fim, $localidade);
            $this->set('dados',$atestado);
            $titulo = "Listagens Atestados(Por Período) ";            
        }else if($tipo == 7){
            $getProjetos = print_projetos($inicio, $fim, $localidade, $projetos);
            $this->set('dados',$getProjetos);
            $titulo = "Listagens Colaboradores ativos (Por Localidade vs Projetos) "; 
            $c = new Criteria();
            $c->addCondition('id','=', $projetos);
            $Rhprojetos =  Rhprojetos::getList($c);
            
          $json[] = null;
          foreach($Rhprojetos as $rhprojeto){
              $json = [
                  "nome" => $rhprojeto->nome
                
              ];
          }
            $projetosJson = (object)$json;     
            $this->set('projetosJson',  $projetosJson);           
        }

        $this->set('titulo', $titulo);
        $this->set('inicio', $inicio);
        $this->set('fim', $fim);
    }
    
    function dss_cursos(){
        $this->setTemplate('empty');
        $this->setTitle('Relatórios');

        $c = new Criteria();
        $c->addCondition('id','=', $this->getParam('id'));
        $cursos =  Dss_curso::getList($c);

      $json[] = null;
        foreach($cursos as $curso){
            $json = [
                "id" => $curso->id,
                "tema" => $curso->tema,
                "descricao" =>  $curso->descricao,
                "palestrante" =>  $curso->palestrante,
                "localidade" =>  $curso->localidade,
                "empresa" =>  $curso->empresa,
                "divisaoProjeto" =>  $curso->divisao_projeto,
                "data_palestra" =>  $curso->data_palestra
            ];
        }
        $dados = (object)$json;
        $this->set('dados', $dados);       
    }
    

    function sesmet_inspecao(){
        $this->setTemplate('empty');
        $this->setTitle('Relatórios');
        $c = new Criteria();
        $c->addCondition('id','=', $this->getParam('id'));
        $inspecao =  Sesmet_inspecao::getList($c);

        $jsonInpecao[] = null;
        foreach($inspecao as $ins){
            $jsonInpecao = [
                'id' => $ins->id,
                'nome' => $ins->nome,
                'empresa' => $ins->empresa,
                'localidade' => $ins->localidade,
                'equipe' => $ins->equipe,
                'servico' => $ins->servico,
                'veiculo' => $ins->veiculo,
                'encarregado' => $ins->encarregado,
                'responsavelVistoria' => $ins->responsavel_vistoria,
                'dataCadastro' => $ins->dt_cadastro,
                'observacao' => $ins->observacao
            ];
        }
        $inpessaoDados = (object)$jsonInpecao;
        //$this->set('inpessaoDados', $inpessaoDados); 

        $g = new Criteria();
        $g->addCondition('sesmet_inspecao_id','=', $this->getParam('id'));
        //$g->addCondition('situacao_id','=', 1);
        $jsonFunc = [];
        $func =  Sesmet_inspecao_funcionario::getList($g);
            foreach($func as $f){
                $jsonFunc[] = [
                    'funCod' => $f->getRhprofissional()->codigo,
                    'funcNome' => $f->getRhprofissional()->nome
                ];               
            }
            $funcDados = (object)$jsonFunc;
            

            $tg = new Criteria();
            $tg->addCondition('sesmet_inspecao_id','=', $this->getParam('id'));
            //$g->addCondition('situacao_id','=', 1);
            $jsonResp = [];
            $pergResp =  Sesmet_inspecao_pergunta::getList($tg);
           
           
                foreach($pergResp as $cg){
                     $grupo = $cg->getSesmet_pergunta()->getSesmet_grupo()->nome;
                    $jsonResp[] = [                      
                       'pergunta' => $cg->getSesmet_pergunta()->nome,
                       'resposta' => $cg->getSesmet_resposta()->nome
                    ];              
                }
                $pergDados = (object)$jsonResp;

                $this->set('grupos', $grupo);
                $this->set('inpessaoDados', $inpessaoDados);
                $this->set('funcDados', $funcDados);
                $this->set('pergDados', $pergDados);
        }

    function reports_inspecao(){
        $this->setTemplate('reports');
        $inspecaoQueryId = (int) $this->getParam('id');
        $inspecaoQuery = new Criteria();
        $inspecaoQuery->addCondition('id','=', (int) $this->getParam('id'));
        $inspecaoQuery->setOrder('id DESC');
        $inspecao =  Sesmet_inspecao::getList($inspecaoQuery);
        
        foreach($inspecao as $ins){
            $json['inspecao'][] = [
                'id' => $ins->id,
                'nome' => $ins->nome,
                'empresa' => $ins->empresa,
                'localidade' => $ins->localidade,
                'equipe' => $ins->equipe,
                'servico' => $ins->servico,
                'veiculo' => $ins->veiculo,
                'encarregado' => $ins->encarregado,
                'responsavelVistoria' => $ins->responsavel_vistoria,
                'dataCadastro' => $ins->dt_cadastro,
                'observacao' => $ins->observacao
            ];
        }
        $func =  Sesmet_inspecao_funcionario::getList();
            foreach($func as $f){
                $json['funcionario'][] = [
                    'funCod' => $f->getRhprofissional()->codigo,
                    'funcNome' => $f->getRhprofissional()->nome
                ];               
            }

            $tg = new Criteria();
            $tg->addCondition('sesmet_inspecao_id','=', $this->getParam('id'));
            $pergResp =  Sesmet_inspecao_pergunta::getList($tg);           
                foreach($pergResp as $cg){
                     $grupo = $cg->getSesmet_pergunta()->getSesmet_grupo()->nome;
                    $json['perguntas'][] = [                      
                       'pergunta' => $cg->getSesmet_pergunta()->nome,
                       'resposta' => $cg->getSesmet_resposta()->nome,
                       'grupo' =>  $grupo
                    ];              
                }

        $reportCreate = new Json();
        $reportCreate->create('reportInspecao.json', json_encode($json));
        //$this->set('report', 'inspecao.mrt');   
    }
    function report_bens(){
        $this->setTemplate('reports');
        $bemQueryId = (int) $this->getParam('id');
        $bemQuery = new Criteria();
        $bemQuery->addCondition('id','=', (int) $this->getParam('id'));
        $bemQuery->setOrder('id DESC');
        $Imobilizado_bem =  Imobilizado_bem::getList($bemQuery);
        foreach($Imobilizado_bem as $bens){
            $json['bens'][] = [
                'id' => $bens->id,
                'descricao' => !empty($bens->descricao) ? $bens->descricao : '-',
                'complemento' => !empty($bens->complemento) ? $bens->complemento : '-',
                'numero_serie' => !empty($bens->numero_serie) ? $bens->numero_serie : '-', 
                'dt_aquisicao' =>  !empty($bens->dt_aquisicao) ? DataBR($bens->dt_aquisicao) : '-',
                'quantidade_atual' => !empty($bens->quantidade_atual) ? $bens->quantidade_atual : '-',
                'data_baixa' => !empty($bens->data_baixa) ? DataBR($bens->data_baixa) : '-',
                'data_manutencao' => !empty($bens->data_manutencao) ? DataBR($bens->data_manutencao) : '-',
                'vencimento_garantia' => !empty($bens->vencimento_garantia) ? DataBR($bens->vencimento_garantia) : '-', 
                'vida_util' => !empty($bens->vida_util) ? $bens->vida_util : '-',
                'valor_comprado' => !empty($bens->valor_comprado) ? $bens->valor_comprado : '-',
                'valor_reajuste' => !empty($bens->valor_reajuste) ? $bens->valor_reajuste : '-',
                'tipo_bem' => !empty($bens->getImobilizado_tipo_bem()->nome) ? $bens->getImobilizado_tipo_bem()->nome : '-',
                'conservacao' => !empty($bens->getImobilizado_conservacao()->descricao) ? $bens->getImobilizado_conservacao()->descricao : '-',
                'grupo' => !empty($bens->getImobilizado_grupo()->nome) ? $bens->getImobilizado_grupo()->nome : '-',
                'depreciacao' => !empty($bens->depreciacao) ? $bens->depreciacao : '-',
                'atualizado' => !empty($bens->atualizado) ? $bens->atualizado : '-',
                'localidade' => !empty($bens->getRhlocalidade()->cidade) ? $bens->getRhlocalidade()->cidade : '-',
                'nota_fiscal' => !empty($bens->nota_fiscal) ? $bens->nota_fiscal : '-',
                'data_nfs' => !empty($bens->data_nfs) ?  DataBR($bens->data_nfs) : '-',
                'origem' => !empty($bens->origem) ? $bens->origem : '-',
                'observacao' => !empty($bens->observacao) ? $bens->observacao : '-',
                'situacao_bem' => !empty($bens->getSituacao_bem_id()->descricao) ? $bens->getSituacao_bem_id()->descricao : '-',
                'situacao' => !empty($bens->getSituacao()->nome) ? $bens->getSituacao()->nome : '-'

            ];
        }
        $reportCreate = new Json();
        $reportCreate->create('reportBens.json', json_encode($json));
    }
    function report_geral_bens(){
        $this->setTemplate('reports');   
        $Imobilizado_bem =  Imobilizado_bem::getList();
        foreach($Imobilizado_bem as $bens){
            $json['bens'][] = [
                'id' => $bens->id,
                'descricao' => !empty($bens->descricao) ? $bens->descricao : '-',
                'complemento' => !empty($bens->complemento) ? $bens->complemento : '-',
                'numero_serie' => !empty($bens->numero_serie) ? $bens->numero_serie : '-', 
                'dt_aquisicao' =>  !empty($bens->dt_aquisicao) ? DataBR($bens->dt_aquisicao) : '-',
                'quantidade_atual' => !empty($bens->quantidade_atual) ? $bens->quantidade_atual : '-',
                'data_baixa' => !empty($bens->data_baixa) ? DataBR($bens->data_baixa) : '-',
                'data_manutencao' => !empty($bens->data_manutencao) ? DataBR($bens->data_manutencao) : '-',
                'vencimento_garantia' => !empty($bens->vencimento_garantia) ? DataBR($bens->vencimento_garantia) : '-', 
                'vida_util' => !empty($bens->vida_util) ? $bens->vida_util : '-',
                'valor_comprado' => !empty($bens->valor_comprado) ? $bens->valor_comprado : '-',
                'valor_reajuste' => !empty($bens->valor_reajuste) ? $bens->valor_reajuste : '-',
                'tipo_bem' => !empty($bens->getImobilizado_tipo_bem()->nome) ? $bens->getImobilizado_tipo_bem()->nome : '-',
                'conservacao' => !empty($bens->getImobilizado_conservacao()->descricao) ? $bens->getImobilizado_conservacao()->descricao : '-',
                'grupo' => !empty($bens->getImobilizado_grupo()->nome) ? $bens->getImobilizado_grupo()->nome : '-',
                'depreciacao' => !empty($bens->depreciacao) ? $bens->depreciacao : '-',
                'atualizado' => !empty($bens->atualizado) ? $bens->atualizado : '-',
                'localidade' => !empty($bens->getRhlocalidade()->cidade) ? $bens->getRhlocalidade()->cidade : '-',
                'nota_fiscal' => !empty($bens->nota_fiscal) ? $bens->nota_fiscal : '-',
                'data_nfs' => !empty($bens->data_nfs) ?  DataBR($bens->data_nfs) : '-',
                'origem' => !empty($bens->origem) ? $bens->origem : '-',
                'observacao' => !empty($bens->observacao) ? $bens->observacao : '-',
                'situacao_bem' => !empty($bens->getSituacao_bem_id()->descricao) ? $bens->getSituacao_bem_id()->descricao : '-',
                'situacao' => !empty($bens->getSituacao()->nome) ? $bens->getSituacao()->nome : '-'

            ];
        }
        $reportCreate = new Json();
        $reportCreate->create('reportBensGeral.json', json_encode($json));
    }
    function report_localidade_bens(){
        $this->setTemplate('reports');   
        $Imobilizado_bem =  Imobilizado_bem::getList();
        foreach($Imobilizado_bem as $bens){
            $json['bens'][] = [
                'id' => $bens->id,
                'descricao' => !empty($bens->descricao) ? $bens->descricao : '-',
                'complemento' => !empty($bens->complemento) ? $bens->complemento : '-',
                'numero_serie' => !empty($bens->numero_serie) ? $bens->numero_serie : '-', 
                'dt_aquisicao' =>  !empty($bens->dt_aquisicao) ? DataBR($bens->dt_aquisicao) : '-',
                'quantidade_atual' => !empty($bens->quantidade_atual) ? $bens->quantidade_atual : '-',
                'data_baixa' => !empty($bens->data_baixa) ? DataBR($bens->data_baixa) : '-',
                'data_manutencao' => !empty($bens->data_manutencao) ? DataBR($bens->data_manutencao) : '-',
                'vencimento_garantia' => !empty($bens->vencimento_garantia) ? DataBR($bens->vencimento_garantia) : '-', 
                'vida_util' => !empty($bens->vida_util) ? $bens->vida_util : '-',
                'valor_comprado' => !empty($bens->valor_comprado) ? $bens->valor_comprado : '-',
                'valor_reajuste' => !empty($bens->valor_reajuste) ? $bens->valor_reajuste : '-',
                'tipo_bem' => !empty($bens->getImobilizado_tipo_bem()->nome) ? $bens->getImobilizado_tipo_bem()->nome : '-',
                'conservacao' => !empty($bens->getImobilizado_conservacao()->descricao) ? $bens->getImobilizado_conservacao()->descricao : '-',
                'grupo' => !empty($bens->getImobilizado_grupo()->nome) ? $bens->getImobilizado_grupo()->nome : '-',
                'depreciacao' => !empty($bens->depreciacao) ? $bens->depreciacao : '-',
                'atualizado' => !empty($bens->atualizado) ? $bens->atualizado : '-',
                'localidade' => !empty($bens->getRhlocalidade()->cidade) ? $bens->getRhlocalidade()->cidade : '-',
                'nota_fiscal' => !empty($bens->nota_fiscal) ? $bens->nota_fiscal : '-',
                'data_nfs' => !empty($bens->data_nfs) ?  DataBR($bens->data_nfs) : '-',
                'origem' => !empty($bens->origem) ? $bens->origem : '-',
                'observacao' => !empty($bens->observacao) ? $bens->observacao : '-',
                'situacao_bem' => !empty($bens->getSituacao_bem_id()->descricao) ? $bens->getSituacao_bem_id()->descricao : '-',
                'situacao' => !empty($bens->getSituacao()->nome) ? $bens->getSituacao()->nome : '-'

            ];
        }
        $reportCreate = new Json();
        $reportCreate->create('reportBensLocalidade.json', json_encode($json));
    }

    function report_situacao_bens(){
        $this->setTemplate('reports');   
        $Imobilizado_bem =  Imobilizado_bem::getList();
        foreach($Imobilizado_bem as $bens){
            $json['bens'][] = [
                'id' => $bens->id,
                'descricao' => !empty($bens->descricao) ? $bens->descricao : '-',
                'complemento' => !empty($bens->complemento) ? $bens->complemento : '-',
                'numero_serie' => !empty($bens->numero_serie) ? $bens->numero_serie : '-', 
                'dt_aquisicao' =>  !empty($bens->dt_aquisicao) ? DataBR($bens->dt_aquisicao) : '-',
                'quantidade_atual' => !empty($bens->quantidade_atual) ? $bens->quantidade_atual : '-',
                'data_baixa' => !empty($bens->data_baixa) ? DataBR($bens->data_baixa) : '-',
                'data_manutencao' => !empty($bens->data_manutencao) ? DataBR($bens->data_manutencao) : '-',
                'vencimento_garantia' => !empty($bens->vencimento_garantia) ? DataBR($bens->vencimento_garantia) : '-', 
                'vida_util' => !empty($bens->vida_util) ? $bens->vida_util : '-',
                'valor_comprado' => !empty($bens->valor_comprado) ? $bens->valor_comprado : '-',
                'valor_reajuste' => !empty($bens->valor_reajuste) ? $bens->valor_reajuste : '-',
                'tipo_bem' => !empty($bens->getImobilizado_tipo_bem()->nome) ? $bens->getImobilizado_tipo_bem()->nome : '-',
                'conservacao' => !empty($bens->getImobilizado_conservacao()->descricao) ? $bens->getImobilizado_conservacao()->descricao : '-',
                'grupo' => !empty($bens->getImobilizado_grupo()->nome) ? $bens->getImobilizado_grupo()->nome : '-',
                'depreciacao' => !empty($bens->depreciacao) ? $bens->depreciacao : '-',
                'atualizado' => !empty($bens->atualizado) ? $bens->atualizado : '-',
                'localidade' => !empty($bens->getRhlocalidade()->cidade) ? $bens->getRhlocalidade()->cidade : '-',
                'nota_fiscal' => !empty($bens->nota_fiscal) ? $bens->nota_fiscal : '-',
                'data_nfs' => !empty($bens->data_nfs) ?  DataBR($bens->data_nfs) : '-',
                'origem' => !empty($bens->origem) ? $bens->origem : '-',
                'observacao' => !empty($bens->observacao) ? $bens->observacao : '-',
                'situacao_bem' => !empty($bens->getSituacao_bem_id()->descricao) ? $bens->getSituacao_bem_id()->descricao : '-',
                'situacao' => !empty($bens->getSituacao()->nome) ? $bens->getSituacao()->nome : '-'
            ];
        }
        $reportCreate = new Json();
        $reportCreate->create('reportBensSituacao.json', json_encode($json));
    }
}