<?php
final class RhsetorController extends AppController{ 

    # página inicial do módulo Rhsetor
    function index(){
        $this->setTitle('Visualização de Setor');
    }

    # lista de Rhsetores
    # renderiza a visão /view/Rhsetor/all.php
    function all(){
        $this->setTitle('Setor');
        $p = new Paginate('Rhsetor', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhsetor', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $this->set('Rhsecao',  Rhsecao::getList());
        $this->set('Rhstatus',  Rhstatus::getList());


    }

    # visualiza um(a) Rhsetor
    # renderiza a visão /view/Rhsetor/view.php
    function view(){
        $this->setTitle('Visualização de Setor');
        try {
            $this->set('Rhsetor', new Rhsetor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhsetor', 'all');
        }
    }

    # formulário de cadastro de Rhsetor
    # renderiza a visão /view/Rhsetor/add.php
    function add(){
        $this->setTitle('Cadastro de Setor');
        $this->set('Rhsetor', new Rhsetor);
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhsecaos',  Rhsecao::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhsetor
    # (true)redireciona ou (false) renderiza a visão /view/Rhsetor/add.php
    function post_add(){
        $this->setTitle('Cadastro de Setor');
        $Rhsetor = new Rhsetor();
        $this->set('Rhsetor', $Rhsetor);
        $user=Session::get('user');// para salvar o usuario que está fazendo
        $Rhsetor->cadastradopor=$user->id; // para salvar o usuario que está fazendo
        $Rhsetor->dtCadastro=date('Y-m-d H:i:s'); //salva a hora que está fazendo
        $_POST['status']=1;
        try {
            $Rhsetor->save($_POST);
            new Msg(__('Setor cadastrado com sucesso'));
            $this->go('Rhsetor', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhsecaos',  Rhsecao::getList());
    }

    # formulário de edição de Rhsetor
    # renderiza a visão /view/Rhsetor/edit.php
    function edit(){
        $this->setTitle('Edição de Setor');
        try {
            $this->set('Rhsetor', new Rhsetor((int) $this->getParam('id')));
            $this->set('Rhstatus',  Rhstatus::getList());
            $this->set('Rhsecaos',  Rhsecao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhsetor', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhsetor
    # (true)redireciona ou (false) renderiza a visão /view/Rhsetor/edit.php
    function post_edit(){
        $this->setTitle('Edição de Setor');
        try {
            $Rhsetor = new Rhsetor((int) $_POST['codigo']);
            $this->set('Rhsetor', $Rhsetor);

            $Rhsetor->dtAtualizacao=date('Y-m-d H:i:s');//para salvar a data e a hora da ultima atualização
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhsetor->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhsetor->save($_POST);
            new Msg(__('Setor atualizado com sucesso'));
            $this->go('Rhsetor', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhstatus',  Rhstatus::getList());
        $this->set('Rhsecaos',  Rhsecao::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhsetor
    # renderiza a /view/Rhsetor/delete.php
    function delete(){
        $this->setTitle('Apagar Setor');
        try {
            $this->set('Rhsetor', new Rhsetor((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhsetor', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhsetor
    # redireciona para Rhsetor/all
    function post_delete(){
        try {
            $Rhsetor = new Rhsetor((int) $_POST['id']);
            $user=Session::get('user');// para salvar o usuario que está fazendo
            $Rhsetor->atualizadopor=$user->id; // para salvar o usuario que está fazendo
            $Rhsetor->dtAtualizacao=date('Y-m-d H:i:s'); //salva a hora que está fazendo
            $Rhsetor->status=3;
            $Rhsetor->save();
            new Msg(__('Setor deletado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhsetor', 'all');
    }

}