<?php
final class Rhprofissional_salario_historicoController extends AppController{ 

    # página inicial do módulo Rhprofissional_salario_historico
    function index(){
        $this->setTitle('Visualização de Rhprofissional_salario_historico');
    }

    # lista de Rhprofissional_salario_historicos
    # renderiza a visão /view/Rhprofissional_salario_historico/all.php
    function all(){
        $this->setTitle('Listagem de Rhprofissional_salario_historico');
        $p = new Paginate('Rhprofissional_salario_historico', 10);
        $c = new Criteria();
        $c->addCondition('rhprofissional_contratacao_id', "=", $_GET['id']);
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhprofissional_salario_historicos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhprofissional_salario_historico
    # renderiza a visão /view/Rhprofissional_salario_historico/view.php
    function view(){
        $this->setTitle('Visualização de Rhprofissional_salario_historico');
        try {
            $this->set('Rhprofissional_salario_historico', new Rhprofissional_salario_historico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_salario_historico', 'all');
        }
    }

    # formulário de cadastro de Rhprofissional_salario_historico
    # renderiza a visão /view/Rhprofissional_salario_historico/add.php
    function add(){
        $this->setTitle('Cadastro de Rhprofissional_salario_historico');
        $this->set('Rhprofissional_salario_historico', new Rhprofissional_salario_historico);
        $this->set('Rhprofissional_contratacaos',  Rhprofissional_contratacao::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # recebe os dados enviados via post do cadastro de Rhprofissional_salario_historico
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_salario_historico/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rhprofissional_salario_historico');
        $Rhprofissional_salario_historico = new Rhprofissional_salario_historico();
        $this->set('Rhprofissional_salario_historico', $Rhprofissional_salario_historico);
        try {
            $Rhprofissional_salario_historico->save($_POST);
            new Msg(__('Rhprofissional_salario_historico cadastrado com sucesso'));
            $this->go('Rhprofissional_salario_historico', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Rhprofissional_contratacaos',  Rhprofissional_contratacao::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # formulário de edição de Rhprofissional_salario_historico
    # renderiza a visão /view/Rhprofissional_salario_historico/edit.php
    function edit(){
        $this->setTitle('Edição de Rhprofissional_salario_historico');
        try {
            $this->set('Rhprofissional_salario_historico', new Rhprofissional_salario_historico((int) $this->getParam('id')));
            $this->set('Rhprofissional_contratacaos',  Rhprofissional_contratacao::getList());
            $this->set('Situacaos',  Situacao::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhprofissional_salario_historico', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhprofissional_salario_historico
    # (true)redireciona ou (false) renderiza a visão /view/Rhprofissional_salario_historico/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhprofissional_salario_historico');
        try {
            $Rhprofissional_salario_historico = new Rhprofissional_salario_historico((int) $_POST['id']);
            $this->set('Rhprofissional_salario_historico', $Rhprofissional_salario_historico);
            $Rhprofissional_salario_historico->save($_POST);
            new Msg(__('Rhprofissional_salario_historico atualizado com sucesso'));
            $this->go('Rhprofissional_salario_historico', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Rhprofissional_contratacaos',  Rhprofissional_contratacao::getList());
        $this->set('Situacaos',  Situacao::getList());
    }

    # Confirma a exclusão ou não de um(a) Rhprofissional_salario_historico
    # renderiza a /view/Rhprofissional_salario_historico/delete.php
    function delete(){
        $this->setTitle('Apagar Rhprofissional_salario_historico');
        try {
            $this->set('Rhprofissional_salario_historico', new Rhprofissional_salario_historico((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhprofissional_salario_historico', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhprofissional_salario_historico
    # redireciona para Rhprofissional_salario_historico/all
    function post_delete(){
        try {
            $Rhprofissional_salario_historico = new Rhprofissional_salario_historico((int) $_POST['id']);
            $Rhprofissional_salario_historico->delete();
            new Msg(__('Rhprofissional_salario_historico apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhprofissional_salario_historico', 'all');
    }

}