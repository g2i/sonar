<?php
final class Ocorencia_moduloController extends AppController{ 

    # página inicial do módulo Ocorencia_modulo
    function index(){
        $this->setTitle('Visualização de Ocorencia_modulo');
    }

    # lista de Ocorencia_modulos
    # renderiza a visão /view/Ocorencia_modulo/all.php
    function all(){
        $this->setTitle('Listagem de Ocorencia_modulo');
        $p = new Paginate('Ocorencia_modulo', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Ocorencia_modulos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Ocorencia_modulo
    # renderiza a visão /view/Ocorencia_modulo/view.php
    function view(){
        $this->setTitle('Visualização de Ocorencia_modulo');
        try {
            $this->set('Ocorencia_modulo', new Ocorencia_modulo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Ocorencia_modulo', 'all');
        }
    }

    # formulário de cadastro de Ocorencia_modulo
    # renderiza a visão /view/Ocorencia_modulo/add.php
    function add(){
        $this->setTitle('Cadastro de Ocorencia_modulo');
        $this->set('Ocorencia_modulo', new Ocorencia_modulo);
        $this->set('Modulos',  Modulo::getList());
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
    }

    # recebe os dados enviados via post do cadastro de Ocorencia_modulo
    # (true)redireciona ou (false) renderiza a visão /view/Ocorencia_modulo/add.php
    function post_add(){
        $this->setTitle('Cadastro de Ocorencia_modulo');
        $Ocorencia_modulo = new Ocorencia_modulo();
        $this->set('Ocorencia_modulo', $Ocorencia_modulo);
        try {
            $Ocorencia_modulo->save($_POST);
            new Msg(__('Ocorencia_modulo cadastrado com sucesso'));
            $this->go('Ocorencia_modulo', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Modulos',  Modulo::getList());
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
    }

    # formulário de edição de Ocorencia_modulo
    # renderiza a visão /view/Ocorencia_modulo/edit.php
    function edit(){
        $this->setTitle('Edição de Ocorencia_modulo');
        try {
            $this->set('Ocorencia_modulo', new Ocorencia_modulo((int) $this->getParam('id')));
            $this->set('Modulos',  Modulo::getList());
            $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Ocorencia_modulo', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Ocorencia_modulo
    # (true)redireciona ou (false) renderiza a visão /view/Ocorencia_modulo/edit.php
    function post_edit(){
        $this->setTitle('Edição de Ocorencia_modulo');
        try {
            $Ocorencia_modulo = new Ocorencia_modulo((int) $_POST['id']);
            $this->set('Ocorencia_modulo', $Ocorencia_modulo);
            $Ocorencia_modulo->save($_POST);
            new Msg(__('Ocorencia_modulo atualizado com sucesso'));
            $this->go('Ocorencia_modulo', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Modulos',  Modulo::getList());
        $this->set('Rhocorrencia_historicos',  Rhocorrencia_historico::getList());
    }

    # Confirma a exclusão ou não de um(a) Ocorencia_modulo
    # renderiza a /view/Ocorencia_modulo/delete.php
    function delete(){
        $this->setTitle('Apagar Ocorencia_modulo');
        try {
            $this->set('Ocorencia_modulo', new Ocorencia_modulo((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Ocorencia_modulo', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Ocorencia_modulo
    # redireciona para Ocorencia_modulo/all
    function post_delete(){
        try {
            $Ocorencia_modulo = new Ocorencia_modulo((int) $_POST['id']);
            $Ocorencia_modulo->delete();
            new Msg(__('Ocorencia_modulo apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Ocorencia_modulo', 'all');
    }

}