<?php
final class Imobilizado_movimentoController extends AppController{ 

    # página inicial do módulo Imobilizado_movimento
    function index(){
        $this->setTitle('Visualização de Imobilizado_movimento');
    }

    # lista de Imobilizado_movimentos
    # renderiza a visão /view/Imobilizado_movimento/all.php
    function all(){
        $this->setTitle('Listagem de Imobilizado_movimento');
        $p = new Paginate('Imobilizado_movimento', 10);
        $c = new Criteria();
        if(!empty($_POST["filtro"])){
            if(!empty($_POST["filtro"]["interno"])){
                foreach($_POST["filtro"]["interno"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "LIKE", "%" . $fv . "%");
                    }
                }
            }
            if(!empty($_POST["filtro"]["externo"])){
                foreach($_POST["filtro"]["externo"] as $fl => $fv ){
                    $this->setParam($fl,$fv);
                    if(!empty($fv)){
                        $c->addCondition($fl, "=", $fv);
                    }
                }
            }
        }
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $c->addCondition('situacao_id','=',1);
        $c->addCondition('bem_id', '=', $this->getParam('id'));
        $this->set('Imobilizado_movimentos', $p->getPage($c));
        $this->set('nav', $p->getNav());

        $this->set('Imobiliado_tipo_movimentos',  Imobiliado_tipo_movimento::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Imobilizado_bens',  Imobilizado_bem::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    

    }

    # visualiza um(a) Imobilizado_movimento
    # renderiza a visão /view/Imobilizado_movimento/view.php
    function view(){
        $this->setTitle('Visualização de Imobilizado_movimento');
        try {
            $this->set('Imobilizado_movimento', new Imobilizado_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_movimento', 'all');
        }
    }

    # formulário de cadastro de Imobilizado_movimento
    # renderiza a visão /view/Imobilizado_movimento/add.php
    function add(){
        $this->setTitle('Cadastro de Imobilizado_movimento');
        $this->set('Imobilizado_movimento', new Imobilizado_movimento);
        $this->set('Imobiliado_tipo_movimentos',  Imobiliado_tipo_movimento::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Imobilizado_bens',  Imobilizado_bem::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # recebe os dados enviados via post do cadastro de Imobilizado_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_movimento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Imobilizado_movimento');
        $Imobilizado_movimento = new Imobilizado_movimento();
        $this->set('Imobilizado_movimento', $Imobilizado_movimento);
        try {
            $Imobilizado_movimento->cadastrado_por = Session::get('user')->id;
            $Imobilizado_movimento->dt_cadastro = date('Y-m-d H:i:d');
            $Imobilizado_movimento->situacao_id = 1;
            $Imobilizado_movimento->save($_POST);
            new Msg(__('Imobilizado_movimento cadastrado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_movimento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->set('Imobiliado_tipo_movimentos',  Imobiliado_tipo_movimento::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Imobilizado_bens',  Imobilizado_bem::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # formulário de edição de Imobilizado_movimento
    # renderiza a visão /view/Imobilizado_movimento/edit.php
    function edit(){
        $this->setTitle('Edição de Imobilizado_movimento');
        try {
            $this->set('Imobilizado_movimento', new Imobilizado_movimento((int) $this->getParam('id')));
            $this->set('Imobiliado_tipo_movimentos',  Imobiliado_tipo_movimento::getList());
            $this->set('Rhlocalidades',  Rhlocalidade::getList());
            $this->set('Imobilizado_bens',  Imobilizado_bem::getList());
            $this->set('Situacaos',  Situacao::getList());
            $this->set('Usuarios',  Usuario::getList());
            $this->set('Usuarios',  Usuario::getList());
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Imobilizado_movimento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Imobilizado_movimento
    # (true)redireciona ou (false) renderiza a visão /view/Imobilizado_movimento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Imobilizado_movimento');
        try {
            $Imobilizado_movimento = new Imobilizado_movimento((int) $_POST['id']);
            $this->set('Imobilizado_movimento', $Imobilizado_movimento);
            $Imobilizado_movimento->modificado_por = Session::get('user')->id;
            $Imobilizado_movimento->dt_modificado = date('Y-m-d H:i:d');
            $Imobilizado_movimento->save($_POST);
            new Msg(__('Imobilizado_movimento atualizado com sucesso'));
            if (!empty($_POST['modal'])) {
                echo 1;
                exit;
            }
            $this->go('Imobilizado_movimento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
        $this->set('Imobiliado_tipo_movimentos',  Imobiliado_tipo_movimento::getList());
        $this->set('Rhlocalidades',  Rhlocalidade::getList());
        $this->set('Imobilizado_bens',  Imobilizado_bem::getList());
        $this->set('Situacaos',  Situacao::getList());
        $this->set('Usuarios',  Usuario::getList());
        $this->set('Usuarios',  Usuario::getList());
    }

    # Confirma a exclusão ou não de um(a) Imobilizado_movimento
    # renderiza a /view/Imobilizado_movimento/delete.php
    function delete(){
        $this->setTitle('Apagar Imobilizado_movimento');
        try {
            $this->set('Imobilizado_movimento', new Imobilizado_movimento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Imobilizado_movimento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Imobilizado_movimento
    # redireciona para Imobilizado_movimento/all
    function post_delete(){
        try {
            $Imobilizado_movimento = new Imobilizado_movimento((int) $_POST['id']);
            $Imobilizado_movimento->situacao_id = 3;
            $Imobilizado_movimento->save();
            new Msg(__('Imobilizado_movimento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        if (!empty($_POST['modal'])) {
            echo 1;
            exit;
        }
        $this->go('Imobilizado_movimento', 'all');
    }

}