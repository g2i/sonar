<?php
final class Rhsituacao_afastamentoController extends AppController{ 

    # página inicial do módulo Rhsituacao_afastamento
    function index(){
        $this->setTitle('Visualização de Rhsituacao_afastamento');
    }

    # lista de Rhsituacao_afastamentos
    # renderiza a visão /view/Rhsituacao_afastamento/all.php
    function all(){
        $this->setTitle('Listagem de Rhsituacao_afastamento');
        $p = new Paginate('Rhsituacao_afastamento', 10);
        $c = new Criteria();
        if ($this->getParam('orderBy')) {
            $c->setOrder($this->getParam('orderBy'));
        }
        $this->set('Rhsituacao_afastamentos', $p->getPage($c));
        $this->set('nav', $p->getNav());
    }

    # visualiza um(a) Rhsituacao_afastamento
    # renderiza a visão /view/Rhsituacao_afastamento/view.php
    function view(){
        $this->setTitle('Visualização de Rhsituacao_afastamento');
        try {
            $this->set('Rhsituacao_afastamento', new Rhsituacao_afastamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhsituacao_afastamento', 'all');
        }
    }

    # formulário de cadastro de Rhsituacao_afastamento
    # renderiza a visão /view/Rhsituacao_afastamento/add.php
    function add(){
        $this->setTitle('Cadastro de Rhsituacao_afastamento');
        $this->set('Rhsituacao_afastamento', new Rhsituacao_afastamento);
    }

    # recebe os dados enviados via post do cadastro de Rhsituacao_afastamento
    # (true)redireciona ou (false) renderiza a visão /view/Rhsituacao_afastamento/add.php
    function post_add(){
        $this->setTitle('Cadastro de Rhsituacao_afastamento');
        $Rhsituacao_afastamento = new Rhsituacao_afastamento();
        $this->set('Rhsituacao_afastamento', $Rhsituacao_afastamento);
        try {
            $Rhsituacao_afastamento->save($_POST);
            new Msg(__('Rhsituacao_afastamento cadastrado com sucesso'));
            $this->go('Rhsituacao_afastamento', 'all');
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
    }

    # formulário de edição de Rhsituacao_afastamento
    # renderiza a visão /view/Rhsituacao_afastamento/edit.php
    function edit(){
        $this->setTitle('Edição de Rhsituacao_afastamento');
        try {
            $this->set('Rhsituacao_afastamento', new Rhsituacao_afastamento((int) $this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
            $this->go('Rhsituacao_afastamento', 'all');
        }
    }

    # recebe os dados enviados via post da edição de Rhsituacao_afastamento
    # (true)redireciona ou (false) renderiza a visão /view/Rhsituacao_afastamento/edit.php
    function post_edit(){
        $this->setTitle('Edição de Rhsituacao_afastamento');
        try {
            $Rhsituacao_afastamento = new Rhsituacao_afastamento((int) $_POST['id']);
            $this->set('Rhsituacao_afastamento', $Rhsituacao_afastamento);
            $Rhsituacao_afastamento->save($_POST);
            new Msg(__('Rhsituacao_afastamento atualizado com sucesso'));
            $this->go('Rhsituacao_afastamento', 'all');
        } catch (Exception $e) {
            new Msg(__('Não foi possível atualizar.'), 2);
        }
    }

    # Confirma a exclusão ou não de um(a) Rhsituacao_afastamento
    # renderiza a /view/Rhsituacao_afastamento/delete.php
    function delete(){
        $this->setTitle('Apagar Rhsituacao_afastamento');
        try {
            $this->set('Rhsituacao_afastamento', new Rhsituacao_afastamento((int)$this->getParam('id')));
        } catch (Exception $e) {
            new Msg($e->getMessage(), 2);
            $this->go('Rhsituacao_afastamento', 'all');
        }
    }

    # Recebe o id via post e exclui um(a) Rhsituacao_afastamento
    # redireciona para Rhsituacao_afastamento/all
    function post_delete(){
        try {
            $Rhsituacao_afastamento = new Rhsituacao_afastamento((int) $_POST['id']);
            $Rhsituacao_afastamento->delete();
            new Msg(__('Rhsituacao_afastamento apagado com sucesso'), 1);
        } catch (Exception $e) {
            new Msg($e->getMessage(),3);
        }
        $this->go('Rhsituacao_afastamento', 'all');
    }

}