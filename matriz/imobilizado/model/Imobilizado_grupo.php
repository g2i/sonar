<?php
final class Imobilizado_grupo extends Record{ 

    const TABLE = 'imobilizado_grupo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Imobilizado_grupo pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Imobilizado_grupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Imobilizado_grupo pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}