<?php
final class Sesmet_pergunta extends Record{ 

    const TABLE = 'sesmet_pergunta';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Sesmet_pergunta pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Sesmet_pergunta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Sesmet_pergunta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
    
    /**
    * Sesmet_pergunta pertence a Sesmet_grupo
    * @return Sesmet_grupo $Sesmet_grupo
    */
    function getSesmet_grupo() {
        return $this->belongsTo('Sesmet_grupo','sesmet_grupo');
    }
}