<?php
final class Imobilizado_bem extends Record{ 

    const TABLE = 'imobilizado_bem';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Imobilizado_bem pertence a Imobilizado_tipo_bem
    * @return Imobilizado_tipo_bem $Imobilizado_tipo_bem
    */
    function getImobilizado_tipo_bem() {
        return $this->belongsTo('Imobilizado_tipo_bem','tipo_bem_id');
    }
    
    /**
    * Imobilizado_bem pertence a Imobilizado_conservacao
    * @return Imobilizado_conservacao $Imobilizado_conservacao
    */
    function getImobilizado_conservacao() {
        return $this->belongsTo('Imobilizado_conservacao','conservacao_id');
    }
    
    /**
    * Imobilizado_bem pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Imobilizado_bem pertence a Imobilizado_grupo
    * @return Imobilizado_grupo $Imobilizado_grupo
    */
    function getImobilizado_grupo() {
        return $this->belongsTo('Imobilizado_grupo','grupo_id');
    }
    
    /**
    * Imobilizado_bem pertence a Rhlocalidade
    * @return Rhlocalidade $Rhlocalidade
    */
    function getRhlocalidade() {
        return $this->belongsTo('Rhlocalidade','localidade_id');
    }
    
    /**
    * Imobilizado_bem pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Imobilizado_bem pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
    function getSituacao_bem_id() {
        return $this->belongsTo('Imobilizado_situacao_bem','situacao_bem_id');
    }
}