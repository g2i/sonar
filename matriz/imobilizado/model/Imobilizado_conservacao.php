<?php
final class Imobilizado_conservacao extends Record{ 

    const TABLE = 'imobilizado_conservacao';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Imobilizado_conservacao pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Imobilizado_conservacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Imobilizado_conservacao pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}