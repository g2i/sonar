<?php
final class Modulo extends Record{ 

    const TABLE = 'modulo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Modulo possui Usuario_modulos
    * @return array de Usuario_modulos
    */
    function getUsuario_modulos($criteria=NULL) {
        return $this->hasMany('Usuario_modulo','modulo_id',$criteria);
    }
}