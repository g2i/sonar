<?php
final class Ocorencia_modulo extends Record{ 

    const TABLE = 'ocorencia_modulo';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Ocorencia_modulo pertence a Modulo
    * @return Modulo $Modulo
    */
    function getModulo() {
        return $this->belongsTo('Modulo','modulo_id');
    }
    
    /**
    * Ocorencia_modulo pertence a Rhocorrencia_historico
    * @return Rhocorrencia_historico $Rhocorrencia_historico
    */
    function getRhocorrencia_historico() {
        return $this->belongsTo('Rhocorrencia_historico','rhocorrencia_historico_id');
    }
}