<?php
final class Imobilizado_movimento extends Record{ 

    const TABLE = 'imobilizado_movimento';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Imobilizado_movimento pertence a Imobiliado_tipo_movimento
    * @return Imobiliado_tipo_movimento $Imobiliado_tipo_movimento
    */
    function getImobiliado_tipo_movimento() {
        return $this->belongsTo('Imobiliado_tipo_movimento','tipo_movimento_id');
    }
    
    /**
    * Imobilizado_movimento pertence a Rhlocalidade
    * @return Rhlocalidade $Rhlocalidade
    */
    function getRhlocalidade() {
        return $this->belongsTo('Rhlocalidade','localidade_id');
    }
    
    /**
    * Imobilizado_movimento pertence a Imobilizado_tipo_bem
    * @return Imobilizado_tipo_bem $Imobilizado_tipo_bem
    */
    function getImobilizado_bem() {
        return $this->belongsTo('Imobilizado_bem','bem_id');
    }
    
    /**
    * Imobilizado_movimento pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
    /**
    * Imobilizado_movimento pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Imobilizado_movimento pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
}