<?php
final class Rhprofissional_contratacao extends Record{ 

    const TABLE = 'rhprofissional_contratacao';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        //$criteria = new Criteria();
        //return $criteria;
    }
    
    /**
    * Rhprofissional_contratacao pertence a Rhcentrocusto
    * @return Rhcentrocusto $Rhcentrocusto
    */
    function getRhcentrocusto() {
        return $this->belongsTo('Rhcentrocusto','centrocusto');
    }
    
    /**
    * Rhprofissional_contratacao pertence a Rhprofissional
    * @return Rhprofissional $Rhprofissional
    */
    function getRhprofissional() {
        return $this->belongsTo('Rhprofissional','codigo_profissional');
    }
    function getRhprofissional_funcao() {
        return $this->belongsTo('Rhprofissional_funcao','funcao');
    }
}