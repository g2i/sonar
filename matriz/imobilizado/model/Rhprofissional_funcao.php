<?php
final class Rhprofissional_funcao extends Record{ 

    const TABLE = 'rhprofissional_funcao';
    const PK = 'codigo';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
         $criteria = new Criteria();
        $criteria->addCondition('status','<>',3);
         return $criteria;
    }
    
    /**
    * Rhprofissional_funcao pertence a Rhstatus
    * @return Rhstatus $Rhstatus
    */
    function getRhstatus() {
        return $this->belongsTo('Rhstatus','status');
    }
}