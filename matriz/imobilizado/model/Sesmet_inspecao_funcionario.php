<?php
final class Sesmet_inspecao_funcionario extends Record{ 

    const TABLE = 'sesmet_inspecao_funcionario';
    const PK = 'id';
    
    /**
    * Configurações e filtros globais do modelo
    * @return Criteria $criteria
    */
    public static function configure(){
        # $criteria = new Criteria();
        # return $criteria;
    }
    
    /**
    * Sesmet_inspecao_funcionario pertence a Sesmet_inspecao
    * @return Sesmet_inspecao $Sesmet_inspecao
    */
    function getSesmet_inspecao() {
        return $this->belongsTo('Sesmet_inspecao','sesmet_inspecao_id');
    }
    
    /**
    * Sesmet_inspecao_funcionario pertence a Rhprofissional
    * @return Rhprofissional $Rhprofissional
    */
    function getRhprofissional() {
        return $this->belongsTo('Rhprofissional','rhfuncionario_id');
    }

     /**
    * Sesmet_inspecao_pergunta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario() {
        return $this->belongsTo('Usuario','cadastrado_por');
    }
    
    /**
    * Sesmet_inspecao_pergunta pertence a Usuario
    * @return Usuario $Usuario
    */
    function getUsuario2() {
        return $this->belongsTo('Usuario','modificado_por');
    }
    
    /**
    * Sesmet_inspecao_pergunta pertence a Situacao
    * @return Situacao $Situacao
    */
    function getSituacao() {
        return $this->belongsTo('Situacao','situacao_id');
    }
    
}