
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Dss </h2>
    <ol class="breadcrumb">
    <li>Dss</li>
    <li class="active">
    <strong>Adicionar </strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Dss_curso', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
      <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="palestrante">Palestrante</label>
            <input type="text" name="palestrante" id="palestrante" class="form-control" value="<?php echo $Dss_curso->palestrante ?>" placeholder="Palestrate">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="tema">Tema</label>
            <input type="text" name="tema" id="tema" class="form-control" value="<?php echo $Dss_curso->tema ?>" placeholder="Tema">
        </div>   
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="data_palestra">Data<span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="date" name="data_palestra" id="data_palestra" class="form-control" value="<?php echo $Dss_curso->data_palestra ?>">
        </div>   
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="localidade">Localidade</label>
            <input type="text" name="localidade" id="localidade" class="form-control" value="<?php echo $Dss_curso->localidade ?>" placeholder="Localidade">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="empresa">Empresa</label>
            <input type="text" name="empresa" id="empresa" class="form-control" value="<?php echo $Dss_curso->empresa ?>" placeholder="Empresa">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="divisao_projeto">Divisão Projeto</label>
            <input type="text" name="divisao_projeto" id="divisao_projeto" class="form-control" value="<?php echo $Dss_curso->divisao_projeto ?>" placeholder="Divisão Projeto">
        </div>        
        <div class="form-group col-md-12 col-sm-6 col-xs-12">
            <label for="descricao">Descrição</label>
            <textarea name="descricao" id="descricao" class="form-control"><?php echo $Dss_curso->descricao ?></textarea>
        </div>
        <div class="form-group col-md-12 col-sm-6 col-xs-12">
            <label for="observacao">Observação</label>
            <textarea name="observacao" id="observacao" class="form-control"><?php echo $Dss_curso->observacao ?></textarea>
        </div>      
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Dss_curso', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>