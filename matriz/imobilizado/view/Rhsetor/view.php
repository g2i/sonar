<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox">
                <div class="ibox-content no-borders">
                    <div class="text-right" style="padding-bottom: 20px">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>

<legend>Setor</legend>
                <p><strong>Nome</strong>: <?php echo $Rhsetor->nome;?></p>
<p>
    <strong>Status</strong>:
    <?php
    echo $this->Html->getLink($Rhsetor->getRhstatus()->descricao, 'Rhstatus', 'view',
    array('id' => $Rhsetor->getRhstatus()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Seção</strong>:
    <?php
    echo $this->Html->getLink($Rhsetor->getRhsecao()->nome, 'Rhsecao', 'view',
    array('id' => $Rhsetor->getRhsecao()->codigo), // variaveis via GET opcionais
    array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
</div><!-- /ibox content -->
</div><!-- / ibox -->
</div><!-- /wrapper-->
</div> <!-- /col-lg 12 -->
</div><!-- /row -->
