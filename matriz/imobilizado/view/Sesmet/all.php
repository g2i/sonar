<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">   
            <div class="col-lg-6">
            </div>
        </div>
       
        <div class="col-lg-12">           
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">vencidas</span>
                        <h5>Ocorrências</h5>
                    </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $ocorencias_vencidas->totale; ?></h1>
                    <div class="stat-percent font-bold text-danger"><i class="fa fa-level-down"></i></div>
                        <small>ocorrências</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-warning pull-right">Ocorrências para vencer com 45 dias</span>
                        <h5>Ocorrências</h5>
                    </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo $ocorenciasCont->totale; ?></h1>
                    <div class="stat-percent font-bold text-warning"><i class="fa fa-level-down"></i></div>
                        <small>ocorrências</small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
                    <div class="ibox float-e-margins">                        
                        <div class="ibox-content">
                            <!-- formulario de pesquisa-->
                            <div class="filtros well">
                                <div class="form">
                                    <form role="form"
                                    action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                    method="post" enctype="application/x-www-form-urlencoded">
                                        <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                        <input type="hidden" name="p" value="<?php echo ACTION; ?>">                                       
                                        <div class="col-md-3 form-group">
                                            <label for="inicio">Inicio</label>
                                            <input type="date" name="inicio" id="inicio" class="form-control maskData" value="<?php echo $this->getParam('data'); ?>">
                                        </div>    
                                        <div class="col-md-3 form-group">
                                            <label for="fim">Fim</label>
                                            <input type="date" name="fim" id="fim" class="form-control maskData" value="<?php echo $this->getParam('data'); ?>">
                                        </div>              
                                        <div class="col-md-3 form-group">
                                            <label for="codigo_profissional">Funcionario</label>
                                            <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                                                    <?php echo '<option value="">Selecione:</option>';  ?>
                                                <?php foreach ($Rhprofissionais as $f): ?>
                                                    <?php echo '<option value="' . $f->codigo . '">' . $f->nome . '</option>';  ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>                                        
                                        <div class="col-md-3 form-group">
                                            <label for="historico">Ocorrência</label>
                                            <select name="historico" class="form-control" id="historico">
                                            <?php echo '<option value="">Selecione:</option>';  ?>
                                                <?php foreach ($Rhocorrencia_historicos as $f): ?>
                                                    <?php echo '<option value="' . $f->codigo . '">' . $f->nome . '</option>';  ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-12 text-right">
                                        <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"
                                        class="btn btn-default"
                                        data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                        title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                                        <button type="button" class="btn btn-default" id="buscar-filtro" data-tool="tooltip"
                                                data-placement="bottom" title="Pesquisar"><span
                                                class="glyphicon glyphicon-search"></span></button>                                
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ocorrências</h5>
                </div>
                
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="text-right">
                             <p><?php echo $this->Html->getLink('<span class="img img-add"></span> Ocorrências', 'Rhocorrencia', 'add', 
                            array('first' => 1,'modal' => 1), 
                            array('class' => 'btn btn-default','data-toggle' => 'modal', 'data-placement' => "bottom", 
                            'title' => "Cadastrar Profissional")); ?></p>

                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Colaborador</th>
                                    <th>Ocorrência</th>
                                    <th>Periodicidade</th>
                                    <th>Última Data</th>
                                    <th>Dias</th>
                                    <th>Vencimento</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach($ocor as $atrasada){
                                $clas = 'class="bg-danger"';
                                if($atrasada->getRhprofissional()->status == 1){
                                echo '<tr ' . $clas . '>';                            
                                echo '<td>' . $atrasada->getRhprofissional()->nome . '</td>';
                                echo '<td>' . $atrasada->getRhocorrencia_historico()->nome . '</td>';
                                echo '<td>' . $atrasada->getRhocorrencia_historico()->periodicidade . '</td>';
                                echo '<td>' . ConvertData($atrasada->data) . '</td>';
                                //vencimento
                                echo '<td>' . diferenca_datas($atrasada->data, $atrasada->data_especifica) . '</td>';
                                echo '<td>' . ConvertData($atrasada->data_especifica) . '</td>';
                                echo '</tr>';
                                }
                             
                            }              
                           foreach ($vencimentoCom45Dias as $o) {
                                    $clas = "";
                                    if (strtotime($o->vencimento) < strtotime(date('Y-m-d'))) {
                                        $clas = 'class="text-danger"';
                                    }
                                  
                                    echo '<tr ' . $clas . '>';
                                    echo '<td>' . $o->nome . '</td>';
                                    echo '<td>' . $o->descricao . '</td>';
                                    echo '<td>' . $o->periodicidade . '</td>';
                                    echo '<td>' . ConvertData($o->data) . '</td>';
                                    //vencimento
                                    echo '<td>' . diferenca_datas($o->data, $o->vencimento) . '</td>';
                                    echo '<td>' . ConvertData($o->vencimento) . '</td>';                               
                                   /*  echo '<td  style="padding: 2px;">';
                                   echo $this->Html->getLink('<span class="img img-edit" data-toggle="tooltip" data-placement="auto" title="Editar Histórico de Ocorrência"></span> ', 'Rhocorrencia', 'all',
                                        array('id' => $o->id_prof, 'modal' => 1, 'first' => 1, 'ajax' => true),
                                        array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                                    echo '</td>';*/
                                    echo '</tr>';
                                
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
   
   /* $(function () {
        $.ajax({
            type: 'POST',
            url: root + '/Index/setor',
            success: function (txt) {
                Morris.Donut({
                    element: 'morris-donut-chart',
                    data: jQuery.parseJSON(txt),
                    resize: true,
                    colors: ['#87d6c6', '#54cdb4', '#1ab394', '#9de8d8', '#b7eee2']
                });
            }
        });


    });*/
</script>