<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Imobilizado Bem</h2>
        <ol class="breadcrumb">
            <li>Imobilizado Bem</li>
            <li class="active">
                <strong>Editar </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Imobilizado_bem', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="complemento">Complemento</label>
                                <input type="text" name="complemento" id="complemento" class="form-control" value="<?php echo $Imobilizado_bem->complemento ?>"
                                    placeholder="Complemento">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="numero_serie" class="required">Número de série<span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="numero_serie" id="numero_serie" class="form-control" value="<?php echo $Imobilizado_bem->numero_serie ?>"
                                    placeholder="Número de série" required>
                            </div>                            
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="dt_aquisicao">Data Aquisição</label>
                                <input type="date" name="dt_aquisicao" id="dt_aquisicao" class="form-control" value="<?php echo $Imobilizado_bem->dt_aquisicao ?>"
                                    placeholder="Data Aquisição">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="quantidade_atual">Quantidade</label>
                                <input type="number" name="quantidade_atual" id="quantidade_atual" class="form-control"
                                    value="<?php echo $Imobilizado_bem->quantidade_atual ?>" placeholder="Quantidade">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data_baixa">Data de Baixa</label>
                                <input type="date" name="data_baixa" id="data_baixa" class="form-control" value="<?php echo $Imobilizado_bem->data_baixa ?>"
                                    placeholder="Data de Baixa">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data_manutencao">Data Manutenção</label>
                                <input type="date" name="data_manutencao" id="data_manutencao" class="form-control"
                                    value="<?php echo $Imobilizado_bem->data_manutencao ?>" placeholder="Data Manutenção">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="vencimento_garantia">Vencimento da Garantia</label>
                                <input type="date" name="vencimento_garantia" id="vencimento_garantia" class="form-control"
                                    value="<?php echo $Imobilizado_bem->vencimento_garantia ?>" placeholder="Vencimento da Garantia">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="vida_util">Vida Util</label>
                                <input type="text" name="vida_util" id="vida_util" class="form-control" value="<?php echo $Imobilizado_bem->vida_util ?>"
                                    placeholder="Vida Util">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="valor_comprado">Valor Comprado</label>
                                <input type="text"  name="valor_comprado" id="valor_comprado" class="form-control"
                                    value="<?php echo $Imobilizado_bem->valor_comprado ?>" placeholder="Valor Comprado">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="valor_reajuste">Valor Reajuste</label>
                                <input type="text"  name="valor_reajuste" id="valor_reajuste" class="form-control"
                                    value="<?php echo $Imobilizado_bem->valor_reajuste ?>" placeholder="Valor Reajuste">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="depreciacao">Depreciação</label>
                                <input type="text" name="depreciacao" id="depreciacao" class="form-control" value="<?php echo $Imobilizado_bem->depreciacao ?>"
                                    placeholder="Depreciação">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="atualizado">Atualizado</label>
                                <input type="text" name="atualizado" id="atualizado" class="form-control" value="<?php echo $Imobilizado_bem->atualizado ?>"
                                    placeholder="Atualizado">
                            </div>
                           <!-- <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="foto">COLOCAR FOTO AQUI</label>
                                <input type="text" name="foto" id="foto" class="form-control" value="<?php echo $Imobilizado_bem->foto ?>"
                                    placeholder="Foto">
                            </div>-->
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="nota_fiscal">Nota Fiscal</label>
                                <input type="text" name="nota_fiscal" id="nota_fiscal" class="form-control" value="<?php echo $Imobilizado_bem->nota_fiscal ?>"
                                    placeholder="Nota fiscal">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data_nfs">Data NFs</label>
                                <input type="date" name="data_nfs" id="data_nfs" class="form-control" value="<?php echo $Imobilizado_bem->data_nfs ?>"
                                    placeholder="Data nfs">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="origem">Origem</label>
                                <input type="text" name="origem" id="origem" class="form-control" value="<?php echo $Imobilizado_bem->origem ?>"
                                    placeholder="Origem">
                            </div>                           
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipo_bem_id">Tipo de Patrimônio</label>
                                <select name="tipo_bem_id" class="form-control" id="tipo_bem_id">
                                    <?php
                                    foreach ($Imobilizado_tipo_bens as $i) {
                                        if ($i->id == $Imobilizado_bem->tipo_bem_id)
                                            echo '<option selected value="' . $i->id . '">' . $i->nome . '</option>';
                                        else
                                            echo '<option value="' . $i->id . '">' . $i->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="conservacao_id">Estado de Conservação</label>
                                <select name="conservacao_id" class="form-control" id="conservacao_id">
                                    <?php
                                    foreach ($Imobilizado_conservacaos as $i) {
                                        if ($i->id == $Imobilizado_bem->conservacao_id)
                                            echo '<option selected value="' . $i->id . '">' . $i->descricao . '</option>';
                                        else
                                            echo '<option value="' . $i->id . '">' . $i->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="situacao_bem_id">Estado de Conservação</label>
                                <select name="situacao_bem_id" class="form-control" id="situacao_bem_id">
                                    <?php
                                    foreach ($Imobilizado_situacao_bens as $i) {
                                        if ($i->id == $Imobilizado_bem->situacao_bem_id)
                                            echo '<option selected value="' . $i->id . '">' . $i->descricao . '</option>';
                                        else
                                            echo '<option value="' . $i->id . '">' . $i->descricao . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="grupo_id">Grupo</label>
                                <select name="grupo_id" class="form-control" id="grupo_id">
                                    <?php
                                    foreach ($Imobilizado_grupos as $i) {
                                        if ($i->id == $Imobilizado_bem->grupo_id)
                                            echo '<option selected value="' . $i->id . '">' . $i->nome . '</option>';
                                        else
                                            echo '<option value="' . $i->id . '">' . $i->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="localidade_id">Localiadade</label>
                                <select name="localidade_id" class="form-control" id="localidade_id">
                                    <?php
                                    foreach ($Rhlocalidades as $r) {
                                        if ($r->id == $Imobilizado_bem->localidade_id)
                                            echo '<option selected value="' . $r->id . '">' . $r->cidade . '</option>';
                                        else
                                            echo '<option value="' . $r->id . '">' . $r->cidade . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>  
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="descricao">Descrição</label>
                                <textarea name="descricao" id="descricao" class="form-control"><?php echo $Imobilizado_bem->descricao ?></textarea>
                            </div> 
                            
                            <div class="form-group col-md-12 col-sm-6 col-xs-12">
                                <label for="observacao">Observação</label>
                                <textarea name="observacao" id="observacao" class="form-control"><?php echo $Imobilizado_bem->observacao ?></textarea>
                            </div>                
                            <input type="hidden" name="id" value="<?php echo $Imobilizado_bem->id;?>">        
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Imobilizado_bem', 'all') ?>" class="btn btn-default"
                                data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
