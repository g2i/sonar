<style>
#azul{
    color: rgba(0, 0, 255, 0.794);
}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="well well-lg">                       
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Numero de serie</label><br>
                            <?php echo $Imobilizado_bem->numero_serie;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Data aquisição </label><br>
                            <?php echo DataBR($Imobilizado_bem->dt_aquisicao);?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Qtd. Atual </label><br>
                            <?php echo $Imobilizado_bem->quantidade_atual;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Data Baixa </label><br>
                            <?php echo DataBR($Imobilizado_bem->data_baixa);?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Data Manutenção </label><br>
                            <?php echo DataBR($Imobilizado_bem->data_manutencao);?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Vencimento Garantia</label><br>
                            <?php echo DataBR($Imobilizado_bem->vencimento_garantia);?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Vida Util </label><br>
                            <?php echo $Imobilizado_bem->vida_util;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Valor Comprado </label><br>
                            <?php echo $Imobilizado_bem->valor_comprado;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Valor Reajuste</label><br>
                            <?php echo $Imobilizado_bem->valor_reajuste;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Depreciação</label><br>
                            <?php echo $Imobilizado_bem->depreciacao;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Atualizado </label><br>
                            <?php echo $Imobilizado_bem->atualizado;?>
                        </div>
                      <!--  <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">COLOCAR A FOTO AQUI </label><br>
                            <?php echo $Imobilizado_bem->foto;?>
                        </div>-->
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Nota Fiscal </label><br>
                            <?php echo $Imobilizado_bem->nota_fiscal;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Data Nfs </label><br>
                            <?php echo DataBR($Imobilizado_bem->data_nfs);?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Origem </label><br>
                            <?php echo $Imobilizado_bem->origem;?>
                        </div>                       
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Tipo Patrimônio </label><br>
                            <?php echo $Imobilizado_bem->getImobilizado_tipo_bem()->nome; ?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Estado de Conservação </label><br>
                            <?php echo $Imobilizado_bem->getImobilizado_conservacao()->descricao ; ?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Situação do Patrimônio</label><br>
                            <?php echo $Imobilizado_bem->getSituacao_bem_id()->descricao;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Grupo </label><br>
                            <?php echo $Imobilizado_bem->getImobilizado_grupo()->nome;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Localidade </label><br>
                            <?php echo $Imobilizado_bem->getRhlocalidade()->cidade;?>
                        </div>                        
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Complemento</label><br>
                            <?php echo $Imobilizado_bem->complemento;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="descricao" id="azul">Descricao</label><br>
                            <?php echo $Imobilizado_bem->descricao;?>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="data_movimento"id="azul">Observação </label><br>
                            <?php echo $Imobilizado_bem->observacao;?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>   
    </div> 
</div>