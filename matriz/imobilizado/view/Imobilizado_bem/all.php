<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Imobilizado bens</h2>
        <ol class="breadcrumb">
            <li>Imobilizado bens</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="numero_serie">Número de série</label>
                                    <input type="text" name="filtro[interno][numero_serie]" id="numero_serie" class="form-control"
                                        value="<?php echo $this->getParam('numero_serie'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tipo_bem_id">Tipo de Patrimônio</label>
                                    <select name="filtro[externo][tipo_bem_id]" class="form-control" id="tipo_bem_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Imobilizado_tipo_bens as $i): ?>
                                        <?php echo '<option value="' . $i->id . '">' . $i->nome . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="grupo_id">Grupo</label>
                                    <select name="filtro[externo][grupo_id]" class="form-control" id="grupo_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Imobilizado_grupos as $i): ?>
                                        <?php echo '<option value="' . $i->id . '">' . $i->nome . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                
                                </div>
                                <div class="col-md-12 text-right">
                                    <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-print"></span> Relatório Geral', 'Rhrelatorio', 'report_geral_bens', NULL, array('class' => 'btn btn-info btn-xs dropdown-toggle','target' => '_blank'));?>
                                    <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-print"></span> Relatório Localidade', 'Rhrelatorio', 'report_localidade_bens', NULL, array('class' => 'btn btn-info btn-xs dropdown-toggle','target' => '_blank'));?>
                                    <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-print"></span> Relatório Situação', 'Rhrelatorio', 'report_situacao_bens', NULL, array('class' => 'btn btn-info btn-xs dropdown-toggle','target' => '_blank'));?>
                                    <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>" class="btn btn-default btn-xs dropdown-togglet"
                                        data-dismiss="modal" data-tool="tooltip" data-placement="bottom" title="Recarregar a página"><span
                                            class="glyphicon glyphicon-refresh "></span></a>
                                    <button type="submit" class="btn btn-default btn-xs dropdown-togglet"  title="Pesquisar"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                    <!-- botao de cadastro -->
                    <div class="text-right">
                        <p>
                            <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Imobilizado_bem', 'add', NULL, array('class' => 'btn btn-primary')); ?>
                        </p>
                    </div>
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Imobilizado_bem', 'all' ,
                                            array('orderBy'=> 'numero_serie')); ?>'>
                                            Número de série
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Imobilizado_bem', 'all' ,
                                            array('orderBy'=> 'descricao')); ?>'>
                                            Descrição
                                        </a>
                                    </th>

                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Imobilizado_bem', 'all' ,
                                            array('orderBy'=> 'quantidade_atual')); ?>'>
                                            Quantidade Atual
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Imobilizado_bem', 'all' ,
                                            array('orderBy'=> 'tipo_bem_id')); ?>'>
                                            Tipo Patrimônio
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Imobilizado_bem', 'all' ,
                                            array('orderBy'=> 'conservacao_id')); ?>'>
                                            Estado de Conservação
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Imobilizado_bem', 'all' ,
                                            array('orderBy'=> 'grupo_id')); ?>'>
                                            Grupo
                                        </a>
                                    </th>
                                    <th>
                                        <a href='<?php echo $this->Html->getUrl(' Imobilizado_bem', 'all' ,
                                            array('orderBy'=> 'localidade_id')); ?>'>
                                            Localidade
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Imobilizado_bens as $i) {
                                    echo '<tr>';
                                      echo '<td>';
                                    echo $this->Html->getLink($i->numero_serie, 'Imobilizado_bem', 'view',
                                        array('id' => $i->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>'; 
                                    echo '<td>';
                                    echo $this->Html->getLink($i->descricao, 'Imobilizado_bem', 'view',
                                        array('id' => $i->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';                                   
                                                                    
                                    echo '<td>';
                                    echo $this->Html->getLink($i->quantidade_atual, 'Imobilizado_bem', 'view',
                                        array('id' => $i->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';                     
                                    echo '<td>';
                                    echo $this->Html->getLink($i->getImobilizado_tipo_bem()->nome, 'Imobilizado_bem', 'view',
                                        array('id' => $i->getImobilizado_tipo_bem()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>';
                                    echo $this->Html->getLink($i->getImobilizado_conservacao()->descricao, 'Imobilizado_bem', 'view',
                                        array('id' => $i->getImobilizado_conservacao()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';                                   
                                    echo '<td>';
                                    echo $this->Html->getLink($i->getImobilizado_grupo()->nome, 'Imobilizado_bem', 'view',
                                        array('id' => $i->getImobilizado_grupo()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>';
                                    echo '<td>'; 
                                    echo $this->Html->getLink($i->getRhlocalidade()->cidade, 'Imobilizado_bem', 'view',
                                        array('id' => $i->getRhlocalidade()->id), // variaveis via GET opcionais
                                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                                    echo '</td>'; 
                                    echo '<td class="actions text-right">';
                                    echo '<div class="dropdown">';
                                        echo '<button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                            <span class="caret"></span></button>';       
                                            echo '<ul class="dropdown-menu">';
                                            echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-exchange"></span> Transferência', 'Imobilizado_movimento', 'all', 
                                            array('id' => $i->id,'first' => 1,'modal' => 1), 
                                            array('class' => 'btn  btn-sm data-toggle','data-toggle' => 'modal'));
                                            echo '</li>';
                                            echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-print"></span> Imprimir', 'Rhrelatorio', 'report_bens', 
                                            array('id' => $i->id), 
                                            array('class' => 'btn  btn-sm data-toggle','target' => '_blank'));
                                            echo '</li>';
                                            echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-pencil"></span> Editar', 'Imobilizado_bem', 'edit', 
                                            array('id' => $i->id), 
                                            array('class' => 'btn  btn-sm data-toggle'));
                                            echo '</li>'; 
                                            echo '<li>';
                                            echo $this->Html->getLink('<span class="fa fa-trash-o"></span> Deletar', 'Imobilizado_bem', 'delete', 
                                            array('id' => $i->id), 
                                            array('class' => 'btn  btn-sm data-toggle','data-toggle' => 'modal'));
                                                echo '</li>';

                                            echo '</ul>';
                                    echo '</div>';
                                echo '</td>';
                                    /**
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="fa fa-pencil-square"></span> ', 'Imobilizado_bem', 'edit', 
                                        array('id' => $i->id), 
                                        array('class' => 'btn btn-xs btn-warning'));
                                    echo '</td>';
                                    echo '<td width="50">';
                                    echo $this->Html->getLink('<span class="fa fa-trash-o"></span> ', 'Imobilizado_bem', 'delete', 
                                        array('id' => $i->id), 
                                        array('class' => 'btn btn-xs btn-danger','data-toggle' => 'modal'));
                                    echo '</td>'; */
                                    echo '</tr>';
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>

                    <script>
                            /* faz a pesquisa com ajax */
                            $(document).ready(function() {
                                $('#search').keyup(function() {
                                    var r = true;
                                    if (r) {
                                        r = false;
                                        $("div.table-responsive").load(
                                        <?php
                                        if (isset($_GET['orderBy']))
                                            echo '"' . $this->Html->getUrl('Imobilizado_bem', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        else
                                            echo '"' . $this->Html->getUrl('Imobilizado_bem', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                        ?>
                                        , function() {
                                            r = true;
                                        });
                                    }
                                });
                            });
                        </script>
                </div>
            </div>
        </div>
    </div>
</div>