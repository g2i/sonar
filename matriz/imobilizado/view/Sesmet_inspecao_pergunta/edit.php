
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Inspeções Respostas</h2>
    <ol class="breadcrumb">
    <li>Inspeções Respostas</li>
    <li class="active">
    <strong> </strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Sesmet_inspecao_pergunta', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg"> 
        <?php  foreach ($Sesmet_perguntas as $s): ?>
            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label for="sesmet_pergunta_id">Sesmet_pergunta</label>
                <?php foreach ($Sesmet_perguntas as $s): ?>
                <input type="text" name="sesmet_pergunta_id" id="sesmet_pergunta_id" class="form-control" value="<?php echo $s->getSesmet_pergunta()->nome?>" placeholder="Dt_cadastro">
                <?php endforeach; ?>
                <select name="sesmet_pergunta_id" class="form-control" id="sesmet_pergunta_id">
                    <?php foreach ($Sesmet_perguntas as $s) {
                        if ($s->id == $Sesmet_inspecao_pergunta->sesmet_pergunta_id)
                            echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                        else
                            echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                <label for="sesmet_resposta_id">Sesmet_resposta</label>
                <select name="sesmet_resposta_id" class="form-control" id="sesmet_resposta_id">
                    <?php
                    foreach ($Sesmet_respostas as $s) {
                        if ($s->id == $Sesmet_inspecao_pergunta->sesmet_resposta_id)
                            echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                        else
                            echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="clearfix"></div> 
        <?php endforeach; ?> 
    </div>
    <input type="hidden" name="id" value="<?php echo $Sesmet_inspecao_pergunta->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Sesmet_inspecao_pergunta', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>