
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Inspeção Perguntas</h2>
    <ol class="breadcrumb">
    <li>Inspeção Perguntas</li>
    <li class="active">
    <strong>Adicionar</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Sesmet_inspecao_pergunta', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dt_cadastro">dt_cadastro</label>
            <input type="datetime" name="dt_cadastro" id="dt_cadastro" class="form-control" value="<?php echo $Sesmet_inspecao_pergunta->dt_cadastro ?>" placeholder="Dt_cadastro">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="dt_modificado">dt_modificado</label>
            <input type="datetime" name="dt_modificado" id="dt_modificado" class="form-control" value="<?php echo $Sesmet_inspecao_pergunta->dt_modificado ?>" placeholder="Dt_modificado">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="sesmet_pergunta_id">sesmet_pergunta_id</label>
            <select name="sesmet_pergunta_id" class="form-control" id="sesmet_pergunta_id">
                <?php
                foreach ($Sesmet_perguntas as $s) {
                    if ($s->id == $Sesmet_inspecao_pergunta->sesmet_pergunta_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="sesmet_inspecao_id">sesmet_inspecao_id</label>
            <select name="sesmet_inspecao_id" class="form-control" id="sesmet_inspecao_id">
                <?php
                foreach ($Sesmet_inspecaos as $s) {
                    if ($s->id == $Sesmet_inspecao_pergunta->sesmet_inspecao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="cadastrado_por">cadastrado_por</label>
            <select name="cadastrado_por" class="form-control" id="cadastrado_por">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Sesmet_inspecao_pergunta->cadastrado_por)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="modificado_por">modificado_por</label>
            <select name="modificado_por" class="form-control" id="modificado_por">
                <?php
                foreach ($Usuarios as $u) {
                    if ($u->id == $Sesmet_inspecao_pergunta->modificado_por)
                        echo '<option selected value="' . $u->id . '">' . $u->senha . '</option>';
                    else
                        echo '<option value="' . $u->id . '">' . $u->senha . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="situacao_id">situacao_id</label>
            <select name="situacao_id" class="form-control" id="situacao_id">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Sesmet_inspecao_pergunta->situacao_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="sesmet_resposta_id">sesmet_resposta_id</label>
            <select name="sesmet_resposta_id" class="form-control" id="sesmet_resposta_id">
                <?php
                foreach ($Sesmet_respostas as $s) {
                    if ($s->id == $Sesmet_inspecao_pergunta->sesmet_resposta_id)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Sesmet_inspecao_pergunta', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>