<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Listagens</h2>
        <ol class="breadcrumb">
            <li>Listagens</li>
            <li class="active">
                <strong>Período</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox">
            <div class="ibox-content">
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhrelatorio', 'print_distribuicao') ?>" target="_blank">
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="inicio"  class="required">Inicio<span class="glyphicon glyphicon-asterisk"></span></label>
                        <div class='input-group '>
                            <input type='date' class="form-control " name="inicio" id="inicio" value="" required/>
                        
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                        <label for="fim" class="required">Fim<span class="glyphicon glyphicon-asterisk"></span></label>
                        <div class='input-group '>
                            <input type='date' class="form-control " name="fim" id="fim"
                                   value="" required/>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-4">
                    <label for="localidade">Localidade</label>
                        <select name="localidade" class="form-control" id="localidade" required>
                            <?php
                                foreach ($Rhlocalidade as $l) {
                                echo '<option value="' . $l->id . '">' . $l->cidade . ' - ' . $l->estado .'</option>';
                                    }
                            ?>
                        </select>
                    </div><div class="clearfix"></div>
                    <div class="form-group col-xs-12 col-md-4">
                            <label for="tipo">Tipo Relatório:</label>
                            <select id="tipo" name="tipo" class="form-control selectPicker">                               
                                <option value="1"> Listagens - Admitidos (Por Período)</option>
                                <option value="2"> Listagens - Demitidos (Por Período)</option>
                                <option value="3"> Listagens - Ferias a Conceder</option>
                                <option value="4"> Listagens - ASO (Periodo Vencimento - Ativos) </option>
                                <option value="5"> Listagens - INSS (Afastamento)</option>
                                <option value="6"> Atestados por Colaborador (geral por localidade)</option>
                                <option value="7"> Listagens - Colaboradores ativos (Por Localidade vs Projetos)</option>
                            </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-4" style="display: none;"  id="historico">
                        <label for="historico" style="color:red;">Histórico</label>
                        <select name="historico" class="form-control">
                            <option value="">Selecione:</option>
                            <?php
                            foreach ($Rhocorrencia_historicos as $r): ?>
                                   <option value="<?= $r->codigo; ?>" data-p="<?= $r->periodicidade; ?>"><?= $r->nome; ?></option>
                            <?php  endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-xs-12 col-md-4" style="display: none;"  id="Rhprojetos">
                        <label for="Rhprojetos" style="color:red;">Projetos</label>
                        <select name="Rhprojetos" class="form-control">
                            <option value="">Selecione:</option>
                            <?php
                            foreach ($Rhprojetos as $r): ?>
                                   <option value="<?= $r->id; ?>" data-p="<?= $r->tipo; ?>"><?= $r->nome; ?></option>
                            <?php  endforeach; ?>
                        </select>
                    </div>
                    
                    <div class="clearfix"></div>
                    <div class="form-group col-xs-12 col-md-4" >                       
                            <button  type="submit" class="btn btn-warning"  title="Gerar Relatório sem Contas Receber"><span class="glyphicon glyphicon-print"></span> Imprimir</button>
                    </div>
                    <div class="clearfix"></div>
                </form>

            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

    $('#tipo').change(function() {
        console.log($(this).val());
        if($(this).val() == 4) {
            $('#historico').show();
        } else {
            $('#historico').hide();
        }
        if($(this).val() == 7) {
            $('#Rhprojetos').show();
        } else {
            $('#Rhprojetos').hide();
        }

    });
});
</script>