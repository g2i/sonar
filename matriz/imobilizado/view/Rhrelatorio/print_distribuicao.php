
<table class="report-container" style="width:100%">
    <thead class="report-header">
        <tr>
           <th class="report-header-cell">
             <div class="row header">
                 <div class="col-3">
                 </div>
                 <div class="col-3">
                   <div class="text-right">
                    </div>
                 </div>
               </div>
           </th>
         </tr>
       </thead>
    <tfoot class="report-footer">
      <tr>
         <td class="report-footer-cell">
           <div class="footer-info">
            <footer></footer>
           </div>
          </td>
      </tr>
    </tfoot>
    <tbody class="report-content">
      <tr>
         <td class="report-content-cell">
            <div class="main">
                <div class="tabela-medidas">
                    <p class="text-left">
                    <br>
                        Período: <strong><?= DataBR($inicio) .' de  '.DataBR($fim) ?></strong><br>
                        <strong><?php if($projetosJson->nome){
                             echo "Projeto: "; 
                             echo $projetosJson->nome;
                        }
                        
                        ?></strong><br>
                        <h2 class="title"><?php echo $titulo; ?></h2>
                    </p>
                   
                  
                    <hr width="100%"/>
                   
                      <table class="table table-hover">
                          <tr>
                              <th>Código</th>
                              <th>Nome</th>
                              <th>Data</th>
                              <th>Localidade</th>
                              <th>Função</th>                          
                          </tr>
                          <?php foreach ($dados as $ad): ?>
                          <tr>
                              <td><?php echo $ad->codigo ?></td>
                              <td class="td-paciente"><?php echo $ad->nome ?></td>
                              <td><?php echo DataBR($ad->datas) ?></td>
                              <td class="td-localidade"><?php echo $ad->local_trabalho ?></td>
                              <td><?php echo $ad->funcao ?></td>
                          </tr>
                            <?php endforeach; ?>
                        </table>                                 
                </div>        
            </div>
          </td>
       </tr>
     </tbody>
     
</table>
<script>
    window.print();
</script>