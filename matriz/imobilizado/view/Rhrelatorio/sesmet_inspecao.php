<table class="report-container" style="width:100%">
   <thead class="report-header">
     <tr>
        <th class="report-header-cell">            
          <div class="row header">              
              <div class="col-3">
                <p class="text-left">                
                    Número: <strong><?php echo $inpessaoDados->id ?></strong><br>
                    Empresa: <strong><?php echo $inpessaoDados->empresa ?></strong><br> 
                    Encarregado:<strong> <?php echo $inpessaoDados->encarregado ?></strong><br>
                    Equipe: <strong><?php echo $inpessaoDados->equipe ?></strong><br>
                    Serviços: <strong><?php echo $inpessaoDados->servico ?></strong> <br> 
                    
                    </p>
              </div>
              <div class="col-3">
                <div class="text-right">                 
                   <p>
                   Data: <strong><?php echo DataBR($inpessaoDados->dataCadastro) ?></strong><br>
                   Local: <strong><?php echo $inpessaoDados->localidade ?></strong><br>
                   Resp. Vistoria: <strong><?php echo $inpessaoDados->responsavelVistoria ?></strong><br> 
                   Veículo: <strong><?php echo $inpessaoDados->veiculo ?></strong><br> 
                     </p><br><br><br><br>
                </div>
              </div>
            </div>
        </th>
      </tr>
    </thead>
    <tfoot class="report-footer">
      <tr>
         <td class="report-footer-cell">
           <div class="footer-info">
            <footer></footer>
           </div>
          </td>
      </tr>
    </tfoot>
    <tbody class="report-content">
        <tr class="tamanho-linha">
            <td class="report-content-cell">
                <div class="main">
                    <div class="tabela-medidas">
                        <div class="grid-container">
                            <div class="row">                              
                                <div class="col-6">                   
                                <h4 class="title text-center">INSPEÇÃO DE SEGURANÇA DO TRABALHO </h4>                                    
                                    <table class="table">
                                            <tr>
                                                <th>Matrícula</th>
                                                <th>Nome do(s) avaliado(s)</th>
                                                <th>Assinatura</th>  
                                            </tr>                                                                
                                            <?php 
                                            foreach($funcDados as $func):?>
                                                <tr class="tamanho-linha">
                                                    <td><?php echo $func['funCod']?></td>
                                                    <td><?php echo $func['funcNome'] ?></td>
                                                    <td></td>
                                                </tr>                                    
                                            <?php endforeach; ?>                                    
                                    </table>  
                                     <table class="table"><br>
                                        <tr class="grups text-left">
                                         <th class="td-grupo"><?php echo $grupos ?></th>
                                                    <th>Resposta</th>                                             
                                        </tr>
                                        <?php foreach($pergDados as $perg): ?>                                            
                                               
                                                                                           
                                                    <tr class="tamanho-linha">
                                                        <td><?php echo $perg['pergunta']?></td>
                                                        <td><?php echo $perg['resposta'] ?></td>
                                                    </tr>                                                                           
                                            
                                        <?php endforeach; ?>  
                                    </table> 
                                    <hr>
                                    <p class="text-left">Observação: <strong><?php echo $inpessaoDados->observacao; ?></strong> </p>             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<script>
  window.print();
</script>