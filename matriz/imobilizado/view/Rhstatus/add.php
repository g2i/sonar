
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhstatus', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="descricao">Descricao</label>
            <input type="text" name="descricao" id="descricao" class="form-control" value="<?php echo $Rhstatus->descricao ?>" placeholder="Descricao">
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhstatus', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>