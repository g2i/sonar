
<div>
    <?php if($this->getParam('modal')){ ?>
        <div class="text-left col-md-6">
            <a href="Javascript:void(0)" class="btn btn-sm" onclick="Navegar('','back')">
                <span class="img img-return"></span></a>
        </div>
        <div class="text-right col-md-6">
            <p><a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('<?php echo $this->Html->getUrl("Anexoocorrencia","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                    <span class="img img-add"></span> Novo Anexo</a></p>
        </div>
    <?php }else{ ?>
    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo anexo', 'Anexoocorrencia', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>
    <?php } ?>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexoocorrencia', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Anexoocorrencia', 'all', array('orderBy' => 'status')); ?>'>
                        status
                    </a>
                </th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Anexoocorrencias as $a) {
                echo '<tr>';
                echo '<td>';
                echo $this->Html->getLink($a->nome, 'Anexoocorrencia', 'view',
                    array('id' => $a->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $this->Html->getLink($a->getRhstatus()->descricao, 'Rhstatus', 'view',
                    array('id' => $a->getRhstatus()->codigo), // variaveis via GET opcionais
                    array('data-toggle' => 'modal')); // atributos HTML opcionais
                echo '</td>';
                if($this->getParam('modal')){
                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip2" title="Deletar Anexo" class="btn btn-sm" style="max-width:50px; max-height:50px;"
             onclick="Navegar(\'' . $this->Html->getUrl("Anexoocorrencia", "delete", array("ajax" => true, "modal" => "1", 'id' => $a->codigo)).'\',\'go\')">
    <span class="img img-remove"></span></a>';
                    echo '</td>';
                    echo '<td width="30" style="padding-top: 0px; padding-bottom: 0px;">';
                    echo $this->Html->getLink('<span class="img img-down"></span> ', 'Download', 'baixar',
                        array('u' => Cript::cript($a->anexo)),
                        array('class' => 'btn btn-sm'));
                    echo '</td>';
                }else {
                    echo '<td width="50">';
                    echo $this->Html->getLink('<span class="img img-edit""></span> ', 'Anexoocorrencia', 'edit',
                        array('id' => $a->codigo),
                        array('class' => 'btn btn-sm'));
                    echo '</td>';
                    echo '<td width="50">';
                    echo $this->Html->getLink('<span class="img img-remove"></span> ', 'Anexoocorrencia', 'delete',
                        array('id' => $a->codigo),
                        array('class' => 'btn btn-sm', 'data-toggle' => 'modal'));
                    echo '</td>';
                }
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Anexoocorrencia', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Anexoocorrencia', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>