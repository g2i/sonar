
<form method="post" role="form" enctype="multipart/form-data" action="<?php echo $this->Html->getUrl('Anexoocorrencia', 'add') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $Anexoocorrencia->nome ?>" placeholder="Nome">
        </div>
        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label class="required" for="anexo">Anexo <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="file" name="anexo" id="anexo" value="<?php echo $Anexoocorrencia->anexo ?>" placeholder="Anexo" required>
        </div>
        <?php if($this->getParam('modal')){ ?>
            <input type="hidden" name="codigo_ocorrencia" value="<?php echo $this->getParam('id');?>" />
        <?php } ?>
        <div class="clearfix"></div>
    </div>
    <?php if($this->getParam('modal')){ ?>
    <input type="hidden" name="modal" value="1" />
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary" value="salvar">
        </div>
    <?php }else{ ?>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Anexoocorrencia', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
    <?php } ?>
</form>