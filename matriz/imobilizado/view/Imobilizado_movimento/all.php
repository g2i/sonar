<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Movimento</h2>
        <ol class="breadcrumb">
            <li>Movimento</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                     <!-- <div class="filtros well">
                        <div class="form">
                            <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-3 form-group">
                                    <label for="data_movimento">data_movimento</label>
                                    <input type="datetime" name="filtro[interno][data_movimento]" id="data_movimento"
                                        class="form-control maskDataTime" value="<?php echo $this->getParam('data_movimento'); ?>">
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tipo_movimento_id">tipo_movimento_id</label>
                                    <select name="filtro[externo][tipo_movimento_id]" class="form-control" id="tipo_movimento_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Imobiliado_tipo_movimentos as $i): ?>
                                        <?php echo '<option value="' . $i->id . '">' . $i->nome . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="localidade_id">localidade_id</label>
                                    <select name="filtro[externo][localidade_id]" class="form-control" id="localidade_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Rhlocalidades as $r): ?>
                                        <?php echo '<option value="' . $r->id . '">' . $r->cidade . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="tipo_bem_id">tipo_bem_id</label>
                                    <select name="filtro[externo][tipo_bem_id]" class="form-control" id="tipo_bem_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Imobilizado_tipo_bens as $i): ?>
                                        <?php echo '<option value="' . $i->id . '">' . $i->nome . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <label for="situacao_id">situacao_id</label>
                                    <select name="filtro[externo][situacao_id]" class="form-control" id="situacao_id">
                                        <?php echo '<option value="">Selecione:</option>';  ?>
                                        <?php foreach ($Situacaos as $s): ?>
                                        <?php echo '<option value="' . $s->id . '">' . $s->nome . '</option>';  ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default botao-impressao"><span class="glyphicon glyphicon-print"></span></button>
                                    <button type="button" class="btn btn-default botao-reset"><span class="glyphicon glyphicon-refresh"></span></button>
                                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>-->
                    <!-- passa o parametro da modal -->
                    <?php if($this->getParam('modal')){ ?>
                    <div class="text-right">
                        <p><a href="Javascript:void(0)" class="btn btn-primary" onclick="Navegar('<?php echo $this->Html->getUrl("Imobilizado_movimento","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                                <span class="glyphicon glyphicon-plus-sign"></span> Nova registro
                            </a></p>
                    </div>
                    <?php }else{ ?>
                        <!-- botao de cadastro -->
                        <div class="text-right">
                            <p>
                                <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Ocorrência', 'Imobilizado_movimento', 'add', NULL, array('class' => 'btn btn-default')); ?>
                            </p>
                        </div>
                    <?php } ?>
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='#'>
                                            Patrimônio Número de série 
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Data Movimento
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Tipo de movimento
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Quantidade
                                        </a>
                                    </th>
                                    <th>
                                        <a href=#'>
                                            Localidade
                                        </a>
                                    </th>                                    
                                    <th>
                                        <a href='#'>
                                            Observação
                                        </a>
                                    </th>                                       
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Imobilizado_movimentos as $i) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $i->getImobilizado_bem()->numero_serie;
                                    echo '</td>';
                                    echo '<td>';
                                    echo DataBR($i->data_movimento);
                                    echo '</td>';
                                    echo '<td>';
                                    echo $i->getImobiliado_tipo_movimento()->nome;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $i->quantidade;
                                    echo '</td>';
                                    echo '<td>';
                                    echo $i->getRhlocalidade()->cidade;
                                    echo '</td>';                                   
                                    echo '<td>';
                                    echo $i->observacao;
                                    echo '</td>';
                                    if($this->getParam('modal')==1) {
                                        echo '<td width="20">';
                                        echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar " class="btn btn-xs btn-warning"
                                        onclick="Navegar(\'' . $this->Html->getUrl("Imobilizado_movimento", "edit", array("ajax" => true, "modal" => "1", 'id' => $i->id)).'\',\'go\')">
                                        <span class="fa fa-pencil-square"></span></a>';
                                        echo '</td>';
                                        echo '<td width="20">';
                                            echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar " class="btn btn-xs btn-danger"
                                            onclick="Navegar(\'' . $this->Html->getUrl("Imobilizado_movimento", "delete", array("ajax" => true, "modal" => "1", 'id' => $i->id)).'\',\'go\')">
                                            <span class="fa fa-trash-o"></span></a>';
                                        echo '</td>'; 
                                    }else{
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Imobilizado_movimento', 'edit', 
                                            array('id' => $i->id), 
                                            array('class' => 'btn btn-warning btn-sm'));
                                        echo '</td>';
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Imobilizado_movimento', 'delete', 
                                            array('id' => $i->id), 
                                            array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                        echo '</td>';
                                        echo '</tr>';
                                    }
                                }
                                ?>
                            </table>
                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>
                   <script>
                    /* faz a pesquisa com ajax */
                    $(document).ready(function() {
                        $('#search').keyup(function() {
                            var r = true;
                            if (r) {
                                r = false;
                                $("div.table-responsive").load(
                                <?php
                                if (isset($_GET['orderBy']))
                                    echo '"' . $this->Html->getUrl('Imobilizado_movimento', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                else
                                    echo '"' . $this->Html->getUrl('Imobilizado_movimento', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                ?>
                                , function() {
                                    r = true;
                                });
                            }
                        });
                    });
                </script>
                </div>
            </div>
        </div>
    </div>
</div>