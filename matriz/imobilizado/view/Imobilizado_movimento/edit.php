<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Transferência</h2>
        <ol class="breadcrumb">
            <li>Transferência</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Imobilizado_movimento', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="data_movimento">Data Transferência</label>
                                <input type="datetime" name="data_movimento" id="data_movimento" class="form-control"
                                    value="<?php echo $Imobilizado_movimento->data_movimento ?>" placeholder="Data_movimento">
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="quantidade">Quantidade</label>
                                <input type="number" name="quantidade" id="quantidade" class="form-control" value="<?php echo $Imobilizado_movimento->quantidade ?>"
                                    placeholder="Quantidade">
                            </div>   
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipo_movimento_id">Tipo de Movimento</label>
                                <select name="tipo_movimento_id" class="form-control" id="tipo_movimento_id">
                                    <?php
                                    foreach ($Imobiliado_tipo_movimentos as $i) {
                                        if ($i->id == $Imobilizado_movimento->tipo_movimento_id)
                                            echo '<option selected value="' . $i->id . '">' . $i->nome . '</option>';
                                        else
                                            echo '<option value="' . $i->id . '">' . $i->nome . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="localidade_id">Localidade</label>
                                <select name="localidade_id" class="form-control" id="localidade_id">
                                <?php
                                foreach ($Rhlocalidades as $r) {
                                    if ($r->id == $Imobilizado_movimento->localidade_id)
                                        echo '<option selected value="' . $r->id . '">' . $r->cidade . '</option>';
                                    else
                                        echo '<option value="' . $r->id . '">' . $r->cidade . '</option>';
                                }
                                ?>
                                </select>
                            </div>
                             <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                         <?php if($this->getParam('modal')){ ?>
                            <input type="hidden" name="bem_id" value="<?php echo $Imobilizado_movimento->bem_id; ?>" />
                         <?php }else{ ?>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="bem_id">Patrimônio</label>
                                <select name="bem_id" class="form-control" id="bem_id">
                                    <?php
                                    foreach ($Imobilizado_bens as $i) {
                                        if ($i->id == $Imobilizado_movimento->bem_id)
                                            echo '<option selected value="' . $i->id . '">' . $i->numero_serie . '</option>';
                                        else
                                            echo '<option value="' . $i->id . '">' . $i->numero_serie . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>                             
                        <?php } ?>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="observacao">Observação</label>
                            <textarea name="observacao" id="observacao" class="form-control"><?php echo $Imobilizado_movimento->observacao ?></textarea>
                        </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="id" value="<?php echo $Imobilizado_movimento->id;?>">
                         <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                         <?php if($this->getParam('modal')){ ?>
                            <input type="hidden" name="modal" value="1" />
                            <div class="text-right">
                                <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">Cancelar</a>
                                <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="salvar">
                            </div>
                        <?php }else{ ?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Imobilizado_movimento', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>