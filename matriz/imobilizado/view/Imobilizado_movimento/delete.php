<form class="form" method="post" action="<?php echo $this->Html->getUrl('Imobilizado_movimento', 'delete') ?>">
    <h1>Confirmação</h1>
    <div class="well well-lg">
        <p>Voce tem certeza que deseja excluir o registro <strong><?php echo $Imobilizado_movimento->getImobiliado_tipo_movimento()->nome; ?></strong>?</p>
    </div>
    <input type="hidden" name="id" value="<?php echo $Imobilizado_movimento->id; ?>">
     <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
     <?php if($this->getParam('modal')){ ?>
    <input type="hidden" name="modal" value="1" />
    <div class="text-right">
        <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">Cancelar</a>
        <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-danger" value="Excluir">
    </div>
    <?php }else{ ?>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Imobilizado_movimento', 'all') ?>" class="btn btn-default"
            data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="Excluir">
    </div>
    <?php } ?> 
</form>