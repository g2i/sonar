<div class="panel panel-default">
    <div class="panel-heading" data-toggle="collapse" data-parent="#configs-acordion"
         href="#collapseCadastro_<?php echo $t->name; ?>">
        <h4 class="panel-title">Configurações de cadastro</h4>
    </div>
    <div id="collapseCadastro_<?php echo $t->name; ?>" class="panel-collapse collapse">
        <div class="panel-body">
            <div class="col-md-12">
                <h4>Titulo dos campos do formuário</h4>
                <hr/>

                <table class="table table-striped">
                    <tr>
                        <th></th>
                        <th>Titulo do Campo</th>
                    </tr>

                    <?php
                    $tbn = $t->name;
                    foreach ($tables_cols->$tbn as $row): ?>
                        <tr>
                            <td><?php echo $row->apresentacao; ?></td>
                            <td>
                                <input type="text" class="form-control"
                                       name="form[<?php echo $t->name; ?>][<?php echo $row->coluna; ?>]"
                                       value="<?php echo $row->coluna; ?>"/>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>