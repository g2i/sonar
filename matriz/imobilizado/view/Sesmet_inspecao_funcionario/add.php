<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">

                <div class="text-right" style="padding-bottom: 20px">
                    <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form method="post" role="form" action="<?php echo $this->Html->getUrl('Sesmet_inspecao_funcionario', 'add') ?>">
                    <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                        são de preenchimento obrigatório.</div>
                    <div class="well well-lg">
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="sesmet_inspecao_id">Inpeção</label>
                            <select name="sesmet_inspecao_id" class="form-control" id="sesmet_inspecao_id">
                                <?php          
                                foreach ($Sesmet_inspecaos as $s) {                
                                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-4 col-sm-6 col-xs-12">
                            <label for="rhfuncionario_id">Funcionário</label>
                            <select name="rhfuncionario_id" class="form-control" id="rhfuncionario_id">
                                <?php
                                foreach ($Rhprofissionais as $r) {
                                    if ($r->codigo == $Sesmet_inspecao_funcionario->rhfuncionario_id)
                                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                                    else
                                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                    <?php if($this->getParam('modal')){ ?>
                    <input type="hidden" name="modal" value="1" />
                    <div class="text-right">
                        <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                            Cancelar
                        </a>
                        <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary" value="salvar">
                    </div>
                    <?php }else{ ?>
                    <div class="text-right">
                        <a href="<?php echo $this->Html->getUrl('Rhprofissional_dependente', 'all') ?>" class="btn btn-default"
                            data-dismiss="modal">Cancelar</a>
                        <input type="submit" class="btn btn-primary" value="salvar">
                    </div>
                    <?php } ?>
                    <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                </form>
            </div><!-- /ibox content -->
        </div><!-- / ibox -->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->
<script>

</script>