<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content no-borders">
                <div class="right">
    <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
</div>
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhfuncao_ocorrencia', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
    <div class="form-group col-sm-4">
            <label for="historico_id" class="required">Histórico<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="historico_id" class="form-control" id="historico_id" required>
                <option value="">Selecione:</option>
                <?php
                foreach ($Rhocorrencia_historicos as $r):
                    if ($r->codigo == $Rhfuncao_ocorrencia->historico_id): ?>
                        <option selected value="<?= $r->codigo;?>"><?= $r->nome;?></option>
                <?php else: ?>
                        <option value="<?= $r->codigo;?>"><?= $r->nome;?></option>
                <?php endif; endforeach; ?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="funcao_id">Função</label>
            <select name="funcao_id" class="form-control" id="funcao_id">
                <?php
                foreach ($Rhprofissional_funcao as $r) {
                    if ($r->codigo == $Rhfuncao_ocorrencia->funcao_id)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4">
                    <label for="obrigatorio">Obrigatório</label>
                    <select name="obrigatorio" class="form-control" id="obrigatorio">
                        <option value="Sim">Sim</option>
                        <option value="Nao">Não</option>
                    </select>
        </div>
       
        <div class="clearfix"></div>
    </div>
    <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
    <?php if($this->getParam('modal')){ ?>
    <input type="hidden" name="modal" value="1"/>
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <button type="submit" class="btn btn-primary" data-toggle="modal" onclick="DialogConfirm('Confirmação','Deseja salvar as Alterações? ')"> Salvar</button>
            <input type="button" onclick="EnviarFormulario('form')" id="validForm" style="display: none;"/>
        </div>
    <?php }else{ ?>
        <div class="text-right">
            <a href="<?php echo $this->Html->getUrl('Rhfuncao_ocorrencia', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
            <input type="submit" class="btn btn-primary" value="salvar">
        </div>
       
    <?php } ?>
    <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
</form>
            </div><!-- /ibox content -->
        </div><!-- / ibox -->
            </div><!--wrapper-->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->
