<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="ibox">
            <div class="ibox-content no-borders">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Rhfuncao_ocorrencia', 'add') ?>">
    <div class="right">
        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
    </div>
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group col-sm-4">
            <label for="historico_id" class="required">Histórico<span class="glyphicon glyphicon-asterisk"></span> </label>
            <select name="historico_id" class="form-control" id="historico_id" required>
                <option value="">Selecione:</option>
                <?php
                foreach ($Rhocorrencia_historicos as $r):
                    if ($r->codigo == $Rhfuncao_ocorrencia->historico_id): ?>
                         <option selected value="<?= $r->codigo; ?>" data-p="<?= $r->periodicidade; ?>"><?= $r->nome; ?></option>
                <?php else: ?>
                <option value="<?= $r->codigo; ?>" data-p="<?= $r->periodicidade; ?>"><?= $r->nome; ?></option>
                <?php endif; endforeach; ?>
            </select>
        </div>
     
        <!-- pega o data-p=" $r->periodicidade -->
         <input type="hidden" id="tipoPeriodicidade" name="tipoPeriodicidade">
        <div class="form-group col-md-4">
            <label for="funcao_id">Função</label>
            <select name="funcao_id" class="form-control" id="funcao_id">
            <option value="">Selecione:</option>
                <?php
                foreach ($Rhprofissional_funcao as $r) {
                    if ($r->codigo == $Rhfuncao_ocorrencia->funcao_id)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="form-group col-md-4">
                    <label for="obrigatorio">Obrigatório</label>
                    <select name="obrigatorio" class="form-control" id="obrigatorio">
                        <option value="Sim">Sim</option>
                        <option value="Nao">Não</option>
                    </select>
        </div>

        <!-- passa o parametro da id do profissional -->
        <?php if($this->getParam('modal')){ ?>
            <input type="hidden" name="codigo_profissional" value="<?php echo $this->getParam('id'); ?>" />
        <?php }else{ ?>
            <!-- / passa o parametro da id do profissional -->

        <div class="form-group col-md-4 col-sm-6 col-xs-12">
            <label for="codigo_profissional">Profissional</label>
            <select name="codigo_profissional" class="form-control" id="codigo_profissional">
                <?php
                foreach ($Rhprofissionais as $r) {
                    if ($r->codigo == $Rhocorrencia->codigo_profissional)
                        echo '<option selected value="' . $r->codigo . '">' . $r->nome . '</option>';
                    else
                        echo '<option value="' . $r->codigo . '">' . $r->nome . '</option>';
                }
                ?>
            </select>
        </div>
        <?php } ?>

        <div class="clearfix"></div>
    </div>
    <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
    <?php if($this->getParam('modal')){ ?>
    <input type="hidden" name="modal" value="1" />
        <div class="text-right">
            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">
                Cancelar
            </a>
            <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="salvar">
        </div>
    <?php }else{ ?>
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Rhocorrencia', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
    <?php } ?>
    <!-- /Comandos para NAVEGAÇÃO ENTRE MODAIS -->
</form>
            </div><!-- /ibox content -->
         </div><!-- / ibox -->
        </div><!-- wrapper -->
    </div> <!-- /col-lg 12 -->
</div><!-- /row -->

<script>
 $(document).ready(function(){
    $('#historico_id').change(function() {
        if($(this).find(':selected').attr('data-p') == "Data Especifica") {
            $('#data_especifica').show();
        } else {
            $('#data_especifica').hide();

             var dataEsp = $(this).find(':selected').attr('data-p');
            $("input[name='tipoPeriodicidade']").val(dataEsp);
        }
    });
});
</script>