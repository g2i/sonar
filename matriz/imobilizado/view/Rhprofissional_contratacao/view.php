<legend>Dados da Contratação</legend>
<p><strong>Tipo</strong>: <?php echo $Rhprofissional_contratacao->tipo;?></p>
<p><strong>Data Inicial</strong>: <?php echo DataBR($Rhprofissional_contratacao->dtInicial);?></p>
<p><strong>Data Final</strong>: <?php echo DataBR($Rhprofissional_contratacao->dtFinal);?></p>
<p>
    <strong>Função</strong>:
    <?php
    echo $this->Html->getLink($Rhprofissional_contratacao->getRhprofissional_funcao()->nome, 'Rhprofissional_funcao', 'view',
        array('id' => $Rhprofissional_contratacao->getRhprofissional_funcao()->codigo), // variaveis via GET opcionais
        array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p><strong>Tipo de Remuneração</strong>: <?php echo $Rhprofissional_contratacao->tiporemuneracao;?></p>
<p><strong>Valor da Remuneracao</strong>: <?php echo number_format($Rhprofissional_contratacao->valorremuneracao,2,',','.');?></p>
<p><strong>Jornada de Trabalho</strong>: <?php echo $Rhprofissional_contratacao->jornada;?></p>
<p><strong>Utiliza Vale Transporte?</strong>: <?php echo $Rhprofissional_contratacao->valetransporte;?></p>
<p><strong>Quantos VT ao dia?</strong>: <?php echo $Rhprofissional_contratacao->valetransporteqtdia;?></p>
<p><strong>Adicional (%)</strong>: <?php echo $Rhprofissional_contratacao->adicional;?></p>
<p><strong>Horários</strong>: <?php echo $Rhprofissional_contratacao->horario;?></p>
<p>
    <strong>Centro de Custo</strong>:
    <?php
    echo $this->Html->getLink($Rhprofissional_contratacao->getRhcentrocusto()->nome, 'Rhcentrocusto', 'view',
        array('id' => $Rhprofissional_contratacao->getRhcentrocusto()->codigo), // variaveis via GET opcionais
        array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p>
    <strong>Profissional</strong>:
    <?php
    echo $this->Html->getLink($Rhprofissional_contratacao->getRhprofissional()->nome, 'Rhprofissional', 'view',
        array('id' => $Rhprofissional_contratacao->getRhprofissional()->codigo), // variaveis via GET opcionais
        array('data-toggle' => 'modal')); // atributos HTML opcionais
    ?>
</p>
<p><strong>Observação</strong>: <?php echo $Rhprofissional_contratacao->observacao;?></p>
<?php
if($Rhprofissional_contratacao->tipo == 'Demissão') {
    echo '<p><strong>Motivo da Demissão:</strong> '. $Rhprofissional_contratacao->motivo .'</p>';
}
?>

<legend>Conta Salário</legend>
<p><strong>Banco</strong>: <?php echo $Rhprofissional_contratacao->banco;?></p>
<p><strong>Agência</strong>: <?php echo $Rhprofissional_contratacao->bancoagencia;?></p>
<p><strong>Conta</strong>: <?php echo $Rhprofissional_contratacao->bancoconta;?></p>
<p><strong>operação</strong>: <?php echo $Rhprofissional_contratacao->bancooperacao;?></p>

<legend>Conta Pessoal</legend>
<p><strong>Banco</strong>: <?php echo $Rhprofissional_contratacao->banco2;?></p>
<p><strong>Agência</strong>: <?php echo $Rhprofissional_contratacao->bancoagencia2;?></p>
<p><strong>Conta</strong>: <?php echo $Rhprofissional_contratacao->bancoconta2;?></p>
<p><strong>operação</strong>: <?php echo $Rhprofissional_contratacao->bancooperacao2;?></p>
