<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situação Patrimônio</h2>
        <ol class="breadcrumb">
            <li>Situação Patrimônio</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" role="form" action="<?php echo $this->Html->getUrl('Imobilizado_situacao_bem', 'edit') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <div class="well well-lg">
                            <div class="form-group">
                                <label for="descricao">Descricao</label>
                                <textarea name="descricao" id="descricao" class="form-control"><?php echo $Imobilizado_situacao_bem->descricao ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $Imobilizado_situacao_bem->id;?>">
                         <!-- Comandos para NAVEGAÇÃO ENTRE MODAIS -->
                         <?php if($this->getParam('modal')){ ?>
                            <input type="hidden" name="modal" value="1" />
                            <div class="text-right">
                                <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">Cancelar</a>
                                <input type="submit" onclick="EnviarFormulario('form');" class="btn btn-primary" value="salvar">
                            </div>
                        <?php }else{ ?>
                        <div class="text-right">
                            <a href="<?php echo $this->Html->getUrl('Imobilizado_situacao_bem', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
                            <input type="submit" class="btn btn-primary" value="salvar">
                        </div>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>