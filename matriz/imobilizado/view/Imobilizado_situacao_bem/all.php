<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situação Patrimônio</h2>
        <ol class="breadcrumb">
            <li>Situação Patrimônio</li>
            <li class="active">
                <strong>Lista</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- formulario de pesquisa -->
                    <div class="filtros well">
                        <div class="form">
                            <form role="form" action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
                                method="post" enctype="application/x-www-form-urlencoded">
                                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
                                <div class="col-md-6 form-group">
                                    <label for="descricao">descricao</label>
                                    <input type="text" name="filtro[interno][descricao]" id="descricao" class="form-control"
                                        value="<?php echo $this->getParam('descricao'); ?>">
                                </div>
                                <div class="col-md-12 text-right">
                                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>"  class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                                title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                                <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                    <!-- passa o parametro da modal -->
                    <?php if($this->getParam('modal')){ ?>
                        <div class="text-right">
                            <p><a href="Javascript:void(0)" class="btn btn-primary" onclick="Navegar('<?php echo $this->Html->getUrl("Imobilizado_situacao_bem","add",array('modal'=>1,'ajax'=>true,'id'=>$this->getParam('id')))?>','go')">
                                    <span class="glyphicon glyphicon-plus-sign"></span> Nova registro
                                </a></p>
                        </div>
                        <?php }else{ ?>
                            <!-- botao de cadastro -->
                            <div class="text-right">
                                <p>
                                    <?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Nova Ocorrência', 'Imobilizado_situacao_bem', 'add', NULL, array('class' => 'btn btn-default')); ?>
                                </p>
                            </div>
                    <?php } ?>
                    <!-- tabela de resultados -->
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <a href='#'>
                                            id
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Descrição
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Cadastrado Por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Data cadastro
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Modificado Por
                                        </a>
                                    </th>
                                    <th>
                                        <a href='#'>
                                            Data Modificado
                                        </a>
                                    </th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                                <?php
                                foreach ($Imobilizado_situacao_bens as $i) {
                                    echo '<tr>';
                                    echo '<td>';
                                    echo $i->id; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $i->descricao; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo $i->getUsuario()->nome; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo DataBR($i->dt_cadastro); 
                                    echo '</td>';
                                     echo '<td>';
                                    echo $i->getUsuario2()->nome; 
                                    echo '</td>';
                                    echo '<td>';
                                    echo DataBR($i->dt_modificado); 
                                    echo '</td>';
                                    if($this->getParam('modal')==1) {
                                        echo '<td width="20">';
                                        echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Editar Grupo" class="btn btn-xs btn-warning"
                                        onclick="Navegar(\'' . $this->Html->getUrl("Imobilizado_situacao_bem", "edit", array("ajax" => true, "modal" => "1", 'id' => $i->id)).'\',\'go\')">
                                        <span class="fa fa-pencil-square"></span></a>';
                                        echo '</td>';
                                        echo '<td width="20">';
                                            echo '<a href="javascript:void(0);" data-placement="bottom" data-toggle="tooltip" title="Deletar Grupo" class="btn btn-xs btn-danger"
                                            onclick="Navegar(\'' . $this->Html->getUrl("Imobilizado_situacao_bem", "delete", array("ajax" => true, "modal" => "1", 'id' => $i->id)).'\',\'go\')">
                                            <span class="fa fa-trash-o"></span></a>';
                                        echo '</td>'; 
                                    }else{   
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Imobilizado_situacao_bem', 'edit', 
                                            array('id' => $i->id), 
                                            array('class' => 'btn btn-warning btn-sm'));
                                        echo '</td>';
                                        echo '<td width="50">';
                                        echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Imobilizado_situacao_bem', 'delete', 
                                            array('id' => $i->id), 
                                            array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                                        echo '</td>';
                                        echo '</tr>';
                                    }
                                }
                                ?>
                            </table>

                            <!-- menu de paginação -->
                            <div style="text-align:center">
                                <?php echo $nav; ?>
                            </div>
                        </div>
                    </div>
                    <script>
                        /* faz a pesquisa com ajax */
                        $(document).ready(function() {
                            $('#search').keyup(function() {
                                var r = true;
                                if (r) {
                                    r = false;
                                    $("div.table-responsive").load(
                                    <?php
                                    if (isset($_GET['orderBy']))
                                        echo '"' . $this->Html->getUrl('Imobilizado_situacao_bem', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    else
                                        echo '"' . $this->Html->getUrl('Imobilizado_situacao_bem', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                                    ?>
                                    , function() {
                                        r = true;
                                    });
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>