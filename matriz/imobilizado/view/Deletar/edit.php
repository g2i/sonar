
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Deletar</h2>
    <ol class="breadcrumb">
    <li>Deletar</li>
    <li class="active">
    <strong>Editar Deletar</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Deletar', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label for="inicio">Inicio</label>
            <input type="datetime" name="inicio" id="inicio" class="form-control" value="<?php echo $Deletar->inicio ?>" placeholder="Inicio">
        </div>
        <div class="form-group">
            <label for="fim">Fim</label>
            <input type="datetime" name="fim" id="fim" class="form-control" value="<?php echo $Deletar->fim ?>" placeholder="Fim">
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Deletar->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Deletar', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>