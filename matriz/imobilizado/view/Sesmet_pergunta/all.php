
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Inspeções Pergunta</h2>
    <ol class="breadcrumb">
    <li>Inspeções Pergunta</li>
    <li class="active">
    <strong>Lista</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
    <!-- formulario de pesquisa -->
    <div class="filtros well">
        <div class="form">
            <form role="form"
            action="<?php echo $this->Html->getUrl(CONTROLLER, ACTION, array('orderBy' => $this->getParam('orderBy'))) ?>"
            method="post" enctype="application/x-www-form-urlencoded">
                <input type="hidden" name="m" value="<?php echo CONTROLLER; ?>">
                <input type="hidden" name="p" value="<?php echo ACTION; ?>">
        <div class="col-md-3 form-group">
            <label for="nome">Nome</label>
            <input type="text" name="filtro[interno][nome]" id="nome" class="form-control" value="<?php echo $this->getParam('nome'); ?>">
        </div>
                <div class="col-md-3 form-group">
                    <label for="situacao_id">Situação</label>
                    <select name="filtro[externo][situacao_id]" class="form-control" id="situacao_id">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Situacaos as $s): ?>
                            <?php echo '<option value="' . $s->id . '">' . $s->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-3 form-group">
                    <label for="sesmet_grupo">Inspeções Grupo</label>
                    <select name="filtro[externo][sesmet_grupo]" class="form-control" id="sesmet_grupo">
                            <?php echo '<option value="">Selecione:</option>';  ?>
                        <?php foreach ($Sesmet_grupos as $s): ?>
                            <?php echo '<option value="' . $s->id . '">' . $s->nome . '</option>';  ?>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-12 text-right">
                <a href="<?php echo $this->Html->getUrl(CONTROLLER, ACTION) ?>" class="btn btn-default" data-dismiss="modal" data-tool="tooltip" data-placement="bottom"
                title="Recarregar a página"><span class="glyphicon glyphicon-refresh "></span></a>
                   <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>

    <!-- botao de cadastro -->
    <div class="text-right">
        <p><?php echo $this->Html->getLink('<span class="glyphicon glyphicon-plus-sign"></span> Novo registro', 'Sesmet_pergunta', 'add', NULL, array('class' => 'btn btn-primary')); ?></p>
    </div>

<!-- tabela de resultados -->
<div class="clearfix">  
    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'id')); ?>'>
                        id
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'nome')); ?>'>
                        Nome
                    </a>
                </th>               
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'sesmet_grupo')); ?>'>
                    Inspeções Grupo
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'situacao_id')); ?>'>
                        Situação
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'cadastrado_por')); ?>'>
                        Cadastrado por
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'dt_cadastro')); ?>'>
                    Data de cadastro
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'modificado_por')); ?>'>
                     Alterado por
                    </a>
                </th>
                <th>
                    <a href='<?php echo $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => 'dt_modificado')); ?>'>
                    Data de alteração 
                    </a>
                </th>               
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($Sesmet_perguntas as $s) {
                echo '<tr>';
                echo '<td>';
                echo $s->id; // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $s->nome; // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $s->getSesmet_grupo()->nome; // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $s->getSituacao()->nome; // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $s->getUsuario()->nome; // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo DataBR($s->dt_cadastro); // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo $s->getUsuario2()->nome; // atributos HTML opcionais
                echo '</td>';
                echo '<td>';
                echo DataBR($s->dt_modificado); // atributos HTML opcionais
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-edit"></span> ', 'Sesmet_pergunta', 'edit',
                            array('id' => $s->id),
                            array('class' => 'btn btn-warning  btn-sm', 'data-toggle' => 'modal')); 
                echo '</td>';
                echo '<td width="50">';
                echo $this->Html->getLink('<span class="glyphicon glyphicon-remove"></span> ', 'Sesmet_pergunta', 'delete', 
                    array('id' => $s->id), 
                    array('class' => 'btn btn-danger btn-sm','data-toggle' => 'modal'));
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>

        <!-- menu de paginação -->
        <div style="text-align:center"><?php echo $nav; ?></div>
    </div>
</div>

<script>
    /* faz a pesquisa com ajax */
    $(document).ready(function() {
        $('#search').keyup(function() {
            var r = true;
            if (r) {
                r = false;
                $("div.table-responsive").load(
                <?php
                if (isset($_GET['orderBy']))
                    echo '"' . $this->Html->getUrl('Sesmet_pergunta', 'all', array('orderBy' => $_GET['orderBy'])) . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                else
                    echo '"' . $this->Html->getUrl('Sesmet_pergunta', 'all') . '&search=" + encodeURIComponent($("#search").val()) + " .table-responsive"';
                ?>
                 , function() {
                    r = true;
                });
            }
        });
    });
</script>
</div>
</div>
</div>
</div>
</div>