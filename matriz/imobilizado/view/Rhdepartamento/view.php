<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content no-borders">
                <legend>Departamento</legend>
                <p><strong>Nome</strong>: <?php echo $Rhdepartamento->nome; ?></p>

                <p>
                    <strong>Status</strong>:
                    <?php
                    echo $this->Html->getLink($Rhdepartamento->getRhstatus()->descricao, 'Rhstatus', 'view',
                        array('id' => $Rhdepartamento->getRhstatus()->codigo), // variaveis via GET opcionais
                        array('data-toggle' => 'modal')); // atributos HTML opcionais
                    ?>
                </p>
            </div>
            <!-- /ibox content -->
        </div>
        <!-- / ibox -->
    </div>
    <!-- /col-lg 12 -->
</div><!-- /row -->