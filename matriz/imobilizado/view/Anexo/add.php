<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anexo</h2>
        <ol class="breadcrumb">
            <li>Anexo</li>
            <li class="active">
                <strong>Adicionar Anexo</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="text-right" style="padding-bottom: 30px">
                        <button type="button" style="alignment: top" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form method="post" role="form" enctype="multipart/form-data" action="<?php echo $this->Html->getUrl('Anexo', 'add') ?>">
                        <div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span>
                            são de preenchimento obrigatório.</div>
                        <input type="hidden" name="dss_id" id="dss_id" value="<?php echo $this->getParam('id');?>" />
                        <div class="well well-lg">
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="titulo">Titulo <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="text" name="titulo" id="titulo" class="form-control" value="<?php echo $Anexo->titulo ?>"
                                    placeholder="Titulo" required>
                            </div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label for="tipo">Tipo</label>
                                <select name="tipo" class="form-control" id="tipo">
                                    <option value="">Selecione:</option>
                                    <?php
                                        foreach ($Tipo_anexos as $r) {                         
                                            echo '<option value="' . $r->id . '">' . $r->nome . '</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                <label class="required" for="url">Anexo <span class="glyphicon glyphicon-asterisk"></span></label>
                                <input type="file" name="url" id="url" value="<?php echo $Anexo->url ?>" placeholder="Anexo">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="text-right">
                        <input type="hidden" name="modal" value="1" />
                            <a href="Javascript:void(0)" class="btn btn-default" onclick="Navegar('','back')">Cancelar</a>
                            <input type="submit" onclick="EnviarFormulario('form')" class="btn btn-primary" value="salvar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>