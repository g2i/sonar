
    <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
    <h2>Anexo</h2>
    <ol class="breadcrumb">
    <li>Anexo</li>
    <li class="active">
    <strong>Editar Anexo</strong>
    </li></ol></div></div>
    <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox float-e-margins">
    <div class="ibox-content">
<form method="post" role="form" action="<?php echo $this->Html->getUrl('Anexo', 'edit') ?>">
<div class="alert alert-info">Os campos marcados com <span class="small glyphicon glyphicon-asterisk"></span> são de preenchimento obrigatório.</div>
    <div class="well well-lg">
        <div class="form-group">
            <label class="required" for="tipo">Tipo <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="number" name="tipo" id="tipo" class="form-control" value="<?php echo $Anexo->tipo ?>" placeholder="Tipo" required>
        </div>
        <div class="form-group">
            <label class="required" for="id_externo">Id_externo <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="number" name="id_externo" id="id_externo" class="form-control" value="<?php echo $Anexo->id_externo ?>" placeholder="Id_externo" required>
        </div>
        <div class="form-group">
            <label class="required" for="titulo">Titulo <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="titulo" id="titulo" class="form-control" value="<?php echo $Anexo->titulo ?>" placeholder="Titulo" required>
        </div>
        <div class="form-group">
            <label class="required" for="url">Url <span class="glyphicon glyphicon-asterisk"></span></label>
            <input type="text" name="url" id="url" class="form-control" value="<?php echo $Anexo->url ?>" placeholder="Url" required>
        </div>
        <div class="form-group">
            <label for="situacao">Situacao</label>
            <select name="situacao" class="form-control" id="situacao">
                <?php
                foreach ($Situacaos as $s) {
                    if ($s->id == $Anexo->situacao)
                        echo '<option selected value="' . $s->id . '">' . $s->nome . '</option>';
                    else
                        echo '<option value="' . $s->id . '">' . $s->nome . '</option>';
                }
                ?>
            </select>
        </div>
    </div>
    <input type="hidden" name="id" value="<?php echo $Anexo->id;?>">
    <div class="text-right">
        <a href="<?php echo $this->Html->getUrl('Anexo', 'all') ?>" class="btn btn-default" data-dismiss="modal">Cancelar</a>
        <input type="submit" class="btn btn-primary" value="salvar">
    </div>
</form>
</div>
</div>
</div>
</div>
</div>