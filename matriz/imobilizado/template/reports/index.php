<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="<?php echo SITE_PATH; ?>/lib/report_2016.3/stimulsoft.viewer.default.css" rel="stylesheet">
    <link href="<?php echo SITE_PATH; ?>/lib/report_2016.3/stimulsoft.designer.office2013.whiteblue.css" rel="stylesheet">
    <script src="<?php echo SITE_PATH; ?>/lib/js/report_2016.3/stimulsoft.reports.js"></script>
    <script src="<?php echo SITE_PATH; ?>/lib/js/report_2016.3/stimulsoft.viewer.js"></script>
    <script src="<?php echo SITE_PATH; ?>/lib/js/report_2016.3/stimulsoft.designer.js"></script>
</head>
<body>
<div class="container">
<?php $this->getContents(); ?>
</div>
</body>
</html>