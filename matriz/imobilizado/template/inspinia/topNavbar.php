<?php
$usuario=Session::get('user');
if($usuario !== null){
?>
<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a role="open-adm" class="animatedClick" data-target='clickExample'>
                    <i class="fa fa-tasks"></i>
                </a>
            </li>
            <li>
                <?php echo $this->Html->getLink('<i class="fa fa-sign-out"></i> Sair', 'Login', 'logout'); ?>
            </li>
        </ul>
    </nav>
</div>

    <div id="right-sidebar" style="display: none" role="adm" class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
        <div class="sidebar-container" full-scroll>
            <ul class="nav nav-tabs navs-2 " role="tablist">
                <li role="presentation" class="active" >
                <a href="#home" aria-controls="home" role="tab"
                data-toggle="tab">Administração</a></li>

                <li role="presentation">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuários</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="background: #2f4050">
                <!-- Administração -->
                <div role="tabpanel" class="tab-pane active" id="home">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-gears"></i> Ajustes </h3>
                        <small><i class="fa fa-tim"></i> Área do administrador.</small>
                    </div>
                    <nav class="navbar-default navbar-static" role="tablist">
                        <div class="sidebar-collapse">
                            <ul class="nav">                       
                                <li <?php echo((CONTROLLER == "Rhprojetos")); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                    href="#Frota_adm"
                                    aria-expanded="false" aria-controls="Frota_adm">
                                        <i class="fa fa-key"></i><span class="nav-label">Administração</span><span
                                            class="fa arrow"></span></a>

                                    <div id="Frota_adm" class="panel-collapse collapse" role="tabpanel"
                                        aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                        style="height: 0px;">
                                        <ul class="nav nav-second-level">    
                                        <!-- campo para chamar a tela all de localizacao-->                                
                                            <li <?php echo(CONTROLLER == "Imobilizado_grupo" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Grupo', 'Imobilizado_grupo', 'all',
                                                array('first' => 1,'modal' => 1),
                                                array('data-toggle' => 'modal'));
                                                ?>                                            
                                            </li>
                                            <li <?php echo(CONTROLLER == "Imobilizado_conservacao" && ACTION == "all"); ?>>
                                            <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Conservação', 'Imobilizado_conservacao', 'all',
                                                array('first' => 1,'modal' => 1),
                                                array('data-toggle' => 'modal'));
                                                ?>   
                                            </li>    
                                            <li <?php echo(CONTROLLER == "Imobilizado_situacao_bem" && ACTION == "all"); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Situacao Patrimônio', 'Imobilizado_situacao_bem', 'all',
                                                    array('first' => 1,'modal' => 1),
                                                    array('data-toggle' => 'modal'));
                                                ?>  
                                            </li> 
                                            <li <?php echo(CONTROLLER == "Imobilizado_tipo_bem" && ACTION == "all"); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Tipo Patrimônio', 'Imobilizado_tipo_bem', 'all',
                                                    array('first' => 1,'modal' => 1),
                                                    array('data-toggle' => 'modal'));
                                                ?> 
                                            </li>  
                                            <li <?php echo(CONTROLLER == "Imobiliado_tipo_movimento" && ACTION == "all"); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-plus-square"></i> Tipo Movimento', 'Imobiliado_tipo_movimento', 'all',
                                                    array('first' => 1,'modal' => 1),
                                                    array('data-toggle' => 'modal'));
                                                ?> 
                                            </li>                                           
                                        </ul>
                                    </div>
                                </li>
                                
                            </ul>
                        </div>
                    </nav>
                </div>
            <!-- Usuário -->
                <div role="tabpanel" class="tab-pane" id="profile">
                    <div class="sidebar-title">
                        <h3><i class="fa fa-users"></i> Usuário</h3>
                        <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                    </div>
                    <nav class="navbar-default navbar-static" role="tablist">
                        <div class="sidebar-collapse">
                            <ul class="nav">
                                <li <?php echo(CONTROLLER == "Usuario" ? 'class="active"' : ''); ?>>
                                    <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse" href="#Usuario"
                                       aria-expanded="false" aria-controls="Usuario">
                                        <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span><span
                                            class="fa arrow"></span></a>

                                    <div id="Usuario" class="panel-collapse collapse" role="tabpanel"
                                         aria-labelledby="collapseListGroupHeading1" aria-expanded="false" style="height: 0px;">
                                        <ul class="nav nav-second-level">
                                            <li <?php echo(CONTROLLER == "Usuario" && ACTION == "conta" ? 'class="active"' : ''); ?>>
                                                <?php echo $this->Html->getLink('<i class="fa fa-user"></i>Conta', 'Usuario', 'conta'); ?>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>


            </div>
        </div>
    </div>
<?php } ?>

